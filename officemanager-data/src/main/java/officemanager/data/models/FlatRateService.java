package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "flat_rate_service", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findByDescriptionLike_FlatRateService", query = "SELECT fr FROM FlatRateService fr WHERE fr.serviceName LIKE :SERVICE_NAME AND fr.activeInd = true"), 
		@NamedQuery(name = "addAmountSold_FlatRateService", query = "UPDATE FlatRateService fr SET fr.amountSold = (fr.amountSold + :AMT) WHERE fr.flatRateServiceId = :FLAT_RATE_SERVICE_ID") })
public class FlatRateService implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -5512283967631914829L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "flat_rate_service_id", unique = true, nullable = false)
	private Long flatRateServiceId;

	@Column(name = "description_long_text_id")
	private Long descriptionLongTextId;

	@Column(name = "service_name", nullable = false, length = 50)
	private String serviceName;

	@Column(name = "amount_sold", nullable = false)
	private short amountSold;

	@Column(name = "barcode", length = 25)
	private String barcode;

	@Column(name = "base_price", nullable = false, precision = 18, scale = 4)
	private BigDecimal basePrice;

	@Column(name = "product_type_cd")
	private Long productTypeCd;

	@Column(name = "category_type_cd")
	private Long categoryTypeCd;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public FlatRateService() {}

	@Override
	public Serializable getPrimaryKey() {
		return flatRateServiceId;
	}

	public Long getFlatRateServiceId() {
		return flatRateServiceId;
	}

	public void setFlatRateServiceId(Long flatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;
	}

	public Long getDescriptionLongTextId() {
		return descriptionLongTextId;
	}

	public void setDescriptionLongTextId(Long descriptionLongTextId) {
		this.descriptionLongTextId = descriptionLongTextId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public short getAmountSold() {
		return amountSold;
	}

	public void setAmountSold(short amountSold) {
		this.amountSold = amountSold;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public Long getProductTypeCd() {
		return productTypeCd;
	}

	public void setProductTypeCd(Long productTypeCd) {
		this.productTypeCd = productTypeCd;
	}

	public Long getCategoryTypeCd() {
		return categoryTypeCd;
	}

	public void setCategoryTypeCd(Long categoryTypeCd) {
		this.categoryTypeCd = categoryTypeCd;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (flatRateServiceId == null ? 0 : flatRateServiceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FlatRateService)) {
			return false;
		}
		final FlatRateService other = (FlatRateService) obj;
		if (flatRateServiceId == null) {
			if (other.flatRateServiceId != null) {
				return false;
			}
		} else if (!flatRateServiceId.equals(other.flatRateServiceId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FlatRateService [flatRateServiceId=" + flatRateServiceId + ", descriptionLongTextId="
				+ descriptionLongTextId + ", serviceName=" + serviceName + ", amountSold=" + amountSold + ", barcode="
				+ barcode + ", basePrice=" + basePrice + ", productTypeCd=" + productTypeCd + ", categoryTypeCd="
				+ categoryTypeCd + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd="
				+ activeInd + "]";
	}
}
