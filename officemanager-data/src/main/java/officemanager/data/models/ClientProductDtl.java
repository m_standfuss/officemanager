package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "client_product_dtl", catalog = "officemanager")
public class ClientProductDtl implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -6474465072980185626L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "detail_id", unique = true, nullable = false)
	private Long detailId;

	@Column(name = "client_product_id", nullable = false)
	private long clientProductId;

	@Column(name = "detail_type_cd")
	private Long detailTypeCd;

	@Column(name = "detail", nullable = false, length = 50)
	private String detail;

	@Column(name = "detail_number")
	private Long detailNumber;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public ClientProductDtl() {}

	@Override
	public Serializable getPrimaryKey() {
		return detailId;
	}

	public Long getDetailId() {
		return detailId;
	}

	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}

	public long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public Long getDetailTypeCd() {
		return detailTypeCd;
	}

	public void setDetailTypeCd(Long detailTypeCd) {
		this.detailTypeCd = detailTypeCd;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Long getDetailNumber() {
		return detailNumber;
	}

	public void setDetailNumber(Long detailNumber) {
		this.detailNumber = detailNumber;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (detailId == null ? 0 : detailId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ClientProductDtl)) {
			return false;
		}
		final ClientProductDtl other = (ClientProductDtl) obj;
		if (detailId == null) {
			if (other.detailId != null) {
				return false;
			}
		} else if (!detailId.equals(other.detailId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ClientProductDtl [detailId=" + detailId + ", clientProductId=" + clientProductId + ", detailTypeCd="
				+ detailTypeCd + ", detail=" + detail + ", detailNumber=" + detailNumber + ", updtPrsnlId="
				+ updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
