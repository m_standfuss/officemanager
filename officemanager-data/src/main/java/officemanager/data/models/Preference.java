package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "preference", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActiveByMeanings_Preference", query = "SELECT p FROM Preference p WHERE "
			+ "p.prefMeaning IN (:PREF_MEANINGS) AND p.activeInd = true"),
	@NamedQuery(name = "inactivateByMeaning_Preference", query = "UPDATE Preference p SET p.activeInd = false WHERE p.prefMeaning = :MEANING") })
public class Preference implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 814836505707043696L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "preference_id", unique = true, nullable = false)
	private Long preferenceId;

	@Column(name = "pref_personnel_id")
	private Long prefPersonnelId;

	@Column(name = "pref_meaning", nullable = false, length = 20)
	private String prefMeaning;

	@Column(name = "pref_value_text")
	private String prefValueText;

	@Column(name = "pref_value_int")
	private Long prefValueInt;

	@Column(name = "pref_value_bool")
	private Boolean prefValueBool;

	@Column(name = "pref_value_decimal", precision = 18, scale = 4)
	private BigDecimal prefValueDecimal;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Preference() {}

	@Override
	public Serializable getPrimaryKey() {
		return preferenceId;
	}

	public Long getPreferenceId() {
		return preferenceId;
	}

	public void setPreferenceId(Long preferenceId) {
		this.preferenceId = preferenceId;
	}

	public Long getPrefPersonnelId() {
		return prefPersonnelId;
	}

	public void setPrefPersonnelId(Long prefPersonnelId) {
		this.prefPersonnelId = prefPersonnelId;
	}

	public String getPrefMeaning() {
		return prefMeaning;
	}

	public void setPrefMeaning(String prefMeaning) {
		this.prefMeaning = prefMeaning;
	}

	public String getPrefValueText() {
		return prefValueText;
	}

	public void setPrefValueText(String prefValueText) {
		this.prefValueText = prefValueText;
	}

	public Long getPrefValueInt() {
		return prefValueInt;
	}

	public void setPrefValueInt(Long prefValueInt) {
		this.prefValueInt = prefValueInt;
	}

	public Boolean getPrefValueBool() {
		return prefValueBool;
	}

	public void setPrefValueBool(Boolean prefValueBool) {
		this.prefValueBool = prefValueBool;
	}

	public BigDecimal getPrefValueDecimal() {
		return prefValueDecimal;
	}

	public void setPrefValueDecimal(BigDecimal prefValueDecimal) {
		this.prefValueDecimal = prefValueDecimal;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (preferenceId == null ? 0 : preferenceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Preference)) {
			return false;
		}
		final Preference other = (Preference) obj;
		if (preferenceId == null) {
			if (other.preferenceId != null) {
				return false;
			}
		} else if (!preferenceId.equals(other.preferenceId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Preference [preferenceId=" + preferenceId + ", prefPersonnelId=" + prefPersonnelId + ", prefMeaning="
				+ prefMeaning + ", prefValueText=" + prefValueText + ", prefValueInt=" + prefValueInt
				+ ", prefValueBool=" + prefValueBool + ", prefValueDecimal=" + prefValueDecimal + ", updtPrsnlId="
				+ updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
