package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "order_comment", catalog = "officemanager")
@NamedQueries({ @NamedQuery(name = "findActiveByOrderIds_OrderComment", query = "SELECT oc FROM OrderComment oc WHERE "
		+ "oc.orderId IN (:ORDER_IDS) AND oc.activeInd = true"),
	@NamedQuery(name = "findActiveByOrderIdAndTypes_OrderComment", query = "SELECT oc FROM OrderComment oc WHERE "
			+ "oc.orderId = :ORDER_ID AND oc.orderCommentTypeCd IN (:TYPE_CDS) AND oc.activeInd = true") })
public class OrderComment implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -4953025714784770158L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "order_comment_id", unique = true, nullable = false)
	private Long orderCommentId;

	@Column(name = "order_comment_type_cd")
	private Long orderCommentTypeCd;

	@Column(name = "order_id", nullable = false)
	private long orderId;

	@Column(name = "long_text_id")
	private Long longTextId;

	@Column(name = "author_id")
	private Long authorId;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public OrderComment() {}

	@Override
	public Serializable getPrimaryKey() {
		return orderCommentId;
	}

	public Long getOrderCommentId() {
		return orderCommentId;
	}

	public void setOrderCommentId(Long orderCommentId) {
		this.orderCommentId = orderCommentId;
	}

	public Long getOrderCommentTypeCd() {
		return orderCommentTypeCd;
	}

	public void setOrderCommentTypeCd(Long orderCommentTypeCd) {
		this.orderCommentTypeCd = orderCommentTypeCd;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public Long getLongTextId() {
		return longTextId;
	}

	public void setLongTextId(Long longTextId) {
		this.longTextId = longTextId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (orderCommentId == null ? 0 : orderCommentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OrderComment)) {
			return false;
		}
		final OrderComment other = (OrderComment) obj;
		if (orderCommentId == null) {
			if (other.orderCommentId != null) {
				return false;
			}
		} else if (!orderCommentId.equals(other.orderCommentId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "OrderComment [orderCommentId=" + orderCommentId + ", orderCommentTypeCd=" + orderCommentTypeCd
				+ ", orderId=" + orderId + ", longTextId=" + longTextId
				+ ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
