package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "client", catalog = "officemanager")
@NamedQueries({ @NamedQuery(name = "findActive_Client", query = "SELECT c FROM Client c WHERE c.activeInd = true"),
	@NamedQuery(name = "findActiveByPersonIds_Client", query = "SELECT c FROM Client c WHERE c.personId IN (:PERSON_IDS) AND c.activeInd = true") })
public class Client implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 165697890276263459L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "client_id", unique = true, nullable = false)
	private Long clientId;

	@Column(name = "person_id")
	private Long personId;

	@Column(name = "attention_of", nullable = false, length = 50)
	private String attentionOf;

	@Column(name = "promotion_club", nullable = false)
	private boolean promotionClub;

	@Column(name = "tax_exempt", nullable = false)
	private boolean taxExempt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "joined_dttm", length = 19)
	private Date joinedDttm;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Client() {}

	@Override
	public Serializable getPrimaryKey() {
		return clientId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getAttentionOf() {
		return attentionOf;
	}

	public void setAttentionOf(String attentionOf) {
		this.attentionOf = attentionOf;
	}

	public boolean isPromotionClub() {
		return promotionClub;
	}

	public void setPromotionClub(boolean promotionClub) {
		this.promotionClub = promotionClub;
	}

	public boolean isTaxExempt() {
		return taxExempt;
	}

	public void setTaxExempt(boolean taxExempt) {
		this.taxExempt = taxExempt;
	}

	public Date getJoinedDttm() {
		return joinedDttm;
	}

	public void setJoinedDttm(Date joinedDttm) {
		this.joinedDttm = joinedDttm;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (clientId ^ clientId >>> 32);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Client other = (Client) obj;
		if (clientId != other.clientId) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Client [clientId=" + clientId + ", personId=" + personId + ", attentionOf=" + attentionOf
				+ ", promotionClub=" + promotionClub + ", taxExempt=" + taxExempt + ", joinedDttm=" + joinedDttm
				+ ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
