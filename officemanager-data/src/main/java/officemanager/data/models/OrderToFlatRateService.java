package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "order_to_flat_rate_service", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActiveProductsByOrderIds_OrderToFlatRateService", query = "SELECT otfrs FROM OrderToFlatRateService otfrs WHERE "
			+ "otfrs.orderId IN (:ORDER_IDS) AND otfrs.activeInd = true"),
	@NamedQuery(name = "findActiveByOrderIdAndClientProductIds_OrderToFlatRateService", query = "SELECT otfrs FROM OrderToFlatRateService otfrs WHERE "
				+ "otfrs.orderId = :ORDER_ID AND otfrs.clientProductId IN ( :CLIENT_PRODUCT_IDS ) AND otfrs.activeInd = true AND otfrs.refundedInd = false"),
	@NamedQuery(name = "inactivateByOrderId_OrderToFlatRateService", query = "UPDATE Service s SET s.activeInd = false WHERE "
			+ "s.orderId = :ORDER_ID"),
	@NamedQuery(name = "inactivateByOrderIdAndPrimaryKeyNotIn_OrderToFlatRateService", query = "UPDATE OrderToFlatRateService otfrs SET otfrs.activeInd = false WHERE "
			+ "otfrs.orderId = :ORDER_ID AND otfrs.orderToFrsId NOT IN (:OTFRS_IDS)") })
public class OrderToFlatRateService implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 1283125817455894365L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "order_to_frs_id", unique = true, nullable = false)
	private Long orderToFrsId;

	@Column(name = "flat_rate_service_id")
	private Long flatRateServiceId;

	@Column(name = "order_id", nullable = false)
	private long orderId;

	@Column(name = "client_product_id")
	private Long clientProductId;

	@Column(name = "service_price", nullable = false, precision = 18, scale = 4)
	private BigDecimal servicePrice;

	@Column(name = "on_sale_ind", nullable = false)
	private boolean onSaleInd;

	@Column(name = "refunded_ind", nullable = false)
	private boolean refundedInd;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public OrderToFlatRateService() {}

	@Override
	public Serializable getPrimaryKey() {
		return orderToFrsId;
	}

	public Long getOrderToFrsId() {
		return orderToFrsId;
	}

	public void setOrderToFrsId(Long orderToFrsId) {
		this.orderToFrsId = orderToFrsId;
	}

	public Long getFlatRateServiceId() {
		return flatRateServiceId;
	}

	public void setFlatRateServiceId(Long flatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public BigDecimal getServicePrice() {
		return servicePrice;
	}

	public void setServicePrice(BigDecimal servicePrice) {
		this.servicePrice = servicePrice;
	}

	public boolean isOnSaleInd() {
		return onSaleInd;
	}

	public void setOnSaleInd(boolean onSaleInd) {
		this.onSaleInd = onSaleInd;
	}

	public boolean isRefundedInd() {
		return refundedInd;
	}

	public void setRefundedInd(boolean refundedInd) {
		this.refundedInd = refundedInd;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (orderToFrsId == null ? 0 : orderToFrsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OrderToFlatRateService)) {
			return false;
		}
		final OrderToFlatRateService other = (OrderToFlatRateService) obj;
		if (orderToFrsId == null) {
			if (other.orderToFrsId != null) {
				return false;
			}
		} else if (!orderToFrsId.equals(other.orderToFrsId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "OrderToFlatRateService [orderToFrsId=" + orderToFrsId + ", flatRateServiceId=" + flatRateServiceId
				+ ", orderId=" + orderId + ", clientProductId=" + clientProductId + ", servicePrice=" + servicePrice
				+ ", onSaleInd=" + onSaleInd + ", refundedInd=" + refundedInd + ", updtPrsnlId=" + updtPrsnlId
				+ ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
