package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "long_text", catalog = "officemanager")
public class LongText implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 3856894672246095084L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "long_text_id", unique = true, nullable = false)
	private Long longTextId;

	@Column(name = "long_text", length = 65535)
	private String longText;

	@Column(name = "format_cd")
	private Long formatCd;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public LongText() {}

	@Override
	public Serializable getPrimaryKey() {
		return longTextId;
	}

	public Long getLongTextId() {
		return longTextId;
	}

	public void setLongTextId(Long longTextId) {
		this.longTextId = longTextId;
	}

	public String getLongText() {
		return longText;
	}

	public void setLongText(String longText) {
		this.longText = longText;
	}

	public Long getFormatCd() {
		return formatCd;
	}

	public void setFormatCd(Long formatCd) {
		this.formatCd = formatCd;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (longTextId == null ? 0 : longTextId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof LongText)) {
			return false;
		}
		final LongText other = (LongText) obj;
		if (longTextId == null) {
			if (other.longTextId != null) {
				return false;
			}
		} else if (!longTextId.equals(other.longTextId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "LongText [longTextId=" + longTextId + ", formatCd=" + formatCd + ", updtPrsnlId=" + updtPrsnlId
				+ ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + ", longText=" + longText + "]";
	}
}
