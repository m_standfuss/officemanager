package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "price_override", catalog = "officemanager")
@NamedQueries({ @NamedQuery(name = "findActiveByDates_PriceOverride", query = "SELECT po FROM PriceOverride po WHERE "
		+ "po.overrideDttm <= :END_DTTM AND po.overrideDttm >= :START_DTTM AND po.activeInd = true") })
public class PriceOverride implements Serializable, OfficeManagerEntity {

	private static final long serialVersionUID = -625810664429158330L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "price_override_id", unique = true, nullable = false)
	private Long priceOverrideId;

	@Column(name = "order_id", nullable = false)
	private Long orderId;

	@Column(name = "parent_entity_name", nullable = false, length = 25)
	private String parentEntityName;

	@Column(name = "parent_entity_id", nullable = false)
	private long parentEntityId;

	@Column(name = "override_reason", length = 255)
	private String overrideReason;

	@Column(name = "original_amount", nullable = false, precision = 18, scale = 4)
	private BigDecimal originalAmount;

	@Column(name = "override_amount", nullable = false, precision = 18, scale = 4)
	private BigDecimal overrideAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "override_dttm", length = 19)
	private Date overrideDttm;

	@Column(name = "override_prsnl_id")
	private Long overridePrsnlId;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return getPriceOverrideId();
	}

	public Long getPriceOverrideId() {
		return priceOverrideId;
	}

	public void setPriceOverrideId(Long priceOverrideId) {
		this.priceOverrideId = priceOverrideId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getParentEntityName() {
		return parentEntityName;
	}

	public void setParentEntityName(String parentEntityName) {
		this.parentEntityName = parentEntityName;
	}

	public long getParentEntityId() {
		return parentEntityId;
	}

	public void setParentEntityId(long parentEntityId) {
		this.parentEntityId = parentEntityId;
	}

	public String getOverrideReason() {
		return overrideReason;
	}

	public void setOverrideReason(String overrideReason) {
		this.overrideReason = overrideReason;
	}

	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}

	public BigDecimal getOverrideAmount() {
		return overrideAmount;
	}

	public void setOverrideAmount(BigDecimal overrideAmount) {
		this.overrideAmount = overrideAmount;
	}

	public Date getOverrideDttm() {
		return overrideDttm;
	}

	public void setOverrideDttm(Date overrideDttm) {
		this.overrideDttm = overrideDttm;
	}

	public Long getOverridePrsnlId() {
		return overridePrsnlId;
	}

	public void setOverridePrsnlId(Long overridePrsnlId) {
		this.overridePrsnlId = overridePrsnlId;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}
}
