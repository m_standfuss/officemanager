package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "phone", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findByPhoneNumber_Phone", query = "SELECT p FROM Phone p WHERE "
			+ "p.phoneNumber = :PHONE_NUMBER AND p.activeInd = true"),
	@NamedQuery(name = "findActiveByPersonIds_Phone", query = "SELECT p FROM Phone p WHERE "
			+ "p.personId IN (:PERSON_IDS) AND p.activeInd = true"),
	@NamedQuery(name = "findActivePrimaryByPersonIds_Phone", query = "SELECT p FROM Phone p WHERE "
			+ "p.personId IN (:PERSON_IDS) AND p.primaryPhoneInd = true AND p.activeInd = true"),
	@NamedQuery(name = "disablePrimaryPhone_Phone", query = "UPDATE Phone p set p.primaryPhoneInd = false WHERE "
				+ "p.personId = :PERSON_ID"),
		@NamedQuery(name = "disablePhonesForPersonWithActivePhones_Phone", query = "UPDATE Phone p SET p.activeInd = false WHERE p.personId = :PERSON_ID AND p.phoneId NOT IN (:PHONE_IDS)"),
		@NamedQuery(name = "disablePhonesForPerson_Phone", query = "UPDATE Phone p SET p.activeInd = false WHERE p.personId = :PERSON_ID") })
public class Phone implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -1142631843637582249L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "phone_id", unique = true, nullable = false)
	private Long phoneId;

	@Column(name = "person_id", nullable = false)
	private long personId;

	@Column(name = "phone_type_cd")
	private Long phoneTypeCd;

	@Column(name = "phone_number", nullable = false, length = 20)
	private String phoneNumber;

	@Column(name = "extension", nullable = false, length = 15)
	private String extension;

	@Column(name = "primary_phone_ind", nullable = false)
	private boolean primaryPhoneInd;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Phone() {}

	@Override
	public Serializable getPrimaryKey() {
		return phoneId;
	}

	public Long getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(Long phoneId) {
		this.phoneId = phoneId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public Long getPhoneTypeCd() {
		return phoneTypeCd;
	}

	public void setPhoneTypeCd(Long phoneTypeCd) {
		this.phoneTypeCd = phoneTypeCd;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getExtension() {
		return extension;
	}

	public boolean isPrimaryPhoneInd() {
		return primaryPhoneInd;
	}

	public void setPrimaryPhoneInd(boolean primaryPhoneInd) {
		this.primaryPhoneInd = primaryPhoneInd;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (phoneId == null ? 0 : phoneId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Phone)) {
			return false;
		}
		final Phone other = (Phone) obj;
		if (phoneId == null) {
			if (other.phoneId != null) {
				return false;
			}
		} else if (!phoneId.equals(other.phoneId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Phone [phoneId=" + phoneId + ", personId=" + personId + ", phoneTypeCd=" + phoneTypeCd
				+ ", phoneNumber=" + phoneNumber + ", extension=" + extension + ", updtPrsnlId=" + updtPrsnlId
				+ ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
