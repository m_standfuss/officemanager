package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sale", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findByDateParentEntityIds_Sale", query = "SELECT s FROM Sale s WHERE "
			+ "s.startDttm <= :DATE AND (s.endDttm >= :DATE OR s.endDttm is null) AND "
			+ "s.parentEntityName = :PARENT_ENTITY_NAME AND s.parentEntityId IN (:PARENT_ENTITY_IDS) AND s.activeInd = true"),
	@NamedQuery(name = "inactivateByParentEntity_Sale", query = "UPDATE Sale s SET s.activeInd = false WHERE "
			+ "s.parentEntityName = :PARENT_ENTITY_NAME AND s.parentEntityId = :PARENT_ENTITY_ID") })
public class Sale implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -7420466602441760122L;

	public static Sale getDefaultSale() {
		Sale sale = new Sale();
		sale.saleId = null;
		sale.activeInd = true;
		sale.amountOff = new BigDecimal(0);
		sale.endDttm = new Date(0);
		sale.parentEntityId = 0L;
		sale.parentEntityName = null;
		sale.saleTypeCd = 0L;
		sale.startDttm = new Date(0);
		sale.updtDttm = new Date();
		sale.updtPrsnlId = 0L;
		return sale;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "sale_id", unique = true, nullable = false)
	private Long saleId;

	@Column(name = "parent_entity_name", nullable = false, length = 25)
	private String parentEntityName;

	@Column(name = "parent_entity_id", nullable = false)
	private long parentEntityId;

	@Column(name = "sale_name", nullable = false, length = 50)
	private String saleName;

	@Column(name = "sale_type_cd")
	private Long saleTypeCd;

	@Column(name = "amount_off", nullable = false, precision = 18, scale = 4)
	private BigDecimal amountOff;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_dttm", length = 19)
	private Date startDttm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_dttm", length = 19)
	private Date endDttm;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Sale() {}

	@Override
	public Serializable getPrimaryKey() {
		return saleId;
	}

	public Long getSaleId() {
		return saleId;
	}

	public void setSaleId(Long saleId) {
		this.saleId = saleId;
	}

	public String getParentEntityName() {
		return parentEntityName;
	}

	public void setParentEntityName(String parentEntityName) {
		this.parentEntityName = parentEntityName;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public long getParentEntityId() {
		return parentEntityId;
	}

	public void setParentEntityId(long parentEntityId) {
		this.parentEntityId = parentEntityId;
	}

	public Long getSaleTypeCd() {
		return saleTypeCd;
	}

	public void setSaleTypeCd(Long saleTypeCd) {
		this.saleTypeCd = saleTypeCd;
	}

	public BigDecimal getAmountOff() {
		return amountOff;
	}

	public void setAmountOff(BigDecimal amountOff) {
		this.amountOff = amountOff;
	}

	public Date getStartDttm() {
		return startDttm;
	}

	public void setStartDttm(Date startDttm) {
		this.startDttm = startDttm;
	}

	public Date getEndDttm() {
		return endDttm;
	}

	public void setEndDttm(Date endDttm) {
		this.endDttm = endDttm;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (saleId == null ? 0 : saleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Sale)) {
			return false;
		}
		final Sale other = (Sale) obj;
		if (saleId == null) {
			if (other.saleId != null) {
				return false;
			}
		} else if (!saleId.equals(other.saleId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Sale [saleId=" + saleId + ", parentEntityName=" + parentEntityName + ", parentEntityId="
				+ parentEntityId + ", saleTypeCd=" + saleTypeCd + ", amountOff=" + amountOff + ", startDttm="
				+ startDttm + ", endDttm=" + endDttm + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + "]";
	}
}
