package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "order_to_payment", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findActiveByOrderIds_OrderToPayment", query = "SELECT otp FROM OrderToPayment otp WHERE "
				+ "otp.orderId IN (:ORDER_IDS) AND otp.activeInd = true") })
public class OrderToPayment implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 6241936234030544970L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "order_to_payment_id", unique = true, nullable = false)
	private Long orderToPaymentId;

	@Column(name = "order_id", nullable = false)
	private long orderId;

	@Column(name = "payment_id", nullable = false)
	private long paymentId;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public OrderToPayment() {}

	@Override
	public Serializable getPrimaryKey() {
		return orderToPaymentId;
	}

	public Long getOrderToPaymentId() {
		return orderToPaymentId;
	}

	public void setOrderToPaymentId(Long orderToPaymentId) {
		this.orderToPaymentId = orderToPaymentId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (orderToPaymentId == null ? 0 : orderToPaymentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OrderToPayment)) {
			return false;
		}
		final OrderToPayment other = (OrderToPayment) obj;
		if (orderToPaymentId == null) {
			if (other.orderToPaymentId != null) {
				return false;
			}
		} else if (!orderToPaymentId.equals(other.orderToPaymentId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "OrderToPayment [orderToPaymentId=" + orderToPaymentId + ", orderId=" + orderId + ", paymentId="
				+ paymentId + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd
				+ "]";
	}
}
