package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "address", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActiveByPersonIds_Address", query = "SELECT a FROM Address a WHERE a.personId IN (:PERSON_IDS) AND a.activeInd = true"),
	@NamedQuery(name = "findPrimaryAddressByPersonIds_Address", query = "SELECT a FROM Address a WHERE a.personId IN (:PERSON_IDS) AND a.primaryAddressInd = true AND a.activeInd = true"),
	@NamedQuery(name = "disablePrimaryAddressForPerson_Address", query = "UPDATE Address a SET a.primaryAddressInd = false WHERE a.personId = :PERSON_ID"),
	@NamedQuery(name = "disableAddressesForPersonWithActiveAddresses_Address", query = "UPDATE Address a SET a.activeInd = false WHERE a.personId = :PERSON_ID AND a.addressId NOT IN (:ADDRESS_IDS)"),
	@NamedQuery(name = "disableAddressesForPerson_Address", query = "UPDATE Address a SET a.activeInd = false WHERE a.personId = :PERSON_ID") })

public class Address implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 7496613255569601475L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "address_id", unique = true, nullable = false)
	private Long addressId;

	@Column(name = "person_id", nullable = false)
	private long personId;

	@Column(name = "address_type_cd")
	private Long addressTypeCd;

	@Column(name = "address_1", nullable = true, length = 70)
	private String address1;

	@Column(name = "address_2", nullable = true, length = 70)
	private String address2;

	@Column(name = "address_3", nullable = true, length = 70)
	private String address3;

	@Column(name = "city", nullable = false, length = 30)
	private String city;

	@Column(name = "zip", nullable = false, length = 20)
	private String zip;

	@Column(name = "state", nullable = false, length = 10)
	private String state;

	@Column(name = "primary_address_ind", nullable = false)
	private boolean primaryAddressInd;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Address() {}

	@Override
	public Serializable getPrimaryKey() {
		return addressId;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public Long getAddressTypeCd() {
		return addressTypeCd;
	}

	public void setAddressTypeCd(Long addressTypeCd) {
		this.addressTypeCd = addressTypeCd;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getState() {
		return state;
	}

	public final boolean isPrimaryAddressInd() {
		return primaryAddressInd;
	}

	public final void setPrimaryAddressInd(boolean primaryAddressInd) {
		this.primaryAddressInd = primaryAddressInd;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (addressId == null ? 0 : addressId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Address other = (Address) obj;
		if (addressId == null) {
			if (other.addressId != null) {
				return false;
			}
		} else if (!addressId.equals(other.addressId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", personId=" + personId + ", addressTypeCd=" + addressTypeCd
				+ ", address1=" + address1 + ", address2=" + address2 + ", address3=" + address3 + ", city=" + city
				+ ", zip=" + zip + ", state=" + state + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + "]";
	}

}
