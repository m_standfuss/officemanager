package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "orders", catalog = "officemanager")
public class Order implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -3726880259708708882L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "order_id", unique = true, nullable = false)
	private Long orderId;

	@Column(name = "client_id")
	private Long clientId;

	@Column(name = "order_status_cd")
	private Long orderStatusCd;

	@Column(name = "order_type_cd")
	private Long orderTypeCd;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dttm", length = 19)
	private Date createdDttm;

	@Column(name = "created_prsnl_id")
	private Long createdPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "closed_dttm", length = 19)
	private Date closedDttm;

	@Column(name = "closed_prsnl_id")
	private Long closedPrsnlId;

	@Column(name = "parent_order_id")
	private Long parentOrderId;

	@Column(name = "warranty_comp_id")
	private Long warrantyCompId;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Order() {}

	@Override
	public Serializable getPrimaryKey() {
		return orderId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getOrderStatusCd() {
		return orderStatusCd;
	}

	public void setOrderStatusCd(Long orderStatusCd) {
		this.orderStatusCd = orderStatusCd;
	}

	public Long getOrderTypeCd() {
		return orderTypeCd;
	}

	public void setOrderTypeCd(Long orderTypeCd) {
		this.orderTypeCd = orderTypeCd;
	}

	public Date getCreatedDttm() {
		return createdDttm;
	}

	public void setCreatedDttm(Date createdDttm) {
		this.createdDttm = createdDttm;
	}

	public Long getCreatedPrsnlId() {
		return createdPrsnlId;
	}

	public void setCreatedPrsnlId(Long createdPrsnlId) {
		this.createdPrsnlId = createdPrsnlId;
	}

	public Date getClosedDttm() {
		return closedDttm;
	}

	public void setClosedDttm(Date closedDttm) {
		this.closedDttm = closedDttm;
	}

	public Long getClosedPrsnlId() {
		return closedPrsnlId;
	}

	public void setClosedPrsnlId(Long closedPrsnlId) {
		this.closedPrsnlId = closedPrsnlId;
	}

	public Long getParentOrderId() {
		return parentOrderId;
	}

	public void setParentOrderId(Long parentOrderId) {
		this.parentOrderId = parentOrderId;
	}

	public Long getWarrantyCompId() {
		return warrantyCompId;
	}

	public void setWarrantyCompId(Long warrantyCompId) {
		this.warrantyCompId = warrantyCompId;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (orderId == null ? 0 : orderId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Order)) {
			return false;
		}
		final Order other = (Order) obj;
		if (orderId == null) {
			if (other.orderId != null) {
				return false;
			}
		} else if (!orderId.equals(other.orderId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", clientId=" + clientId + ", orderStatusCd=" + orderStatusCd
				+ ", createdDttm=" + createdDttm + ", createdPrsnlId=" + createdPrsnlId + ", closedDttm=" + closedDttm
				+ ", closedPrsnlId=" + closedPrsnlId + ", parentOrderId=" + parentOrderId + ", warrantyCompId="
				+ warrantyCompId + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd="
				+ activeInd + "]";
	}
}
