package officemanager.data.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "permission", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActive_Permission", query = "SELECT p FROM Permission p WHERE p.activeInd = true"), })
public class Permission implements Serializable, OfficeManagerEntity {

	private static final long serialVersionUID = -7590577171099724337L;

	@Id
	@Column(name = "permission_name", unique = true, nullable = false)
	private String permissionName;

	@Column(name = "display_name")
	private String displayName;

	@Column(name = "category_cd")
	private Long categoryCd;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return permissionName;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Long getCategoryCd() {
		return categoryCd;
	}

	public void setCategoryCd(Long categoryCd) {
		this.categoryCd = categoryCd;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}
}
