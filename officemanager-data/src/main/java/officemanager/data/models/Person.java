package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "person", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findActiveByPersonIds_Person", query = "SELECT p FROM Person p WHERE "
				+ "p.personId IN (:PERSON_IDS) AND p.activeInd = true"),
		@NamedQuery(name = "findActiveByNameFirstLike_Person", query = "SELECT p FROM Person p WHERE "
				+ "p.nameFirst LIKE :NAME_FIRST AND p.activeInd = true"),
		@NamedQuery(name = "findActiveByNameLastLike_Person", query = "SELECT p FROM Person p WHERE "
				+ "p.nameLast LIKE :NAME_LAST AND p.activeInd = true"),
		@NamedQuery(name = "findActiveByBothNamesLike_Person", query = "SELECT p FROM Person p WHERE "
				+ "p.nameFirst LIKE :NAME_FIRST AND p.nameLast LIKE :NAME_LAST AND p.activeInd = true") })
public class Person implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 608228198768654357L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "person_id", unique = true, nullable = false)
	private Long personId;

	@Column(name = "name_last", nullable = false, length = 50)
	private String nameLast;

	@Column(name = "name_first", nullable = false, length = 30)
	private String nameFirst;

	@Column(name = "name_full_formatted", nullable = false, length = 80)
	private String nameFullFormatted;

	@Column(name = "email", nullable = false, length = 50)
	private String email;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Person() {}

	@Override
	public Serializable getPrimaryKey() {
		return personId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getNameLast() {
		return nameLast;
	}

	public void setNameLast(String nameLast) {
		this.nameLast = nameLast;
	}

	public String getNameFirst() {
		return nameFirst;
	}

	public void setNameFirst(String nameFirst) {
		this.nameFirst = nameFirst;
	}

	public String getNameFullFormatted() {
		return nameFullFormatted;
	}

	public void setNameFullFormatted(String nameFullFormatted) {
		this.nameFullFormatted = nameFullFormatted;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (personId == null ? 0 : personId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Person)) {
			return false;
		}
		final Person other = (Person) obj;
		if (personId == null) {
			if (other.personId != null) {
				return false;
			}
		} else if (!personId.equals(other.personId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Person [personId=" + personId + ", nameLast=" + nameLast + ", nameFirst=" + nameFirst
				+ ", nameFullFormatted=" + nameFullFormatted + ", email=" + email + ", updtPrsnlId=" + updtPrsnlId
				+ ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
