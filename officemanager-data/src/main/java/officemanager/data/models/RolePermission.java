package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "role_permission", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActiveByRoleIds_RolePermission", query = "SELECT rp FROM RolePermission rp WHERE "
			+ "rp.roleId IN (:ROLE_IDS) AND rp.activeInd = true"),
	@NamedQuery(name = "inactivateByRoleId_RolePermission", query = "UPDATE RolePermission rp SET rp.activeInd = false WHERE rp.roleId = :ROLE_ID"),
		@NamedQuery(name = "inactivateByRoleIdExcept_RolePermission", query = "UPDATE RolePermission rp SET rp.activeInd = false WHERE rp.roleId = :ROLE_ID AND rp.rolePermissionId NOT IN (:ROLE_PERMISSION_IDS)") })
public class RolePermission implements Serializable, OfficeManagerEntity {

	private static final long serialVersionUID = -8916537942706256257L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "role_permission_id", unique = true, nullable = false)
	private Long rolePermissionId;

	@Column(name = "role_id")
	private Long roleId;

	@Column(name = "permission_name")
	private String permissionName;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return rolePermissionId;
	}

	public Long getRolePermissionId() {
		return rolePermissionId;
	}

	public void setRolePermissionId(Long rolePermissionId) {
		this.rolePermissionId = rolePermissionId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}
}
