package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "payment", catalog = "officemanager")
@NamedQueries({ @NamedQuery(name = "findActiveByDates_Payment", query = "SELECT p FROM Payment p WHERE "
		+ "p.createdDttm <= :END_DTTM AND p.createdDttm >= :START_DTTM AND p.activeInd = true") })
public class Payment implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 204946546796131873L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "payment_id", unique = true, nullable = false)
	private Long paymentId;

	@Column(name = "payment_type_cd")
	private Long paymentTypeCd;

	@Column(name = "amount", nullable = false, precision = 18, scale = 4)
	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dttm", length = 19)
	private Date createdDttm;

	@Column(name = "created_prsnl_id")
	private Long createdPrsnlId;

	@Column(name = "payment_nbr", nullable = false, length = 50)
	private String paymentNbr;

	@Column(name = "payment_desc", nullable = false, length = 50)
	private String paymentDesc;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Payment() {}

	@Override
	public Serializable getPrimaryKey() {
		return paymentId;
	}

	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	public Long getPaymentTypeCd() {
		return paymentTypeCd;
	}

	public void setPaymentTypeCd(Long paymentTypeCd) {
		this.paymentTypeCd = paymentTypeCd;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDttm() {
		return createdDttm;
	}

	public void setCreatedDttm(Date createdDttm) {
		this.createdDttm = createdDttm;
	}

	public Long getCreatedPrsnlId() {
		return createdPrsnlId;
	}

	public void setCreatedPrsnlId(Long createdPrsnlId) {
		this.createdPrsnlId = createdPrsnlId;
	}

	public String getPaymentNbr() {
		return paymentNbr;
	}

	public void setPaymentNbr(String paymentNbr) {
		this.paymentNbr = paymentNbr;
	}

	public String getPaymentDesc() {
		return paymentDesc;
	}

	public void setPaymentDesc(String paymentDesc) {
		this.paymentDesc = paymentDesc;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (paymentId == null ? 0 : paymentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Payment)) {
			return false;
		}
		final Payment other = (Payment) obj;
		if (paymentId == null) {
			if (other.paymentId != null) {
				return false;
			}
		} else if (!paymentId.equals(other.paymentId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Payment [paymentId=" + paymentId + ", paymentTypeCd=" + paymentTypeCd + ", amount=" + amount
				+ ", createdDttm=" + createdDttm + ", createdPrsnlId=" + createdPrsnlId + ", paymentNbr=" + paymentNbr
				+ ", paymentDesc=" + paymentDesc + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + "]";
	}
}
