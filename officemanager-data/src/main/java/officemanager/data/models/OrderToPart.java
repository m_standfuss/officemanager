package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "order_to_part", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActiveProductsByOrderIds_OrderToPart", query = "SELECT otp FROM OrderToPart otp WHERE "
			+ "otp.orderId IN (:ORDER_IDS) AND otp.activeInd = true"),
	@NamedQuery(name = "inactivateByOrderId_OrderToPart", query = "UPDATE OrderToPart otp SET "
			+ "otp.activeInd = false WHERE otp.orderId = :ORDER_ID"),
	@NamedQuery(name = "inactivateByOrderIdAndPrimaryKeysNotIn_OrderToPart", query = "UPDATE OrderToPart otp SET "
				+ "otp.activeInd = false WHERE otp.orderId = :ORDER_ID AND otp.orderToPartId NOT IN (:ORDER_TO_PART_IDS)") })
public class OrderToPart implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 3248726339570684521L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "order_to_part_id", unique = true, nullable = false)
	private Long orderToPartId;

	@Column(name = "part_id")
	private Long partId;

	@Column(name = "order_id", nullable = false)
	private long orderId;

	@Column(name = "quantity", nullable = false)
	private long quantity;

	@Column(name = "part_price", nullable = false, precision = 18, scale = 4)
	private BigDecimal partPrice;

	@Column(name = "client_product_id")
	private Long clientProductId;

	@Column(name = "on_sale_ind", nullable = false)
	private boolean onSaleInd;

	@Column(name = "refunded_ind", nullable = false)
	private boolean refundedInd;

	@Column(name = "tax_exempt_ind", nullable = false)
	private boolean taxExemptInd;

	@Column(name = "taxed_amount", nullable = false, precision = 18, scale = 4)
	private BigDecimal taxedAmount;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public OrderToPart() {}

	@Override
	public Serializable getPrimaryKey() {
		return orderToPartId;
	}

	public Long getOrderToPartId() {
		return orderToPartId;
	}

	public void setOrderToPartId(Long orderToPartId) {
		this.orderToPartId = orderToPartId;
	}

	public Long getPartId() {
		return partId;
	}

	public void setPartId(Long partId) {
		this.partId = partId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPartPrice() {
		return partPrice;
	}

	public void setPartPrice(BigDecimal partPrice) {
		this.partPrice = partPrice;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public boolean isOnSaleInd() {
		return onSaleInd;
	}

	public void setOnSaleInd(boolean onSaleInd) {
		this.onSaleInd = onSaleInd;
	}

	public boolean isRefundedInd() {
		return refundedInd;
	}

	public void setRefundedInd(boolean refundedInd) {
		this.refundedInd = refundedInd;
	}

	public boolean isTaxExemptInd() {
		return taxExemptInd;
	}

	public void setTaxExemptInd(boolean taxExemptInd) {
		this.taxExemptInd = taxExemptInd;
	}

	public BigDecimal getTaxedAmount() {
		return taxedAmount;
	}

	public void setTaxedAmount(BigDecimal taxedAmount) {
		this.taxedAmount = taxedAmount;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (orderToPartId == null ? 0 : orderToPartId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OrderToPart)) {
			return false;
		}
		final OrderToPart other = (OrderToPart) obj;
		if (orderToPartId == null) {
			if (other.orderToPartId != null) {
				return false;
			}
		} else if (!orderToPartId.equals(other.orderToPartId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "OrderToPart [orderToPartId=" + orderToPartId + ", partId=" + partId + ", orderId=" + orderId
				+ ", quantity=" + quantity + ", partPrice=" + partPrice + ", clientProductId=" + clientProductId
				+ ", onSaleInd=" + onSaleInd + ", refundedInd=" + refundedInd + ", taxExempt=" + taxExemptInd
				+ ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
