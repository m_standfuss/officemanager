package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "part", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findByPartNameLike_Part", query = "SELECT p FROM Part p WHERE p.partName LIKE :PART_NAME AND p.activeInd = true"),
		@NamedQuery(name = "findByLowInventory_Part", query = "SELECT p FROM Part p WHERE p.inventoryCount < p.inventoryThreshold AND p.inventoryCount > 0 AND p.activeInd = true"),
		@NamedQuery(name = "findByOutOfStock_Part", query = "SELECT p FROM Part p WHERE p.inventoryCount <= 0 AND p.activeInd = true"),
		@NamedQuery(name = "findByBarcode_Part", query = "SELECT p FROM Part p WHERE p.barcode = :BARCODE AND p.activeInd = true"),
		@NamedQuery(name = "findByLocation_Part", query = "SELECT p FROM Part p WHERE p.locationId = :LOCATION_ID AND p.activeInd = true"),
		@NamedQuery(name = "addInventory_Part", query = "UPDATE Part p SET p.inventoryCount = (p.inventoryCount + :INV_CNT) WHERE p.partId = :PART_ID"),
		@NamedQuery(name = "updateLocation_Part", query = "UPDATE Part p SET p.locationId = :NEW_LOC_ID WHERE p.locationId = :OLD_LOC_ID"),
		@NamedQuery(name = "setLocation_Part", query = "UPDATE Part p SET p.locationId = :LOCATION_ID WHERE p.partId IN (:PART_IDS)"),
		@NamedQuery(name = "clearLocation_Part", query = "UPDATE Part p SET p.locationId = null WHERE p.locationId = :LOCATION_ID"),
		@NamedQuery(name = "addAmountSold_Part", query = "UPDATE Part p SET p.amountSold = (p.amountSold + :AMT) WHERE p.partId = :PART_ID") })
public class Part implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 8271662910982305438L;

	/**
	 * Creates a new Part model with all the fields defaulted to their default
	 * values.
	 *
	 * @return The part model with fields defaulted.
	 */
	public static Part getDefaultPart() {
		Part part = new Part();
		part.partId = null;
		part.activeInd = true;
		part.amountSold = 0;
		part.barcode = null;
		part.basePrice = new BigDecimal(0);
		part.categoryTypeCd = 0L;
		part.description = "";
		part.inventoryCount = 0;
		part.inventoryThreshold = 0;
		part.locationId = 0L;
		part.manufacturerCd = 0L;
		part.manufPartId = "";
		part.partName = "";
		part.productTypeCd = 0L;
		part.updtDttm = new Date();
		part.updtPrsnlId = 0L;
		return part;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "part_id", unique = true, nullable = false)
	private Long partId;

	@Column(name = "part_name", nullable = false, length = 50)
	private String partName;

	@Column(name = "manuf_part_id", nullable = false, length = 50)
	private String manufPartId;

	@Column(name = "description", nullable = false, length = 80)
	private String description;

	@Column(name = "amount_sold", nullable = false)
	private int amountSold;

	@Column(name = "barcode", length = 25)
	private String barcode;

	@Column(name = "base_price", nullable = false, precision = 18, scale = 4)
	private BigDecimal basePrice;

	@Column(name = "product_type_cd")
	private Long productTypeCd;

	@Column(name = "category_type_cd")
	private Long categoryTypeCd;

	@Column(name = "manufacturer_cd")
	private Long manufacturerCd;

	@Column(name = "inventory_count", nullable = false)
	private int inventoryCount;

	@Column(name = "inventory_threshold", nullable = false)
	private int inventoryThreshold;

	@Column(name = "location_id")
	private Long locationId;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return partId;
	}

	public Long getPartId() {
		return partId;
	}

	public void setPartId(Long partId) {
		this.partId = partId;
	}

	public String getPartName() {
		return partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getManufPartId() {
		return manufPartId;
	}

	public void setManufPartId(String manufPartId) {
		this.manufPartId = manufPartId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getAmountSold() {
		return amountSold;
	}

	public void setAmountSold(int amountSold) {
		this.amountSold = amountSold;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public Long getProductTypeCd() {
		return productTypeCd;
	}

	public void setProductTypeCd(Long productTypeCd) {
		this.productTypeCd = productTypeCd;
	}

	public Long getCategoryTypeCd() {
		return categoryTypeCd;
	}

	public void setCategoryTypeCd(Long categoryTypeCd) {
		this.categoryTypeCd = categoryTypeCd;
	}

	public Long getManufacturerCd() {
		return manufacturerCd;
	}

	public void setManufacturerCd(Long manufacturerCd) {
		this.manufacturerCd = manufacturerCd;
	}

	public int getInventoryCount() {
		return inventoryCount;
	}

	public void setInventoryCount(int inventoryCount) {
		this.inventoryCount = inventoryCount;
	}

	public int getInventoryThreshold() {
		return inventoryThreshold;
	}

	public void setInventoryThreshold(int inventoryThreshold) {
		this.inventoryThreshold = inventoryThreshold;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (partId == null ? 0 : partId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Part)) {
			return false;
		}
		final Part other = (Part) obj;
		if (partId == null) {
			if (other.partId != null) {
				return false;
			}
		} else if (!partId.equals(other.partId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Part [partId=" + partId + ", partName=" + partName + ", manufPartId=" + manufPartId + ", description="
				+ description + ", amountSold=" + amountSold + ", barcode=" + barcode + ", basePrice=" + basePrice
				+ ", productTypeCd=" + productTypeCd + ", categoryTypeCd=" + categoryTypeCd + ", manufacturerCd="
				+ manufacturerCd + ", inventoryCount=" + inventoryCount + ", inventoryThreshold=" + inventoryThreshold
				+ ", locationId=" + locationId + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + "]";
	}
}
