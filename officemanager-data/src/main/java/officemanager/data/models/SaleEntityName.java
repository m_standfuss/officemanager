package officemanager.data.models;

public enum SaleEntityName {
	PART, FLAT_SERVICE
}
