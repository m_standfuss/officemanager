package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "location", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findActiveRoots_Location", query = "SELECT l FROM Location l WHERE l.parentLocationId IS NULL AND l.activeInd = true"),
		@NamedQuery(name = "findActiveChildren_Location", query = "SELECT l FROM Location l WHERE l.parentLocationId IN (:PARENT_LOC_IDS) AND l.activeInd = true"),
		@NamedQuery(name = "updateParentLocation_Location", query = "UPDATE Location l SET l.parentLocationId = :NEW_LOC_ID WHERE l.parentLocationId = :OLD_LOC_ID") })
public class Location implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 8826743160368544844L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "location_id", unique = true, nullable = false)
	private Long locationId;

	@Column(name = "display", nullable = false, length = 50)
	private String display;

	@Column(name = "abbr_display", nullable = false, length = 5)
	private String abbrDisplay;

	@Column(name = "location_type_cd")
	private Long locationTypeCd;

	@Column(name = "parent_location_id")
	private Long parentLocationId;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Location() {}

	@Override
	public Serializable getPrimaryKey() {
		return locationId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getAbbrDisplay() {
		return abbrDisplay;
	}

	public void setAbbrDisplay(String abbrDisplay) {
		this.abbrDisplay = abbrDisplay;
	}

	public Long getLocationTypeCd() {
		return locationTypeCd;
	}

	public void setLocationTypeCd(Long locationTypeCd) {
		this.locationTypeCd = locationTypeCd;
	}

	public Long getParentLocationId() {
		return parentLocationId;
	}

	public void setParentLocationId(Long parentLocationId) {
		this.parentLocationId = parentLocationId;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (locationId == null ? 0 : locationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Location)) {
			return false;
		}
		final Location other = (Location) obj;
		if (locationId == null) {
			if (other.locationId != null) {
				return false;
			}
		} else if (!locationId.equals(other.locationId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Location [locationId=" + locationId + ", display=" + display + ", abbrDisplay=" + abbrDisplay
				+ ", locationTypeCd=" + locationTypeCd + ", parentLocationId=" + parentLocationId + ", updtPrsnlId="
				+ updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
