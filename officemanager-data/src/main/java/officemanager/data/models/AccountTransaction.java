package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "account_transaction", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findActiveByAccountId_AccountTransaction", query = "SELECT at FROM AccountTransaction at WHERE "
				+ "accountId = :ACCOUNT_ID"),
		@NamedQuery(name = "findActiveByAccountIdAndDates_AccountTransaction", query = "SELECT at FROM AccountTransaction at WHERE "
				+ "accountId = :ACCOUNT_ID AND transactionDttm >= :STARTING_DATE AND transactionDttm <= :ENDING_DATE") })
public class AccountTransaction implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 5760786077491341350L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "account_transaction_id", unique = true, nullable = false)
	private Long accountTransactionId;

	@Column(name = "account_id", nullable = false)
	private long accountId;

	@Column(name = "payment_id", nullable = false)
	private long paymentId;

	@Column(name = "transaction_type_cd")
	private Long transactionTypeCd;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_dttm", length = 19)
	private Date transactionDttm;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public AccountTransaction() {}

	@Override
	public Serializable getPrimaryKey() {
		return accountTransactionId;
	}

	public Long getAccountTransactionId() {
		return accountTransactionId;
	}

	public void setAccountTransactionId(Long accountTransactionId) {
		this.accountTransactionId = accountTransactionId;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public Long getTransactionTypeCd() {
		return transactionTypeCd;
	}

	public void setTransactionTypeCd(Long transactionTypeCd) {
		this.transactionTypeCd = transactionTypeCd;
	}

	public Date getTransactionDttm() {
		return transactionDttm;
	}

	public void setTransactionDttm(Date transactionDttm) {
		this.transactionDttm = transactionDttm;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (accountTransactionId ^ accountTransactionId >>> 32);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AccountTransaction other = (AccountTransaction) obj;
		if (accountTransactionId != other.accountTransactionId) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AccountTransaction [accountTransactionId=" + accountTransactionId + ", accountId=" + accountId
				+ ", paymentId=" + paymentId + ", transactionTypeCd=" + transactionTypeCd + ", updtPrsnlId="
				+ updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}
}
