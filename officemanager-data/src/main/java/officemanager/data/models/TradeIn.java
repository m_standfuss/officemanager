package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "trade_in", catalog = "officemanager")
public class TradeIn implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 8870663773656031263L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "trade_in_id", unique = true, nullable = false)
	private Long tradeInId;

	@Column(name = "manufacturer", nullable = false, length = 50)
	private String manufacturer;

	@Column(name = "model", nullable = false, length = 50)
	private String model;

	@Column(name = "amount", nullable = false, precision = 18, scale = 4)
	private BigDecimal amount;

	@Column(name = "payment_id")
	private Long paymentId;

	@Column(name = "description", nullable = false, length = 80)
	private String description;

	@Column(name = "trade_in_type_cd")
	private Long tradeInTypeCd;

	@Column(name = "product_type_cd")
	private Long productTypeCd;

	@Column(name = "client_id")
	private Long clientId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt_tm", length = 19)
	private Date createdDtTm;

	@Column(name = "create_prsnl_id")
	private Long createPrsnlId;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public TradeIn() {}

	@Override
	public Serializable getPrimaryKey() {
		return tradeInId;
	}

	public Long getTradeInId() {
		return tradeInId;
	}

	public void setTradeInId(Long tradeInId) {
		this.tradeInId = tradeInId;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTradeInTypeCd() {
		return tradeInTypeCd;
	}

	public void setTradeInTypeCd(Long tradeInTypeCd) {
		this.tradeInTypeCd = tradeInTypeCd;
	}

	public Long getProductTypeCd() {
		return productTypeCd;
	}

	public void setProductTypeCd(Long productTypeCd) {
		this.productTypeCd = productTypeCd;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	public Long getCreatePrsnlId() {
		return createPrsnlId;
	}

	public void setCreatePrsnlId(Long createPrsnlId) {
		this.createPrsnlId = createPrsnlId;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (tradeInId == null ? 0 : tradeInId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof TradeIn)) {
			return false;
		}
		final TradeIn other = (TradeIn) obj;
		if (tradeInId == null) {
			if (other.tradeInId != null) {
				return false;
			}
		} else if (!tradeInId.equals(other.tradeInId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "TradeIn [tradeInId=" + tradeInId + ", manufacturer=" + manufacturer + ", model=" + model + ", amount="
				+ amount + ", paymentId=" + paymentId + ", description=" + description + ", tradeInTypeCd=" + tradeInTypeCd
				+ ", productTypeCd=" + productTypeCd + ", clientId=" + clientId + ", createdDtTm=" + createdDtTm
				+ ", createPrsnlId=" + createPrsnlId + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + "]";
	}
}
