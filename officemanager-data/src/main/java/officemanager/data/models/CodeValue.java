package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "code_value", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findByCodeSetAndCDFList_CodeValue", query = "SELECT cd FROM CodeValue cd WHERE "
				+ "cd.codeSet = :CODE_SET AND " + "cd.cdfMeaning IN (:CDF_MEANINGS)"),
		@NamedQuery(name = "findByCodeSetList_CodeValue", query = "SELECT cd FROM CodeValue cd WHERE "
				+ "cd.codeSet IN (:CODE_SETS)") })
public class CodeValue implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -1789647689757976522L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "code_value", unique = true, nullable = false)
	private Long codeValue;

	@Column(name = "code_set", nullable = false)
	private long codeSet;

	@Column(name = "display", nullable = false, length = 50)
	private String display;

	@Column(name = "cdf_meaning", nullable = false, length = 25)
	private String cdfMeaning;

	@Column(name = "collation_seq")
	private Short collationSeq;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public CodeValue() {}

	@Override
	public Serializable getPrimaryKey() {
		return codeValue;
	}

	public Long getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(Long codeValue) {
		this.codeValue = codeValue;
	}

	public long getCodeSet() {
		return codeSet;
	}

	public void setCodeSet(long codeSet) {
		this.codeSet = codeSet;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getCdfMeaning() {
		return cdfMeaning;
	}

	public void setCdfMeaning(String cdfMeaning) {
		this.cdfMeaning = cdfMeaning;
	}

	public Short getCollationSeq() {
		return collationSeq;
	}

	public void setCollationSeq(Short collationSeq) {
		this.collationSeq = collationSeq;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (codeValue == null ? 0 : codeValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CodeValue)) {
			return false;
		}
		final CodeValue other = (CodeValue) obj;
		if (codeValue == null) {
			if (other.codeValue != null) {
				return false;
			}
		} else if (!codeValue.equals(other.codeValue)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "CodeValue [codeValue=" + codeValue + ", codeSet=" + codeSet + ", display=" + display + ", cdfMeaning="
				+ cdfMeaning + ", collationSeq=" + collationSeq + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm="
				+ updtDttm + ", activeInd=" + activeInd + "]";
	}
}
