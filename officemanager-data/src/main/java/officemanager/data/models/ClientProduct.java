package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "client_product", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActiveProductsByClientId_ClientProduct", query = "SELECT cp FROM ClientProduct cp WHERE "
			+ "cp.clientId = :CLIENT_ID AND cp.activeInd = true") })
public class ClientProduct implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -8053210785515403680L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "client_product_id", unique = true, nullable = false)
	private Long clientProductId;

	@Column(name = "client_id", nullable = false)
	private long clientId;

	@Column(name = "product_manuf", nullable = false, length = 50)
	private String productManuf;

	@Column(name = "product_model", nullable = false, length = 50)
	private String productModel;

	@Column(name = "product_serial_num", nullable = false, length = 32)
	private String productSerialNum;

	@Column(name = "product_barcode", nullable = false, length = 32)
	private String productBarcode;

	@Column(name = "work_cnt", nullable = false)
	private int workCnt;

	@Column(name = "product_status_cd")
	private Long productStatusCd;

	@Column(name = "product_type_cd")
	private Long productTypeCd;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public ClientProduct() {}

	@Override
	public Serializable getPrimaryKey() {
		return clientProductId;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public String getProductManuf() {
		return productManuf;
	}

	public void setProductManuf(String productManuf) {
		this.productManuf = productManuf;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getProductSerialNum() {
		return productSerialNum;
	}

	public void setProductSerialNum(String productSerialNum) {
		this.productSerialNum = productSerialNum;
	}

	public String getProductBarcode() {
		return productBarcode;
	}

	public void setProductBarcode(String productBarcode) {
		this.productBarcode = productBarcode;
	}

	public int getWorkCnt() {
		return workCnt;
	}

	public void setWorkCnt(int workCnt) {
		this.workCnt = workCnt;
	}

	public Long getProductStatusCd() {
		return productStatusCd;
	}

	public void setProductStatusCd(Long productStatusCd) {
		this.productStatusCd = productStatusCd;
	}

	public Long getProductTypeCd() {
		return productTypeCd;
	}

	public void setProductTypeCd(Long productTypeCd) {
		this.productTypeCd = productTypeCd;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (clientProductId == null ? 0 : clientProductId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ClientProduct other = (ClientProduct) obj;
		if (clientProductId == null) {
			if (other.clientProductId != null) {
				return false;
			}
		} else if (!clientProductId.equals(other.clientProductId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ClientProduct [clientProductId=" + clientProductId + ", clientId=" + clientId + ", productManuf="
				+ productManuf + ", productModel=" + productModel + ", productVin=" + productSerialNum
				+ ", productBarcode="
				+ productBarcode + ", workCnt=" + workCnt + ", productStatusCd=" + productStatusCd + ", productTypeCd="
				+ productTypeCd + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd
				+ "]";
	}
}
