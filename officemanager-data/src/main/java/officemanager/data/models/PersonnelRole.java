package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "personnel_role", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActiveByPersonnelId_PersonnelRole", query = "SELECT pr FROM PersonnelRole pr, Role r WHERE "
			+ "pr.personnelId = :PRSNL_ID AND pr.beginEffectiveDttm <= :DATE AND "
			+ "(pr.endEffectiveDttm >= :DATE OR pr.endEffectiveDttm is null) AND " + "pr.roleId = r.roleId AND "
			+ "r.activeInd = true AND pr.activeInd = true") })
public class PersonnelRole implements Serializable, OfficeManagerEntity {

	private static final long serialVersionUID = 2611383152281059678L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "personnel_role_id", unique = true, nullable = false)
	private Long personnelRoleId;

	@Column(name = "personnel_id")
	private Long personnelId;

	@Column(name = "role_id")
	private Long roleId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "begin_effective_dttm", length = 19)
	private Date beginEffectiveDttm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_effective_dttm", length = 19)
	private Date endEffectiveDttm;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	@Override
	public Serializable getPrimaryKey() {
		return personnelRoleId;
	}

	public Long getPersonnelRoleId() {
		return personnelRoleId;
	}

	public void setPersonnelRoleId(Long personnelRoleId) {
		this.personnelRoleId = personnelRoleId;
	}

	public Long getPersonnelId() {
		return personnelId;
	}

	public void setPersonnelId(Long personnelId) {
		this.personnelId = personnelId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Date getBeginEffectiveDttm() {
		return beginEffectiveDttm;
	}

	public void setBeginEffectiveDttm(Date beginEffectiveDttm) {
		this.beginEffectiveDttm = beginEffectiveDttm;
	}

	public Date getEndEffectiveDttm() {
		return endEffectiveDttm;
	}

	public void setEndEffectiveDttm(Date endEffectiveDttm) {
		this.endEffectiveDttm = endEffectiveDttm;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}
}