package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "order_to_client_product", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findActiveProductsByOrderIds_OrderToClientProduct", query = "SELECT otcp FROM OrderToClientProduct otcp WHERE "
				+ "otcp.orderId IN (:ORDER_IDS) AND otcp.activeInd = true"),
		@NamedQuery(name = "inactivateProductsByOrderId_OrderToClientProduct", query = "UPDATE OrderToClientProduct otcp SET "
				+ "otcp.activeInd = false WHERE otcp.orderId = :ORDER_ID"),
		@NamedQuery(name = "inactivateProductsByOrderIdAndPrimaryKeyNotIn_OrderToClientProduct", query = "UPDATE OrderToClientProduct otcp SET "
				+ "otcp.activeInd = false WHERE otcp.orderId = :ORDER_ID AND otcp.orderToClientProductId NOT IN (:OTCP_IDS)") })
public class OrderToClientProduct implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 4416325514008190655L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "order_to_client_product_id", unique = true, nullable = false)
	private Long orderToClientProductId;

	@Column(name = "order_id", nullable = false)
	private long orderId;

	@Column(name = "client_product_id", nullable = false)
	private long clientProductId;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public OrderToClientProduct() {}

	@Override
	public Serializable getPrimaryKey() {
		return orderToClientProductId;
	}

	public Long getOrderToClientProductId() {
		return orderToClientProductId;
	}

	public void setOrderToClientProductId(Long orderToClientProductId) {
		this.orderToClientProductId = orderToClientProductId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (orderToClientProductId == null ? 0 : orderToClientProductId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OrderToClientProduct)) {
			return false;
		}
		final OrderToClientProduct other = (OrderToClientProduct) obj;
		if (orderToClientProductId == null) {
			if (other.orderToClientProductId != null) {
				return false;
			}
		} else if (!orderToClientProductId.equals(other.orderToClientProductId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "OrderToClientProduct [orderToClientProductId=" + orderToClientProductId + ", orderId=" + orderId
				+ ", clientProductId=" + clientProductId + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + "]";
	}
}
