package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "account", catalog = "officemanager")
@NamedQueries({
		@NamedQuery(name = "findActiveAccountByClientId_Account", query = "SELECT a FROM Account a WHERE "
				+ "a.activeInd = true AND a.clientId = :CLIENT_ID"),
		@NamedQuery(name = "updateAccountStatus_Account", query = "UPDATE Account a SET a.statusCd = :STATUS_CD WHERE a.accountId = :ACCOUNT_ID") })
public class Account implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = -9142218287928463438L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "account_id", unique = true, nullable = false)
	private Long accountId;

	@Column(name = "client_id", nullable = false)
	private Long clientId;

	@Column(name = "status_cd")
	private Long statusCd;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_statement_dt_tm", length = 19)
	private Date lastStatementDtTm;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Account() {}

	@Override
	public Serializable getPrimaryKey() {
		return accountId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(Long statusCd) {
		this.statusCd = statusCd;
	}

	public Date getLastStatementDtTm() {
		return lastStatementDtTm;
	}

	public void setLastStatementDtTm(Date lastStatementDtTm) {
		this.lastStatementDtTm = lastStatementDtTm;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (accountId == null ? 0 : accountId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Account other = (Account) obj;
		if (accountId == null) {
			if (other.accountId != null) {
				return false;
			}
		} else if (!accountId.equals(other.accountId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", statusCd=" + statusCd + ", lastStatementDtTm=" + lastStatementDtTm
				+ ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm + ", activeInd=" + activeInd + "]";
	}

}
