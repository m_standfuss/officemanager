package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "personnel", catalog = "officemanager", uniqueConstraints = @UniqueConstraint(columnNames = "username") )
@NamedQueries({
	@NamedQuery(name = "findByUsername_Personnel", query = "SELECT p FROM Personnel p WHERE p.username = :USERNAME AND p.activeInd = true"),
	@NamedQuery(name = "findByPersonIds_Personnel", query = "SELECT p FROM Personnel p WHERE p.personId IN ( :PERSON_IDS ) AND p.activeInd = true"),
	@NamedQuery(name = "updatePassword_Prsnl", query = "UPDATE Personnel p SET p.password = :PASSWORD, p.passwordLastSet = :DATE_SET, p.passwordNeedsReset = false WHERE p.personnelId = :PERSONNEL_ID") })
public class Personnel implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 6265522827376104752L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "personnel_id", unique = true, nullable = false)
	private Long personnelId;

	@Column(name = "person_id", nullable = false)
	private Long personId;

	@Column(name = "username", unique = true, nullable = false, length = 25)
	private String username;

	@Column(name = "password")
	private byte[] password;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_dttm", length = 19)
	private Date startDttm;

	@Column(name = "locked_out", nullable = false)
	private boolean lockedOut;

	@Column(name = "personnel_status_cd")
	private Long personnelStatusCd;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "terminate_dttm", length = 19)
	private Date terminateDttm;

	@Column(name = "w2_on_file", nullable = false)
	private boolean w2OnFile;

	@Column(name = "personnel_type_cd")
	private Long personnelTypeCd;

	@Column(name = "password_needs_reset", nullable = false)
	private boolean passwordNeedsReset;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "password_last_set", length = 19)
	private Date passwordLastSet;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Personnel() {}

	@Override
	public Serializable getPrimaryKey() {
		return personnelId;
	}

	public Long getPersonnelId() {
		return personnelId;
	}

	public void setPersonnelId(Long personnelId) {
		this.personnelId = personnelId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public byte[] getPassword() {
		return password;
	}

	public void setPassword(byte[] password) {
		this.password = password;
	}

	public Date getStartDttm() {
		return startDttm;
	}

	public void setStartDttm(Date startDttm) {
		this.startDttm = startDttm;
	}

	public boolean isLockedOut() {
		return lockedOut;
	}

	public void setLockedOut(boolean lockedOut) {
		this.lockedOut = lockedOut;
	}

	public Long getPersonnelStatusCd() {
		return personnelStatusCd;
	}

	public void setPersonnelStatusCd(Long personnelStatusCd) {
		this.personnelStatusCd = personnelStatusCd;
	}

	public Date getTerminateDttm() {
		return terminateDttm;
	}

	public void setTerminateDttm(Date terminateDttm) {
		this.terminateDttm = terminateDttm;
	}

	public boolean isW2OnFile() {
		return w2OnFile;
	}

	public void setW2OnFile(boolean w2OnFile) {
		this.w2OnFile = w2OnFile;
	}

	public Long getPersonnelTypeCd() {
		return personnelTypeCd;
	}

	public void setPersonnelTypeCd(Long personnelTypeCd) {
		this.personnelTypeCd = personnelTypeCd;
	}

	public boolean isPasswordNeedsReset() {
		return passwordNeedsReset;
	}

	public void setPasswordNeedsReset(boolean passwordNeedsReset) {
		this.passwordNeedsReset = passwordNeedsReset;
	}

	public Date getPasswordLastSet() {
		return passwordLastSet;
	}

	public void setPasswordLastSet(Date passwordLastSet) {
		this.passwordLastSet = passwordLastSet;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (personnelId ^ personnelId >>> 32);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Personnel)) {
			return false;
		}
		final Personnel other = (Personnel) obj;
		if (personnelId != other.personnelId) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Personnel [personnelId=" + personnelId + ", personId=" + personId + ", username=" + username
				+ ", password=" + Arrays.toString(password) + ", startDttm=" + startDttm + ", lockedOut=" + lockedOut
				+ ", personnelStatusCd=" + personnelStatusCd + ", terminateDttm=" + terminateDttm + ", w2OnFile="
				+ w2OnFile + ", personnelTypeCd=" + personnelTypeCd + ", passwordNeedsReset=" + passwordNeedsReset
				+ ", passwordLastSet=" + passwordLastSet + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + "]";
	}
}
