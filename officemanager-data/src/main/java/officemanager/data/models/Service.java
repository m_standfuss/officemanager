package officemanager.data.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "service", catalog = "officemanager")
@NamedQueries({
	@NamedQuery(name = "findActiveProductsByOrderIds_Service", query = "SELECT s FROM Service s WHERE "
				+ "s.orderId IN (:ORDER_IDS) AND s.activeInd = true"),
	@NamedQuery(name = "findActiveByOrderIdAndClientProductIds_Service", query = "SELECT s FROM Service s WHERE "
			+ "s.orderId = :ORDER_ID AND s.clientProductId IN ( :CLIENT_PRODUCT_IDS ) AND s.activeInd = true AND s.refundedInd = false"),
	@NamedQuery(name = "inactivateByOrderId_Service", query = "UPDATE Service s SET s.activeInd = false WHERE "
			+ "s.orderId = :ORDER_ID"),
	@NamedQuery(name = "inactivateByOrderIdAndPrimaryKeyNotIn_Service", query = "UPDATE Service s SET s.activeInd = false WHERE "
			+ "s.orderId = :ORDER_ID AND s.serviceId NOT IN (:SERVICE_IDS)") })
public class Service implements Serializable, OfficeManagerEntity {
	private static final long serialVersionUID = 8010409148576231092L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "service_id", unique = true, nullable = false)
	private Long serviceId;

	@Column(name = "service_name", nullable = false, length = 50)
	private String serviceName;

	@Column(name = "service_long_text_id")
	private Long serviceLongTextId;

	@Column(name = "order_id", nullable = false)
	private long orderId;

	@Column(name = "client_product_id")
	private Long clientProductId;

	@Column(name = "hours_qnt", nullable = false, precision = 18, scale = 4)
	private BigDecimal hoursQnt;

	@Column(name = "price_per_hour", nullable = false, precision = 18, scale = 4)
	private BigDecimal pricePerHour;

	@Column(name = "refunded_ind", nullable = false)
	private boolean refundedInd;

	@Column(name = "updt_prsnl_id")
	private Long updtPrsnlId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updt_dttm", length = 19)
	private Date updtDttm;

	@Column(name = "active_ind", nullable = false)
	private boolean activeInd;

	public Service() {}

	@Override
	public Serializable getPrimaryKey() {
		return serviceId;
	}

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Long getServiceLongTextId() {
		return serviceLongTextId;
	}

	public void setServiceLongTextId(Long serviceLongTextId) {
		this.serviceLongTextId = serviceLongTextId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public BigDecimal getHoursQnt() {
		return hoursQnt;
	}

	public void setHoursQnt(BigDecimal hoursQnt) {
		this.hoursQnt = hoursQnt;
	}

	public BigDecimal getPricePerHour() {
		return pricePerHour;
	}

	public void setPricePerHour(BigDecimal pricePerHour) {
		this.pricePerHour = pricePerHour;
	}

	public boolean isRefundedInd() {
		return refundedInd;
	}

	public void setRefundedInd(boolean refundedInd) {
		this.refundedInd = refundedInd;
	}

	public Long getUpdtPrsnlId() {
		return updtPrsnlId;
	}

	@Override
	public void setUpdtPrsnlId(Long updtPrsnlId) {
		this.updtPrsnlId = updtPrsnlId;
	}

	public Date getUpdtDttm() {
		return updtDttm;
	}

	@Override
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}

	public boolean isActiveInd() {
		return activeInd;
	}

	@Override
	public void setActiveInd(boolean activeInd) {
		this.activeInd = activeInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (serviceId == null ? 0 : serviceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Service)) {
			return false;
		}
		final Service other = (Service) obj;
		if (serviceId == null) {
			if (other.serviceId != null) {
				return false;
			}
		} else if (!serviceId.equals(other.serviceId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Service [serviceId=" + serviceId + ", serviceLongTextId=" + serviceLongTextId + ", orderId=" + orderId
				+ ", clientProductId=" + clientProductId + ", hoursQnt=" + hoursQnt + ", pricePerHour=" + pricePerHour
				+ ", refundedInd=" + refundedInd + ", updtPrsnlId=" + updtPrsnlId + ", updtDttm=" + updtDttm
				+ ", activeInd=" + activeInd + "]";
	}
}
