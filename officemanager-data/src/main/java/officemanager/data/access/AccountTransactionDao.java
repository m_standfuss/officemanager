package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.AccountTransaction;

public class AccountTransactionDao extends Dao<AccountTransaction> {

	private static final Logger logger = LogManager.getLogger(AccountTransactionDao.class);

	@Override
	public void persist(AccountTransaction entity) {
		logger.debug("Calling persist on the payment dao directly.");
		checkNotNull(entity);
		if (entity.getAccountTransactionId() == null) {
			logger.debug("A new transaction so adding transaction dttm.");
			entity.setTransactionDttm(new Date());
		}
		super.persist(entity);

	}

	/**
	 * Gets all active account transactions for the given account id regardless
	 * of date.
	 * 
	 * @param accountId
	 *            The account id of the account to get transactions for.
	 * @return The list of active transactions.
	 */
	public List<AccountTransaction> findActiveByAccountId(Long accountId) {
		logger.debug("findActiveByAccountId for accountId=" + accountId);
		checkNotNull(accountId);
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findActiveByAccountId_AccountTransaction")
					.setLong("ACCOUNT_ID", accountId);
			return getQueryResults(query);
		}
	}

	/**
	 * Gets all active account transactions for the given account id for the
	 * given time range.
	 * 
	 * @param accountId
	 *            The account id of the account to get transactions for.
	 * @param startDttm
	 *            The starting date of the time frame to get transactions for.
	 * @param endDttm
	 *            The ending date of the time frame to get transactions for.
	 * @return The list of transactions for the account between the given times.
	 */
	public List<AccountTransaction> findActiveByAccountIdAndDates(Long accountId, Date startDttm, Date endDttm) {
		logger.debug("findActiveByAccountIdAndDates for accountId=" + accountId + ", starting date=" + startDttm
				+ ", ending date=" + endDttm);
		checkNotNull(accountId);
		checkNotNull(startDttm);
		checkNotNull(endDttm);
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findActiveByAccountIdAndDates_AccountTransaction")
					.setLong("ACCOUNT_ID", accountId).setTimestamp("STARTING_DATE", startDttm)
					.setTimestamp("ENDING_DATE", endDttm);
			return getQueryResults(query);
		}
	}
}
