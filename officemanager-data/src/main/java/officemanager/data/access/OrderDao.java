package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.data.models.Order;

public class OrderDao extends Dao<Order> {

	private static final Logger logger = LogManager.getLogger(OrderDao.class);

	@Override
	public void persist(Order entity) {
		checkNotNull(entity);
		if (entity.getOrderId() == null) {
			entity.setCreatedDttm(new Date());
			entity.setCreatedPrsnlId(currentUser);
		}
		super.persist(entity);
	}

	public List<Order> findBySearchCriteria(Long orderId, Date begOpenedDttm, Date endOpenedDttm, Date begClosedDttm,
			Date endClosedDttm, Collection<Long> clientIdList, Collection<Long> orderStatusList, Long openedPrsnlId,
			Long closedPrsnlId) {
		logger.debug("findBySearchCriteria with begOpenedDttm=" + begOpenedDttm + ", endOpenedDttm=" + endOpenedDttm
				+ ", begClosedDttm=" + begClosedDttm + ", endClosedDttm=" + endClosedDttm + ", clientIdList="
				+ clientIdList + ", orderStatusList=" + orderStatusList + ", openedPrsnlId=" + openedPrsnlId
				+ ", closedPrsnlId=" + closedPrsnlId);

		if (clientIdList != null && clientIdList.isEmpty()) {
			logger.debug("Empty client id collection specified returning empty set.");
			return Lists.newArrayList();
		}
		if (orderStatusList != null && orderStatusList.isEmpty()) {
			logger.debug("Empty order status collection specified returning empty set.");
			return Lists.newArrayList();
		}

		final QueryBuilder builder = new QueryBuilder().SELECT("o").FROM("Order o").WHERE("o.activeInd = true");

		if (orderId != null) {
			builder.AND("o.orderId = :ORDER_ID").addParameter("ORDER_ID", orderId);
		}

		if (begOpenedDttm != null) {
			builder.AND("o.createdDttm >= :BEG_OPEN_DTTM").addParameter("BEG_OPEN_DTTM", begOpenedDttm);
		}

		if (endOpenedDttm != null) {
			builder.AND("o.createdDttm <= :END_OPEN_DTTM").addParameter("END_OPEN_DTTM", endOpenedDttm);
		}

		if (begClosedDttm != null) {
			builder.AND("o.closedDttm >= :BEG_CLOSED_DTTM").addParameter("BEG_CLOSED_DTTM", begClosedDttm);
		}

		if (endClosedDttm != null) {
			builder.AND("o.closedDttm <= :END_CLOSED_DTTM").addParameter("END_CLOSED_DTTM", endClosedDttm);
		}

		if (clientIdList != null) {
			builder.AND("o.clientId IN (:CLIENT_IDS)").addParameter("CLIENT_IDS", clientIdList);
		}

		if (orderStatusList != null) {
			builder.AND("o.orderStatusCd IN (:ORDER_STATUS_LIST)").addParameter("ORDER_STATUS_LIST", orderStatusList);
		}

		if (openedPrsnlId != null) {
			builder.AND("o.createdPrsnlId = :OPEN_PRSNL_ID").addParameter("OPEN_PRSNL_ID", openedPrsnlId);
		}

		if (closedPrsnlId != null) {
			builder.AND("o.closedPrsnlId = :CLOSED_PRSNL_ID").addParameter("CLOSED_PRSNL_ID", closedPrsnlId);
		}
		return runDynamicQuery(builder);
	}
}
