package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.Person;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class PersonDao extends Dao<Person> {
	private static final Logger logger = LogManager.getLogger(PersonDao.class);

	/**
	 * Gets the active person models that have the person ids.
	 * 
	 * @param personIds
	 *            The person ids to use to qualify on.
	 * @return The resulting active persons.
	 */
	public List<Person> findActiveByPersonIds(Collection<Long> personIds) {
		logger.debug("Starting findActiveByPersonIds with personIds=" + personIds);

		if (CollectionUtility.isNullOrEmpty(personIds)) {
			logger.debug("No person ids specified for querying returning an empty list.");
			return Lists.newArrayList();
		}

		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findActiveByPersonIds_Person")
					.setParameterList("PERSON_IDS", personIds);
			return getQueryResults(query);
		}
	}

	/**
	 * Queries the active Persons by their first and last names like the ones
	 * provided. A % wild-card will be added to the end of either name if used
	 * for searching. </br>
	 * </br>
	 * <b>*Note*</b> that if either name (first/last) is null or empty it is
	 * removed from the query, so if both are null/empty then an empty list will
	 * be returned.
	 * 
	 * @param nameFirst
	 *            The first name to query by.
	 * @param nameLast
	 *            The last name to query by.
	 * @return The list of Persons that meet the criteria.
	 */
	public List<Person> findActiveByNameFirstAndLastLike(String nameFirst, String nameLast) {
		logger.debug("findActiveByNameFirstAndLast with nameFirst=" + nameFirst + ", nameLast=" + nameLast);

		if (StringUtility.isNullOrWhiteSpace(nameFirst) && StringUtility.isNullOrWhiteSpace(nameLast)) {
			return Lists.newArrayList();
		}
		if (StringUtility.isNullOrWhiteSpace(nameFirst)) {
			return findActiveByNameLastLike(nameLast);
		}
		if (StringUtility.isNullOrWhiteSpace(nameLast)) {
			return findActiveByNameFirstLike(nameFirst);
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByBothNamesLike_Person")
					.setString("NAME_FIRST", nameFirst + '%').setString("NAME_LAST", nameLast + '%');
			return getQueryResults(query);
		}
	}

	/**
	 * Queries the active Persons by their first name like the one given. A %
	 * wild-card will be added to the end of the name.
	 * 
	 * @param nameFirst
	 *            The first name to query by.
	 * @return The list of Persons that meet the criteria.
	 * @throws NullPointerException
	 *             if nameFirst is null.
	 */
	public List<Person> findActiveByNameFirstLike(String nameFirst) {
		logger.debug("findActiveByNameFirstLike with nameFirst=" + nameFirst);
		checkNotNull(nameFirst);
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByNameFirstLike_Person")
					.setString("NAME_FIRST", nameFirst + '%');
			return getQueryResults(query);
		}
	}

	/**
	 * Queries the active Persons by their last name like the one given. A %
	 * wild-card will be added to the end of the name.
	 * 
	 * @param nameLast
	 *            The last name to query by.
	 * @return The list of Persons that meet the criteria.
	 * @throws NullPointerException
	 *             if nameLast is null.
	 */
	public List<Person> findActiveByNameLastLike(String nameLast) {
		logger.debug("findActiveByNameLastLike with nameLast=" + nameLast);
		checkNotNull(nameLast);
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByNameLastLike_Person")
					.setString("NAME_LAST", nameLast + '%');
			return getQueryResults(query);
		}
	}
}
