package officemanager.data.access;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.Permission;

public class PermissionDao extends Dao<Permission> {
	private static final Logger logger = LogManager.getLogger(PermissionDao.class);

	public List<Permission> findActive() {
		logger.debug("findActive.");
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActive_Permission");
			return getQueryResults(query);
		}
	}
}
