package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.Client;

public class ClientDao extends Dao<Client> {

	private static final Logger logger = LogManager.getLogger(ClientDao.class);

	@Override
	public void persist(Client entity) {
		checkNotNull(entity);

		if (entity.getClientId() == null) {
			entity.setJoinedDttm(new Date());
		}

		super.persist(entity);
	}

	/**
	 * Queries for all active clients (activeInd = true). </br>
	 * </br>
	 * <b>*Note*</b> This query has the potential to have a large results set.
	 * Should only be used whenever there is no other option of qualifying on
	 * the Client model.
	 * 
	 * @return The list of active clients.
	 */
	public List<Client> findActiveClients() {
		logger.debug("Starting findActiveClients");
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActive_Client");
			return getQueryResults(query);
		}
	}

	/**
	 * Gets the active Clients that have a person id associated to them in the
	 * ids given.
	 * 
	 * @param personIds
	 *            The person ids to query by.
	 * @return The Clients that meet the criteria.
	 */
	public List<Client> findActiveByPersonIds(List<Long> personIds) {
		logger.debug("findActiveByPersonIds with personIds=" + personIds);
		checkNotNull(personIds);
		if (personIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByPersonIds_Client")
					.setParameterList("PERSON_IDS", personIds);
			return getQueryResults(query);
		}
	}
}
