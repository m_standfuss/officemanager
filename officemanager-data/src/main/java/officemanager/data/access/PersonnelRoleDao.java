package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.PersonnelRole;

public class PersonnelRoleDao extends Dao<PersonnelRole> {

	private static final Logger logger = LogManager.getLogger(PersonnelRoleDao.class);

	public List<PersonnelRole> getActiveByPersonnelId(Long personnelId, Date activeDate) {
		logger.debug(
				"Starting getActiveByPersonnelId for personnelId=" + personnelId + " and activeDate=" + activeDate);
		checkNotNull(personnelId);
		checkNotNull(activeDate);

		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findActiveByPersonnelId_PersonnelRole")
					.setLong("PRSNL_ID", personnelId).setTimestamp("DATE", activeDate);
			return getQueryResults(query);
		}
	}
}
