package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

class CloseableSession implements AutoCloseable {

	private static final Logger logger = LogManager.getLogger(CloseableSession.class);

	private final Session session;
	private final boolean manuallyClose;

	CloseableSession(Session session, boolean manuallyClose) {
		checkNotNull(session);
		this.session = session;
		this.manuallyClose = manuallyClose;
	}

	CloseableSession(Session session) {
		this(session, false);
	}

	CloseableTransaction beginTransaction() {
		return new CloseableTransaction(session.beginTransaction(), manuallyClose);
	}

	Session getSession() {
		return session;
	}

	void manuallyClose() {
		_close();
	}

	@Override
	public void close() {
		if (manuallyClose) {
			logger.debug("Manually closeable session not closing.");
			return;
		}
		_close();
	}

	private void _close() {
		try {
			session.close();
		} catch (Exception e) {
			logger.error("An error occured while attempting to close the session.", e);
		}
	}
}
