package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.OrderToPart;

public class OrderToPartDao extends Dao<OrderToPart> {

	private static final Logger logger = LogManager.getLogger(OrderToPartDao.class);

	public List<OrderToPart> findActiveByOrderIds(Collection<Long> orderIds) {
		logger.debug("findActiveByOrderIds for orderIds = " + orderIds + ".");
		checkNotNull(orderIds);
		if (orderIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveProductsByOrderIds_OrderToPart")
					.setParameterList("ORDER_IDS", orderIds);
			return getQueryResults(query);
		}
	}

	public int inactivateByOrderId(Long orderId) {
		logger.debug("inactivateByOrderId for orderId=" + orderId);
		checkNotNull(orderId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				Query query = session.getSession().getNamedQuery("inactivateByOrderId_OrderToPart").setLong("ORDER_ID",
						orderId);
				return query.executeUpdate();
			}
		}
	}

	public int inactivateByOrderIdAndPrimaryKeysNotIn(Long orderId, List<Long> orderToPartIds) {
		logger.debug("inactivateByOrderId for orderId=" + orderId + ", and orderToPartIds=" + orderToPartIds);
		checkNotNull(orderId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				Query query = session.getSession().getNamedQuery("inactivateByOrderIdAndPrimaryKeysNotIn_OrderToPart")
						.setLong("ORDER_ID", orderId).setParameterList("ORDER_TO_PART_IDS", orderToPartIds);
				return query.executeUpdate();
			}
		}
	}
}
