package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Transaction;

class CloseableTransaction implements AutoCloseable {

	private static final Logger logger = LogManager.getLogger(CloseableTransaction.class);

	private final Transaction transaction;
	private final boolean manuallyClose;

	public CloseableTransaction(Transaction transaction, boolean manuallyClose) {
		checkNotNull(transaction);
		this.transaction = transaction;
		this.manuallyClose = manuallyClose;
	}

	public CloseableTransaction(Transaction transaction) {
		this(transaction, false);
	}

	Transaction getTransaction() {
		return transaction;
	}

	void manuallyClose() {
		_close();
	}

	@Override
	public void close() {
		if (manuallyClose) {
			logger.debug("Manually closed transaction, not closing.");
			return;
		}
		_close();
	}

	private void _close() {
		try {
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			logger.error(
					"An error occured when attempting to commit changes to the database, transaction will be rolled back.",
					e);
		}
	}
}
