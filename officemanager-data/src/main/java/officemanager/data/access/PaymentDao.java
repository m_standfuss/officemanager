package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.Payment;

public class PaymentDao extends Dao<Payment> {
	private static final Logger logger = LogManager.getLogger(PaymentDao.class);

	@Override
	public void persist(Payment entity) {
		logger.debug("Calling persist on the payment dao directly.");
		checkNotNull(entity);
		if (entity.getPaymentId() == null) {
			logger.debug("A new payment so adding created dttm and created prsnl ids.");
			entity.setCreatedDttm(new Date());
			entity.setCreatedPrsnlId(currentUser);
		}
		super.persist(entity);
	}

	public List<Payment> findActiveByDate(Date startDttm, Date endDttm) {
		logger.debug("findActiveByDates with startDttm=" + startDttm + ", endDttm=" + endDttm);
		if (startDttm == null || endDttm == null) {
			throw new IllegalArgumentException("The dates cannot be null.");
		}
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findActiveByDates_Payment")
					.setTimestamp("START_DTTM", startDttm).setTimestamp("END_DTTM", endDttm);
			return getQueryResults(query);
		}
	}

}
