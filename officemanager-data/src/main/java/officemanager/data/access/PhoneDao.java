package officemanager.data.access;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.Phone;

public class PhoneDao extends Dao<Phone> {

	private static Logger logger = LogManager.getLogger(PhoneDao.class);

	public List<Phone> findByPhoneNumber(String phoneNumber) {
		logger.debug("findByPhoneNumber for phoneNumber = " + phoneNumber);
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findByPhoneNumber_Phone").setString("PHONE_NUMBER",
					phoneNumber);
			return getQueryResults(query);
		}
	}

	public List<Phone> findActivePhones(Long personId) {
		return findActivePhones(Lists.newArrayList(personId));
	}

	public List<Phone> findActivePhones(Collection<Long> personIds) {
		logger.debug("Starting findActivePhones for personIds=" + personIds);
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByPersonIds_Phone")
					.setParameterList("PERSON_IDS", personIds);
			return getQueryResults(query);
		}
	}

	/**
	 * Finds a single primary phone number for a person id. If multiples are
	 * found a warning message is logged and the first one is returned.
	 * 
	 * @param personId
	 *            The person id to find the primary phone for.
	 * @return The phone or null if none is found.
	 */
	public List<Phone> findPrimaryPhone(Long personId) {
		return findPrimaryPhone(Lists.newArrayList(personId));
	}

	public List<Phone> findPrimaryPhone(Collection<Long> personIdList) {
		logger.debug("findPrimaryPhone for personIdList = " + personIdList);
		if (personIdList == null) {
			throw new IllegalArgumentException("The personIdList cannot be null.");
		}
		if (personIdList.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActivePrimaryByPersonIds_Phone")
					.setParameterList("PERSON_IDS", personIdList);
			return getQueryResults(query);
		}
	}

	/**
	 * Updates all the person's phone numbers to not be the primary phone except
	 * the one specified.
	 * 
	 * @param phoneId
	 *            The new primary phone number.
	 * @param personId
	 *            The person id.
	 */
	public void updatePrimaryPhoneForPerson(Long phoneId, Long personId) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				String update = "UPDATE Phone p SET p.primary_phone_ind = (p.phone_id = :PRIMARY_PHONE_ID) WHERE "
						+ "p.person_id = :PERSON_ID";

				session.getSession().createSQLQuery(update).setLong("PRIMARY_PHONE_ID", phoneId)
				.setLong("PERSON_ID", personId).executeUpdate();
			}
		}
	}

	public void disablePrimaryPhone(Long personId) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("disablePrimaryPhone_Phone").setLong("PERSON_ID", personId)
				.executeUpdate();
			}
		}
	}

	public void disablePhonesForPerson(Long personId, Collection<Long> activePhoneIds) {
		logger.debug(
				"Starting disablePhonesForPerson with personId=" + personId + ", and activePhoneIds=" + activePhoneIds);
		checkNotNull(personId);
		checkNotNull(activePhoneIds);
		checkArgument(activePhoneIds.size() > 0);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("disablePhonesForPersonWithActivePhones_Phone")
						.setLong("PERSON_ID", personId).setParameterList("PHONE_IDS", activePhoneIds).executeUpdate();
			}
		}
	}

	public void disablePhonesForPerson(Long personId) {
		logger.debug("Starting disablePhonesForPerson with personId=" + personId);
		checkNotNull(personId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("disablePhonesForPerson_Phone").setLong("PERSON_ID", personId)
						.executeUpdate();
			}
		}
	}
}
