package officemanager.data.access;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.Service;

public class ServiceDao extends Dao<Service> {

	private static final Logger logger = LogManager.getLogger(ServiceDao.class);

	public List<Service> findActiveByOrderIds(Collection<Long> orderIds) {
		logger.debug("findActiveByOrderIds for orderIds = " + orderIds + ".");
		checkNotNull(orderIds);
		if (orderIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveProductsByOrderIds_Service")
					.setParameterList("ORDER_IDS", orderIds);
			return getQueryResults(query);
		}
	}

	public List<Service> findActiveByOrderIdAndClientProductIds(Long orderId, List<Long> clientProductIds) {
		logger.debug("findActiveByOrderIdAndClientProductIds for orderId = " + orderId + ", and clientProductIds="
				+ clientProductIds);
		checkNotNull(orderId);
		checkNotNull(clientProductIds);
		checkArgument(!clientProductIds.contains(null));
		if (clientProductIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByOrderIdAndClientProductIds_Service")
					.setLong("ORDER_ID", orderId).setParameterList("CLIENT_PRODUCT_IDS", clientProductIds);
			return getQueryResults(query);
		}
	}

	public int inactivateByOrderId(Long orderId) {
		logger.debug("inactivateByOrderId for orderId=" + orderId);
		checkNotNull(orderId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				Query query = session.getSession().getNamedQuery("inactivateByOrderId_Service").setLong("ORDER_ID",
						orderId);
				return query.executeUpdate();
			}
		}
	}

	public int inactivateByOrderIdAndPrimaryKeyNotIn(Long orderId, List<Long> serviceIds) {
		logger.debug("inactivateByOrderIdAndPrimaryKeyNotIn for orderId=" + orderId + ", and serviceIds=" + serviceIds);
		checkNotNull(orderId, serviceIds);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				Query query = session.getSession().getNamedQuery("inactivateByOrderIdAndPrimaryKeyNotIn_Service")
						.setLong("ORDER_ID", orderId).setParameterList("SERVICE_IDS", serviceIds);
				return query.executeUpdate();
			}
		}
	}
}
