package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.Location;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class LocationDao extends Dao<Location> {

	private static final Logger logger = LogManager.getLogger(LocationDao.class);

	public List<Location> findActiveRootLocations() {
		logger.debug("Starting findActiveRootLocations.");
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveRoots_Location");
			return getQueryResults(query);
		}
	}

	public List<Location> findActiveChildrenLocations(Long parentLocationId) {
		logger.debug("Starting findActiveChildrenLocations.");
		checkNotNull(parentLocationId);
		return findActiveChildrenLocations(Lists.newArrayList(parentLocationId));
	}

	public List<Location> findActiveChildrenLocations(List<Long> parentLocationIds) {
		logger.debug("Starting findActiveChildrenLocations.");
		checkNotNull(parentLocationIds);
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveChildren_Location")
					.setParameterList("PARENT_LOC_IDS", parentLocationIds);
			return getQueryResults(query);
		}
	}

	public List<Location> searchLocations(String display, String abbrDisplay, List<Long> locationTypeCds) {
		logger.debug("Starting searchLocations with display=" + display + ", abbrDisplay=" + abbrDisplay
				+ ", and locationTypeCds=" + locationTypeCds);
		QueryBuilder qb = new QueryBuilder().SELECT("l").FROM("Location l").WHERE("l.activeInd = true");
		if (!StringUtility.isNullOrWhiteSpace(display)) {
			qb.AND("l.display LIKE :DISPLAY").addParameter("DISPLAY", display + '%');
		}
		if (!StringUtility.isNullOrWhiteSpace(abbrDisplay)) {
			qb.AND("l.abbrDisplay LIKE :ABBR_DISPLAY").addParameter("ABBR_DISPLAY", abbrDisplay + '%');
		}
		if (!CollectionUtility.isNullOrEmpty(locationTypeCds)) {
			qb.AND("l.locationTypeCd IN (:LOCATION_TYPES)").addParameter("LOCATION_TYPES", locationTypeCds);
		}
		return runDynamicQuery(qb);
	}

	public void updateParentLocation(Long oldLocation, Long newLocation) {
		logger.debug("updateParentLocation for oldLocation=" + oldLocation + " and newLocation=" + newLocation);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("updateParentLocation_Location").setLong("NEW_LOC_ID", newLocation)
						.setLong("OLD_LOC_ID", oldLocation).executeUpdate();
			}
		}
	}
}
