package officemanager.data.access;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.FlatRateService;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class FlatRateServiceDao extends Dao<FlatRateService> {

	private static final Logger logger = LogManager.getLogger(FlatRateServiceDao.class);

	public void addAmountSold(Long flatRateServiceId, int amountSold) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("addAmountSold_FlatRateService")
						.setLong("FLAT_RATE_SERVICE_ID", flatRateServiceId).setInteger("AMT", amountSold)
						.executeUpdate();
			}
		}
	}

	public List<FlatRateService> findByDescriptionLike(String description) {
		logger.debug("findByPartNameLike for findByDescriptionLike = '" + description + "'.");
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findByDescriptionLike_FlatRateService")
					.setString("SERVICE_NAME", description + '%');
			return getQueryResults(query);
		}
	}

	/**
	 * Searches for {@link FlatRateService} models based on the searching
	 * criteria provided. Will only search for services with active indicator
	 * set to true. <b>Note:</b> If all searching criteria is passed in as
	 * null/empty then this will perform a search only based on active ind being
	 * set to true and will likely result in a huge result set.
	 * 
	 * @param serviceDescription
	 *            The services short description text to search like, if null or
	 *            empty will omit from the search.
	 * @param serviceCategoryCds
	 *            The service categories to include in the search, if null or
	 *            empty will omit from the search.
	 * @param productTypeCds
	 *            The product types to include in the search, if null or empty
	 *            will omit from the search.
	 * @return The list of flat rate service models that meet the searching
	 *         criteria.
	 */
	public List<FlatRateService> findBySearchCriteria(String serviceName, List<Long> serviceCategoryCds,
			List<Long> productTypeCds) {
		logger.debug("findBySearchCriteria with serviceName=" + serviceName + ", serviceCategoryCds="
				+ serviceCategoryCds + ", productTypeCds=" + productTypeCds);

		QueryBuilder builder = new QueryBuilder().SELECT("frs").FROM("FlatRateService frs")
				.WHERE("frs.activeInd = true");

		if (!StringUtility.isNullOrWhiteSpace(serviceName)) {
			builder.AND("frs.serviceName LIKE :SERVICE_NAME").addParameter("SERVICE_NAME", serviceName + '%');
		}
		if (!CollectionUtility.isNullOrEmpty(serviceCategoryCds)) {
			builder.AND("frs.categoryTypeCd IN (:CATEGORY_TYPE_CDS)").addParameter("CATEGORY_TYPE_CDS",
					serviceCategoryCds);
		}
		if (!CollectionUtility.isNullOrEmpty(productTypeCds)) {
			builder.AND("frs.productTypeCd IN (:PRODUCT_TYPE_CDS)").addParameter("PRODUCT_TYPE_CDS", productTypeCds);
		}
		return runDynamicQuery(builder);
	}

}
