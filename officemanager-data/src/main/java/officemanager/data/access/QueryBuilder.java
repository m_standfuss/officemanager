package officemanager.data.access;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import officemanager.utility.StringUtility;
import officemanager.utility.Tuple;

class QueryBuilder {
	private final List<String> selectFields;
	private final List<String> tables;
	private StringBuilder whereClause;
	private final List<Tuple<String, Object>> parameters;

	/**
	 * Constructor that will copy the contents of the other DynamicQueryBuilder
	 * without making any references to the other objects.
	 * 
	 * @param toCopy
	 *            The DynamicQueryBuilder to copy.
	 * @throws NullPointerException
	 *             If toCopy is null.
	 */
	QueryBuilder(QueryBuilder toCopy) {
		if (toCopy == null) {
			throw new NullPointerException("toCopy cannot be null.");
		}
		selectFields = toCopy.copySelectFields();
		tables = toCopy.copyTables();
		whereClause = toCopy.copyWhereClause();
		parameters = toCopy.copyParameters();
	}

	/**
	 * Constructor
	 */
	QueryBuilder() {
		selectFields = new ArrayList<String>();
		tables = new ArrayList<String>();
		whereClause = new StringBuilder();
		parameters = new ArrayList<Tuple<String, Object>>();
	}

	/**
	 * Builder method to add to the select fields. Only adds if the field is not
	 * null or empty.
	 * 
	 * @param field
	 *            to add to the select of the query.
	 * @return the same DynamicQueryBuilder
	 * @throws NullPointerException
	 *             if field is null.
	 * @throws IllegalArgumentException
	 *             if field is empty.
	 */
	QueryBuilder SELECT(String field) {
		checkString(field, "field");
		selectFields.add(field);
		return this;
	}

	/**
	 * Builder method to add a list of fields to the select fields. Only adds if
	 * the list is not null.
	 * 
	 * @param fieldList
	 *            the list to add to the select fields.
	 * @return the same DynamicQueryBuilder
	 * @throws NullPointerException
	 *             if fieldList is null, or contains a null value.
	 * @throws IllegalArgumentException
	 *             if fieldList contains an empty value.
	 */
	QueryBuilder SELECT(List<String> fieldList) {
		checkList(fieldList, "fieldList");
		for (final String field : fieldList) {
			SELECT(field);
		}
		return this;
	}

	/**
	 * Builder method to add to the tables to select from. Only adds if the
	 * table is not null.
	 * 
	 * @param table
	 *            to add to the tables to select from.
	 * @return the same DynamicQueryBuilder
	 * @throws NullPointerException
	 *             if table is null.
	 * @throws IllegalArgumentException
	 *             if table is empty.
	 */
	QueryBuilder FROM(String table) {
		checkString(table, "table");
		tables.add(table);
		return this;
	}

	/**
	 * Builder method to add a list of tables to select from. Only adds if the
	 * list is not null.
	 * 
	 * @param tables
	 *            the list of tables to select from.
	 * @return the same DynamicQueryBuilder
	 * @throws NullPointerException
	 *             if tables is null, or contains a null value.
	 * @throws IllegalArgumentException
	 *             if tables contains an empty value.
	 */
	QueryBuilder FROM(List<String> tables) {
		checkList(tables, "tables");
		for (final String table : tables) {
			FROM(table);
		}
		return this;
	}

	/**
	 * Builder method to set the initial where clause. Will overwrite any
	 * existing where clause set, if wanting to append to the current where
	 * clause use the AND/OR functions.
	 * 
	 * @param whereClause
	 *            initial base where clause
	 * @return the same DynamicQueryBuilder
	 * @throws NullPointerException
	 *             if whereClause is null.
	 * @throws IllegalArgumentException
	 *             if whereClause is empty.
	 */
	QueryBuilder WHERE(String whereClause) {
		checkString(whereClause, "whereClause");
		this.whereClause = new StringBuilder();
		this.whereClause.append(whereClause);
		return this;
	}

	/**
	 * Builder method to append onto the current where clause by joining with an
	 * AND statement. Only appends if the where clause has already been set.
	 * 
	 * @param whereClause
	 *            to join to the current where clause
	 * @return the same DynamicQueryBuilder
	 * @throws NullPointerException
	 *             if whereClause is null.
	 * @throws IllegalArgumentException
	 *             if whereClause is empty.
	 * @throws IllegalStateException
	 *             if the base where clause is not set.
	 */
	QueryBuilder AND(String whereClause) {
		checkString(whereClause, "whereClause");
		checkWhereClause();
		this.whereClause.append(" AND " + whereClause);
		return this;
	}

	/**
	 * Builder method to append onto the current where clause by joining with an
	 * OR statement. The or's will be placed between each of the supplied where
	 * clauses then added with an and to the entire where clause. Only appends
	 * if the where clause has already been set.
	 * 
	 * @param clauses
	 *            to join to the current where clause
	 * @return the same DynamicQueryBuilder
	 * @throws NullPointerException
	 *             if whereClause is null.
	 * @throws IllegalArgumentException
	 *             if whereClause is empty.
	 * @throws IllegalStateException
	 *             if the base where clause is not set.
	 */
	QueryBuilder OR(Collection<String> clauses) {
		checkStrings(clauses, "whereClause");
		checkWhereClause();
		StringBuilder orClause = new StringBuilder("(");
		String OR = "";
		for (String where : clauses) {
			orClause.append(OR).append(where);
			OR = " OR ";
		}
		orClause.append(")");
		return AND(orClause.toString());
	}

	/**
	 * Returns the current state of the class as a SQL query.
	 * 
	 * @return The SQL query.
	 * @throws IllegalStateException
	 *             If a table has not been set to select from.
	 */
	String getQueryString() {
		if (tables.size() < 1) {
			throw new IllegalStateException("Must speficy at least one table to select from.");
		}
		String query = "SELECT ";
		if (selectFields.size() > 0) {
			query += StringUtility.joinStringsByComma(selectFields);
		} else {
			query += "*";
		}
		query += " FROM " + StringUtility.joinStringsByComma(tables);
		if (whereClause.length() > 0) {
			query += " WHERE " + whereClause.toString();
		}
		return query;
	}

	/**
	 * Adds a parameter to the query for later retrieval.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param value
	 *            The value of the parameter.
	 */
	void addParameter(String name, Object value) {
		checkString(name, "name");
		parameters.add(new Tuple<String, Object>(name, value));
	}

	/**
	 * Converts the state of this QueryBuilder into a hibernate Query.
	 * 
	 * @param session
	 *            The session to create a query in.
	 * @return The query.
	 * @throws IllegalArgumentException
	 *             If the session is null.
	 */
	@SuppressWarnings("rawtypes")
	Query getQuery(Session session) {
		if (session == null) {
			throw new IllegalArgumentException("The session cannot be null.");
		}
		final Query q = session.createQuery(getQueryString());
		for (final Tuple<String, Object> param : parameters) {
			if (param.item2 instanceof Collection) {
				q.setParameterList(param.item1, (Collection) param.item2);
			} else if (param.item2 instanceof Date) {
				q.setTimestamp(param.item1, (Date) param.item2);
			} else {
				q.setParameter(param.item1, param.item2);
			}
		}
		return q;
	}

	/**
	 * Returns the getQueryString method.
	 */
	@Override
	public String toString() {
		return getQueryString();
	}

	// For making a deep copy of the current state of the query.
	private List<String> copySelectFields() {
		return new ArrayList<String>(selectFields);
	}

	private List<String> copyTables() {
		return new ArrayList<String>(tables);
	}

	private StringBuilder copyWhereClause() {
		return new StringBuilder(whereClause);
	}

	private List<Tuple<String, Object>> copyParameters() {
		return new ArrayList<Tuple<String, Object>>(parameters);
	}

	private void checkStrings(Collection<String> values, String name) {
		if (values == null) {
			throw new NullPointerException(name + " cannot be null.");
		}
		if (values.size() == 0) {
			throw new IllegalArgumentException(name + " must have values.");
		}
		for (String value : values) {
			checkString(value, name);
		}
	}

	private void checkString(String value, String name) {
		if (value == null) {
			throw new NullPointerException(name + " cannot be null.");
		}
		if (value.trim().isEmpty()) {
			throw new IllegalArgumentException(name + " cannot be blank.");
		}
	}

	private void checkList(List<String> value, String name) {
		if (value == null) {
			throw new NullPointerException(name + " cannot be null.");
		}
		for (final String s : value) {
			if (s == null) {
				throw new NullPointerException(name + " cannot contain a null value.");
			}
			if (s.trim().isEmpty()) {
				throw new IllegalArgumentException(name + " cannot contain an empty value.");
			}
		}
	}

	private void checkWhereClause() {
		if (whereClause.length() < 1) {
			throw new IllegalStateException("The where clause must be set prior to appending to it.");
		}
	}
}
