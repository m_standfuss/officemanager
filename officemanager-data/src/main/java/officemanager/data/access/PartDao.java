package officemanager.data.access;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.Part;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class PartDao extends Dao<Part> {

	private static final Logger logger = LogManager.getLogger(PartDao.class);

	public List<Part> findByPartNameLike(String partName) {
		logger.debug("findByPartNameLike for partName = '" + partName + "'.");
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findByPartNameLike_Part").setString("PART_NAME",
					partName + '%');
			return getQueryResults(query);
		}
	}

	public List<Part> findLowInventory() {
		logger.debug("findLowInventory.");
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findByLowInventory_Part");
			return getQueryResults(query);
		}
	}

	public List<Part> findOutOfStock() {
		logger.debug("findLowInventory.");
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findByOutOfStock_Part");
			return getQueryResults(query);
		}
	}

	public List<Part> findByBarcode(String barcode) {
		logger.debug("findByBarcode for barcode=" + barcode);
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findByBarcode_Part").setString("BARCODE", barcode);
			return getQueryResults(query);
		}
	}

	public List<Part> findByLocation(Long locationId) {
		logger.debug("findByLocation for locationId=" + locationId);
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findByLocation_Part").setLong("LOCATION_ID", locationId);
			return getQueryResults(query);
		}
	}

	public void addInventory(Long partId, int inventoryToAdd) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("addInventory_Part").setLong("PART_ID", partId)
						.setInteger("INV_CNT", inventoryToAdd).executeUpdate();
			}
		}
	}

	public void updateLocation(Long oldLocation, Long newLocation) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("updateLocation_Part").setLong("NEW_LOC_ID", newLocation)
						.setLong("OLD_LOC_ID", oldLocation).executeUpdate();
			}
		}
	}

	public void setLocation(Long locationId, Collection<Long> partIds) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("setLocation_Part").setLong("LOCATION_ID", locationId)
						.setParameterList("PART_IDS", partIds).executeUpdate();
			}
		}
	}

	public void clearLocation(Long locationId) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("clearLocation_Part").setLong("LOCATION_ID", locationId)
						.executeUpdate();
			}
		}
	}
	
	public void addAmountSold(Long partId, int amountSold) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("addAmountSold_Part").setLong("PART_ID", partId)
						.setInteger("AMT", amountSold).executeUpdate();
			}
		}
	}

	public List<Part> findBySearchCriteria(String partName, String partDescription, List<Long> partCategoryCds,
			List<Long> partManufacturerCds, List<Long> productTypeCds, boolean onlyInStock, String partManufId) {
		logger.debug("findBySearchCriteria with partName=" + partName + ", partDescription=" + partDescription
				+ ", partCategoryCds=" + partCategoryCds + ", partManufacturerCds=" + partManufacturerCds
				+ ", productTypeCds=" + productTypeCds + ", onlyInStock=" + onlyInStock + ", partManufId="
				+ partManufId);

		final QueryBuilder builder = new QueryBuilder().SELECT("p").FROM("Part p").WHERE("p.activeInd = true");

		if (!StringUtility.isNullOrWhiteSpace(partName)) {
			builder.AND("p.partName LIKE :PART_NAME").addParameter("PART_NAME", partName + '%');
		}

		if (!StringUtility.isNullOrWhiteSpace(partDescription)) {
			builder.AND("p.description LIKE :PART_DESCRIPTION").addParameter("PART_DESCRIPTION",
					'%' + partDescription + '%');
		}

		if (!CollectionUtility.isNullOrEmpty(partCategoryCds)) {
			builder.AND("p.categoryTypeCd IN (:PART_CATEGORY_CDS)").addParameter("PART_CATEGORY_CDS", partCategoryCds);
		}

		if (!CollectionUtility.isNullOrEmpty(partManufacturerCds)) {
			builder.AND("p.manufacturerCd IN (:PART_MANUF_CDS)").addParameter("PART_MANUF_CDS", partManufacturerCds);
		}

		if (!CollectionUtility.isNullOrEmpty(productTypeCds)) {
			builder.AND("p.productTypeCd IN (:PRODUCT_TYPE_CDS)").addParameter("PRODUCT_TYPE_CDS", productTypeCds);
		}

		if (onlyInStock) {
			builder.AND("p.inventoryCount > 0");
		}

		if (!StringUtility.isNullOrWhiteSpace(partManufId)) {
			builder.AND("p.manufPartId = :PART_MANUF_ID").addParameter("PART_MANUF_ID", partManufId);
		}

		return runDynamicQuery(builder);
	}
}
