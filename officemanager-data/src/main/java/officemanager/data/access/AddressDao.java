package officemanager.data.access;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.Address;

public class AddressDao extends Dao<Address> {

	private static Logger logger = LogManager.getLogger(AddressDao.class);

	public List<Address> findActiveAddress(Long personId) {
		return findActiveAddress(Lists.newArrayList(personId));
	}

	public List<Address> findActiveAddress(List<Long> personIdList) {
		logger.debug("Starting findActiveAddress for personIds=" + personIdList);
		checkNotNull(personIdList);
		if (personIdList.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByPersonIds_Address")
					.setParameterList("PERSON_IDS", personIdList);
			return getQueryResults(query);
		}
	}

	/**
	 * Finds a single primary address for a person id. If multiples are found a
	 * warning message is logged and the first one is returned.
	 * 
	 * @param personId
	 *            The person id to find the primary address for.
	 * @return The list of primary addresses.
	 */
	public List<Address> findPrimaryAddress(Long personId) {
		return findPrimaryAddress(Lists.newArrayList(personId));
	}

	public List<Address> findPrimaryAddress(List<Long> personIdList) {
		logger.debug("Find primary address for person ids=" + personIdList);
		if (personIdList == null) {
			throw new IllegalArgumentException("The personIdList cannot be null.");
		}
		if (personIdList.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findPrimaryAddressByPersonIds_Address")
					.setParameterList("PERSON_IDS", personIdList);
			return getQueryResults(query);
		}
	}

	/**
	 * Updates all the person's addresses to not be the primary address except
	 * the one specified.
	 * 
	 * @param addressId
	 *            The new primary address number.
	 * @param personId
	 *            The person id.
	 */
	public void updatePrimaryAddressForPerson(Long addressId, Long personId) {
		logger.debug(
				"Starting updatePrimaryAddressForPerson with addressId=" + addressId + ", and personId=" + personId);
		checkNotNull(addressId);
		checkNotNull(personId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				String update = "UPDATE Address a SET a.primary_address_ind = (a.address_id = :PRIMARY_ADDRESS_ID) WHERE a.person_id = :PERSON_ID";

				session.getSession().createSQLQuery(update).setLong("PRIMARY_ADDRESS_ID", addressId)
				.setLong("PERSON_ID", personId).executeUpdate();
			}
		}
	}

	/**
	 * Updates all addresses for the person to not be the primary address.
	 * 
	 * @param personId
	 *            The person id to update addresses for.
	 */
	public void disablePrimaryAddressForPerson(Long personId) {
		logger.debug("Starting disablePrimaryAddressForPerson with personId=" + personId);
		checkNotNull(personId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("disablePrimaryAddressForPerson_Address")
				.setLong("PERSON_ID", personId).executeUpdate();
			}
		}
	}

	public void disableAddressesForPerson(Long personId, Collection<Long> activeAddressIds) {
		logger.debug("Starting disableAddressesForPerson with personId=" + personId + ", and activeAddressIds="
				+ activeAddressIds);
		checkNotNull(personId);
		checkNotNull(activeAddressIds);
		checkArgument(activeAddressIds.size() > 0);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("disableAddressesForPersonWithActiveAddresses_Address")
				.setLong("PERSON_ID", personId).setParameterList("ADDRESS_IDS", activeAddressIds)
				.executeUpdate();
			}
		}
	}

	public void disableAddressesForPerson(Long personId){
		logger.debug("Starting disableAddressesForPerson with personId=" + personId);
		checkNotNull(personId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("disableAddressesForPerson_Address").setLong("PERSON_ID", personId)
				.executeUpdate();
			}
		}
	}
}
