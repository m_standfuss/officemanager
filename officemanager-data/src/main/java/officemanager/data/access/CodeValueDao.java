package officemanager.data.access;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.CodeValue;

public class CodeValueDao extends Dao<CodeValue> {

	private static final Logger logger = LogManager.getLogger(CodeValueDao.class);

	public List<CodeValue> findByCodeSetAndCdfMeaningList(Long codeSet, List<String> cdfMeaningList) {
		logger.debug("findByCodeSetAndCdfMeaningList for codeSet = " + codeSet + " and cdf meanings " + cdfMeaningList
				+ ".");
		if (cdfMeaningList == null) {
			throw new IllegalArgumentException("The cdf meaning list cannot be null.");
		}
		if (cdfMeaningList.isEmpty()) {
			return Lists.newArrayList();
		}

		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findByCodeSetAndCDFList_CodeValue")
					.setLong("CODE_SET", codeSet).setParameterList("CDF_MEANINGS", cdfMeaningList);
			return getQueryResults(query);
		}
	}

	public List<CodeValue> findByCodeSetList(List<Long> codeSetList) {
		logger.debug("findByCodeSetList for codesets=" + codeSetList + ".");
		if (codeSetList == null) {
			throw new IllegalArgumentException("The code set list cannot be null.");
		}
		if (codeSetList.isEmpty()) {
			return Lists.newArrayList();
		}

		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findByCodeSetList_CodeValue")
					.setParameterList("CODE_SETS", codeSetList);
			return getQueryResults(query);
		}
	}
}
