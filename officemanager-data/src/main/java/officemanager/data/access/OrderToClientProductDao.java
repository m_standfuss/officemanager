package officemanager.data.access;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.OrderToClientProduct;

public class OrderToClientProductDao extends Dao<OrderToClientProduct> {

	private static final Logger logger = LogManager.getLogger(OrderToClientProductDao.class);

	public List<OrderToClientProduct> findActiveByOrderId(Long orderId) {
		logger.debug("findActiveByOrderId for orderId = " + orderId + ".");
		checkNotNull(orderId);
		return findActiveByOrderIds(Lists.newArrayList(orderId));
	}

	public List<OrderToClientProduct> findActiveByOrderIds(Collection<Long> orderIds) {
		logger.debug("findActiveByOrderIds for orderIds = " + orderIds + ".");
		checkNotNull(orderIds);
		if (orderIds.isEmpty()) {
			return Lists.newArrayList();
		}

		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveProductsByOrderIds_OrderToClientProduct")
					.setParameterList("ORDER_IDS", orderIds);
			return getQueryResults(query);
		}
	}

	public int inactivateByOrderId(Long orderId) {
		logger.debug("inactivateByOrderId for orderId=" + orderId);
		checkNotNull(orderId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				Query query = session.getSession().getNamedQuery("inactivateProductsByOrderId_OrderToClientProduct")
						.setLong("ORDER_ID", orderId);
				return query.executeUpdate();
			}
		}
	}

	public int inactivateByOrderIdAndPrimaryKeysNotIn(Long orderId, List<Long> otcpIds) {
		logger.debug("inactivateByOrderIdAndPrimaryKeysNotIn for orderId=" + orderId + ", otcpIds=" + otcpIds);
		checkNotNull(orderId, otcpIds);
		checkArgument(!otcpIds.isEmpty());
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				Query query = session.getSession().getNamedQuery("inactivateByOrderIdAndPrimaryKeysNotIn_OrderToPart")
						.setLong("ORDER_ID", orderId)
						.setParameterList("ORDER_TO_PART_IDS", otcpIds);
				return query.executeUpdate();
			}
		}
	}
}
