package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.Account;

public class AccountDao extends Dao<Account> {

	private static final Logger logger = LogManager.getLogger(AccountDao.class);

	public Account findActiveAccountByClientId(Long clientId) {
		logger.debug("Starting findActiveAccountByClientId with clientId=" + clientId);
		checkNotNull(clientId);

		final List<Account> resultList;
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveAccountByClientId_Account")
					.setMaxResults(1).setLong("CLIENT_ID", clientId);
			resultList = getQueryResults(query);
		}

		if (resultList.isEmpty()) {
			logger.debug("No results were found.");
			return null;
		}
		return resultList.get(0);
	}

	public void updateStatus(Long accountId, Long accountStatusCd) {
		logger.debug("Starting updateStatus for accountId=" + accountId + " with status cd=" + accountStatusCd);
		checkNotNull(accountId);
		checkNotNull(accountStatusCd);

		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("updateAccountStatus_Account").setLong("ACCOUNT_ID", accountId)
						.setLong("STATUS_CD", accountStatusCd).executeUpdate();
			}
		}
	}

	public List<Account> findBySearchCriteria(Long accountId, List<Long> clientIds) {
		logger.debug("findBySearchCriteria with accountId=" + accountId + ", clientIds=" + clientIds);

		if (clientIds != null && clientIds.isEmpty()) {
			logger.debug("Empty client id collection specified returning empty set.");
			return Lists.newArrayList();
		}

		final QueryBuilder builder = new QueryBuilder().SELECT("a").FROM("Account a").WHERE("a.activeInd = true");

		if (accountId != null) {
			builder.AND("a.accountId = :ACCOUNT_ID").addParameter("ACCOUNT_ID", accountId);
		}

		if (clientIds != null) {
			builder.AND("a.clientId IN (:CLIENT_IDS)").addParameter("CLIENT_IDS", clientIds);
		}
		return runDynamicQuery(builder);
	}
}
