package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.OrderComment;

public class OrderCommentDao extends Dao<OrderComment> {

	private final static Logger logger = LogManager.getLogger(OrderCommentDao.class);

	@Override
	public void persist(OrderComment entity) {
		if (entity.getOrderCommentId() == null) {
			entity.setAuthorId(currentUser);
		}
		super.persist(entity);
	}

	public List<OrderComment> findActiveByOrderId(Long orderId) {
		return findActiveByOrderIds(Lists.newArrayList(orderId));
	}

	public List<OrderComment> findActiveByOrderIds(List<Long> orderIds) {
		logger.debug("findActiveByOrderIds for orderIds = " + orderIds + ".");

		checkNotNull(orderIds);
		if (orderIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByOrderIds_OrderComment")
					.setParameterList("ORDER_IDS", orderIds);
			return getQueryResults(query);
		}
	}

	public List<OrderComment> findActiveByOrderIdAndTypes(Long orderId, List<Long> orderCommentTypes) {
		logger.debug("findActiveByOrderIdAndTypes for orderId = " + orderId + " and types = " + orderCommentTypes);
		checkNotNull(orderId);
		checkNotNull(orderCommentTypes);
		if (orderCommentTypes.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByOrderIdAndTypes_OrderComment")
					.setLong("ORDER_ID", orderId).setParameterList("TYPE_CDS", orderCommentTypes);
			return getQueryResults(query);
		}
	}
}
