package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.RolePermission;
import officemanager.utility.CollectionUtility;

public class RolePermissionDao extends Dao<RolePermission> {
	private static final Logger logger = LogManager.getLogger(RolePermissionDao.class);

	/**
	 * Finds a list of active role permissions for a given collection of roles.
	 * 
	 * @param roleIds
	 *            The role identifiers. If empty will not query.
	 * @return The list of active role permissions.
	 * @throws NullPointerException
	 *             if roleIds is null.
	 */
	public List<RolePermission> findActiveByRoleIds(Collection<Long> roleIds) {
		logger.debug("Starting findActiveByRoleIds_RolePermission for roleIds=" + roleIds);
		checkNotNull(roleIds);

		if (roleIds.isEmpty()) {
			logger.debug("No role ids, skipping query.");
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findActiveByRoleIds_RolePermission")
					.setParameterList("ROLE_IDS", roleIds);
			return getQueryResults(query);
		}
	}

	public List<RolePermission> findActiveByRoleId(Long roleId) {
		logger.debug("Starting findActiveByRoleId for roleId=" + roleId);
		checkNotNull(roleId);
		return findActiveByRoleIds(Lists.newArrayList(roleId));
	}

	public void inactivateByRoleId(Long roleId) {
		logger.debug("Starting inactivateByRoleId for roleId=" + roleId);
		checkNotNull(roleId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("inactivateByRoleId_RolePermission").setLong("ROLE_ID", roleId)
				.executeUpdate();
			}
		}
	}

	public void inactivateByRoleIdExcept(Long roleId, List<Long> rolePermissionIds) {
		logger.debug("Starting inactivateByRoleIdExcept for roleId=" + roleId + " except rolePermissionIds="
				+ rolePermissionIds);
		checkNotNull(roleId);
		if (CollectionUtility.isNullOrEmpty(rolePermissionIds)) {
			inactivateByRoleId(roleId);
		}
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("inactivateByRoleIdExcept_RolePermission").setLong("ROLE_ID", roleId)
				.setParameterList("ROLE_PERMISSION_IDS", rolePermissionIds).executeUpdate();
			}
		}
	}
}
