package officemanager.data.access;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.Personnel;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class PersonnelDao extends Dao<Personnel> {
	private static Logger logger = LogManager.getLogger(PersonnelDao.class);

	@Override
	public void persist(Personnel entity) {
		if (entity.getPersonnelId() == null) {
			entity.setPassword(null);
			entity.setPasswordLastSet(null);
			entity.setPasswordNeedsReset(true);
			entity.setStartDttm(new Date());
		}
		super.persist(entity);
	}

	public Personnel findByUsername(String username) {
		logger.debug("findByUsername for username = '" + username + "'.");

		final List<Personnel> resultList;
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findByUsername_Personnel").setMaxResults(1)
					.setString("USERNAME", username);
			resultList = getQueryResults(query);
		}

		if (resultList.isEmpty()) {
			logger.debug("No results were found.");
			return null;
		}
		return resultList.get(0);
	}

	public List<Personnel> findByPersonIds(Collection<Long> personIds) {
		logger.debug("findByPersonIds, personIds=" + personIds);

		final List<Personnel> resultList;
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findByPersonIds_Personnel")
					.setParameterList("PERSON_IDS", personIds);
			resultList = getQueryResults(query);
		}
		return resultList;
	}

	public void updatePassword(Long personnelId, byte[] encryptToBytes) {
		logger.debug("updatePassword for personnelId=" + personnelId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("updatePassword_Prsnl").setLong("PERSONNEL_ID", personnelId)
						.setBinary("PASSWORD", encryptToBytes).setTimestamp("DATE_SET", new Date()).executeUpdate();
			}
		}
	}

	public List<Personnel> searchPersonnel(String username, List<Long> personnelStatusCds,
			List<Long> personnelTypeCds) {
		logger.debug("searchPersonnel with username=" + username + ", personnelStatusCds=" + personnelStatusCds
				+ ", personnelTypeCds=" + personnelTypeCds);

		final QueryBuilder builder = new QueryBuilder().SELECT("p").FROM("Personnel p").WHERE("p.activeInd = true");

		if (!StringUtility.isNullOrWhiteSpace(username)) {
			builder.AND("p.username LIKE :USERNAME").addParameter("USERNAME", username + "%");
		}
		if (!CollectionUtility.isNullOrEmpty(personnelStatusCds)) {
			builder.AND("p.personnelStatusCd IN ( :PERSONNEL_STATUS )").addParameter("PERSONNEL_STATUS",
					personnelStatusCds);
		}
		if (!CollectionUtility.isNullOrEmpty(personnelTypeCds)) {
			builder.AND("p.personnelTypeCd IN ( :PERSONNEL_TYPES )").addParameter("PERSONNEL_TYPES", personnelTypeCds);
		}
		return runDynamicQuery(builder);
	}
}
