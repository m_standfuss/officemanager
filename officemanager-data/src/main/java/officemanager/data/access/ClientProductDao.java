package officemanager.data.access;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.ClientProduct;

public class ClientProductDao extends Dao<ClientProduct> {

	private static final Logger logger = LogManager.getLogger(ClientProductDao.class);

	@Override
	public void persist(ClientProduct entity) {
		if (entity.getClientProductId() == null) {
			entity.setWorkCnt((short) 0);
		}
		super.persist(entity);
	}

	public List<ClientProduct> findActiveProductsByClientId(Long clientId) {
		logger.debug("findActiveProductsByClientId for client id=" + clientId);
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveProductsByClientId_ClientProduct")
					.setLong("CLIENT_ID", clientId);
			return getQueryResults(query);
		}
	}
}
