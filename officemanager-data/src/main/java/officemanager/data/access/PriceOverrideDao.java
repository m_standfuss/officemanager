package officemanager.data.access;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import officemanager.data.models.PriceOverride;

public class PriceOverrideDao extends Dao<PriceOverride> {

	private static final Logger logger = LogManager.getLogger(PriceOverrideDao.class);

	public List<PriceOverride> findActiveByDates(Date startDttm, Date endDttm) {
		logger.debug("findActiveByDates with startDttm=" + startDttm + ", endDttm=" + endDttm);
		if (startDttm == null || endDttm == null) {
			throw new IllegalArgumentException("The dates cannot be null.");
		}
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findActiveByDates_PriceOverride")
					.setTimestamp("START_DTTM", startDttm).setTimestamp("END_DTTM", endDttm);
			return getQueryResults(query);
		}
	}
}