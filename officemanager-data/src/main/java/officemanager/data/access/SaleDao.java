package officemanager.data.access;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.data.models.Sale;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;
import officemanager.utility.Tuple;

public class SaleDao extends Dao<Sale> {

	private static final Logger logger = LogManager.getLogger(SaleDao.class);

	public List<Sale> getSaleForDateAndParentEntityIdList(String parentEntityName, List<Long> parentEntityIds,
			Date date) {
		logger.debug("Starting getSaleForDateAndParentEntityIdList for parentEntityName=" + parentEntityName + ", date="
				+ date + " and partIds=" + parentEntityIds);
		try (CloseableSession session = getOpenSession()) {
			return getQueryResults(session.getSession().getNamedQuery("findByDateParentEntityIds_Sale")
					.setString("PARENT_ENTITY_NAME", parentEntityName)
					.setParameterList("PARENT_ENTITY_IDS", parentEntityIds).setTimestamp("DATE", date));
		}
	}

	public void inactivateByParentEntity(String parentEntityName, Long parentEntityId) {
		logger.debug("Starting inactivateByParentEntity for parentEntityName=" + parentEntityName
				+ " and parentEntityId=" + parentEntityId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("inactivateByParentEntity_Sale")
						.setString("PARENT_ENTITY_NAME", parentEntityName).setLong("PARENT_ENTITY_ID", parentEntityId)
						.executeUpdate();
			}
		}
	}

	public List<Sale> searchActiveSales(String saleName, List<Long> saleTypeCds,
			List<Tuple<String, List<Long>>> parentEntities, Date begActiveDttm, Date endActiveDttm) {
		logger.debug("Starting searchActiveSales with saleName=" + saleName + ", saleTypeCds=" + saleTypeCds
				+ ", parentEntities=" + parentEntities + "begActiveDttm=" + begActiveDttm + ", endActiveDttm="
				+ endActiveDttm);
		final QueryBuilder builder = new QueryBuilder().SELECT("s").FROM("Sale s").WHERE("s.activeInd = true");

		if (!StringUtility.isNullOrWhiteSpace(saleName)) {
			builder.AND("s.saleName LIKE :SALE_NAME").addParameter("SALE_NAME", saleName + '%');
		}
		if (!CollectionUtility.isNullOrEmpty(saleTypeCds)) {
			builder.AND("s.saleTypeCd IN ( :SALE_TYPE_CDS )").addParameter("SALE_TYPE_CDS", saleTypeCds);
		}
		if (!CollectionUtility.isNullOrEmpty(parentEntities)) {
			List<String> clauses = Lists.newArrayList();
			List<Tuple<String, Object>> parameterList = Lists.newArrayList();
			for (int i = 0; i < parentEntities.size(); i++) {
				clauses.add("s.parentEntityName = :PARENT_ENTITY_NAME" + i
						+ " AND s.parentEntityId IN ( :PARENT_ENTITY_IDS" + i + ")");

				Tuple<String, List<Long>> parentEntity = parentEntities.get(i);
				parameterList.add(Tuple.newTuple("PARENT_ENTITY_NAME" + i, parentEntity.item1));
				parameterList.add(Tuple.newTuple("PARENT_ENTITY_IDS" + i, parentEntity.item2));
			}
			builder.OR(clauses);
			for (Tuple<String, Object> parameter : parameterList) {
				builder.addParameter(parameter.item1, parameter.item2);
			}
		}
		if (begActiveDttm != null && endActiveDttm != null) {
			builder.AND("s.startDttm <= :END_DATE").addParameter("END_DATE", endActiveDttm);
			builder.AND("(s.endDttm >= :START_DATE OR s.endDttm is null)").addParameter("START_DATE", begActiveDttm);
		}
		if (endActiveDttm != null) {}
		return runDynamicQuery(builder);
	}
}
