package officemanager.data.access;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.Preference;

public class PreferenceDao extends Dao<Preference> {

	private static final Logger logger = LogManager.getLogger(PreferenceDao.class);

	public void inactivateByMeaning(String meaning) {
		logger.debug("Starting inactivateByMeaning with meaning=" + meaning);
		checkNotNull(meaning);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().getNamedQuery("inactivateByMeaning_Preference").setString("MEANING", meaning)
						.executeUpdate();
			}
		}
	}

	public List<Preference> findActiveByMeaning(String meaning) {
		logger.debug("Starting findActiveByMeaning for meaning = " + meaning);
		checkNotNull(meaning);
		return findActiveByMeanings(Lists.newArrayList(meaning));
	}

	public List<Preference> findActiveByMeanings(List<String> meanings) {
		logger.debug("Starting findActiveByMeanings for meanings = " + meanings);
		checkNotNull(meanings);
		checkArgument(!meanings.contains(null));

		if (meanings.isEmpty()) {
			logger.debug("No preference meanings, skipping query.");
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			Query query = session.getSession().getNamedQuery("findActiveByMeanings_Preference")
					.setParameterList("PREF_MEANINGS", meanings);
			return getQueryResults(query);
		}
	}
}
