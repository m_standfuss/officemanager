package officemanager.data.access;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.OrderToPayment;

public class OrderToPaymentDao extends Dao<OrderToPayment> {

	private static final Logger logger = LogManager.getLogger(OrderToPaymentDao.class);

	public List<OrderToPayment> getActiveByOrderId(Long orderId) {
		logger.debug("Start getActiveOrderToPaymentByOrderId for orderId=" + orderId);
		checkNotNull(orderId);
		return getActiveByOrderIds(Lists.newArrayList(orderId));
	}

	public List<OrderToPayment> getActiveByOrderIds(List<Long> orderIds) {
		logger.debug("Start getActiveByOrderIds for orderIds=" + orderIds);
		checkNotNull(orderIds);
		checkArgument(!orderIds.contains(null));
		if (orderIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession().getNamedQuery("findActiveByOrderIds_OrderToPayment")
					.setParameterList("ORDER_IDS", orderIds);
			return getQueryResults(query);
		}
	}
}
