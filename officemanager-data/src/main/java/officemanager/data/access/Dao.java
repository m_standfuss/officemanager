package officemanager.data.access;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.google.common.collect.Lists;

import officemanager.data.models.OfficeManagerEntity;

public abstract class Dao<T extends OfficeManagerEntity> {

	public static final int MAX_IN_PARAMS = 2000;

	private static final Logger logger = LogManager.getLogger(Dao.class);
	private static final Object sessionFactoryLock = new Object();

	private static String hostName = null;
	private static boolean credsChanged = true;
	private static SessionFactory sessionFactory = null;
	/**
	 * A session and transaction to be used if you want to make multiple actions
	 * on a single transaction. Instead of returning a new
	 * {@link ClosableSession} when {@link Dao#getOpenSession()} is called the
	 * same session will be returned.
	 */
	private static CloseableSession currentSession = null;
	private static CloseableTransaction currentTransaction = null;

	protected static Long currentUser = 0L;

	private final Class<T> entityClass;

	protected Dao() {
		entityClass = getEntityClass();
	}

	/**
	 * Sets the current user for all update prsnl id's to be set to for
	 * inserts/updates.
	 * 
	 * @param currentUser
	 *            The personnel id of the currently logged on user.
	 */
	public static void setCurrentUser(Long currentUser) {
		Dao.currentUser = currentUser;
	}

	/**
	 * Sets the url of the datastore to connect to. If not set will default to
	 * localhost.
	 * 
	 * @param hostName
	 *            The url to connect to.
	 */
	public static final void setHostName(String hostName) {
		logger.debug("Setting the database host name to " + hostName);
		Dao.hostName = hostName;
		credsChanged = true;
	}

	/**
	 * Closes all open connections allowing them to terminate properly.
	 * 
	 * @throws HibernateException
	 *             Indicates a problem closing connections.
	 */
	public static void closeConnections() throws HibernateException {
		if (sessionFactory != null) {
			sessionFactory.close();
		}
	}

	/**
	 * Opens a new transaction. Every data access object will use this same
	 * transaction until {@link #closeTransaction()} is called. </br>
	 * </br>
	 * <b>Note</b> this is not necessarily thread safe unless you want all
	 * thread to share a transaction.
	 */
	public static void openTransaction() {
		if (currentSession != null) {
			throw new IllegalStateException(
					"A transaction is currently already open. Must be closed before opening a new.");
		}
		synchronized (sessionFactoryLock) {
			currentSession = _getOpenSession(true);
			currentTransaction = currentSession.beginTransaction();
		}
	}

	/**
	 * Closes the current transaction opened through {@link #openTransaction()}.
	 */
	public static void closeTransaction() {
		checkNotNull(currentTransaction, "The current transaction was not opened first.");
		checkNotNull(currentSession, "The current transaction was not opened first.");
		synchronized (sessionFactoryLock) {
			currentTransaction.manuallyClose();
			currentSession.manuallyClose();
			currentTransaction = null;
			currentSession = null;
		}
	}

	/**
	 * Attempts to create a session to the data store. If anything goes wrong
	 * swallows the problem and returns false.
	 * 
	 * @return If a connection can successfully be made to the data store.
	 */
	public static boolean testConnection() {
		logger.debug("Testing the hibernate connection to the database.");

		boolean validConnection;
		try {
			createSessionFactory();
			try (CloseableSession session = getOpenSession()) {
				session.getSession().createSQLQuery("select 0").uniqueResult();
			}
			validConnection = true;
		} catch (final Exception e) {
			logger.error("Cannot make a successful connection to the data base.", e);
			validConnection = false;
		}

		if (!validConnection) {
			logger.error("The connection could not be made to the database.");
			logger.debug("Checking if the hostname is reachable.");
			if (!isHostNamePingable()) {
				logger.error("The database host is not reachable check the pc's ip or hostname.");
			} else {
				logger.debug(
						"The database host was found to be reachable, check that the database service is running and that all the correct ports are opened.");
			}
		}
		return validConnection;
	}

	/**
	 * Gets a new session from the data store and opens it with the current
	 * session configuration settings. Caller of this function is responsible
	 * for closing the session properly.
	 * 
	 * If any configurations have changed since the last session was created the
	 * session factory will be flushed and recreated to reflect those changes.
	 * 
	 * @return A new open session.
	 */
	protected static CloseableSession getOpenSession() {
		if (credsChanged) {
			logger.debug("The connection credentials have changed since the last session was generated. "
					+ "Recreating the session factory.");
			createSessionFactory();
		}
		synchronized (sessionFactoryLock) {
			if (currentSession != null) {
				return currentSession;
			}
			return _getOpenSession(false);
		}
	}

	/**
	 * Takes a list and partitions it up into a lists of acceptable length (
	 * {@value #MAX_IN_PARAMS}) for using in an in parameter query.
	 * 
	 * @param list
	 *            The list to chunk up.
	 * @return The resulting list of lists.
	 */
	protected static <E> List<List<E>> partitionInParameterList(List<E> list) {
		return Lists.partition(list, MAX_IN_PARAMS);
	}

	/**
	 * Uses the current url to create a session factory and store it in static
	 * state.
	 * 
	 * @throws IllegalStateException
	 *             If the url is not set.
	 */
	private static void createSessionFactory() throws IllegalStateException {
		logger.debug("Attempting to make a new session factory.");
		checkNotNull(hostName);
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure()
				.applySetting("hibernate.connection.url", getUrl()).build();

		synchronized (sessionFactoryLock) {
			try {
				sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
				credsChanged = false;
			} catch (Exception e) {
				logger.error("An error occured while attempting to make a session factory.", e);
				StandardServiceRegistryBuilder.destroy(registry);
			}
		}
	}

	private static String getUrl() {
		return "jdbc:mysql://" + hostName + ":3306/officemanager";
	}

	private static boolean isHostNamePingable() {
		try {
			return InetAddress.getByName(hostName).isReachable(5000);
		} catch (final IOException e) {
			logger.error("Unable to ping the host of the database.", e);
			return false;
		}
	}

	/**
	 * The private implementation of getting an open session. Should only be
	 * called if you currently posses the session factory lock.
	 * 
	 * @return A new session.
	 */
	private static CloseableSession _getOpenSession(boolean manuallyClose) {
		Session session = sessionFactory.openSession();
		return new CloseableSession(session, manuallyClose);
	}

	/**
	 * Inserts or updates the entity into the data store based on if the primary
	 * key is set or not.
	 * 
	 * Note all data entities will have their update date time set to now, updt
	 * prsnl id to the session user and the active ind set to true.
	 * 
	 * @param entity
	 *            The entity to persist into data store.
	 * @throws IllegalArgumentException
	 *             If the entity is null.
	 */
	public void persist(T entity) {
		checkNotNull(entity);
		entity.setUpdtDttm(new Date());
		entity.setUpdtPrsnlId(currentUser);
		entity.setActiveInd(true);
		logger.debug("Persisting entity " + entity.toString());

		_persist(entity);
	}

	/**
	 * Removes the entity from the data store.
	 * 
	 * @param entity
	 *            The entity to remove.
	 * @throws IllegalArgumentException
	 *             If the entity is null.
	 */
	public void remove(T entity) {
		checkNotNull(entity);
		logger.debug("Removing entity " + entity);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().delete(entity);
			}
		}
	}

	/**
	 * Inactivates the entity in the data store. Used for logical deletion of a
	 * row instead of the remove method.
	 * 
	 * Note this will not make any cascading updates to child models.
	 * 
	 * @param entity
	 *            The entity to logically delete.
	 */
	public void inactivate(T entity) {
		checkNotNull(entity);
		entity.setUpdtDttm(new Date());
		entity.setUpdtPrsnlId(currentUser);
		entity.setActiveInd(false);
		logger.debug("Inactivating the entity " + entity);
		_persist(entity);
	}

	/**
	 * Inactivates an entity based on the primary key only. First gets the
	 * entity with findById then calls inactivate(T entity) with the result, if
	 * not null.
	 * 
	 * @param id
	 *            The id of the entity to inactivate.
	 */
	public void inactivate(Serializable id) {
		checkNotNull(id);
		logger.debug("inactivate for id = " + id);
		final T entity = findById(id);
		if (entity == null) {
			logger.error("Unable to find the entity with id = + " + id + " unable to inactivate.");
			return;
		}
		inactivate(entity);
	}

	public void reactivate(T entity) {
		checkNotNull(entity);
		entity.setUpdtDttm(new Date());
		entity.setUpdtPrsnlId(currentUser);
		entity.setActiveInd(true);
		logger.debug("Reactivating the entity " + entity);
		_persist(entity);
	}

	public void reactivate(Serializable id) {
		checkNotNull(id);
		logger.debug("reactivate for id = " + id);
		T entity = findById(id);
		if (entity == null) {
			logger.error("Unable to find the entity with id = " + id + " unable to reactivate.");
			return;
		}
		reactivate(entity);
	}

	/**
	 * Gets a entity model based on the primary key from the data store. Uses
	 * the type parameter of the JpaDao in order to tell which entity to map to.
	 * 
	 * @param id
	 *            The identifier to get on.
	 * @throws IllegalArgumentException
	 *             If the id is null.
	 * @return The entity if found, null if not.
	 */
	public T findById(Serializable id) {
		checkNotNull(id);
		logger.debug("Finding by id for entity class = '" + entityClass + "' and id = '" + id + "'.");

		try (CloseableSession session = getOpenSession()) {
			return session.getSession().get(entityClass, id);
		}
	}

	public T findActiveById(Serializable id) {
		checkNotNull(id);
		logger.debug("Finding active by id for entity class = '" + entityClass + "' and id = " + id);

		try (CloseableSession session = getOpenSession()) {
			final Query q = session.getSession()
					.createQuery("from " + entityClass.getName() + " where id = :ID AND activeInd = true")
					.setParameter("ID", id);
			List<T> ts = getQueryResults(q);
			if (ts.isEmpty()) {
				return null;
			}
			return ts.get(0);
		}
	}

	/**
	 * Gets a list of entity models based on a set of primary keys. If an empty
	 * list of identifiers is passed in an empty list of entities will be
	 * returned.
	 * 
	 * @param idList
	 *            The list of identifiers.
	 * @return The result list.
	 * @throws NullPointerException
	 *             if the idList is null.
	 */
	public List<T> findByIdList(Collection<? extends Serializable> idList) {
		checkNotNull(idList);
		if (idList.isEmpty()) {
			return Lists.newArrayList();
		}
		logger.debug("Finding by id list for entity class = '" + entityClass + "' and id list = " + idList);

		try (CloseableSession session = getOpenSession()) {
			final Query q = session.getSession().createQuery("from " + entityClass.getName() + " where id IN :ID_LIST")
					.setParameterList("ID_LIST", idList);
			return getQueryResults(q);
		}
	}

	public List<T> findActive() {
		logger.debug("Finding active rows for entity class = " + entityClass + ".");
		try (CloseableSession session = getOpenSession()) {
			final Query q = session.getSession()
					.createQuery("from " + entityClass.getName() + " where activeInd = true");
			return getQueryResults(q);
		}
	}

	/**
	 * Gets a list of active entity models based on a set of primary keys. If an
	 * empty list of identifiers is passed in an empty list will be returned
	 * without querying.
	 * 
	 * @param idList
	 *            The list of identifiers.
	 * @return The active entities for the primary ids.
	 * @throws NullPointerException
	 *             If the idList is null.
	 */
	public List<T> findActiveByIdList(Collection<? extends Serializable> idList) {
		checkNotNull(idList);
		logger.debug("Starting findActiveByIdList for ids=" + idList);
		if (idList.isEmpty()) {
			logger.debug("No primary keys provided.");
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			Query q = session.getSession()
					.createQuery("from " + entityClass.getName() + " where id IN :ID_LIST AND activeInd = true")
					.setParameterList("ID_LIST", idList);
			return getQueryResults(q);
		}
	}

	/**
	 * Runs a dynamically created query populated by using a QueryBuilder
	 * object.
	 * 
	 * @param builder
	 *            The builder to run the query from.
	 * @return The result set of the query.
	 */
	protected List<T> runDynamicQuery(QueryBuilder builder) {
		try (CloseableSession session = getOpenSession()) {
			final Query query = builder.getQuery(session.getSession());
			return getQueryResults(query);
		}
	}

	protected void runDynamicStatement(String statement) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				SQLQuery query = session.getSession().createSQLQuery(statement);
				query.executeUpdate();
			}
		}
	}

	/**
	 * Allows for the unchecked warning to be supressed in a single place.
	 * 
	 * @param query
	 *            The query to get the result list from.
	 * @return The resulting list cast into the class entity.
	 */
	@SuppressWarnings("unchecked")
	protected List<T> getQueryResults(Query query) {
		return query.list();
	}

	/**
	 * Private implementation of the persist method which does not set any of
	 * the fields prior to being written to the datastore. Allows methods like
	 * inactivate to use same logic as persist but not have the active ind set
	 * back to true.
	 * 
	 * @param entity
	 *            The entity to persist. Should not be null but unchecked.
	 */
	private void _persist(T entity) {
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				session.getSession().saveOrUpdate(entity);
			}
		}
	}

	/**
	 * Uses reflection to get the type parameter of the JpaDao class.
	 */
	@SuppressWarnings("unchecked")
	private Class<T> getEntityClass() {
		final ParameterizedType genericSuperClass = (ParameterizedType) getClass().getGenericSuperclass();
		if (genericSuperClass.getActualTypeArguments().length == 0) {
			throw new IllegalArgumentException("The JpaDao must be implemented with a type parameter.");
		}
		logger.debug("Entity type of jpa " + genericSuperClass.getActualTypeArguments()[0]);
		return (Class<T>) genericSuperClass.getActualTypeArguments()[0];
	}
}
