package officemanager.data.access;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import officemanager.data.models.TradeIn;

public class TradeInDao extends Dao<TradeIn> {

	private static final Logger logger = LogManager.getLogger(TradeInDao.class);

	@Override
	public void persist(TradeIn entity) {
		if (entity.getTradeInId() == null) {
			logger.debug("Inserting a new trade in setting the created date and user.");
			entity.setCreatedDtTm(new Date());
			entity.setCreatePrsnlId(currentUser);
		}
		super.persist(entity);
	}
}
