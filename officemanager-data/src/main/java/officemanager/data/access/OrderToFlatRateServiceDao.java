package officemanager.data.access;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;

import com.google.common.collect.Lists;

import officemanager.data.models.OrderToFlatRateService;

public class OrderToFlatRateServiceDao extends Dao<OrderToFlatRateService> {

	private static final Logger logger = LogManager.getLogger(OrderToFlatRateServiceDao.class);

	public List<OrderToFlatRateService> findActiveByOrderId(Long orderId) {
		logger.debug("Starting findActiveByOrderId for orderId = " + orderId);
		checkNotNull(orderId);
		return findActiveByOrderIds(Lists.newArrayList(orderId));
	}

	public List<OrderToFlatRateService> findActiveByOrderIds(Collection<Long> orderIds) {
		logger.debug("findActiveByOrderIds for orderIds = " + orderIds + ".");
		checkNotNull(orderIds);
		if (orderIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession()
					.getNamedQuery("findActiveProductsByOrderIds_OrderToFlatRateService")
					.setParameterList("ORDER_IDS", orderIds);
			return getQueryResults(query);
		}
	}

	public List<OrderToFlatRateService> findActiveByOrderIdAndClientProductIds(Long orderId,
			List<Long> clientProductIds) {
		logger.debug("findActiveByOrderIdAndClientProductIds for orderId = " + orderId + ", and clientProductIds="
				+ clientProductIds);
		checkNotNull(orderId);
		checkNotNull(clientProductIds);
		checkArgument(!clientProductIds.contains(null));
		if (clientProductIds.isEmpty()) {
			return Lists.newArrayList();
		}
		try (CloseableSession session = getOpenSession()) {
			final Query query = session.getSession()
					.getNamedQuery("findActiveByOrderIdAndClientProductIds_OrderToFlatRateService")
					.setLong("ORDER_ID", orderId).setParameterList("CLIENT_PRODUCT_IDS", clientProductIds);
			return getQueryResults(query);
		}
	}

	public int inactivateByOrderId(Long orderId) {
		logger.debug("inactivateByOrderId with orderId=" + orderId);
		checkNotNull(orderId);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				Query query = session.getSession().getNamedQuery("inactivateByOrderId_OrderToFlatRateService")
						.setLong("ORDER_ID", orderId);
				return query.executeUpdate();
			}
		}
	}

	public int inactivateByOrderIdAndPrimaryKeyNotIn(Long orderId, List<Long> otfrsIds) {
		logger.debug("inactivateByOrderId with orderId=" + orderId + " and orderToFlatRateServicdIds=" + otfrsIds);
		checkNotNull(orderId, otfrsIds);
		try (CloseableSession session = getOpenSession()) {
			try (CloseableTransaction t = session.beginTransaction()) {
				Query query = session.getSession()
						.getNamedQuery("inactivateByOrderIdAndPrimaryKeyNotIn_OrderToFlatRateService")
						.setLong("ORDER_ID", orderId).setParameterList("OTFRS_IDS", otfrsIds);
				return query.executeUpdate();
			}
		}
	}
}
