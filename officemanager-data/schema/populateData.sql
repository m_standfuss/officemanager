
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (2, 'ACTIVE', 'ACTIVE', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (3, 'MOWER', 'MOWER', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (4, 'SET PRICE OFF', 'SET_PRICE_OFF', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (4, 'PERCENTAGE OFF', 'PERCENTAGE_OFF', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (5, 'IN STOCK', 'INSTOCK', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (5, 'OUT OF STOCK', 'OUTOFSTOCK', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (5, 'LOW STOCK', 'LOWSTOCK', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (6, 'MOWER', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (6, 'TRIMMER', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (6, 'BLOWER', '', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (7, 'OTHER', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (7, 'HYDRO GEAR', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (7, 'SCAG', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (7, 'POLARIS', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (7, 'POULAN', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (7, 'CHAMPION', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (7, 'MARUYAMA', '', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (8, 'BLADES', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (8, 'BELTS', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (8, 'FILTERS', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (8, 'MISC.', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (8, 'OIL', '', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (9, 'MAINTENANCE', '', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (9, 'MILEAGE', '', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (10, 'BID', 'BID', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (10, 'MERCHANDISE ORDER', 'MERCHANDISE', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (10, 'WORK', 'WORK', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (11, 'PLAIN TEXT', 'PLAINTEXT', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (11, 'HTML', 'HTML', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (11, 'RTF', 'RTF', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'CASH', 'CASH', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'CHECK', 'CHECK', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'CREDIT CARD', 'CREDIT_CARD', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'CLIENT CREDIT', 'CLIENT_CREDIT', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'PENDING', 'PENDING', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'GIFT CARD', 'GIFT_CARD', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'FINANCING', 'FINANCING', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'TRADE IN', 'TRADE_IN', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (12, 'REFUND', 'REFUND', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (14, 'CREDIT', 'CREDIT', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (14, 'DEBIT', 'DEBIT', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (15, 'ACTIVE', 'ACTIVE', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (15, 'DELETED', 'DELETED', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (15, 'TERMINATED', 'TERMINATED', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (16, 'FULL TIME', 'FULL_TIME', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (16, 'PART TIME', 'PART_TIME', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (16, 'CONTRACTOR', 'CONTRACTOR', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (19, 'BUILDING', 'BUILDING', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (19, 'ROOM', 'ROOM', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (19, 'INVENTORY LOCATION', 'INVLOC', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (21, 'CUSTOMER', 'CUSTOMER', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (21, 'SHOP', 'SHOP', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (21, 'IN HOUSE', 'INHOUSE', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (21, 'GENERAL', 'GENERAL', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (22, 'HOME', 'HOME', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (23, 'HOME', 'HOME', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (24, 'OPEN ORDER', 'OPEN', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (24, 'CLOSED ORDER', 'CLOSED', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (24, 'REFUNDED ORDER', 'REFUNDED', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (24, 'ORDER IN ERROR', 'INERROR', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (24, 'IN PROGRESS', 'INPROGRESS', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (25, 'ACTIVE', 'ACTIVE', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (25, 'CLOSED', 'CLOSED', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (25, 'CLOSED AWAITING PAYMENT', 'CLOSED_AWT_PYMT', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (25, 'SUSPENDED', 'SUSPENDED', null, '0', now(), true);

INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'ORDERS', 'ORDERS', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'FLAT RATE SERVICES', 'FLAT_RT_SERVS', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'SECURITY', 'SECURITY', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'PERSONNEL', 'PERSONNEL', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'CLIENT', 'CLIENT', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'PERSON', 'PERSON', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'PARTS', 'PARTS', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'INVENTORY', 'INVENTORY', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'SALES', 'SALES', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'PREFERENCES', 'PREFERENCES', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'REPORTING', 'REPORTING', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'ACCOUNTS', 'ACCOUNTS', null, '0', now(), true);
INSERT INTO `officemanager`.`code_value` (`code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (26, 'LOCATIONS', 'LOCATIONS', null, '0', now(), true);

INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_BID', 'ADD NEW BID', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_CLIENT', 'ADD NEW CLIENT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'CLIENT'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_CLIENT_ACCOUNT', 'ADD ACCOUNT TO CLIENT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ACCOUNTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_FLAT_RATE', 'ADD NEW FLAT RATE SERVICE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'FLAT_RT_SERVS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_PART', 'ADD NEW PART', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PARTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_PERSONNEL', 'ADD NEW PERSONNEL', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PERSONNEL'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_INVTRY', 'ADD INVENTORY', (select code_value from code_value where code_set = 26 and cdf_meaning = 'INVENTORY'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_SALE', 'ADD A NEW SALE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SALES'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_SERVICE_ORDER', 'ADD A NEW SERVICE ORDER', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_MERCH_ORDER', 'ADD A MERCHANDISE ORDER', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('ADD_ROLE', 'ADD A NEW ROLE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SECURITY'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('CLOSE_ACCOUNT', 'CLOSE CLIENT ACCOUNT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ACCOUNTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('CLOSE_ORDER', 'CLOSE AN ORDER', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('DELETE_ORDER', 'DELETE AN ORDER', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_CLIENT', 'EDIT CLIENT INFORMATION', (select code_value from code_value where code_set = 26 and cdf_meaning = 'CLIENT'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_CLIENT_PRODUCT', 'EDIT CLIENT PRODUCT INFORMATION', (select code_value from code_value where code_set = 26 and cdf_meaning = 'CLIENT'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_FLAT_RATE_DETAILS', 'EDIT FLAT RATE SERVICE DETAILS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'FLAT_RT_SERVS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_FLAT_RATE_PRICE', 'EDIT BASE FLAT RATE SERVICE PRICE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'FLAT_RT_SERVS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_INVTRY_CNT', 'EDIT CURRENT INVENTORY COUNT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'INVENTORY'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_PART_DETAILS', 'EDIT PART DETAILS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PARTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_PART_PRICE', 'EDIT BASE PART PRICE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PARTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_PERSONNEL', 'EDIT PERSONNEL DETAILS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PERSONNEL'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_PROMO_CLUB', 'SET CLIENT\'S PROMOTIONAL STATUS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'CLIENT'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_ROLE', 'EDIT A CURRENT ROLE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SECURITY'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_SALE_DETAILS', 'EDIT CURRENT SALES', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SALES'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_SALE_DATE', 'EDIT A CURRENT SALES DATE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SALES'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_SERVICE_LABOR_RATE', 'EDIT LABOR RATE OF A SERVICE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('EDIT_TAX_EXEMPT', 'SET CLIENT\'S TAX EXEMPT STATUS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'CLIENT'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('END_SALE', 'END A CURRENT SALE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SALES'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('DISCOUNT_PRICE', 'DISCOUNT A PRICE OF AN ORDER ITEM', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SALES'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('LOCATIONS_VIEW', 'VIEW LOCATION DATA', (select code_value from code_value where code_set = 26 and cdf_meaning = 'LOCATIONS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('PREFERENCES_VIEW', 'VIEW APPLICATION PREFERENCES', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PREFERENCES'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('PREFERENCES_EDIT', 'EDIT APPLICATION PREFERENCES', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PREFERENCES'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REFUND_ORDER', 'REFUND ORDERS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REMOVE_ADDRESS', 'REMOVE ADDRESSES', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PERSON'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REMOVE_CLIENT_PRODUCT', 'REMOVE CLIENT PRODUCT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'CLIENT'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REMOVE_PART', 'REMOVE PART', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PARTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REMOVE_PHONE', 'REMOVE PHONE NUMBERS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PERSON'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REMOVE_SERVICE', 'REMOVE FLAT RATE SERVICE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'FLAT_RT_SERVS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REOPEN_ACCOUNT', 'REOPEN CLIENT ACCOUNT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ACCOUNTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REOPEN_ORDER', 'REOPEN AN ORDER', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REPORTS_VIEW', 'VIEW REPORTS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'REPORTING'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REPORTS_TOTALS', 'VIEW TOTALS REPORT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'REPORTING'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REPORTS_BLNC_DRWR', 'VIEW BALANCE DRAWER REPORT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'REPORTING'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('REPORTS_INVENTORY', 'VIEW INVENTORY REPORT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'REPORTING'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SEARCH_ACCOUNTS', 'SEARCH CLIENT ACCOUNTS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ACCOUNTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SEARCH_CLOSED_ORDERS', 'SEARCH CLOSED ORDERS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SEARCH_OPEN_ORDERS', 'SEARCH OPEN ORDERS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ORDERS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SEARCH_PARTS', 'SEARCH PARTS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PARTS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SEARCH_FLAT_RATES', 'SEARCH FLAT RATE SERVICES', (select code_value from code_value where code_set = 26 and cdf_meaning = 'FLAT_RT_SERVS'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SEARCH_CLIENTS', 'SEARCH CLIENTS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'CLIENT'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SEARCH_PERSONNEL', 'SEARCH PERSONNEL', (select code_value from code_value where code_set = 26 and cdf_meaning = 'PERSONNEL'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SEARCH_SALES', 'SEARCH SALES', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SALES'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SECURITY_ADD_ROLE', 'ADD NEW ROLE', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SECURITY'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SECURITY_EDIT_ROLE', 'EDIT ROLE PERMISSIONS', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SECURITY'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SECURITY_USER', 'SET USER ROLES', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SECURITY'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SECURITY_VIEW', 'VIEW APPLICATION SECURITY', (select code_value from code_value where code_set = 26 and cdf_meaning = 'SECURITY'), '0', now(), TRUE);
INSERT INTO `officemanager`.`permission` (`permission_name`, `display_name`, `category_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('SUSPEND_ACCOUNT', 'SUSPEND CLIENT ACCOUNT', (select code_value from code_value where code_set = 26 and cdf_meaning = 'ACCOUNTS'), '0', now(), TRUE);

-- Everything below here is values for testing. 
INSERT INTO `officemanager`.`person` (`person_id`, `name_last`, `name_first`, `name_full_formatted`, `email`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'STANDFUSS', 'MIKE', 'MIKE STANDFUSS', 'M_STANDFUSS@HOTMAIL.COM', '0', now(), TRUE);
INSERT INTO `officemanager`.`person` (`person_id`, `name_last`, `name_first`, `name_full_formatted`, `email`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('2', 'STANDFUSS', 'BETH', 'BETH STANDFUSS', 'BETH@PARKLAND.COM', '0', now(), TRUE);

INSERT INTO `officemanager`.`address` (`person_id`, `address_type_cd`, `address_1`, `address_2`, `address_3`, `city`, `zip`, `state`, `updt_prsnl_id`, `updt_dttm`, `active_ind`, `primary_address_ind`) VALUES ('1', (select code_value from code_value where code_set = 23 and cdf_meaning = 'HOME'), '11517 S. PARKWOOD DR.', '', '', 'OLATHE', '64131', 'KS', '0', now(), true, true);

INSERT INTO `officemanager`.`phone` (`person_id`, `phone_type_cd`, `phone_number`, `extension`, `primary_phone_ind`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', (select code_value from code_value where code_set = 22 and cdf_meaning = 'HOME'), '5737479108', '', true, '0', now(), true);

INSERT INTO `officemanager`.`client` (`client_id`, `person_id`, `promotion_club`, `tax_exempt`, `joined_dttm`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', '1', false, false, now(), '0', now(), true);

INSERT INTO `officemanager`.`client_product` (`client_product_id`, `client_id`, `product_manuf`, `product_model`, `product_serial_num`, `product_barcode`, `work_cnt`, `product_status_cd`, `product_type_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', '1', 'SCAG', 'TURF-TIGER', '123abc', '', '0', (select code_value from code_value where code_set = 2 and cdf_meaning = 'ACTIVE'), (select code_value from code_value where code_set = 3 and cdf_meaning = 'MOWER'), '0', now(), true);

INSERT INTO `officemanager`.`personnel` (`personnel_id`, `person_id`, `username`, `start_dttm`, `locked_out`, `personnel_status_cd`, `terminate_dttm`, `w2_on_file`, `personnel_type_cd`, `password_needs_reset`, `password_last_set`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', '1', 'mike', now(), false, (select code_value from code_value where code_set = 15 and cdf_meaning = 'ACTIVE'), null, TRUE, (select code_value from code_value where code_set = 16 and cdf_meaning = 'CONTRACTOR'), FALSE, NOW(), '0', NOW(), TRUE);
INSERT INTO `officemanager`.`personnel` (`personnel_id`, `person_id`, `username`, `start_dttm`, `locked_out`, `personnel_status_cd`, `terminate_dttm`, `w2_on_file`, `personnel_type_cd`, `password_needs_reset`, `password_last_set`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('2', '2', 'beth', now(), false, (select code_value from code_value where code_set = 15 and cdf_meaning = 'ACTIVE'), null, TRUE, (select code_value from code_value where code_set = 16 and cdf_meaning = 'CONTRACTOR'), FALSE, NOW(), '0', NOW(), TRUE);

-- User Roles and Permissions
INSERT INTO `officemanager`.`role` (`role_id`, `name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1','ADMINISTRATOR','0', now(), TRUE);

INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_BID', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_CLIENT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_CLIENT_ACCOUNT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_FLAT_RATE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_PART', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_PERSONNEL', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_INVTRY', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_ROLE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_SALE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_SERVICE_ORDER', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'ADD_MERCH_ORDER', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'CLOSE_ACCOUNT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'CLOSE_ORDER', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'DELETE_ORDER', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_CLIENT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_CLIENT_PRODUCT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_FLAT_RATE_DETAILS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_FLAT_RATE_PRICE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_INVTRY_CNT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_PART_DETAILS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_PART_PRICE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_PERSONNEL', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_ROLE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_SALE_DETAILS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_SALE_DATE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_SERVICE_LABOR_RATE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_PROMO_CLUB', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'EDIT_TAX_EXEMPT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'END_SALE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'DISCOUNT_PRICE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'LOCATIONS_VIEW', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'PREFERENCES_VIEW', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'PREFERENCES_EDIT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REFUND_ORDER', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REMOVE_ADDRESS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REMOVE_CLIENT_PRODUCT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REMOVE_PART', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REMOVE_PHONE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REMOVE_SERVICE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REOPEN_ACCOUNT', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REOPEN_ORDER', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SEARCH_ACCOUNTS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SEARCH_CLOSED_ORDERS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SEARCH_OPEN_ORDERS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SEARCH_PARTS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SEARCH_FLAT_RATES', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SEARCH_CLIENTS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SEARCH_PERSONNEL', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SEARCH_SALES', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SECURITY_USER', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SECURITY_ADD_ROLE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SECURITY_EDIT_ROLE', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SECURITY_VIEW', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REPORTS_VIEW', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REPORTS_TOTALS', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REPORTS_BLNC_DRWR', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'REPORTS_INVENTORY', '0', now(), TRUE);
INSERT INTO `officemanager`.`role_permission` (`role_id`, `permission_name`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SUSPEND_ACCOUNT', '0', now(), TRUE);

INSERT INTO `officemanager`.`personnel_role` (`personnel_id`, `role_id`, `begin_effective_dttm`, `end_effective_dttm`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', '1', now(), null, '0', now(), TRUE);
INSERT INTO `officemanager`.`personnel_role` (`personnel_id`, `role_id`, `begin_effective_dttm`, `end_effective_dttm`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('2', '1', now(), null, '0', now(), TRUE);

INSERT INTO `officemanager`.`location` (`display`, `abbr_display`, `location_type_cd`, `parent_location_id`, `updt_prsnl_id`,`updt_dttm`, `active_ind`) VALUES ('TEST LOCATION', 'TST', (select code_value from code_value where code_set = 19 and cdf_meaning = 'INVLOC'), NULL, '0', now(), true);
INSERT INTO `officemanager`.`location` (`display`, `abbr_display`, `location_type_cd`, `parent_location_id`, `updt_prsnl_id`,`updt_dttm`, `active_ind`) VALUES ('TEST LOCATION2', 'TST2', (select code_value from code_value where code_set = 19 and cdf_meaning = 'INVLOC'), NULL, '0', now(), true);

INSERT INTO `officemanager`.`part` (`part_name`, `manuf_part_id`, `description`,`amount_sold`,`barcode`,`base_price`,`product_type_cd`,`category_type_cd`,`manufacturer_cd`,`inventory_count`,`inventory_threshold`,`location_id`,`updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('TEST PART', '2342334ADAD-ADSFA', 'DESCRIPTION OF A TESTING PART', '0', NULL, '23.99', (select code_value from code_value where code_set = 6 and display = 'MOWER'), (select code_value from code_value where code_set = 8 and display = 'BLADES'), (select code_value from code_value where code_set = 7 and display = 'SCAG'), '0', '2', (select location_id from location where display = 'TEST LOCATION'), '0', now(), TRUE);
INSERT INTO `officemanager`.`part` (`part_name`, `manuf_part_id`, `description`,`amount_sold`,`barcode`,`base_price`,`product_type_cd`,`category_type_cd`,`manufacturer_cd`,`inventory_count`,`inventory_threshold`,`location_id`,`updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('TEST PART2', '2342334ADAD-ADSFA', 'DESCRIPTION OF A TESTING PART2', '0', NULL, '23.99', (select code_value from code_value where code_set = 6 and display = 'MOWER'), (select code_value from code_value where code_set = 8 and display = 'BLADES'), (select code_value from code_value where code_set = 7 and display = 'SCAG'), '0', '2', (select location_id from location where display = 'TEST LOCATION'), '0', now(), TRUE);

INSERT INTO `officemanager`.`sale` (`parent_entity_name`,`parent_entity_id`,`sale_type_cd`,`amount_off`,`start_dttm`,`end_dttm`,`updt_prsnl_id`,`updt_dttm`,`active_ind`) VALUES ('PART', 1, (select code_value from code_value where code_set = 4 and cdf_meaning = 'SET_PRICE_OFF'), '5.00', now(), null, '0', now(), true);

INSERT INTO `officemanager`.`long_text` (`long_text`, `format_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) values ('some tEXT', (select code_value from code_value where code_set = 11 and cdf_meaning = 'PLAINTEXT'), '0', now(), true);
INSERT INTO `officemanager`.`flat_rate_service` (`description_long_text_id`,`service_name`,`amount_sold`,`barcode`,`base_price`,`product_type_cd`, `category_type_cd`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('1', 'SERVICE NAME', '1', null, '23.99', (select code_value from code_value where code_set = 6 and display = 'MOWER'), (select code_value from code_value where code_set = 9 and display = 'MAINTENANCE'), '0', now(), true);
