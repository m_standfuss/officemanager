﻿CREATE DATABASE IF NOT EXISTS `officemanager`;
USE `officemanager`;
CREATE USER IF NOT EXISTS 'officemanager'@'%' IDENTIFIED BY 'officemanager';
GRANT ALL ON officemanager.* TO 'officemanager'@'%' identified by 'officemanager';
FLUSH PRIVILEGES;

-- MySQL dump 10.13  Distrib 5.6.21, for Win64 (x86_64)
--
-- Host: localhost    Database: officemanager
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL,
  `status_cd` bigint(20) DEFAULT NULL,
  `last_statement_dt_tm` datetime DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`account_id`),
  KEY `client_id` (`client_id`),
  KEY `status_cd` (`status_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `account_ibfk_1` FOREIGN KEY (`status_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `account_ibfk_2` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `account_ibfk_3` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_transaction`
--

DROP TABLE IF EXISTS `account_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_transaction` (
  `account_transaction_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) NOT NULL,
  `payment_id` bigint(20) NOT NULL,
  `transaction_type_cd` bigint(20) DEFAULT NULL,
  `transaction_dttm` datetime NOT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`account_transaction_id`),
  KEY `account_id` (`account_id`),
  KEY `payment_id` (`payment_id`),
  KEY `transaction_type_cd` (`transaction_type_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `account_transaction_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE,
  CONSTRAINT `account_transaction_ibfk_2` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`payment_id`) ON DELETE CASCADE,
  CONSTRAINT `account_transaction_ibfk_3` FOREIGN KEY (`transaction_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `account_transaction_ibfk_4` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `address_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `address_type_cd` bigint(20) DEFAULT NULL,
  `address_1` varchar(70) NULL DEFAULT '',
  `address_2` varchar(70) NULL DEFAULT '',
  `address_3` varchar(70) NULL DEFAULT '',
  `city` varchar(30) NOT NULL DEFAULT '',
  `zip` varchar(20) NOT NULL DEFAULT '',
  `state` varchar(30) NOT NULL DEFAULT '',
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  `primary_address_ind` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`address_id`),
  KEY `city` (`city`),
  KEY `state` (`state`),
  KEY `zip` (`zip`),
  KEY `person_id` (`person_id`),
  KEY `address_type_cd` (`address_type_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `address_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE,
  CONSTRAINT `address_ibfk_2` FOREIGN KEY (`address_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `address_ibfk_3` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `client_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `attention_of` varchar(50) NOT NULL DEFAULT '',
  `promotion_club` bit(1) NOT NULL DEFAULT b'0',
  `tax_exempt` bit(1) NOT NULL DEFAULT b'0',
  `joined_dttm` datetime DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`client_id`),
  KEY `tax_exempt` (`tax_exempt`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `client_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE,
  CONSTRAINT `client_ibfk_3` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `client_product`
--

DROP TABLE IF EXISTS `client_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_product` (
  `client_product_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL,
  `product_manuf` varchar(50) NOT NULL DEFAULT '',
  `product_model` varchar(50) NOT NULL DEFAULT '',
  `product_serial_num` varchar(50) NOT NULL DEFAULT '',
  `product_barcode` varchar(32) NOT NULL DEFAULT '',
  `work_cnt` smallint(11) NOT NULL DEFAULT '0',
  `product_status_cd` bigint(20) DEFAULT NULL,
  `product_type_cd` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`client_product_id`),
  KEY `client_id` (`client_id`),
  KEY `product_status_cd` (`product_status_cd`),
  KEY `product_type_cd` (`product_type_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `client_product_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE CASCADE,
  CONSTRAINT `client_product_ibfk_2` FOREIGN KEY (`product_status_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `client_product_ibfk_3` FOREIGN KEY (`product_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `client_product_ibfk_4` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `client_product_dtl`
--

DROP TABLE IF EXISTS `client_product_dtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_product_dtl` (
  `detail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_product_id` bigint(20) NOT NULL,
  `detail_type_cd` bigint(20) DEFAULT NULL,
  `detail` varchar(50) NOT NULL DEFAULT '',
  `detail_number` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`detail_id`),
  KEY `client_product_id` (`client_product_id`),
  KEY `detail_type_cd` (`detail_type_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `client_product_dtl_ibfk_1` FOREIGN KEY (`client_product_id`) REFERENCES `client_product` (`client_product_id`) ON DELETE CASCADE,
  CONSTRAINT `client_product_dtl_ibfk_2` FOREIGN KEY (`detail_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `client_product_dtl_ibfk_3` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `code_value`
--

DROP TABLE IF EXISTS `code_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code_value` (
  `code_value` bigint(20) NOT NULL AUTO_INCREMENT,
  `code_set` bigint(20) NOT NULL,
  `display` varchar(50) NOT NULL DEFAULT '',
  `cdf_meaning` varchar(25) DEFAULT NULL,
  `collation_seq` smallint(11) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`code_value`),
  KEY `code_set` (`code_set`,`cdf_meaning`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `code_value_ibfk_1` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flat_rate_service`
--

DROP TABLE IF EXISTS `flat_rate_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flat_rate_service` (
  `flat_rate_service_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL DEFAULT '',
  `description_long_text_id` bigint(20) DEFAULT NULL,
  `amount_sold` smallint(11) NOT NULL DEFAULT '0',
  `barcode` varchar(25) DEFAULT '',
  `base_price` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `product_type_cd` bigint(20) DEFAULT NULL,
  `category_type_cd` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`flat_rate_service_id`),
  KEY `barcode` (`barcode`),
  KEY `description_short_ft` (`service_name`) USING BTREE,
  KEY `description_long_text_id` (`description_long_text_id`),
  KEY `product_type_cd` (`product_type_cd`),
  KEY `category_type_cd` (`category_type_cd`),
  CONSTRAINT `flat_rate_service_ibfk_1` FOREIGN KEY (`description_long_text_id`) REFERENCES `long_text` (`long_text_id`) ON DELETE SET NULL,
  CONSTRAINT `flat_rate_service_ibfk_2` FOREIGN KEY (`product_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `flat_rate_service_ibfk_3` FOREIGN KEY (`category_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL, 
  CONSTRAINT `flat_rate_service_ibfk_4` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `location_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `display` varchar(50) NOT NULL DEFAULT '',
  `abbr_display` varchar(5) NOT NULL DEFAULT '',
  `location_type_cd` bigint(20) DEFAULT NULL,
  `parent_location_id` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`location_id`),
  KEY `abbr_display` (`abbr_display`),
  KEY `location_type_cd` (`location_type_cd`),
  KEY `parent_location_id` (`parent_location_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`location_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `location_ibfk_2` FOREIGN KEY (`parent_location_id`) REFERENCES `location` (`location_id`) ON DELETE SET NULL,
  CONSTRAINT `location_ibfk_3` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `long_text`
--

DROP TABLE IF EXISTS `long_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `long_text` (
  `long_text_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `long_text` text,
  `format_cd` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`long_text_id`),
  KEY `format_cd` (`format_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `long_text_ibfk_1` FOREIGN KEY (`format_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `long_text_ibfk_2` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_comment`
--

DROP TABLE IF EXISTS `order_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_comment` (
  `order_comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_comment_type_cd` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) NOT NULL,
  `long_text_id` bigint(20) DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`order_comment_id`),
  KEY `order_comment_type_cd` (`order_comment_type_cd`),
  KEY `order_id` (`order_id`),
  KEY `long_text_id` (`long_text_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `order_comment_ibfk_1` FOREIGN KEY (`order_comment_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `order_comment_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `order_comment_ibfk_3` FOREIGN KEY (`long_text_id`) REFERENCES `long_text` (`long_text_id`) ON DELETE SET NULL,
  CONSTRAINT `order_comment_ibfk_4` FOREIGN KEY (`author_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `order_comment_ibfk_5` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_to_client_product`
--

DROP TABLE IF EXISTS `order_to_client_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_to_client_product` (
  `order_to_client_product_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `client_product_id` bigint(20) NOT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`order_to_client_product_id`),
  KEY `order_id` (`order_id`),
  KEY `client_product_id` (`client_product_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `order_to_client_product_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `order_to_client_product_ibfk_2` FOREIGN KEY (`client_product_id`) REFERENCES `client_product` (`client_product_id`) ON DELETE CASCADE,
  CONSTRAINT `order_to_client_product_ibfk_3` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_to_flat_rate_service`
--

DROP TABLE IF EXISTS `order_to_flat_rate_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_to_flat_rate_service` (
  `order_to_frs_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `flat_rate_service_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) NOT NULL,
  `client_product_id` bigint(20) DEFAULT NULL,
  `service_price` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `on_sale_ind` bit(1) NOT NULL DEFAULT b'0',
  `refunded_ind` bit(1) NOT NULL DEFAULT b'0',
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`order_to_frs_id`),
  KEY `flat_rate_service_id` (`flat_rate_service_id`),
  KEY `order_id` (`order_id`),
  KEY `client_product_id` (`client_product_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `order_to_flat_rate_service_ibfk_1` FOREIGN KEY (`flat_rate_service_id`) REFERENCES `flat_rate_service` (`flat_rate_service_id`) ON DELETE SET NULL,
  CONSTRAINT `order_to_flat_rate_service_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `order_to_flat_rate_service_ibfk_3` FOREIGN KEY (`client_product_id`) REFERENCES `client_product` (`client_product_id`) ON DELETE SET NULL,
  CONSTRAINT `order_to_flat_rate_service_ibfk_4` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_to_part`
--

DROP TABLE IF EXISTS `order_to_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_to_part` (
  `order_to_part_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `part_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` bigint(20) NOT NULL DEFAULT '0',
  `part_price` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_product_id` bigint(20) DEFAULT NULL,
  `on_sale_ind` bit(1) NOT NULL DEFAULT b'0',
  `refunded_ind` bit(1) NOT NULL DEFAULT b'0',
  `taxed_amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `tax_exempt_ind` bit(1) NOT NULL DEFAULT b'0',
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`order_to_part_id`),
  KEY `part_price` (`part_price`) USING BTREE,
  KEY `part_id` (`part_id`),
  KEY `order_id` (`order_id`),
  KEY `client_product_id` (`client_product_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `order_to_part_ibfk_1` FOREIGN KEY (`part_id`) REFERENCES `part` (`part_id`) ON DELETE SET NULL,
  CONSTRAINT `order_to_part_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `order_to_part_ibfk_3` FOREIGN KEY (`client_product_id`) REFERENCES `client_product` (`client_product_id`) ON DELETE SET NULL,
  CONSTRAINT `order_to_part_ibfk_4` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_to_payment`
--

DROP TABLE IF EXISTS `order_to_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_to_payment` (
  `order_to_payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `payment_id` bigint(20) NOT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`order_to_payment_id`),
  KEY `order_id` (`order_id`),
  KEY `payment_id` (`payment_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `order_to_payment_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `order_to_payment_ibfk_2` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`payment_id`) ON DELETE CASCADE,
  CONSTRAINT `order_to_payment_ibfk_3` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) DEFAULT NULL,
  `order_status_cd` bigint(20) DEFAULT NULL,
  `order_type_cd` bigint(20) DEFAULT NULL,
  `created_dttm` datetime DEFAULT NULL,
  `created_prsnl_id` bigint(20) DEFAULT NULL,
  `closed_dttm` datetime DEFAULT NULL,
  `closed_prsnl_id` bigint(20) DEFAULT NULL,
  `parent_order_id` bigint(20) DEFAULT NULL,
  `warranty_comp_id` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`order_id`),
  KEY `created_dttm` (`created_dttm`) USING BTREE,
  KEY `closed_dttm` (`closed_dttm`) USING BTREE,
  KEY `client_id` (`client_id`),
  KEY `order_status_cd` (`order_status_cd`),
  KEY `created_prsnl_id` (`created_prsnl_id`),
  KEY `closed_prsnl_id` (`closed_prsnl_id`),
  KEY `parent_order_id` (`parent_order_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE SET NULL,
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`order_status_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`order_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`created_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `orders_ibfk_5` FOREIGN KEY (`closed_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `orders_ibfk_6` FOREIGN KEY (`parent_order_id`) REFERENCES `orders` (`order_id`) ON DELETE SET NULL,
  CONSTRAINT `orders_ibfk_7` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `part`
--

DROP TABLE IF EXISTS `part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `part` (
  `part_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `part_name` varchar(50) NOT NULL DEFAULT '',
  `manuf_part_id` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(80) NOT NULL DEFAULT '',
  `amount_sold` int(11) NOT NULL DEFAULT '0',
  `barcode` varchar(25) NULL DEFAULT NULL,
  `base_price` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `product_type_cd` bigint(20) DEFAULT NULL,
  `category_type_cd` bigint(20) DEFAULT NULL,
  `manufacturer_cd` bigint(20) DEFAULT NULL,
  `inventory_count` int(11) NOT NULL DEFAULT '0',
  `inventory_threshold` int(11) NOT NULL DEFAULT '0',
  `location_id` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`part_id`),
  KEY `part_name` (`part_name`) USING BTREE,
  KEY `barcode` (`barcode`),
  KEY `product_type_cd` (`product_type_cd`),
  KEY `category_type_cd` (`category_type_cd`),
  KEY `manufacturer_cd` (`manufacturer_cd`),
  KEY `location_id` (`location_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `part_ibfk_1` FOREIGN KEY (`product_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `part_ibfk_2` FOREIGN KEY (`category_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `part_ibfk_3` FOREIGN KEY (`manufacturer_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `part_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE SET NULL,
  CONSTRAINT `part_ibfk_5` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_type_cd` bigint(20) DEFAULT NULL,
  `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `created_dttm` datetime DEFAULT NULL,
  `created_prsnl_id` bigint(20) DEFAULT NULL,
  `payment_nbr` varchar(50) NOT NULL DEFAULT '',
  `payment_desc` varchar(50) NOT NULL DEFAULT '',
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`payment_id`),
  KEY `created_dttm` (`created_dttm`) USING BTREE,
  KEY `payment_type_cd` (`payment_type_cd`),
  KEY `created_prsnl_id` (`created_prsnl_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`payment_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`created_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `payment_ibfk_3` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `permission_name` varchar(55) NOT NULL,
  `display_name` varchar(255) NOT NULL DEFAULT '',
  `category_cd` bigint(20) DEFAULT NULL,  
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`permission_name`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `permission_ibfk_1` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `permission_ibfk_2` FOREIGN KEY (`category_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `person_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name_last` varchar(50) NOT NULL DEFAULT '',
  `name_first` varchar(30) NOT NULL DEFAULT '',
  `name_full_formatted` varchar(80) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`person_id`),
  KEY `name_last` (`name_last`,`name_first`),
  KEY `email` (`email`) USING BTREE,
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `person_ibfk_1` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personnel`
--

DROP TABLE IF EXISTS `personnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personnel` (
  `personnel_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` blob DEFAULT NULL,
  `start_dttm` datetime DEFAULT NULL,
  `locked_out` bit(1) NOT NULL DEFAULT b'0',
  `personnel_status_cd` bigint(20) DEFAULT NULL,
  `terminate_dttm` datetime DEFAULT NULL,
  `w2_on_file` bit(1) NOT NULL DEFAULT b'0',
  `personnel_type_cd` bigint(20) DEFAULT NULL,
  `password_needs_reset` bit(1) NOT NULL DEFAULT b'0',
  `password_last_set` datetime DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`personnel_id`),
  UNIQUE KEY `username` (`username`),
  KEY `personnel_status_cd` (`personnel_status_cd`),
  KEY `personnel_type_cd` (`personnel_type_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `personnel_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE,
  CONSTRAINT `personnel_ibfk_2` FOREIGN KEY (`personnel_status_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `personnel_ibfk_3` FOREIGN KEY (`personnel_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `personnel_ibfk_4` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personnel_role`
--

DROP TABLE IF EXISTS `personnel_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personnel_role` (
  `personnel_role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `personnel_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `begin_effective_dttm` datetime DEFAULT NULL,
  `end_effective_dttm` datetime DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`personnel_role_id`),
  KEY `personnel_id` (`personnel_id`),
  KEY `role_id` (`role_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `personnel_role_ibfk_1` FOREIGN KEY (`personnel_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `personnel_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE SET NULL,
  CONSTRAINT `personnel_role_ibfk_3` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phone`
--

DROP TABLE IF EXISTS `phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phone` (
  `phone_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `phone_type_cd` bigint(20) DEFAULT NULL,
  `phone_number` varchar(20) NOT NULL DEFAULT '',
  `extension` varchar(15) NOT NULL DEFAULT '',
  `primary_phone_ind` bit(1) NOT NULL DEFAULT b'0',
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`phone_id`),
  KEY `phone_number` (`phone_number`,`phone_type_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `phone_ibfk_1` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `phone_ibfk_2` FOREIGN KEY (`phone_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `preference`
--

DROP TABLE IF EXISTS `preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preference` (
  `preference_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pref_personnel_id` int(11) NULL DEFAULT NULL,
  `pref_meaning` varchar(20) NOT NULL,
  `pref_value_text` varchar(255) DEFAULT NULL,
  `pref_value_int` bigint(20) DEFAULT NULL,
  `pref_value_bool` bit(1) DEFAULT b'0',
  `pref_value_decimal` decimal(18,4) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`preference_id`),
  KEY `pref_meaning` (`pref_meaning`,`pref_personnel_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `preference_ibfk_1` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `price_override`
--

DROP TABLE IF EXISTS `price_override`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_override` (
  `price_override_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `parent_entity_name` varchar(25) NOT NULL,
  `parent_entity_id` bigint(20) NOT NULL,
  `override_reason` varchar(255) NOT NULL DEFAULT '',
  `original_amount` decimal(18,4) NOT NULL,
  `override_amount` decimal(18,4) NOT NULL,
  `override_dttm` datetime NOT NULL,
  `override_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`price_override_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `price_override_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `price_override_ibfk_1` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`role_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `role_ibfk_1` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `role_permission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `permission_name` varchar(255) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`role_permission_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  KEY `role_id` (`role_id`),
  KEY `permission_name` (`permission_name`),
  CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL,
  CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE SET NULL,
  CONSTRAINT `role_permission_ibfk_3` FOREIGN KEY (`permission_name`) REFERENCES `permission` (`permission_name`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sale`
--

DROP TABLE IF EXISTS `sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale` (
  `sale_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_entity_name` varchar(25) NOT NULL DEFAULT '',
  `parent_entity_id` bigint(20) NOT NULL,
  `sale_name` varchar(50) NOT NULL DEFAULT '',
  `sale_type_cd` bigint(20) DEFAULT NULL,
  `amount_off` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `start_dttm` datetime DEFAULT NULL,
  `end_dttm` datetime DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`sale_id`),
  KEY `parent_entity_name` (`parent_entity_name`,`parent_entity_id`),
  KEY `sale_type_cd` (`sale_type_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `sale_ibfk_1` FOREIGN KEY (`sale_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `sale_ibfk_2` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `service_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL DEFAULT '',  
  `service_long_text_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) NOT NULL,
  `client_product_id` bigint(20) DEFAULT NULL,
  `hours_qnt` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `price_per_hour` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `refunded_ind` bit(1) NOT NULL DEFAULT b'0',
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`service_id`),
  KEY `service_long_text_id` (`service_long_text_id`),
  KEY `order_id` (`order_id`),
  KEY `client_product_id` (`client_product_id`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `service_ibfk_1` FOREIGN KEY (`service_long_text_id`) REFERENCES `long_text` (`long_text_id`) ON DELETE SET NULL,
  CONSTRAINT `service_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `service_ibfk_3` FOREIGN KEY (`client_product_id`) REFERENCES `client_product` (`client_product_id`) ON DELETE SET NULL,
  CONSTRAINT `service_ibfk_4` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trade_in`
--

DROP TABLE IF EXISTS `trade_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trade_in` (
  `trade_in_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `manufacturer` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `payment_id` bigint(20) DEFAULT NULL,
  `description` varchar(80) NOT NULL DEFAULT '',
  `trade_in_type_cd` bigint(20) DEFAULT NULL,
  `product_type_cd` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `create_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_prsnl_id` bigint(20) DEFAULT NULL,
  `updt_dttm` datetime DEFAULT NULL,
  `active_ind` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`trade_in_id`),
  KEY `payment_id` (`payment_id`),
  KEY `trade_in_type_cd` (`trade_in_type_cd`),
  KEY `product_type_cd` (`product_type_cd`),
  KEY `updt_prsnl_id` (`updt_prsnl_id`),
  CONSTRAINT `trade_in_ibfk_1` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`payment_id`) ON DELETE SET NULL,
  CONSTRAINT `trade_in_ibfk_2` FOREIGN KEY (`trade_in_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `trade_in_ibfk_3` FOREIGN KEY (`product_type_cd`) REFERENCES `code_value` (`code_value`) ON DELETE SET NULL,
  CONSTRAINT `trade_in_ibfk_4` FOREIGN KEY (`updt_prsnl_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

--
-- Inventory Triggers
--
DELIMITER $$

DROP TRIGGER IF EXISTS before_otp_update$$
CREATE TRIGGER before_otp_update
	BEFORE UPDATE ON order_to_part
	FOR EACH ROW 
BEGIN 
	IF (NEW.active_ind = FALSE && OLD.active_ind = TRUE) THEN 
		## If inactivating a row do not allow the quantity to change. 
		SET NEW.quantity = OLD.quantity;
	ELSEIF (OLD.refunded_ind = FALSE && NEW.refunded_ind = TRUE) THEN 
		IF NEW.quantity > OLD.quantity THEN 
			## If refunding do not allow the quanity to increase. 
			SET NEW.quantity = OLD.quantity;
		END IF;
	END IF;
END$$	
	
DROP TRIGGER IF EXISTS after_otp_update$$
CREATE TRIGGER after_otp_update 
    AFTER UPDATE ON order_to_part
    FOR EACH ROW 
BEGIN
	DECLARE diff INTEGER;
	SET diff = 0;
	IF (NEW.active_ind = FALSE && OLD.active_ind = TRUE) THEN 
		## Inactivating return all to inventory.
		SET diff = -1 * OLD.quantity;
	ELSEIF (OLD.refunded_ind = FALSE && NEW.refunded_ind = TRUE) THEN 
		## Refunding return all to inventory. 
		SET diff = -1 * OLD.quantity;
	ELSEIF NEW.quantity != OLD.quantity THEN 
		## Updating quantity update inventory.
		SET diff = NEW.quantity - OLD.quantity;
	END IF;
	
	IF diff != 0 THEN 
		UPDATE part p SET p.inventory_count = p.inventory_count - diff WHERE p.part_id = NEW.part_id;
	END IF;
END$$

DROP TRIGGER IF EXISTS after_otp_insert$$
CREATE TRIGGER after_otp_insert
	AFTER INSERT ON order_to_part
	FOR EACH ROW
BEGIN
	IF (NEW.active_ind = TRUE && NEW.refunded_ind = FALSE) THEN 
		UPDATE part p SET p.inventory_count = p.inventory_count - NEW.quantity WHERE p.part_id = NEW.part_id;
	END IF;
END$$

DROP TRIGGER IF EXISTS after_otp_delete$$
CREATE TRIGGER after_otp_delete
	AFTER DELETE ON order_to_part
	FOR EACH ROW 
BEGIN 
	IF (OLD.active_ind = TRUE && OLD.refunded_ind = FALSE) THEN 
		UPDATE part p SET p.inventory_count = p.inventory_count - OLD.quantity WHERE p.part_id = OLD.part_id;
	END IF;
END$$ 
DELIMITER ;


-- Dump completed on 2015-06-09 20:42:53

SET @OLD_SQL_MODE=@@SQL_MODE;
SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
SET FOREIGN_KEY_CHECKS = 0;
INSERT INTO `officemanager`.`person` (`person_id`, `name_last`, `name_first`, `name_full_formatted`, `email`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES  ('0', 'OFFICEMANAGER', 'OFFICEMANAGER', 'OFFICEMANAGER', '', '0', now(), FALSE);
INSERT INTO `officemanager`.`personnel` (`personnel_id`, `person_id`, `username`, `start_dttm`, `locked_out`, `personnel_status_cd`, `terminate_dttm`, `w2_on_file`, `personnel_type_cd`, `password_needs_reset`, `password_last_set`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES ('0', '0', 'officemanager', now(), false, '0', null, TRUE, 0, FALSE, NOW(), '0', NOW(), FALSE);
INSERT INTO `officemanager`.`code_value` (`code_value`, `code_set`, `display`, `cdf_meaning`, `collation_seq`, `updt_prsnl_id`, `updt_dttm`, `active_ind`) VALUES (0, 0, '', '', null, '0', now(), true);
SET FOREIGN_KEY_CHECKS = 1;
SET SQL_MODE=@OLD_SQL_MODE