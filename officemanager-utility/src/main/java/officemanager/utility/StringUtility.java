package officemanager.utility;

import java.util.List;
import java.util.stream.Collectors;

public class StringUtility {

	public static final String ABBR_STRING = "...";

	public static String joinStringsByComma(List<String> list) {
		return list.stream().collect(Collectors.joining(", "));
	}

	public static boolean isNullOrWhiteSpace(String s) {
		return s == null || s.trim().isEmpty();
	}

	public static String abbreviate(String text, int maxChars) {
		if (maxChars < ABBR_STRING.length()) {
			throw new IllegalArgumentException("The max characters has to be greater than the abbrevation string.");
		}
		if (text == null) {
			return null;
		}
		if (text.length() <= maxChars) {
			return text;
		}

		return text.substring(0, maxChars - ABBR_STRING.length()) + ABBR_STRING;
	}
}
