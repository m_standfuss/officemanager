package officemanager.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import com.google.common.collect.Maps;

public class DateFormatter {

	private static final Map<TimeZone, DateFormatter> instanceMap = Maps.newHashMap();

	public static final String FULL_DATE_FORMAT = "MM/dd/yyyy hh:mm a";
	public static final String DATE_FORMAT = "MM/dd/yyyy";

	public static DateFormatter getInstance() {
		return getInstance(TimeZone.getDefault());
	}

	public static DateFormatter getInstance(TimeZone timeZone) {
		synchronized (instanceMap) {
			DateFormatter instance = instanceMap.get(timeZone);
			if (instance == null) {
				instance = new DateFormatter(timeZone);
				instanceMap.put(timeZone, instance);
			}
			return instance;
		}
	}

	private final TimeZone timeZone;

	private DateFormatter(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * Formats a Date as the full text version of the date default value is
	 * MM/dd/yyyy hh:mm a. Returns an empty string if the date is null.
	 * 
	 * @param date
	 *            The date to format.
	 * @return The formatted date.
	 */
	public String formatFullDate_DB(Date date) {
		if (date == null) {
			return "";
		}
		int offset = timeZone.getOffset(date.getTime());
		date = new Date(date.getTime() + offset);
		final SimpleDateFormat format = new SimpleDateFormat(FULL_DATE_FORMAT);
		format.setTimeZone(timeZone);
		return format.format(date);
	}

	public String formatFullDate(Date date) {
		if (date == null) {
			return "";
		}
		final SimpleDateFormat format = new SimpleDateFormat(FULL_DATE_FORMAT);
		format.setTimeZone(timeZone);
		return format.format(date);
	}

	/**
	 * Formats a Date as the date text version of the date default value is
	 * MM/dd/yyyy. Returns an empty string if the date is null.
	 * 
	 * @param date
	 *            The date to format.
	 * @return The formatted date.
	 */
	public String formatDate_DB(Date date) {
		if (date == null) {
			return "";
		}
		int offset = timeZone.getOffset(date.getTime());
		date = new Date(date.getTime() + offset);
		final SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		format.setTimeZone(timeZone);
		return format.format(date);
	}

	public String formatDate(Date date) {
		if (date == null) {
			return "";
		}
		final SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		format.setTimeZone(timeZone);
		return format.format(date);
	}
}
