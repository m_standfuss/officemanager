package officemanager.utility;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtility {

	public static Date addDays(Date fromDate, int numberOfDays) {
		Calendar c = Calendar.getInstance();
		c.setTime(fromDate);
		c.set(Calendar.DATE, numberOfDays);
		return c.getTime();
	}

	public static Date toDate(LocalDateTime localDate, ZoneId zoneId) {
		ZonedDateTime zonedDateTime = ZonedDateTime.of(localDate, zoneId);
		return toDate(zonedDateTime);
	}

	public static Date toDate(ZonedDateTime zonedDateTime) {
		Instant instant = zonedDateTime.toInstant();
		return Date.from(instant);

	}

	public static Date convertToLocalTime(Date utcTime, TimeZone localTimeZone) {
		long newTime = utcTime.getTime() + localTimeZone.getOffset(utcTime.getTime());
		return new Date(newTime);
	}
}
