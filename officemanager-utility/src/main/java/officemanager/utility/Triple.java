package officemanager.utility;

public class Triple<X, Y, Z> {

	public static <X, Y, Z> Triple<X, Y, Z> newTriple(X item1, Y item2, Z item3) {
		return new Triple<X, Y, Z>(item1, item2, item3);
	}

	public final X item1;
	public final Y item2;
	public final Z item3;

	public Triple(X item1, Y item2, Z item3) {
		this.item1 = item1;
		this.item2 = item2;
		this.item3 = item3;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item1 == null) ? 0 : item1.hashCode());
		result = prime * result + ((item2 == null) ? 0 : item2.hashCode());
		result = prime * result + ((item3 == null) ? 0 : item3.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Triple)) {
			return false;
		}
		@SuppressWarnings("rawtypes")
		Triple other = (Triple) obj;
		if (item1 == null) {
			if (other.item1 != null) {
				return false;
			}
		} else if (!item1.equals(other.item1)) {
			return false;
		}
		if (item2 == null) {
			if (other.item2 != null) {
				return false;
			}
		} else if (!item2.equals(other.item2)) {
			return false;
		}
		if (item3 == null) {
			if (other.item3 != null) {
				return false;
			}
		} else if (!item3.equals(other.item3)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Triple [item1=").append(item1).append(", item2=").append(item2).append(", item3=").append(item3)
		.append("]");
		return builder.toString();
	}
}