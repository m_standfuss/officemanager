package officemanager.utility;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

/**
 * Tuple class to make it easier to dynamically return two connected values of
 * potentially different types without creating a class just for that.
 *
 * @param <X>
 *            The Type of the first object of the tuple.
 * @param <Y>
 *            The Type of the second object of the tuple.
 */
public class Tuple<X, Y> {

	public static <S, T> Tuple<S, T> newTuple(S item1, T item2) {
		return new Tuple<S, T>(item1, item2);
	}

	public static <S, T> List<Tuple<S, T>> toTuples(Map<S, T> map) {
		List<Tuple<S, T>> list = Lists.newArrayList();
		for (Map.Entry<S, T> entry : map.entrySet()) {
			list.add(newTuple(entry.getKey(), entry.getValue()));
		}
		return list;
	}

	public final X item1;
	public final Y item2;

	public Tuple(X item1, Y item2) {
		this.item1 = item1;
		this.item2 = item2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (item1 == null ? 0 : item1.hashCode());
		result = prime * result + (item2 == null ? 0 : item2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Tuple)) {
			return false;
		}
		@SuppressWarnings("rawtypes")
		final Tuple other = (Tuple) obj;
		if (item1 == null) {
			if (other.item1 != null) {
				return false;
			}
		} else if (!item1.equals(other.item1)) {
			return false;
		}
		if (item2 == null) {
			if (other.item2 != null) {
				return false;
			}
		} else if (!item2.equals(other.item2)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Tuple [item1=" + item1 + ", item2=" + item2 + "]";
	}
}
