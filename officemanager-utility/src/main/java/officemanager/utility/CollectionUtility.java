package officemanager.utility;

import java.util.Collection;

import com.google.common.collect.Lists;

public class CollectionUtility {

	public static <T> Collection<T> intersectCollections(Collection<? extends Collection<T>> collectionOfCollections) {
		if (collectionOfCollections == null) {
			throw new IllegalArgumentException("The collection of collections cannot be null.");
		}
		if (collectionOfCollections.isEmpty()) {
			return Lists.newArrayList();
		}

		Collection<T> masterList = null;
		boolean firstCollection = true;
		for (Collection<T> collection : collectionOfCollections) {
			if (firstCollection) {
				firstCollection = false;
				masterList = collection;
			} else {
				masterList.retainAll(collection);
			}
		}
		return masterList;
	}

	public static <T> boolean isNullOrEmpty(Collection<T> collection) {
		return collection == null || collection.isEmpty();
	}

}
