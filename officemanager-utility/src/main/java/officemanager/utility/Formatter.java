package officemanager.utility;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

public class Formatter {
	private static final Logger logger = LogManager.getLogger(Formatter.class);

	private static final int PRSNL_NUM_OFFSET = 10;
	private static final int CLIENT_NUM_OFFSET = 123;
	private static final int ORDER_NUM_OFFSET = 5555;
	private static final int ACCOUNT_NUM_OFFSET = 8849;

	private static final Formatter instance = new Formatter();

	public static Formatter getInstance() {
		return instance;
	}

	private Formatter() {}

	/**
	 * Formats a BigDecimal into a displayable currency value.
	 * 
	 * @param amount
	 *            The amount to format.
	 * @return The formatted currency.
	 */
	public String formatMoney(BigDecimal amount) {
		if (amount == null) {
			return "";
		}
		return NumberFormat.getCurrencyInstance().format(amount);
	}

	/**
	 * Formats a BigDecimal into a displayable percentage value.
	 * 
	 * @param percentage
	 *            The percentage to format.
	 * @return The formatted percentage.
	 */
	public String formatPercentage(BigDecimal percentage) {
		if (percentage == null) {
			return "";
		}
		return NumberFormat.getPercentInstance().format(percentage.doubleValue() / 100);
	}

	public String formatDecimal(BigDecimal amount, int decimals) {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(decimals);

		return df.format(amount);
	}

	/**
	 * Applies the offset to the order id to get the displayable order number.
	 * 
	 * @param orderId
	 *            The order id to format.
	 * @return The order number.
	 */
	public String formatOrderNumber(Long orderId) {
		if (orderId == null) {
			return "";
		}
		return formatOffsetNumber(orderId, ORDER_NUM_OFFSET);
	}

	/**
	 * Unapplies the offset to the order number to get the order id.
	 * 
	 * @param orderNumber
	 *            The order number.
	 * @return The order id, null if cannot be converted.
	 */
	public Long unformatOrderNumber(String orderNumber) {
		if (orderNumber == null || orderNumber.isEmpty()) {
			return null;
		}
		return unformatOffsetNumber(orderNumber, ORDER_NUM_OFFSET);
	}

	/**
	 * Applies the offset to the personnel id to get the displayable personnel
	 * number.
	 * 
	 * @param prsnlId
	 *            The personnel id to format.
	 * @return The personnel number.
	 */
	public String formatPersonnelNumber(Long prsnlId) {
		if (prsnlId == null) {
			return "";
		}
		return formatOffsetNumber(prsnlId, PRSNL_NUM_OFFSET);
	}

	/**
	 * Applies the offset to the client id to get the displayable client number.
	 * 
	 * @param clientId
	 *            The client id to format.
	 * @return The client number.
	 */
	public String formatClientNumber(Long clientId) {
		if (clientId == null) {
			return "";
		}
		return formatOffsetNumber(clientId, CLIENT_NUM_OFFSET);
	}

	/**
	 * Applies the offset to the account id to get the displayable account
	 * number.
	 * 
	 * @param accountId
	 *            The account id to format.
	 * @return The account number.
	 */
	public String formatAccountNumber(Long accountId) {
		if (accountId == null) {
			return "";
		}
		return formatOffsetNumber(accountId, ACCOUNT_NUM_OFFSET);
	}

	/**
	 * Unapplies the offset to the account number to get the account id.
	 * 
	 * @param accountNumber
	 *            The account number.
	 * @return The account id, null if cannot be converted.
	 */
	public Long unformatAccountNumber(String accountNumber) {
		if (accountNumber == null || accountNumber.isEmpty()) {
			return null;
		}
		return unformatOffsetNumber(accountNumber, ACCOUNT_NUM_OFFSET);
	}

	/**
	 * Formats a string of numbers into a displayable phone number.
	 * 
	 * @param phoneNumber
	 *            The phone number to format.
	 * @return The phone number formatted.
	 */
	public String formatPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
			return "";
		}
		phoneNumber = stripCharacters(phoneNumber, Lists.newArrayList(" ", "(", ")", "-"));
		if (phoneNumber.length() < 8) {
			return formatPhoneWithoutAreaCode(phoneNumber);
		}
		return formatPhoneWithAreaCode(phoneNumber);
	}

	/**
	 * Formats a phone record into the full phone number (including extensions
	 * etc).
	 * 
	 * @param phone
	 *            The phone number to format.
	 * @return The formatted phone number.
	 */
	public String formatPhoneNumber(String phoneNumber, String extension) {
		String phoneNumberFormatted = formatPhoneNumber(phoneNumber);
		if (StringUtility.isNullOrWhiteSpace(extension)) {
			return phoneNumberFormatted;
		}
		return phoneNumberFormatted + " x" + extension;
	}

	/**
	 * Gets the number of characters added the formatted string between the
	 * start of the string and the current position of the string.
	 * 
	 * </br>
	 * Great for seamlessly moving the caret of a text field after user input.
	 * 
	 * @param originalString
	 *            The string pre formatted.
	 * @param currentPosition
	 *            The current position on the string.
	 * @return The number of characters added.
	 */
	public int getAddedCharactersAfter_Phone(String originalString, int currentPosition) {
		if (originalString.length() < 4) {
			return 0;
		}
		if (originalString.length() < 8) {
			if (originalString.length() > 3) {
				return currentPosition > 3 ? 1 : 0;
			}
		}
		if (originalString.length() < 11) {
			if (currentPosition < 4) {
				return 1;
			} else if (currentPosition < 6) {
				return 2;
			}
			return 3;
		}
		if (currentPosition < 4) {
			return 1;
		} else if (currentPosition < 6) {
			return 2;
		} else if (currentPosition < 11) {
			return 3;
		} else {
			return 4;
		}
	}

	/**
	 * Formats an address into a single string with corresponding new lines in
	 * correct places. The city state and zip code will be placed in the correct
	 * format as well and removed if they do not exist.
	 * 
	 * @param addressLines
	 *            The address lines (typically just a single line, but can be
	 *            multiple if for example an apartment building).
	 * @param city
	 *            The city, if null or empty will be removed from the
	 *            formatting.
	 * @param state
	 *            The state, if null or empty will be removed from the
	 *            formatting.
	 * @param zip
	 *            The zipcode, if null or empty will be removed from the
	 *            formatting.
	 * @return The formatted string.
	 */
	public String formatAddress(List<String> addressLines, String city, String state, String zip) {
		StringBuilder b = new StringBuilder();
		if (addressLines != null) {
			String NEW_LINE = "";
			for (String addressLine : addressLines) {
				if (!StringUtility.isNullOrWhiteSpace(addressLine)) {
					b.append(NEW_LINE).append(addressLine);
					NEW_LINE = System.lineSeparator();
				}
			}
		}
		boolean hasCityStateZipLine = !(StringUtility.isNullOrWhiteSpace(city)
				&& StringUtility.isNullOrWhiteSpace(state) && StringUtility.isNullOrWhiteSpace(zip));
		if (hasCityStateZipLine) {
			b.append(System.lineSeparator());
		}

		String SPACE = "";
		for (String value : Lists.newArrayList(city, state, zip)) {
			if (!StringUtility.isNullOrWhiteSpace(value)) {
				b.append(SPACE).append(value);
				SPACE = " ";
			}
		}
		return b.toString();
	}

	public List<String> formatAddressStrings(List<String> addressLines, String city, String state, String zip) {
		List<String> lines = Lists.newArrayList();
		if (addressLines != null) {
			for (String addrLine : addressLines) {
				if (!StringUtility.isNullOrWhiteSpace(addrLine)) {
					lines.add(addrLine);
				}
			}
		}

		StringBuilder b = new StringBuilder();
		String SPACE = "";
		for (String value : Lists.newArrayList(city, state, zip)) {
			if (!StringUtility.isNullOrWhiteSpace(value)) {
				b.append(SPACE).append(value);
				SPACE = " ";
			}
		}
		if (b.length() > 0) {
			lines.add(b.toString());
		}
		return lines;
	}

	public String formatClientProductDisplay(String productManufacturer, String productModel, String serialNumber) {
		final StringBuilder b = new StringBuilder();

		String HYPHEN = "";
		if (!StringUtility.isNullOrWhiteSpace(productManufacturer)) {
			b.append(productManufacturer);
			HYPHEN = "-";
		}
		if (!StringUtility.isNullOrWhiteSpace(productModel)) {
			b.append(HYPHEN).append(productModel);
			HYPHEN = "";
		}
		if (!StringUtility.isNullOrWhiteSpace(serialNumber)) {
			b.append(HYPHEN).append('(').append(serialNumber).append(')');
		}
		return b.toString();
	}

	private String formatOffsetNumber(Long number, int offset) {
		final int MINIMUM_DIGITS = 6;
		final StringBuilder builder = new StringBuilder(String.valueOf(number + offset));
		while (builder.length() < MINIMUM_DIGITS) {
			builder.insert(0, "0");
		}
		return builder.toString();
	}

	private Long unformatOffsetNumber(String formattedNumber, int offset) {
		Long id;
		try {
			id = Long.parseLong(formattedNumber);
		} catch (final NumberFormatException e) {
			logger.error("Unable to parse the formatted number " + formattedNumber, e);
			return null;
		}
		return id - offset;
	}

	private String formatPhoneWithAreaCode(String phoneNumber) {
		if (phoneNumber.length() < 8) {
			throw new IllegalArgumentException(
					"The phone number has to be over 7 characters to format as with area code.");
		}
		String toRtrn;
		if (phoneNumber.length() < 11) {
			toRtrn = "(" + phoneNumber.substring(0, 3) + ")" + phoneNumber.substring(3, 6) + "-"
					+ phoneNumber.substring(6);
		} else {
			toRtrn = phoneNumber.substring(0, 1) + '-' + phoneNumber.substring(1, 4) + '-' + phoneNumber.substring(4, 7)
					+ "-" + phoneNumber.substring(7, 11);
			if (phoneNumber.length() > 11) {
				phoneNumber += " " + phoneNumber.substring(11);
			}
		}
		return toRtrn;
	}

	private String formatPhoneWithoutAreaCode(String phoneNumber) {
		if (phoneNumber.length() > 3) {
			return phoneNumber.substring(0, 3) + "-" + phoneNumber.substring(3);
		}
		return phoneNumber;
	}

	private String stripCharacters(String string, ArrayList<String> replaceCharacters) {
		StringBuilder sb = new StringBuilder("[");
		for (String s : replaceCharacters) {
			sb.append(s);
		}
		sb.append(']');
		return string.replaceAll(sb.toString(), "");
	}

	public String substring(String text, int length) {
		if (text.length() <= length) {
			return text;
		}
		return text.substring(0, length) + "...";
	}
}
