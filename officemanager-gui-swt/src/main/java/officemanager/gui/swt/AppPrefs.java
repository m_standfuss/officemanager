package officemanager.gui.swt;

import java.math.BigDecimal;
import java.util.List;
import java.util.TimeZone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;

import com.google.common.collect.Lists;

import officemanager.biz.records.UserSecurityRecord;
import officemanager.biz.records.UserSessionRecord;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.DateFormatter;
import officemanager.utility.Formatter;

public class AppPrefs {

	public static final int ICN_SIZE_CLPCMP = 20;
	public static final int ICN_SIZE_SIDEBAR = 20;
	public static final int ICN_SIZE_WIZARD_HEADER = 64;
	public static final int ICN_SIZE_VIEW = 16;
	public static final int ICN_SIZE_PAGETOOLITEM = 32;
	public static final int ICN_SIZE_MAINTOOLBAR = 32;

	public static final Color COLOR_DISCOUNT_PRICE = ResourceManager.getColor(SWT.COLOR_RED);
	public static final Color COLOR_NEGATIVE_PRICE = ResourceManager.getColor(SWT.COLOR_RED);

	public static final Color COLOR_BREADCRUMB = ResourceManager.getColor(244, 244, 244);
	public static final Color COLOR_BREADCRUMB_GRADIENT = ResourceManager.getColor(225, 225, 225);
	public static final Color COLOR_BREADCRUMB_SELECTED = ResourceManager.getColor(0, 146, 224);
	public static final Color COLOR_BREADCRUMB_TEXT = ResourceManager.getColor(SWT.COLOR_BLACK);
	public static final Color COLOR_BREADCRUMB_SELECTED_TEXT = ResourceManager.getColor(SWT.COLOR_WHITE);

	public static final Color COLOR_SIDEBAR = ResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW);
	public static final Color COLOR_SIDEBAR_TEXT = ResourceManager.getColor(SWT.COLOR_BLACK);
	public static final Color COLOR_SIDEBAR_SELECTED = ResourceManager.getColor(0, 146, 224);
	public static final Color COLOR_SIDEBAR_SELECTED_TEXT = ResourceManager.getColor(SWT.COLOR_WHITE);

	public static final Color COLOR_ORDER_HEADER = ResourceManager.getColor(67, 74, 77);
	public static final Color COLOR_ORDER_HEADER_TEXT = ResourceManager.getColor(SWT.COLOR_WHITE);

	public static final Color COLOR_COLLAPSIBLE_HEADER = ResourceManager.getColor(134, 138, 140);
	public static final Color COLOR_COLLAPSIBLE_HEADER_TEXT = ResourceManager.getColor(SWT.COLOR_WHITE);

	public static final Color COLOR_ROW = ResourceManager.getColor(SWT.COLOR_WHITE);
	public static final Color COLOR_OFFSET_ROW = ResourceManager.getColor(244, 244, 244);

	public static final Formatter FORMATTER = Formatter.getInstance();

	public static DateFormatter dateFormatter = DateFormatter.getInstance();

	public static TimeZone localTimeZone;
	public static UserSessionRecord currentSession;
	public static UserSecurityRecord currentSessionPermissions;

	public static BigDecimal hourlyRate = new BigDecimal("75.00");
	public static BigDecimal hourlyRateDiscounted = new BigDecimal("60.00");

	public static String companyAddress = "";
	public static String companyPhone = "";
	public static String companyWebsite = "";

	public static List<Long> claimTicketCommentTypes = Lists.newArrayList();
	public static List<Long> shopTicketCommentTypes = Lists.newArrayList();
	public static List<Long> orderRecieptCommentTypes = Lists.newArrayList();

	public static String bidComment = "Bids are honored for up to 30 days after creation.";

	public static boolean hasPermission(String permission) {
		return currentSessionPermissions.hasPermission(permission);
	}
}
