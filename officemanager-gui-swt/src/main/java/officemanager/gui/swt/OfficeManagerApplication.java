package officemanager.gui.swt;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.mihalis.opal.breadcrumb.BreadcrumbItem;

import com.google.common.collect.Lists;

import officemanager.gui.swt.sidebaritems.AccountSearchSideItem;
import officemanager.gui.swt.sidebaritems.ApplicationSecuritySideItem;
import officemanager.gui.swt.sidebaritems.ClientSearchSideItem;
import officemanager.gui.swt.sidebaritems.ClosedOrdersSideItem;
import officemanager.gui.swt.sidebaritems.FlatRateSearchSideItem;
import officemanager.gui.swt.sidebaritems.ISideBarItem;
import officemanager.gui.swt.sidebaritems.LocationsSideItem;
import officemanager.gui.swt.sidebaritems.OpenOrdersSideItem;
import officemanager.gui.swt.sidebaritems.PartSearchSideItem;
import officemanager.gui.swt.sidebaritems.PersonnelHomeSideItem;
import officemanager.gui.swt.sidebaritems.PersonnelSearchSideItem;
import officemanager.gui.swt.sidebaritems.PreferencesSideItem;
import officemanager.gui.swt.sidebaritems.ReportingSideItem;
import officemanager.gui.swt.sidebaritems.SaleSearchSideItem;
import officemanager.gui.swt.sidebaritems.SideBarView;
import officemanager.gui.swt.sidebaritems.SideBarWizard;
import officemanager.gui.swt.ui.composites.FrameworkComposite;
import officemanager.gui.swt.ui.dialogs.AboutDialog;
import officemanager.gui.swt.ui.dialogs.HelpDialog;
import officemanager.gui.swt.ui.widgets.SideBarItem;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizard;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.IRefreshableView;
import officemanager.gui.swt.views.ISaveableView;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.gui.swt.views.listeners.ChangeViewListener;
import officemanager.gui.swt.views.listeners.ChangeViewNameListener;
import officemanager.gui.swt.views.listeners.SaveStateChangedListener;

public class OfficeManagerApplication {

	private static final int SIDE_BAR_WIDTH = 200;
	public static final int EXIT_OTHER = 0;
	public static final int EXIT_QUIT = 1;
	public static final int EXIT_LOGOUT = 2;

	private static final Logger logger = LogManager.getLogger(OfficeManagerApplication.class);

	private static final String DATA_SIDE_BAR_ITEM = "DATA_SIDE_BAR_ITEM";
	private static final String DATA_SIDE_BAR_COMPOSITE = "DATA_SIDE_BAR_COMPOSITE";
	private static final String DATA_VIEW = "DATA_VIEW";
	private static final String DATA_COMPOSITE = "DATA_COMPOSITE";
	private static final String DATA_DISPOSE_ON_CLOSE = "DATA_DISPOSE_ON_CLOSE";
	private static final String DATA_SAVE_STATE = "DATA_SAVE_STATE";
	private static final String DATA_SELECTED = "DATA_SELECTED";

	private final Shell shell;
	private final FrameworkComposite composite;

	private IOfficeManagerView currentView;
	private int exitCode;

	public OfficeManagerApplication(Shell shell) {
		this.shell = shell;
		shell.setLayout(new FillLayout());
		composite = new FrameworkComposite(shell, SWT.NONE);
		addSideBarElements();
		addListeners();

		composite.layout();
		composite.redraw();
		composite.update();
		exitCode = EXIT_OTHER;
	}

	public int getExitStatus() {
		return exitCode;
	}

	private void addSideBarElements() {
		logger.debug("Adding the personnel home side bar.");
		SideBarItem homeItem = addSideBar(new PersonnelHomeSideItem());

		List<ISideBarItem> sideItems = Lists.newArrayList();
		if (AppPrefs.hasPermission(AppPermissions.SEARCH_OPEN_ORDERS)) {
			logger.debug("Adding the open order search side bar element.");
			sideItems.add(new OpenOrdersSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.SEARCH_CLOSED_ORDERS)) {
			logger.debug("Adding closed order search side bar element.");
			sideItems.add(new ClosedOrdersSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.SEARCH_PARTS)) {
			logger.debug("Adding closed order search side bar element.");
			sideItems.add(new PartSearchSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.SEARCH_SALE)) {
			logger.debug("Adding Sale Search side bar element.");
			sideItems.add(new SaleSearchSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.SEARCH_FLAT_RATES)) {
			logger.debug("Adding Flat Rate Search side bar element.");
			sideItems.add(new FlatRateSearchSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.SEARCH_CLIENTS)) {
			logger.debug("Adding Client Search side bar element.");
			sideItems.add(new ClientSearchSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.SEARCH_ACCOUNTS)) {
			logger.debug("Adding Account Search side bar element.");
			sideItems.add(new AccountSearchSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.SEARCH_PERSONNEL)) {
			logger.debug("Adding personnel search side bar element.");
			sideItems.add(new PersonnelSearchSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.SECURITY_VIEW)) {
			logger.debug("Adding application security side bar element.");
			sideItems.add(new ApplicationSecuritySideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.PREFERENCES_VIEW)) {
			logger.debug("Adding application preferences side bar element.");
			sideItems.add(new PreferencesSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.REPORTS_VIEW)) {
			logger.debug("Adding reporting side bar element.");
			sideItems.add(new ReportingSideItem());
		}
		if (AppPrefs.hasPermission(AppPermissions.LOCATIONS_VIEW)) {
			logger.debug("Adding locations side bar element.");
			sideItems.add(new LocationsSideItem());
		}
		Collections.sort(sideItems, (i1, i2) -> i1.getSideBarName().compareTo(i2.getSideBarName()));
		for (ISideBarItem item : sideItems) {
			addSideBar(item);
		}
		composite.scrCompositeSideBar.setMinSize(composite.compositeSideBar.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		selectSideBar(homeItem);
	}

	private void addListeners() {
		composite.tltmSaveItem.addListener(SWT.Selection, l -> {
			if (currentView instanceof ISaveableView) {
				((ISaveableView) currentView).saveView();
			} else {
				logger.error("The current view does not implement the saveable interface.");
			}
		});

		composite.tltmRefresh.addListener(SWT.Selection, l -> {
			if (currentView instanceof IRefreshableView) {
				BusyIndicator.showWhile(Display.getDefault(), () -> {
					((IRefreshableView) currentView).refresh();
				});
			} else {
				logger.error("The current view does not implement the refreshable interface.");
			}
		});

		composite.logoutMenuItem.addListener(SWT.Selection, l -> {
			exitCode = EXIT_LOGOUT;
			shell.dispose();
		});

		composite.exitMenuItem.addListener(SWT.Selection, l -> {
			exitCode = EXIT_QUIT;
			shell.dispose();
		});

		composite.helpContentItem.addListener(SWT.Selection, l -> {
			HelpDialog dialog = new HelpDialog(shell, SWT.NONE);
			dialog.open();
		});

		composite.aboutMenuItem.addListener(SWT.Selection, e -> {
			AboutDialog dialog = new AboutDialog(shell, SWT.NONE);
			dialog.open();
		});
	}

	private SideBarItem addSideBar(ISideBarItem sideBarItem) {
		SideBarItem sideBar = new SideBarItem(composite.compositeSideBar, SWT.NONE);
		sideBar.setLayoutData(new RowData(SIDE_BAR_WIDTH, 35));

		sideBar.setMessageText(sideBarItem.getSideBarName());
		sideBar.setIcon(sideBarItem.getSideBarIcon());
		sideBar.setData(DATA_SIDE_BAR_ITEM, sideBarItem);
		sideBar.addSelectionListener(e -> selectSideBar(sideBar));

		return sideBar;
	}

	private void selectSideBar(SideBarItem sideBar) {

		ISideBarItem sideBarItem = (ISideBarItem) sideBar.getData(DATA_SIDE_BAR_ITEM);

		if (sideBarItem instanceof SideBarView) {
			// unselect all other side bars.
			for (Control child : composite.compositeSideBar.getChildren()) {
				if (child instanceof SideBarItem) {
					((SideBarItem) child).setSelected(sideBar == child);
				}
			}
			IOfficeManagerView view = ((SideBarView) sideBarItem).getView();
			clearBreadcrumbs();
			view.clearListeners();

			if (sideBar.getData(DATA_SIDE_BAR_COMPOSITE) == null) {
				sideBar.setData(DATA_SIDE_BAR_COMPOSITE,
						((SideBarView) sideBarItem).getView().makeComposite(composite.compositeMainStage, SWT.NONE));
			}
			BreadcrumbItem breadcrumbItem = makeBreadCrumb(view, (Composite) sideBar.getData(DATA_SIDE_BAR_COMPOSITE));
			selectBreadCrumb(breadcrumbItem);
		} else if (sideBarItem instanceof SideBarWizard) {
			SideBarItem prevItem = null;
			for (Control child : composite.compositeSideBar.getChildren()) {
				if (child instanceof SideBarItem && ((SideBarItem) child).isSelected()) {
					if (child != sideBar) {
						prevItem = (SideBarItem) child;
					}
					((SideBarItem) child).setSelected(false);
				}
			}
			OfficeManagerWizard wizard = ((SideBarWizard) sideBarItem).getWizard();
			OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(shell, wizard);
			dialog.setBlockOnOpen(true);
			if (dialog.open() == Dialog.OK) {
				OfficeManagerView returnView = wizard.getReturnView();
				if (returnView == null && prevItem != null) {
					for (Control child : composite.compositeSideBar.getChildren()) {
						if (child == prevItem) {
							((SideBarItem) child).setSelected(true);
						}
					}
				} else {
					clearBreadcrumbs();
					BreadcrumbItem breadcrumbItem = makeBreadCrumb(returnView);
					selectBreadCrumb(breadcrumbItem);
				}
			} else {
				for (Control child : composite.compositeSideBar.getChildren()) {
					if (child == prevItem) {
						((SideBarItem) child).setSelected(true);
						composite.compositeSideBar.redraw();
					}
				}
			}
		}
	}

	private void clearBreadcrumbs() {
		for (BreadcrumbItem item : composite.breadcrumb.getItems()) {
			Boolean dispose = (Boolean) item.getData(DATA_DISPOSE_ON_CLOSE);
			if (dispose == null || dispose) {
				((IOfficeManagerView) item.getData(DATA_VIEW)).closeView();
			}
		}
		composite.breadcrumb.dispose();
		composite.recreateBreadcrumb();
	}

	private void clearBreadcrumbsAfterSelected() {
		int selectedIndex = getSelectedBreadcrumbIndex();

		if (selectedIndex == composite.breadcrumb.getItemCount() - 1) {
			return;
		}

		for (int i = composite.breadcrumb.getItemCount() - 1; i > selectedIndex; i--) {
			BreadcrumbItem item = composite.breadcrumb.getItem(i);
			composite.breadcrumb.removeItem(item);
			item.dispose();
		}
		composite.compositeBreadcrumb.layout();
	}

	private int getSelectedBreadcrumbIndex() {
		int selectedIndex = -1;
		for (int i = 0; i < composite.breadcrumb.getItemCount(); i++) {
			BreadcrumbItem item = composite.breadcrumb.getItem(i);
			if (item.getSelection()) {
				selectedIndex = i;
				break;
			}
		}
		return selectedIndex;
	}

	private BreadcrumbItem makeBreadCrumb(IOfficeManagerView view) {
		return makeBreadCrumb(view, null);
	}

	private BreadcrumbItem makeBreadCrumb(IOfficeManagerView view, Composite viewComposite) {
		BreadcrumbItem item = new BreadcrumbItem(composite.breadcrumb, SWT.TOGGLE);
		item.setTextColor(AppPrefs.COLOR_BREADCRUMB_TEXT);
		item.setTextColorSelected(AppPrefs.COLOR_BREADCRUMB_SELECTED_TEXT);
		view.addChangeViewNameListener(new ChangeViewNameListener() {
			@Override
			public void changeViewName(String newName) {
				item.setText(" " + newName);
				composite.breadcrumb.layout();
				composite.compositeBreadcrumb.layout();
			}
		});
		view.addChangeViewListener(new ChangeViewListener() {
			@Override
			public void changeView(IOfficeManagerView view) {
				clearBreadcrumbsAfterSelected();
				BreadcrumbItem item = makeBreadCrumb(view);
				selectBreadCrumb(item);
			}
		});
		view.addForceCloseListener(e -> {
			int selectedIndex = getSelectedBreadcrumbIndex();
			if (selectedIndex <= 0) {
				clearBreadcrumbs();
			} else {
				selectBreadCrumb(selectedIndex - 1);
				clearBreadcrumbsAfterSelected();
			}
		});
		if (view instanceof ISaveableView) {
			composite.tltmSaveItem.setEnabled(false);
			item.setData(DATA_SAVE_STATE, false);
			((ISaveableView) view).addSaveStateChangeListener(new SaveStateChangedListener() {
				@Override
				public void saveStateChanged(boolean canSave) {
					item.setData(DATA_SAVE_STATE, canSave);
					composite.tltmSaveItem.setEnabled(canSave);
				}
			});
		}
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				logger.debug("Bread crumb '" + item.getText() + "' selected.");
				selectBreadCrumb(item);
			}
		});
		item.setText(" " + view.getViewName());
		item.setData(DATA_VIEW, view);
		item.setData(DATA_DISPOSE_ON_CLOSE, viewComposite == null);
		item.setData(DATA_SELECTED, false);
		if (viewComposite == null) {
			viewComposite = view.makeComposite(composite.compositeMainStage, SWT.DOUBLE_BUFFERED);
		}
		item.setData(DATA_COMPOSITE, viewComposite);
		if (view.getViewIcon() != null) {
			item.setImage(view.getViewIcon());
			item.setSelectionImage(view.getViewIcon());
		}
		return item;
	}

	private void selectBreadCrumb(BreadcrumbItem item) {
		if ((boolean) item.getData(DATA_SELECTED)) {
			logger.debug("Breadcrumb is already selected.");
			item.setSelection(true);
			return;
		}
		for (BreadcrumbItem crumbItem : composite.breadcrumb.getItems()) {
			crumbItem.setSelection(crumbItem == item);
			crumbItem.setData(DATA_SELECTED, crumbItem == item);
		}
		showBreadCrumb(item);
	}

	private void selectBreadCrumb(int index) {
		if (index < 0 || index >= composite.breadcrumb.getItemCount()) {
			throw new ArrayIndexOutOfBoundsException("The breadcrumb index to select does not exist.");
		}
		if ((boolean) composite.breadcrumb.getItem(index).getData(DATA_SELECTED)) {
			logger.debug("Breadcrumb is already selected.");
			composite.breadcrumb.getItem(index).setSelection(true);
			return;
		}
		for (int i = 0; i < composite.breadcrumb.getItemCount(); i++) {
			composite.breadcrumb.getItem(i).setSelection(index == i);
			composite.breadcrumb.getItem(i).setData(DATA_SELECTED, index == i);
		}
		showBreadCrumb(composite.breadcrumb.getItem(index));
	}

	private void showBreadCrumb(BreadcrumbItem item) {
		IOfficeManagerView view = (IOfficeManagerView) item.getData(DATA_VIEW);
		StackLayout layout = (StackLayout) composite.compositeMainStage.getLayout();
		layout.topControl = (Control) item.getData(DATA_COMPOSITE);

		composite.tltmRefresh.setEnabled(view instanceof IRefreshableView);
		if (view instanceof ISaveableView) {
			composite.tltmSaveItem.setEnabled((boolean) item.getData(DATA_SAVE_STATE));
		} else {
			composite.tltmSaveItem.setEnabled(false);
		}

		if (view instanceof IRefreshableView) {
			((IRefreshableView) view).refresh();
		}

		composite.compositeBreadcrumb.layout();
		composite.compositeMainStage.layout();
		composite.layout();
		currentView = view;
	}
}
