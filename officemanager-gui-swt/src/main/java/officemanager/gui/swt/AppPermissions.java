package officemanager.gui.swt;

public class AppPermissions {

	/**
	 * ORDERS
	 */
	public static final String SEARCH_CLOSED_ORDERS = "SEARCH_CLOSED_ORDERS";
	public static final String SEARCH_OPEN_ORDERS = "SEARCH_OPEN_ORDERS";
	public static final String ADD_SERVICE_ORDER = "ADD_SERVICE_ORDER";
	public static final String ADD_MERCH_ORDER = "ADD_MERCH_ORDER";
	public static final String ADD_BID = "ADD_BID";
	public static final String EDIT_SERVICE_LABOR_RATE = "EDIT_SERVICE_LABOR_RATE";
	public static final String CLOSE_ORDER = "CLOSE_ORDER";
	public static final String DELETE_ORDER = "DELETE_ORDER";
	public static final String REFUND_ORDER = "REFUND_ORDER";
	public static final String REOPEN_ORDER = "REOPEN_ORDER";

	/**
	 * FLAT_RT_SERVS
	 */
	public static final String ADD_FLAT_RATE = "ADD_FLAT_RATE";
	public static final String EDIT_FLAT_RATE_DETAILS = "EDIT_FLAT_RATE_DETAILS";
	public static final String EDIT_FLAT_RATE_PRICE = "EDIT_FLAT_RATE_PRICE";
	public static final String SEARCH_FLAT_RATES = "SEARCH_FLAT_RATES";
	public static final String REMOVE_SERVICE = "REMOVE_SERVICE";

	/**
	 * SECURITY
	 */
	public static final String SECURITY_USER = "SECURITY_USER";
	public static final String SECURITY_ADD_ROLE = "SECURITY_ADD_ROLE";
	public static final String SECURITY_EDIT_ROLE = "SECURITY_EDIT_ROLE";
	public static final String SECURITY_VIEW = "SECURITY_VIEW";

	/**
	 * PERSONNEL
	 */
	public static final String ADD_PERSONNEL = "ADD_PERSONNEL";
	public static final String EDIT_PERSONNEL = "EDIT_PERSONNEL";
	public static final String SEARCH_PERSONNEL = "SEARCH_PERSONNEL";

	/**
	 * CLIENT
	 */
	public static final String EDIT_CLIENT = "EDIT_CLIENT";
	public static final String EDIT_CLIENT_PRODUCT = "EDIT_CLIENT_PRODUCT";
	public static final String REMOVE_CLIENT_PRODUCT = "REMOVE_CLIENT_PRODUCT";
	public static final String ADD_CLIENT = "ADD_CLIENT";
	public static final String SEARCH_CLIENTS = "SEARCH_CLIENTS";
	public static final String EDIT_PROMO_CLUB = "EDIT_PROMO_CLUB";
	public static final String EDIT_TAX_EXEMPT = "EDIT_TAX_EXEMPT";

	/**
	 * ACCOUNT
	 */
	public static final String ADD_CLIENT_ACCOUNT = "ADD_CLIENT_ACCOUNT";
	public static final String SEARCH_ACCOUNTS = "SEARCH_ACCOUNTS";
	public static final String REOPEN_ACCOUNT = "REOPEN_ACCOUNT";
	public static final String SUSPEND_ACCOUNT = "SUSPEND_ACCOUNT";
	public static final String CLOSE_ACCOUNT = "CLOSE_ACCOUNT";

	/**
	 * PERSON
	 */
	public static final String REMOVE_PHONE = "REMOVE_PHONE";
	public static final String REMOVE_ADDRESS = "REMOVE_ADDRESS";

	/**
	 * PARTS
	 */
	public static final String ADD_PART = "ADD_PART";
	public static final String EDIT_PART_DETAILS = "EDIT_PART_DETAILS";
	public static final String EDIT_PART_PRICE = "EDIT_PART_PRICE";
	public static final String SEARCH_PARTS = "SEARCH_PARTS";
	public static final String REMOVE_PART = "REMOVE_PART";

	/**
	 * INVENTORY
	 */
	public static final String EDIT_INVTRY_CNT = "EDIT_INVTRY_CNT";
	public static final String ADD_INVTRY = "ADD_INVTRY";

	/**
	 * SALES
	 */
	public static final String ADD_SALE = "ADD_SALE";
	public static final String EDIT_SALE_DETAILS = "EDIT_SALE_DETAILS";
	public static final String EDIT_SALE_DATE = "EDIT_SALE_DATE";
	public static final String END_SALE = "END_SALE";
	public static final String DISCOUNT_PRICE = "DISCOUNT_PRICE";
	public static final String SEARCH_SALE = "SEARCH_SALES";

	/**
	 * PREFERENCES
	 */
	public static final String PREFERENCES_VIEW = "PREFERENCES_VIEW";
	public static final String PREFERENCES_EDIT = "PREFERENCES_EDIT";

	/**
	 * REPORTING
	 */
	public static final String REPORTS_VIEW = "REPORTS_VIEW";
	public static final String REPORTS_TOTALS = "REPORTS_TOTALS";
	public static final String REPORTS_BLNC_DRWR = "REPORTS_BLNC_DRWR";
	public static final String REPORTS_INVENTORY = "REPORTS_INVENTORY";

	/**
	 * LOCATIONS
	 */
	public static final String LOCATIONS_VIEW = "LOCATIONS_VIEW";
}
