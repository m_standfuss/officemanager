package officemanager.gui.swt.views;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.LocationDelegate;
import officemanager.biz.delegates.PartDelegate;
import officemanager.biz.records.LocationRecord;
import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.LocationTableService;
import officemanager.gui.swt.services.PartSearchTableService;
import officemanager.gui.swt.ui.composites.LocationComposite;
import officemanager.gui.swt.ui.tables.LocationTableComposite;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.PartSelectionWizard;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class LocationView extends OfficeManagerView implements IRefreshableView {

	private static final List<Integer> partColumnsToKeep = Lists.newArrayList(PartSearchTableComposite.COL_AMT_SOLD,
			PartSearchTableComposite.COL_BASE_PRICE, PartSearchTableComposite.COL_DESCRIPTION,
			PartSearchTableComposite.COL_INVNTRY_CNT, PartSearchTableComposite.COL_INVNTRY_STATUS,
			PartSearchTableComposite.COL_MANUF, PartSearchTableComposite.COL_MANUF_ID,
			PartSearchTableComposite.COL_PART_NAME);

	private static final List<Integer> locationColumnsToKeep = Lists.newArrayList(LocationTableComposite.COL_ABBR_NAME,
			LocationTableComposite.COL_LOCATION_NAME, LocationTableComposite.COL_TYPE);

	private final Long locationId;

	private final LocationDelegate locationDelegate;
	private final PartDelegate partDelegate;

	private final PartSearchTableService partService;
	private final LocationTableService locationService;

	private LocationComposite composite;

	public LocationView(Long locationId) {
		checkNotNull(locationId);
		this.locationId = locationId;

		locationDelegate = new LocationDelegate();
		partDelegate = new PartDelegate();

		partService = new PartSearchTableService(partColumnsToKeep);
		locationService = new LocationTableService(locationColumnsToKeep);
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new LocationComposite(parent, style);
		addListeners();
		locationService.setComposite(composite.childrenLocationTable);
		partService.setComposite(composite.partTblCmp);
		refresh();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Location";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.CRATE, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			LocationRecord record = locationDelegate.getLocationRecord(locationId);
			changeViewName("Location- " + record.getAbbrDisplay());
			composite.lblAbbreviatedName.setText(record.getAbbrDisplay());
			composite.lblName.setText(record.getDisplay());
			if (record.getParentLocationId() != null) {
				composite.lblParentLocation
						.setText(locationDelegate.getFullLocationDisplay(record.getParentLocationId()));
			} else {
				composite.lblParentLocation.setText("");
			}
			composite.lblType.setText(record.getLocationType().getDisplay());

			List<LocationRecord> childrenLocations = locationDelegate.getChildrenLocations(locationId);
			locationService.populateTable(childrenLocations);

			List<PartSearchRecord> partRecords = partDelegate.searchParts(locationId);
			partService.populateTable(partRecords);

			composite.layout();
		});
	}

	private void addListeners() {
		composite.childrenLocationTable.table.addListener(SWT.MouseDoubleClick, e -> {
			Long locationId = locationService.getSelectedLocationId();
			changeView(new LocationView(locationId));
		});
		composite.partTblCmp.table.addListener(SWT.MouseDoubleClick, e -> {
			Long partId = partService.getSelectedPartId();
			changeView(new PartView(partId));
		});

		composite.tltmAddParts.addListener(SWT.Selection, e -> {
			PartSelectionWizard wizard = new PartSelectionWizard(Integer.MAX_VALUE, true, true, false);
			List<PartSearchRecord> partRecords = partDelegate.searchParts(locationId);
			wizard.showParts(partRecords);

			OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
			dialog.setBlockOnOpen(true);
			if (dialog.open() != Window.OK) {
				return;
			}

			BusyIndicator.showWhile(Display.getDefault(), () -> {
				List<PartSearchRecord> selectedParts = wizard.getResults();
				List<Long> partIds = Lists.newArrayList();
				for (PartSearchRecord record : selectedParts) {
					partIds.add(record.getPartId());
				}
				partDelegate.updatePartLocation(locationId, partIds);
				refresh();
			});
		});
		composite.tltmEditLocation.addListener(SWT.Selection, e -> changeView(new LocationEditView(locationId)));
		composite.tltmRemoveLocation.addListener(SWT.Selection, e -> {
			if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm Deletion",
					"All parts assigned to this location will be unassigned and any children location will be moved under the partent of this location. Are you sure you wish to delete this location? ")) {
				return;
			}

			BusyIndicator.showWhile(Display.getDefault(), () -> {
				locationDelegate.deleteLocation(locationId);
				forceCloseView();
			});
		});
	}
}
