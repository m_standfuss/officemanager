package officemanager.gui.swt.views;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.FlatRateServiceDelegate;
import officemanager.biz.records.FlatRateServiceRecord;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.records.SaleRecord.SaleProductType;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.SaleService;
import officemanager.gui.swt.ui.composites.FlatRateComposite;
import officemanager.gui.swt.ui.tables.SaleTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class FlatRateView extends OfficeManagerView implements IRefreshableView {

	private static final Logger logger = LogManager.getLogger(FlatRateView.class);

	private final SaleService saleTableService;
	private final FlatRateServiceDelegate flatRateServiceDelegate;
	private final long flatRateServiceId;

	private FlatRateComposite composite;

	public FlatRateView(long flatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;

		saleTableService = new SaleService(Lists.newArrayList(SaleTableComposite.COL_ENTITY_NAME));
		flatRateServiceDelegate = new FlatRateServiceDelegate();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new FlatRateComposite(parent, style);

		composite.saleTableComposite.table.addListener(SWT.MouseDoubleClick, e -> openSale());
		composite.tlItmEditFlatRate.addListener(SWT.Selection, e -> editFlatRateService());
		composite.tlItmDeleteFlatRate.addListener(SWT.Selection, e -> removeFlatRateService());
		composite.tlItmAddSale.addListener(SWT.Selection,
				e -> changeView(new SaleEditView(SaleProductType.FLAT_RATE_SERVICE, flatRateServiceId)));

		saleTableService.setSaleTableComposite(composite.saleTableComposite);
		return composite;
	}

	@Override
	public String getViewName() {
		return "Flat Rate Service";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		if (composite != null) {
			composite.dispose();
		}
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			FlatRateServiceRecord record = flatRateServiceDelegate.getFlatRateService(flatRateServiceId);
			composite.lblAmountSold.setText(String.valueOf(record.getAmountSold()));
			composite.lblBarcode.setText(record.getBarcode());
			composite.lblBasePrice.setText(AppPrefs.FORMATTER.formatMoney(record.getBasePrice()));
			composite.lblCategory.setText(record.getCategory().getDisplay());
			composite.lblProductType.setText(record.getProductType().getDisplay());
			composite.lblServiceName.setText(record.getServiceName());
			composite.txtDescription.setText(record.getServiceDescription().getLongText());
			composite.lblAmountSold.getParent().layout(true);
			changeViewName("Flat Rate Service - " + record.getServiceName());

			List<SaleRecord> activeSales = flatRateServiceDelegate.getActiveSales(flatRateServiceId);
			saleTableService.populateTable(activeSales);
		});
	}

	private void editFlatRateService() {
		changeView(new FlatRateEditView(flatRateServiceId));
	}

	private void openSale() {
		if (composite.saleTableComposite.table.getSelectionCount() == 0) {
			logger.error("No order rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.saleTableComposite.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Opening the first of the two.");
		}
		TableItem saleRow = selectedRows[0];
		SaleRecord record = (SaleRecord) saleRow.getData(SaleService.DATA_SALE_RECORD);
		changeView(new SaleView(record.getSaleId()));
	}

	private void removeFlatRateService() {
		if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm Delete",
				"Are you sure you wish to remove this service? Any current sales for the part will be ended.")) {
			return;
		}
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			flatRateServiceDelegate.deactiveService(flatRateServiceId);
			forceCloseView();
		});
	}
}
