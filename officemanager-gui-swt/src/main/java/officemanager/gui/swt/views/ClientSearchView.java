package officemanager.gui.swt.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import officemanager.biz.records.ClientSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.ClientSearchService;
import officemanager.gui.swt.ui.composites.ClientSearchComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ClientSearchView extends OfficeManagerView {
	private static final Logger logger = LogManager.getLogger(ClientSearchView.class);

	private final ClientSearchService clientSearchService;
	private ClientSearchComposite composite;

	public ClientSearchView() {
		clientSearchService = new ClientSearchService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new ClientSearchComposite(parent, style);
		composite.searchResultsTable.table.addListener(SWT.MouseDoubleClick, e -> openClientView());
		clientSearchService.setComposite(composite);
		return composite;
	}

	@Override
	public String getViewName() {
		return "Client Search";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.USER_SEARCH, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		if (composite == null) {
			composite.dispose();
		}
	}

	private void openClientView() {
		if (composite.searchResultsTable.table.getSelectionCount() == 0) {
			logger.error("No client row is currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.searchResultsTable.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one client is currently selected. Opening the first of the two.");
		}
		TableItem row = selectedRows[0];
		ClientSearchRecord record = (ClientSearchRecord) row.getData(ClientSearchService.DATA_RECORD);
		changeView(new ClientView(record.getClientId()));
	}

}
