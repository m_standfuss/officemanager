package officemanager.gui.swt.views;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.SimpleContentProposalProvider;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.PersonDelegate;
import officemanager.biz.records.AddressRecord;
import officemanager.biz.records.CodeValueRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.AddressEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class AddressEditView extends OfficeManagerSaveableView {
	private static final Logger logger = LogManager.getLogger(AddressEditView.class);

	private static final String ALPHA_LOWER = "abcdefghijklmnopqrstuvwxyz";
	private static final String ALPHA_UPPER = ALPHA_LOWER.toUpperCase();

	private final Long addressId;
	private final Long personId;
	private final PersonDelegate personDelegate;

	private AddressRecord addressInContext;
	private AddressEditComposite composite;
	private ToolBar toolBar;
	private ToolItem tlItmRemoveAddress;

	public AddressEditView(Long addressId, Long personId) {
		this.addressId = addressId;
		this.personId = personId;
		personDelegate = new PersonDelegate();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		Composite root = new Composite(parent, style);
		root.setLayout(new GridLayout());

		toolBar = new ToolBar(root, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		tlItmRemoveAddress = new ToolItem(toolBar, SWT.NONE);
		tlItmRemoveAddress.setEnabled(addressId != null);
		tlItmRemoveAddress.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmRemoveAddress.setToolTipText("Remove Address");
		tlItmRemoveAddress.addListener(SWT.Selection, e -> removeAddress());

		composite = new AddressEditComposite(root, SWT.NONE);
		composite.btnPrimaryAddress.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.cmbAddressType.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.txtAddressLine1.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtAddressLine2.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtAddressLine3.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtCity.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtState.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtZip.addListener(SWT.Modify, e -> changeSaveState(true));

		populateCityAutoCompletes();
		populateComposite();
		return root;
	}

	@Override
	public String getViewName() {
		return "Address";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.ADDRESS, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.getParent().dispose();
	}

	@Override
	public void saveView() {
		if (!validate()) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			List<String> addressLines = getAddressLines();
			addressInContext.setAddressLines(addressLines);
			addressInContext.setAddressType(composite.cmbAddressType.getCodeValueRecord());
			addressInContext.setCity(composite.txtCity.getText());
			addressInContext.setPersonId(personId);
			addressInContext.setPrimaryAddress(composite.btnPrimaryAddress.getSelection());
			addressInContext.setState(composite.txtState.getText());
			addressInContext.setZip(composite.txtZip.getText());

			Long addressId = personDelegate.persistAddressRecord(addressInContext);
			addressInContext = new AddressRecord(addressId, addressInContext);
			tlItmRemoveAddress.setEnabled(true);
			changeSaveState(false);
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully Saved Address");
	}

	private boolean validate() {
		if (getAddressLines().isEmpty()) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Must provide an address.");
			return false;
		}
		if (composite.cmbAddressType.getSelectionIndex() == -1) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Please select an address type.");
			return false;
		}
		return true;
	}

	private void populateComposite() {

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.cmbAddressType.populateSelections(personDelegate.getAddressTypes());
		});

		if (addressId == null) {
			logger.debug("Creating a new phone record for personId=" + personId);
			addressInContext = new AddressRecord();
			addressInContext.setPersonId(personId);
		} else {
			logger.debug("Existing phone attempting to obtain.");
			addressInContext = personDelegate.getAddressRecord(addressId);
			if (addressInContext == null) {
				logger.warn("Unable to load previous phone, creating new one instead.");
				addressInContext = new AddressRecord();
				addressInContext.setPersonId(personId);
			}
		}

		composite.btnPrimaryAddress.setSelection(addressInContext.isPrimaryAddress());
		composite.cmbAddressType.setSelectedValue(addressInContext.getAddressType());

		for (int i = 0; i < addressInContext.getAddressLines().size(); i++) {
			if (i == 0) {
				composite.txtAddressLine1.setText(addressInContext.getAddressLines().get(i));
			} else if (i == 1) {
				composite.txtAddressLine2.setText(addressInContext.getAddressLines().get(i));
			} else {
				composite.txtAddressLine3
						.setText(composite.txtAddressLine3.getText() + " " + addressInContext.getAddressLines().get(i));
			}
		}
		composite.txtCity.setText(addressInContext.getCity());
		composite.txtState.setText(addressInContext.getState());
		composite.txtZip.setText(addressInContext.getZip());
	}

	private void removeAddress() {
		if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm Deletion",
				"Are you sure you wish to delete this address?")) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			personDelegate.deactivateAddress(addressId);
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully Removed Address");
		forceCloseView();
	}

	private List<String> getAddressLines() {
		List<String> addressLines = Lists.newArrayList();
		if (!composite.txtAddressLine1.getText().isEmpty()) {
			addressLines.add(composite.txtAddressLine1.getText());
		}
		if (!composite.txtAddressLine2.getText().isEmpty()) {
			addressLines.add(composite.txtAddressLine2.getText());
		}
		if (!composite.txtAddressLine3.getText().isEmpty()) {
			addressLines.add(composite.txtAddressLine3.getText());
		}
		return addressLines;
	}

	private void populateCityAutoCompletes() {
		List<CodeValueRecord> cities = personDelegate.getCities();
		String[] cityArray = new String[cities.size()];
		for (int i = 0; i < cities.size(); i++) {
			CodeValueRecord record = cities.get(i);
			cityArray[i] = record.getDisplay();
		}

		SimpleContentProposalProvider proposalProvider = new SimpleContentProposalProvider(cityArray);
		ContentProposalAdapter proposalAdapter = new ContentProposalAdapter(composite.txtCity, new TextContentAdapter(),
				proposalProvider, getActivationKeystroke(), getAutoactivationChars());
		proposalProvider.setFiltering(true);
		proposalAdapter.setPropagateKeys(true);
		proposalAdapter.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_REPLACE);
	}

	private char[] getAutoactivationChars() {
		String delete = new String(new char[] { 8 });
		String allChars = ALPHA_LOWER + ALPHA_UPPER + delete;
		return allChars.toCharArray();
	}

	private KeyStroke getActivationKeystroke() {
		KeyStroke instance = KeyStroke.getInstance(new Integer(SWT.CTRL).intValue(), new Integer(' ').intValue());
		return instance;
	}
}
