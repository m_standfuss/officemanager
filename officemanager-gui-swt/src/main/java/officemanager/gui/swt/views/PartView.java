package officemanager.gui.swt.views;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.PartDelegate;
import officemanager.biz.records.PartRecord;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.records.SaleRecord.SaleProductType;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.SaleService;
import officemanager.gui.swt.ui.composites.PartComposite;
import officemanager.gui.swt.ui.tables.SaleTableComposite;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.PartWizard;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class PartView extends OfficeManagerView implements IRefreshableView {

	private static final Logger logger = LogManager.getLogger(PartView.class);

	private final SaleService saleTableService;
	private final PartDelegate partDelegate;
	private final long partId;
	private PartComposite composite;

	public PartView(long partId) {
		saleTableService = new SaleService(Lists.newArrayList(SaleTableComposite.COL_ENTITY_NAME));
		partDelegate = new PartDelegate();
		this.partId = partId;
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new PartComposite(parent, style);

		composite.saleTableComposite.table.addListener(SWT.MouseDoubleClick, e -> openSale());
		composite.tlItmDeletePart.addListener(SWT.Selection, e -> removePart());
		composite.tlItmEditPart.addListener(SWT.Selection, e -> editPart());
		composite.tlItmAddSale.addListener(SWT.Selection,
				e -> changeView(new SaleEditView(SaleProductType.PART, partId)));

		saleTableService.setSaleTableComposite(composite.saleTableComposite);
		return composite;
	}

	@Override
	public void closeView() {
		if (composite != null) {
			composite.dispose();
			composite = null;
		}
	}

	@Override
	public String getViewName() {
		return "Part Details";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.PART, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			PartRecord record = partDelegate.getPart(partId);
			composite.lblAmountSold.setText(String.valueOf(record.getAmountSold()));
			composite.lblBarcode.setText(record.getBarcode());
			composite.lblBasePrice.setText(AppPrefs.FORMATTER.formatMoney(record.getBasePrice()));
			composite.lblCategory.setText(record.getCategoryType().getDisplay());
			composite.lblCurrentInventory.setText(String.valueOf(record.getInventoryCount()));
			composite.lblInventoryThreshold.setText(String.valueOf(record.getInventoryThreshold()));
			composite.lblLastUpdated.setText(AppPrefs.dateFormatter.formatFullDate_DB(record.getUpdtDtTm()));
			composite.lblLastUpdatedBy.setText(record.getLastUpdatedBy());
			composite.lblLocation.setText(record.getLocationDisplay());
			composite.lblManufacturer.setText(record.getManufacturer().getDisplay());
			composite.lblManufId.setText(record.getManufPartId());
			composite.lblPartName.setText(record.getPartName());
			composite.lblProductType.setText(record.getProductType().getDisplay());
			composite.txtPartDescription.setText(record.getDescription());
			composite.txtPartDescription.getParent().layout(true);
			changeViewName("Part Details - " + record.getPartName());

			List<SaleRecord> activeSales = partDelegate.getActiveSales(partId);
			saleTableService.populateTable(activeSales);
		});
	}

	private void editPart() {
		PartWizard wizard = new PartWizard(partId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		if (dialog.open() == Window.OK) {
			refresh();
		}
	}

	private void removePart() {
		if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm Delete",
				"Are you sure you wish to remove this part? Any current sales for the part will be ended.")) {
			return;
		}
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			partDelegate.deactivePart(partId);
			forceCloseView();
		});
	}

	private void openSale() {
		if (composite.saleTableComposite.table.getSelectionCount() == 0) {
			logger.error("No order rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.saleTableComposite.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Opening the first of the two.");
		}
		TableItem saleRow = selectedRows[0];
		SaleRecord record = (SaleRecord) saleRow.getData(SaleService.DATA_SALE_RECORD);
		changeView(new SaleView(record.getSaleId()));
	}
}
