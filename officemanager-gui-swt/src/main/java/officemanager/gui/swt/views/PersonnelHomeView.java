package officemanager.gui.swt.views;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.OrderSearchDelegate;
import officemanager.biz.delegates.PersonnelDelegate;
import officemanager.biz.records.OrderSearchRecord;
import officemanager.biz.records.PersonnelRecord;
import officemanager.biz.searchingcriteria.OrderSearchCriteria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.OrderSearchService;
import officemanager.gui.swt.services.PersonnelInformationService;
import officemanager.gui.swt.ui.composites.PersonnelHomeComposite;
import officemanager.gui.swt.ui.tables.OrderTableComposite;
import officemanager.gui.swt.ui.wizards.NewBidWizard;
import officemanager.gui.swt.ui.wizards.NewMerchandiseWizard;
import officemanager.gui.swt.ui.wizards.NewServiceOrderWizard;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.PersonWizard;
import officemanager.gui.swt.ui.wizards.PersonnelWizard;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelHomeView extends OfficeManagerView implements IRefreshableView {
	private static final Logger logger = LogManager.getLogger(PersonnelHomeView.class);
	private static final List<Integer> COLUMNS_TO_KEEP = Lists.newArrayList(OrderTableComposite.COL_ORDER_NUM,
			OrderTableComposite.COL_ORDER_TYPE, OrderTableComposite.COL_CLIENT_NAME,
			OrderTableComposite.COL_CLIENT_PHONE, OrderTableComposite.COL_AMOUNT, OrderTableComposite.COL_ORDER_STATUS,
			OrderTableComposite.COL_OPENED_DTTM, OrderTableComposite.COL_OPENED_BY,
			OrderTableComposite.COL_PRODUCT_CNT);

	private static final String DATA_ORDER_ID = "DATA_ORDER_ID";

	private final OrderSearchDelegate orderSearchDelegate;
	private final PersonnelDelegate personnelDelegate;
	private final PersonnelInformationService personnelInformationService;
	private final OrderSearchService orderTableService;

	private PersonnelHomeComposite composite;

	public PersonnelHomeView() {
		orderSearchDelegate = new OrderSearchDelegate();
		personnelDelegate = new PersonnelDelegate();
		personnelInformationService = new PersonnelInformationService();
		orderTableService = new OrderSearchService(COLUMNS_TO_KEEP);
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new PersonnelHomeComposite(parent, style);
		personnelInformationService.setComposite(composite.personnelInfoComposite);
		orderTableService.setComposite(composite.ordersTable);

		composite.ordersTable.table.addListener(SWT.MouseDoubleClick, l -> openOrder());
		composite.mntmServiceOrder.addListener(SWT.Selection, e -> newServiceWizard());
		composite.mntmMerchandiseOrder.addListener(SWT.Selection, e -> newMerchandiseWizard());
		composite.mntmBid.addListener(SWT.Selection, e -> newBidWizard());
		composite.tlItmEditInformation.addListener(SWT.Selection, l -> editPersonnel());
		return composite;
	}

	@Override
	public void closeView() {
		if (composite != null) {
			composite.dispose();
			composite = null;
		}
	}

	@Override
	public String getViewName() {
		return "Home";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.HOME, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			logger.debug("Starting refresh.");
			OrderSearchCriteria orderCriteria = new OrderSearchCriteria();
			orderCriteria.openedPrsnlId = AppPrefs.currentSession.prsnlId;

			List<OrderSearchRecord> orderSearchRecords = orderSearchDelegate.searchCurrentOrders(orderCriteria);
			orderTableService.populateTable(orderSearchRecords);

			PersonnelRecord record = personnelDelegate.getPersonnelRecord(AppPrefs.currentSession.prsnlId);
			personnelInformationService.refresh(record);
		});
	}

	private void newMerchandiseWizard() {
		NewMerchandiseWizard wizard = new NewMerchandiseWizard();
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Window.OK) {
			changeView(wizard.getReturnView());
		}
	}

	private void newServiceWizard() {
		NewServiceOrderWizard wizard = new NewServiceOrderWizard();
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Window.OK) {
			changeView(wizard.getReturnView());
		}
	}

	private void newBidWizard() {
		NewBidWizard wizard = new NewBidWizard();
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		dialog.open();
	}

	private void editPersonnel() {
		PersonWizard wizard = new PersonnelWizard(AppPrefs.currentSession.prsnlId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Dialog.OK) {
			refresh();
		}
	}

	private void openOrder() {
		if (composite.ordersTable.table.getSelectionCount() == 0) {
			logger.error("No order rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.ordersTable.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Opening the first of the two.");
		}
		TableItem orderRow = selectedRows[0];
		Long orderId = (Long) orderRow.getData(DATA_ORDER_ID);
		changeView(new OrderView(orderId));
	}
}
