package officemanager.gui.swt.views;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import officemanager.biz.delegates.SaleDelegate;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.records.SaleRecord.SaleType;
import officemanager.biz.searchingcriteria.SaleSearchCritieria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.SaleSearchComposite;
import officemanager.gui.swt.ui.tables.SaleTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class SaleSearchView extends OfficeManagerView {

	private static final String DATA_SALE_RECORD = "DATA_SALE_RECORD";
	private static final Logger logger = LogManager.getLogger(SaleSearchView.class);

	private final SaleDelegate saleDelegate;
	private SaleSearchComposite composite;

	public SaleSearchView() {
		saleDelegate = new SaleDelegate();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new SaleSearchComposite(parent, style);

		TraverseListener enterKeyListener = e -> {
			if (e.keyCode == SWT.CR || e.keyCode == 16777296) {
				search();
				e.doit = false;
			}
		};

		composite.btnSearch.addListener(SWT.Selection, l -> search());
		composite.searchResultsTable.table.addListener(SWT.MouseDoubleClick, e -> openSale());
		composite.txtSaleItem.addTraverseListener(enterKeyListener);
		composite.txtSaleName.addTraverseListener(enterKeyListener);
		composite.tlItmNewSale.addListener(SWT.Selection, e -> changeView(new SaleEditView()));
		return composite;
	}

	private void search() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			SaleSearchCritieria criteria = new SaleSearchCritieria();
			criteria.saleName = composite.txtSaleName.getText();
			criteria.saleItemName = composite.txtSaleItem.getText();
			criteria.amountOffSale = composite.btnAmountOff.getSelection();
			criteria.percentageOffSale = composite.btnPercentageOff.getSelection();
			criteria.begActiveDttm = convertToBeginningOfDayUTC(composite.dateTimeStart);
			criteria.endActiveDttm = convertToEndOfDayUTC(composite.dateTimeEnd);

			List<SaleRecord> activeSales = saleDelegate.searchSaleRecords(criteria);
			composite.searchResultsTable.table.removeAll();
			for (SaleRecord sale : activeSales) {
				TableItem item = new TableItem(composite.searchResultsTable.table, SWT.NONE);
				String amountOff = "";
				if (sale.getSaleType() == SaleType.FLAT_RATE_OFF) {
					amountOff = AppPrefs.FORMATTER.formatMoney(sale.getAmountOff());
				} else if (sale.getSaleType() == SaleType.PERCENTAGE_OFF) {
					amountOff = AppPrefs.FORMATTER.formatPercentage(sale.getAmountOff());
				}
				item.setText(SaleTableComposite.COL_ENTITY_NAME, sale.getEntityName());
				item.setText(SaleTableComposite.COL_SALE_NAME, sale.getSaleName());
				item.setText(SaleTableComposite.COL_AMOUNT_OFF, amountOff);
				item.setText(SaleTableComposite.COL_BEGIN_DTTM,
						AppPrefs.dateFormatter.formatFullDate_DB(sale.getStartDttm()));
				item.setText(SaleTableComposite.COL_END_DTTM,
						AppPrefs.dateFormatter.formatFullDate_DB(sale.getEndDttm()));
				item.setText(SaleTableComposite.COL_SALE_TYPE, sale.getSaleTypeDisplay());

				item.setData(DATA_SALE_RECORD, sale);
			}
			composite.searchResultsTable.table.packTableColumns();
		});
	}

	private void openSale() {
		if (composite.searchResultsTable.table.getSelectionCount() == 0) {
			logger.error("No order rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.searchResultsTable.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Opening the first of the two.");
		}
		TableItem saleRow = selectedRows[0];
		SaleRecord record = (SaleRecord) saleRow.getData(DATA_SALE_RECORD);
		changeView(new SaleView(record.getSaleId()));
	}

	@Override
	public String getViewName() {
		return "Sale Search";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.SEARCH, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}
}
