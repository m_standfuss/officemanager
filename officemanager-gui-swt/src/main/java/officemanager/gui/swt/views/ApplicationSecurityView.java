package officemanager.gui.swt.views;

import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.delegates.SecurityDelegate;
import officemanager.biz.records.RolePermissionRecord;
import officemanager.biz.records.RoleSelectionRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.ApplicationSecurityComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ApplicationSecurityView extends OfficeManagerView implements IRefreshableView {

	private static final String DATA_ROLE_RECORD = "DATA_ROLE_RECORD";

	private final SecurityDelegate securityDelegate;
	private ApplicationSecurityComposite composite;

	public ApplicationSecurityView() {
		securityDelegate = new SecurityDelegate();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new ApplicationSecurityComposite(parent, style);
		composite.tlItmNewPart.addListener(SWT.Selection, e -> changeView(new RoleEditView()));
		composite.listRoles.addListener(SWT.Selection, e -> {
			composite.btnEditRole.setEnabled(AppPrefs.hasPermission(AppPermissions.SECURITY_EDIT_ROLE));
			showPermissions();
		});
		composite.btnEditRole.addListener(SWT.Selection, e -> openEditRole());
		composite.btnEditRole.setEnabled(false);
		populateComposite();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Application Security";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.SECURITY, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		showPermissions();
	}

	private RoleSelectionRecord getSelectedRole() {
		int i = composite.listRoles.getSelectionIndex();
		RoleSelectionRecord role = (RoleSelectionRecord) composite.listRoles.getData(DATA_ROLE_RECORD + i);
		return role;
	}

	private void openEditRole() {
		RoleSelectionRecord role = getSelectedRole();
		changeView(new RoleEditView(role.getRoleId()));
	}

	private void showPermissions() {
		RoleSelectionRecord role = getSelectedRole();
		if (role == null) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			List<RolePermissionRecord> rolePermissions = securityDelegate.getPermissionsForRole(role.getRoleId());
			Collections.sort(rolePermissions);
			composite.listPermissions.removeAll();
			for (RolePermissionRecord permissionRecord : rolePermissions) {
				composite.listPermissions.add(permissionRecord.getPermissionDisplay());
			}
		});
	}

	private void populateComposite() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			List<RoleSelectionRecord> roleRecords = securityDelegate.getActiveRoles();
			Collections.sort(roleRecords);

			for (int i = 0; i < roleRecords.size(); i++) {
				RoleSelectionRecord role = roleRecords.get(i);
				composite.listRoles.add(role.getRoleName());
				composite.listRoles.setData(DATA_ROLE_RECORD + i, role);
			}
		});
	}
}
