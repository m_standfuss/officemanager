package officemanager.gui.swt.views;

import java.math.BigDecimal;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolItem;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.OrderCommentDelegate;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderRecord.OrderStatus;
import officemanager.biz.records.OrderRecord.OrderType;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PaymentRecord.PaymentType;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.ClientProductService;
import officemanager.gui.swt.services.OrderCommentService;
import officemanager.gui.swt.services.OrderFlatRateService;
import officemanager.gui.swt.services.OrderPartService;
import officemanager.gui.swt.services.OrderPrintService;
import officemanager.gui.swt.services.OrderServiceService;
import officemanager.gui.swt.services.PaymentTableService;
import officemanager.gui.swt.ui.composites.OrderComposite;
import officemanager.gui.swt.ui.dialogs.EditOrderFlatRateDialog;
import officemanager.gui.swt.ui.dialogs.EditOrderPartDialog;
import officemanager.gui.swt.ui.dialogs.EditOrderServiceDialog;
import officemanager.gui.swt.ui.tables.ClientProductTableComposite;
import officemanager.gui.swt.ui.tables.OfficeManagerTable;
import officemanager.gui.swt.ui.tables.OrderCommentTableComposite;
import officemanager.gui.swt.ui.tables.OrderFlatRateTableComposite;
import officemanager.gui.swt.ui.tables.OrderPartTableComposite;
import officemanager.gui.swt.ui.tables.OrderServiceTableComposite;
import officemanager.gui.swt.ui.tables.PaymentTableComposite;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.OrderCommentWizard;
import officemanager.gui.swt.ui.wizards.OrderFlatRateWizard;
import officemanager.gui.swt.ui.wizards.OrderPartWizard;
import officemanager.gui.swt.ui.wizards.OrderPaymentWizard;
import officemanager.gui.swt.ui.wizards.OrderRefundWizard;
import officemanager.gui.swt.ui.wizards.OrderServiceWizard;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class OrderView extends OfficeManagerView implements IRefreshableView {

	private static final Logger logger = LogManager.getLogger(OrderView.class);

	private static final List<Integer> CLIENT_PRODUCT_COLUMNS = Lists.newArrayList(
			ClientProductTableComposite.COL_MANUFACTURER, ClientProductTableComposite.COL_MODEL,
			ClientProductTableComposite.COL_PRODUCT, ClientProductTableComposite.COL_SERIAL_NUM);
	private static final List<Integer> ORDER_PART_COLUMNS = Lists.newArrayList(
			OrderPartTableComposite.COL_CLIENT_PRODUCT, OrderPartTableComposite.COL_PART_MANUF,
			OrderPartTableComposite.COL_PART_NAME, OrderPartTableComposite.COL_PRICE,
			OrderPartTableComposite.COL_QUANTITY, OrderPartTableComposite.COL_TAX, OrderPartTableComposite.COL_TOTAL);
	private static final List<Integer> ORDER_COMMENT_COLUMNS = Lists.newArrayList(OrderCommentTableComposite.COL_AUTHOR,
			OrderCommentTableComposite.COL_COMMENT, OrderCommentTableComposite.COL_COMMENT_TYPE,
			OrderCommentTableComposite.COL_DATE);
	private static final List<Integer> ORDER_FLAT_RATE_COLUMNS = Lists.newArrayList(
			OrderFlatRateTableComposite.COL_CLIENT_PRODUCT, OrderFlatRateTableComposite.COL_PRICE,
			OrderFlatRateTableComposite.COL_SERVICE_NAME);
	private static final List<Integer> ORDER_SERVICE_COLUMNS = Lists.newArrayList(
			OrderServiceTableComposite.COL_CLIENT_PRODUCT, OrderServiceTableComposite.COL_HOURS,
			OrderServiceTableComposite.COL_PRICE, OrderServiceTableComposite.COL_PRICE,
			OrderServiceTableComposite.COL_SERVICE_NAME, OrderServiceTableComposite.COL_TOTAL);
	private static final List<Integer> ORDER_PAYMENT_COLUMNS = Lists.newArrayList(PaymentTableComposite.COL_AMOUNT,
			PaymentTableComposite.COL_DATE, PaymentTableComposite.COL_DESCRIPTION, PaymentTableComposite.COL_NUMBER,
			PaymentTableComposite.COL_TYPE);

	private final long orderId;

	private final OrderDelegate orderDelegate;
	private final OrderCommentDelegate orderCommentDelegate;
	private final PaymentDelegate paymentDelegate;

	private final ClientProductService clientProductService;
	private final OrderCommentService orderCommentService;
	private final OrderFlatRateService orderFlatRateService;
	private final OrderPartService orderPartService;
	private final OrderPrintService orderPrintService;
	private final OrderServiceService orderServiceService;
	private final PaymentTableService paymentService;

	private OrderComposite composite;

	public OrderView(long orderId) {
		logger.debug("Creating a new order view for order id=" + orderId);
		this.orderId = orderId;

		paymentDelegate = new PaymentDelegate();
		orderDelegate = new OrderDelegate();
		orderCommentDelegate = new OrderCommentDelegate();

		clientProductService = new ClientProductService(CLIENT_PRODUCT_COLUMNS);
		orderPartService = new OrderPartService(ORDER_PART_COLUMNS);
		orderFlatRateService = new OrderFlatRateService(ORDER_FLAT_RATE_COLUMNS);
		orderServiceService = new OrderServiceService(ORDER_SERVICE_COLUMNS);
		orderCommentService = new OrderCommentService(ORDER_COMMENT_COLUMNS);
		paymentService = new PaymentTableService(ORDER_PAYMENT_COLUMNS);
		orderPrintService = new OrderPrintService();
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			OrderRecord record = orderDelegate.loadOrderRecord(orderId);
			if (record == null) {
				logger.error("Unable to load the order.");
				hideToolItems();
			} else {
				showComponents(record);
				refreshOrderInformation(record);
				refreshProducts(record.getClientProducts());
				refreshFlatRates(record.getFlatRates());
				refreshServices(record.getServices());
				refreshParts(record.getParts());
			}

			refreshComments();
			refreshPayments();
			layoutComposite();
		});
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new OrderComposite(parent, style);
		clientProductService.setComposite(composite.compositeClientProductTable);
		orderCommentService.setComposite(composite.compositeCommentTable);
		orderPartService.setComposite(composite.compositePartTable);
		orderServiceService.setComposite(composite.compositeServiceTable);
		orderFlatRateService.setComposite(composite.compositeFlatRateTable);
		paymentService.setComposite(composite.compositePaymentTable);
		orderPrintService.setShell(composite.getShell());
		addListeners();
		addToolItemListeners();
		return composite;
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public String getViewName() {
		return "Order #" + AppPrefs.FORMATTER.formatOrderNumber(orderId);
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.ORDER, AppPrefs.ICN_SIZE_VIEW);
	}

	private void addListeners() {
		composite.mntmPrintShopTicket.addListener(SWT.Selection, e -> orderPrintService.printShopTicket(orderId));
		composite.mntmPrintClaimTicket.addListener(SWT.Selection, e -> orderPrintService.printClaimTicket(orderId));
		composite.mntmPrintReciept.addListener(SWT.Selection, e -> orderPrintService.printReciept(orderId));
		composite.mntmServices.addListener(SWT.Selection, e -> manageServices());
		composite.mntmFlatRateServices.addListener(SWT.Selection, e -> manageFlatRates());

		composite.compositePaymentTable.table.addListener(SWT.FocusIn,
				e -> unfocusTables(composite.compositePaymentTable.table));
		composite.compositeClientProductTable.table.addListener(SWT.FocusIn,
				e -> unfocusTables(composite.compositeClientProductTable.table));
		composite.compositePartTable.table.addListener(SWT.FocusIn,
				e -> unfocusTables(composite.compositePartTable.table));
		composite.compositeServiceTable.table.addListener(SWT.FocusIn,
				e -> unfocusTables(composite.compositeServiceTable.table));
		composite.compositeFlatRateTable.table.addListener(SWT.FocusIn,
				e -> unfocusTables(composite.compositeFlatRateTable.table));

		composite.compositePartTable.table.addListener(SWT.MouseDoubleClick, e -> openPartDetails());
		composite.compositeServiceTable.table.addListener(SWT.MouseDoubleClick, e -> openServiceDetails());
		composite.compositeFlatRateTable.table.addListener(SWT.MouseDoubleClick, e -> openFlatRateDetails());
	}

	private void addToolItemListeners() {
		addToolItemListener(composite.tltmAddComment, e -> addComment());
		addToolItemListener(composite.tltmPaymentItem, e -> managePayments());
		addToolItemListener(composite.tltmCloseOrder, e -> closeOrder());
		addToolItemListener(composite.tltmReopenOrder, e -> reopenOrder());
		addToolItemListener(composite.tltmDeleteOrder, e -> deleteOrder());
		addToolItemListener(composite.tltmRefund, e -> refundItems());
		addToolItemListener(composite.tltmManageParts, e -> manageParts());
	}

	private void addToolItemListener(ToolItem tltm, Listener object) {
		if (tltm.isDisposed()) {
			return;
		}
		tltm.addListener(SWT.Selection, object);
	}

	private void showComponents(OrderRecord record) {
		if (record.getOrderStatus() == OrderStatus.CLOSED) {
			boolean showReopen = record.getOrderType() == OrderType.WORK
					&& AppPrefs.hasPermission(AppPermissions.REOPEN_ORDER);
			boolean showComments = record.getOrderType() == OrderType.WORK;
			boolean showRefund = AppPrefs.hasPermission(AppPermissions.REFUND_ORDER);
			composite.recreateToolItems(false, false, true, showComments, false, false, false, showReopen, showRefund);
			composite.mntmPrintShopTicket.setEnabled(false);
			composite.mntmPrintClaimTicket.setEnabled(false);
			composite.mntmPrintReciept.setEnabled(true);

		} else if (record.getOrderStatus() == OrderStatus.OPEN) {
			boolean closeOrder = AppPrefs.hasPermission(AppPermissions.CLOSE_ORDER);
			boolean deleteOrder = AppPrefs.hasPermission(AppPermissions.DELETE_ORDER);
			composite.recreateToolItems(true, closeOrder, true, true, true, true, deleteOrder, false, false);
			composite.mntmPrintShopTicket.setEnabled(true);
			composite.mntmPrintClaimTicket.setEnabled(true);
			composite.mntmPrintReciept.setEnabled(true);
		} else {
			hideToolItems();
		}
		addToolItemListeners();

		if (record.getOrderType() == OrderType.MERCHANDISE) {
			((GridData) composite.collapsibleProducts.getLayoutData()).exclude = true;
			((GridData) composite.collapsibleServices.getLayoutData()).exclude = true;
			((GridData) composite.collapsibleFlatRateServices.getLayoutData()).exclude = true;
			((GridData) composite.collapsibleComments.getLayoutData()).exclude = true;
		} else {
			((GridData) composite.collapsibleProducts.getLayoutData()).exclude = false;
			((GridData) composite.collapsibleServices.getLayoutData()).exclude = false;
			((GridData) composite.collapsibleFlatRateServices.getLayoutData()).exclude = false;
			((GridData) composite.collapsibleComments.getLayoutData()).exclude = false;
		}
	}

	private void hideToolItems() {
		composite.recreateToolItems(false, false, false, false, false, false, false, false, false);
	}

	private void layoutComposite() {
		composite.layout();
		composite.scrCompositeCenter.setMinSize(-1, composite.compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	}

	private void refreshOrderInformation(OrderRecord record) {
		boolean showClosedInfo = record.getOrderStatus() == OrderStatus.CLOSED;
		composite.orderInformationComposite.lblClosedBy.setVisible(showClosedInfo);
		composite.orderInformationComposite.lblClosedByTitle.setVisible(showClosedInfo);
		composite.orderInformationComposite.lblClosedOn.setVisible(showClosedInfo);
		composite.orderInformationComposite.lblClosedOnTitle.setVisible(showClosedInfo);

		composite.orderInformationComposite.lblClient.setText(record.getClientName());
		composite.orderInformationComposite.lblOpenedBy.setText(record.getCreatedBy());
		composite.orderInformationComposite.lblOpenedOn
				.setText(AppPrefs.dateFormatter.formatFullDate_DB(record.getCreatedDttm()));
		composite.orderInformationComposite.lblOrderNumber
				.setText(AppPrefs.FORMATTER.formatOrderNumber(record.getOrderId()));
		composite.orderInformationComposite.lblOrderStatus.setText(record.getOrderStatusDisplay());
		composite.orderInformationComposite.lblOrderType.setText(record.getOrderTypeDisplay());
		composite.orderInformationComposite.lblClientPhone
				.setText(AppPrefs.FORMATTER.formatPhoneNumber(record.getClientPhone()));
		composite.orderInformationComposite.lblTotal
				.setText(AppPrefs.FORMATTER.formatMoney(Calculator.calculateOrderTotal(record)));

		composite.orderInformationComposite.lblClosedBy.setText(record.getClosedBy());
		composite.orderInformationComposite.lblClosedOn
				.setText(AppPrefs.dateFormatter.formatFullDate_DB(record.getClosedDttm()));

		composite.orderInformationComposite.layout();
	}

	private void refreshParts(List<OrderPartRecord> parts) {
		orderPartService.populateTable(parts);
		boolean noParts = parts.isEmpty();
		composite.lblNoParts.setVisible(noParts);
		((GridData) composite.lblNoParts.getLayoutData()).exclude = !noParts;
		composite.compositePartTable.setVisible(!noParts);
		((GridData) composite.compositePartTable.getLayoutData()).exclude = noParts;
	}

	private void refreshServices(List<OrderServiceRecord> services) {
		orderServiceService.populateTable(services);
		boolean noServices = services.isEmpty();
		composite.lblNoServices.setVisible(noServices);
		((GridData) composite.lblNoServices.getLayoutData()).exclude = !noServices;
		composite.compositeServiceTable.setVisible(!noServices);
		((GridData) composite.compositeServiceTable.getLayoutData()).exclude = noServices;
	}

	private void refreshFlatRates(List<OrderFlatRateServiceRecord> flatRates) {
		orderFlatRateService.populateTable(flatRates);
		boolean noFlatRates = flatRates.isEmpty();
		composite.lblNoFlatRateServices.setVisible(noFlatRates);
		((GridData) composite.lblNoFlatRateServices.getLayoutData()).exclude = !noFlatRates;
		composite.compositeFlatRateTable.setVisible(!noFlatRates);
		((GridData) composite.compositeFlatRateTable.getLayoutData()).exclude = noFlatRates;
	}

	private void refreshProducts(List<OrderClientProductRecord> clientProducts) {
		clientProductService.populateTableOrderRecords(clientProducts);
		boolean noProducts = clientProducts.isEmpty();
		composite.lblNoProducts.setVisible(noProducts);
		((GridData) composite.lblNoProducts.getLayoutData()).exclude = !noProducts;
		composite.compositeClientProductTable.setVisible(!noProducts);
		((GridData) composite.compositeClientProductTable.getLayoutData()).exclude = noProducts;
	}

	private void refreshPayments() {
		List<PaymentRecord> payments = paymentDelegate.getPaymentsForOrder(orderId);
		if (payments == null) {
			logger.error("Unable to load payments.");
		} else {
			logger.debug(payments.size() + " payments loaded.");
			paymentService.populatePayments(payments);
		}
		((GridData) (composite.collapsiblePayments.getLayoutData())).exclude = payments.isEmpty();
		composite.collapsiblePayments.setVisible(!payments.isEmpty());
	}

	private void refreshComments() {
		List<OrderCommentRecord> comments = orderCommentDelegate.loadOrderComments(orderId);
		if (comments == null) {
			logger.error("Comments could not be loaded.");
		} else {
			logger.debug(comments.size() + " comments loaded.");
			orderCommentService.populateComments(comments);

			boolean noComments = comments.isEmpty();
			composite.lblNoComments.setVisible(noComments);
			((GridData) composite.lblNoComments.getLayoutData()).exclude = !noComments;
			composite.compositeCommentTable.setVisible(!noComments);
			((GridData) composite.compositeCommentTable.getLayoutData()).exclude = noComments;
		}
	}

	private void addComment() {
		OrderCommentWizard wizard = new OrderCommentWizard(orderId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		if (dialog.open() == Dialog.OK) {
			refreshComments();
			layoutComposite();
		}
	}

	private void managePayments() {
		OrderPaymentWizard wizard = new OrderPaymentWizard(orderId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		if (dialog.open() == Dialog.OK) {
			refreshPayments();
			layoutComposite();
		}
	}

	private void closeOrder() {
		List<PaymentRecord> payments = paymentDelegate.getPaymentsForOrder(orderId);
		for (PaymentRecord payment : payments) {
			if (payment.getPaymentType() == PaymentType.PENDING) {
				MessageDialogs.displayError(composite.getShell(), "Unable To Close",
						"Order has pending payments, cannot close.");
				return;
			}
		}
		BigDecimal paymentTotal = Calculator.calculatePaymentTotal(payments);
		OrderRecord orderRecord = orderDelegate.loadOrderRecord(orderId);
		BigDecimal orderTotal = Calculator.calculateOrderTotal(orderRecord);
		if (!Calculator.amountsMatch(paymentTotal, orderTotal)) {
			MessageDialogs.displayError(composite.getShell(), "Unable To Close",
					"Order total does not match the payments made.");
			return;
		}
		if (!MessageDialogs.askQuestion(composite.getShell(), "Close Order",
				"Are you sure you wish to close this order?")) {
			return;
		}
		orderDelegate.closeOrder(orderId, AppPrefs.currentSession.prsnlId);
		if (MessageDialogs.askQuestion(composite.getShell(), "Successfully Closed",
				"Order was successfully closed, do you wish to print a reciept?")) {
			orderPrintService.printReciept(orderId);
		}
		refresh();
	}

	private void reopenOrder() {
		if (!MessageDialogs.askQuestion(composite.getShell(), "Reopen Order",
				"Are you sure you wish to reopen this closed order?")) {
			return;
		}
		orderDelegate.reopenOrder(orderId);
		MessageDialogs.displayMessage(composite.getShell(), "Successfully Reopened",
				"Order was successfully reopened.");
		refresh();
	}

	private void deleteOrder() {
		List<PaymentRecord> payments = paymentDelegate.getPaymentsForOrder(orderId);
		if (!payments.isEmpty()) {
			MessageDialogs.displayError(composite.getShell(), "Unable To Delete",
					"This order currently has one or more payments made to it, unable to delete.");
			return;
		}

		if (!MessageDialogs.askQuestion(composite.getShell(), "Delete Order",
				"Are you sure you wish to delete this order?")) {
			return;
		}
		orderDelegate.markInError(orderId);
		MessageDialogs.displayMessage(composite.getShell(), "Successfully Deleted", "Order was successfully deleted.");
		forceCloseView();
	}

	private void refundItems() {
		OrderRefundWizard wizard = new OrderRefundWizard(orderId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		if (dialog.open() == Dialog.OK) {
			refresh();
		}
	}

	private void manageParts() {
		OrderPartWizard wizard = new OrderPartWizard(orderId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		if (dialog.open() == Dialog.OK) {
			refresh();
		}
	}

	private void manageServices() {
		OrderServiceWizard wizard = new OrderServiceWizard(orderId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		if (dialog.open() == Dialog.OK) {
			refresh();
		}
	}

	private void manageFlatRates() {
		OrderFlatRateWizard wizard = new OrderFlatRateWizard(orderId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		if (dialog.open() == Dialog.OK) {
			refresh();
		}
	}

	private void unfocusTables(OfficeManagerTable table) {
		if (table != composite.compositePaymentTable.table) {
			composite.compositePaymentTable.table.deselectAll();
		}
		if (table != composite.compositeClientProductTable.table) {
			composite.compositeClientProductTable.table.deselectAll();
		}
		if (table != composite.compositePartTable.table) {
			composite.compositePartTable.table.deselectAll();
		}
		if (table != composite.compositeServiceTable.table) {
			composite.compositeServiceTable.table.deselectAll();
		}
		if (table != composite.compositeFlatRateTable.table) {
			composite.compositeFlatRateTable.table.deselectAll();
		}
	}

	private void openPartDetails() {
		OrderPartRecord partRecord = orderPartService.getSelectedPart();

		if (partRecord == null) {
			logger.error("Attempting to open an order part without any parts selected.");
			return;
		}
		EditOrderPartDialog dialog = new EditOrderPartDialog(composite.getShell(), orderId,
				partRecord.getOrderToPartId());

		if (dialog.open()) {
			refresh();
		}
	}

	private void openFlatRateDetails() {
		OrderFlatRateServiceRecord flatRateRecord = orderFlatRateService.getSelectedFlatRate();
		if (flatRateRecord == null) {
			logger.error("Attempting to open an order flat rate without any selected.");
			return;
		}
		EditOrderFlatRateDialog dialog = new EditOrderFlatRateDialog(composite.getShell(), orderId,
				flatRateRecord.getOrderToFrsId());

		if (dialog.open()) {
			refresh();
		}
	}

	private void openServiceDetails() {
		OrderServiceRecord serviceRecord = orderServiceService.getSelectedService();
		if (serviceRecord == null) {
			logger.error("Attempting to open an order service without any services selected.");
			return;
		}
		EditOrderServiceDialog dialog = new EditOrderServiceDialog(composite.getShell(), orderId,
				serviceRecord.getServiceId());

		if (dialog.open()) {
			refresh();
		}
	}
}
