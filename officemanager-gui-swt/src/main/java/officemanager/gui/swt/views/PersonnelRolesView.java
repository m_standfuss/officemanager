package officemanager.gui.swt.views;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.SecurityDelegate;
import officemanager.biz.records.PersonnelRoleRecord;
import officemanager.biz.records.RoleSelectionRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PersonnelRolesComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelRolesView extends OfficeManagerSaveableView {

	private static final String DATA_ROLE_SELECTION = "DATA_ROLE_SELECTION";

	private final Long personnelId;
	private final SecurityDelegate securityDelegate;
	private final List<PersonnelRoleRecord> currentRoles;
	private final List<PersonnelRoleRecord> rolesToAdd;
	private final List<PersonnelRoleRecord> rolesToDelete;
	private PersonnelRolesComposite composite;

	public PersonnelRolesView(Long personnelId) {
		this.personnelId = personnelId;
		currentRoles = Lists.newArrayList();
		rolesToAdd = Lists.newArrayList();
		rolesToDelete = Lists.newArrayList();
		securityDelegate = new SecurityDelegate();
	}

	@Override
	public void saveView() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			for (PersonnelRoleRecord toAdd : rolesToAdd) {
				securityDelegate.addRole(toAdd);
			}
			rolesToAdd.clear();
			for (PersonnelRoleRecord toDelete : rolesToDelete) {
				securityDelegate.removeRole(toDelete);
			}
			rolesToDelete.clear();
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful Save",
				"Successfully saved the personnel roles.");
		changeSaveState(false);
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new PersonnelRolesComposite(parent, style);
		composite.btnAddRole.addListener(SWT.Selection, e -> addRole());
		setEndDateEnabled(false);
		composite.btnNeverEnding.addListener(SWT.Selection,
				e -> setEndDateEnabled(!composite.btnNeverEnding.getSelection()));

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			populateRoleDropDown();
			populateActiveRoles();
		});
		return composite;
	}

	@Override
	public String getViewName() {
		return "Personnel Roles";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.USER_SECURITY, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	private void addRole() {
		if (composite.cmbRole.getSelectionIndex() == -1) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Add", "Please select a role to add.");
			return;
		}
		Long roleId = (Long) composite.cmbRole.getData(DATA_ROLE_SELECTION + composite.cmbRole.getSelectionIndex());
		for (PersonnelRoleRecord currentRole : currentRoles) {
			if (currentRole.getRoleId().equals(roleId)) {
				MessageDialogs.displayError(composite.getShell(), "Cannot Add",
						"The personnel already has this role assigned to them.");
				return;
			}
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			Date endEffectiveDttm = composite.dateAndTimeEndEffective.getDate();
			if (!composite.btnNeverEnding.getSelection()) {
				if (endEffectiveDttm.before(new Date())) {
					MessageDialogs.displayError(composite.getShell(), "Cannot Add",
							"Please select and ending effective date and time in the future.");
					return;
				}
			}

			PersonnelRoleRecord record = new PersonnelRoleRecord();
			if (composite.btnNeverEnding.getSelection()) {
				record.setEndEffectiveDttm(null);
			} else {
				record.setEndEffectiveDttm(endEffectiveDttm);
			}
			record.setPersonnelId(personnelId);
			record.setRoleId(roleId);
			record.setRoleName(composite.cmbRole.getText());
			rolesToAdd.add(record);
			changeSaveState(true);
			addRoleToComposite(record);
		});
	}

	private void setEndDateEnabled(boolean enabled) {
		composite.dateAndTimeEndEffective.setEnabled(enabled);
	}

	private void populateRoleDropDown() {
		List<RoleSelectionRecord> roleRecords = securityDelegate.getActiveRoles();
		Collections.sort(roleRecords);

		composite.cmbRole.removeAll();
		for (int i = 0; i < roleRecords.size(); i++) {
			RoleSelectionRecord record = roleRecords.get(i);
			composite.cmbRole.add(record.getRoleName(), i);
			composite.cmbRole.setData(DATA_ROLE_SELECTION + i, record.getRoleId());
		}
	}

	private void populateActiveRoles() {
		List<PersonnelRoleRecord> activeRoles = securityDelegate.getActivePersonnelRoles(personnelId);
		for (PersonnelRoleRecord role : activeRoles) {
			addRoleToComposite(role);
		}

		composite.scrolledCompositeRoles.setContent(composite.compositeRoles);
		composite.scrolledCompositeRoles.setMinSize(composite.compositeRoles.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	private void addRoleToComposite(PersonnelRoleRecord role) {
		currentRoles.add(role);
		Link link = new Link(composite.compositeRoles, SWT.NONE);
		link.setText("<a>" + role.getRoleName() + "</a>");
		link.addListener(SWT.Selection, e -> changeView(new RoleEditView(role.getRoleId())));

		Label lblNewLabel = new Label(composite.compositeRoles, SWT.NONE);
		lblNewLabel.setText(AppPrefs.dateFormatter.formatFullDate_DB(role.getEndEffectiveDttm()));

		Button btnRemoveRole = new Button(composite.compositeRoles, SWT.NONE);
		btnRemoveRole.setText("Remove");
		btnRemoveRole.addListener(SWT.Selection, e -> {
			link.dispose();
			lblNewLabel.dispose();
			btnRemoveRole.dispose();
			currentRoles.remove(role);
			rolesToDelete.add(role);
			changeSaveState(true);
			composite.compositeRoles.layout();
		});
		composite.compositeRoles.layout();
	}

}
