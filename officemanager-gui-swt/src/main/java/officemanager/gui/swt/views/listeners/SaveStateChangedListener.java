
package officemanager.gui.swt.views.listeners;

public abstract class SaveStateChangedListener {

	public abstract void saveStateChanged(boolean canSave);

}