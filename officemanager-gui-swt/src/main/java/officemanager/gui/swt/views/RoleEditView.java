package officemanager.gui.swt.views;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import officemanager.biz.delegates.SecurityDelegate;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PermissionRecord;
import officemanager.biz.records.RolePermissionRecord;
import officemanager.biz.records.RoleRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.RolePermissionsComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class RoleEditView extends OfficeManagerSaveableView {

	private static final Logger logger = LogManager.getLogger(RoleEditView.class);
	private static final String DATA_PERMISSION_NAME = "DATA_PERMISSION_NAME";
	private static final String DATA_ROLE_PERMISSION_RECORD = "DATA_ROLE_PERMISSION_RECORD";

	private final Long roleId;
	private final SecurityDelegate securityDelegate;
	private final List<Button> permissionCheckItems;

	private RoleRecord recordInContext;
	private RolePermissionsComposite composite;

	public RoleEditView() {
		this(null);
	}

	public RoleEditView(Long roleId) {
		this.roleId = roleId;
		securityDelegate = new SecurityDelegate();
		permissionCheckItems = Lists.newArrayList();
	}

	@Override
	public void saveView() {
		logger.debug("Starting save for role.");
		if (composite.txtRoleName.getText().isEmpty()) {
			MessageDialogs.displayError(composite.getShell(), "Unable To Save", "Please populate a role name.");
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			if (recordInContext == null) {
				recordInContext = new RoleRecord();
			}
			recordInContext.setRoleName(composite.txtRoleName.getText());
			List<RolePermissionRecord> permissionRecords = Lists.newArrayList();
			for (Button btn : permissionCheckItems) {
				if (!btn.getSelection()) {
					continue;
				}
				RolePermissionRecord permissionRecord = (RolePermissionRecord) btn.getData(DATA_ROLE_PERMISSION_RECORD);
				if (permissionRecord == null) {
					permissionRecord = new RolePermissionRecord();
					permissionRecord.setPermissionName((String) btn.getData(DATA_PERMISSION_NAME));
				}
				permissionRecords.add(permissionRecord);
			}
			recordInContext.setPermissionRecords(permissionRecords);
			Long roleId = securityDelegate.persistRoleRecord(recordInContext);
			recordInContext.setRoleId(roleId);
			changeSaveState(false);
		});
		MessageDialogs.displayMessage(composite.getShell(), "Success", "Successfully saved the role.");
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new RolePermissionsComposite(parent, style);
		composite.txtRoleName.setEditable(AppPrefs.hasPermission(AppPermissions.SECURITY_EDIT_ROLE));
		composite.txtRoleName.addListener(SWT.Modify, e -> changeSaveState(true));

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			populateComposite();
			fillComposite();
		});
		changeSaveState(false);
		return composite;
	}

	@Override
	public String getViewName() {
		return "Role Permissions";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.USER_SECURITY, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	private void populateComposite() {
		List<PermissionRecord> permissions = securityDelegate.getActivePermissions();

		Map<CodeValueRecord, List<PermissionRecord>> permissionMap = Maps.newHashMap();
		for (PermissionRecord permission : permissions) {
			if (!permissionMap.containsKey(permission.getCategory())) {
				permissionMap.put(permission.getCategory(), Lists.newArrayList());
			}
			permissionMap.get(permission.getCategory()).add(permission);
		}

		for (Map.Entry<CodeValueRecord, List<PermissionRecord>> entry : permissionMap.entrySet()) {
			Label categoryLbl = new Label(composite.compositePermissions, SWT.NONE);
			categoryLbl.setText(entry.getKey().getDisplay());
			categoryLbl.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

			for (PermissionRecord permission : entry.getValue()) {
				Button btnCheckButton = new Button(composite.compositePermissions, SWT.CHECK);
				btnCheckButton.setEnabled(AppPrefs.hasPermission(AppPermissions.SECURITY_EDIT_ROLE));
				btnCheckButton.setText(permission.getDisplay());
				btnCheckButton.setData(DATA_PERMISSION_NAME, permission.getPermissionName());
				btnCheckButton.addListener(SWT.Selection, e -> changeSaveState(true));
				permissionCheckItems.add(btnCheckButton);
			}
		}
		composite.scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	private void fillComposite() {
		if (roleId == null) {
			return;
		}
		recordInContext = securityDelegate.getRole(roleId);
		composite.txtRoleName.setText(recordInContext.getRoleName());
		changeViewName("Role Permissions-" + recordInContext.getRoleName());

		Map<String, RolePermissionRecord> permissionRecordMap = Maps.newHashMap();
		for (RolePermissionRecord permissionRecord : recordInContext.getPermissionRecords()) {
			permissionRecordMap.put(permissionRecord.getPermissionName(), permissionRecord);
		}

		for (Button btn : permissionCheckItems) {
			String permissionName = (String) btn.getData(DATA_PERMISSION_NAME);
			if (permissionRecordMap.containsKey(permissionName)) {
				btn.setSelection(true);
				btn.setData(DATA_ROLE_PERMISSION_RECORD, permissionRecordMap.get(permissionName));
			} else {
				btn.setSelection(false);
			}
		}
	}
}