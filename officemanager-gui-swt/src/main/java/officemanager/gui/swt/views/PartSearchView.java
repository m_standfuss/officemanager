package officemanager.gui.swt.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.PartSearchService;
import officemanager.gui.swt.services.PartSearchTableService;
import officemanager.gui.swt.ui.composites.PartSearchComposite;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.PartWizard;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PartSearchView extends OfficeManagerView {

	private static final Logger logger = LogManager.getLogger(PartSearchView.class);

	private final PartSearchService partSearchService;

	private PartSearchComposite composite;

	public PartSearchView() {
		partSearchService = new PartSearchService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		logger.debug("Creating the searching composite");

		composite = new PartSearchComposite(parent, style);
		composite.searchResultsTable.table.addListener(SWT.MouseDoubleClick, e -> openPart());
		partSearchService.setComposite(composite);

		addListeners();
		return composite;
	}

	@Override
	public void closeView() {
		if (composite != null) {
			composite.dispose();
			composite = null;
		}
	}

	@Override
	public String getViewName() {
		return "Part Search";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.SEARCH, AppPrefs.ICN_SIZE_VIEW);
	}

	private void addListeners() {
		composite.tlItmEnterInventory.addListener(SWT.Selection, e -> changeView(new InventoryEntryView()));
		composite.tlItmNewPart.addListener(SWT.Selection, e -> {
			PartWizard wizard = new PartWizard();
			OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
			dialog.setBlockOnOpen(true);
			if (dialog.open() == Window.OK) {
				changeView(wizard.getReturnView());
			}
		});
	}

	private void openPart() {
		if (composite.searchResultsTable.table.getSelectionCount() == 0) {
			logger.error("No part row is currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.searchResultsTable.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Opening the first of the two.");
		}
		TableItem partRow = selectedRows[0];
		PartSearchRecord record = (PartSearchRecord) partRow.getData(PartSearchTableService.DATA_RECORD);
		changeView(new PartView(record.getPartId()));
	}
}
