package officemanager.gui.swt.views;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.delegates.ClientProductDelegate;
import officemanager.biz.delegates.OrderSearchDelegate;
import officemanager.biz.records.AddressRecord;
import officemanager.biz.records.ClientProductSelectionRecord;
import officemanager.biz.records.ClientRecord;
import officemanager.biz.records.OrderSearchRecord;
import officemanager.biz.records.PhoneRecord;
import officemanager.biz.searchingcriteria.OrderSearchCriteria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.AddressService;
import officemanager.gui.swt.services.ClientInformationService;
import officemanager.gui.swt.services.ClientProductService;
import officemanager.gui.swt.services.OrderSearchService;
import officemanager.gui.swt.services.PhoneService;
import officemanager.gui.swt.ui.composites.ClientComposite;
import officemanager.gui.swt.ui.tables.OrderTableComposite;
import officemanager.gui.swt.ui.wizards.ClientWizard;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.DateUtility;

public class ClientView extends OfficeManagerView implements IRefreshableView {

	private static final Logger logger = LogManager.getLogger(ClientView.class);

	private static final List<Integer> OPEN_ORDERS_COLUMNS = Lists.newArrayList(OrderTableComposite.COL_ORDER_NUM,
			OrderTableComposite.COL_ORDER_TYPE, OrderTableComposite.COL_AMOUNT, OrderTableComposite.COL_ORDER_STATUS,
			OrderTableComposite.COL_OPENED_DTTM, OrderTableComposite.COL_OPENED_BY,
			OrderTableComposite.COL_PRODUCT_CNT);

	private static final List<Integer> CLOSED_ORDERS_COLUMNS = Lists.newArrayList(OrderTableComposite.COL_ORDER_NUM,
			OrderTableComposite.COL_ORDER_TYPE, OrderTableComposite.COL_AMOUNT, OrderTableComposite.COL_ORDER_STATUS,
			OrderTableComposite.COL_PRODUCT_CNT, OrderTableComposite.COL_CLOSED_DTTM,
			OrderTableComposite.COL_CLOSED_BY);

	private final Long clientId;

	private final OrderSearchDelegate orderSearchDelegate;
	private final ClientDelegate clientDelegate;
	private final ClientProductDelegate clientProductDelegate;

	private final ClientInformationService clientInformationService;
	private final ClientProductService productService;
	private final AddressService addressService;
	private final PhoneService phoneService;
	private final OrderSearchService openOrdersService;
	private final OrderSearchService closedOrdersService;

	private Long personId;
	private ClientComposite composite;

	public ClientView(Long clientId) {
		this.clientId = clientId;

		orderSearchDelegate = new OrderSearchDelegate();
		clientDelegate = new ClientDelegate();
		clientProductDelegate = new ClientProductDelegate();

		clientInformationService = new ClientInformationService();
		addressService = new AddressService();
		phoneService = new PhoneService();
		productService = new ClientProductService();
		openOrdersService = new OrderSearchService(OPEN_ORDERS_COLUMNS);
		closedOrdersService = new OrderSearchService(CLOSED_ORDERS_COLUMNS);
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new ClientComposite(parent, style);
		composite.tlItmEditClient.addListener(SWT.Selection, e -> openEditWizard());

		composite.currentOrdersTblCmp.table.addListener(SWT.MouseDoubleClick, e -> openCurrentOrder());
		composite.pastOrdersTblCmp.table.addListener(SWT.MouseDoubleClick, e -> openClosedOrder());
		composite.productTblCmp.table.addListener(SWT.MouseDoubleClick, e -> openClientProduct());
		composite.phoneTblCmp.table.addListener(SWT.MouseDoubleClick, e -> openPhone());
		composite.addressTblCmp.table.addListener(SWT.MouseDoubleClick, e -> openAddress());

		clientInformationService.setComposite(composite);
		productService.setComposite(composite.productTblCmp);
		addressService.setComposite(composite.addressTblCmp);
		phoneService.setComposite(composite.phoneTblCmp);
		openOrdersService.setComposite(composite.currentOrdersTblCmp);
		closedOrdersService.setComposite(composite.pastOrdersTblCmp);
		return composite;
	}

	@Override
	public String getViewName() {
		return "Client";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.USER_INFO, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			ClientRecord record = clientDelegate.getClientRecord(clientId);
			changeViewName("Client - " + record.getNameFullFormatted());
			personId = record.getPersonId();
			List<ClientProductSelectionRecord> clientProducts = clientProductDelegate
					.getActiveClientProductSelections(clientId);
			List<OrderSearchRecord> currentOrders = orderSearchDelegate.searchCurrentOrders(getCurrentOrderCriteria());
			List<OrderSearchRecord> pastOrders = orderSearchDelegate.searchClosedOrders(getClosedOrderCriteria());

			productService.populateTable(clientProducts);
			clientInformationService.refresh(record);
			phoneService.populatePhoneTable(record.getPhoneRecords());
			addressService.populateAddress(record.getAddressRecords());
			openOrdersService.populateTable(currentOrders);
			closedOrdersService.populateTable(pastOrders);
			composite.composite.layout();
			composite.scrComposite.setMinSize(-1, composite.composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		});
	}

	private OrderSearchCriteria getClosedOrderCriteria() {
		OrderSearchCriteria criteria = new OrderSearchCriteria();
		criteria.clientId = clientId;
		criteria.begClosedDttm = DateUtility.addDays(new Date(), -30);
		criteria.endClosedDttm = new Date();
		return criteria;
	}

	private OrderSearchCriteria getCurrentOrderCriteria() {
		OrderSearchCriteria criteria = new OrderSearchCriteria();
		criteria.clientId = clientId;
		return criteria;
	}

	private void openEditWizard() {
		ClientWizard wizard = new ClientWizard(clientId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Dialog.OK) {
			refresh();
		}
	}

	private void openPhone() {
		if (composite.phoneTblCmp.table.getSelectionCount() == 0) {
			logger.error("No phone rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.phoneTblCmp.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one phone is currently selected. Opening the first of the two.");
		}
		TableItem phoneRow = selectedRows[0];
		PhoneRecord record = (PhoneRecord) phoneRow.getData(PhoneService.DATA_RECORD);
		changeView(new PhoneEditView(record.getPhoneId(), personId));
	}

	private void openAddress() {
		if (composite.addressTblCmp.table.getSelectionCount() == 0) {
			logger.error("No address rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.addressTblCmp.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one phone is currently selected. Opening the first of the two.");
		}
		TableItem addressRow = selectedRows[0];
		AddressRecord record = (AddressRecord) addressRow.getData(AddressService.DATA_RECORD);
		changeView(new AddressEditView(record.getAddressId(), personId));
	}

	private void openClientProduct() {
		if (composite.productTblCmp.table.getSelectionCount() == 0) {
			logger.error("No client products rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.productTblCmp.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one product is currently selected. Opening the first of the two.");
		}
		TableItem productRow = selectedRows[0];
		ClientProductSelectionRecord record = (ClientProductSelectionRecord) productRow
				.getData(ClientProductService.DATA_SELECTION_RECORD);
		changeView(new ClientProductView(record.getClientProductId(), clientId));
	}

	private void openClosedOrder() {
		Long orderId = closedOrdersService.getSelectedOrderId();
		if (orderId == null) {
			logger.error("No order was selected cannot open.");
			return;
		}
		changeView(new OrderView(orderId));
	}

	private void openCurrentOrder() {
		Long orderId = openOrdersService.getSelectedOrderId();
		if (orderId == null) {
			logger.error("No order was selected cannot open.");
			return;
		}
		changeView(new OrderView(orderId));
	}
}
