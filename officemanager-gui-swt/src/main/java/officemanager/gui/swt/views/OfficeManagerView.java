package officemanager.gui.swt.views;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import com.google.common.collect.Lists;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.views.listeners.ChangeViewListener;
import officemanager.gui.swt.views.listeners.ChangeViewNameListener;

public abstract class OfficeManagerView implements IOfficeManagerView {

	private final List<ChangeViewListener> changeViewListeners;
	private final List<ChangeViewNameListener> changeViewNameListeners;
	private final List<Listener> forceCloseListeners;

	public OfficeManagerView() {
		changeViewListeners = Lists.newArrayList();
		changeViewNameListeners = Lists.newArrayList();
		forceCloseListeners = Lists.newArrayList();
	}

	@Override
	public void clearListeners() {
		changeViewListeners.clear();
		changeViewNameListeners.clear();
	}

	@Override
	public void addChangeViewListener(ChangeViewListener l) {
		checkNotNull(l);
		changeViewListeners.add(l);
	}

	@Override
	public void addChangeViewNameListener(ChangeViewNameListener l) {
		checkNotNull(l);
		changeViewNameListeners.add(l);
	}

	@Override
	public void addForceCloseListener(Listener l) {
		checkNotNull(l);
		forceCloseListeners.add(l);
	}

	protected void changeView(IOfficeManagerView view) {
		for (ChangeViewListener l : changeViewListeners) {
			l.changeView(view);
		}
	}

	protected void changeViewName(String newName) {
		for (ChangeViewNameListener l : changeViewNameListeners) {
			l.changeViewName(newName);
		}
	}

	protected void forceCloseView() {
		for (Listener l : forceCloseListeners) {
			l.handleEvent(new Event());
		}
	}

	protected Date convertToBeginningOfDayUTC(Date date) {
		Calendar instance = Calendar.getInstance(AppPrefs.localTimeZone);
		instance.setTime(date);

		instance.set(Calendar.HOUR_OF_DAY, 0);
		instance.set(Calendar.MINUTE, 0);
		instance.set(Calendar.SECOND, 0);
		return instance.getTime();
	}

	/**
	 * Converts a {@link DateTime} UI object into the beginning of the date
	 * selected in the local timezone of the application.
	 * 
	 * @param dateTime
	 *            The date and time UI object.
	 * @return The corresponding {@link Date} object corresponding to the
	 *         beginning of the day selected.
	 */
	protected Date convertToBeginningOfDayUTC(DateTime dateTime) {
		Calendar instance = Calendar.getInstance(AppPrefs.localTimeZone);
		instance.set(Calendar.DAY_OF_MONTH, dateTime.getDay());
		instance.set(Calendar.MONTH, dateTime.getMonth());
		instance.set(Calendar.YEAR, dateTime.getYear());

		instance.set(Calendar.HOUR_OF_DAY, 0);
		instance.set(Calendar.MINUTE, 0);
		instance.set(Calendar.SECOND, 0);
		return instance.getTime();
	}

	protected Date convertToEndOfDayUTC(Date date) {
		Calendar instance = Calendar.getInstance(AppPrefs.localTimeZone);
		instance.setTime(date);

		instance.set(Calendar.HOUR_OF_DAY, 23);
		instance.set(Calendar.MINUTE, 59);
		instance.set(Calendar.SECOND, 59);
		instance.set(Calendar.MILLISECOND, 999);
		return instance.getTime();
	}

	/**
	 * Converts a {@link DateTime} UI object into the beginning of the date
	 * selected in the local timezone of the application.
	 * 
	 * @param dateTime
	 *            The date and time UI object.
	 * @return The corresponding {@link Date} object corresponding to the
	 *         beginning of the day selected.
	 */
	protected Date convertToEndOfDayUTC(DateTime dateTime) {
		Calendar instance = Calendar.getInstance(AppPrefs.localTimeZone);
		instance.set(Calendar.DAY_OF_MONTH, dateTime.getDay());
		instance.set(Calendar.MONTH, dateTime.getMonth());
		instance.set(Calendar.YEAR, dateTime.getYear());

		instance.set(Calendar.HOUR_OF_DAY, 23);
		instance.set(Calendar.MINUTE, 59);
		instance.set(Calendar.SECOND, 59);
		instance.set(Calendar.MILLISECOND, 999);
		return instance.getTime();
	}

	protected void hideControl(Control control, boolean exclude) {
		if (control.getLayoutData() == null) {
			control.setLayoutData(new GridData());
		}
		((GridData) control.getLayoutData()).exclude = exclude;
		control.setVisible(!exclude);
	}
}
