package officemanager.gui.swt.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.delegates.FlatRateServiceDelegate;
import officemanager.biz.records.FlatRateServiceRecord;
import officemanager.biz.records.LongTextRecord;
import officemanager.biz.records.LongTextRecord.TextFormat;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.FlatRateEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.StringUtility;

public class FlatRateEditView extends OfficeManagerSaveableView {

	private final Long flatRateServiceId;
	private final FlatRateServiceDelegate flatRateServiceDelegate;

	private FlatRateEditComposite composite;
	private FlatRateServiceRecord serviceInContext;

	public FlatRateEditView() {
		this(null);
	}

	public FlatRateEditView(Long flatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;
		flatRateServiceDelegate = new FlatRateServiceDelegate();
	}

	@Override
	public void saveView() {
		if (!requriedFieldsFilled()) {
			MessageDialogs.displayError(composite.getShell(), "Unable to Save", "Please fill out all required fields.");
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			if (serviceInContext == null) {
				serviceInContext = new FlatRateServiceRecord();
			}

			serviceInContext.setBarcode(composite.txtBarcode.getText());
			serviceInContext.setBasePrice(composite.txtBasePrice.getValue());
			serviceInContext.setCategory(composite.cmbCategory.getCodeValueRecord());
			serviceInContext.setProductType(composite.cmbProductType.getCodeValueRecord());
			serviceInContext.setServiceName(composite.txtServiceName.getText());

			LongTextRecord serviceDescription;
			if (composite.txtDescription.getText().isEmpty()) {
				serviceDescription = null;
			} else {
				if (serviceInContext.getServiceDescription() != null) {
					serviceDescription = serviceInContext.getServiceDescription();
				} else {
					serviceDescription = new LongTextRecord();
				}
				serviceDescription.setFormat(TextFormat.PLAIN_TEXT);
				serviceDescription.setLongText(composite.txtDescription.getText());
			}
			serviceInContext.setServiceDescription(serviceDescription);

			serviceInContext.setFlatRateServiceId(flatRateServiceDelegate.persistFlatRateService(serviceInContext));
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully saved flat rate service.");
		changeSaveState(false);
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new FlatRateEditComposite(parent, style);
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.cmbCategory.populateSelections(flatRateServiceDelegate.getFlatRateServiceCategories());
			composite.cmbProductType.populateSelections(flatRateServiceDelegate.getProductTypes());
			populateComposite();
		});

		composite.cmbCategory.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.cmbProductType.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.txtBarcode.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtBasePrice.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtDescription.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtServiceName.addListener(SWT.Modify, e -> changeSaveState(true));
		return composite;
	}

	@Override
	public String getViewName() {
		return "Edit Flat Rate";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.EDIT, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	private void populateComposite() {
		if (flatRateServiceId == null) {
			changeViewName("New Flat Rate Service");
			return;
		}
		serviceInContext = flatRateServiceDelegate.getFlatRateService(flatRateServiceId);
		composite.cmbCategory.setSelectedValue(serviceInContext.getCategory());
		composite.cmbProductType.setSelectedValue(serviceInContext.getProductType());
		composite.txtBarcode.setText(serviceInContext.getBarcode());
		composite.txtBasePrice.setValue(serviceInContext.getBasePrice());
		composite.txtServiceName.setText(serviceInContext.getServiceName());

		String description = "";
		if (serviceInContext.getServiceDescription() != null) {
			description = serviceInContext.getServiceDescription().getLongText();
		}
		composite.txtDescription.setText(description);
	}

	private boolean requriedFieldsFilled() {
		if (StringUtility.isNullOrWhiteSpace(composite.txtServiceName.getText())) {
			return false;
		}
		if (composite.cmbCategory.getSelectionIndex() == -1) {
			return false;
		}
		if (composite.cmbProductType.getSelectionIndex() == -1) {
			return false;
		}
		if (composite.txtBasePrice.getValue() == null) {
			return false;
		}
		return true;
	}
}
