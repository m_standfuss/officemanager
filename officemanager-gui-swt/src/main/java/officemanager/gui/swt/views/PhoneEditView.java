package officemanager.gui.swt.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.biz.delegates.PersonDelegate;
import officemanager.biz.records.PhoneRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PhoneEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class PhoneEditView extends OfficeManagerSaveableView {
	private static final Logger logger = LogManager.getLogger(PhoneEditView.class);

	private final Long phoneId;
	private final Long personId;
	private final PersonDelegate personDelegate;

	private PhoneRecord phoneInContext;
	private PhoneEditComposite composite;
	private ToolBar toolBar;
	private ToolItem tlItmRemoveAddress;

	public PhoneEditView(Long phoneId, Long personId) {
		this.phoneId = phoneId;
		this.personId = personId;
		personDelegate = new PersonDelegate();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		Composite root = new Composite(parent, style);
		root.setLayout(new GridLayout());

		toolBar = new ToolBar(root, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		tlItmRemoveAddress = new ToolItem(toolBar, SWT.NONE);
		tlItmRemoveAddress.setEnabled(phoneId != null);
		tlItmRemoveAddress.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmRemoveAddress.setToolTipText("Remove Phone");
		tlItmRemoveAddress.addListener(SWT.Selection, e -> removePhone());

		composite = new PhoneEditComposite(root, SWT.NONE);
		composite.btnPrimaryPhone.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.cmbPhoneType.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.txtExtension.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtPhone.addListener(SWT.Modify, e -> changeSaveState(true));

		populateComposite();
		return root;
	}

	@Override
	public String getViewName() {
		return "Phone Number";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.PHONE, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void saveView() {
		if (!validate()) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			phoneInContext.setExtension(composite.txtExtension.getText());
			phoneInContext.setPersonId(personId);
			phoneInContext.setPhoneNumber(composite.txtPhone.getUnformattedText());
			phoneInContext.setPhoneType(composite.cmbPhoneType.getCodeValueRecord());
			phoneInContext.setPrimaryPhone(composite.btnPrimaryPhone.getSelection());

			Long phoneId = personDelegate.persistPhoneRecord(phoneInContext);
			phoneInContext = new PhoneRecord(phoneId, phoneInContext);
			tlItmRemoveAddress.setEnabled(true);
			changeSaveState(false);
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully Saved Phone");
	}

	private boolean validate() {
		if (!composite.txtPhone.isValidPhoneNumber()) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Invalid phone number.");
			return false;
		}
		if (composite.cmbPhoneType.getSelectionIndex() == -1) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Please select a phone type.");
			return false;
		}
		return true;
	}

	private void populateComposite() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.cmbPhoneType.populateSelections(personDelegate.getPhoneTypes());

			if (phoneId == null) {
				logger.debug("Creating a new phone record for personId=" + personId);
				phoneInContext = new PhoneRecord();
				phoneInContext.setPersonId(personId);
			} else {
				logger.debug("Existing phone attempting to obtain.");
				phoneInContext = personDelegate.getPhoneRecord(phoneId);
				if (phoneInContext == null) {
					logger.warn("Unable to load previous phone, creating new one instead.");
					phoneInContext = new PhoneRecord();
					phoneInContext.setPersonId(personId);
				}
			}

			composite.btnPrimaryPhone.setSelection(phoneInContext.isPrimaryPhone());
			composite.cmbPhoneType.setSelectedValue(phoneInContext.getPhoneType());
			composite.txtExtension.setText(phoneInContext.getExtension());
			composite.txtPhone.setText(phoneInContext.getPhoneNumber());
		});
	}

	private void removePhone() {
		if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm Deletion",
				"Are you sure you wish to delete this phone number?")) {
			return;
		}
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			personDelegate.deactivatePhone(phoneId);
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully Removed Phone");
		forceCloseView();
	}
}
