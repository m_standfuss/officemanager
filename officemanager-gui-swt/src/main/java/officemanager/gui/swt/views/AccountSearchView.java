package officemanager.gui.swt.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.records.AccountSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.AccountSearchService;
import officemanager.gui.swt.ui.composites.AccountSearchComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class AccountSearchView extends OfficeManagerView {
	private static final Logger logger = LogManager.getLogger(AccountSearchView.class);

	private final AccountSearchService accountSearchService;
	private AccountSearchComposite composite;

	public AccountSearchView() {
		accountSearchService = new AccountSearchService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new AccountSearchComposite(parent, style);
		composite.searchResultsTable.table.addListener(SWT.MouseDoubleClick, e -> openAccountView());
		accountSearchService.setComposite(composite);
		return composite;
	}

	@Override
	public String getViewName() {
		return "Account Search";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.USER_SEARCH, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		if (composite == null) {
			composite.dispose();
		}
	}

	private void openAccountView() {
		AccountSearchRecord selectedAccount = accountSearchService.getSelectedRow();
		if (selectedAccount == null) {
			logger.error("No account row is currently selected.");
			return;
		}
		changeView(new AccountView(selectedAccount.getAccountId()));
	}

}
