package officemanager.gui.swt.views;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.delegates.LocationDelegate;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.LocationRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.LocationEditComposite;
import officemanager.gui.swt.ui.dialogs.LocationSelectionDialog;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class LocationEditView extends OfficeManagerSaveableView {

	private static final Logger logger = LogManager.getLogger(LocationEditView.class);
	private static final String DATA_PARENT_LOC_ID = "DATA_PARENT_LOC_ID";

	private final LocationDelegate locationDelegate;
	private final Long locationId;

	private LocationRecord locationInContext;
	private LocationEditComposite composite;

	public LocationEditView(Long locationId) {
		this.locationId = locationId;
		locationDelegate = new LocationDelegate();
	}

	public LocationEditView() {
		this(null);
	}

	@Override
	public void saveView() {
		if (!validatePage()) {
			logger.debug("Location is not in a valid state to save.");
			MessageDialogs.displayError(composite.getShell(), "Unable to Save", "Please fill out all required fields.");
			return;
		}
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			if (locationInContext == null) {
				locationInContext = new LocationRecord();
			}

			locationInContext.setAbbrDisplay(composite.txtAbbrName.getText());
			locationInContext.setDisplay(composite.txtDisplayName.getText());
			locationInContext.setLocationType(composite.cmbLocationType.getCodeValueRecord());
			locationInContext.setParentLocationId((Long) composite.getData(DATA_PARENT_LOC_ID));

			locationInContext.setLocationId(locationDelegate.insertUpdateLocation(locationInContext));
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully saved location.");
		changeSaveState(false);
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new LocationEditComposite(parent, style);
		addListeners();

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			populateComposite();
			fillComposite();
		});
		return composite;
	}

	@Override
	public String getViewName() {
		return "Edit Location";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.CRATE_EDIT, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	private void addListeners() {
		composite.btnSelectLocation.addListener(SWT.Selection, e -> chooseLocation());
		composite.cmbLocationType.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.txtAbbrName.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtDisplayName.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.btnRootLocation.addListener(SWT.Selection, e -> {
			changeSaveState(true);
			if (composite.btnRootLocation.getSelection()) {
				composite.setData(DATA_PARENT_LOC_ID, null);
				composite.txtParentLocation.setText("");
				composite.btnSelectLocation.setEnabled(false);
			} else {
				composite.btnSelectLocation.setEnabled(true);
			}
		});
	}

	private void populateComposite() {
		List<CodeValueRecord> locationCategories = locationDelegate.getLocationTypes();
		composite.cmbLocationType.populateSelections(locationCategories);
	}

	private void fillComposite() {
		if (locationId == null) {
			changeViewName("New Location");
			return;
		}
		locationInContext = locationDelegate.getLocationRecord(locationId);
		if (locationInContext == null) {
			logger.error("Unable to find location with id = " + locationId);
			return;
		}

		composite.cmbLocationType.setSelectedValue(locationInContext.getLocationType());
		composite.txtAbbrName.setText(locationInContext.getAbbrDisplay());
		composite.txtDisplayName.setText(locationInContext.getDisplay());
		if (locationInContext.getParentLocationId() != null) {
			String parentLocDisplay = locationDelegate.getFullLocationDisplay(locationInContext.getParentLocationId());
			composite.txtParentLocation.setText(parentLocDisplay);
		}
		composite.setData(DATA_PARENT_LOC_ID, locationInContext.getParentLocationId());
	}

	private boolean validatePage() {
		if (composite.cmbLocationType.getSelectionIndex() == -1) {
			return false;
		}
		if (composite.txtAbbrName.getText().isEmpty()) {
			return false;
		}
		if (composite.txtDisplayName.getText().isEmpty()) {
			return false;
		}
		if (!composite.btnRootLocation.getSelection() && composite.getData(DATA_PARENT_LOC_ID) == null) {
			composite.btnRootLocation.setSelection(true);
		}
		return true;
	}

	private void chooseLocation() {
		LocationSelectionDialog dialog = new LocationSelectionDialog(composite.getShell());
		if (dialog.open() == Window.OK) {
			LocationRecord selection = dialog.getSelection();
			composite.setData(DATA_PARENT_LOC_ID, selection.getLocationId());
			String parentLocDisplay = locationDelegate.getFullLocationDisplay(selection.getLocationId());
			composite.txtParentLocation.setText(parentLocDisplay);
			changeSaveState(true);
		}
	}
}
