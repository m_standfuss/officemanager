package officemanager.gui.swt.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.records.LocationRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.LocationTreeService;
import officemanager.gui.swt.ui.composites.LocationTreeComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class LocationTreeView extends OfficeManagerView implements IRefreshableView {

	private final LocationTreeService service;
	private LocationTreeComposite composite;

	public LocationTreeView() {
		service = new LocationTreeService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new LocationTreeComposite(parent, style);
		addListeners();
		service.setComposite(composite);
		refresh();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Locations";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.CRATE, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		service.loadRootLocations();
	}

	private void addListeners() {
		composite.treeLocations.addListener(SWT.MouseDoubleClick, e -> openLocation());
		composite.tltmNewLocation.addListener(SWT.Selection, e -> changeView(new LocationEditView()));
	}

	private void openLocation() {
		LocationRecord locationRecord = service.getSelectedLocation();
		changeView(new LocationView(locationRecord.getLocationId()));
	}
}
