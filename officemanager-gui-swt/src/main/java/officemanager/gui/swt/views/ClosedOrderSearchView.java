package officemanager.gui.swt.views;

import java.util.List;

import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.OrderSearchDelegate;
import officemanager.biz.records.OrderSearchRecord;
import officemanager.biz.searchingcriteria.OrderSearchCriteria;
import officemanager.biz.searchingcriteria.PersonSearchCriteria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.OrderSearchComposite;
import officemanager.gui.swt.ui.tables.OrderTableComposite;

public class ClosedOrderSearchView extends OrderSearchView {

	private static final List<Integer> COLUMNS_TO_KEEP = Lists.newArrayList(OrderTableComposite.COL_ORDER_NUM,
			OrderTableComposite.COL_ORDER_TYPE, OrderTableComposite.COL_CLIENT_NAME,
			OrderTableComposite.COL_CLIENT_PHONE, OrderTableComposite.COL_AMOUNT, OrderTableComposite.COL_PRODUCT_CNT,
			OrderTableComposite.COL_CLOSED_DTTM, OrderTableComposite.COL_CLOSED_BY);

	private final OrderSearchDelegate orderSearchDelegate;
	private OrderSearchComposite composite;

	public ClosedOrderSearchView() {
		orderSearchDelegate = new OrderSearchDelegate();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = super.makeSearchComposite(parent, style);
		composite.lblStartingDate.setText("Closed on");
		return composite;
	}

	@Override
	public void closeView() {
		if (composite != null) {
			composite.dispose();
			composite = null;
		}
	}

	@Override
	protected List<OrderSearchRecord> getSearchResults() {
		OrderSearchCriteria criteria = makeSearchCriteria();
		return orderSearchDelegate.searchClosedOrders(criteria);
	}

	@Override
	protected List<Integer> getColumnsToKeep() {
		return COLUMNS_TO_KEEP;
	}

	private PersonSearchCriteria getPersonSearchCriteria() {
		PersonSearchCriteria criteria = new PersonSearchCriteria();
		criteria.nameFirst = composite.txtClientFirstName.getText();
		criteria.nameLast = composite.txtClientLastName.getText();
		criteria.primaryPhone = composite.txtPrimaryPhone.getUnformattedText();
		return criteria;
	}

	private OrderSearchCriteria makeSearchCriteria() {
		OrderSearchCriteria criteria = new OrderSearchCriteria();
		criteria.begClosedDttm = convertToBeginningOfDayUTC(composite.dateTimeStart);
		criteria.endClosedDttm = convertToEndOfDayUTC(composite.dateTimeEnd);
		if (composite.btnMyOrders.getSelection()) {
			criteria.closedPrsnlId = AppPrefs.currentSession.prsnlId;
		}
		if (composite.txtOrderNumber.getNumber() != null) {
			criteria.orderId = AppPrefs.FORMATTER.unformatOrderNumber(composite.txtOrderNumber.getText());
		}
		criteria.personSearchCriteria = getPersonSearchCriteria();
		return criteria;
	}
}
