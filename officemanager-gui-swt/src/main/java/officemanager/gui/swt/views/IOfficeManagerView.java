package officemanager.gui.swt.views;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;

import officemanager.gui.swt.views.listeners.ChangeViewListener;
import officemanager.gui.swt.views.listeners.ChangeViewNameListener;

public interface IOfficeManagerView {

	public Composite makeComposite(Composite parent, int style);

	public String getViewName();

	public Image getViewIcon();

	public void clearListeners();

	public void addChangeViewListener(ChangeViewListener l);

	public void addChangeViewNameListener(ChangeViewNameListener l);

	public void addForceCloseListener(Listener l);

	public void closeView();
}
