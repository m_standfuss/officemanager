package officemanager.gui.swt.views;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import officemanager.biz.delegates.PartDelegate;
import officemanager.biz.records.PartRecord;
import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.printing.AddInventoryReport;
import officemanager.gui.swt.printing.PaperclipsPrinter;
import officemanager.gui.swt.services.PartSearchTableService;
import officemanager.gui.swt.ui.composites.InventoryEntryComposite;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.PartSelectionWizard;
import officemanager.gui.swt.ui.wizards.PartWizard;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class InventoryEntryView extends OfficeManagerSaveableView {

	private static final Logger logger = LogManager.getLogger(InventoryEntryView.class);

	private static final List<Integer> PART_COLUMNS = Lists.newArrayList(PartSearchTableComposite.COL_BASE_PRICE,
			PartSearchTableComposite.COL_DESCRIPTION, PartSearchTableComposite.COL_INVNTRY_CNT,
			PartSearchTableComposite.COL_MANUF, PartSearchTableComposite.COL_MANUF_ID,
			PartSearchTableComposite.COL_PART_NAME);

	private final PartDelegate partDelegate;
	private final PartSearchTableService partService;

	private InventoryEntryComposite composite;

	public InventoryEntryView() {
		partDelegate = new PartDelegate();
		partService = new PartSearchTableService(PART_COLUMNS);
	}

	@Override
	public void saveView() {
		logger.debug("Saving inventory.");
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			List<PartSearchRecord> parts = partService.getPartSearchRecords();
			logger.debug(parts.size() + " items adding into inventory.");
			Map<Long, Integer> inventoryCountMap = Maps.newHashMap();
			for (PartSearchRecord part : parts) {
				Integer count = inventoryCountMap.get(part.getPartId());
				if (count == null) {
					count = 1;
				} else {
					count = count + 1;
				}
				inventoryCountMap.put(part.getPartId(), count);
			}
			partDelegate.addInventory(inventoryCountMap);
			if (MessageDialogs.askQuestion(composite.getShell(), "Successfully Saved",
					"Successfully saved the inventory. Do you want to print a report?")) {
				AddInventoryReport print = new AddInventoryReport(parts);
				try {
					PaperclipsPrinter.promptAndPrint(composite.getShell(), print);
				} catch (SWTError | Exception e) {
					logger.error("Unable to print the claim ticket.", e);
					MessageDialogs.displayError(composite.getShell(), "Unable To Print",
							"Unexpected error occured while printing.");
				}
			}
			forceCloseView();
		});
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new InventoryEntryComposite(parent, style);
		partService.setComposite(composite.partSearchTableComposite);

		addListeners();
		composite.scannerText.forceFocus();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Inventory Entry";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.INVENTORY, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	private void addListeners() {
		composite.tltmManuallyAdd.addListener(SWT.Selection, e -> {
			PartSelectionWizard wizard = new PartSelectionWizard(1, true);
			OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
			if (dialog.open() == Window.OK) {
				addPart(wizard.getResult());
			}
		});

		composite.tlItmScanPart.addListener(SWT.Selection, e -> {
			Boolean currentFocus = (Boolean) composite.tlItmScanPart.getData();

			if (currentFocus == null || currentFocus == false) {
				composite.tlItmScanPart.setData(true);
				composite.tlItmScanPart
						.setImage(ResourceManager.getImage(ImageFile.SCAN_CANCEL, AppPrefs.ICN_SIZE_PAGETOOLITEM));
				composite.scannerText.forceFocus();
			} else {
				composite.tlItmScanPart.setData(false);
				composite.tlItmScanPart
						.setImage(ResourceManager.getImage(ImageFile.SCAN, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			}
		});

		composite.scannerText.addListener(SWT.FocusOut, e -> {
			composite.tlItmScanPart.setData(false);
			composite.tlItmScanPart.setImage(ResourceManager.getImage(ImageFile.SCAN, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		});

		composite.scannerText.addListener(SWT.FocusIn, e -> {
			composite.tlItmScanPart.setData(true);
			composite.tlItmScanPart
					.setImage(ResourceManager.getImage(ImageFile.SCAN_CANCEL, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		});
		composite.scannerText.addEntryEnteredListener(e -> addPart(e.text));
	}

	private void addPart(String barcode) {
		List<PartSearchRecord> parts = partDelegate.searchParts(barcode);
		logger.debug(parts.size() + " parts found for the barcode.");

		if (parts.isEmpty()) {
			if (!AppPrefs.hasPermission(AppPermissions.ADD_PART)) {
				MessageDialogs.displayError(composite.getShell(), "No Part Found",
						"No part was found with a barcode of '" + barcode
								+ "'. Please have someone with the add part permission add this part to the system before inventoring it.");
			} else if (MessageDialogs.askQuestion(composite.getShell(), "No Part Found",
					"No part was found with a barcode of '" + barcode + "'. Would you like to add it now?")) {
				PartRecord part = new PartRecord();
				part.setBarcode(barcode);

				PartWizard wizard = new PartWizard(part);
				OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
				dialog.setBlockOnOpen(true);
				dialog.open();
			}
		} else if (parts.size() > 1) {
			PartSelectionWizard wizard = new PartSelectionWizard(parts.size(), false);
			OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
			dialog.setMessage("Multiple parts were found with that barcode please the parts to add.");
			if (dialog.open() == Window.OK) {
				List<PartSearchRecord> results = wizard.getResults();
				for (PartSearchRecord record : results) {
					addPart(record);
				}
			} else {
				logger.debug("No part was choosen from part selection wizard.");
			}
		} else {
			addPart(parts.get(0));
		}
	}

	private void addPart(PartSearchRecord record) {
		changeSaveState(true);
		partService.addRow(record);
	}

}
