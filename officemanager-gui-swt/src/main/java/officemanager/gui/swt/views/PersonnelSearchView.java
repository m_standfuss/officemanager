package officemanager.gui.swt.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import officemanager.biz.records.PersonnelSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.PersonnelSearchService;
import officemanager.gui.swt.ui.composites.PersonnelSearchComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelSearchView extends OfficeManagerView {

	private static final Logger logger = LogManager.getLogger(PersonnelSearchView.class);

	private final PersonnelSearchService personnelSearchService;
	private PersonnelSearchComposite composite;

	public PersonnelSearchView() {
		personnelSearchService = new PersonnelSearchService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new PersonnelSearchComposite(parent, style);
		personnelSearchService.setComposite(composite);
		composite.searchResultsTable.table.addListener(SWT.MouseDoubleClick, e -> openPersonnelView());
		return composite;
	}

	@Override
	public String getViewName() {
		return "Personnel Search";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.USER_SEARCH, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		if (composite == null) {
			composite.dispose();
		}
	}

	private void openPersonnelView() {
		if (composite.searchResultsTable.table.getSelectionCount() == 0) {
			logger.error("No part row is currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.searchResultsTable.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Opening the first of the two.");
		}
		TableItem partRow = selectedRows[0];
		PersonnelSearchRecord record = (PersonnelSearchRecord) partRow.getData(PersonnelSearchService.DATA_RECORD);
		changeView(new PersonnelView(record.getPersonnelId()));
	}
}
