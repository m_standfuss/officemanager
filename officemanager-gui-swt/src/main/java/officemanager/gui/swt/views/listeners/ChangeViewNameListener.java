package officemanager.gui.swt.views.listeners;

public abstract class ChangeViewNameListener {

	public abstract void changeViewName(String newName);
}
