package officemanager.gui.swt.views;

import java.util.Date;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.delegates.SaleDelegate;
import officemanager.biz.records.FlatRateSearchRecord;
import officemanager.biz.records.PartSearchRecord;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.records.SaleRecord.SaleProductType;
import officemanager.biz.records.SaleRecord.SaleType;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.SaleEditComposite;
import officemanager.gui.swt.ui.wizards.FlatRateSelectionWizard;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.PartSelectionWizard;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class SaleEditView extends OfficeManagerSaveableView {

	private static final String DATA_ENTITY_ID = "DATA_ENTITY_ID";

	private final SaleDelegate saleDelegate;

	private final SaleProductType productType;
	private final Long productId;

	private Long saleId;

	private SaleEditComposite composite;

	public SaleEditView() {
		this(null);
	}

	public SaleEditView(Long saleId) {
		saleDelegate = new SaleDelegate();
		this.saleId = saleId;
		productType = null;
		productId = null;
	}

	public SaleEditView(SaleProductType productType, Long productId) {
		saleDelegate = new SaleDelegate();
		saleId = null;
		this.productId = productId;
		this.productType = productType;
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new SaleEditComposite(parent, style);

		composite.btnFlatAmountOff.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.btnFlatRateService.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.btnNoEnd.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.btnPart.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.btnPercentageOff.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.dateAndTimeEnd.addDateChangeListener(e -> changeSaveState(true));
		composite.dateAndTimeStart.addDateChangeListener(e -> changeSaveState(true));
		composite.txtAmountOff.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtSaleName.addListener(SWT.Modify, e -> changeSaveState(true));

		composite.btnSearchItems.addListener(SWT.Selection, l -> searchItems());
		composite.btnNoEnd.addListener(SWT.Selection, l -> toggleEndDateTimes());
		composite.btnFlatRateService.addListener(SWT.Selection, e -> clearSaleItem());
		composite.btnPart.addListener(SWT.Selection, e -> clearSaleItem());
		if (saleId != null) {
			fillComposite();
		}
		if (productType != null) {
			if (productType == SaleProductType.FLAT_RATE_SERVICE) {
				composite.btnFlatRateService.setSelection(true);
			} else {
				composite.btnPart.setSelection(true);
			}

			if (productId != null) {
				composite.txtSaleItem.setText(saleDelegate.getSaleItemName(productType, productId));
				composite.txtSaleItem.setData(DATA_ENTITY_ID, productId);
			}
		}
		return composite;
	}

	@Override
	public String getViewName() {
		return saleId == null ? "New Sale" : "Edit Sale";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.EDIT, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void saveView() {
		if (!validateState()) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			SaleRecord record = getSaleRecord();
			saleId = saleDelegate.insertOrUpdateSale(record);
			changeSaveState(false);
			changeViewName("Edit Sale");
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful Save", "Successfully saved the sale.");
	}

	private void fillComposite() {

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			SaleRecord record = saleDelegate.getSaleRecord(saleId);

			composite.txtAmountOff.setValue(record.getAmountOff());
			if (record.getEndDttm() == null) {
				composite.btnNoEnd.setSelection(true);
				toggleEndDateTimes();
			} else {
				composite.dateAndTimeEnd.setDate(record.getEndDttm());
			}
			composite.txtSaleName.setText(record.getSaleName());
			if (record.getSaleProductType() == SaleProductType.FLAT_RATE_SERVICE) {
				composite.btnFlatRateService.setSelection(true);
			} else {
				composite.btnPart.setSelection(true);
			}
			if (record.getSaleType() == SaleType.FLAT_RATE_OFF) {
				composite.btnFlatAmountOff.setSelection(true);
			} else {
				composite.btnPercentageOff.setSelection(true);
			}
			composite.dateAndTimeStart.setDate(record.getStartDttm());
			composite.txtSaleItem.setText(record.getEntityName());
			composite.txtSaleItem.setData(DATA_ENTITY_ID, record.getEntityId());
		});
	}

	private boolean validateState() {
		if (!(composite.btnFlatAmountOff.getSelection() || composite.btnPercentageOff.getSelection())) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Please select a sale type.");
			return false;
		}
		if (composite.txtAmountOff.getValue() == null) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Please enter a sale amount.");
			return false;
		}
		if (!(composite.btnFlatRateService.getSelection() || composite.btnPart.getSelection())) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Please enter a sale item type.");
			return false;
		}
		if (composite.txtSaleItem.getData(DATA_ENTITY_ID) == null) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Please enter a sale item.");
			return false;
		}
		return true;
	}

	private SaleRecord getSaleRecord() {
		SaleRecord record = new SaleRecord();
		record.setSaleId(saleId);
		record.setAmountOff(composite.txtAmountOff.getValue());
		Date endDttm = null;
		if (!composite.btnNoEnd.getSelection()) {
			endDttm = composite.dateAndTimeEnd.getDate();
		}
		record.setEndDttm(endDttm);
		record.setEntityId((Long) composite.txtSaleItem.getData(DATA_ENTITY_ID));
		record.setSaleName(composite.txtSaleName.getText());
		SaleProductType saleProductType;
		if (composite.btnFlatRateService.getSelection()) {
			saleProductType = SaleProductType.FLAT_RATE_SERVICE;
		} else {
			saleProductType = SaleProductType.PART;
		}
		record.setSaleProductType(saleProductType);
		SaleType saleType;
		if (composite.btnFlatAmountOff.getSelection()) {
			saleType = SaleType.FLAT_RATE_OFF;
		} else {
			saleType = SaleType.PERCENTAGE_OFF;
		}
		record.setSaleType(saleType);
		record.setStartDttm(composite.dateAndTimeStart.getDate());
		return record;
	}

	private void searchItems() {
		if (!composite.btnPart.getSelection() && !composite.btnFlatRateService.getSelection()) {
			MessageDialogs.displayError(composite.getShell(), "Unable To Search",
					"Please select the sale item type first.");
			return;
		}
		if (composite.btnPart.getSelection()) {
			PartSelectionWizard wizard = new PartSelectionWizard(1, true);
			OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
			dialog.setBlockOnOpen(true);
			if (dialog.open() == Dialog.OK) {
				changeSaveState(true);
				PartSearchRecord result = wizard.getResult();
				composite.txtSaleItem.setText(result.getPartName());
				composite.txtSaleItem.setData(DATA_ENTITY_ID, result.getPartId());
			}
		} else {
			FlatRateSelectionWizard wizard = new FlatRateSelectionWizard(1);
			OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
			dialog.setBlockOnOpen(true);
			if (dialog.open() == Dialog.OK) {
				changeSaveState(true);
				FlatRateSearchRecord result = wizard.getResult();
				composite.txtSaleItem.setText(result.getServiceName());
				composite.txtSaleItem.setData(DATA_ENTITY_ID, result.getFlatRateServiceId());
			}
		}
	}

	private void toggleEndDateTimes() {
		composite.dateAndTimeEnd.setEnabled(!composite.btnNoEnd.getSelection());
	}

	private void clearSaleItem() {
		composite.txtSaleItem.setText("");
		composite.txtSaleItem.setData(DATA_ENTITY_ID, null);
	}
}
