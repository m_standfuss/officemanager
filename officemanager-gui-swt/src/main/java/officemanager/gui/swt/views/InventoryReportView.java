package officemanager.gui.swt.views;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.PartDelegate;
import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.printing.InventoryReportPrint;
import officemanager.gui.swt.printing.PaperclipsPrinter;
import officemanager.gui.swt.services.PartSearchTableService;
import officemanager.gui.swt.ui.composites.InventoryReportComposite;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class InventoryReportView extends OfficeManagerView implements IRefreshableView {

	private static final Logger logger = LogManager.getLogger(InventoryReportView.class);

	private static final List<Integer> PART_COLUMNS = Lists.newArrayList(PartSearchTableComposite.COL_AMT_SOLD,
			PartSearchTableComposite.COL_BASE_PRICE, PartSearchTableComposite.COL_DESCRIPTION,
			PartSearchTableComposite.COL_INVNTRY_CNT, PartSearchTableComposite.COL_MANUF,
			PartSearchTableComposite.COL_MANUF_ID, PartSearchTableComposite.COL_PART_NAME);

	private final PartDelegate partDelegate;
	private final PartSearchTableService outOfStockService;
	private final PartSearchTableService lowStockService;

	private InventoryReportComposite composite;

	public InventoryReportView() {
		partDelegate = new PartDelegate();
		outOfStockService = new PartSearchTableService(PART_COLUMNS);
		lowStockService = new PartSearchTableService(PART_COLUMNS);
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new InventoryReportComposite(parent, style);
		outOfStockService.setComposite(composite.tblOutOfStock);
		lowStockService.setComposite(composite.tblLowStock);
		addListeners();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Inventory Report";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.REPORTS, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		_refresh();
	}

	private void addListeners() {
		composite.tlItmPrint.addListener(SWT.Selection, e -> {
			List<PartSearchRecord> outOfStockParts = partDelegate.getOutOfStockParts();
			List<PartSearchRecord> lowStockParts = partDelegate.getLowStockParts();

			InventoryReportPrint print = new InventoryReportPrint(lowStockParts, outOfStockParts);
			try {
				PaperclipsPrinter.promptAndPrint(composite.getShell(), print);
			} catch (SWTError | Exception ex) {
				logger.error("Unable to print the reciept.", ex);
				MessageDialogs.displayError(composite.getShell(), "Unable To Print",
						"Unexpected error occured while printing.");
			}
		});
	}

	private void _refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			logger.debug("Starting refresh.");

			List<PartSearchRecord> outOfStockParts = partDelegate.getOutOfStockParts();
			logger.debug(outOfStockParts.size() + " out of stock parts found.");
			outOfStockService.populateTable(outOfStockParts);

			List<PartSearchRecord> lowStockParts = partDelegate.getLowStockParts();
			logger.debug(lowStockParts.size() + " low stock parts found.");
			lowStockService.populateTable(lowStockParts);

			composite.scrCompositeCenter.setMinSize(composite.composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		});
	}
}
