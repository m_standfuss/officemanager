package officemanager.gui.swt.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.ReportsComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ReportsView extends OfficeManagerView {

	private ReportsComposite composite;

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new ReportsComposite(parent, style);
		customizeComposite();
		addListeners();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Reports";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.REPORTS, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	private void customizeComposite() {
		hideControl(composite.lnkBalanceDrawer, !AppPrefs.hasPermission(AppPermissions.REPORTS_BLNC_DRWR));
		hideControl(composite.lnkInventory, !AppPrefs.hasPermission(AppPermissions.REPORTS_INVENTORY));
		hideControl(composite.lnkTotals, !AppPrefs.hasPermission(AppPermissions.REPORTS_TOTALS));
	}

	private void addListeners() {
		composite.lnkBalanceDrawer.addListener(SWT.Selection, e -> changeView(new BalanceDrawerReportView()));
		composite.lnkInventory.addListener(SWT.Selection, e -> changeView(new InventoryReportView()));
		composite.lnkTotals.addListener(SWT.Selection, e -> changeView(new TotalsReportView()));
	}
}
