package officemanager.gui.swt.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.delegates.SaleDelegate;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.records.SaleRecord.SaleType;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.SaleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.StringUtility;

public class SaleView extends OfficeManagerView implements IRefreshableView {

	private final long saleId;
	private final SaleDelegate saleDelegate;
	private SaleComposite composite;

	public SaleView(long saleId) {
		this.saleId = saleId;
		saleDelegate = new SaleDelegate();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new SaleComposite(parent, style);
		composite.tlItmDeleteSale.addListener(SWT.Selection, l -> endSale());
		composite.tlItmEditSale.addListener(SWT.Selection, l -> changeView(new SaleEditView(saleId)));
		return composite;
	}

	@Override
	public void closeView() {
		if (composite != null) {
			composite.dispose();
			composite = null;
		}
	}

	@Override
	public String getViewName() {
		return "Sale";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.SALE, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			SaleRecord record = saleDelegate.getSaleRecord(saleId);
			if (record == null) {
				return;
			}
			String amountOff = "";
			if (record.getSaleType() == SaleType.FLAT_RATE_OFF) {
				amountOff = AppPrefs.FORMATTER.formatMoney(record.getAmountOff());
			} else if (record.getSaleType() == SaleType.PERCENTAGE_OFF) {
				amountOff = AppPrefs.FORMATTER.formatPercentage(record.getAmountOff());
			}
			composite.lblAmountOff.setText(amountOff);
			composite.lblEndingDate.setText(AppPrefs.dateFormatter.formatFullDate_DB(record.getEndDttm()));
			composite.lblSaleName.setText(record.getSaleName());
			composite.lblSaleItem.setText(record.getEntityName());
			composite.lblSaleType.setText(record.getSaleTypeDisplay());
			composite.lblStartingDate.setText(AppPrefs.dateFormatter.formatFullDate_DB(record.getStartDttm()));
			if (!StringUtility.isNullOrWhiteSpace(record.getSaleName())) {
				changeViewName("Sale-" + record.getSaleName());
			}
		});
	}

	private void endSale() {
		if (!MessageDialogs.askQuestion(composite.getShell(), "End Sale",
				"Are you sure you wish to end this current sale?")) {
			return;
		}
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			saleDelegate.endSale(saleId);
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully ended the sale.");
	}
}
