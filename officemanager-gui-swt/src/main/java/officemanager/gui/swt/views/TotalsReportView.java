package officemanager.gui.swt.views;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.Calculator;
import officemanager.biz.OrderTotal;
import officemanager.biz.PaymentsTotal;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.delegates.OrderSearchDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderSearchRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.biz.searchingcriteria.OrderSearchCriteria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.printing.PaperclipsPrinter;
import officemanager.gui.swt.printing.TotalsReportPrint;
import officemanager.gui.swt.services.OrderSearchService;
import officemanager.gui.swt.services.PaymentTableService;
import officemanager.gui.swt.services.PriceOverrideService;
import officemanager.gui.swt.ui.composites.TotalsComposite;
import officemanager.gui.swt.ui.tables.OfficeManagerTable;
import officemanager.gui.swt.ui.tables.OrderTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class TotalsReportView extends OfficeManagerView implements IRefreshableView {
	private static final Logger logger = LogManager.getLogger(TotalsReportView.class);

	private static final List<Integer> OPENED_ORDER_COLUMNS = Lists.newArrayList(OrderTableComposite.COL_AMOUNT,
			OrderTableComposite.COL_CLIENT_NAME, OrderTableComposite.COL_CLIENT_PHONE,
			OrderTableComposite.COL_OPENED_BY, OrderTableComposite.COL_OPENED_DTTM, OrderTableComposite.COL_ORDER_NUM,
			OrderTableComposite.COL_ORDER_STATUS, OrderTableComposite.COL_ORDER_TYPE,
			OrderTableComposite.COL_PRODUCT_CNT);

	private static final int MAX_TABLE_HEIGHT = 500;

	private final OrderDelegate orderDelegate;
	private final OrderSearchDelegate orderSearchDelegate;
	private final PaymentDelegate paymentDelegate;

	private final OrderSearchService openedOrderSearchService;
	private final OrderSearchService closedOrderSearchService;
	private final PaymentTableService paymentService;
	private final PriceOverrideService priceOverrideService;

	private TotalsComposite composite;

	public TotalsReportView() {
		orderDelegate = new OrderDelegate();
		orderSearchDelegate = new OrderSearchDelegate();
		paymentDelegate = new PaymentDelegate();

		openedOrderSearchService = new OrderSearchService(OPENED_ORDER_COLUMNS);
		closedOrderSearchService = new OrderSearchService();
		paymentService = new PaymentTableService();
		priceOverrideService = new PriceOverrideService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new TotalsComposite(parent, style);

		openedOrderSearchService.setComposite(composite.tblOrdersOpened);
		closedOrderSearchService.setComposite(composite.tblOrdersClosed);
		paymentService.setComposite(composite.paymentsTotalComposite.tblCmpPayments);
		priceOverrideService.setComposite(composite.tblCmpPriceOverrides);

		addListeners();
		_refresh();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Totals";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.REPORTS, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		_refresh();
	}

	private void _refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			Date startDttm = convertToBeginningOfDayUTC(composite.dateTimeStart);
			Date endDttm = convertToEndOfDayUTC(composite.dateTimeEnd);
			logger.debug("Starting refresh with startDttm=" + startDttm + ", and endDttm=" + endDttm);

			List<PaymentRecord> payments = paymentDelegate.getPayments(startDttm, endDttm);
			paymentService.populatePayments(payments);
			if (payments.size() > 100) {
				setHeight(composite.paymentsTotalComposite.tblCmpPayments.table);
			}

			PaymentsTotal paymentTotals = Calculator.getPaymentTotals(payments);
			composite.paymentsTotalComposite.lblCashPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.cashTotal));
			composite.paymentsTotalComposite.lblCheckPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.checkTotal));
			composite.paymentsTotalComposite.lblClientCreditPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.clientCreditTotal));
			composite.paymentsTotalComposite.lblCreditCardPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.creditCardTotal));
			composite.paymentsTotalComposite.lblFinancingPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.financingTotal));
			composite.paymentsTotalComposite.lblGiftCardPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.giftCardTotal));
			composite.paymentsTotalComposite.lblPendingPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.pendingTotal));
			composite.paymentsTotalComposite.lblTotalPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.total));
			composite.paymentsTotalComposite.lblTradeInPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.tradeInTotal));
			composite.paymentsTotalComposite.lblRefunds
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.refundTotal));

			List<OrderSearchRecord> openedOrders = orderSearchDelegate
					.searchOrders(makeOpenOrderCriteria(startDttm, endDttm));
			openedOrderSearchService.populateTable(openedOrders);
			if (openedOrders.size() > 100) {
				setHeight(composite.tblOrdersOpened.table);
			}

			List<OrderSearchRecord> closedOrders = orderSearchDelegate
					.searchOrders(makeClosedOrderCriteria(startDttm, endDttm));
			closedOrderSearchService.populateTable(closedOrders);
			if (closedOrders.size() > 100) {
				setHeight(composite.tblOrdersClosed.table);
			}

			List<OrderRecord> closedOrderRecords = getOrderRecords(closedOrders);
			OrderTotal orderTotal = Calculator.getOrdersTotal(closedOrderRecords);
			composite.lblTotal.setText(AppPrefs.FORMATTER.formatMoney(orderTotal.orderTotal));
			composite.lblLaborTotal.setText(AppPrefs.FORMATTER.formatMoney(orderTotal.laborTotal));
			composite.lblMerchTotal.setText(AppPrefs.FORMATTER.formatMoney(orderTotal.merchandiseTotal));
			composite.lblTaxTotal.setText(AppPrefs.FORMATTER.formatMoney(orderTotal.taxTotal));
			composite.lblTaxExemptTotal.setText(AppPrefs.FORMATTER.formatMoney(orderTotal.taxExempt));
			composite.lblMerchTotalTaxExempt.setText(AppPrefs.FORMATTER.formatMoney(orderTotal.merchTaxExempt));

			List<PriceOverrideRecord> priceOverrides = orderDelegate.getOverridenPrices(startDttm, endDttm);
			priceOverrideService.populateTable(priceOverrides);

			composite.layout();
			composite.scrCompositeCenter.setMinSize(-1,
					composite.compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		});
	}

	private void setHeight(OfficeManagerTable table) {
		if (table.getLayoutData() instanceof GridData) {
			((GridData) table.getLayoutData()).heightHint = MAX_TABLE_HEIGHT;
		}
	}

	private void print() {
		Date startDttm = convertToBeginningOfDayUTC(composite.dateTimeStart);
		Date endDttm = convertToEndOfDayUTC(composite.dateTimeEnd);

		List<PaymentRecord> payments = paymentDelegate.getPayments(startDttm, endDttm);
		PaymentsTotal paymentsTotal = Calculator.getPaymentTotals(payments);

		List<OrderSearchRecord> openedOrders = orderSearchDelegate
				.searchOrders(makeOpenOrderCriteria(startDttm, endDttm));
		openedOrderSearchService.populateTable(openedOrders);

		List<OrderSearchRecord> closedOrders = orderSearchDelegate
				.searchOrders(makeClosedOrderCriteria(startDttm, endDttm));
		closedOrderSearchService.populateTable(closedOrders);

		List<PriceOverrideRecord> priceOverrides = orderDelegate.getOverridenPrices(startDttm, endDttm);
		priceOverrideService.populateTable(priceOverrides);

		TotalsReportPrint print = new TotalsReportPrint(startDttm, endDttm, paymentsTotal, payments, openedOrders,
				closedOrders, priceOverrides);
		try {
			PaperclipsPrinter.promptAndPrint(composite.getShell(), print);
		} catch (SWTError | Exception e) {
			logger.error("Unable to print the reciept.", e);
			MessageDialogs.displayError(composite.getShell(), "Unable To Print",
					"Unexpected error occured while printing.");
		}
	}

	private OrderSearchCriteria makeClosedOrderCriteria(Date startDttm, Date endDttm) {
		OrderSearchCriteria closedOrderCriteria = new OrderSearchCriteria();
		closedOrderCriteria.begClosedDttm = startDttm;
		closedOrderCriteria.endClosedDttm = endDttm;
		return closedOrderCriteria;
	}

	private OrderSearchCriteria makeOpenOrderCriteria(Date startDttm, Date endDttm) {
		OrderSearchCriteria openOrderCriteria = new OrderSearchCriteria();
		openOrderCriteria.begOpenedDttm = startDttm;
		openOrderCriteria.endOpenedDttm = endDttm;
		return openOrderCriteria;
	}

	private List<OrderRecord> getOrderRecords(List<OrderSearchRecord> orders) {
		Set<Long> orderIds = Sets.newHashSet();
		for (OrderSearchRecord record : orders) {
			orderIds.add(record.getOrderId());
		}
		return orderDelegate.loadOrderRecords(orderIds);
	}

	private void addListeners() {
		composite.dateTimeEnd.addListener(SWT.Selection, e -> _refresh());
		composite.dateTimeStart.addListener(SWT.Selection, e -> _refresh());
		composite.tlItmPrint.addListener(SWT.Selection, e -> print());
	}
}
