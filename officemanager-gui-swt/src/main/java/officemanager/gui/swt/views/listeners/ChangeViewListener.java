package officemanager.gui.swt.views.listeners;

import officemanager.gui.swt.views.IOfficeManagerView;

public abstract class ChangeViewListener {

	public abstract void changeView(IOfficeManagerView view);
}
