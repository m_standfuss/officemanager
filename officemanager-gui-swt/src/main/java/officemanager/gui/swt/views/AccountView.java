package officemanager.gui.swt.views;

import java.util.Date;
import java.util.List;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.biz.delegates.AccountDelegate;
import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.records.AccountRecord;
import officemanager.biz.records.AccountRecord.AccountStatus;
import officemanager.biz.records.AccountTransactionRecord;
import officemanager.biz.records.ClientRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.AccountInfoService;
import officemanager.gui.swt.services.AccountTransactionService;
import officemanager.gui.swt.services.ClientInformationService;
import officemanager.gui.swt.ui.composites.AccountComposite;
import officemanager.gui.swt.ui.wizards.AccountPaymentWizard;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class AccountView extends OfficeManagerView implements IRefreshableView {

	private final Long accountId;

	private final AccountDelegate accountDelegate;
	private final ClientDelegate clientDelegate;

	private final AccountTransactionService accountTransactionService;
	private final AccountInfoService accountInfoService;
	private final ClientInformationService clientInfoService;

	private AccountComposite composite;

	public AccountView(Long accountId) {
		this.accountId = accountId;

		accountDelegate = new AccountDelegate();
		clientDelegate = new ClientDelegate();

		accountTransactionService = new AccountTransactionService();
		accountInfoService = new AccountInfoService();
		clientInfoService = new ClientInformationService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new AccountComposite(parent, style);
		accountTransactionService.setComposite(composite.tblAccountTransactions);
		accountInfoService.setComposite(composite.accountInformationComposite);
		clientInfoService.setComposite(composite.cmpClientInfo);

		refresh();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Account #" + AppPrefs.FORMATTER.formatAccountNumber(accountId);
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.USER_INFO, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			AccountRecord record = accountDelegate.getAccountRecord(accountId);
			showComponents(record);
			accountInfoService.populateComposite(record);

			ClientRecord clientRecord = clientDelegate.getClientRecord(record.getClientId());
			clientInfoService.refresh(clientRecord);

			Date startDate = convertToBeginningOfDayUTC(composite.dttmStart);
			Date endDate = convertToEndOfDayUTC(composite.dttmEnd);
			List<AccountTransactionRecord> accountTransactions = accountDelegate.getAccountTransactions(accountId,
					startDate, endDate);
			accountTransactionService.populateTable(accountTransactions);
			composite.layout();
		});
	}

	private void showComponents(AccountRecord record) {
		boolean addPayment = record.getAccountStatus() != AccountStatus.CLOSED;
		boolean suspendAccount = record.getAccountStatus() == AccountStatus.ACTIVE;
		boolean closeAccount = record.getAccountStatus() != AccountStatus.CLOSED
				&& record.getAccountStatus() != AccountStatus.CLOSED_AWAITING_PAYMENT;
		boolean reopenAccount = record.getAccountStatus() != AccountStatus.ACTIVE;

		composite.addToolItems(addPayment, suspendAccount, closeAccount, reopenAccount);
		addToolItemListeners();
	}

	private void addToolItemListeners() {
		addToolItemListener(composite.tlItmAddPayment, e -> {
			AccountPaymentWizard wizard = new AccountPaymentWizard(accountId);
			OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
			if (dialog.open() == Window.OK) {
				refresh();
			}
		});
		addToolItemListener(composite.tlItmCloseAccount, e -> {
			if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm",
					"Are you sure you wish to close this account? "
							+ "If there is a current balance on the account it will be left "
							+ "in a closed awaiting payment status until a zero balance is reached.")) {
				return;
			}
			accountDelegate.closeAccount(accountId);
			refresh();
		});
		addToolItemListener(composite.tlItmReopenAccount, e -> {
			if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm",
					"Are you sure you wish to reopen this account?")) {
				return;
			}
			accountDelegate.reopenAccount(accountId);
			refresh();
		});
		addToolItemListener(composite.tlItmSuspendAccount, e -> {
			if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm",
					"Are you sure you wish to suspend this account? Client will no longer be able to use account to make payments until reopened.")) {
				return;
			}
			accountDelegate.suspendAccount(accountId);
			refresh();
		});
	}

	private void addToolItemListener(ToolItem tltm, Listener object) {
		if (tltm.isDisposed()) {
			return;
		}
		tltm.addListener(SWT.Selection, object);
	}
}
