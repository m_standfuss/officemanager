package officemanager.gui.swt.views;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.delegates.Delegate;
import officemanager.biz.delegates.PreferencesDelegate;
import officemanager.biz.records.ApplicationPreferencesRecord;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PreferenceRecord;
import officemanager.biz.records.PreferenceRecord.PrefType;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.AppPrefLoader;
import officemanager.gui.swt.ui.composites.PreferencesComposite;
import officemanager.gui.swt.ui.dialogs.CodeSetEditDialog;
import officemanager.gui.swt.ui.dialogs.CodeValueSelectionDialog;
import officemanager.gui.swt.ui.tables.CodeValueTable;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.Tuple;

public class PreferencesView extends OfficeManagerSaveableView {

	private static final Logger logger = LogManager.getLogger(PreferencesView.class);

	private final PreferencesDelegate preferenceDelegate;
	private final Set<PrefType> modifiedPreferences;

	private PreferencesComposite composite;
	private ApplicationPreferencesRecord applicationPrefRecord;

	public PreferencesView() {
		preferenceDelegate = new PreferencesDelegate();
		modifiedPreferences = Sets.newHashSet();
	}

	@Override
	public void saveView() {
		logger.debug("Starting save page.");
		if (!validatePage()) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			for (PrefType prefType : modifiedPreferences) {
				PreferenceRecord record = getPreference(prefType);
				populatePreferenceRecord(record);
				preferenceDelegate.insertOrUpdatePreferenceRecord(record);
			}
		});
		logger.debug("Finished saving preferences.");
		MessageDialogs.displayMessage(composite.getShell(), "Saved",
				"Successfully saved preferences. Any other current instances of the application running must be restarted before changes will take effect.");
		changeSaveState(false);
		modifiedPreferences.clear();
		reloadPreferences();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new PreferencesComposite(parent, style);
		fillComposite();
		populateComposite();
		makeEditable(AppPrefs.hasPermission(AppPermissions.PREFERENCES_EDIT));
		addListeners();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Application Preferences";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.APP_PREFS, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	private boolean validatePage() {
		for (PrefType prefType : modifiedPreferences) {
			switch (prefType) {
			case HOURLY_RATE:
				BigDecimal hourlyRate = composite.txtHourlyRate.getValue();
				if (hourlyRate == null) {
					MessageDialogs.displayError(composite.getShell(), "Unable to Save",
							"The hourly rate must be populated.");
					composite.txtHourlyRate.forceFocus();
					return false;
				}
				if (hourlyRate.compareTo(BigDecimal.ZERO) <= 0) {
					MessageDialogs.displayError(composite.getShell(), "Unable to Save",
							"The discount hourly rate must be greater than zero.");
					composite.txtHourlyRate.forceFocus();
					return false;
				}
			case HOURLY_RATE_DISCOUNT:
				BigDecimal discountRate = composite.txtDiscountedRate.getValue();
				if (discountRate == null) {
					MessageDialogs.displayError(composite.getShell(), "Unable to Save",
							"The discount hourly rate must be populated.");
					composite.txtDiscountedRate.forceFocus();
					return false;
				}
				if (discountRate.compareTo(BigDecimal.ZERO) <= 0) {
					MessageDialogs.displayError(composite.getShell(), "Unable to Save",
							"The discount hourly rate must be greater than zero.");
					composite.txtDiscountedRate.forceFocus();
					return false;
				}
			case TAX_RATE:
				BigDecimal taxRate = composite.txtTaxRate.getValue();
				if (taxRate == null) {
					MessageDialogs.displayError(composite.getShell(), "Unable to Save",
							"The tax rate must be populated.");
					composite.txtTaxRate.forceFocus();
					return false;
				}
				if (taxRate.compareTo(BigDecimal.ZERO) <= 0) {
					MessageDialogs.displayError(composite.getShell(), "Unable to Save",
							"The tax rate must be greater than zero.");
					composite.txtTaxRate.forceFocus();
					return false;
				}
				if (taxRate.compareTo(BigDecimal.ONE) >= 0) {
					MessageDialogs.displayError(composite.getShell(), "Unable to Save",
							"The tax rate must be less than one.");
					composite.txtTaxRate.forceFocus();
					return false;
				}
			default:
				continue;
			}
		}
		return true;
	}

	private void makeEditable(boolean hasPermission) {
		composite.txtCompanyAddress.setEditable(hasPermission);
		composite.txtCompanyPhone.setEditable(hasPermission);
		composite.txtCompanyWebsite.setEditable(hasPermission);
		composite.txtHourlyRate.setEditable(hasPermission);
		composite.txtDiscountedRate.setEditable(hasPermission);
		composite.txtTaxRate.setEditable(hasPermission);
		composite.btnEditShopTicketComments.setEnabled(hasPermission);
		composite.btnEditClaimTicketComments.setEnabled(hasPermission);
		composite.btnEditRecieptComments.setEnabled(hasPermission);
	}

	private void fillComposite() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			List<Tuple<String, Long>> codeSets = Lists.newArrayList();
			codeSets.add(Tuple.newTuple("Product Statuses", Delegate.PRODUCT_STATUS_CDSET));
			codeSets.add(Tuple.newTuple("Product Types", Delegate.PRODUCT_TYPE_CDSET));
			codeSets.add(Tuple.newTuple("Merchandise Product Types", Delegate.MERC_PRODUCT_TYPE_CDSET));
			codeSets.add(Tuple.newTuple("Part Manufacturers", Delegate.MERC_MANUF_CDSET));
			codeSets.add(Tuple.newTuple("Part Categories", Delegate.MERC_CATEGORIES_CDSET));
			codeSets.add(Tuple.newTuple("Flat Rate Service Categories", Delegate.FLTRT_SERV_CATEGORIES_CDSET));
			codeSets.add(Tuple.newTuple("Credit Card Companies", Delegate.CRDT_CARD_COMPANIES_CDSET));
			codeSets.add(Tuple.newTuple("Personnel Statuses", Delegate.PRSNL_STATUS_TYPE_CDSET));
			codeSets.add(Tuple.newTuple("Personnel Types", Delegate.PRSNL_TYPE_CDSET));
			codeSets.add(Tuple.newTuple("Cities", Delegate.CITY_CDSET));
			codeSets.add(Tuple.newTuple("Order Comment Types", Delegate.ORDER_CMMT_TYPE_CDSET));
			codeSets.add(Tuple.newTuple("Phone Types", Delegate.PHONE_TYPE_CDSET));
			codeSets.add(Tuple.newTuple("Address Types", Delegate.ADDRESS_TYPE_CDSET));

			codeSets.sort((t1, t2) -> {
				return t1.item1.compareTo(t2.item1);
			});

			List<Tuple<String, Long>> firstHalf = codeSets.subList(0, codeSets.size() / 2 + 1);
			List<Tuple<String, Long>> secondHalf = codeSets.subList(codeSets.size() / 2 + 1, codeSets.size());

			for (int i = 0; i < firstHalf.size(); i++) {
				Tuple<String, Long> first = firstHalf.get(i);
				Button btnModify = composite.addCodeSet(first.item1);
				btnModify.addListener(SWT.Selection, e -> openCodeSet(first.item2));

				if (secondHalf.size() > i) {
					Tuple<String, Long> second = secondHalf.get(i);
					Button btnModify2 = composite.addCodeSet(second.item1);
					btnModify2.addListener(SWT.Selection, e -> openCodeSet(second.item2));
				}
			}
			composite.scrolledComposite.setMinSize(composite.composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		});
	}

	private void populateComposite() {
		logger.debug("Starting populateComposite.");
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			applicationPrefRecord = preferenceDelegate.getApplicationPreferences();

			if (applicationPrefRecord.getCompanyAddress() != null) {
				composite.txtCompanyAddress.setText(applicationPrefRecord.getCompanyAddress().getPrefValueText());
			}
			if (applicationPrefRecord.getCompanyPhone() != null) {
				composite.txtCompanyPhone.setText(applicationPrefRecord.getCompanyPhone().getPrefValueText());
			}
			if (applicationPrefRecord.getCompanyWebsite() != null) {
				composite.txtCompanyWebsite.setText(applicationPrefRecord.getCompanyWebsite().getPrefValueText());
			}

			if (applicationPrefRecord.getHourlyRate() != null) {
				composite.txtHourlyRate.setValue(applicationPrefRecord.getHourlyRate().getPrefValueDecimal());
			}
			if (applicationPrefRecord.getHourlyRateDiscounted() != null) {
				composite.txtDiscountedRate
						.setValue(applicationPrefRecord.getHourlyRateDiscounted().getPrefValueDecimal());
			}
			if (applicationPrefRecord.getTaxRate() != null) {
				composite.txtTaxRate.setValue(applicationPrefRecord.getTaxRate().getPrefValueDecimal());
			}

			loadCodeValueList(composite.listClaimTicketTypes, applicationPrefRecord.getClaimTicketCommentTypes());
			loadCodeValueList(composite.listRecieptCommentTypes, applicationPrefRecord.getOrderRecieptCommentTypes());
			loadCodeValueList(composite.listShopTicketTypes, applicationPrefRecord.getShopTicketCommentTypes());
		});
	}

	private void loadCodeValueList(CodeValueTable table, List<PreferenceRecord> records) {
		table.removeAll();
		for (PreferenceRecord prefRecord : records) {
			table.addCodeValue(preferenceDelegate.getCodeValueRecord(prefRecord.getPrefValueNumber()));
		}
	}

	private void addListeners() {
		composite.txtCompanyAddress.addListener(SWT.Modify, e -> preferenceModified(PrefType.COMPANY_ADDRESS));
		composite.txtCompanyPhone.addListener(SWT.Modify, e -> preferenceModified(PrefType.COMPANY_PHONE));
		composite.txtCompanyWebsite.addListener(SWT.Modify, e -> preferenceModified(PrefType.COMPANY_WEBSITE));

		composite.txtHourlyRate.addListener(SWT.Modify, e -> preferenceModified(PrefType.HOURLY_RATE));
		composite.txtDiscountedRate.addListener(SWT.Modify, e -> preferenceModified(PrefType.HOURLY_RATE_DISCOUNT));
		composite.txtTaxRate.addListener(SWT.Modify, e -> preferenceModified(PrefType.TAX_RATE));

		composite.btnEditClaimTicketComments.addListener(SWT.Selection,
				e -> editPrintComments(PrefType.CLAIM_TIX_CMMT_TYPE, "Claim Ticket", composite.listClaimTicketTypes));
		composite.btnEditRecieptComments.addListener(SWT.Selection, e -> editPrintComments(PrefType.RECIEPT_CMMT_TYPE,
				"Client Reciept", composite.listRecieptCommentTypes));
		composite.btnEditShopTicketComments.addListener(SWT.Selection,
				e -> editPrintComments(PrefType.SHOP_TIX_CMMT_TYPE, "Shop Ticket", composite.listShopTicketTypes));
	}

	private void editPrintComments(PrefType prefType, String printoutName, CodeValueTable tableToPopulate) {
		CodeValueSelectionDialog dialog = new CodeValueSelectionDialog(composite.getShell());
		dialog.setHeaderText("Select the comment types to be displayed on a " + printoutName + ".");
		dialog.setCodeSet(Delegate.ORDER_CMMT_TYPE_CDSET);
		dialog.setValuesToSelect(tableToPopulate.getCodeValueRecords());
		if (dialog.open() != Window.OK) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			preferenceDelegate.clearPreferences(prefType);
			List<CodeValueRecord> commentTypes = dialog.getSelectedValues();
			tableToPopulate.populateList(commentTypes);
			for (CodeValueRecord record : commentTypes) {
				PreferenceRecord preferenceRecord = makeNewPreference(prefType);
				preferenceRecord.setPrefValueNumber(record.getCodeValue());
				preferenceDelegate.insertOrUpdatePreferenceRecord(preferenceRecord);
			}
		});
		MessageDialogs.displayMessage(composite.getShell(), "Saved",
				"Successfully saved printing preferences. Any other current instances of the application running must be restarted before changes will take effect.");
		reloadPreferences();

	}

	private void reloadPreferences() {
		new AppPrefLoader().loadApplicationPreferences();
	}

	private void preferenceModified(PrefType type) {
		changeSaveState(true);
		modifiedPreferences.add(type);
	}

	private PreferenceRecord getPreference(PrefType prefType) {
		switch (prefType) {
		case COMPANY_ADDRESS:
			if (applicationPrefRecord.getCompanyAddress() != null) {
				return applicationPrefRecord.getCompanyAddress();
			}
			break;
		case COMPANY_WEBSITE:
			if (applicationPrefRecord.getCompanyWebsite() != null) {
				return applicationPrefRecord.getCompanyWebsite();
			}
			break;
		case COMPANY_PHONE:
			if (applicationPrefRecord.getCompanyPhone() != null) {
				return applicationPrefRecord.getCompanyPhone();
			}
			break;
		case HOURLY_RATE:
			if (applicationPrefRecord.getHourlyRate() != null) {
				return applicationPrefRecord.getHourlyRate();
			}
			break;
		case HOURLY_RATE_DISCOUNT:
			if (applicationPrefRecord.getHourlyRateDiscounted() != null) {
				return applicationPrefRecord.getHourlyRateDiscounted();
			}
			break;
		case TAX_RATE:
			if (applicationPrefRecord.getTaxRate() != null) {
				return applicationPrefRecord.getTaxRate();
			}
			break;
		default:
			break;
		}
		return makeNewPreference(prefType);
	}

	private void populatePreferenceRecord(PreferenceRecord record) {
		switch (record.getPrefType()) {
		case COMPANY_ADDRESS:
			record.setPrefValueText(composite.txtCompanyAddress.getText());
			break;
		case COMPANY_WEBSITE:
			record.setPrefValueText(composite.txtCompanyWebsite.getText());
			break;
		case COMPANY_PHONE:
			record.setPrefValueText(composite.txtCompanyPhone.getText());
			break;
		case HOURLY_RATE:
			record.setPrefValueDecimal(composite.txtHourlyRate.getValue());
			break;
		case HOURLY_RATE_DISCOUNT:
			record.setPrefValueDecimal(composite.txtDiscountedRate.getValue());
			break;
		case TAX_RATE:
			record.setPrefValueDecimal(composite.txtTaxRate.getValue());
			break;
		default:
			break;
		}
	}

	private PreferenceRecord makeNewPreference(PrefType type) {
		PreferenceRecord record = new PreferenceRecord();
		record.setPrefType(type);
		return record;
	}

	private void openCodeSet(Long codeSet) {
		CodeSetEditDialog dialog = new CodeSetEditDialog(composite.getShell(), codeSet);
		dialog.open();
	}
}
