package officemanager.gui.swt.views;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.biz.Calculator;
import officemanager.biz.PaymentsTotal;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.PaymentRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.printing.BalanceDrawerPrint;
import officemanager.gui.swt.printing.PaperclipsPrinter;
import officemanager.gui.swt.services.PaymentTableService;
import officemanager.gui.swt.ui.composites.BalanceDrawerComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class BalanceDrawerReportView extends OfficeManagerView implements IRefreshableView {

	private static final Logger logger = LogManager.getLogger(BalanceDrawerReportView.class);

	private final PaymentDelegate paymentDelegate;
	private final PaymentTableService paymentService;

	private ToolBar toolBar;
	private ToolItem tlItmPrint;
	private BalanceDrawerComposite composite;

	public BalanceDrawerReportView() {
		paymentDelegate = new PaymentDelegate();
		paymentService = new PaymentTableService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		Composite root = new Composite(parent, style);
		root.setLayout(new GridLayout());

		toolBar = new ToolBar(root, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false, 1, 1));

		tlItmPrint = new ToolItem(toolBar, SWT.NONE);
		tlItmPrint.setToolTipText("Print Report");
		tlItmPrint.setWidth(16);
		tlItmPrint.setImage(ResourceManager.getImage(ImageFile.PRINT, AppPrefs.ICN_SIZE_PAGETOOLITEM));

		composite = new BalanceDrawerComposite(root, style);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		paymentService.setComposite(composite.paymentsTotalsComposite.tblCmpPayments);
		addListeners();
		return root;
	}

	@Override
	public String getViewName() {
		return "Balance Drawer";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.CASH, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		_refresh();
	}

	private void _refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			List<PaymentRecord> payments = paymentDelegate.getPayments(convertToBeginningOfDayUTC(new Date()),
					convertToEndOfDayUTC(new Date()));
			paymentService.populatePayments(payments);

			PaymentsTotal paymentTotals = Calculator.getPaymentTotals(payments);
			composite.paymentsTotalsComposite.lblCashPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.cashTotal));
			composite.paymentsTotalsComposite.lblCheckPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.checkTotal));
			composite.paymentsTotalsComposite.lblClientCreditPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.clientCreditTotal));
			composite.paymentsTotalsComposite.lblCreditCardPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.creditCardTotal));
			composite.paymentsTotalsComposite.lblFinancingPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.financingTotal));
			composite.paymentsTotalsComposite.lblGiftCardPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.giftCardTotal));
			composite.paymentsTotalsComposite.lblPendingPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.pendingTotal));
			composite.paymentsTotalsComposite.lblTotalPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.total));
			composite.paymentsTotalsComposite.lblTradeInPayments
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.tradeInTotal));
			composite.paymentsTotalsComposite.lblRefunds
					.setText(AppPrefs.FORMATTER.formatMoney(paymentTotals.refundTotal));
		});
	}

	private void addListeners() {
		tlItmPrint.addListener(SWT.Selection, e -> print());
	}

	private void print() {
		List<PaymentRecord> payments = paymentDelegate.getPayments(convertToBeginningOfDayUTC(new Date()),
				convertToEndOfDayUTC(new Date()));

		PaymentsTotal paymentTotals = Calculator.getPaymentTotals(payments);
		BalanceDrawerPrint print = new BalanceDrawerPrint(paymentTotals, payments);
		try {
			PaperclipsPrinter.promptAndPrint(composite.getShell(), print);
		} catch (SWTError | Exception e) {
			logger.error("Unable to print the reciept.", e);
			MessageDialogs.displayError(composite.getShell(), "Unable To Print",
					"Unexpected error occured while printing.");
		}
	}

}
