package officemanager.gui.swt.views;

public interface IRefreshableView {

	public void refresh();
}
