package officemanager.gui.swt.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import officemanager.biz.delegates.PersonnelDelegate;
import officemanager.biz.records.AddressRecord;
import officemanager.biz.records.PersonnelRecord;
import officemanager.biz.records.PhoneRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.AddressService;
import officemanager.gui.swt.services.PersonnelInformationService;
import officemanager.gui.swt.services.PhoneService;
import officemanager.gui.swt.ui.composites.PersonnelComposite;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.PersonWizard;
import officemanager.gui.swt.ui.wizards.PersonnelWizard;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelView extends OfficeManagerView implements IRefreshableView {

	private final static Logger logger = LogManager.getLogger(PersonnelView.class);

	private final Long personnelId;
	private final PersonnelDelegate personnelDelegate;
	private final PersonnelInformationService personnelInformationService;
	private final AddressService addressService;
	private final PhoneService phoneService;

	private Long personId;
	private PersonnelComposite composite;

	public PersonnelView(Long personnelId) {
		this.personnelId = personnelId;
		personnelDelegate = new PersonnelDelegate();
		personnelInformationService = new PersonnelInformationService();
		addressService = new AddressService();
		phoneService = new PhoneService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new PersonnelComposite(parent, style);
		composite.tlItmEditPermissions.addListener(SWT.Selection, e -> changeView(new PersonnelRolesView(personnelId)));
		composite.tlItmEditInformation.addListener(SWT.Selection, e -> openEditWizard());
		composite.phoneTblCmp.table.addListener(SWT.MouseDoubleClick, e -> openPhone());
		composite.addressTblCmp.table.addListener(SWT.MouseDoubleClick, e -> openAddress());

		personnelInformationService.setComposite(composite.personnelInformationComposite);
		addressService.setComposite(composite.addressTblCmp);
		phoneService.setComposite(composite.phoneTblCmp);

		return composite;
	}

	@Override
	public String getViewName() {
		return "Personnel";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.USER_INFO, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void refresh() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			PersonnelRecord record = personnelDelegate.getPersonnelRecord(personnelId);
			changeViewName("Personnel - " + record.getNameFullFormatted());
			personId = record.getPersonId();
			personnelInformationService.refresh(record);
			addressService.populateAddress(record.getAddressRecords());
			phoneService.populatePhoneTable(record.getPhoneRecords());
		});
	}

	private void openEditWizard() {
		PersonWizard wizard = new PersonnelWizard(personnelId);
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Dialog.OK) {
			refresh();
		}
	}

	private void openPhone() {
		if (composite.phoneTblCmp.table.getSelectionCount() == 0) {
			logger.error("No phone rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.phoneTblCmp.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one phone is currently selected. Opening the first of the two.");
		}
		TableItem phoneRow = selectedRows[0];
		PhoneRecord record = (PhoneRecord) phoneRow.getData(PhoneService.DATA_RECORD);
		changeView(new PhoneEditView(record.getPhoneId(), personId));
	}

	private void openAddress() {
		if (composite.addressTblCmp.table.getSelectionCount() == 0) {
			logger.error("No address rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.addressTblCmp.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one phone is currently selected. Opening the first of the two.");
		}
		TableItem addressRow = selectedRows[0];
		AddressRecord record = (AddressRecord) addressRow.getData(AddressService.DATA_RECORD);
		changeView(new AddressEditView(record.getAddressId(), personId));
	}
}
