package officemanager.gui.swt.views;

import java.util.Calendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import officemanager.biz.records.OrderSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.OrderSearchService;
import officemanager.gui.swt.ui.composites.OrderSearchComposite;
import officemanager.gui.swt.ui.wizards.NewBidWizard;
import officemanager.gui.swt.ui.wizards.NewMerchandiseWizard;
import officemanager.gui.swt.ui.wizards.NewServiceOrderWizard;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.util.TablePrinter;
import officemanager.gui.swt.util.TableWriter;

abstract class OrderSearchView extends OfficeManagerView {

	private static final Logger logger = LogManager.getLogger(OrderSearchView.class);

	private final OrderSearchService orderTableService;
	private OrderSearchComposite composite;

	public OrderSearchView() {
		orderTableService = new OrderSearchService(getColumnsToKeep());
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.SEARCH, AppPrefs.ICN_SIZE_VIEW);
	}

	protected abstract List<OrderSearchRecord> getSearchResults();

	protected abstract List<Integer> getColumnsToKeep();

	protected OrderSearchComposite makeSearchComposite(Composite parent, int style) {
		logger.debug("Creating the searching composite");
		composite = new OrderSearchComposite(parent, style);
		composite.btnSearch.addListener(SWT.Selection, e -> search());
		composite.searchResultsTable.table.addListener(SWT.MouseDoubleClick, e -> openOrder());
		TraverseListener searchKeyListener = e -> {
			if (e.keyCode == SWT.CR || e.keyCode == 16777296) {
				search();
				e.doit = false;
			}
		};

		composite.txtClientFirstName.addTraverseListener(searchKeyListener);
		composite.txtClientLastName.addTraverseListener(searchKeyListener);
		composite.txtOrderNumber.addTraverseListener(searchKeyListener);
		composite.txtPrimaryPhone.addTraverseListener(searchKeyListener);
		composite.mntmServiceOrder.addListener(SWT.Selection, e -> newServiceWizard());
		composite.mntmMerchandiseOrder.addListener(SWT.Selection, e -> newMerchandiseWizard());
		composite.mntmBid.addListener(SWT.Selection, e -> newBidWizard());
		composite.mntmPrintOrders.addListener(SWT.Selection, e -> printTable());
		composite.mntmExportOrders.addListener(SWT.Selection, e -> exportTable());

		orderTableService.setComposite(composite.searchResultsTable);

		Calendar cal = Calendar.getInstance(AppPrefs.localTimeZone);
		cal.add(Calendar.DAY_OF_MONTH, -7);
		composite.dateTimeStart.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
				cal.get(Calendar.DAY_OF_MONTH));

		return composite;
	}

	private void newMerchandiseWizard() {
		NewMerchandiseWizard wizard = new NewMerchandiseWizard();
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Window.OK) {
			changeView(wizard.getReturnView());
		}
	}

	private void newServiceWizard() {
		NewServiceOrderWizard wizard = new NewServiceOrderWizard();
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Window.OK) {
			changeView(wizard.getReturnView());
		}
	}

	private void newBidWizard() {
		NewBidWizard wizard = new NewBidWizard();
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		dialog.open();
	}

	private void printTable() {
		if (composite.searchResultsTable.table.getItemCount() == 0) {
			MessageDialogs.displayMessage(composite.getShell(), "Cannot Print", "No orders to print.");
			return;
		}
		TablePrinter printer = new TablePrinter(composite.searchResultsTable.table);
		printer.printTable("Orders", getColumnsToKeep());
	}

	private void exportTable() {
		if (composite.searchResultsTable.table.getItemCount() == 0) {
			MessageDialogs.displayMessage(composite.getShell(), "Cannot Print", "No orders to export.");
			return;
		}
		TableWriter writer = new TableWriter(composite.searchResultsTable.table);
		writer.writeToFile(getColumnsToKeep());
	}

	private void openOrder() {
		if (composite.searchResultsTable.table.getSelectionCount() == 0) {
			logger.error("No order rows are currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.searchResultsTable.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Opening the first of the two.");
		}
		TableItem orderRow = selectedRows[0];
		OrderSearchRecord record = (OrderSearchRecord) orderRow.getData(OrderSearchService.DATA_RECORD);
		changeView(new OrderView(record.getOrderId()));
	}

	private void search() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			logger.debug("Search button selected.");
			composite.searchResultsTable.table.removeAll();
			List<OrderSearchRecord> searchResults = getSearchResults();
			orderTableService.populateTable(searchResults);
		});
	}

	@Override
	public String getViewName() {
		return "Order Search";
	}
}
