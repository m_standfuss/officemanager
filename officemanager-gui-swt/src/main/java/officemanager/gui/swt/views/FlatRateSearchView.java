package officemanager.gui.swt.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import officemanager.biz.records.FlatRateSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.FlatRateSearchService;
import officemanager.gui.swt.ui.composites.FlatRateSearchComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class FlatRateSearchView extends OfficeManagerView {
	private static final Logger logger = LogManager.getLogger(FlatRateSearchView.class);

	private final FlatRateSearchService flatRateSearchService;
	private FlatRateSearchComposite composite;

	public FlatRateSearchView() {
		flatRateSearchService = new FlatRateSearchService();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		logger.debug("Creating the searching composite");
		composite = new FlatRateSearchComposite(parent, style);
		composite.searchResultsTable.table.addListener(SWT.MouseDoubleClick, e -> openFlatRate());
		composite.tlItmNewFlatRate.addListener(SWT.Selection, e -> changeView(new FlatRateEditView()));
		flatRateSearchService.setComposite(composite);
		return composite;
	}

	@Override
	public void closeView() {
		if (composite != null) {
			composite.dispose();
			composite = null;
		}
	}

	@Override
	public String getViewName() {
		return "Flat Rate Service Search";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.SEARCH, AppPrefs.ICN_SIZE_VIEW);
	}

	private void openFlatRate() {
		if (composite.searchResultsTable.table.getSelectionCount() == 0) {
			logger.error("No flat rate row is currently selected.");
			return;
		}
		TableItem[] selectedRows = composite.searchResultsTable.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Opening the first of the two.");
		}
		TableItem flatRateRow = selectedRows[0];
		FlatRateSearchRecord record = (FlatRateSearchRecord) flatRateRow.getData(FlatRateSearchService.DATA_RECORD);
		changeView(new FlatRateView(record.getFlatRateServiceId()));
	}
}
