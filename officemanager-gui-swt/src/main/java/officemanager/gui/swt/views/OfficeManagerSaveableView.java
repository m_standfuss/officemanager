package officemanager.gui.swt.views;

import java.util.List;

import com.google.common.collect.Lists;

import officemanager.gui.swt.views.listeners.SaveStateChangedListener;

public abstract class OfficeManagerSaveableView extends OfficeManagerView implements ISaveableView {

	private final List<SaveStateChangedListener> saveStateChangedListeners;

	public OfficeManagerSaveableView() {
		saveStateChangedListeners = Lists.newArrayList();
	}

	@Override
	public void addSaveStateChangeListener(SaveStateChangedListener listener) {
		saveStateChangedListeners.add(listener);
	}

	@Override
	public void clearListeners() {
		saveStateChangedListeners.clear();
		super.clearListeners();
	}

	protected void changeSaveState(boolean canSave) {
		for (SaveStateChangedListener listener : saveStateChangedListeners) {
			listener.saveStateChanged(canSave);
		}
	}
}
