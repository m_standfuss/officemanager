package officemanager.gui.swt.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.delegates.ClientProductDelegate;
import officemanager.biz.records.ClientProductRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.ClientProductEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class ClientProductView extends OfficeManagerSaveableView {

	private static final Logger logger = LogManager.getLogger(ClientProductView.class);

	private final ClientProductDelegate clientProductDelegate;
	private final Long clientId;
	private final Long clientProductId;

	private ClientProductRecord recordInContext;
	private ClientProductEditComposite composite;

	public ClientProductView(Long clientProductId, Long clientId) {
		if (clientId == null) {
			throw new IllegalStateException("Must provide the client id.");
		}
		this.clientProductId = clientProductId;
		this.clientId = clientId;
		clientProductDelegate = new ClientProductDelegate();
	}

	@Override
	public Composite makeComposite(Composite parent, int style) {
		composite = new ClientProductEditComposite(parent, style);

		composite.cmbProductStatus.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.cmbProductType.addListener(SWT.Selection, e -> changeSaveState(true));
		composite.txtManufacturer.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtBarcode.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtModel.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.txtSerialNumber.addListener(SWT.Modify, e -> changeSaveState(true));
		composite.tlItmDeleteProduct.addListener(SWT.Selection, e -> removeProduct());

		populateComposite();
		return composite;
	}

	@Override
	public String getViewName() {
		return "Client Product";
	}

	@Override
	public Image getViewIcon() {
		return ResourceManager.getImage(ImageFile.PRODUCT, AppPrefs.ICN_SIZE_VIEW);
	}

	@Override
	public void closeView() {
		composite.dispose();
	}

	@Override
	public void saveView() {
		if (!validate()) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			recordInContext.setBarcode(composite.txtBarcode.getText());
			recordInContext.setClientId(clientId);
			recordInContext.setManufacturer(composite.txtManufacturer.getText());
			recordInContext.setModel(composite.txtModel.getText());
			recordInContext.setProductStatus(composite.cmbProductStatus.getCodeValueRecord());
			recordInContext.setProductType(composite.cmbProductType.getCodeValueRecord());
			recordInContext.setSerialNum(composite.txtSerialNumber.getText());
			Long clientProductId = clientProductDelegate.persistClientProduct(recordInContext);
			recordInContext = new ClientProductRecord(clientProductId, recordInContext);
			changeSaveState(false);
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully Saved Product");
	}

	private void removeProduct() {
		if (!MessageDialogs.askQuestion(composite.getShell(), "Confirm Deletion",
				"Are you sure you wish to delete this client product?")) {
			return;
		}

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			clientProductDelegate.deactivateProduct(clientProductId);
		});
		MessageDialogs.displayMessage(composite.getShell(), "Successful", "Successfully Removed Product");
		forceCloseView();
	}

	private boolean validate() {
		if (composite.cmbProductType.getCodeValueRecord() == null) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Please populate a product type.");
			return false;
		}
		if (composite.txtManufacturer.getText().isEmpty()) {
			MessageDialogs.displayError(composite.getShell(), "Cannot Save", "Please populate a product manufacturer.");
			return false;
		}
		return true;
	}

	private void populateComposite() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.cmbProductStatus.populateSelections(clientProductDelegate.getProductStatuses());
			composite.cmbProductType.populateSelections(clientProductDelegate.getProductTypes());

			if (clientProductId == null) {
				logger.debug("Creating a new client product for client id=" + clientId);
				recordInContext = new ClientProductRecord();
				recordInContext.setClientId(clientId);
			} else {
				logger.debug("Modify existing client product with id = " + clientProductId);
				recordInContext = clientProductDelegate.getClientProduct(clientProductId);
				if (recordInContext == null) {
					logger.warn("Unable to find existing client product creating a new one instead.");

				}
			}

			composite.cmbProductStatus.setSelectedValue(recordInContext.getProductStatus());
			composite.cmbProductType.setSelectedValue(recordInContext.getProductType());
			composite.txtManufacturer.setText(recordInContext.getManufacturer());
			composite.txtBarcode.setText(recordInContext.getBarcode());
			composite.txtModel.setText(recordInContext.getModel());
			composite.txtSerialNumber.setText(recordInContext.getSerialNum());
			if (recordInContext.getWorkCount() != null) {
				composite.txtWorkCnt.setText(recordInContext.getWorkCount().toString());
			}
		});
	}
}
