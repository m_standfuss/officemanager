package officemanager.gui.swt.views;

import officemanager.gui.swt.views.listeners.SaveStateChangedListener;

public interface ISaveableView {

	public void addSaveStateChangeListener(SaveStateChangedListener listener);

	public void saveView();
}
