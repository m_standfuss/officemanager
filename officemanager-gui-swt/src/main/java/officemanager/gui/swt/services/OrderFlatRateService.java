package officemanager.gui.swt.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderFlatRateTableComposite;

public class OrderFlatRateService {
	private static final Logger logger = LogManager.getLogger(OrderFlatRateService.class);

	public static final String DATA_ORDER_FLAT_RATE_RECORD = "DATA_ORDER_FLAT_RATE_RECORD";

	private final List<Integer> columnsToSkip;
	private OrderFlatRateTableComposite composite;

	public OrderFlatRateService() {
		columnsToSkip = Lists.newArrayList();
	}

	public OrderFlatRateService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(OrderFlatRateTableComposite.COL_SERVICE_NAME)) {
			columnsToSkip.add(OrderFlatRateTableComposite.COL_SERVICE_NAME);
		}
		if (!columnsToKeep.contains(OrderFlatRateTableComposite.COL_CLIENT_PRODUCT)) {
			columnsToSkip.add(OrderFlatRateTableComposite.COL_CLIENT_PRODUCT);
		}
		if (!columnsToKeep.contains(OrderFlatRateTableComposite.COL_PRICE)) {
			columnsToSkip.add(OrderFlatRateTableComposite.COL_PRICE);
		}
	}

	public void setComposite(OrderFlatRateTableComposite composite) {
		this.composite = composite;
	}

	public void populateTable(List<OrderFlatRateServiceRecord> records) {
		logger.debug("Starting populateTable.");
		composite.table.removeAll();
		for (OrderFlatRateServiceRecord record : records) {
			_addRecordToTable(record);
		}
		composite.table.packTableColumns(columnsToSkip);
		composite.layout();
	}

	public List<OrderFlatRateServiceRecord> getCheckedFlatRates() {
		List<TableItem> checkedItems = composite.table.getCheckedItems();
		List<OrderFlatRateServiceRecord> records = Lists.newArrayList();
		for (TableItem item : checkedItems) {
			records.add((OrderFlatRateServiceRecord) item.getData(DATA_ORDER_FLAT_RATE_RECORD));
		}
		return records;
	}

	public OrderFlatRateServiceRecord getSelectedFlatRate() {
		return composite.table.getSelectedData(DATA_ORDER_FLAT_RATE_RECORD);
	}

	private TableItem _addRecordToTable(OrderFlatRateServiceRecord record) {
		TableItem item = new TableItem(composite.table, SWT.NONE);
		item.setText(OrderFlatRateTableComposite.COL_CLIENT_PRODUCT,
				AppPrefs.FORMATTER.formatClientProductDisplay(record.getClientProductManufacturer(),
						record.getClientProductModel(), record.getClientProductSerialNumber()));
		item.setText(OrderFlatRateTableComposite.COL_SERVICE_NAME, record.getServiceName());
		if (record.isRefunded()) {
			item.setText(OrderFlatRateTableComposite.COL_PRICE, "REFUNDED");
		} else {
			item.setText(OrderFlatRateTableComposite.COL_PRICE,
					AppPrefs.FORMATTER.formatMoney(record.getServicePrice()));
		}
		item.setData(DATA_ORDER_FLAT_RATE_RECORD, record);
		return item;
	}
}
