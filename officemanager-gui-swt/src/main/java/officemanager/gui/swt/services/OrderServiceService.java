package officemanager.gui.swt.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderServiceTableComposite;

public class OrderServiceService {
	private static final Logger logger = LogManager.getLogger(OrderServiceService.class);

	public static final String DATA_ORDER_SERVICE_RECORD = "DATA_ORDER_SERVICE_RECORD";

	private final List<Integer> columnsToSkip;
	private OrderServiceTableComposite composite;

	public OrderServiceService() {
		columnsToSkip = Lists.newArrayList();
	}

	public OrderServiceService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(OrderServiceTableComposite.COL_SERVICE_NAME)) {
			columnsToSkip.add(OrderServiceTableComposite.COL_SERVICE_NAME);
		}
		if (!columnsToKeep.contains(OrderServiceTableComposite.COL_CLIENT_PRODUCT)) {
			columnsToSkip.add(OrderServiceTableComposite.COL_CLIENT_PRODUCT);
		}
		if (!columnsToKeep.contains(OrderServiceTableComposite.COL_PRICE)) {
			columnsToSkip.add(OrderServiceTableComposite.COL_PRICE);
		}
		if (!columnsToKeep.contains(OrderServiceTableComposite.COL_HOURS)) {
			columnsToSkip.add(OrderServiceTableComposite.COL_HOURS);
		}
		if (!columnsToKeep.contains(OrderServiceTableComposite.COL_TOTAL)) {
			columnsToSkip.add(OrderServiceTableComposite.COL_TOTAL);
		}
	}

	public void setComposite(OrderServiceTableComposite composite) {
		this.composite = composite;
	}

	public void populateTable(List<OrderServiceRecord> records) {
		logger.debug("Starting populateTable.");
		composite.table.removeAll();
		for (OrderServiceRecord record : records) {
			_addRecordToTable(record);
		}
		composite.table.packTableColumns(columnsToSkip);
		composite.layout();
	}

	public List<OrderServiceRecord> getCheckedServices() {
		List<TableItem> checkedItems = composite.table.getCheckedItems();
		List<OrderServiceRecord> records = Lists.newArrayList();
		for (TableItem item : checkedItems) {
			records.add((OrderServiceRecord) item.getData(DATA_ORDER_SERVICE_RECORD));
		}
		return records;
	}

	public OrderServiceRecord getSelectedService() {
		return composite.table.getSelectedData(DATA_ORDER_SERVICE_RECORD);
	}

	public List<OrderServiceRecord> getSelectedServices() {
		return composite.table.getSelectedDataAll(DATA_ORDER_SERVICE_RECORD);
	}

	private TableItem _addRecordToTable(OrderServiceRecord record) {
		TableItem item = new TableItem(composite.table, SWT.NONE);
		item.setText(OrderServiceTableComposite.COL_CLIENT_PRODUCT,
				AppPrefs.FORMATTER.formatClientProductDisplay(record.getClientProductManufacturer(),
						record.getClientProductModel(), record.getClientProductSerialNumber()));
		item.setText(OrderServiceTableComposite.COL_SERVICE_NAME, record.getServiceName());
		item.setText(OrderServiceTableComposite.COL_PRICE, AppPrefs.FORMATTER.formatMoney(record.getPricePerHour()));
		item.setText(OrderServiceTableComposite.COL_HOURS, AppPrefs.FORMATTER.formatDecimal(record.getHoursQnt(), 2));

		if (record.isRefunded()) {
			item.setText(OrderServiceTableComposite.COL_TOTAL, "REFUNDED");
		} else {
			BigDecimal total = Calculator.totalService(record);
			item.setText(OrderServiceTableComposite.COL_TOTAL, AppPrefs.FORMATTER.formatMoney(total));
		}
		item.setData(DATA_ORDER_SERVICE_RECORD, record);
		return item;
	}
}
