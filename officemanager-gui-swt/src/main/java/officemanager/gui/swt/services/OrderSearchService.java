package officemanager.gui.swt.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.OrderSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderTableComposite;
import officemanager.utility.CollectionUtility;

public class OrderSearchService {

	public static final String DATA_RECORD = "DATA_RECORD";
	public static final String DATA_ORDER_ID = "DATA_ORDER_ID";
	public static final String DATA_ORDER_NUMBER = "DATA_ORDER_NUMBER";
	public static final String DATA_AMOUNT = "DATA_AMOUNT";
	public static final String DATA_OPENED_DTTM = "DATA_OPENED_DTTM";
	public static final String DATA_PRODUCT_CNT = "DATA_PRODUCT_CNT";
	public static final String DATA_CLOSED_DTTM = "DATA_CLOSED_DTTM";

	private static final Logger logger = LogManager.getLogger(OrderSearchService.class);

	private final List<Integer> columnsToSkip;
	private OrderTableComposite composite;

	public OrderSearchService() {
		columnsToSkip = Lists.newArrayList();
	}

	public OrderSearchService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(OrderTableComposite.COL_ORDER_NUM)) {
			columnsToSkip.add(OrderTableComposite.COL_ORDER_NUM);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_ORDER_TYPE)) {
			columnsToSkip.add(OrderTableComposite.COL_ORDER_TYPE);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_CLIENT_NAME)) {
			columnsToSkip.add(OrderTableComposite.COL_CLIENT_NAME);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_AMOUNT)) {
			columnsToSkip.add(OrderTableComposite.COL_AMOUNT);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_ORDER_STATUS)) {
			columnsToSkip.add(OrderTableComposite.COL_ORDER_STATUS);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_OPENED_DTTM)) {
			columnsToSkip.add(OrderTableComposite.COL_OPENED_DTTM);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_OPENED_BY)) {
			columnsToSkip.add(OrderTableComposite.COL_OPENED_BY);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_PRODUCT_CNT)) {
			columnsToSkip.add(OrderTableComposite.COL_PRODUCT_CNT);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_CLOSED_DTTM)) {
			columnsToSkip.add(OrderTableComposite.COL_CLOSED_DTTM);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_CLOSED_BY)) {
			columnsToSkip.add(OrderTableComposite.COL_CLOSED_BY);
		}
		if (!columnsToKeep.contains(OrderTableComposite.COL_CLIENT_PHONE)) {
			columnsToSkip.add(OrderTableComposite.COL_CLIENT_PHONE);
		}
	}

	public void setComposite(OrderTableComposite composite) {
		this.composite = composite;
		composite.table.hideColumns(columnsToSkip);
	}

	public void populateTable(List<OrderSearchRecord> records) {
		composite.table.removeAll();
		if (CollectionUtility.isNullOrEmpty(records)) {
			logger.debug("No records to populate.");
			return;
		}
		for (OrderSearchRecord record : records) {
			TableItem item = new TableItem(composite.table, SWT.NONE);
			item.setText(OrderTableComposite.COL_AMOUNT, AppPrefs.FORMATTER.formatMoney(record.getAmount()));
			item.setText(OrderTableComposite.COL_CLIENT_NAME, record.getClientName());
			item.setText(OrderTableComposite.COL_CLOSED_BY, record.getClosedBy());
			item.setText(OrderTableComposite.COL_CLOSED_DTTM,
					AppPrefs.dateFormatter.formatFullDate_DB(record.getClosedDttm()));
			item.setText(OrderTableComposite.COL_OPENED_BY, record.getOpenedBy());
			item.setText(OrderTableComposite.COL_OPENED_DTTM,
					AppPrefs.dateFormatter.formatFullDate_DB(record.getOpenedDttm()));
			item.setText(OrderTableComposite.COL_ORDER_NUM, AppPrefs.FORMATTER.formatOrderNumber(record.getOrderId()));
			item.setText(OrderTableComposite.COL_ORDER_STATUS, record.getOrderStatus());
			item.setText(OrderTableComposite.COL_ORDER_TYPE, record.getOrderType());
			item.setText(OrderTableComposite.COL_PRODUCT_CNT, String.valueOf(record.getProductCount()));
			item.setText(OrderTableComposite.COL_CLIENT_PHONE,
					AppPrefs.FORMATTER.formatPhoneNumber(record.getClientPhone()));

			item.setData(DATA_RECORD, record);
			item.setData(DATA_ORDER_ID, record.getOrderId());
			item.setData(DATA_ORDER_NUMBER, record.getOrderId());
			item.setData(DATA_AMOUNT, record.getAmount());
			item.setData(DATA_OPENED_DTTM, record.getOpenedDttm());
			item.setData(DATA_PRODUCT_CNT, record.getProductCount());
			item.setData(DATA_CLOSED_DTTM, record.getClosedDttm());
		}
		composite.table.packTableColumns(columnsToSkip);
	}

	public Long getSelectedOrderId() {
		if (composite.table.getSelectionCount() == 0) {
			logger.error("No order rows are currently selected.");
			return null;
		}
		TableItem[] selectedRows = composite.table.getSelection();
		if (selectedRows.length > 1) {
			logger.error("More then one order is currently selected. Returning the first of the two.");
		}
		TableItem orderRow = selectedRows[0];
		return ((OrderSearchRecord) orderRow.getData(DATA_RECORD)).getOrderId();
	}
}
