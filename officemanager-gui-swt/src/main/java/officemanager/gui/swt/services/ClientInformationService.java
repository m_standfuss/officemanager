package officemanager.gui.swt.services;

import officemanager.biz.records.ClientRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.ClientComposite;
import officemanager.gui.swt.ui.composites.ClientInformationComposite;

public class ClientInformationService extends PersonInformationService {

	private ClientInformationComposite composite;

	public ClientInformationService() {}

	public void setComposite(ClientComposite composite) {
		super.setComposite(composite.clientInfoComposite);
		this.composite = composite.clientInfoComposite;
	}

	public void setComposite(ClientInformationComposite composite) {
		super.setComposite(composite);
		this.composite = composite;
	}

	public void refresh(ClientRecord record) {
		populatePersonInformation(record);

		composite.lblAttentionOf.setText(record.getAttentionOf());
		composite.lblJoinedOn.setText(AppPrefs.dateFormatter.formatDate_DB(record.getJoinedOn()));
		composite.lblPromoClub.setText(Boolean.toString(record.isPromotionClub()));
		composite.lblTaxExempt.setText(Boolean.toString(record.isTaxExempt()));
		composite.getParent().layout();
	}
}
