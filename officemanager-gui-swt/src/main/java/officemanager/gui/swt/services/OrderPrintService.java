package officemanager.gui.swt.services;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.eclipse.swt.SWTError;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Shell;
import org.jboss.logging.Logger;

import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.delegates.OrderCommentDelegate;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.ClientRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.printing.ClaimTicketPrint;
import officemanager.gui.swt.printing.OrderRecieptPrint;
import officemanager.gui.swt.printing.PaperclipsPrinter;
import officemanager.gui.swt.printing.Printable;
import officemanager.gui.swt.printing.ShopTicketPrint;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.utility.CollectionUtility;

public class OrderPrintService {

	private static final Logger logger = Logger.getLogger(OrderPrintService.class);

	private Shell shell;
	private final ClientDelegate clientDelegate;
	private final OrderCommentDelegate orderCommentDelegate;
	private final OrderDelegate orderDelegate;
	private final PaymentDelegate paymentDelegate;

	public OrderPrintService() {
		clientDelegate = new ClientDelegate();
		orderCommentDelegate = new OrderCommentDelegate();
		orderDelegate = new OrderDelegate();
		paymentDelegate = new PaymentDelegate();
	}

	public void setShell(Shell shell) {
		this.shell = shell;
	}

	public void printReciept(Long orderId) {
		logger.debug("Starting printReciept for orderId=" + orderId);
		if (shell == null) {
			throw new IllegalStateException("The shell must be set on the service prior to printing.");
		}
		checkNotNull(orderId);

		OrderRecord orderRecord = orderDelegate.loadOrderRecord(orderId);
		if (orderRecord == null) {
			logger.error("Unable to load a order record for orderId = " + orderId);
			MessageDialogs.displayError(shell, "Unable To Print",
					"An error occured and the reciept cannot be printed at this time. "
							+ "Please generate manual reciepts for this order until the problem can be fixed.");
			return;
		}

		ClientRecord clientRecord = null;
		if (orderRecord.getClientId() != null) {
			clientRecord = clientDelegate.getClientRecord(orderRecord.getClientId());
		}

		List<OrderCommentRecord> commentRecords = null;
		if (!CollectionUtility.isNullOrEmpty(AppPrefs.orderRecieptCommentTypes)) {
			commentRecords = orderCommentDelegate.loadOrderComments(orderId, AppPrefs.orderRecieptCommentTypes);
		}

		List<PaymentRecord> payments = paymentDelegate.getPaymentsForOrder(orderId);

		Printable print = new OrderRecieptPrint(orderRecord, clientRecord, payments, commentRecords);
		try {
			PaperclipsPrinter.promptAndPrint(shell, print);
		} catch (SWTError | Exception e) {
			logger.error("Unable to print the reciept.", e);
			MessageDialogs.displayError(shell, "Unable To Print", "Unexpected error occured while printing.");
		}
	}

	public void printClaimAndShopTicket(Long orderId) {
		logger.debug("Starting printClaimTicket for orderId=" + orderId);
		if (shell == null) {
			throw new IllegalStateException("The shell must be set on the service prior to printing.");
		}
		checkNotNull(orderId);

		OrderRecord orderRecord = orderDelegate.loadOrderRecord(orderId);
		if (orderRecord == null) {
			logger.error("Unable to load a order record for orderId = " + orderId);
			MessageDialogs.displayError(shell, "Unable To Print",
					"An error occured and the request cannot be printed at this time. "
							+ "Please generate manual tickets for this order until the problem can be fixed.");
			return;
		} else if (orderRecord.getClientProducts().isEmpty()) {
			MessageDialogs.displayError(shell, "No Products",
					"No client products have been assigned to this order, unable to print tickets.");
			return;
		}

		ClientRecord clientRecord = null;
		if (orderRecord.getClientId() != null) {
			clientRecord = clientDelegate.getClientRecord(orderRecord.getClientId());
		}

		List<OrderCommentRecord> claimTicketComments = null;
		if (!CollectionUtility.isNullOrEmpty(AppPrefs.claimTicketCommentTypes)) {
			claimTicketComments = orderCommentDelegate.loadOrderComments(orderId, AppPrefs.claimTicketCommentTypes);
		}

		List<OrderCommentRecord> shopTicketComments = null;
		if (!CollectionUtility.isNullOrEmpty(AppPrefs.shopTicketCommentTypes)) {
			shopTicketComments = orderCommentDelegate.loadOrderComments(orderId, AppPrefs.shopTicketCommentTypes);
		}

		ClaimTicketPrint claimTicket = new ClaimTicketPrint(orderRecord, clientRecord, claimTicketComments);
		ShopTicketPrint shopTicket = new ShopTicketPrint(orderRecord, clientRecord, shopTicketComments);

		PrinterData printerData = PaperclipsPrinter.getPrinterData(shell);
		if (printerData != null) {
			try {
				PaperclipsPrinter.print(claimTicket, printerData);
			} catch (Exception e) {
				logger.error("Unable to print claim ticket.", e);
				MessageDialogs.displayError(shell, "Unable To Print", "Unexpected error occured while printing.");
			}

			Boolean shrinkAsNeeded = null;
			while (true) {
				try {
					PaperclipsPrinter.print(shopTicket, printerData);
				} catch (SWTError e) {
					if (shrinkAsNeeded == null) {
						shrinkAsNeeded = MessageDialogs.askQuestion(shell, "Unable To Print",
								"Unable to fit entire contents on a single page. Reprint with a smaller font size?");
						if (!shrinkAsNeeded) {
							break;
						}
					}
					try {
						shopTicket.decreaseFontSize();
					} catch (Exception e2) {
						logger.error(e2);
						MessageDialogs.displayError(shell, "Unable To Print",
								"Unable to print shop ticket on a single page.");
						break;
					}
					continue;
				} catch (Exception e) {
					logger.error("Unable to print the shop ticket.", e);
					MessageDialogs.displayError(shell, "Unable To Print", "Unexpected error occured while printing.");
					break;
				}
				break;
			}
		}
	}

	public void printClaimTicket(Long orderId) {
		logger.debug("Starting printClaimTicket for orderId=" + orderId);
		if (shell == null) {
			throw new IllegalStateException("The shell must be set on the service prior to printing.");
		}
		checkNotNull(orderId);

		OrderRecord orderRecord = orderDelegate.loadOrderRecord(orderId);
		if (orderRecord == null) {
			logger.error("Unable to load a order record for orderId = " + orderId);
			MessageDialogs.displayError(shell, "Unable To Print",
					"An error occured and the claim tickets cannot be printed at this time. "
							+ "Please generate manual claim tickets for this order until the problem can be fixed.");
			return;
		} else if (orderRecord.getClientProducts().isEmpty()) {
			MessageDialogs.displayError(shell, "No Products",
					"No client products have been assigned to this order, unable to print claim tickets.");
			return;
		}

		ClientRecord clientRecord = null;
		if (orderRecord.getClientId() != null) {
			clientRecord = clientDelegate.getClientRecord(orderRecord.getClientId());
		}

		List<OrderCommentRecord> commentRecords = null;
		if (!CollectionUtility.isNullOrEmpty(AppPrefs.claimTicketCommentTypes)) {
			commentRecords = orderCommentDelegate.loadOrderComments(orderId, AppPrefs.claimTicketCommentTypes);
		}

		ClaimTicketPrint print = new ClaimTicketPrint(orderRecord, clientRecord, commentRecords);
		try {
			PaperclipsPrinter.promptAndPrint(shell, print);
		} catch (SWTError | Exception e) {
			logger.error("Unable to print the claim ticket.", e);
			MessageDialogs.displayError(shell, "Unable To Print", "Unexpected error occured while printing.");
		}
	}

	public void printShopTicket(Long orderId) {
		logger.debug("Starting printShopTicket for orderId = " + orderId);
		if (shell == null) {
			throw new IllegalStateException("The shell must be set on the service prior to printing.");
		}
		checkNotNull(orderId);

		OrderRecord orderRecord = orderDelegate.loadOrderRecord(orderId);
		if (orderRecord == null) {
			logger.error("Unable to load order record for orderId = " + orderId);
			MessageDialogs.displayError(shell, "Unable To Print",
					"An error occured and the shop tickets cannot be printed at this time. "
							+ "Please generate manual shop tickets for this order until the problem can be fixed.");
			return;
		} else if (orderRecord.getClientProducts().isEmpty()) {
			MessageDialogs.displayError(shell, "No Products",
					"No client products have been assigned to this order, unable to print shop tickets.");
			return;
		}

		ClientRecord clientRecord = null;
		if (orderRecord.getClientId() != null) {
			clientRecord = clientDelegate.getClientRecord(orderRecord.getClientId());
		}

		List<OrderCommentRecord> commentRecords = null;
		if (!CollectionUtility.isNullOrEmpty(AppPrefs.shopTicketCommentTypes)) {
			commentRecords = orderCommentDelegate.loadOrderComments(orderId, AppPrefs.shopTicketCommentTypes);
		}

		ShopTicketPrint print = new ShopTicketPrint(orderRecord, clientRecord, commentRecords);

		PrinterData printerData = PaperclipsPrinter.getPrinterData(shell);
		if (printerData != null) {
			Boolean shrinkAsNeeded = null;
			while (true) {
				try {
					PaperclipsPrinter.print(print, printerData);
				} catch (SWTError e) {
					if (shrinkAsNeeded == null) {
						shrinkAsNeeded = MessageDialogs.askQuestion(shell, "Unable To Print",
								"Unable to fit entire contents on a single page. Reprint with a smaller font size?");
						if (!shrinkAsNeeded) {
							break;
						}
					}
					try {
						print.decreaseFontSize();
					} catch (Exception e2) {
						logger.error(e2);
						MessageDialogs.displayError(shell, "Unable To Print",
								"Unable to print shop ticket on a single page.");
						break;
					}
					continue;
				} catch (Exception e) {
					logger.error("Unable to print the shop ticket.", e);
					MessageDialogs.displayError(shell, "Unable To Print", "Unexpected error occured while printing.");
					break;
				}
				break;
			}
		}
	}
}
