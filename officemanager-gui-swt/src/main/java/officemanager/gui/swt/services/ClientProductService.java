package officemanager.gui.swt.services;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.ClientProductSelectionRecord;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.gui.swt.ui.tables.ClientProductTableComposite;

public class ClientProductService {
	private static final Logger logger = LogManager.getLogger(ClientProductService.class);

	public static final String DATA_SELECTION_RECORD = "DATA_SELECTION_RECORD";
	public static final String DATA_ORDER_RECORD = "DATA_ORDER_RECORD";

	private final List<Integer> columnsToSkip;
	private ClientProductTableComposite composite;

	public ClientProductService() {
		columnsToSkip = Lists.newArrayList();
	}

	public ClientProductService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(ClientProductTableComposite.COL_MANUFACTURER)) {
			columnsToSkip.add(ClientProductTableComposite.COL_MANUFACTURER);
		}
		if (!columnsToKeep.contains(ClientProductTableComposite.COL_MODEL)) {
			columnsToSkip.add(ClientProductTableComposite.COL_MODEL);
		}
		if (!columnsToKeep.contains(ClientProductTableComposite.COL_PRODUCT)) {
			columnsToSkip.add(ClientProductTableComposite.COL_PRODUCT);
		}
		if (!columnsToKeep.contains(ClientProductTableComposite.COL_SERIAL_NUM)) {
			columnsToSkip.add(ClientProductTableComposite.COL_SERIAL_NUM);
		}
	}

	public ClientProductTableComposite getComposite() {
		return composite;
	}

	public void setComposite(ClientProductTableComposite composite) {
		this.composite = composite;
	}

	public void populateTable(List<ClientProductSelectionRecord> records) {
		logger.debug("Starting populateTable.");
		composite.table.removeAll();
		for (ClientProductSelectionRecord record : records) {
			_addRecordToTable(record);
		}
		composite.table.packTableColumns(columnsToSkip);
		composite.layout();
	}

	public void populateTableOrderRecords(List<OrderClientProductRecord> records) {
		logger.debug("Starting populateTable.");
		composite.table.removeAll();
		for (OrderClientProductRecord record : records) {
			_addRecordToTable(record);
		}
		composite.table.packTableColumns(columnsToSkip);
		composite.layout();
	}

	public List<ClientProductSelectionRecord> getCheckedRecords() {
		return composite.table.getCheckedData(DATA_SELECTION_RECORD);
	}

	public List<ClientProductSelectionRecord> getRecords() {
		return composite.table.getDataAll(DATA_SELECTION_RECORD);
	}

	public void addRecord(ClientProductSelectionRecord record) {
		addRecord(record, false);
	}

	public void addRecord(ClientProductSelectionRecord record, boolean checked) {
		TableItem item = _addRecordToTable(record);
		item.setChecked(checked);
		composite.table.packTableColumns(columnsToSkip);
	}

	public void removeRecord(ClientProductSelectionRecord record) {
		Integer index = null;
		for (int i = 0; i < composite.table.getItemCount(); i++) {
			if (record.equals(composite.table.getItem(i).getData(DATA_SELECTION_RECORD))) {
				index = i;
				break;
			}
		}
		if (index != null) {
			composite.table.remove(index);
		}
	}

	public ClientProductSelectionRecord getRecord(int index) {
		checkArgument(index >= 0);
		checkArgument(index < composite.table.getItemCount());
		return (ClientProductSelectionRecord) composite.table.getItem(index).getData(DATA_SELECTION_RECORD);
	}

	private TableItem _addRecordToTable(ClientProductSelectionRecord record) {
		TableItem item = new TableItem(composite.table, SWT.NONE);
		item.setText(ClientProductTableComposite.COL_MANUFACTURER, record.getProductManufacturer());
		item.setText(ClientProductTableComposite.COL_MODEL, record.getProductModel());
		item.setText(ClientProductTableComposite.COL_PRODUCT, record.getProductType());
		item.setText(ClientProductTableComposite.COL_SERIAL_NUM, record.getSerialNumber());
		item.setData(DATA_SELECTION_RECORD, record);
		return item;
	}

	private TableItem _addRecordToTable(OrderClientProductRecord record) {
		TableItem item = new TableItem(composite.table, SWT.NONE);
		item.setText(ClientProductTableComposite.COL_MANUFACTURER, record.getProductManufacturer());
		item.setText(ClientProductTableComposite.COL_MODEL, record.getProductModel());
		item.setText(ClientProductTableComposite.COL_PRODUCT, record.getProductType());
		item.setText(ClientProductTableComposite.COL_SERIAL_NUM, record.getSerialNumber());
		item.setData(DATA_ORDER_RECORD, record);
		return item;
	}
}
