package officemanager.gui.swt.services;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.delegates.LocationDelegate;
import officemanager.biz.records.LocationRecord;
import officemanager.biz.records.LocationType;
import officemanager.gui.swt.ui.composites.LocationTreeComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class LocationTreeService {

	private static final Logger logger = LogManager.getLogger(LocationTreeService.class);

	private static final Image IMG_BUILDING = ResourceManager.getImage(ImageFile.BUILDING, 16);
	private static final Image IMG_ROOM = ResourceManager.getImage(ImageFile.ROOM, 16);
	private static final Image IMG_INV_LOC = ResourceManager.getImage(ImageFile.SHELF, 16);

	private static final String DATA_LOCATION_RECORD = "DATA_LOCATION_RECORD";
	private static final String DATA_LOADED_CHILDREN = "DATA_LOADED_CHILDREN";

	private final LocationDelegate locationDelegate;

	private final Set<Long> locationsToSelect;

	private LocationTreeComposite composite;

	public LocationTreeService() {
		locationDelegate = new LocationDelegate();
		locationsToSelect = Sets.newHashSet();
	}

	public void setComposite(LocationTreeComposite composite) {
		this.composite = composite;
		addListeners();
	}

	public void setLocationToSelect(Long locationId) {
		checkArgument(locationId != null, "The locationId cannot be null.");
		locationsToSelect.add(locationId);
	}

	public void setLocationsToSelect(Collection<Long> locationIds) {
		checkArgument(locationIds != null, "The locationIds cannot be null.");
		locationsToSelect.addAll(locationIds);
	}

	public LocationRecord getSelectedLocation() {
		TreeItem[] selectedItems = composite.treeLocations.getSelection();
		if (selectedItems.length == 0) {
			logger.debug("No currently selected locations.");
			return null;
		}
		if (selectedItems.length > 1) {
			logger.warn("Multiple locations selected returning first.");
		}
		return (LocationRecord) selectedItems[0].getData(DATA_LOCATION_RECORD);
	}

	public Long getCheckedLocationId() {
		logger.debug("Starting getCheckedLocationId.");
		List<Long> locationIds = getCheckedLocationIds();
		if (locationIds.isEmpty()) {
			logger.debug("No currently checked locations.");
			return null;
		}
		if (locationIds.size() > 1) {
			logger.warn("Multiple locations checked returning first.");
		}
		return locationIds.get(0);
	}

	public List<Long> getCheckedLocationIds() {
		logger.debug("Starting getCheckedLocationIds.");
		List<Long> locationIds;
		if (!locationsToSelect.isEmpty()) {
			logger.debug("Locations previously selected but not loaded for yet.");
			locationIds = Lists.newArrayList(locationsToSelect);
		} else {
			locationIds = Lists.newArrayList();
		}
		for (TreeItem item : composite.treeLocations.getItems()) {
			addCheckedLocations(item, locationIds);
		}
		return locationIds;
	}

	private void addCheckedLocations(TreeItem item, List<Long> locationIds) {
		if (item.getChecked()) {
			locationIds.add(((LocationRecord) item.getData(DATA_LOCATION_RECORD)).getLocationId());
		}
		for (TreeItem child : item.getItems()) {
			addCheckedLocations(child, locationIds);
		}
	}

	public void loadRootLocations() {
		logger.debug("Starting load root locations.");

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.treeLocations.removeAll();
			List<LocationRecord> rootLocations = locationDelegate.getRootLocations();
			logger.debug(rootLocations.size() + " root level locations found.");
			Map<Long, TreeItem> treeItemMap = Maps.newHashMap();
			List<Long> locationIds = Lists.newArrayList();
			for (LocationRecord record : rootLocations) {
				TreeItem locationRoot = makeLocationItem(composite.treeLocations, record);
				locationRoot.setData(DATA_LOADED_CHILDREN, true);
				treeItemMap.put(record.getLocationId(), locationRoot);
				locationIds.add(record.getLocationId());
			}

			logger.debug("Loading first level children of root locations.");
			if (!locationIds.isEmpty()) {
				List<LocationRecord> locationRecords = locationDelegate.getChildrenLocations(locationIds);
				logger.debug(locationRecords.size() + " first level locations found.");
				for (LocationRecord record : locationRecords) {
					TreeItem parentItem = treeItemMap.get(record.getParentLocationId());
					makeLocationItem(parentItem, record);
				}
			}
		});
	}

	private void addListeners() {
		composite.treeLocations.addTreeListener(new TreeListener() {
			@Override
			public void treeCollapsed(TreeEvent e) {}

			@Override
			public void treeExpanded(TreeEvent e) {
				BusyIndicator.showWhile(Display.getDefault(), () -> {
					TreeItem ti = (TreeItem) e.item;
					TreeItem[] children = ti.getItems();
					Map<Long, TreeItem> treeItemMap = Maps.newHashMap();
					List<Long> locationIds = Lists.newArrayList();
					for (TreeItem childItem : children) {
						if ((boolean) childItem.getData(DATA_LOADED_CHILDREN)) {
							continue;
						}
						LocationRecord record = (LocationRecord) childItem.getData(DATA_LOCATION_RECORD);
						treeItemMap.put(record.getLocationId(), childItem);
						locationIds.add(record.getLocationId());
						childItem.setData(DATA_LOADED_CHILDREN, true);
					}

					if (!locationIds.isEmpty()) {
						logger.debug("Loading grand children locations for expanded location.");
						List<LocationRecord> locationRecords = locationDelegate.getChildrenLocations(locationIds);
						locationIds.clear();
						for (LocationRecord record : locationRecords) {
							TreeItem parentItem = treeItemMap.get(record.getParentLocationId());
							makeLocationItem(parentItem, record);
						}
					}
				});
			}
		});
		composite.treeLocations.addListener(SWT.MeasureItem, e -> {});
	}

	private TreeItem makeLocationItem(Tree tree, LocationRecord location) {
		TreeItem item = new TreeItem(tree, SWT.NONE);
		populateLocationItem(item, location);
		return item;
	}

	private TreeItem makeLocationItem(TreeItem parentItem, LocationRecord location) {
		TreeItem item = new TreeItem(parentItem, SWT.NONE);
		populateLocationItem(item, location);
		return item;
	}

	private void populateLocationItem(TreeItem locationItem, LocationRecord location) {
		if (locationsToSelect.contains(location.getLocationId())) {
			locationItem.setChecked(true);
			locationsToSelect.remove(location.getLocationId());
		}
		locationItem.setExpanded(false);
		locationItem.setText(location.getDisplay());
		LocationType locType = locationDelegate.getLocationType(location.getLocationType().getCodeValue());
		locationItem.setImage(getLocationImage(locType));
		locationItem.setData(DATA_LOCATION_RECORD, location);
		locationItem.setData(DATA_LOADED_CHILDREN, false);
	}

	private Image getLocationImage(LocationType locationType) {
		if (locationType == null) {
			return null;
		}
		if (locationType == LocationType.BUILDING) {
			return IMG_BUILDING;
		}
		if (locationType == LocationType.INVENTORY_LOC) {
			return IMG_INV_LOC;
		}
		if (locationType == LocationType.ROOM) {
			return IMG_ROOM;
		}
		return null;
	}
}
