package officemanager.gui.swt.services;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import officemanager.biz.records.PhoneRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.PhoneTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PhoneService {
	public static String DATA_RECORD = "DATA_RECORD";

	private PhoneTableComposite phoneTbl;

	public PhoneService() {}

	public PhoneTableComposite getComposite() {
		return phoneTbl;
	}

	public void setComposite(PhoneTableComposite phoneTbl) {
		this.phoneTbl = phoneTbl;
	}

	public void populatePhoneTable(List<PhoneRecord> phones) {
		phoneTbl.table.removeAll();
		for (PhoneRecord phone : phones) {
			_addRow(phone);
		}
		phoneTbl.table.packTableColumns();
	}

	public void addPhoneRow(PhoneRecord phone) {
		_addRow(phone);
		phoneTbl.table.packTableColumns();
	}

	private void _addRow(PhoneRecord phone) {
		TableItem item = new TableItem(phoneTbl.table, SWT.NONE);
		if (phone.isPrimaryPhone()) {
			for (TableItem otherItem : phoneTbl.table.getItems()) {
				if (otherItem == item) {
					continue;
				}
				otherItem.setImage(PhoneTableComposite.COL_PRIMARY_IND, null);
				((PhoneRecord) otherItem.getData(DATA_RECORD)).setPrimaryPhone(false);
			}
			item.setImage(PhoneTableComposite.COL_PRIMARY_IND, ResourceManager.getImage(ImageFile.CHECK, 16));
		}
		item.setText(PhoneTableComposite.COL_PHONE_NUM, AppPrefs.FORMATTER.formatPhoneNumber(phone.getPhoneNumber()));
		item.setText(PhoneTableComposite.COL_EXT, phone.getExtension());
		item.setText(PhoneTableComposite.COL_PHONE_TYPE, phone.getPhoneType().getDisplay());

		item.setData(DATA_RECORD, phone);
	}
}
