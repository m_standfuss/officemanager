package officemanager.gui.swt.services;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.PriceOverrideRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.PriceOverrideTableComposite;

public class PriceOverrideService {
	private static final String DATA_RECORD = "DATA_COMMENT_RECORD";

	private final List<Integer> columnsToSkip;
	private PriceOverrideTableComposite composite;

	public PriceOverrideService() {
		columnsToSkip = Lists.newArrayList();
	}

	public PriceOverrideService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(PriceOverrideTableComposite.COL_DATE)) {
			columnsToSkip.add(PriceOverrideTableComposite.COL_DATE);
		}
		if (!columnsToKeep.contains(PriceOverrideTableComposite.COL_ORDER_NUM)) {
			columnsToSkip.add(PriceOverrideTableComposite.COL_ORDER_NUM);
		}
		if (!columnsToKeep.contains(PriceOverrideTableComposite.COL_ORIG_PRICE)) {
			columnsToSkip.add(PriceOverrideTableComposite.COL_ORIG_PRICE);
		}
		if (!columnsToKeep.contains(PriceOverrideTableComposite.COL_OVERRIDE_PRICE)) {
			columnsToSkip.add(PriceOverrideTableComposite.COL_OVERRIDE_PRICE);
		}
		if (!columnsToKeep.contains(PriceOverrideTableComposite.COL_PRSNL_NAME)) {
			columnsToSkip.add(PriceOverrideTableComposite.COL_PRSNL_NAME);
		}
		if (!columnsToKeep.contains(PriceOverrideTableComposite.COL_REASON)) {
			columnsToSkip.add(PriceOverrideTableComposite.COL_REASON);
		}
	}

	public void setComposite(PriceOverrideTableComposite composite) {
		this.composite = composite;
		composite.table.hideColumns(columnsToSkip);
	}

	public void populateTable(List<PriceOverrideRecord> records) {
		composite.table.removeAll();
		for (PriceOverrideRecord record : records) {
			_addRow(record);
		}
		List<Integer> skips = Lists.newArrayList(columnsToSkip);
		if (!columnsToSkip.contains(PriceOverrideTableComposite.COL_REASON)) {
			skips.add(PriceOverrideTableComposite.COL_REASON);
		}
		composite.table.packTableColumns(columnsToSkip);
		if (!columnsToSkip.contains(PriceOverrideTableComposite.COL_REASON)) {
			composite.table.getColumn(PriceOverrideTableComposite.COL_REASON).setWidth(200);
		}
	}

	public void addRow(PriceOverrideRecord record) {
		_addRow(record);
		composite.table.packTableColumns(columnsToSkip);
	}

	private void _addRow(PriceOverrideRecord record) {
		TableItem item = new TableItem(composite.table, SWT.None);
		item.setText(PriceOverrideTableComposite.COL_DATE,
				AppPrefs.dateFormatter.formatFullDate_DB(record.getOverrideDttm()));
		item.setText(PriceOverrideTableComposite.COL_ORDER_NUM,
				AppPrefs.FORMATTER.formatOrderNumber(record.getOrderId()));
		item.setText(PriceOverrideTableComposite.COL_ORIG_PRICE,
				AppPrefs.FORMATTER.formatMoney(record.getOriginalAmount()));
		item.setText(PriceOverrideTableComposite.COL_OVERRIDE_PRICE,
				AppPrefs.FORMATTER.formatMoney(record.getOverrideAmount()));
		item.setText(PriceOverrideTableComposite.COL_PRSNL_NAME, record.getOverridePrsnlName());
		item.setText(PriceOverrideTableComposite.COL_REASON, record.getOverrideReason());
		item.setData(DATA_RECORD, record);
	}
}
