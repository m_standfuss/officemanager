package officemanager.gui.swt.services;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.PaymentRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.PaymentTableComposite;

public class PaymentTableService extends OfficeManagerService {
	public static String DATA_RECORD = "DATA_RECORD";

	private final List<Integer> columnsToSkip;
	private PaymentTableComposite paymentTbl;

	public PaymentTableService() {
		columnsToSkip = Lists.newArrayList();
	}

	public PaymentTableService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(PaymentTableComposite.COL_AMOUNT)) {
			columnsToSkip.add(PaymentTableComposite.COL_AMOUNT);
		}
		if (!columnsToKeep.contains(PaymentTableComposite.COL_DATE)) {
			columnsToSkip.add(PaymentTableComposite.COL_DATE);
		}
		if (!columnsToKeep.contains(PaymentTableComposite.COL_TYPE)) {
			columnsToSkip.add(PaymentTableComposite.COL_TYPE);
		}
		if (!columnsToKeep.contains(PaymentTableComposite.COL_NUMBER)) {
			columnsToSkip.add(PaymentTableComposite.COL_NUMBER);
		}
		if (!columnsToKeep.contains(PaymentTableComposite.COL_DESCRIPTION)) {
			columnsToSkip.add(PaymentTableComposite.COL_DESCRIPTION);
		}
	}

	public List<PaymentRecord> getPayments() {
		List<PaymentRecord> records = Lists.newArrayList();
		for (TableItem item : paymentTbl.table.getItems()) {
			records.add((PaymentRecord) item.getData(DATA_RECORD));
		}
		return records;
	}

	public PaymentTableComposite getComposite() {
		return paymentTbl;
	}

	public void setComposite(PaymentTableComposite paymentTbl) {
		this.paymentTbl = paymentTbl;
	}

	public void populatePayments(List<PaymentRecord> paymentRecords) {
		paymentTbl.table.removeAll();
		for (PaymentRecord payment : paymentRecords) {
			addPaymentRow(payment);
		}
		paymentTbl.table.packTableColumns(columnsToSkip);
	}

	public void addPayment(PaymentRecord payment) {
		addPaymentRow(payment);
		paymentTbl.table.packTableColumns(columnsToSkip);
	}

	public void removePayment(PaymentRecord payment) {
		for (int i = 0; i < paymentTbl.table.getItemCount(); i++) {
			TableItem item = paymentTbl.table.getItem(i);
			if (item.getData(DATA_RECORD) == payment) {
				paymentTbl.table.remove(i);
				return;
			}
		}
	}

	public void updatePayment(PaymentRecord record) {
		for (TableItem item : paymentTbl.table.getItems()) {
			if (item.getData(DATA_RECORD) == record) {
				updateRow(record, item);
			}
		}
	}

	private void addPaymentRow(PaymentRecord payment) {
		TableItem item = new TableItem(paymentTbl.table, SWT.NONE);
		updateRow(payment, item);
	}

	private void updateRow(PaymentRecord payment, TableItem item) {
		item.setText(PaymentTableComposite.COL_AMOUNT, AppPrefs.FORMATTER.formatMoney(payment.getAmount()));
		item.setText(PaymentTableComposite.COL_DATE,
				AppPrefs.dateFormatter.formatFullDate_DB(payment.getCreatedDtTm()));
		item.setText(PaymentTableComposite.COL_TYPE, payment.getPaymentTypeDisplay());
		item.setText(PaymentTableComposite.COL_NUMBER, payment.getPaymentNumber());
		item.setText(PaymentTableComposite.COL_DESCRIPTION, payment.getPaymentDescription());

		item.setData(DATA_RECORD, payment);
	}
}
