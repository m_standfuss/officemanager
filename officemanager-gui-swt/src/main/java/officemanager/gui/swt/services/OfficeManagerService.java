package officemanager.gui.swt.services;

import java.awt.GridLayout;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Control;

public abstract class OfficeManagerService {

	/**
	 * Includes/excludes a control that is part of a {@link GridLayout}, by
	 * setting the controls layout data exclude. Does not check that the layout
	 * data is of {@link GridData} just does a blind cast so on the consumer to
	 * verify that prior to using or this will throw a cast exception.
	 * 
	 * @param control
	 *            The control to include/exclude.
	 * @param exclude
	 *            Wheter to exclude this control or not.
	 */
	protected void hideControl(Control control, boolean exclude) {
		if (control.getLayoutData() == null) {
			control.setLayoutData(new GridData());
		}
		((GridData) control.getLayoutData()).exclude = exclude;
		control.setVisible(!exclude);
	}
}
