package officemanager.gui.swt.services;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.LocationRecord;
import officemanager.gui.swt.ui.tables.LocationTableComposite;
import officemanager.utility.CollectionUtility;

public class LocationTableService {

	private static final String DATA_RECORD = "DATA_RECORD";

	private final List<Integer> columnsToSkip;

	private LocationTableComposite composite;

	public LocationTableService() {
		columnsToSkip = Lists.newArrayList();
	}

	public LocationTableService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(LocationTableComposite.COL_ABBR_NAME)) {
			columnsToSkip.add(LocationTableComposite.COL_ABBR_NAME);
		}
		if (!columnsToKeep.contains(LocationTableComposite.COL_LOCATION_NAME)) {
			columnsToSkip.add(LocationTableComposite.COL_LOCATION_NAME);
		}
		if (!columnsToKeep.contains(LocationTableComposite.COL_TYPE)) {
			columnsToSkip.add(LocationTableComposite.COL_TYPE);
		}
	}

	public void setComposite(LocationTableComposite composite) {
		this.composite = composite;
		composite.table.hideColumns(columnsToSkip);
	}

	public void populateTable(List<LocationRecord> records) {
		composite.table.removeAll();
		if (CollectionUtility.isNullOrEmpty(records)) {
			return;
		}
		for (LocationRecord record : records) {
			TableItem item = new TableItem(composite.table, SWT.NONE);
			item.setText(LocationTableComposite.COL_ABBR_NAME, record.getAbbrDisplay());
			item.setText(LocationTableComposite.COL_LOCATION_NAME, record.getDisplay());
			item.setText(LocationTableComposite.COL_TYPE, record.getLocationType().getDisplay());

			item.setData(DATA_RECORD, record);
		}
		composite.table.packTableColumns(columnsToSkip);
	}

	public Long getSelectedLocationId() {
		if (composite.table.getSelectionCount() == 0) {
			return null;
		}
		TableItem[] selectedRows = composite.table.getSelection();
		TableItem row = selectedRows[0];
		return ((LocationRecord) row.getData(DATA_RECORD)).getLocationId();
	}
}
