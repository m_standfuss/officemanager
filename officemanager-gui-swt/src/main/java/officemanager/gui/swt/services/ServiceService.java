package officemanager.gui.swt.services;

import java.math.BigDecimal;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.ServiceTableComposite;

public class ServiceService {
	public static String DATA_RECORD = "DATA_RECORD";

	private ServiceTableComposite composite;

	public ServiceService() {}

	public void setComposite(ServiceTableComposite composite) {
		this.composite = composite;
	}

	public void populateServiceTable(List<OrderServiceRecord> services) {
		composite.table.removeAll();
		for (OrderServiceRecord service : services) {
			addServiceRow(service);
		}
		composite.table.packTableColumns(Lists.newArrayList(ServiceTableComposite.COL_DESCRIPTION));
		composite.table.getColumn(ServiceTableComposite.COL_DESCRIPTION).setWidth(200);
	}

	public void addServiceRow(OrderServiceRecord service) {
		_addServiceRow(service);
		composite.table.packTableColumns(Lists.newArrayList(ServiceTableComposite.COL_DESCRIPTION));
		composite.table.getColumn(ServiceTableComposite.COL_DESCRIPTION).setWidth(200);
	}

	private void _addServiceRow(OrderServiceRecord service) {
		TableItem item = new TableItem(composite.table, SWT.NONE);

		if (service.getServiceText() != null) {
			item.setText(ServiceTableComposite.COL_DESCRIPTION, service.getServiceText().getLongText());
		}
		item.setText(ServiceTableComposite.COL_HOURS, AppPrefs.FORMATTER.formatDecimal(service.getHoursQnt(), 2));
		item.setText(ServiceTableComposite.COL_RATE, AppPrefs.FORMATTER.formatMoney(service.getPricePerHour()));
		item.setText(ServiceTableComposite.COL_SERVICE_NAME, service.getServiceName());

		BigDecimal total = Calculator.calculatePrice(service.getPricePerHour(), service.getHoursQnt());
		item.setText(ServiceTableComposite.COL_TOTAL, AppPrefs.FORMATTER.formatMoney(total));

		item.setData(DATA_RECORD, service);
	}
}
