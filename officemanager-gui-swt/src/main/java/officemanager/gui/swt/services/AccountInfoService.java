package officemanager.gui.swt.services;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import officemanager.biz.Calculator;
import officemanager.biz.records.AccountRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.AccountInformationComposite;

public class AccountInfoService {

	private final Logger logger = LogManager.getLogger(AccountInfoService.class);

	private AccountInformationComposite composite;

	public AccountInfoService() {}

	public void setComposite(AccountInformationComposite composite) {
		checkNotNull(composite);
		this.composite = composite;
	}

	public void populateComposite(AccountRecord record) {
		logger.debug("Starting populateComposite for record=" + record);
		if (record == null) {
			clearComposite();
			return;
		}
		composite.lblCurrentBalance.setText(AppPrefs.FORMATTER.formatMoney(Calculator.negate(record.getBalance())));
		composite.lblLastStatement.setText(AppPrefs.dateFormatter.formatDate_DB(record.getLastStatementDttm()));
		composite.lblStatus.setText(record.getAccountStatusDisplay());

	}

	private void clearComposite() {
		composite.lblCurrentBalance.setText("");
		composite.lblLastStatement.setText("");
		composite.lblStatus.setText("");
	}
}
