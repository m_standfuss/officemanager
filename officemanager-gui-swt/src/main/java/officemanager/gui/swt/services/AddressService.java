package officemanager.gui.swt.services;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.AddressRecord;
import officemanager.gui.swt.ui.tables.AddressTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class AddressService {
	public static String DATA_RECORD = "DATA_RECORD";

	private AddressTableComposite addressTbl;

	public AddressService() {}

	public AddressTableComposite getComposite() {
		return addressTbl;
	}

	public void setComposite(AddressTableComposite addressTbl) {
		this.addressTbl = addressTbl;
	}

	public void populateAddress(List<AddressRecord> addressRecords) {
		addressTbl.table.removeAll();
		for (AddressRecord address : addressRecords) {
			addAddressRow(address);
		}
		addressTbl.table.packTableColumns(Lists.newArrayList(AddressTableComposite.COL_ADDRESS));
		addressTbl.tblclmnAddress.setWidth(200);
	}

	public void addAddress(AddressRecord address) {
		addAddressRow(address);
		addressTbl.table.packTableColumns(Lists.newArrayList(AddressTableComposite.COL_ADDRESS));
		addressTbl.tblclmnAddress.setWidth(200);
	}

	private void addAddressRow(AddressRecord address) {
		TableItem item = new TableItem(addressTbl.table, SWT.NONE);
		if (address.isPrimaryAddress()) {
			for (TableItem otherItem : addressTbl.table.getItems()) {
				if (otherItem == item) {
					continue;
				}
				otherItem.setImage(AddressTableComposite.COL_PRIMARY_IND, null);
				((AddressRecord) otherItem.getData(DATA_RECORD)).setPrimaryAddress(false);
			}
			item.setImage(AddressTableComposite.COL_PRIMARY_IND, ResourceManager.getImage(ImageFile.CHECK, 16));
		}
		item.setText(AddressTableComposite.COL_ADDRESS, address.getAddressCombined());
		item.setText(AddressTableComposite.COL_ADDRESS_TYPE, address.getAddressType().getDisplay());
		item.setText(AddressTableComposite.COL_CITY, address.getCity());
		item.setText(AddressTableComposite.COL_STATE, address.getState());
		item.setText(AddressTableComposite.COL_ZIP, address.getZip());

		item.setData(DATA_RECORD, address);
	}
}
