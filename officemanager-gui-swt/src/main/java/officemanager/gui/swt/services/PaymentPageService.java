package officemanager.gui.swt.services;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.delegates.TradeInDelegate;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PaymentRecord.PaymentType;
import officemanager.biz.records.TradeInRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.wizards.pages.PaymentsPage;

public class PaymentPageService extends PaymentTableService {

	private final PaymentDelegate paymentDelegate;
	private final TradeInDelegate tradeInDelegate;

	private final List<PaymentRecord> paymentsAdded;
	private final List<TradeInRecord> tradeInsAdded;

	private final List<Listener> paymentAddedListeners;

	private Long clientId;
	private Long accountId;
	private BigDecimal amountOwed;
	private boolean allowNegativeBalance;
	private PaymentsPage page;

	public PaymentPageService() {
		paymentDelegate = new PaymentDelegate();
		tradeInDelegate = new TradeInDelegate();

		paymentsAdded = Lists.newArrayList();
		tradeInsAdded = Lists.newArrayList();
		paymentAddedListeners = Lists.newArrayList();
		amountOwed = BigDecimal.ZERO;
	}

	public void setPage(PaymentsPage page) {
		this.page = page;
		page.addFirstShowListener(e -> {
			BusyIndicator.showWhile(Display.getDefault(), () -> {
				addListeners();
				setComposite(page.paymentTableComposite);
				page.paymentEditComposite.cmbCreditCardCompanies
						.populateSelections(paymentDelegate.getCreditCardCompanies());
				page.tradeInComposite.cmbTradeInProductType.populateSelections(tradeInDelegate.getProductTypes());
				page.tradeInComposite.cmbTradeInType.populateSelections(tradeInDelegate.getTradeInTypes());
			});
		});
	}

	public void setAmountOwed(BigDecimal amountOwed) {
		this.amountOwed = amountOwed;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public void setAllowNegativeBalance(boolean allowNegativeBalance) {
		this.allowNegativeBalance = allowNegativeBalance;
	}

	public BigDecimal getBalance() {
		List<PaymentRecord> records = getPayments();
		BigDecimal paymentTotal = Calculator.calculatePaymentTotal(records);
		return Calculator.calculateBalance(amountOwed, paymentTotal);
	}

	public void addPaymentAddedListener(Listener l) {
		checkNotNull(l);
		paymentAddedListeners.add(l);
	}

	public List<PaymentRecord> getPaymentsAdded() {
		return paymentsAdded;
	}

	public List<TradeInRecord> getTradeInsAdded() {
		return tradeInsAdded;
	}

	public void populatePage(boolean includeClientCredit, boolean includeTradeIn) {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			page.paymentEditComposite.cmbType
					.populateSelections(paymentDelegate.getPaymentOptions(includeClientCredit, includeTradeIn));
			changePaymentType();
			refreshTotals();
		});
	}

	protected void refreshTotals() {
		List<PaymentRecord> records = getPayments();
		BigDecimal paymentTotal = Calculator.calculatePaymentTotal(records);
		BigDecimal balance = Calculator.calculateBalance(amountOwed, paymentTotal);

		page.orderPaymentTotalsComposite.lblBalance.setText(AppPrefs.FORMATTER.formatMoney(balance));
		page.orderPaymentTotalsComposite.lblTotal.setText(AppPrefs.FORMATTER.formatMoney(amountOwed));
		page.orderPaymentTotalsComposite.lblPayments.setText(AppPrefs.FORMATTER.formatMoney(paymentTotal));

		page.orderPaymentTotalsComposite.lblBalance.setForeground(balance.compareTo(BigDecimal.ZERO) < 0
				? AppPrefs.COLOR_NEGATIVE_PRICE : page.orderPaymentTotalsComposite.lblTotal.getForeground());

		page.orderPaymentTotalsComposite.layout();
	}

	private boolean validatePrices() {
		Long selectedCodeValue = page.paymentEditComposite.cmbType.getCodeValueRecord().getCodeValue();
		PaymentType type = paymentDelegate.getPaymentType(selectedCodeValue);

		PaymentRecord newPayment = new PaymentRecord();
		newPayment.setAmount(page.paymentEditComposite.txtAmount.getValue());
		newPayment.setPaymentType(type);

		List<PaymentRecord> records = Lists.newArrayList(getPayments());
		records.add(newPayment);
		BigDecimal paymentTotal = Calculator.calculatePaymentTotal(records);
		BigDecimal balance = Calculator.calculateBalance(amountOwed, paymentTotal);

		if (Calculator.isZeroAmount(balance)) {
			return true;
		}

		List<PaymentRecord> newPayments = Lists.newArrayList(getPaymentsAdded());
		newPayments.add(newPayment);
		List<PaymentRecord> cashPayments = Lists.newArrayList();
		for (PaymentRecord record : records) {
			if (record.getPaymentType() == PaymentType.CASH) {
				cashPayments.add(record);
			}
		}

		BigDecimal cashPaymentTotal = Calculator.calculatePaymentTotal(cashPayments);
		return cashPaymentTotal.compareTo(balance.multiply(new BigDecimal("-1"))) >= 0;
	}

	private String getTradeInDescription() {
		StringBuilder sb = new StringBuilder();
		sb.append(page.tradeInComposite.txtTradeInManuf.getText());
		if (!page.tradeInComposite.txtTradeInModel.getText().isEmpty()) {
			sb.append("-").append(page.tradeInComposite.txtTradeInModel.getText());
		}
		return sb.toString();
	}

	private boolean validate() {
		if (page.paymentEditComposite.cmbType.getCodeValueRecord() == null) {
			page.setErrorMessage("Please select a payment type.");
			return false;
		}
		if (page.paymentEditComposite.txtAmount.getValue() == null) {
			page.setErrorMessage("Please enter a payment amount.");
			return false;
		}
		if (!allowNegativeBalance && !validatePrices()) {
			page.setErrorMessage("Change can only be made with new cash payments.");
			return false;
		}
		Long selectedCodeValue = page.paymentEditComposite.cmbType.getCodeValueRecord().getCodeValue();
		PaymentType type = paymentDelegate.getPaymentType(selectedCodeValue);
		switch (type) {
		case CASH:
			break;
		case CHECK:
			if (page.paymentEditComposite.txtNumber.getText().isEmpty()) {
				page.setErrorMessage("Please enter a check number.");
				return false;
			}
			break;
		case CLIENT_CREDIT:
			break;
		case CREDIT_CARD:
			if (page.paymentEditComposite.cmbCreditCardCompanies.getCodeValueRecord() == null) {
				page.setErrorMessage("Please enter a credit card company.");
				return false;
			}
			if (page.paymentEditComposite.txtNumber.getText().isEmpty()) {
				page.setErrorMessage("Please enter a card number.");
				return false;
			}
			break;
		case FINANCING:
			if (page.paymentEditComposite.txtDescription.getText().isEmpty()) {
				page.setErrorMessage("Please enter a financing company.");
				return false;
			}
			break;
		case GIFT_CARD:
			if (page.paymentEditComposite.txtNumber.getText().isEmpty()) {
				page.setErrorMessage("Please enter a gift card number.");
				return false;
			}
			break;
		case PENDING:
			break;
		case TRADE_IN:
			if (page.tradeInComposite.cmbTradeInType.getCodeValueRecord() == null) {
				page.setErrorMessage("Please enter a trade in type.");
				return false;
			}
			if (page.tradeInComposite.txtTradeInManuf.getText().isEmpty()) {
				page.setErrorMessage("Please enter the manufacturer of the trade in item.");
				return false;
			}
			break;
		default:
			break;
		}

		page.setErrorMessage(null);
		return true;
	}

	private void addListeners() {
		page.paymentEditComposite.cmbType.addListener(SWT.Selection, e -> changePaymentType());
		page.buttonBarComposite.btnClear.addListener(SWT.Selection, e -> clearPayment());
		page.buttonBarComposite.btnSave.addListener(SWT.Selection, e -> savePayment());
	}

	private void savePayment() {
		if (!validate()) {
			return;
		}

		PaymentRecord payment = new PaymentRecord();
		payment.setAccountId(accountId);
		payment.setAmount(page.paymentEditComposite.txtAmount.getValue());

		Long selectedCodeValue = page.paymentEditComposite.cmbType.getCodeValueRecord().getCodeValue();
		PaymentType type = paymentDelegate.getPaymentType(selectedCodeValue);

		if (type == PaymentType.CREDIT_CARD) {
			payment.setPaymentDescription(page.paymentEditComposite.cmbCreditCardCompanies.getText());
		} else if (type == PaymentType.TRADE_IN) {
			payment.setPaymentDescription(getTradeInDescription());
		} else {
			payment.setPaymentDescription(page.paymentEditComposite.txtDescription.getText());
		}
		payment.setPaymentNumber(page.paymentEditComposite.txtNumber.getText());
		payment.setPaymentType(type);
		payment.setPaymentTypeDisplay(page.paymentEditComposite.cmbType.getText());

		addPayment(payment);
		paymentsAdded.add(payment);

		if (type == PaymentType.TRADE_IN) {
			TradeInRecord tradeIn = new TradeInRecord();
			tradeIn.setAmount(page.paymentEditComposite.txtAmount.getValue());
			tradeIn.setClientId(clientId);
			tradeIn.setDescription(page.tradeInComposite.txtTradeInDescription.getText());
			tradeIn.setManufacturer(page.tradeInComposite.txtTradeInManuf.getText());
			tradeIn.setModel(page.tradeInComposite.txtTradeInModel.getText());
			tradeIn.setProductTypeCd(page.tradeInComposite.cmbTradeInProductType.getCodeValueRecord());
			tradeIn.setTradeInType(page.tradeInComposite.cmbTradeInType.getCodeValueRecord());

			tradeInsAdded.add(tradeIn);
		}

		clearPayment();
		refreshTotals();
		for (Listener l : paymentAddedListeners) {
			l.handleEvent(new Event());
		}
		page.setErrorMessage(null);
	}

	private void clearPayment() {
		page.paymentEditComposite.cmbType.deselectAll();
		page.paymentEditComposite.cmbCreditCardCompanies.deselectAll();
		page.paymentEditComposite.txtAmount.setValue(null);
		page.paymentEditComposite.txtDescription.setText("");
		page.paymentEditComposite.txtNumber.setText("");
		page.tradeInComposite.cmbTradeInProductType.deselectAll();
		page.tradeInComposite.cmbTradeInType.deselectAll();
		page.tradeInComposite.txtTradeInDescription.setText("");
		page.tradeInComposite.txtTradeInManuf.setText("");
		page.tradeInComposite.txtTradeInModel.setText("");
		changePaymentType();
	}

	private void changePaymentType() {
		PaymentType type = null;
		if (page.paymentEditComposite.cmbType.getCodeValueRecord() != null) {
			Long selectedCodeValue = page.paymentEditComposite.cmbType.getCodeValueRecord().getCodeValue();
			type = paymentDelegate.getPaymentType(selectedCodeValue);
		}

		String descriptionLabel = "";
		String numberLabel = "";
		boolean descriptionTextbox = false;
		boolean creditCardCombo = false;
		boolean numberTextbox = false;
		boolean tradeIn = false;
		if (type != null) {
			switch (type) {
			case CASH:
				break;
			case CHECK:
				numberLabel = "Check #:";
				numberTextbox = true;
				break;
			case CLIENT_CREDIT:
				break;
			case CREDIT_CARD:
				descriptionLabel = "Card Company:";
				creditCardCombo = true;
				numberLabel = "Last 4 of Card:";
				numberTextbox = true;
				break;
			case FINANCING:
				descriptionLabel = "Financing Company:";
				descriptionTextbox = true;
				break;
			case GIFT_CARD:
				numberLabel = "Gift Card #:";
				numberTextbox = true;
				break;
			case PENDING:
				break;
			case TRADE_IN:
				tradeIn = true;
			default:
			}
		}

		page.paymentEditComposite.lblDescription.setText(descriptionLabel);
		page.paymentEditComposite.lblNumber.setText(numberLabel);
		hideControl(page.paymentEditComposite.lblDescription, !(descriptionTextbox || creditCardCombo));
		hideControl(page.paymentEditComposite.txtDescription, !descriptionTextbox);
		hideControl(page.paymentEditComposite.cmbCreditCardCompanies, !creditCardCombo);
		hideControl(page.paymentEditComposite.lblNumber, !numberTextbox);
		hideControl(page.paymentEditComposite.txtNumber, !numberTextbox);
		hideControl(page.tradeInInfoCllpCmp, !tradeIn);

		page.paymentEditComposite.layout();
		page.newPaymentCllpCmp.getParent().layout();
		page.paymentEditComposite.getParent().redraw();

		page.scrolledComp.setMinSize(page.scrolledCompComp.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
}
