package officemanager.gui.swt.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.PersonnelDelegate;
import officemanager.biz.records.PersonnelSearchRecord;
import officemanager.biz.searchingcriteria.PersonnelSearchCriteria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PersonnelSearchComposite;
import officemanager.gui.swt.ui.tables.PersonnelTableComposite;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;
import officemanager.gui.swt.ui.wizards.PersonnelWizard;

public class PersonnelSearchService {
	private static final Logger logger = LogManager.getLogger(PersonnelSearchService.class);

	public static final String DATA_RECORD = "DATA_RECORD";
	public static final String DATA_JOINED_DTTM = "DATA_JOINED_DTTM";

	private final List<Integer> columnsToSkip;
	private final PersonnelDelegate personnelDelegate;
	private PersonnelSearchComposite composite;

	public PersonnelSearchService() {
		columnsToSkip = Lists.newArrayList();
		personnelDelegate = new PersonnelDelegate();
	}

	public PersonnelSearchService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(PersonnelTableComposite.COL_NAME)) {
			columnsToSkip.add(PersonnelTableComposite.COL_NAME);
		}
		if (!columnsToKeep.contains(PersonnelTableComposite.COL_PHONE)) {
			columnsToSkip.add(PersonnelTableComposite.COL_PHONE);
		}
		if (!columnsToKeep.contains(PersonnelTableComposite.COL_EMAIL)) {
			columnsToSkip.add(PersonnelTableComposite.COL_EMAIL);
		}
		if (!columnsToKeep.contains(PersonnelTableComposite.COL_START_DTTM)) {
			columnsToSkip.add(PersonnelTableComposite.COL_START_DTTM);
		}
		if (!columnsToKeep.contains(PersonnelTableComposite.COL_STATUS)) {
			columnsToSkip.add(PersonnelTableComposite.COL_STATUS);
		}
		if (!columnsToKeep.contains(PersonnelTableComposite.COL_TYPE)) {
			columnsToSkip.add(PersonnelTableComposite.COL_TYPE);
		}
		if (!columnsToKeep.contains(PersonnelTableComposite.COL_W2_ON_FILE)) {
			columnsToSkip.add(PersonnelTableComposite.COL_W2_ON_FILE);
		}
	}

	public PersonnelSearchComposite getComposite() {
		return composite;
	}

	public void setComposite(PersonnelSearchComposite composite) {
		this.composite = composite;

		composite.btnClear.addListener(SWT.Selection, e -> clear());
		composite.btnSearch.addListener(SWT.Selection, e -> search());
		TraverseListener searchKeyListener = e -> {
			if (e.keyCode == SWT.CR || e.keyCode == 16777296) {
				search();
				e.doit = false;
			}
		};

		composite.txtNameFirst.addTraverseListener(searchKeyListener);
		composite.txtNameLast.addTraverseListener(searchKeyListener);
		composite.txtPhone.addTraverseListener(searchKeyListener);
		composite.txtUsername.addTraverseListener(searchKeyListener);
		composite.searchResultsTable.table.hideColumns(columnsToSkip);
		composite.tlItmNewPersonnel.addListener(SWT.Selection, e -> openNewPersonnelWizard());

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.cdvlListStatus.populateList(personnelDelegate.getPersonnelStatus());
			composite.cdvlListType.populateList(personnelDelegate.getPersonnelTypes());
		});
	}

	private void search() {
		logger.debug("Search button selected.");
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.searchResultsTable.table.removeAll();
			List<PersonnelSearchRecord> searchResults = getSearchResults();
			populateTable(searchResults);
		});
	}

	private void clear() {
		composite.txtNameFirst.setText("");
		composite.txtNameLast.setText("");
		composite.txtPhone.setText("");
		composite.txtUsername.setText("");
		composite.cdvlListStatus.checkAllItems(false);
		composite.cdvlListType.checkAllItems(false);
	}

	private List<PersonnelSearchRecord> getSearchResults() {
		PersonnelSearchCriteria searchCriteria = new PersonnelSearchCriteria();
		searchCriteria.nameFirst = composite.txtNameFirst.getText();
		searchCriteria.nameLast = composite.txtNameLast.getText();
		searchCriteria.primaryPhone = composite.txtPhone.getUnformattedText();
		searchCriteria.username = composite.txtUsername.getText();

		searchCriteria.personnelStatusCds = composite.cdvlListStatus.getCheckedCodeValues();
		searchCriteria.personnelTypeCds = composite.cdvlListType.getCheckedCodeValues();

		return personnelDelegate.searchPersonnel(searchCriteria);
	}

	private void populateTable(List<PersonnelSearchRecord> records) {
		if (records == null) {
			return;
		}
		for (PersonnelSearchRecord record : records) {
			TableItem item = new TableItem(composite.searchResultsTable.table, SWT.NONE);
			item.setText(PersonnelTableComposite.COL_EMAIL, record.getEmail());
			item.setText(PersonnelTableComposite.COL_NAME, record.getNameFullFormatted());
			item.setText(PersonnelTableComposite.COL_PHONE,
					AppPrefs.FORMATTER.formatPhoneNumber(record.getPrimaryPhone()));
			item.setText(PersonnelTableComposite.COL_START_DTTM,
					AppPrefs.dateFormatter.formatDate_DB(record.getStartDttm()));
			item.setText(PersonnelTableComposite.COL_STATUS, record.getPersonnelStatus().getDisplay());
			item.setText(PersonnelTableComposite.COL_TYPE, record.getPersonnelType().getDisplay());
			item.setText(PersonnelTableComposite.COL_W2_ON_FILE, Boolean.toString(record.getW2OnFile()));

			item.setData(DATA_RECORD, record);
		}
		composite.searchResultsTable.table.packTableColumns(columnsToSkip);
	}

	private void openNewPersonnelWizard() {
		PersonnelWizard wizard = new PersonnelWizard();
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Window.OK) {
			search();
		}
	}
}
