package officemanager.gui.swt.services;

import officemanager.biz.records.PersonRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PersonInformationComposite;

public abstract class PersonInformationService {

	private PersonInformationComposite composite;

	protected PersonInformationService() {}

	protected void setComposite(PersonInformationComposite composite) {
		this.composite = composite;
	}

	protected void populatePersonInformation(PersonRecord record) {
		composite.lblEmail.setText(record.getEmail());
		composite.lblFirstName.setText(record.getNameFirst());
		composite.lblLastName.setText(record.getNameLast());
		composite.lblFullFormatted.setText(record.getNameFullFormatted());
		if (record.getPrimaryAddress() == null) {
			composite.lblPrimaryAddress.setText("");
		} else {
			composite.lblPrimaryAddress.setText(AppPrefs.FORMATTER.formatAddress(
					record.getPrimaryAddress().getAddressLines(), record.getPrimaryAddress().getCity(),
					record.getPrimaryAddress().getState(), record.getPrimaryAddress().getZip()));
		}
		if (record.getPrimaryPhone() == null) {
			composite.lblPrimaryPhone.setText("");
		} else {
			composite.lblPrimaryPhone.setText(AppPrefs.FORMATTER.formatPhoneNumber(
					record.getPrimaryPhone().getPhoneNumber(), record.getPrimaryPhone().getExtension()));
		}
	}
}
