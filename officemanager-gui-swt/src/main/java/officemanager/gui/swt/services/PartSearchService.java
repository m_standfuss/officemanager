package officemanager.gui.swt.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.delegates.PartDelegate;
import officemanager.biz.records.PartSearchRecord;
import officemanager.biz.searchingcriteria.PartSearchCriteria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PartSearchComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class PartSearchService extends PartSearchTableService {

	static final Logger logger = LogManager.getLogger(PartSearchService.class);

	private final PartDelegate partDelegate;
	PartSearchComposite composite;

	public PartSearchService() {
		super();
		partDelegate = new PartDelegate();
	}

	public PartSearchService(List<Integer> columnsToKeep) {
		super(columnsToKeep);
		partDelegate = new PartDelegate();
	}

	public PartSearchComposite getComposite() {
		return composite;
	}

	public void setComposite(PartSearchComposite composite) {
		setComposite(composite.searchResultsTable);
		this.composite = composite;

		composite.btnSearch.addListener(SWT.Selection, e -> search());
		composite.btnClear.addListener(SWT.Selection, e -> clear());

		TraverseListener searchKeyListener = e -> {
			if (e.keyCode == SWT.CR || e.keyCode == 16777296) {
				search();
				e.doit = false;
			}
		};

		composite.txtPartName.addTraverseListener(searchKeyListener);
		composite.txtPartDescription.addTraverseListener(searchKeyListener);
		composite.txtManufId.addTraverseListener(searchKeyListener);

		composite.tlItmScanPart.addListener(SWT.Selection, e -> {
			Boolean currentFocus = (Boolean) composite.tlItmScanPart.getData();

			if (currentFocus == null || currentFocus == false) {
				composite.tlItmScanPart.setData(true);
				composite.tlItmScanPart
						.setImage(ResourceManager.getImage(ImageFile.SCAN_CANCEL, AppPrefs.ICN_SIZE_PAGETOOLITEM));
				composite.scnTxt.forceFocus();
			} else {
				composite.tlItmScanPart.setData(false);
				composite.tlItmScanPart
						.setImage(ResourceManager.getImage(ImageFile.SCAN, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			}
		});

		composite.scnTxt.addListener(SWT.FocusOut, e -> {
			composite.tlItmScanPart.setData(false);
			composite.tlItmScanPart.setImage(ResourceManager.getImage(ImageFile.SCAN, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		});

		composite.scnTxt.addListener(SWT.FocusIn, e -> {
			composite.tlItmScanPart.setData(true);
			composite.tlItmScanPart
					.setImage(ResourceManager.getImage(ImageFile.SCAN_CANCEL, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		});
		composite.scnTxt.addEntryEnteredListener(e -> searchByBarcode(e.text));

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.tblCategory.populateList(partDelegate.getPartCategories());
			composite.tblManufacturer.populateList(partDelegate.getPartManufacturers());
			composite.tblProductType.populateList(partDelegate.getProductTypes());
		});
	}

	private void search() {
		logger.debug("Search button selected.");

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			List<PartSearchRecord> searchResults = getSearchResults();
			populateTable(searchResults);
		});
	}

	private void searchByBarcode(String barcode) {
		clear();
		List<PartSearchRecord> searchResults = partDelegate.searchParts(barcode);
		if (searchResults.isEmpty()) {
			MessageDialogs.displayError(composite.getShell(), "Unable to Find Part",
					"Unable to find a part with barcode '" + barcode + "'. Please manually search for the part.");
			return;
		}
		populateTable(searchResults, true);
	}

	private void clear() {
		composite.txtPartName.setText("");
		composite.txtPartDescription.setText("");
		composite.txtManufId.setText("");
		composite.tblManufacturer.checkAllItems(false);
		composite.tblProductType.checkAllItems(false);
		composite.tblCategory.checkAllItems(false);
		composite.btnInStock.setSelection(false);
	}

	private List<PartSearchRecord> getSearchResults() {
		PartSearchCriteria searchCriteria = new PartSearchCriteria();
		searchCriteria.onlyInStock = composite.btnInStock.getSelection();
		searchCriteria.partCategoryCds = composite.tblCategory.getCheckedCodeValues();
		searchCriteria.partDescription = composite.txtPartDescription.getText();
		searchCriteria.partManufacturerCds = composite.tblManufacturer.getCheckedCodeValues();
		searchCriteria.partManufacturerId = composite.txtManufId.getText();
		searchCriteria.partName = composite.txtPartName.getText();
		searchCriteria.productTypeCds = composite.tblProductType.getCheckedCodeValues();
		return partDelegate.searchParts(searchCriteria);
	}
}
