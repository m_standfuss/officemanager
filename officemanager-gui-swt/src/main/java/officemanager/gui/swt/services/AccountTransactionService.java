package officemanager.gui.swt.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.AccountTransactionRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.AccountTransactionTableComposite;
import officemanager.utility.CollectionUtility;

public class AccountTransactionService {

	public static final String DATA_RECORD = "DATA_RECORD";
	private static final Logger logger = LogManager.getLogger(AccountTransactionService.class);

	private final List<Integer> columnsToSkip;
	private AccountTransactionTableComposite composite;

	public AccountTransactionService() {
		columnsToSkip = Lists.newArrayList();
	}

	public AccountTransactionService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(AccountTransactionTableComposite.COL_AMOUNT)) {
			columnsToSkip.add(AccountTransactionTableComposite.COL_AMOUNT);
		}
		if (!columnsToKeep.contains(AccountTransactionTableComposite.COL_TYPE)) {
			columnsToSkip.add(AccountTransactionTableComposite.COL_TYPE);
		}
		if (!columnsToKeep.contains(AccountTransactionTableComposite.COL_DATE)) {
			columnsToSkip.add(AccountTransactionTableComposite.COL_DATE);
		}
		if (!columnsToKeep.contains(AccountTransactionTableComposite.COL_PAYMENT_TYPE)) {
			columnsToSkip.add(AccountTransactionTableComposite.COL_PAYMENT_TYPE);
		}
	}

	public void setComposite(AccountTransactionTableComposite composite) {
		this.composite = composite;
		composite.table.hideColumns(columnsToSkip);
	}

	public void populateTable(List<AccountTransactionRecord> records) {
		composite.table.removeAll();
		if (CollectionUtility.isNullOrEmpty(records)) {
			logger.debug("No records to populate.");
			return;
		}
		for (AccountTransactionRecord record : records) {
			TableItem item = new TableItem(composite.table, SWT.NONE);

			item.setText(AccountTransactionTableComposite.COL_AMOUNT,
					AppPrefs.FORMATTER.formatMoney(record.getAmount()));
			item.setText(AccountTransactionTableComposite.COL_TYPE, record.getTransactionType().getDisplay());
			item.setText(AccountTransactionTableComposite.COL_DATE,
					AppPrefs.dateFormatter.formatFullDate_DB(record.getTransactionDttm()));
			item.setText(AccountTransactionTableComposite.COL_PAYMENT_TYPE, record.getPaymentType().getDisplay());

			item.setData(DATA_RECORD, record);
		}
		composite.table.packTableColumns(columnsToSkip);
	}
}
