package officemanager.gui.swt.services;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.OrderCommentRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderCommentTableComposite;

public class OrderCommentService {

	private static final String DATA_COMMENT_RECORD = "DATA_COMMENT_RECORD";

	private final List<Integer> columnsToSkip;
	private OrderCommentTableComposite composite;

	public OrderCommentService() {
		columnsToSkip = Lists.newArrayList();
	}

	public OrderCommentService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(OrderCommentTableComposite.COL_AUTHOR)) {
			columnsToSkip.add(OrderCommentTableComposite.COL_AUTHOR);
		}
		if (!columnsToKeep.contains(OrderCommentTableComposite.COL_COMMENT)) {
			columnsToSkip.add(OrderCommentTableComposite.COL_COMMENT);
		}
		if (!columnsToKeep.contains(OrderCommentTableComposite.COL_COMMENT_TYPE)) {
			columnsToSkip.add(OrderCommentTableComposite.COL_COMMENT_TYPE);
		}
		if (!columnsToKeep.contains(OrderCommentTableComposite.COL_DATE)) {
			columnsToSkip.add(OrderCommentTableComposite.COL_DATE);
		}
	}

	public void setComposite(OrderCommentTableComposite composite) {
		this.composite = composite;
		composite.table.hideColumns(columnsToSkip);
	}

	public void populateComments(List<OrderCommentRecord> records) {
		composite.table.removeAll();
		for (OrderCommentRecord record : records) {
			_addRow(record);
		}
		composite.table.packTableColumns(columnsToSkip);
	}

	public void addComment(OrderCommentRecord record) {
		_addRow(record);
		composite.table.packTableColumns(columnsToSkip);
	}

	private void _addRow(OrderCommentRecord record) {
		TableItem item = new TableItem(composite.table, SWT.None);
		item.setText(OrderCommentTableComposite.COL_AUTHOR, record.getAuthoredBy());
		item.setText(OrderCommentTableComposite.COL_COMMENT,
				AppPrefs.FORMATTER.substring(record.getLongText().getLongText(), 100));
		item.setText(OrderCommentTableComposite.COL_COMMENT_TYPE, record.getCommentType().getDisplay());
		item.setText(OrderCommentTableComposite.COL_DATE,
				AppPrefs.dateFormatter.formatFullDate_DB(record.getLastUpdtOn()));
		item.setData(DATA_COMMENT_RECORD, record);
	}
}
