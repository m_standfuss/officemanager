package officemanager.gui.swt.services;

import officemanager.biz.records.PersonnelRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PersonnelInformationComposite;

public class PersonnelInformationService extends PersonInformationService {

	private PersonnelInformationComposite composite;

	public PersonnelInformationService() {}

	public void setComposite(PersonnelInformationComposite composite) {
		super.setComposite(composite);
		this.composite = composite;
	}

	public void refresh(PersonnelRecord record) {
		populatePersonInformation(record);
		composite.lblStatus.setText(record.getPersonnelStatus().getDisplay());
		composite.lblPersonnelType.setText(record.getPersonnelType().getDisplay());
		composite.lblUsername.setText(record.getUsername());
		composite.lblStartDate.setText(AppPrefs.dateFormatter.formatDate_DB(record.getStartDttm()));
		composite.lblW2OnFile.setText(Boolean.toString(record.isW2OnFile()));
		composite.lblTerminatedDate.setText(AppPrefs.dateFormatter.formatDate_DB(record.getTerminatedDttm()));
		composite.layout();
	}
}
