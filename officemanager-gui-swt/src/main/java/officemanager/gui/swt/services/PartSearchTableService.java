package officemanager.gui.swt.services;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;

public class PartSearchTableService {

	public static final String DATA_RECORD = "DATA_RECORD";
	public static final String DATE_BASE_PRICE = "DATE_BASE_PRICE";
	public static final String DATA_CURRENT_PRICE = "DATA_CURRENT_PRICE";
	public static final String DATA_AMOUNT_SOLD = "DATA_AMOUNT_SOLD";

	private static final Logger logger = LogManager.getLogger(PartSearchTableService.class);

	private final List<Integer> columnsToSkip;

	private PartSearchTableComposite composite;

	public PartSearchTableService() {
		columnsToSkip = Lists.newArrayList();
	}

	public PartSearchTableService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_AMT_SOLD)) {
			columnsToSkip.add(PartSearchTableComposite.COL_AMT_SOLD);
		}
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_BASE_PRICE)) {
			columnsToSkip.add(PartSearchTableComposite.COL_BASE_PRICE);
		}
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_CURRENT_PRICE)) {
			columnsToSkip.add(PartSearchTableComposite.COL_CURRENT_PRICE);
		}
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_DESCRIPTION)) {
			columnsToSkip.add(PartSearchTableComposite.COL_DESCRIPTION);
		}
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_INVNTRY_CNT)) {
			columnsToSkip.add(PartSearchTableComposite.COL_INVNTRY_CNT);
		}
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_INVNTRY_STATUS)) {
			columnsToSkip.add(PartSearchTableComposite.COL_INVNTRY_STATUS);
		}
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_MANUF)) {
			columnsToSkip.add(PartSearchTableComposite.COL_MANUF);
		}
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_MANUF_ID)) {
			columnsToSkip.add(PartSearchTableComposite.COL_MANUF_ID);
		}
		if (!columnsToKeep.contains(PartSearchTableComposite.COL_PART_NAME)) {
			columnsToSkip.add(PartSearchTableComposite.COL_PART_NAME);
		}
	}

	public void addColumnsToSkip(Integer... columnIndexes) {
		for (Integer column : columnIndexes) {
			columnsToSkip.add(column);
		}
	}

	public void setComposite(PartSearchTableComposite composite) {
		this.composite = composite;
		composite.table.hideColumns(columnsToSkip);
	}

	public void populateTable(List<PartSearchRecord> records) {
		populateTable(records, false);
	}

	public void populateTable(List<PartSearchRecord> records, boolean autoSelect) {
		composite.table.removeUncheckedItems();
		if (records == null) {
			return;
		}

		Map<Long, Integer> currentSelectedIds = Maps.newHashMap();
		for (int i = 0; i < composite.table.getItemCount(); i++) {
			PartSearchRecord currentRecord = (PartSearchRecord) composite.table.getItem(i).getData(DATA_RECORD);
			currentSelectedIds.put(currentRecord.getPartId(), i);
		}

		for (PartSearchRecord record : records) {
			int index;
			if (currentSelectedIds.containsKey(record.getPartId())) {
				logger.debug("Part is already currently selected updating it.");
				index = currentSelectedIds.get(record.getPartId());
			} else {
				logger.debug("Adding new part to table.");
				index = composite.table.getItemCount();
				TableItem item = new TableItem(composite.table, SWT.NONE);
				item.setChecked(autoSelect);
			}
			updateRow(index, record);
		}
		packTable();
	}

	public void addRow(PartSearchRecord record) {
		int index = composite.table.getItemCount();
		new TableItem(composite.table, SWT.NONE);
		updateRow(index, record);
		packTable();
	}

	public void clearTable() {
		composite.table.removeAll();
	}

	public List<PartSearchRecord> getPartSearchRecords() {
		return composite.table.getDataAll(DATA_RECORD);
	}

	public Long getSelectedPartId() {
		if (composite.table.getSelectionCount() == 0) {
			return null;
		}
		TableItem[] selectedRows = composite.table.getSelection();
		TableItem row = selectedRows[0];
		return ((PartSearchRecord) row.getData(DATA_RECORD)).getPartId();
	}

	private void updateRow(int index, PartSearchRecord record) {
		TableItem item = composite.table.getItem(index);
		item.setText(PartSearchTableComposite.COL_AMT_SOLD, String.valueOf(record.getAmountSold()));
		item.setText(PartSearchTableComposite.COL_BASE_PRICE, AppPrefs.FORMATTER.formatMoney(record.getBasePrice()));
		item.setText(PartSearchTableComposite.COL_DESCRIPTION, record.getPartDescription());
		item.setText(PartSearchTableComposite.COL_INVNTRY_STATUS, record.getInventoryStatus());
		item.setText(PartSearchTableComposite.COL_INVNTRY_CNT, String.valueOf(record.getInventoryCount()));
		item.setText(PartSearchTableComposite.COL_MANUF, record.getPartManufacturer());
		item.setText(PartSearchTableComposite.COL_MANUF_ID, record.getManufacturerId());
		item.setText(PartSearchTableComposite.COL_PART_NAME, record.getPartName());
		item.setText(PartSearchTableComposite.COL_CURRENT_PRICE,
				AppPrefs.FORMATTER.formatMoney(record.getCurrentPrice()));

		if (record.isOnSale()) {
			item.setForeground(PartSearchTableComposite.COL_CURRENT_PRICE, AppPrefs.COLOR_DISCOUNT_PRICE);
		}

		item.setData(DATA_RECORD, record);
		item.setData(DATA_CURRENT_PRICE, record.getCurrentPrice());
		item.setData(DATE_BASE_PRICE, record.getBasePrice());
		item.setData(DATA_AMOUNT_SOLD, record.getAmountSold());
	}

	private void packTable() {
		List<Integer> skips = Lists.newArrayList(PartSearchTableComposite.COL_DESCRIPTION);
		skips.addAll(columnsToSkip);

		composite.table.packTableColumns(skips);
		if (!columnsToSkip.contains(PartSearchTableComposite.COL_DESCRIPTION)) {
			composite.tblclmnDescription.setWidth(200);
		}
	}
}