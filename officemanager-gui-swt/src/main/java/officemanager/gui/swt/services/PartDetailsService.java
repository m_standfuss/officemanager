package officemanager.gui.swt.services;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import officemanager.biz.delegates.PartDelegate;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PartRecord;
import officemanager.gui.swt.ui.composites.PartEditComposite;

public class PartDetailsService {
	private final PartDelegate partDelegate;

	private PartEditComposite composite;

	public PartDetailsService() {
		partDelegate = new PartDelegate();
	}

	public void setComposite(PartEditComposite composite) {
		checkArgument(composite != null, "composite cannot be null");
		this.composite = composite;
		populateComposite();
	}

	public void populateRecord(PartRecord record) {
		record.setCategoryType(composite.cmbCategory.getCodeValueRecord());
		record.setManufacturer(composite.cmbManufacturer.getCodeValueRecord());
		record.setProductType(composite.cmbProductType.getCodeValueRecord());
		record.setBarcode(composite.txtBarcode.getText());
		record.setBasePrice(composite.txtBasePrice.getValue());
		record.setDescription(composite.txtDescription.getText());
		record.setInventoryCount(composite.txtInventoryCount.getNumberInt());
		record.setInventoryThreshold(composite.txtInventoryThreshold.getNumberInt());
		record.setManufPartId(composite.txtManufacturerID.getText());
		record.setPartName(composite.txtPartName.getText());
	}

	public void fillComposite(PartRecord record) {
		composite.cmbCategory.setSelectedValue(record.getCategoryType());
		composite.cmbManufacturer.setSelectedValue(record.getManufacturer());
		composite.cmbProductType.setSelectedValue(record.getProductType());
		composite.txtBarcode.setText(record.getBarcode());
		composite.txtBasePrice.setValue(record.getBasePrice());
		composite.txtDescription.setText(record.getDescription());
		composite.txtInventoryCount.setNumber(record.getInventoryCount());
		composite.txtInventoryThreshold.setNumber(record.getInventoryThreshold());
		composite.txtManufacturerID.setText(record.getManufPartId());
		composite.txtPartName.setText(record.getPartName());
	}

	private void populateComposite() {
		List<CodeValueRecord> partCategories = partDelegate.getPartCategories();
		composite.cmbCategory.populateSelections(partCategories);
		List<CodeValueRecord> partManufacturers = partDelegate.getPartManufacturers();
		composite.cmbManufacturer.populateSelections(partManufacturers);
		List<CodeValueRecord> partProductTypes = partDelegate.getProductTypes();
		composite.cmbProductType.populateSelections(partProductTypes);
	}
}