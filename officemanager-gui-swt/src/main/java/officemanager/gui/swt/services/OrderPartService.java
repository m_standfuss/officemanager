package officemanager.gui.swt.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.biz.records.OrderPartRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderPartTableComposite;

public class OrderPartService {

	private static final Logger logger = LogManager.getLogger(OrderPartService.class);

	public static final String DATA_ORDER_PART_RECORD = "DATA_ORDER_PART_RECORD";

	private final List<Integer> columnsToSkip;
	private OrderPartTableComposite composite;

	public OrderPartService() {
		columnsToSkip = Lists.newArrayList();
	}

	public OrderPartService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(OrderPartTableComposite.COL_PART_NAME)) {
			columnsToSkip.add(OrderPartTableComposite.COL_PART_NAME);
		}
		if (!columnsToKeep.contains(OrderPartTableComposite.COL_PART_MANUF)) {
			columnsToSkip.add(OrderPartTableComposite.COL_PART_MANUF);
		}
		if (!columnsToKeep.contains(OrderPartTableComposite.COL_CLIENT_PRODUCT)) {
			columnsToSkip.add(OrderPartTableComposite.COL_CLIENT_PRODUCT);
		}
		if (!columnsToKeep.contains(OrderPartTableComposite.COL_QUANTITY)) {
			columnsToSkip.add(OrderPartTableComposite.COL_QUANTITY);
		}
		if (!columnsToKeep.contains(OrderPartTableComposite.COL_PRICE)) {
			columnsToSkip.add(OrderPartTableComposite.COL_PRICE);
		}
		if (!columnsToKeep.contains(OrderPartTableComposite.COL_TAX)) {
			columnsToSkip.add(OrderPartTableComposite.COL_TAX);
		}
		if (!columnsToKeep.contains(OrderPartTableComposite.COL_TOTAL)) {
			columnsToSkip.add(OrderPartTableComposite.COL_TOTAL);
		}
	}

	public OrderPartRecord getSelectedPart() {
		return composite.table.getSelectedData(DATA_ORDER_PART_RECORD);
	}

	public List<OrderPartRecord> getSelectedParts() {
		return composite.table.getSelectedDataAll(DATA_ORDER_PART_RECORD);
	}

	public List<OrderPartRecord> getCheckedParts() {
		List<TableItem> checkedItems = composite.table.getCheckedItems();
		List<OrderPartRecord> records = Lists.newArrayList();
		for (TableItem item : checkedItems) {
			records.add((OrderPartRecord) item.getData(DATA_ORDER_PART_RECORD));
		}
		return records;
	}

	public void setComposite(OrderPartTableComposite composite) {
		this.composite = composite;
	}

	public void populateTable(List<OrderPartRecord> records) {
		logger.debug("Starting populateTable.");
		composite.table.removeAll();
		for (OrderPartRecord record : records) {
			_addRecordToTable(record);
		}
		composite.table.packTableColumns(columnsToSkip);
		composite.layout();
	}

	private TableItem _addRecordToTable(OrderPartRecord record) {
		TableItem item = new TableItem(composite.table, SWT.NONE);
		item.setText(OrderPartTableComposite.COL_CLIENT_PRODUCT,
				AppPrefs.FORMATTER.formatClientProductDisplay(record.getClientProductManufacturer(),
						record.getClientProductModel(), record.getClientProductSerialNumber()));
		item.setText(OrderPartTableComposite.COL_PART_MANUF, record.getManufacturerDisplay());
		item.setText(OrderPartTableComposite.COL_PART_NAME, record.getPartName());

		if (record.isRefunded()) {
			item.setText(OrderPartTableComposite.COL_TOTAL, "REFUNDED");
		} else {
			item.setText(OrderPartTableComposite.COL_TOTAL,
					AppPrefs.FORMATTER.formatMoney(Calculator.totalPart(record)));
		}
		item.setText(OrderPartTableComposite.COL_PRICE, AppPrefs.FORMATTER.formatMoney(record.getPartPrice()));
		item.setText(OrderPartTableComposite.COL_QUANTITY, String.valueOf(record.getQuantity()));

		BigDecimal taxAmount = record.isTaxExempt() ? Calculator.getZero()
				: Calculator.calculatePrice(record.getTaxAmount(), record.getQuantity());
		item.setText(OrderPartTableComposite.COL_TAX, AppPrefs.FORMATTER.formatMoney(taxAmount));

		item.setData(DATA_ORDER_PART_RECORD, record);
		return item;
	}

}
