package officemanager.gui.swt.services;

import java.math.BigDecimal;
import java.util.List;

import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.OrderPaymentsRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PaymentRecord.PaymentType;
import officemanager.gui.swt.ui.wizards.pages.PaymentsPage;

public class OrderPaymentService extends PaymentPageService {

	private final PaymentDelegate paymentDelegate;
	private final OrderDelegate orderDelegate;

	private boolean repopulateNeeded;
	private Long orderId;
	private Long clientId;
	private BigDecimal amountOwed;

	public OrderPaymentService() {
		paymentDelegate = new PaymentDelegate();
		orderDelegate = new OrderDelegate();

		orderId = null;
		clientId = null;
		amountOwed = BigDecimal.ZERO;
		setAllowNegativeBalance(false);
	}

	@Override
	public void setPage(PaymentsPage page) {
		super.setPage(page);
		page.addFirstShowListener(e -> populatePage());
		page.addIsShownListener(e -> {
			if (repopulateNeeded) {
				BusyIndicator.showWhile(Display.getDefault(), () -> {
					populatePage();
				});
			}
		});
	}

	public void setOrderId(Long orderId) {
		if (this.orderId == orderId) {
			return;
		}
		if (this.orderId != null && this.orderId.equals(orderId)) {
			return;
		}
		this.orderId = orderId;
		repopulateNeeded = true;
	}

	@Override
	public void setClientId(Long clientId) {
		super.setClientId(clientId);
		if (this.clientId == clientId) {
			return;
		}
		if (this.clientId != null && this.clientId.equals(clientId)) {
			return;
		}
		this.clientId = clientId;
		repopulateNeeded = true;
	}

	@Override
	public void setAmountOwed(BigDecimal amountOwed) {
		super.setAmountOwed(amountOwed);
		if (this.amountOwed == amountOwed) {
			return;
		}
		if (Calculator.amountsMatch(amountOwed, this.amountOwed)) {
			return;
		}
		this.amountOwed = amountOwed;
		repopulateNeeded = true;
	}

	public BigDecimal makeChange() {
		List<PaymentRecord> records = getPayments();
		BigDecimal paymentTotal = Calculator.calculatePaymentTotal(records);
		BigDecimal balance = Calculator.calculateBalance(amountOwed, paymentTotal);

		if (balance.compareTo(BigDecimal.ZERO) >= 0) {
			return BigDecimal.ZERO;
		}

		BigDecimal totalChange = balance.multiply(new BigDecimal("-1"));
		BigDecimal changeToBeMade = totalChange;
		for (PaymentRecord record : getPaymentsAdded()) {
			if (changeToBeMade.compareTo(BigDecimal.ZERO) == 0) {
				break;
			}
			if (record.getPaymentType() == PaymentType.CASH) {
				if (record.getAmount().compareTo(changeToBeMade) <= 0) {
					removePayment(record);
					changeToBeMade = changeToBeMade.subtract(record.getAmount());
				} else {
					record.setAmount(record.getAmount().subtract(changeToBeMade));
					updatePayment(record);
					changeToBeMade = BigDecimal.ZERO;
				}
			}
		}
		refreshTotals();
		return totalChange;
	}

	private void populatePage() {
		if (clientId == null) {
			if (orderId != null) {
				clientId = orderDelegate.getClientId(orderId);
			}
		}

		boolean hasClientCredit = false;
		if (clientId != null) {
			hasClientCredit = paymentDelegate.hasClientCreditOption(clientId);
			if (hasClientCredit) {
				setAccountId(paymentDelegate.getAccountId(clientId));
			}
		}
		populatePage(hasClientCredit, true);

		if (orderId != null) {
			OrderPaymentsRecord record = paymentDelegate.getOrderPaymentsRecord(orderId);
			setAmountOwed(record.getOrderTotal());
			populatePayments(record.getPayments());
			refreshTotals();
		}
		repopulateNeeded = false;
	}
}
