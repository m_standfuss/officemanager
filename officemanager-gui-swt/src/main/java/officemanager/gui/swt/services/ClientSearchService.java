package officemanager.gui.swt.services;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.records.ClientSearchRecord;
import officemanager.biz.searchingcriteria.PersonSearchCriteria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.ClientSearchComposite;
import officemanager.gui.swt.ui.tables.ClientTableComposite;
import officemanager.gui.swt.ui.wizards.ClientWizard;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;

public class ClientSearchService {

	private static final Logger logger = LogManager.getLogger(ClientSearchService.class);

	public static final String DATA_RECORD = "DATA_RECORD";
	public static final String DATA_JOINED_DTTM = "DATA_JOINED_DTTM";

	private final List<Integer> columnsToSkip;
	private final ClientDelegate clientDelegate;
	private ClientSearchComposite composite;

	public ClientSearchService() {
		columnsToSkip = Lists.newArrayList();
		clientDelegate = new ClientDelegate();
	}

	public ClientSearchService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(ClientTableComposite.COL_NAME)) {
			columnsToSkip.add(ClientTableComposite.COL_NAME);
		}
		if (!columnsToKeep.contains(ClientTableComposite.COL_PHONE)) {
			columnsToSkip.add(ClientTableComposite.COL_PHONE);
		}
		if (!columnsToKeep.contains(ClientTableComposite.COL_EMAIL)) {
			columnsToSkip.add(ClientTableComposite.COL_EMAIL);
		}
		if (!columnsToKeep.contains(ClientTableComposite.COL_JOINED_DTTM)) {
			columnsToSkip.add(ClientTableComposite.COL_JOINED_DTTM);
		}
	}

	public ClientSearchComposite getComposite() {
		return composite;
	}

	public void setComposite(ClientSearchComposite composite) {
		this.composite = composite;
		composite.btnClear.addListener(SWT.Selection, e -> clear());
		composite.btnSearch.addListener(SWT.Selection, e -> search());

		TraverseListener searchKeyListener = e -> {
			if (e.keyCode == SWT.CR || e.keyCode == 16777296) {
				search();
				e.doit = false;
			}
		};

		composite.txtNameFirst.addTraverseListener(searchKeyListener);
		composite.txtNameLast.addTraverseListener(searchKeyListener);
		composite.txtPhone.addTraverseListener(searchKeyListener);
		composite.searchResultsTable.table.hideColumns(columnsToSkip);
		composite.tlItmNewClient.addListener(SWT.Selection, e -> openNewClientWizard());
	}

	public void search() {
		logger.debug("Search button selected.");

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.searchResultsTable.table.removeUncheckedItems();
			List<ClientSearchRecord> searchResults = getSearchResults();
			populateTable(searchResults);
		});
	}

	private void clear() {
		composite.txtNameFirst.setText("");
		composite.txtNameLast.setText("");
		composite.txtPhone.setText("");
	}

	private List<ClientSearchRecord> getSearchResults() {
		PersonSearchCriteria searchCriteria = new PersonSearchCriteria();
		searchCriteria.nameFirst = composite.txtNameFirst.getText();
		searchCriteria.nameLast = composite.txtNameLast.getText();
		searchCriteria.primaryPhone = composite.txtPhone.getUnformattedText();
		return clientDelegate.searchForClientSelections(searchCriteria);
	}

	private void populateTable(List<ClientSearchRecord> records) {
		if (records == null) {
			return;
		}
		List<ClientSearchRecord> currentSelectedRecords = composite.searchResultsTable.table
				.getCheckedData(DATA_RECORD);
		Set<Long> currentSelectedIds = Sets.newHashSet();
		for (ClientSearchRecord record : currentSelectedRecords) {
			currentSelectedIds.add(record.getClientId());
		}

		for (ClientSearchRecord record : records) {
			if (currentSelectedIds.contains(record.getClientId())) {
				logger.debug("Flat Rate is already currently selected not readding it.");
				continue;
			}
			TableItem item = new TableItem(composite.searchResultsTable.table, SWT.NONE);
			item.setText(ClientTableComposite.COL_EMAIL, record.getEmail());
			item.setText(ClientTableComposite.COL_JOINED_DTTM,
					AppPrefs.dateFormatter.formatDate_DB(record.getJoinedOnDttm()));
			item.setText(ClientTableComposite.COL_NAME, record.getNameFullFormatted());
			item.setText(ClientTableComposite.COL_PHONE,
					AppPrefs.FORMATTER.formatPhoneNumber(record.getPrimaryPhone()));

			item.setData(DATA_RECORD, record);
			item.setData(DATA_JOINED_DTTM, record.getJoinedOnDttm());
		}
		composite.searchResultsTable.table.packTableColumns(columnsToSkip);
	}

	private void openNewClientWizard() {
		ClientWizard wizard = new ClientWizard();
		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		int returnCode = dialog.open();
		if (returnCode == Window.OK) {
			search();
		}
	}
}
