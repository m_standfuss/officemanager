package officemanager.gui.swt.services;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.SaleRecord;
import officemanager.biz.records.SaleRecord.SaleType;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.SaleTableComposite;

public class SaleService {

	public static final String DATA_SALE_RECORD = "DATA_SALE_RECORD";

	private final List<Integer> columnsToSkip;

	private SaleTableComposite saleTableComposite;

	public SaleService() {
		this(Lists.newArrayList());
	}

	public SaleService(List<Integer> columnsToSkip) {
		this.columnsToSkip = columnsToSkip;
	}

	public SaleTableComposite getSaleTableComposite() {
		return saleTableComposite;
	}

	public void setSaleTableComposite(SaleTableComposite saleTableComposite) {
		this.saleTableComposite = saleTableComposite;
		saleTableComposite.table.hideColumns(columnsToSkip);
	}

	public void populateTable(List<SaleRecord> sales) {
		saleTableComposite.table.removeAll();
		for (SaleRecord sale : sales) {
			TableItem item = new TableItem(saleTableComposite.table, SWT.NONE);
			String amountOff = "";
			if (sale.getSaleType() == SaleType.FLAT_RATE_OFF) {
				amountOff = AppPrefs.FORMATTER.formatMoney(sale.getAmountOff());
			} else if (sale.getSaleType() == SaleType.PERCENTAGE_OFF) {
				amountOff = AppPrefs.FORMATTER.formatPercentage(sale.getAmountOff());
			}
			item.setText(SaleTableComposite.COL_SALE_NAME, sale.getSaleName());
			item.setText(SaleTableComposite.COL_AMOUNT_OFF, amountOff);
			item.setText(SaleTableComposite.COL_BEGIN_DTTM, AppPrefs.dateFormatter.formatFullDate_DB(sale.getStartDttm()));
			item.setText(SaleTableComposite.COL_END_DTTM, AppPrefs.dateFormatter.formatFullDate_DB(sale.getEndDttm()));
			item.setText(SaleTableComposite.COL_SALE_TYPE, sale.getSaleTypeDisplay());

			item.setData(DATA_SALE_RECORD, sale);
		}
		saleTableComposite.table.packTableColumns(columnsToSkip);
	}

}
