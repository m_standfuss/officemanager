package officemanager.gui.swt.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.PreferencesDelegate;
import officemanager.biz.records.ApplicationPreferencesRecord;
import officemanager.biz.records.PreferenceRecord;
import officemanager.gui.swt.AppPrefs;

public class AppPrefLoader {

	private static final Logger logger = LogManager.getLogger(AppPrefLoader.class);

	private final PreferencesDelegate preferenceDelegate;

	public AppPrefLoader() {
		preferenceDelegate = new PreferencesDelegate();
	}

	public void loadApplicationPreferences() {
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			ApplicationPreferencesRecord record = preferenceDelegate.getApplicationPreferences();

			AppPrefs.claimTicketCommentTypes.clear();
			for (PreferenceRecord pref : record.getClaimTicketCommentTypes()) {
				AppPrefs.claimTicketCommentTypes.add(pref.getPrefValueNumber());
			}

			AppPrefs.orderRecieptCommentTypes.clear();
			for (PreferenceRecord pref : record.getOrderRecieptCommentTypes()) {
				AppPrefs.orderRecieptCommentTypes.add(pref.getPrefValueNumber());
			}

			AppPrefs.shopTicketCommentTypes.clear();
			for (PreferenceRecord pref : record.getShopTicketCommentTypes()) {
				AppPrefs.shopTicketCommentTypes.add(pref.getPrefValueNumber());
			}

			if (record.getCompanyAddress() != null) {
				AppPrefs.companyAddress = record.getCompanyAddress().getPrefValueText();
			}

			if (record.getCompanyPhone() != null) {
				AppPrefs.companyPhone = record.getCompanyPhone().getPrefValueText();
			}

			if (record.getCompanyWebsite() != null) {
				AppPrefs.companyWebsite = record.getCompanyWebsite().getPrefValueText();
			}

			if (record.getHourlyRate() != null) {
				AppPrefs.hourlyRate = record.getHourlyRate().getPrefValueDecimal();
			} else {
				logger.warn(
						"No hourly rate has been set! In order to calculate order totals an hourly rate must be set.");
			}

			if (record.getHourlyRateDiscounted() != null) {
				AppPrefs.hourlyRateDiscounted = record.getHourlyRateDiscounted().getPrefValueDecimal();
			} else {
				logger.warn(
						"No hourly discount rate has been set! In order to calculate order totals for discount members this must be set.");
			}

			if (record.getTaxRate() != null) {
				Calculator.setTaxRate(record.getTaxRate().getPrefValueDecimal());
			} else {
				logger.warn("No tax rate has been set! In order to calculate part totals this must be set.");
			}
		});
	}
}
