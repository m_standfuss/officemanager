package officemanager.gui.swt.services;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.delegates.FlatRateServiceDelegate;
import officemanager.biz.records.FlatRateSearchRecord;
import officemanager.biz.searchingcriteria.FlatRateSearchCriteria;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.FlatRateSearchComposite;
import officemanager.gui.swt.ui.tables.FlatRateTableComposite;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;

public class FlatRateSearchService {
	private static final Logger logger = LogManager.getLogger(FlatRateSearchService.class);

	public static final String DATA_RECORD = "DATA_RECORD";
	public static final String DATE_BASE_PRICE = "DATE_BASE_PRICE";
	public static final String DATA_CURRENT_PRICE = "DATA_CURRENT_PRICE";
	public static final String DATA_AMOUNT_SOLD = "DATA_AMOUNT_SOLD";

	private final FlatRateServiceDelegate flatRateServiceDelegate;
	private final List<Integer> columnsToSkip;
	private FlatRateSearchComposite composite;

	public FlatRateSearchService() {
		flatRateServiceDelegate = new FlatRateServiceDelegate();
		columnsToSkip = Lists.newArrayList();
	}

	public FlatRateSearchService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(FlatRateTableComposite.COL_SERVICE_NAME)) {
			columnsToSkip.add(FlatRateTableComposite.COL_SERVICE_NAME);
		}
		if (!columnsToKeep.contains(FlatRateTableComposite.COL_PRODUCT_TYPE)) {
			columnsToSkip.add(FlatRateTableComposite.COL_PRODUCT_TYPE);
		}
		if (!columnsToKeep.contains(FlatRateTableComposite.COL_CATEGORY)) {
			columnsToSkip.add(FlatRateTableComposite.COL_CATEGORY);
		}
		if (!columnsToKeep.contains(FlatRateTableComposite.COL_CURRENT_PRICE)) {
			columnsToSkip.add(FlatRateTableComposite.COL_CURRENT_PRICE);
		}
		if (!columnsToKeep.contains(FlatRateTableComposite.COL_BASE_PRICE)) {
			columnsToSkip.add(FlatRateTableComposite.COL_BASE_PRICE);
		}
		if (!columnsToKeep.contains(FlatRateTableComposite.COL_AMT_SOLD)) {
			columnsToSkip.add(FlatRateTableComposite.COL_AMT_SOLD);
		}
	}

	public void addColumnsToSkip(Integer... columnIndexes) {
		for (Integer column : columnIndexes) {
			columnsToSkip.add(column);
		}
	}

	public FlatRateSearchComposite getComposite() {
		return composite;
	}

	public void setComposite(FlatRateSearchComposite composite) {
		this.composite = composite;

		composite.btnSearch.addListener(SWT.Selection, e -> search());
		composite.btnClear.addListener(SWT.Selection, e -> clear());
		composite.scrCompositeSideBar.setContent(composite.compositeSideBar);
		composite.scrCompositeSideBar.setMinSize(composite.compositeSideBar.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		TraverseListener searchKeyListener = e -> {
			if (e.keyCode == SWT.CR || e.keyCode == 16777296) {
				search();
				e.doit = false;
			}
		};

		composite.txtServiceName.addTraverseListener(searchKeyListener);

		composite.searchResultsTable.table.hideColumns(columnsToSkip);

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.tblCategory.populateList(flatRateServiceDelegate.getFlatRateServiceCategories());
			composite.tblProductType.populateList(flatRateServiceDelegate.getProductTypes());
		});
	}

	private void search() {
		logger.debug("Search button selected.");

		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.searchResultsTable.table.removeUncheckedItems();
			List<FlatRateSearchRecord> searchResults = getSearchResults();
			populateTable(searchResults);
		});
	}

	private void clear() {
		composite.txtServiceName.setText("");
		composite.tblProductType.checkAllItems(false);
		composite.tblCategory.checkAllItems(false);
		composite.btnSaleItems.setSelection(false);
	}

	private List<FlatRateSearchRecord> getSearchResults() {
		FlatRateSearchCriteria searchCriteria = new FlatRateSearchCriteria();
		searchCriteria.onlyOnSale = composite.btnSaleItems.getSelection();
		searchCriteria.productTypeCds = composite.tblProductType.getCheckedCodeValues();
		searchCriteria.serviceCategoryCds = composite.tblCategory.getCheckedCodeValues();
		searchCriteria.serviceName = composite.txtServiceName.getText();
		return flatRateServiceDelegate.searchFlatRateServices(searchCriteria);
	}

	private void populateTable(List<FlatRateSearchRecord> records) {
		if (records == null) {
			return;
		}
		List<FlatRateSearchRecord> currentSelectedRecords = composite.searchResultsTable.table
				.getCheckedData(DATA_RECORD);
		Set<Long> currentSelectedIds = Sets.newHashSet();
		for (FlatRateSearchRecord record : currentSelectedRecords) {
			currentSelectedIds.add(record.getFlatRateServiceId());
		}

		for (FlatRateSearchRecord record : records) {
			if (currentSelectedIds.contains(record.getFlatRateServiceId())) {
				logger.debug("Flat Rate is already currently selected not readding it.");
				continue;
			}
			TableItem item = new TableItem(composite.searchResultsTable.table, SWT.NONE);
			item.setText(FlatRateTableComposite.COL_AMT_SOLD, String.valueOf(record.getAmountSold()));
			item.setText(FlatRateTableComposite.COL_BASE_PRICE, AppPrefs.FORMATTER.formatMoney(record.getBasePrice()));
			item.setText(FlatRateTableComposite.COL_CATEGORY, record.getCategory());
			item.setText(FlatRateTableComposite.COL_CURRENT_PRICE,
					AppPrefs.FORMATTER.formatMoney(record.getCurrentPrice()));
			item.setText(FlatRateTableComposite.COL_PRODUCT_TYPE, record.getProductType());
			item.setText(FlatRateTableComposite.COL_SERVICE_NAME, record.getServiceName());

			if (record.isOnSale()) {
				item.setForeground(PartSearchTableComposite.COL_CURRENT_PRICE, AppPrefs.COLOR_DISCOUNT_PRICE);
			}

			item.setData(DATA_RECORD, record);
			item.setData(DATA_CURRENT_PRICE, record.getCurrentPrice());
			item.setData(DATE_BASE_PRICE, record.getBasePrice());
			item.setData(DATA_AMOUNT_SOLD, record.getAmountSold());
		}
		composite.searchResultsTable.table.packTableColumns(columnsToSkip);
	}
}
