package officemanager.gui.swt.services;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.delegates.AccountDelegate;
import officemanager.biz.records.AccountSearchRecord;
import officemanager.biz.searchingcriteria.AccountSearchCriteria;
import officemanager.biz.searchingcriteria.PersonSearchCriteria;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.AccountSearchComposite;
import officemanager.gui.swt.ui.tables.AccountTableComposite;
import officemanager.gui.swt.ui.wizards.AddAccountWizard;
import officemanager.gui.swt.ui.wizards.OfficeManagerWizardDialog;

public class AccountSearchService {
	private static final Logger logger = LogManager.getLogger(AccountSearchService.class);

	public static final String DATA_RECORD = "DATA_RECORD";

	private final List<Integer> columnsToSkip;
	private final AccountDelegate accountDelegate;

	private AccountSearchComposite composite;

	public AccountSearchService() {
		columnsToSkip = Lists.newArrayList();
		accountDelegate = new AccountDelegate();
	}

	public AccountSearchService(List<Integer> columnsToKeep) {
		this();
		if (!columnsToKeep.contains(AccountTableComposite.COL_ACCNT_NUMBER)) {
			columnsToSkip.add(AccountTableComposite.COL_ACCNT_NUMBER);
		}
		if (!columnsToKeep.contains(AccountTableComposite.COL_NAME)) {
			columnsToSkip.add(AccountTableComposite.COL_NAME);
		}
		if (!columnsToKeep.contains(AccountTableComposite.COL_PHONE)) {
			columnsToSkip.add(AccountTableComposite.COL_PHONE);
		}
		if (!columnsToKeep.contains(AccountTableComposite.COL_EMAIL)) {
			columnsToSkip.add(AccountTableComposite.COL_EMAIL);
		}
		if (!columnsToKeep.contains(AccountTableComposite.COL_STATUS)) {
			columnsToSkip.add(AccountTableComposite.COL_STATUS);
		}
	}

	public AccountSearchComposite getComposite() {
		return composite;
	}

	public void setComposite(AccountSearchComposite composite) {
		this.composite = composite;
		composite.tlItmAddAccount.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_CLIENT_ACCOUNT));

		composite.btnClear.addListener(SWT.Selection, e -> clear());
		composite.btnSearch.addListener(SWT.Selection, e -> search());
		composite.tlItmAddAccount.addListener(SWT.Selection, e -> addAccount());

		TraverseListener searchKeyListener = e -> {
			if (e.keyCode == SWT.CR || e.keyCode == 16777296) {
				search();
				e.doit = false;
			}
		};

		composite.txtAccountNumber.addTraverseListener(searchKeyListener);
		composite.txtNameFirst.addTraverseListener(searchKeyListener);
		composite.txtNameLast.addTraverseListener(searchKeyListener);
		composite.txtPhone.addTraverseListener(searchKeyListener);
		composite.searchResultsTable.table.hideColumns(columnsToSkip);
	}

	public void search() {
		logger.debug("Search button selected.");
		BusyIndicator.showWhile(Display.getDefault(), () -> {
			composite.searchResultsTable.table.removeUncheckedItems();
			List<AccountSearchRecord> searchResults = getSearchResults();
			populateTable(searchResults);
		});
	}

	public AccountSearchRecord getSelectedRow() {
		return composite.searchResultsTable.table.getSelectedData(DATA_RECORD);
	}

	private void clear() {
		composite.txtAccountNumber.setText("");
		composite.txtNameFirst.setText("");
		composite.txtNameLast.setText("");
		composite.txtPhone.setText("");
	}

	private void addAccount() {
		AddAccountWizard wizard = new AddAccountWizard();

		OfficeManagerWizardDialog dialog = new OfficeManagerWizardDialog(composite.getShell(), wizard);
		dialog.setBlockOnOpen(true);
		dialog.open();
	}

	private List<AccountSearchRecord> getSearchResults() {
		PersonSearchCriteria personCriteria = new PersonSearchCriteria();
		personCriteria.nameFirst = composite.txtNameFirst.getText();
		personCriteria.nameLast = composite.txtNameLast.getText();
		personCriteria.primaryPhone = composite.txtPhone.getUnformattedText();

		AccountSearchCriteria criteria = new AccountSearchCriteria();
		criteria.personSearchCriteria = personCriteria;
		criteria.accountId = AppPrefs.FORMATTER.unformatAccountNumber(composite.txtAccountNumber.getText());
		return accountDelegate.searchAccounts(criteria);
	}

	private void populateTable(List<AccountSearchRecord> records) {
		if (records == null) {
			return;
		}
		List<AccountSearchRecord> currentSelectedRecords = composite.searchResultsTable.table
				.getCheckedData(DATA_RECORD);
		Set<Long> currentSelectedIds = Sets.newHashSet();
		for (AccountSearchRecord record : currentSelectedRecords) {
			currentSelectedIds.add(record.getAccountId());
		}

		for (AccountSearchRecord record : records) {
			if (currentSelectedIds.contains(record.getAccountId())) {
				logger.debug("Account is already currently selected not readding it.");
				continue;
			}
			TableItem item = new TableItem(composite.searchResultsTable.table, SWT.NONE);
			item.setText(AccountTableComposite.COL_ACCNT_NUMBER,
					AppPrefs.FORMATTER.formatAccountNumber(record.getAccountId()));
			item.setText(AccountTableComposite.COL_EMAIL, record.getEmail());
			item.setText(AccountTableComposite.COL_NAME, record.getNameFullFormatted());
			item.setText(AccountTableComposite.COL_PHONE,
					AppPrefs.FORMATTER.formatPhoneNumber(record.getPrimaryPhone()));
			item.setText(AccountTableComposite.COL_STATUS, record.getStatus().getDisplay());

			item.setData(DATA_RECORD, record);
		}
		composite.searchResultsTable.table.packTableColumns(columnsToSkip);
	}
}
