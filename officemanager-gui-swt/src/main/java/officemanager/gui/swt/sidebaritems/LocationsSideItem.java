package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.LocationTreeView;

public class LocationsSideItem extends SideBarView {

	private final LocationTreeView view;

	public LocationsSideItem() {
		view = new LocationTreeView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.CRATE, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Locations";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
