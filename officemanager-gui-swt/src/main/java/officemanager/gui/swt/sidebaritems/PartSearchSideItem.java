package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.PartSearchView;

public class PartSearchSideItem extends SideBarView {

	private final PartSearchView view;

	public PartSearchSideItem() {
		view = new PartSearchView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.PART, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Parts";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
