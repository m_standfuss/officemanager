package officemanager.gui.swt.sidebaritems;

import officemanager.gui.swt.views.IOfficeManagerView;

public abstract class SideBarView implements ISideBarItem {

	public abstract IOfficeManagerView getView();
}
