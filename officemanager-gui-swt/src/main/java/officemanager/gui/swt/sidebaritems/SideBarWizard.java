package officemanager.gui.swt.sidebaritems;

import officemanager.gui.swt.ui.wizards.OfficeManagerWizard;

public interface SideBarWizard extends ISideBarItem {

	public OfficeManagerWizard getWizard();
}
