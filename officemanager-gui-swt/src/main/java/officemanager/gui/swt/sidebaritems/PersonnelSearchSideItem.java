package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.PersonnelSearchView;

public class PersonnelSearchSideItem extends SideBarView {

	private final PersonnelSearchView view;

	public PersonnelSearchSideItem() {
		view = new PersonnelSearchView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.USER_SEARCH, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Personnel";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
