package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.AccountSearchView;
import officemanager.gui.swt.views.IOfficeManagerView;

public class AccountSearchSideItem extends SideBarView {

	private final AccountSearchView view;

	public AccountSearchSideItem() {
		view = new AccountSearchView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.USER_SEARCH, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Accounts";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
