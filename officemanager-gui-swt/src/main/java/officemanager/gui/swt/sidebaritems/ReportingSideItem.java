package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.ReportsView;

public class ReportingSideItem extends SideBarView {

	private final ReportsView view;

	public ReportingSideItem() {
		view = new ReportsView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.REPORTS, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Reporting";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
