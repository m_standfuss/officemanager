package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.SaleSearchView;

public class SaleSearchSideItem extends SideBarView {

	private final SaleSearchView view;

	public SaleSearchSideItem() {
		view = new SaleSearchView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.SALE, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Sales";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
