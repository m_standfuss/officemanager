package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.ClosedOrderSearchView;
import officemanager.gui.swt.views.IOfficeManagerView;

public class ClosedOrdersSideItem extends SideBarView {

	private final ClosedOrderSearchView view;

	public ClosedOrdersSideItem() {
		view = new ClosedOrderSearchView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.ORDER_SEARCH, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Closed Orders";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}

}
