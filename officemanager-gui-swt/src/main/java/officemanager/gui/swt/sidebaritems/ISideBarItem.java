package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

public interface ISideBarItem {

	public Image getSideBarIcon();

	public String getSideBarName();
}
