package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.OpenOrderSearchView;

public class OpenOrdersSideItem extends SideBarView {

	private final OpenOrderSearchView view;

	public OpenOrdersSideItem() {
		view = new OpenOrderSearchView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.ORDER_SEARCH, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Open Orders";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
