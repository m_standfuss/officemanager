package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.PersonnelHomeView;

public class PersonnelHomeSideItem extends SideBarView {

	private final PersonnelHomeView view;

	public PersonnelHomeSideItem() {
		view = new PersonnelHomeView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.HOME, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Home";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}

}
