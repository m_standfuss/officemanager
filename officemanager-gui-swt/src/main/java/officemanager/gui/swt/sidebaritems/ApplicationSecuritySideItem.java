package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.ApplicationSecurityView;
import officemanager.gui.swt.views.IOfficeManagerView;

public class ApplicationSecuritySideItem extends SideBarView {

	private final ApplicationSecurityView view;

	public ApplicationSecuritySideItem() {
		view = new ApplicationSecurityView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.SECURITY, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Application Security";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
