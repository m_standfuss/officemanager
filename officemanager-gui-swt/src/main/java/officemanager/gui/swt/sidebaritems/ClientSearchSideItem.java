package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.ClientSearchView;
import officemanager.gui.swt.views.IOfficeManagerView;

public class ClientSearchSideItem extends SideBarView {

	private final ClientSearchView view;

	public ClientSearchSideItem() {
		view = new ClientSearchView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.USER_SEARCH, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Clients";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
