package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.IOfficeManagerView;
import officemanager.gui.swt.views.PreferencesView;

public class PreferencesSideItem extends SideBarView {

	private final PreferencesView view;

	public PreferencesSideItem() {
		view = new PreferencesView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.APP_PREFS, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Application Preferences";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}

}
