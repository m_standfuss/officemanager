package officemanager.gui.swt.sidebaritems;

import org.eclipse.swt.graphics.Image;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.gui.swt.views.FlatRateSearchView;
import officemanager.gui.swt.views.IOfficeManagerView;

public class FlatRateSearchSideItem extends SideBarView {

	private final FlatRateSearchView view;

	public FlatRateSearchSideItem() {
		view = new FlatRateSearchView();
	}

	@Override
	public Image getSideBarIcon() {
		return ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_SIDEBAR);
	}

	@Override
	public String getSideBarName() {
		return "Flat Rate Services";
	}

	@Override
	public IOfficeManagerView getView() {
		return view;
	}
}
