package officemanager.gui.swt.ui.widgets;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.Maps;

import officemanager.utility.Tuple;

public class IdentifierCombo<T> extends Combo {
	private final Map<Integer, T> indexToIdentifier = Maps.newHashMap();

	public IdentifierCombo(Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * Batch loads a list of items calling loadItem for each.
	 * 
	 * @param items
	 *            The list of items to load.
	 */
	public void loadItems(List<Tuple<T, String>> items) {
		checkNotNull(items);
		for (final Tuple<T, String> item : items) {
			loadItem(item);
		}
	}

	/**
	 * Loads an item at the bottom of the combo item list.
	 * 
	 * @param item
	 *            The identifier and display of the item to add.
	 */
	public void loadItem(Tuple<T, String> item) {
		final Integer nextIndex = getItemCount();
		super.add(item.item2, nextIndex);
		indexToIdentifier.put(nextIndex, item.item1);
	}

	/**
	 * Gets the identifier set for the selected item in the combo box. Null if
	 * nothing is selected.
	 * 
	 * @return The identifier.
	 */
	public T getSelectedIdentifier() {
		return indexToIdentifier.get(getSelectionIndex());
	}

	@Override
	public void add(String string) {
		throw new IllegalAccessError("Call the loadItem or loadItems method instead to load items into the combo.");
	}

	@Override
	public void add(String string, int index) {
		throw new IllegalAccessError("Call the loadItem or loadItems method instead to load items into the combo.");
	}

	@Override
	public void setItem(int index, String string) {
		throw new IllegalAccessError("Call the loadItem or loadItems method instead to load items into the combo.");
	}

	@Override
	public void setItems(String[] items) {
		throw new IllegalAccessError("Call the loadItem or loadItems method instead to load items into the combo.");
	}

	@Override
	protected void checkSubclass() {}
}
