package officemanager.gui.swt.ui.wizards.pages;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TreeItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.LocationTreeComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class LocationSelectionPage extends OfficeManagerWizardPage {

	public LocationTreeComposite composite;

	private final boolean requireSelection;
	private final boolean multipleSelections;

	public LocationSelectionPage(boolean requireSelection, boolean multipleSelections) {
		super("locationSelectionPage", "Location Selection", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.CRATE, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		this.requireSelection = requireSelection;
		this.multipleSelections = multipleSelections;
		setDescription("Select an inventory location.");
		setPageComplete(!requireSelection);
	}

	public LocationSelectionPage() {
		this(false, false);
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrComposite = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrComposite.setExpandHorizontal(true);
		scrComposite.setExpandVertical(true);

		composite = new LocationTreeComposite(scrComposite, SWT.NONE, SWT.SINGLE | SWT.CHECK | SWT.BORDER);
		composite.showToolbar(false);

		scrComposite.setContent(composite);
		scrComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrComposite);
		composite.addListener(SWT.Resize,
				l -> scrComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));
		addListeners();
	}

	private void addListeners() {
		composite.treeLocations.addListener(SWT.Selection, e -> {
			if (e.detail != SWT.CHECK) {
				return;
			}
			setPageComplete(!requireSelection || isItemChecked());
			if (!((TreeItem) e.item).getChecked()) {
				// If unselecting always allow to happen.
				return;
			}
			if (multipleSelections) {
				// Under the limit dont care.
				return;
			} else {
				unCheckAllItems();
				((TreeItem) e.item).setChecked(true);
			}
		});
	}

	private boolean isItemChecked() {
		for (TreeItem item : composite.treeLocations.getItems()) {
			if (item.getChecked()) {
				return true;
			}
		}
		return false;
	}

	public void unCheckAllItems() {
		for (final TreeItem item : composite.treeLocations.getItems()) {
			item.setChecked(false);
		}
	}
}
