package officemanager.gui.swt.ui.wizards;

import java.util.List;

import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.delegates.ClientProductDelegate;
import officemanager.biz.delegates.PersonDelegate;
import officemanager.biz.records.ClientProductRecord;
import officemanager.biz.records.ClientProductSelectionRecord;
import officemanager.biz.records.ClientRecord;
import officemanager.biz.records.PersonRecord;
import officemanager.gui.swt.ui.wizards.pages.ClientInformationPage;
import officemanager.gui.swt.ui.wizards.pages.ClientProductPage;
import officemanager.gui.swt.views.ClientView;
import officemanager.gui.swt.views.OfficeManagerView;

public class ClientWizard extends PersonWizard {

	private final ClientDelegate clientDelegate;
	private final ClientProductDelegate clientProductDelegate;

	private ClientRecord clientInContext;
	private ClientInformationPage clientInformationPage;
	private ClientProductPage clientProductPage;

	public ClientWizard() {
		this(null);
	}

	public ClientWizard(Long clientId) {
		setWindowTitle("Client Information");
		clientDelegate = new ClientDelegate();
		clientProductDelegate = new ClientProductDelegate();
		if (clientId != null) {
			clientInContext = clientDelegate.getClientRecord(clientId);
		} else {
			clientInContext = new ClientRecord();
		}
	}

	@Override
	public PersonDelegate getPersonDelegate() {
		return clientDelegate;
	}

	@Override
	public PersonRecord getPersonInContext() {
		return clientInContext;
	}

	@Override
	public void addPages() {
		super.addPages();

		clientInformationPage = new ClientInformationPage();
		clientInformationPage.addFirstShowListener(e -> populateClientPage());
		addPage(clientInformationPage);

		clientProductPage = new ClientProductPage();
		clientProductPage.addFirstShowListener(e -> populateClientProductPage());
		addPage(clientProductPage);
	}

	@Override
	public boolean performFinish() {
		populatePersonRecord();

		clientInformationPage.populateClientRecord(clientInContext);
		Long clientId = clientDelegate.persistClientRecord(clientInContext);

		List<ClientProductRecord> productRecords = clientProductPage.getProductsToAdd();
		for (ClientProductRecord product : productRecords) {
			product.setClientId(clientId);
			clientProductDelegate.persistClientProduct(product);
		}

		clientInContext.setClientId(clientId);
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		if (clientInContext.getClientId() == null) {
			return null;
		}
		return new ClientView(clientInContext.getClientId());
	}

	@Override
	public boolean canFinish() {
		return getContainer().getCurrentPage() == clientProductPage;
	}

	private void populateClientPage() {
		clientInformationPage.populateClientInformation(clientInContext);
	}

	private void populateClientProductPage() {
		clientProductPage.populateProductStatus(clientProductDelegate.getProductStatuses());
		clientProductPage.populateProductTypes(clientProductDelegate.getProductTypes());
		if (clientInContext.getClientId() != null) {
			List<ClientProductSelectionRecord> productRecords = clientProductDelegate
					.getActiveClientProductSelections(clientInContext.getClientId());
			clientProductPage.populateClientProducts(productRecords);
		}
	}
}
