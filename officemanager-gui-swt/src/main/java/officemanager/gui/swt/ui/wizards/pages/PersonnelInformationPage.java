package officemanager.gui.swt.ui.wizards.pages;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.delegates.PersonnelDelegate;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PersonnelRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PersonnelEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelInformationPage extends OfficeManagerWizardPage {

	private final PersonnelDelegate personnelDelegate;

	private Long personnelId;
	private PersonnelEditComposite composite;

	public PersonnelInformationPage() {
		super("PersonnelInformation", "Personnel Information", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.USER_EDIT, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("Required fields must be completed.");
		setPageComplete(false);
		personnelDelegate = new PersonnelDelegate();
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		composite = new PersonnelEditComposite(scrCompositeSideBar, SWT.NULL);

		scrCompositeSideBar.setContent(composite);
		scrCompositeSideBar.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		composite.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));
		setControl(scrCompositeSideBar);
		addListeners();
	}

	@Override
	public void setFocus() {
		composite.cmbPersonnelType.setFocus();
	}

	public void populatePersonnelTypes(List<CodeValueRecord> personnelTypes) {
		composite.cmbPersonnelType.populateSelections(personnelTypes);
	}

	public void populatePersonnelStatus(List<CodeValueRecord> personnelStatus) {
		composite.cmbPersonnelStatus.populateSelections(personnelStatus);
	}

	public void populatePersonnelInformation(PersonnelRecord personnelRecord) {
		personnelId = personnelRecord.getPersonnelId();
		composite.cmbPersonnelStatus.setSelectedValue(personnelRecord.getPersonnelStatus());
		composite.cmbPersonnelType.setSelectedValue(personnelRecord.getPersonnelType());
		composite.txtUsername.setText(personnelRecord.getUsername());
		checkPageComplete();
	}

	public void populatePersonnelRecord(PersonnelRecord personnel) {
		personnel.setPersonnelStatus(composite.cmbPersonnelStatus.getCodeValueRecord());
		personnel.setPersonnelType(composite.cmbPersonnelType.getCodeValueRecord());
		personnel.setUsername(composite.txtUsername.getText());
	}

	private void addListeners() {
		composite.cmbPersonnelStatus.addListener(SWT.Selection, l -> checkPageComplete());
		composite.cmbPersonnelType.addListener(SWT.Selection, l -> checkPageComplete());
		composite.txtUsername.addModifyListener(e -> checkPageComplete());
	}

	private void checkPageComplete() {
		if (composite.cmbPersonnelStatus.getSelectionIndex() == -1) {
			setPageComplete(false);
			setErrorMessage("Please select a personnel status.");
			return;
		}
		if (composite.cmbPersonnelType.getSelectionIndex() == -1) {
			setPageComplete(false);
			setErrorMessage("Please select a personnel type.");
			return;
		}
		if (composite.txtUsername.getText().isEmpty()) {
			setPageComplete(false);
			setErrorMessage("Please fill out the username.");
			return;
		}

		boolean usernameTaken;
		if (personnelId == null) {
			usernameTaken = personnelDelegate.usernameTaken(composite.txtUsername.getText());
		} else {
			usernameTaken = personnelDelegate.usernameTaken(personnelId, composite.txtUsername.getText());
		}
		if (usernameTaken) {
			setPageComplete(false);
			setErrorMessage("The username is currently taken.");
			return;
		}
		setPageComplete(true);
		setErrorMessage(null);
		setDescription("Enter the personnel's information.");
	}
}
