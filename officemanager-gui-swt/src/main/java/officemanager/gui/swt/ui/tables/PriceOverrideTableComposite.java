package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class PriceOverrideTableComposite extends Composite {

	public static final int COL_ORDER_NUM = 0;
	public static final int COL_ORIG_PRICE = 1;
	public static final int COL_OVERRIDE_PRICE = 2;
	public static final int COL_DATE = 3;
	public static final int COL_PRSNL_NAME = 4;
	public static final int COL_REASON = 5;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnOrderNumber;
	public final TableColumn tblclmnOriginalPrice;
	public final TableColumn tblclmnOverridePrice;
	public final TableColumn tblclmnOverrideDate;
	public final TableColumn tblclmnPersonnelName;
	public final TableColumn tblclmnReason;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public PriceOverrideTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new FillLayout());
		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnOrderNumber = new TableColumn(table, SWT.NONE);
		tblclmnOrderNumber.setWidth(100);
		tblclmnOrderNumber.setText("Order #");

		tblclmnOriginalPrice = new TableColumn(table, SWT.NONE);
		tblclmnOriginalPrice.setWidth(100);
		tblclmnOriginalPrice.setText("Orig. Price");

		tblclmnOverridePrice = new TableColumn(table, SWT.NONE);
		tblclmnOverridePrice.setWidth(100);
		tblclmnOverridePrice.setText("New Price");

		tblclmnOverrideDate = new TableColumn(table, SWT.NONE);
		tblclmnOverrideDate.setWidth(100);
		tblclmnOverrideDate.setText("Date");

		tblclmnPersonnelName = new TableColumn(table, SWT.NONE);
		tblclmnPersonnelName.setWidth(100);
		tblclmnPersonnelName.setText("Personnel");

		tblclmnReason = new TableColumn(table, SWT.NONE);
		tblclmnReason.setWidth(100);
		tblclmnReason.setText("Reason");
	}
}
