package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.NumberText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class AddressEditComposite extends Composite {
	public final Label lblAddress;
	public final UpperCaseText txtAddressLine1;
	public final UpperCaseText txtAddressLine2;
	public final UpperCaseText txtAddressLine3;
	public final Label lblCity;
	public final Label lblState;
	public final Label lblZip;
	public final Label lblAddressType;
	public final CodeValueCombo cmbAddressType;
	public final UpperCaseText txtCity;
	public final UpperCaseText txtState;
	public final NumberText txtZip;
	public final Button btnPrimaryAddress;
	private final Label lblDenotesA;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public AddressEditComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(4, false));

		lblAddress = new Label(this, SWT.NONE);
		lblAddress.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAddress.setText("Address*:");
		lblAddress.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtAddressLine1 = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtAddressLine1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAddressLine1.widthHint = 200;
		txtAddressLine1.setLayoutData(gd_txtAddressLine1);
		txtAddressLine1.setTextLimit(70);

		lblCity = new Label(this, SWT.NONE);
		lblCity.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCity.setText("City:");
		lblCity.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtCity = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtCity = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtCity.widthHint = 150;
		txtCity.setLayoutData(gd_txtCity);
		new Label(this, SWT.NONE);

		txtAddressLine2 = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtAddressLine2 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAddressLine2.widthHint = 200;
		txtAddressLine2.setLayoutData(gd_txtAddressLine2);
		txtAddressLine2.setTextLimit(70);

		lblState = new Label(this, SWT.NONE);
		lblState.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblState.setText("State:");
		lblState.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtState = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtState = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtState.widthHint = 75;
		txtState.setLayoutData(gd_txtState);
		new Label(this, SWT.NONE);
		txtState.setTextLimit(30);

		txtAddressLine3 = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtAddressLine3 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAddressLine3.widthHint = 200;
		txtAddressLine3.setLayoutData(gd_txtAddressLine3);
		txtAddressLine3.setTextLimit(70);

		lblZip = new Label(this, SWT.NONE);
		lblZip.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblZip.setText("Zip:");
		lblZip.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtZip = new NumberText(this, SWT.BORDER);
		GridData gd_txtZip = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtZip.widthHint = 50;
		txtZip.setLayoutData(gd_txtZip);
		txtZip.setTextLimit(20);

		lblAddressType = new Label(this, SWT.NONE);
		lblAddressType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAddressType.setText("Address Type*:");
		lblAddressType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbAddressType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbAddressType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbAddressType.widthHint = 150;
		cmbAddressType.setLayoutData(gd_cmbAddressType);

		btnPrimaryAddress = new Button(this, SWT.CHECK);
		btnPrimaryAddress.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnPrimaryAddress.setText("Primary Address");

		lblDenotesA = new Label(this, SWT.NONE);
		lblDenotesA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		lblDenotesA.setText("* Denotes a required field");

		setTabList(new Control[] { txtAddressLine1, txtAddressLine2, txtAddressLine3, cmbAddressType, txtCity, txtState,
				txtZip, btnPrimaryAddress });
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
