package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.CodeValueTable;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;
import officemanager.gui.swt.ui.widgets.ScannerText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PartSearchComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmNewPart;
	public final ToolItem tlItmEnterInventory;
	public final ToolItem tlItmScanPart;

	public final ScannerText scnTxt;

	public final ScrolledComposite scrCompositeSideBarFields;
	public final Composite compositeSideBarFields;
	public final Button btnSearch;
	public final Button btnClear;

	public final Composite compositeCenter;
	public final PartSearchTableComposite searchResultsTable;
	public final Label lblPartName;
	public final UpperCaseText txtPartName;
	public final Label lblPartDescription;
	public final UpperCaseText txtPartDescription;
	public final Label lblManufacturerId;
	public final UpperCaseText txtManufId;
	public final Label lblManufacturer;
	public final CodeValueTable tblManufacturer;
	public final Label lblProductType;
	public final Label lblCategory;
	public final CodeValueTable tblProductType;
	public final CodeValueTable tblCategory;
	public final Button btnInStock;
	public final Label lblDenotesCurrentSale;
	public Composite compositeButtons;
	public Composite compositeSideBar;

	public PartSearchComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		tlItmScanPart = new ToolItem(toolBar, SWT.NONE);
		tlItmScanPart.setImage(ResourceManager.getImage(ImageFile.SCAN, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmScanPart.setToolTipText("Scan Part");

		tlItmNewPart = new ToolItem(toolBar, SWT.NONE);
		tlItmNewPart.setImage(ResourceManager.getImage(ImageFile.PART_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmNewPart.setToolTipText("Add New Part");
		tlItmNewPart.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_PART));

		tlItmEnterInventory = new ToolItem(toolBar, SWT.NONE);
		tlItmEnterInventory.setImage(ResourceManager.getImage(ImageFile.INVENTORY, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmEnterInventory.setToolTipText("Enter Inventory");
		tlItmEnterInventory.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_INVTRY));

		scnTxt = new ScannerText(this, SWT.NONE);
		GridData gd_scnTxt = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_scnTxt.exclude = true;
		scnTxt.setLayoutData(gd_scnTxt);

		compositeSideBar = new Composite(this, SWT.BORDER);
		compositeSideBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		compositeSideBar.setLayout(new GridLayout(1, false));

		scrCompositeSideBarFields = new ScrolledComposite(compositeSideBar, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeSideBarFields.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		scrCompositeSideBarFields.setExpandHorizontal(true);
		scrCompositeSideBarFields.setExpandVertical(true);

		compositeSideBarFields = new Composite(scrCompositeSideBarFields, SWT.NONE);
		compositeSideBarFields.setLayout(new GridLayout(1, false));

		lblPartName = new Label(compositeSideBarFields, SWT.NONE);
		lblPartName.setText("Part Name");
		lblPartName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtPartName = new UpperCaseText(compositeSideBarFields, SWT.BORDER);
		GridData gd_txtPartName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtPartName.widthHint = 150;
		txtPartName.setLayoutData(gd_txtPartName);

		lblPartDescription = new Label(compositeSideBarFields, SWT.NONE);
		lblPartDescription.setText("Part Description");
		lblPartDescription.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtPartDescription = new UpperCaseText(compositeSideBarFields, SWT.BORDER);
		GridData gd_txtPartDescription = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtPartDescription.widthHint = 150;
		txtPartDescription.setLayoutData(gd_txtPartDescription);

		lblManufacturerId = new Label(compositeSideBarFields, SWT.NONE);
		lblManufacturerId.setText("Manufacturer ID");
		lblManufacturerId.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtManufId = new UpperCaseText(compositeSideBarFields, SWT.BORDER);
		GridData gd_txtManufId = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtManufId.widthHint = 150;
		txtManufId.setLayoutData(gd_txtManufId);

		lblManufacturer = new Label(compositeSideBarFields, SWT.NONE);
		lblManufacturer.setText("Manufacturer");
		lblManufacturer.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		tblManufacturer = new CodeValueTable(compositeSideBarFields);
		GridData gd_tblManufacturer = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_tblManufacturer.widthHint = 175;
		gd_tblManufacturer.heightHint = 150;
		tblManufacturer.setLayoutData(gd_tblManufacturer);

		lblCategory = new Label(compositeSideBarFields, SWT.NONE);
		lblCategory.setText("Category");
		lblCategory.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		tblCategory = new CodeValueTable(compositeSideBarFields);
		GridData gd_tblCategory = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_tblCategory.widthHint = 175;
		gd_tblCategory.heightHint = 120;
		tblCategory.setLayoutData(gd_tblCategory);

		lblProductType = new Label(compositeSideBarFields, SWT.NONE);
		lblProductType.setText("Product Type");
		lblProductType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		tblProductType = new CodeValueTable(compositeSideBarFields);
		GridData gd_tblProductType = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_tblProductType.widthHint = 175;
		gd_tblProductType.heightHint = 100;
		tblProductType.setLayoutData(gd_tblProductType);

		btnInStock = new Button(compositeSideBarFields, SWT.CHECK);
		btnInStock.setText("In Stock");

		compositeButtons = new Composite(compositeSideBar, SWT.NONE);
		compositeButtons.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		compositeButtons.setLayout(new GridLayout(2, false));

		btnSearch = new Button(compositeButtons, SWT.NONE);
		btnSearch.setText("Search");

		btnClear = new Button(compositeButtons, SWT.NONE);
		btnClear.setText("Clear");

		scrCompositeSideBarFields.setContent(compositeSideBarFields);
		scrCompositeSideBarFields.setMinSize(compositeSideBarFields.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		compositeCenter = new Composite(this, SWT.BORDER);
		compositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeCenter.setLayout(new GridLayout(1, false));

		searchResultsTable = new PartSearchTableComposite(compositeCenter, SWT.NONE, tableStyle);
		searchResultsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		lblDenotesCurrentSale = new Label(compositeCenter, SWT.NONE);
		lblDenotesCurrentSale.setText("Denotes Current Sale Price");
		lblDenotesCurrentSale.setForeground(AppPrefs.COLOR_DISCOUNT_PRICE);
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PartSearchComposite(Composite parent, int style) {
		this(parent, style, SWT.BORDER | SWT.FULL_SELECTION);
	}

	public void hideToolItems(boolean scanPart, boolean addPart, boolean addInventory) {
		if (scanPart) {
			tlItmScanPart.dispose();
		}
		if (addPart) {
			tlItmNewPart.dispose();
		}
		if (addInventory) {
			tlItmEnterInventory.dispose();
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
