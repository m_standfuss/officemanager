package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.AccountTransactionTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class AccountComposite extends Composite {
	public final ToolBar toolBar;
	public ToolItem tlItmAddPayment;
	public ToolItem tlItmCloseAccount;
	public ToolItem tlItmSuspendAccount;
	public ToolItem tlItmReopenAccount;
	public final CollapsibleComposite cllCmpClientInfo;
	public final ClientInformationComposite cmpClientInfo;
	public final AccountInformationComposite accountInformationComposite;
	public final CollapsibleComposite cllCmpTransactions;
	public final AccountTransactionTableComposite tblAccountTransactions;
	public final Composite composite;
	public final DateTime dttmStart;
	public final Label lblTo;
	public final DateTime dttmEnd;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public AccountComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout());

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		addToolItems();

		accountInformationComposite = new AccountInformationComposite(this, SWT.NONE);
		accountInformationComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		accountInformationComposite.setBackground(AppPrefs.COLOR_ORDER_HEADER);
		accountInformationComposite.lblCurrentBalance.setForeground(AppPrefs.COLOR_ORDER_HEADER_TEXT);
		accountInformationComposite.lblCurrentBalanceHeader.setForeground(AppPrefs.COLOR_ORDER_HEADER_TEXT);
		accountInformationComposite.lblLastStatement.setForeground(AppPrefs.COLOR_ORDER_HEADER_TEXT);
		accountInformationComposite.lblLastStatementHeader.setForeground(AppPrefs.COLOR_ORDER_HEADER_TEXT);
		accountInformationComposite.lblStatus.setForeground(AppPrefs.COLOR_ORDER_HEADER_TEXT);
		accountInformationComposite.lblStatusHeader.setForeground(AppPrefs.COLOR_ORDER_HEADER_TEXT);

		accountInformationComposite.lblCurrentBalance.setBackground(AppPrefs.COLOR_ORDER_HEADER);
		accountInformationComposite.lblCurrentBalanceHeader.setBackground(AppPrefs.COLOR_ORDER_HEADER);
		accountInformationComposite.lblLastStatement.setBackground(AppPrefs.COLOR_ORDER_HEADER);
		accountInformationComposite.lblLastStatementHeader.setBackground(AppPrefs.COLOR_ORDER_HEADER);
		accountInformationComposite.lblStatus.setBackground(AppPrefs.COLOR_ORDER_HEADER);
		accountInformationComposite.lblStatusHeader.setBackground(AppPrefs.COLOR_ORDER_HEADER);

		accountInformationComposite.lblCurrentBalanceHeader.setFont(ResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		accountInformationComposite.lblCurrentBalance.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		accountInformationComposite.lblLastStatementHeader.setFont(ResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		accountInformationComposite.lblLastStatement.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		accountInformationComposite.lblStatusHeader.setFont(ResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		accountInformationComposite.lblStatus.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cllCmpClientInfo = new CollapsibleComposite(this, SWT.NONE);
		cllCmpClientInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cllCmpClientInfo.setTitleText("Client Information");
		cllCmpClientInfo.setTitleImage(ResourceManager.getImage(ImageFile.USER_INFO, AppPrefs.ICN_SIZE_CLPCMP));
		cllCmpClientInfo.composite.setLayout(new FillLayout());

		cmpClientInfo = new ClientInformationComposite(cllCmpClientInfo.composite, SWT.NONE);

		cllCmpTransactions = new CollapsibleComposite(this, SWT.NONE);
		cllCmpTransactions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cllCmpTransactions.setTitleText("Recent Transactions");
		cllCmpTransactions.setTitleImage(ResourceManager.getImage(ImageFile.TRANSACTION, AppPrefs.ICN_SIZE_CLPCMP));
		cllCmpTransactions.composite.setLayout(new GridLayout(1, false));

		composite = new Composite(cllCmpTransactions.composite, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		composite.setLayout(new GridLayout(3, false));

		dttmStart = new DateTime(composite, SWT.BORDER | SWT.DROP_DOWN);

		lblTo = new Label(composite, SWT.NONE);
		lblTo.setText("to");

		dttmEnd = new DateTime(composite, SWT.BORDER | SWT.DROP_DOWN);

		tblAccountTransactions = new AccountTransactionTableComposite(cllCmpTransactions.composite, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION);
		tblAccountTransactions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	public void addToolItems() {
		addToolItems(true, true, true, true);
	}

	public void addToolItems(boolean addPayment, boolean suspendAccount, boolean closeAccount, boolean reopenAccount) {
		disposeToolItems();
		if (addPayment) {
			tlItmAddPayment = new ToolItem(toolBar, SWT.NONE);
			tlItmAddPayment.setImage(ResourceManager.getImage(ImageFile.CASH, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tlItmAddPayment.setToolTipText("Make Payment");
		}

		if (reopenAccount) {
			tlItmReopenAccount = new ToolItem(toolBar, SWT.NONE);
			tlItmReopenAccount.setImage(ResourceManager.getImage(ImageFile.OPEN, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tlItmReopenAccount.setToolTipText("Reopen Account");
			tlItmReopenAccount.setEnabled(AppPrefs.hasPermission(AppPermissions.REOPEN_ACCOUNT));
		}

		if (suspendAccount) {
			tlItmSuspendAccount = new ToolItem(toolBar, SWT.NONE);
			tlItmSuspendAccount.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tlItmSuspendAccount.setToolTipText("Suspend Account");
			tlItmSuspendAccount.setEnabled(AppPrefs.hasPermission(AppPermissions.SUSPEND_ACCOUNT));
		}

		if (closeAccount) {
			tlItmCloseAccount = new ToolItem(toolBar, SWT.NONE);
			tlItmCloseAccount.setImage(ResourceManager.getImage(ImageFile.CLOSE, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tlItmCloseAccount.setToolTipText("Close Account");
			tlItmCloseAccount.setEnabled(AppPrefs.hasPermission(AppPermissions.CLOSE_ACCOUNT));
		}
	}

	private void disposeToolItems() {
		if (tlItmAddPayment != null) {
			tlItmAddPayment.dispose();
		}
		if (tlItmSuspendAccount != null) {
			tlItmSuspendAccount.dispose();
		}
		if (tlItmCloseAccount != null) {
			tlItmCloseAccount.dispose();
		}
		if (tlItmReopenAccount != null) {
			tlItmReopenAccount.dispose();
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
