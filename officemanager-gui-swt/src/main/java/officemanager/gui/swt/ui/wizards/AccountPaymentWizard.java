package officemanager.gui.swt.ui.wizards;

import java.math.BigDecimal;
import java.util.List;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.AccountDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.delegates.TradeInDelegate;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.TradeInRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.AccountPaymentService;
import officemanager.gui.swt.ui.wizards.pages.PaymentsPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;

public class AccountPaymentWizard extends OfficeManagerWizard {

	private final long accountId;

	private final AccountDelegate accountDelegate;
	private final PaymentDelegate paymentDelegate;
	private final TradeInDelegate tradeInDelegate;
	private final AccountPaymentService paymentService;

	private PaymentsPage paymentsPage;
	private boolean canFinish;

	public AccountPaymentWizard(long accountId) {
		this.accountId = accountId;
		accountDelegate = new AccountDelegate();
		paymentDelegate = new PaymentDelegate();
		tradeInDelegate = new TradeInDelegate();
		paymentService = new AccountPaymentService();
		setWindowTitle("Make Payment Account #" + AppPrefs.FORMATTER.formatAccountNumber(accountId));
	}

	@Override
	public void addPages() {
		paymentsPage = new PaymentsPage();
		paymentsPage.addFirstShowListener(e -> {
			BigDecimal currentBalance = accountDelegate.getAccountBalance(accountId);
			Long clientId = accountDelegate.getClientId(accountId);

			paymentService.setAllowNegativeBalance(true);
			paymentService.setAccountId(accountId);
			paymentService.setClientId(clientId);
			paymentService.setAmountOwed(Calculator.negate(currentBalance));
			paymentService.populatePage(false, true);
		});
		paymentService.setPage(paymentsPage);
		paymentService.addPaymentAddedListener(e -> {
			canFinish = true;
			getContainer().updateButtons();
		});
		addPage(paymentsPage);
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	@Override
	public boolean canFinish() {
		return canFinish;
	}

	@Override
	public boolean performFinish() {
		if (!MessageDialogs.askQuestion(getShell(), "Confirm Payments",
				"Are you sure you wish to make these payments on the account?")) {
			return false;
		}
		List<PaymentRecord> payments = paymentService.getPaymentsAdded();
		for (PaymentRecord record : payments) {
			paymentDelegate.makePaymentOnAccount(accountId, record);
		}

		List<TradeInRecord> tradeIns = paymentService.getTradeInsAdded();
		for (TradeInRecord record : tradeIns) {
			tradeInDelegate.insertTradeIn(record);
		}
		return true;
	}

	@Override
	public boolean performCancel() {
		if (!paymentService.getPaymentsAdded().isEmpty()) {
			if (!MessageDialogs.askQuestion(getShell(), "Close Payments",
					"Are you sure you wish to cancel? Any new payments added will be lost.")) {
				return false;
			}
		}
		return super.performCancel();
	}
}
