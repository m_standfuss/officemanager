package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.util.ResourceManager;

public class PasswordSetComposite extends Composite {
	private Text txtPassword;
	private Text txtConfirmPassword;
	private Button btnOk;
	private Button btnCancel;
	private Label label;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PasswordSetComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	public String getPassword() {
		return txtPassword.getText();
	}

	public String getConfirmPassword() {
		return txtConfirmPassword.getText();
	}

	public void addOkListener(Listener listener) {
		btnOk.addListener(SWT.Selection, listener);
	}

	public void addCancelListener(Listener listener) {
		btnCancel.addListener(SWT.Selection, listener);
	}

	public void addNewKeyListener(KeyListener listener) {
		txtConfirmPassword.addKeyListener(listener);
	}

	private void createContents() {
		setLayout(new GridLayout(3, false));

		Label lblPassword = new Label(this, SWT.NONE);
		lblPassword.setText("New Password:");

		txtPassword = new Text(this, SWT.BORDER | SWT.PASSWORD);
		GridData gd_txtPassword = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtPassword.widthHint = 175;
		txtPassword.setLayoutData(gd_txtPassword);
		new Label(this, SWT.NONE);

		Label lblConfirmPassword = new Label(this, SWT.NONE);
		lblConfirmPassword.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblConfirmPassword.setText("Confirm Password:");

		txtConfirmPassword = new Text(this, SWT.BORDER | SWT.PASSWORD);
		GridData gd_txtConfirmPassword = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtConfirmPassword.widthHint = 175;
		txtConfirmPassword.setLayoutData(gd_txtConfirmPassword);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);

		btnOk = new Button(this, SWT.RIGHT_TO_LEFT);
		btnOk.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnOk.setText("     OK     ");

		btnCancel = new Button(this, SWT.NONE);
		btnCancel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		btnCancel.setText("   Cancel   ");

		label = new Label(this, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		label.setText("*Passwords must be between 6-25 characters.");
		label.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
	}

	@Override
	public void dispose() {
		ResourceManager.disposeFonts();
		super.dispose();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
