package officemanager.gui.swt.ui.wizards.pages;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.Lists;

import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.LongTextRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.OrderCommentService;
import officemanager.gui.swt.ui.composites.ButtonBarComposite;
import officemanager.gui.swt.ui.composites.CommentEditComposite;
import officemanager.gui.swt.ui.tables.OrderCommentTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class CommentsPage extends OfficeManagerWizardPage {

	private final List<OrderCommentRecord> recordsToAdd;
	private final OrderCommentService orderCommentService;

	private CommentEditComposite editComposite;
	private OrderCommentTableComposite tblComposite;

	/**
	 * Create the wizard.
	 */
	public CommentsPage() {
		super("ClientProducts", "Order Comments", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.COMMENT, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("Enter any comments for the order.");
		setPageComplete(true);
		recordsToAdd = Lists.newArrayList();
		orderCommentService = new OrderCommentService(Lists.newArrayList(OrderCommentTableComposite.COL_AUTHOR,
				OrderCommentTableComposite.COL_COMMENT, OrderCommentTableComposite.COL_COMMENT_TYPE));
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		Composite root = new Composite(scrCompositeSideBar, SWT.NONE);
		root.setLayout(new GridLayout());

		CollapsibleComposite clpseCmp = new CollapsibleComposite(root, SWT.NONE);
		clpseCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		clpseCmp.setTitleText("New Comment");

		clpseCmp.composite.setLayout(new GridLayout());
		editComposite = new CommentEditComposite(clpseCmp.composite, SWT.NONE);
		editComposite.txtAuthor.setText(AppPrefs.currentSession.nameFullFormatted);
		editComposite.txtAuthor.setEnabled(false);
		editComposite.txtLastUpdatedBy.setEnabled(false);
		editComposite.txtLastUpdatedOn.setEnabled(false);

		ButtonBarComposite btnBar = new ButtonBarComposite(clpseCmp.composite, SWT.NONE);
		btnBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		btnBar.btnSave.addListener(SWT.Selection, l -> addNewComment());
		btnBar.btnClear.addListener(SWT.Selection, l -> clear());

		tblComposite = new OrderCommentTableComposite(root, SWT.NONE, SWT.BORDER | SWT.FULL_SELECTION | SWT.NONE);
		GridData gd_phoneTbl = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_phoneTbl.heightHint = 200;
		tblComposite.setLayoutData(gd_phoneTbl);

		scrCompositeSideBar.setContent(root);
		scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrCompositeSideBar);

		clpseCmp.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT)));

		orderCommentService.setComposite(tblComposite);
	}

	@Override
	public void setFocus() {
		editComposite.cmbCommentType.setFocus();
	}

	public void populateCommentTypes(List<CodeValueRecord> commentTypes) {
		editComposite.cmbCommentType.populateSelections(commentTypes);
	}

	public List<OrderCommentRecord> getComments() {
		return recordsToAdd;
	}

	private void clear() {
		editComposite.cmbCommentType.deselectAll();
		editComposite.txtAuthor.setText(AppPrefs.currentSession.nameFullFormatted);
		editComposite.txtComment.setText("");
	}

	private void addNewComment() {
		if (editComposite.cmbCommentType.getCodeValueRecord() == null) {
			MessageDialogs.displayError(getShell(), "Cannot Add", "Please select a comment type.");
			return;
		}

		OrderCommentRecord record = new OrderCommentRecord();
		record.setAuthoredBy(editComposite.txtAuthor.getText());
		record.setCommentType(editComposite.cmbCommentType.getCodeValueRecord());
		record.setLongText(new LongTextRecord(editComposite.txtComment.getText()));
		recordsToAdd.add(record);
		orderCommentService.addComment(record);
		clear();
	}
}
