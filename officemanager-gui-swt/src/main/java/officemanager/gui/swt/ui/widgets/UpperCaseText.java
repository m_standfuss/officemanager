package officemanager.gui.swt.ui.widgets;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class UpperCaseText extends Text {

	public UpperCaseText(Composite parent, int style) {
		super(parent, style);
		addVerifyListener(e -> e.text = e.text.toUpperCase());
	}

	@Override
	protected void checkSubclass() {}
}
