package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.SaleTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class FlatRateComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmEditFlatRate;
	public final ToolItem tlItmAddSale;
	public final ToolItem tlItmDeleteFlatRate;

	public final CollapsibleComposite compositeFlatRateInfo;
	public final CollapsibleComposite compositeSaleInfo;
	public final Label lblServiceNameHeader;
	public final Label lblServiceName;
	public final Label lblDescriptionHeader;
	public final Text txtDescription;
	public final Label lblBasePriceHeader;
	public final Label lblBasePrice;
	public final Label lblCategoryHeader;
	public final Label lblCategory;
	public final Label lblProductTypeHeader;
	public final Label lblProductType;
	public final Label lblAmountSoldHeader;
	public final Label lblAmountSold;
	public final Label lblBarcodeHeader;
	public final Label lblBarcode;
	public final SaleTableComposite saleTableComposite;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public FlatRateComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		tlItmEditFlatRate = new ToolItem(toolBar, SWT.NONE);
		tlItmEditFlatRate.setImage(ResourceManager.getImage(ImageFile.SERVICE_EDIT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmEditFlatRate.setToolTipText("Edit Service");
		tlItmEditFlatRate.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_FLAT_RATE_DETAILS));

		tlItmAddSale = new ToolItem(toolBar, SWT.NONE);
		tlItmAddSale.setImage(ResourceManager.getImage(ImageFile.SALE_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmAddSale.setToolTipText("Add Sale");
		tlItmAddSale.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_SALE));

		tlItmDeleteFlatRate = new ToolItem(toolBar, SWT.NONE);
		tlItmDeleteFlatRate.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmDeleteFlatRate.setToolTipText("Delete Service");
		tlItmDeleteFlatRate.setEnabled(AppPrefs.hasPermission(AppPermissions.REMOVE_SERVICE));

		ScrolledComposite scrolledComposite = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Composite compsite = new Composite(scrolledComposite, style);
		compsite.setLayout(new GridLayout());

		compositeFlatRateInfo = new CollapsibleComposite(compsite, SWT.NONE);
		compositeFlatRateInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		compositeFlatRateInfo.setTitleText("Flat Rate Service Details");
		GridLayout gl_partInfoComposite = new GridLayout();
		gl_partInfoComposite.verticalSpacing = 15;
		gl_partInfoComposite.horizontalSpacing = 20;
		gl_partInfoComposite.numColumns = 4;
		compositeFlatRateInfo.composite.setLayout(gl_partInfoComposite);

		lblServiceNameHeader = new Label(compositeFlatRateInfo.composite, SWT.NONE);
		lblServiceNameHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblServiceNameHeader.setText("Service Name:");
		lblServiceNameHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblServiceName = new Label(compositeFlatRateInfo.composite, SWT.NONE);

		lblBasePriceHeader = new Label(compositeFlatRateInfo.composite, SWT.NONE);
		lblBasePriceHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblBasePriceHeader.setText("Base Price:");
		lblBasePriceHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblBasePrice = new Label(compositeFlatRateInfo.composite, SWT.NONE);

		lblDescriptionHeader = new Label(compositeFlatRateInfo.composite, SWT.NONE);
		lblDescriptionHeader.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblDescriptionHeader.setText("Description");
		lblDescriptionHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblCategoryHeader = new Label(compositeFlatRateInfo.composite, SWT.NONE);
		lblCategoryHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCategoryHeader.setText("Category:");
		lblCategoryHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblCategory = new Label(compositeFlatRateInfo.composite, SWT.NONE);

		txtDescription = new Text(compositeFlatRateInfo.composite,
				SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescription = new GridData(SWT.FILL, SWT.FILL, false, false, 2, 3);
		gd_txtDescription.heightHint = 150;
		gd_txtDescription.widthHint = 150;
		txtDescription.setLayoutData(gd_txtDescription);

		lblProductTypeHeader = new Label(compositeFlatRateInfo.composite, SWT.NONE);
		lblProductTypeHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblProductTypeHeader.setText("Product Type:");
		lblProductTypeHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblProductType = new Label(compositeFlatRateInfo.composite, SWT.NONE);

		lblAmountSoldHeader = new Label(compositeFlatRateInfo.composite, SWT.NONE);
		lblAmountSoldHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAmountSoldHeader.setText("Amount Sold:");
		lblAmountSoldHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblAmountSold = new Label(compositeFlatRateInfo.composite, SWT.NONE);

		lblBarcodeHeader = new Label(compositeFlatRateInfo.composite, SWT.NONE);
		lblBarcodeHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblBarcodeHeader.setText("Barcode:");
		lblBarcodeHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblBarcode = new Label(compositeFlatRateInfo.composite, SWT.NONE);
		lblBarcode.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));

		compositeSaleInfo = new CollapsibleComposite(compsite, SWT.BORDER);
		compositeSaleInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		compositeSaleInfo.composite.setLayout(new FillLayout());
		compositeSaleInfo.setTitleText("Active Sales");

		saleTableComposite = new SaleTableComposite(compositeSaleInfo.composite, SWT.NONE, SWT.FULL_SELECTION);

		compsite.layout();
		compsite.addListener(SWT.Resize,
				e -> scrolledComposite.setMinSize(-1, compsite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		compositeFlatRateInfo.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, compsite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		compositeSaleInfo.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, compsite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		scrolledComposite.setContent(compsite);
		scrolledComposite.setMinSize(-1, compsite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
