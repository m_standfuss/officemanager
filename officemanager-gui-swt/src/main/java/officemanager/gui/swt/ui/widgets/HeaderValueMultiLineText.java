package officemanager.gui.swt.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

public class HeaderValueMultiLineText extends HeaderValue<String> {

	private static final int WIDTH_DEFAULT = 225;
	private static final int HEIGHT_DEFAULT = 100;

	private Text txtValue;

	public HeaderValueMultiLineText(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public String getEditableValue() {
		return txtValue.getText();
	}

	@Override
	protected void setEditableValue(String value) {
		txtValue.setText(value);
	}

	@Override
	protected int getEditableWidth() {
		return WIDTH_DEFAULT;
	}

	@Override
	protected int getEditableHeight() {
		return HEIGHT_DEFAULT;
	}

	@Override
	protected Control getEditableControl(Composite parent) {
		txtValue = new UpperCaseText(parent, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		return txtValue;
	}
}
