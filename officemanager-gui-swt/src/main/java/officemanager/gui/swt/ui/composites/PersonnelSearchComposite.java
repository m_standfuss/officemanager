package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.CodeValueTable;
import officemanager.gui.swt.ui.tables.PersonnelTableComposite;
import officemanager.gui.swt.ui.widgets.PhoneNumberText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelSearchComposite extends Composite {
	public final ToolBar toolBar;
	public final ToolItem tlItmNewPersonnel;

	public final ScrolledComposite scrCompositeSideBar;
	public final Composite compositeSideBar;
	public final Button btnSearch;
	public final Button btnClear;

	public final Composite compositeCenter;
	public final PersonnelTableComposite searchResultsTable;
	public final Label lblNameLast;
	public final UpperCaseText txtNameLast;
	public final Label lblNameFirst;
	public final UpperCaseText txtNameFirst;
	public final Label lblPrimaryPhone;
	public final PhoneNumberText txtPhone;
	public final CodeValueTable cdvlListStatus;
	public final Label lblStatus;
	public final Label lblUsername;
	public final Text txtUsername;
	public final Label lblType;
	public final CodeValueTable cdvlListType;

	public PersonnelSearchComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		tlItmNewPersonnel = new ToolItem(toolBar, SWT.NONE);
		tlItmNewPersonnel.setImage(ResourceManager.getImage(ImageFile.USER_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmNewPersonnel.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_PERSONNEL));
		tlItmNewPersonnel.setToolTipText("New Personnel");

		scrCompositeSideBar = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeSideBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		compositeSideBar = new Composite(scrCompositeSideBar, SWT.NONE);
		compositeSideBar.setLayout(new GridLayout(2, false));

		lblNameLast = new Label(compositeSideBar, SWT.NONE);
		lblNameLast.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblNameLast.setText("Name Last");
		lblNameLast.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtNameLast = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_txtNameLast = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtNameLast.widthHint = 150;
		txtNameLast.setLayoutData(gd_txtNameLast);

		lblNameFirst = new Label(compositeSideBar, SWT.NONE);
		lblNameFirst.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblNameFirst.setText("Name First");
		lblNameFirst.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtNameFirst = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_txtNameFirst = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtNameFirst.widthHint = 150;
		txtNameFirst.setLayoutData(gd_txtNameFirst);

		lblUsername = new Label(compositeSideBar, SWT.NONE);
		lblUsername.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblUsername.setText("Username");
		lblUsername.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtUsername = new Text(compositeSideBar, SWT.BORDER);
		GridData gd_txtUsername = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtUsername.widthHint = 150;
		txtUsername.setLayoutData(gd_txtUsername);

		lblPrimaryPhone = new Label(compositeSideBar, SWT.NONE);
		lblPrimaryPhone.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblPrimaryPhone.setText("Primary Phone");
		lblPrimaryPhone.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtPhone = new PhoneNumberText(compositeSideBar, SWT.BORDER);
		GridData gd_txtPhone = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtPhone.widthHint = 100;
		txtPhone.setLayoutData(gd_txtPhone);

		lblType = new Label(compositeSideBar, SWT.NONE);
		lblType.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblType.setText("Type");
		lblType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cdvlListType = new CodeValueTable(compositeSideBar);
		GridData gd_codeValueList = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_codeValueList.widthHint = 150;
		cdvlListType.setLayoutData(gd_codeValueList);

		lblStatus = new Label(compositeSideBar, SWT.NONE);
		lblStatus.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblStatus.setText("Status");
		lblStatus.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cdvlListStatus = new CodeValueTable(compositeSideBar);
		GridData gd_cdvlListStatus = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_cdvlListStatus.widthHint = 150;
		cdvlListStatus.setLayoutData(gd_cdvlListStatus);

		btnSearch = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnSearch = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnSearch.widthHint = 100;
		btnSearch.setLayoutData(gd_btnSearch);
		btnSearch.setText("Search");

		btnClear = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnClear = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnClear.widthHint = 100;
		btnClear.setLayoutData(gd_btnClear);
		btnClear.setText("Clear");

		scrCompositeSideBar.setContent(compositeSideBar);
		scrCompositeSideBar.setMinSize(compositeSideBar.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		compositeCenter = new Composite(this, SWT.BORDER);
		compositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeCenter.setLayout(new GridLayout(1, false));

		searchResultsTable = new PersonnelTableComposite(compositeCenter, SWT.NONE, tableStyle);
		searchResultsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PersonnelSearchComposite(Composite parent, int style) {
		this(parent, style, SWT.BORDER | SWT.FULL_SELECTION);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
