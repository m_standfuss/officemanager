package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class ServiceTableComposite extends Composite {

	public static final int COL_SERVICE_NAME = 0;
	public static final int COL_HOURS = 1;
	public static final int COL_RATE = 2;
	public static final int COL_TOTAL = 3;
	public static final int COL_DESCRIPTION = 4;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnServiceName;
	public final TableColumn tblclmnHours;
	public final TableColumn tblclmnRate;
	public final TableColumn tblclmnTotal;
	public final TableColumn tblclmnDescription;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ServiceTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new FillLayout());

		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnServiceName = new TableColumn(table, SWT.NONE);
		tblclmnServiceName.setWidth(100);
		tblclmnServiceName.setText("Service Name");

		tblclmnHours = new TableColumn(table, SWT.NONE);
		tblclmnHours.setWidth(100);
		tblclmnHours.setText("Hours");

		tblclmnRate = new TableColumn(table, SWT.NONE);
		tblclmnRate.setWidth(100);
		tblclmnRate.setText("Service Rate");

		tblclmnTotal = new TableColumn(table, SWT.NONE);
		tblclmnTotal.setWidth(100);
		tblclmnTotal.setText("Total Price");

		tblclmnDescription = new TableColumn(table, SWT.NONE);
		tblclmnDescription.setWidth(100);
		tblclmnDescription.setText("Description");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
