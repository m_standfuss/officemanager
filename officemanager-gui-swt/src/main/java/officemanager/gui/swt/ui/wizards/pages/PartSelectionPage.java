package officemanager.gui.swt.ui.wizards.pages;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.Lists;

import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.PartSearchService;
import officemanager.gui.swt.services.PartSearchTableService;
import officemanager.gui.swt.ui.composites.PartSearchComposite;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PartSelectionPage extends OfficeManagerWizardPage {

	private static final List<Integer> COLUMNS_TO_KEEP = Lists.newArrayList(PartSearchTableComposite.COL_PART_NAME,
			PartSearchTableComposite.COL_MANUF, PartSearchTableComposite.COL_MANUF_ID,
			PartSearchTableComposite.COL_CURRENT_PRICE, PartSearchTableComposite.COL_BASE_PRICE,
			PartSearchTableComposite.COL_DESCRIPTION, PartSearchTableComposite.COL_INVNTRY_STATUS,
			PartSearchTableComposite.COL_INVNTRY_CNT, PartSearchTableComposite.COL_AMT_SOLD);

	private final boolean removeCurrentPrice;
	private final int maxSelection;
	private final boolean needsSelection;
	private final boolean allowSearch;
	private final PartSearchService partSearchService;

	private PartSearchComposite partSearchComposite;

	/**
	 * Create the wizard.
	 */
	public PartSelectionPage() {
		this(1, true, false, true);
	}

	public PartSelectionPage(int maxSelection, boolean needsSelection, boolean removeCurrentPrice,
			boolean allowSearch) {
		super("partSelectionPage", "Part Selection", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.PART, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		checkArgument(maxSelection > 0, "The max selection must be greater than 0.");

		setPageComplete(!needsSelection);

		if (maxSelection == 1) {
			setDescription("Please select a part.");
		} else {
			setDescription("Please select parts.");
		}
		List<Integer> columnsToKeep = Lists.newArrayList(COLUMNS_TO_KEEP);
		if (removeCurrentPrice) {
			columnsToKeep.remove(PartSearchTableComposite.COL_CURRENT_PRICE);
		}
		partSearchService = new PartSearchService(columnsToKeep);
		this.removeCurrentPrice = removeCurrentPrice;
		this.maxSelection = maxSelection;
		this.needsSelection = needsSelection;
		this.allowSearch = allowSearch;
	}

	/**
	 * Create contents of the wizard.
	 * 
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout());
		partSearchComposite = new PartSearchComposite(container, SWT.NONE, SWT.BORDER | SWT.CHECK | SWT.FULL_SELECTION);
		partSearchComposite.hideToolItems(false, true, true);
		((GridData) partSearchComposite.compositeSideBar.getLayoutData()).exclude = !allowSearch;
		partSearchComposite.compositeSideBar.setVisible(allowSearch);
		partSearchComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		partSearchComposite.searchResultsTable.table.setMaxCheckedItems(maxSelection);
		partSearchComposite.searchResultsTable.table.addCheckListener(e -> {
			boolean isPageComplete = !needsSelection
					|| partSearchComposite.searchResultsTable.table.getCheckedCount() > 0;
			setPageComplete(isPageComplete);
		});

		if (removeCurrentPrice) {
			partSearchComposite.lblDenotesCurrentSale.setText("");
		}
		partSearchService.setComposite(partSearchComposite);

		setControl(container);
	}

	@Override
	public void setFocus() {
		partSearchComposite.txtPartName.setFocus();
	}

	public List<PartSearchRecord> getSelectedRecords() {
		return partSearchComposite.searchResultsTable.table.getCheckedData(PartSearchTableService.DATA_RECORD);
	}

	public void populateParts(List<PartSearchRecord> parts) {
		partSearchService.populateTable(parts);
	}

	public void populateParts(List<PartSearchRecord> parts, boolean selected) {
		partSearchService.populateTable(parts, selected);
	}

}
