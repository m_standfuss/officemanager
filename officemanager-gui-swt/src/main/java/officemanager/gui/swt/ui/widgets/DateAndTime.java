package officemanager.gui.swt.ui.widgets;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Listener;

import officemanager.gui.swt.AppPrefs;

public class DateAndTime extends Composite {
	private final DateTime dateTimeDate;
	private final DateTime dateTimeTime;

	public DateAndTime(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		dateTimeDate = new DateTime(this, SWT.BORDER);
		dateTimeTime = new DateTime(this, SWT.BORDER | SWT.TIME | SWT.SHORT);
	}

	public void setDate(Date date) {
		Calendar cal = Calendar.getInstance(AppPrefs.localTimeZone);
		cal.setTime(date);

		dateTimeDate.setYear(cal.get(Calendar.YEAR));
		dateTimeDate.setMonth(cal.get(Calendar.MONTH));
		dateTimeDate.setDay(cal.get(Calendar.DAY_OF_MONTH));

		dateTimeTime.setHours(cal.get(Calendar.HOUR_OF_DAY));
		dateTimeTime.setMinutes(cal.get(Calendar.MINUTE));
		dateTimeTime.setSeconds(cal.get(Calendar.SECOND));
	}

	public Date getDate() {
		Calendar cal = Calendar.getInstance(AppPrefs.localTimeZone);

		cal.set(dateTimeDate.getYear(), dateTimeDate.getMonth(), dateTimeDate.getDay(), dateTimeTime.getHours(),
				dateTimeTime.getMinutes(), dateTimeTime.getSeconds());
		return cal.getTime();
	}

	@Override
	public void setEnabled(boolean enabled) {
		dateTimeDate.setEnabled(enabled);
		dateTimeTime.setEnabled(enabled);
	}

	public void addDateChangeListener(Listener listener) {
		dateTimeDate.addListener(SWT.Selection, listener);
		dateTimeTime.addListener(SWT.Selection, listener);
	}
}
