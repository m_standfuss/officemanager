package officemanager.gui.swt.ui.wizards;

import officemanager.biz.delegates.PartDelegate;
import officemanager.biz.records.PartRecord;
import officemanager.gui.swt.services.LocationTreeService;
import officemanager.gui.swt.services.PartDetailsService;
import officemanager.gui.swt.ui.wizards.pages.LocationSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.PartDetailsPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.gui.swt.views.PartView;

public class PartWizard extends OfficeManagerWizard {

	private final PartDelegate partDelegate;

	private final LocationTreeService locationTreeService;
	private final PartDetailsService partDetailsService;

	private PartDetailsPage partDetailsPage;
	private LocationSelectionPage locationSelectionPage;

	private boolean saved;
	private Long partId;
	private PartRecord partInContext;

	public PartWizard() {
		this((Long) null);
	}

	public PartWizard(PartRecord part) {
		this(part.getPartId());
		partInContext = part;
	}

	public PartWizard(Long partId) {
		this.partId = partId;
		setWindowTitle(partId == null ? "New Part" : "Edit Part");

		partDelegate = new PartDelegate();
		locationTreeService = new LocationTreeService();
		partDetailsService = new PartDetailsService();
	}

	@Override
	public void addPages() {
		partDetailsPage = new PartDetailsPage();
		partDetailsPage.addFirstShowListener(e -> showPartDetails());
		addPage(partDetailsPage);

		locationSelectionPage = new LocationSelectionPage(false, false);
		locationSelectionPage.addFirstShowListener(e -> showLocations());
		addPage(locationSelectionPage);
	}

	@Override
	public OfficeManagerView getReturnView() {
		if (saved) {
			return new PartView(partId);
		}
		return null;
	}

	@Override
	public boolean performFinish() {
		if (!MessageDialogs.askQuestion(getShell(), "Save Part", "Would you like to save this part?")) {
			return false;
		}
		partDetailsService.populateRecord(partInContext);
		partInContext.setLocationId(locationTreeService.getCheckedLocationId());
		partId = partDelegate.insertOrUpdatePart(partInContext);
		partInContext.setPartId(partId);
		return saved = true;
	}

	private void showLocations() {
		locationTreeService.setComposite(locationSelectionPage.composite);
		locationTreeService.loadRootLocations();
		if (partInContext.getLocationId() != null) {
			locationTreeService.setLocationToSelect(partInContext.getLocationId());
		}
	}

	private void showPartDetails() {
		if (partInContext == null) {
			if (partId == null) {
				partInContext = new PartRecord();
			} else {
				partInContext = partDelegate.getPart(partId);
				if (partInContext == null) {
					partInContext = new PartRecord();
				}
			}
		}

		partDetailsService.setComposite(partDetailsPage.composite);
		partDetailsService.fillComposite(partInContext);
	}
}
