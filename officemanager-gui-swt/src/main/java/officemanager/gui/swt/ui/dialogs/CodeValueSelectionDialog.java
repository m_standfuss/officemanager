package officemanager.gui.swt.ui.dialogs;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import officemanager.biz.delegates.CodeValueDelegate;
import officemanager.biz.records.CodeValueRecord;
import officemanager.gui.swt.ui.tables.CodeValueTable;

public class CodeValueSelectionDialog extends Dialog {
	private final CodeValueDelegate codeValueDelegate;

	private Long codeSet;
	private String headerText;
	private List<CodeValueRecord> valuesToSelect;

	private Label lblHeader;
	private CodeValueTable codeValueTable;

	private List<CodeValueRecord> selectedResults;

	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public CodeValueSelectionDialog(Shell parentShell) {
		super(parentShell);
		codeValueDelegate = new CodeValueDelegate();
		headerText = "";
	}

	public void setCodeSet(Long codeSet) {
		this.codeSet = codeSet;
	}

	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}

	public void setValuesToSelect(List<CodeValueRecord> valuesToSelect) {
		this.valuesToSelect = valuesToSelect;
	}

	public List<CodeValueRecord> getSelectedValues() {
		return selectedResults;
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout());

		lblHeader = new Label(container, SWT.WRAP);
		GridData gd_lblHeader = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblHeader.widthHint = 300;
		lblHeader.setLayoutData(gd_lblHeader);
		lblHeader.setText(headerText);

		codeValueTable = new CodeValueTable(container);
		GridData gd_codeValueTable = new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1);
		gd_codeValueTable.heightHint = 200;
		gd_codeValueTable.widthHint = 300;
		codeValueTable.setLayoutData(gd_codeValueTable);
		loadTable();
		return container;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(339, 333);
	}

	@Override
	protected void okPressed() {
		selectedResults = codeValueTable.getCheckedCodeValueRecords();
		super.okPressed();
	}

	private void loadTable(){
		if(codeSet == null){
			throw new IllegalStateException("The code value to load for needs to be set.");
		}
		codeValueTable.populateList(codeValueDelegate.getActiveCodeValueRecords(codeSet));
		if (valuesToSelect != null) {
			codeValueTable.checkCodeValues(valuesToSelect);
		}
	}

}
