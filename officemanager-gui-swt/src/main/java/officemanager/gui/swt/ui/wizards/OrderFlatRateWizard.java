package officemanager.gui.swt.ui.wizards;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.wizards.pages.FlatRateSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.OrderDetailsPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;

public class OrderFlatRateWizard extends OfficeManagerWizard {

	private static final Logger logger = LogManager.getLogger(OrderFlatRateWizard.class);

	private final OrderDelegate orderDelegate;
	private final Long orderId;

	private OrderRecord record;
	private FlatRateSelectionPage flatRateSelectionPage;
	private OrderDetailsPage orderDetailsPage;

	public OrderFlatRateWizard(Long orderId) {
		logger.debug("Opening an order part wizard for orderId=" + orderId);
		checkNotNull(orderId);
		this.orderId = orderId;
		setWindowTitle("Flat Rate Management - Order#" + AppPrefs.FORMATTER.formatOrderNumber(orderId));

		orderDelegate = new OrderDelegate();
	}

	@Override
	public boolean canFinish() {
		return getContainer().getCurrentPage() == orderDetailsPage && orderDetailsPage.isPageComplete();
	}

	@Override
	public void addPages() {
		record = orderDelegate.loadOrderRecord(orderId);

		flatRateSelectionPage = new FlatRateSelectionPage(Integer.MAX_VALUE, false, false);
		flatRateSelectionPage.setMessage("Add new flat rates for the order.");
		addPage(flatRateSelectionPage);

		orderDetailsPage = new OrderDetailsPage(true, true, false, false);
		orderDetailsPage.addFirstShowListener(e -> populateOrderDetailsPage());
		orderDetailsPage.addIsShownListener(e -> updateOrderDetailsPage());
		addPage(orderDetailsPage);
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	@Override
	public boolean performFinish() {
		logger.debug("Starting preform finish.");
		if (!MessageDialogs.askQuestion(getShell(), "Continue",
				"Do you wish to save the order's flat rates services?")) {
			return false;
		}
		List<OrderFlatRateServiceRecord> services = orderDetailsPage.getFlatRates();

		logger.debug("Updating order details.");
		orderDelegate.insertOrUpdateFlatRates(orderId, services);

		List<PriceOverrideRecord> priceOverrides = orderDetailsPage.getPriceOverrides();
		logger.debug("Inserting " + priceOverrides.size() + " price overrides.");
		for (PriceOverrideRecord record : priceOverrides) {
			record.setOrderId(orderId);
			orderDelegate.insertPriceOverride(record);
		}

		MessageDialogs.displayMessage(getShell(), "Successfully Saved",
				"Order flat rate services were successfully saved.");
		return true;
	}

	private void populateOrderDetailsPage() {
		logger.debug(
				"Showing order details page for the first time populating with client products and existing parts.");
		orderDetailsPage.populateClientProducts(record.getClientProducts());
		orderDetailsPage.populateFlatRates(record.getFlatRates());
	}

	private void updateOrderDetailsPage() {
		logger.debug("Showing order details page, updating new part selections.");
		orderDetailsPage.populateFlatRateSearches(flatRateSelectionPage.getSelectedRecords());
	}
}
