package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class OrderPartTableComposite extends Composite {

	public static final int COL_PART_NAME = 0;
	public static final int COL_PART_MANUF = 1;
	public static final int COL_CLIENT_PRODUCT = 2;
	public static final int COL_QUANTITY = 3;
	public static final int COL_PRICE = 4;
	public static final int COL_TAX = 5;
	public static final int COL_TOTAL = 6;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnPartName;
	public final TableColumn tblclmnPartManuf;
	public final TableColumn tblclmnClientProduct;
	public final TableColumn tblclmnQuantity;
	public final TableColumn tblclmnPrice;
	public final TableColumn tblclmnTax;
	public final TableColumn tblclmnTotal;

	public OrderPartTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new FillLayout());
		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnPartName = new TableColumn(table, SWT.NONE);
		tblclmnPartName.setWidth(100);
		tblclmnPartName.setText("Part");

		tblclmnPartManuf = new TableColumn(table, SWT.NONE);
		tblclmnPartManuf.setWidth(100);
		tblclmnPartManuf.setText("Manufacturer");

		tblclmnClientProduct = new TableColumn(table, SWT.NONE);
		tblclmnClientProduct.setWidth(100);
		tblclmnClientProduct.setText("Client Product");

		tblclmnQuantity = new TableColumn(table, SWT.NONE);
		tblclmnQuantity.setWidth(100);
		tblclmnQuantity.setText("Quantity");

		tblclmnPrice = new TableColumn(table, SWT.NONE);
		tblclmnPrice.setWidth(100);
		tblclmnPrice.setText("Price");

		tblclmnTax = new TableColumn(table, SWT.NONE);
		tblclmnTax.setWidth(100);
		tblclmnTax.setText("Tax");

		tblclmnTotal = new TableColumn(table, SWT.NONE);
		tblclmnTotal.setWidth(100);
		tblclmnTotal.setText("Total");
	}
}
