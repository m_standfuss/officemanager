package officemanager.gui.swt.ui.wizards;

import officemanager.biz.delegates.PersonDelegate;
import officemanager.biz.delegates.PersonnelDelegate;
import officemanager.biz.records.PersonRecord;
import officemanager.biz.records.PersonnelRecord;
import officemanager.gui.swt.ui.wizards.pages.PersonnelInformationPage;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.gui.swt.views.PersonnelView;

public class PersonnelWizard extends PersonWizard {

	private final PersonnelDelegate personnelDelegate;

	private PersonnelRecord personnelInContext;
	private PersonnelInformationPage personnelInformationPage;

	public PersonnelWizard() {
		this(null);
	}

	public PersonnelWizard(Long personnelId) {
		setWindowTitle("Personnel Information");
		personnelDelegate = new PersonnelDelegate();
		if (personnelId != null) {
			personnelInContext = personnelDelegate.getPersonnelRecord(personnelId);
		} else {
			personnelInContext = new PersonnelRecord();
		}
	}

	@Override
	public PersonDelegate getPersonDelegate() {
		return personnelDelegate;
	}

	@Override
	public PersonRecord getPersonInContext() {
		return personnelInContext;
	}

	@Override
	public void addPages() {
		super.addPages();

		personnelInformationPage = new PersonnelInformationPage();
		personnelInformationPage.addFirstShowListener(l -> populatePersonnelPage());
		addPage(personnelInformationPage);
	}

	@Override
	public boolean performFinish() {
		populatePersonRecord();
		personnelInformationPage.populatePersonnelRecord(personnelInContext);
		Long personnelId = personnelDelegate.persistPersonnelRecord(personnelInContext);
		personnelInContext.setPersonnelId(personnelId);
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		if (personnelInContext == null || personnelInContext.getPersonnelId() == null) {
			return null;
		}
		return new PersonnelView(personnelInContext.getPersonnelId());
	}

	private void populatePersonnelPage() {
		personnelInformationPage.populatePersonnelTypes(personnelDelegate.getPersonnelTypes());
		personnelInformationPage.populatePersonnelStatus(personnelDelegate.getPersonnelStatus());
		personnelInformationPage.populatePersonnelInformation(personnelInContext);
	}
}
