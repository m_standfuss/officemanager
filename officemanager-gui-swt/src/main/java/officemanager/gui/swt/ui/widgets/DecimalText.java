package officemanager.gui.swt.ui.widgets;

import static com.google.common.base.Preconditions.checkArgument;

import java.math.BigDecimal;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class DecimalText extends Text {
	private int numbersAfterDecimal;

	public DecimalText(Composite parent, int style) {
		this(0, parent, style);
	}

	public DecimalText(int numbersAfterDecimal, Composite parent, int style) {
		super(parent, style);
		this.numbersAfterDecimal = numbersAfterDecimal;

		addVerifyListener(e -> verifyEvent(e));
	}

	public void setValue(BigDecimal decimal) {
		if (decimal == null) {
			super.setText("");
			return;
		}
		String value = decimal.toPlainString();
		if (value.contains(".")) {
			int index = value.indexOf('.');
			if (value.length() - index - 1 > numbersAfterDecimal) {
				value = value.substring(0, index + numbersAfterDecimal + 1);
			}
		}
		setText(value);
	}

	public BigDecimal getValue() {
		String text = getText().trim();
		if (text.isEmpty() || text.equals(".")) {
			return null;
		}
		return new BigDecimal(text);
	}

	public void setNumbersAfterDecimal(int numbersAfterDecimal) {
		checkArgument(numbersAfterDecimal >= 0);
		this.numbersAfterDecimal = numbersAfterDecimal;
	}

	private void verifyEvent(VerifyEvent e) {
		e.text = e.text.trim();
		if (e.text.isEmpty()) {
			return;
		}
		if (e.text.equals(".")) {
			return;
		}
		String newText = getText().substring(0, e.start) + e.text + getText().substring(e.start);
		try {
			new BigDecimal(newText);
		} catch (final NumberFormatException excep) {
			e.doit = false;
			return;
		}

		if (newText.contains(".")) {
			if (newText.length() - newText.indexOf('.') > numbersAfterDecimal + 1) {
				e.doit = false;
			}
		}
	}

	@Override
	protected void checkSubclass() {}
}
