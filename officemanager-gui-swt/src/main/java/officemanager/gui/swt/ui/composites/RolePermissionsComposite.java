package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class RolePermissionsComposite extends Composite {

	public final Label lblRoleName;
	public final Composite compositePermissions;
	public final ScrolledComposite scrolledComposite;
	public final UpperCaseText txtRoleName;
	public Label lblRolesCanBe;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public RolePermissionsComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		lblRoleName = new Label(this, SWT.NONE);
		lblRoleName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRoleName.setText("Role Name:");
		lblRoleName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtRoleName = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtRoleName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtRoleName.widthHint = 175;
		txtRoleName.setLayoutData(gd_txtRoleName);
		txtRoleName.setTextLimit(255);

		scrolledComposite = new ScrolledComposite(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gd_scrolledComposite = new GridData(SWT.FILL, SWT.FILL, false, true, 2, 1);
		gd_scrolledComposite.widthHint = 225;
		scrolledComposite.setLayoutData(gd_scrolledComposite);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		compositePermissions = new Composite(scrolledComposite, SWT.NONE);
		compositePermissions.setLayout(new GridLayout(1, false));
		scrolledComposite.setContent(compositePermissions);
		scrolledComposite.setMinSize(compositePermissions.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		lblRolesCanBe = new Label(this, SWT.NONE);
		lblRolesCanBe.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblRolesCanBe.setText("Roles can be assigned to users on their individual information page.");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
