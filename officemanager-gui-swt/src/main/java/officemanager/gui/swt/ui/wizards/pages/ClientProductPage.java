package officemanager.gui.swt.ui.wizards.pages;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.Lists;

import officemanager.biz.records.ClientProductRecord;
import officemanager.biz.records.ClientProductSelectionRecord;
import officemanager.biz.records.CodeValueRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.ClientProductService;
import officemanager.gui.swt.ui.composites.ButtonBarComposite;
import officemanager.gui.swt.ui.composites.ClientProductEditComposite;
import officemanager.gui.swt.ui.tables.ClientProductTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.Tuple;

public class ClientProductPage extends OfficeManagerWizardPage {

	private final List<Tuple<ClientProductRecord, ClientProductSelectionRecord>> recordsToAdd;
	private final ClientProductService clientProductService;

	private final boolean allowSelection;

	private ClientProductTableComposite tblComposite;
	private ClientProductEditComposite editComposite;

	/**
	 * Create the wizard.
	 */
	public ClientProductPage() {
		this(false);
	}

	public ClientProductPage(boolean allowSelection) {
		super("ClientProducts", "Client Products", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.PRODUCT, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		if (allowSelection) {
			setDescription("Please select client products.");
		} else {
			setDescription("Enter client products.");
		}
		setPageComplete(true);
		recordsToAdd = Lists.newArrayList();
		clientProductService = new ClientProductService();

		this.allowSelection = allowSelection;
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		Composite root = new Composite(scrCompositeSideBar, SWT.NONE);
		root.setLayout(new GridLayout());

		CollapsibleComposite clpseCmp = new CollapsibleComposite(root, SWT.NONE);
		clpseCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		clpseCmp.setTitleText("New Client Product");

		clpseCmp.composite.setLayout(new GridLayout());
		editComposite = new ClientProductEditComposite(clpseCmp.composite, SWT.NONE);
		editComposite.toolBar.dispose();

		ButtonBarComposite btnBar = new ButtonBarComposite(clpseCmp.composite, SWT.NONE);
		btnBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		btnBar.btnSave.addListener(SWT.Selection, l -> addNewProduct());
		btnBar.btnClear.addListener(SWT.Selection, l -> clear());

		tblComposite = new ClientProductTableComposite(root, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION | (allowSelection ? SWT.CHECK : SWT.NONE));
		GridData gd_phoneTbl = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_phoneTbl.heightHint = 200;
		tblComposite.setLayoutData(gd_phoneTbl);

		scrCompositeSideBar.setContent(root);
		scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrCompositeSideBar);

		clpseCmp.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT)));

		clientProductService.setComposite(tblComposite);
	}

	@Override
	public void setFocus() {
		editComposite.cmbProductType.setFocus();
	}

	public void populateProductTypes(List<CodeValueRecord> productTypes) {
		editComposite.cmbProductType.populateSelections(productTypes);
	}

	public void populateProductStatus(List<CodeValueRecord> productStatus) {
		editComposite.cmbProductStatus.populateSelections(productStatus);
	}

	public List<ClientProductRecord> getProductsToAdd() {
		List<ClientProductRecord> records = Lists.newArrayList();
		for (Tuple<ClientProductRecord, ClientProductSelectionRecord> tuple : recordsToAdd) {
			records.add(tuple.item1);
		}
		return records;
	}

	public List<Tuple<ClientProductRecord, ClientProductSelectionRecord>> getProductsAndSelectionsToAdd() {
		return recordsToAdd;
	}

	public List<ClientProductSelectionRecord> getSelectedProducts() {
		return clientProductService.getCheckedRecords();
	}

	public void populateClientProducts(List<ClientProductSelectionRecord> clientProducts) {
		clientProductService.populateTable(clientProducts);
	}

	private void addNewProduct() {
		if (!MessageDialogs.askQuestion(getShell(), "Add Client Product",
				"Are you sure you wish to add the new client product?")) {
			return;
		}
		if (editComposite.cmbProductType.getCodeValueRecord() == null) {
			MessageDialogs.displayError(getShell(), "Cannot Add", "Please select a product type.");
			return;
		}
		if (editComposite.txtManufacturer.getText().isEmpty()) {
			MessageDialogs.displayError(getShell(), "Cannot Add", "Please enter manufacturer information.");
			return;
		}

		ClientProductRecord record = new ClientProductRecord();
		record.setBarcode(editComposite.txtBarcode.getText());
		record.setManufacturer(editComposite.txtManufacturer.getText());
		record.setModel(editComposite.txtModel.getText());
		record.setProductStatus(editComposite.cmbProductStatus.getCodeValueRecord());
		record.setProductType(editComposite.cmbProductType.getCodeValueRecord());
		record.setSerialNum(editComposite.txtSerialNumber.getText());
		record.setWorkCount(0);

		ClientProductSelectionRecord selectionRecord = new ClientProductSelectionRecord();
		selectionRecord.setProductManufacturer(editComposite.txtManufacturer.getText());
		selectionRecord.setProductModel(editComposite.txtModel.getText());
		selectionRecord.setProductType(editComposite.cmbProductType.getText());
		selectionRecord.setSerialNumber(editComposite.txtSerialNumber.getText());
		clientProductService.addRecord(selectionRecord, allowSelection);
		recordsToAdd.add(Tuple.newTuple(record, selectionRecord));
		clear();
	}

	private void clear() {
		editComposite.cmbProductStatus.deselectAll();
		editComposite.cmbProductType.deselectAll();

		editComposite.txtBarcode.setText("");
		editComposite.txtManufacturer.setText("");
		editComposite.txtModel.setText("");
		editComposite.txtSerialNumber.setText("");
		editComposite.txtWorkCnt.setText("");
	}
}
