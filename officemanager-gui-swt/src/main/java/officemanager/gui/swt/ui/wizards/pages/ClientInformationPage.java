package officemanager.gui.swt.ui.wizards.pages;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.records.ClientRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.ClientEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ClientInformationPage extends OfficeManagerWizardPage {

	public ClientEditComposite composite;

	/**
	 * Create the wizard.
	 */
	public ClientInformationPage() {
		super("ClientInformation", "Client Information", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.USER_EDIT, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setPageComplete(true);
	}

	@Override
	public void createControl(Composite parent) {
		composite = new ClientEditComposite(parent, SWT.NULL);
		composite.btnPromotionalClub.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_PROMO_CLUB));
		composite.btnTaxExempt.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_TAX_EXEMPT));
		setControl(composite);
	}

	@Override
	public void setFocus() {
		composite.txtAttentionOf.setFocus();
	}

	public void populateClientInformation(ClientRecord clientRecord) {
		composite.btnPromotionalClub.setSelection(clientRecord.isPromotionClub());
		composite.btnTaxExempt.setSelection(clientRecord.isTaxExempt());
		composite.txtAttentionOf.setText(clientRecord.getAttentionOf());
		composite.txtJoinedOn.setText(AppPrefs.dateFormatter.formatDate_DB(clientRecord.getJoinedOn()));
	}

	public void populateClientRecord(ClientRecord record) {
		record.setPromotionClub(composite.btnPromotionalClub.getSelection());
		record.setTaxExempt(composite.btnTaxExempt.getSelection());
		record.setAttentionOf(composite.txtAttentionOf.getText());
	}
}
