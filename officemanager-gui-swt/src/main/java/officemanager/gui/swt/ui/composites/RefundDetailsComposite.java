package officemanager.gui.swt.ui.composites;

import java.math.BigDecimal;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ResourceManager;

public class RefundDetailsComposite extends Composite {
	public final Composite cmpComponents;
	public final ScrolledComposite scrolledComposite;
	public final Label lblPartsHeader;
	public final Label lblServicesHeader;
	public final Composite cmpParts;
	public final Composite cmpServices;
	public final Label lblPart;
	public final Label lblPartPriceHeader;
	public final Label lblQnt;
	public final Label lblServiceNameHeader;
	public final Label lblServicePriceHeader;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public RefundDetailsComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		setLayout(gridLayout);

		scrolledComposite = new ScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		cmpComponents = new Composite(scrolledComposite, SWT.NONE);
		cmpComponents.setLayout(new GridLayout(1, false));

		lblPartsHeader = new Label(cmpComponents, SWT.NONE);
		lblPartsHeader.setText("Parts");
		lblPartsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmpParts = new Composite(cmpComponents, SWT.NONE);
		cmpParts.setLayout(new GridLayout(3, false));

		lblPart = new Label(cmpParts, SWT.NONE);
		lblPart.setText("Part");
		lblPart.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));

		lblPartPriceHeader = new Label(cmpParts, SWT.NONE);
		lblPartPriceHeader.setText("Price");
		lblPartPriceHeader.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));

		lblQnt = new Label(cmpParts, SWT.NONE);
		lblQnt.setText("Qnt.");
		lblQnt.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));

		lblServicesHeader = new Label(cmpComponents, SWT.NONE);
		lblServicesHeader.setText("Services");
		lblServicesHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmpServices = new Composite(cmpComponents, SWT.NONE);
		cmpServices.setLayout(new GridLayout(2, false));

		lblServiceNameHeader = new Label(cmpServices, SWT.NONE);
		lblServiceNameHeader.setText("Service");
		lblServiceNameHeader.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));

		lblServicePriceHeader = new Label(cmpServices, SWT.NONE);
		lblServicePriceHeader.setText("Price");
		lblServicePriceHeader.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));

		scrolledComposite.setContent(cmpComponents);
		scrolledComposite.setMinSize(cmpComponents.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	public PartItem addPart(String partName, BigDecimal partPrice, Long partQuantityMax) {
		PartItem item = new PartItem(partName, partQuantityMax, partPrice);
		scrolledComposite.setMinSize(cmpComponents.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		return item;
	}

	public ServiceItem addService(String serviceName, BigDecimal servicePrice) {
		ServiceItem item = new ServiceItem(serviceName, servicePrice);
		scrolledComposite.setMinSize(cmpComponents.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		return item;
	}

	@Override
	protected void checkSubclass() {}

	public class ServiceItem {
		public final Label lblServiceName;
		public final Label lblServicePrice;

		private Object data;

		public ServiceItem(String serviceName, BigDecimal servicePrice) {
			lblServiceName = new Label(cmpServices, SWT.NONE);
			lblServiceName.setText(serviceName);

			lblServicePrice = new Label(cmpServices, SWT.NONE);
			lblServicePrice.setText(AppPrefs.FORMATTER.formatMoney(servicePrice));
		}

		public void dispose() {
			lblServiceName.dispose();
			lblServicePrice.dispose();
			scrolledComposite.setMinSize(cmpComponents.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}

		public void setData(Object data) {
			this.data = data;
		}

		public Object getData() {
			return data;
		}
	}

	public class PartItem {
		public final Label lblPartname;
		public final Spinner spnPartQuantity;
		public final Label lblPartPrice;

		private Object data;

		public PartItem(String partName, Long maxPartQuantity, BigDecimal partPrice) {
			lblPartname = new Label(cmpParts, SWT.NONE);
			lblPartname.setText(partName);

			lblPartPrice = new Label(cmpParts, SWT.NONE);
			lblPartPrice.setText(AppPrefs.FORMATTER.formatMoney(partPrice));

			spnPartQuantity = new Spinner(cmpParts, SWT.BORDER);
			spnPartQuantity.setMinimum(1);
			spnPartQuantity.setMaximum(maxPartQuantity.intValue());
			spnPartQuantity.setSelection(1);
			spnPartQuantity.setIncrement(1);
		}

		public void dispose() {
			lblPartname.dispose();
			spnPartQuantity.dispose();
			lblPartPrice.dispose();
			scrolledComposite.setMinSize(cmpComponents.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}

		public void setData(Object data) {
			this.data = data;
		}

		public Object getData() {
			return data;
		}
	}
}
