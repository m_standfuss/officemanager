package officemanager.gui.swt.ui.wizards;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class OfficeManagerWizardDialog extends WizardDialog {

	private static final Logger logger = LogManager.getLogger(OfficeManagerWizardDialog.class);

	public OfficeManagerWizardDialog(Shell parentShell, OfficeManagerWizard newWizard) {
		super(parentShell, newWizard);
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setImage(ResourceManager.getIcon());
	}

	@Override
	public void showPage(IWizardPage page) {
		try {
			super.showPage(page);
		} catch (Exception e) {
			logger.error("Unexpected error occured during showing of next page.", e);
			MessageDialogs.displayError(getShell(), "Error Occured",
					"An unexpected error occured. Please refer to the log file for more details.");
			getShell().close();
		}
	}

}
