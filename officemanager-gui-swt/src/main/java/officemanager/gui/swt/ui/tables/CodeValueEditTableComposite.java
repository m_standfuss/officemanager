package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class CodeValueEditTableComposite extends Composite {

	public static final int COL_DISPLAY = 0;
	public static final int COL_CODE_VALUE = 1;
	public static final int COL_CDF_MEANING = 2;
	public static final int COL_ACTIVE = 3;
	public static final int COL_UPDT_DTTM = 4;
	public static final int COL_EDIT = 5;
	public static final int COL_ACTIVATE = 6;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnDisplay;
	public final TableColumn tblclmnCodeValue;
	public final TableColumn tblclmnCdfMeaning;
	public final TableColumn tblclmnActive;
	public final TableColumn tblclmnUpdtDttm;
	public final TableColumn tblclmEdit;
	public final TableColumn tblclmActivate;

	public CodeValueEditTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new FillLayout());

		table = new OfficeManagerTable(this, tableStyle | OfficeManagerTable.NO_EXTEND_LAST_COLUMN);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnDisplay = new TableColumn(table, SWT.NONE);
		tblclmnDisplay.setWidth(100);
		tblclmnDisplay.setText("Display");

		tblclmnCodeValue = new TableColumn(table, SWT.NONE);
		tblclmnCodeValue.setWidth(100);
		tblclmnCodeValue.setText("Coded Value");

		tblclmnCdfMeaning = new TableColumn(table, SWT.NONE);
		tblclmnCdfMeaning.setWidth(100);
		tblclmnCdfMeaning.setText("Coded Meaning");

		tblclmnActive = new TableColumn(table, SWT.NONE);
		tblclmnActive.setWidth(100);
		tblclmnActive.setText("Active");

		tblclmnUpdtDttm = new TableColumn(table, SWT.NONE);
		tblclmnUpdtDttm.setWidth(100);
		tblclmnUpdtDttm.setText("Last Updated");

		tblclmEdit = new TableColumn(table, SWT.NONE);
		tblclmEdit.setWidth(100);
		tblclmEdit.setText("");

		tblclmActivate = new TableColumn(table, SWT.NONE);
		tblclmActivate.setWidth(100);
		tblclmActivate.setText("");
	}

	@Override
	protected void checkSubclass() {}
}
