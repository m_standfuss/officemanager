package officemanager.gui.swt.ui.wizards.pages;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import com.google.common.collect.Lists;

public abstract class OfficeManagerWizardPage extends WizardPage {

	private final List<Listener> firstShowListeners;
	private final List<Listener> isShownListeners;
	private boolean hasShown;

	protected OfficeManagerWizardPage(String pageName) {
		this(pageName, null, null);
	}

	protected OfficeManagerWizardPage(String pageName, String title, ImageDescriptor titleImage) {
		super(pageName, title, titleImage);
		firstShowListeners = Lists.newArrayList();
		isShownListeners = Lists.newArrayList();
	}

	public void setFocus() {}

	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			if (!hasShown) {
				fireFirstShowingListeners();
				hasShown = true;
			}
			fireIsShownListeners();
		}
		super.setVisible(visible);
		if (visible) {
			setFocus();
		}
	}

	public void addFirstShowListener(Listener l) {
		firstShowListeners.add(l);
	}

	public void addIsShownListener(Listener l) {
		isShownListeners.add(l);
	}

	private void fireFirstShowingListeners() {
		for (Listener l : firstShowListeners) {
			Event event = new Event();
			l.handleEvent(event);
		}
	}

	private void fireIsShownListeners() {
		for (Listener l : isShownListeners) {
			Event event = new Event();
			l.handleEvent(event);
		}
	}
}
