package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class OrderServiceTableComposite extends Composite {

	public static final int COL_SERVICE_NAME = 0;
	public static final int COL_CLIENT_PRODUCT = 1;
	public static final int COL_PRICE = 2;
	public static final int COL_HOURS = 3;
	public static final int COL_TOTAL = 4;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnServiceName;
	public final TableColumn tblclmnClientProduct;
	public final TableColumn tblclmnPrice;
	public final TableColumn tblclmnHours;
	public final TableColumn tblclmnTotal;

	public OrderServiceTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new FillLayout());
		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnServiceName = new TableColumn(table, SWT.NONE);
		tblclmnServiceName.setWidth(100);
		tblclmnServiceName.setText("Service");

		tblclmnClientProduct = new TableColumn(table, SWT.NONE);
		tblclmnClientProduct.setWidth(100);
		tblclmnClientProduct.setText("Client Product");

		tblclmnPrice = new TableColumn(table, SWT.NONE);
		tblclmnPrice.setWidth(100);
		tblclmnPrice.setText("Price/Hour");

		tblclmnHours = new TableColumn(table, SWT.NONE);
		tblclmnHours.setWidth(100);
		tblclmnHours.setText("Hours");

		tblclmnTotal = new TableColumn(table, SWT.NONE);
		tblclmnTotal.setWidth(100);
		tblclmnTotal.setText("Total");
	}
}
