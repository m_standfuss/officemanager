package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderTableComposite;
import officemanager.gui.swt.ui.tables.PriceOverrideTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class TotalsComposite extends Composite {
	public final Composite cmpDateRange;
	public final Label lblFrom;
	public final DateTime dateTimeStart;
	public final Label lblTo;
	public final DateTime dateTimeEnd;

	public final ScrolledComposite scrCompositeCenter;
	public final Composite compositeCenter;

	public final PaymentsTotalsComposite paymentsTotalComposite;

	public final CollapsibleComposite clpCmpOverriddenPrices;
	public final PriceOverrideTableComposite tblCmpPriceOverrides;

	public final CollapsibleComposite clpCmpOrdersOpened;
	public final OrderTableComposite tblOrdersOpened;

	public final CollapsibleComposite clpCmpOrdersClosed;
	public final OrderTableComposite tblOrdersClosed;
	public final ToolBar tlBarPrint;
	public final ToolItem tlItmPrint;
	public final Composite cmpClosedTotals;
	public final Label lblTotalHeader;
	public final Label lblTotal;
	public final Label lblLaborTotalHeader;
	public final Label lblLaborTotal;
	public final Label lblMerchTotalHeader;
	public final Label lblMerchTotal;
	public final Label lblTaxTotalHeader;
	public final Label lblTaxExemptTotalHeader;
	public final Label lblTaxTotal;
	public final Label lblTaxExemptTotal;
	public final Label lblMerchTotalTaxExemptHeader;
	public final Label lblMerchTotalTaxExempt;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public TotalsComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, true));

		tlBarPrint = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		tlItmPrint = new ToolItem(tlBarPrint, SWT.NONE);
		tlItmPrint.setToolTipText("Print Report");
		tlItmPrint.setWidth(16);
		tlItmPrint.setImage(ResourceManager.getImage(ImageFile.PRINT, AppPrefs.ICN_SIZE_PAGETOOLITEM));

		cmpDateRange = new Composite(this, SWT.NONE);
		cmpDateRange.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cmpDateRange.setLayout(new GridLayout(4, false));

		lblFrom = new Label(cmpDateRange, SWT.NONE);
		lblFrom.setText("From");

		dateTimeStart = new DateTime(cmpDateRange, SWT.BORDER | SWT.DROP_DOWN);

		lblTo = new Label(cmpDateRange, SWT.NONE);
		lblTo.setText("To");

		dateTimeEnd = new DateTime(cmpDateRange, SWT.BORDER | SWT.DROP_DOWN);

		scrCompositeCenter = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrCompositeCenter.setExpandHorizontal(true);
		scrCompositeCenter.setExpandVertical(true);

		compositeCenter = new Composite(scrCompositeCenter, SWT.NONE);
		compositeCenter.setLayout(new GridLayout(1, true));

		paymentsTotalComposite = new PaymentsTotalsComposite(compositeCenter, SWT.NONE);
		paymentsTotalComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		paymentsTotalComposite.clpCmpPayments.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		clpCmpOverriddenPrices = new CollapsibleComposite(compositeCenter, SWT.NONE);
		clpCmpOverriddenPrices.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		clpCmpOverriddenPrices.setTitleText("Overriden Prices");
		clpCmpOverriddenPrices.setTitleImage(ResourceManager.getImage(ImageFile.SALE, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpOverriddenPrices.composite.setLayout(new GridLayout());
		clpCmpOverriddenPrices.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		tblCmpPriceOverrides = new PriceOverrideTableComposite(clpCmpOverriddenPrices.composite, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION);
		tblCmpPriceOverrides.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		clpCmpOrdersOpened = new CollapsibleComposite(compositeCenter, SWT.NONE);
		clpCmpOrdersOpened.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		clpCmpOrdersOpened.setTitleText("Orders Opened");
		clpCmpOrdersOpened.setTitleImage(ResourceManager.getImage(ImageFile.ORDER, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpOrdersOpened.composite.setLayout(new GridLayout());
		clpCmpOrdersOpened.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		tblOrdersOpened = new OrderTableComposite(clpCmpOrdersOpened.composite, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION);
		tblOrdersOpened.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		clpCmpOrdersClosed = new CollapsibleComposite(compositeCenter, SWT.NONE);
		clpCmpOrdersClosed.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		clpCmpOrdersClosed.setTitleText("Orders Closed");
		clpCmpOrdersClosed.setTitleImage(ResourceManager.getImage(ImageFile.ORDER, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpOrdersClosed.composite.setLayout(new GridLayout());
		clpCmpOrdersClosed.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		cmpClosedTotals = new Composite(clpCmpOrdersClosed.composite, SWT.NONE);
		cmpClosedTotals.setLayout(new GridLayout(6, false));

		lblTotalHeader = new Label(cmpClosedTotals, SWT.NONE);
		lblTotalHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTotalHeader.setText("Total:");
		lblTotalHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblTotal = new Label(cmpClosedTotals, SWT.NONE);

		lblTaxTotalHeader = new Label(cmpClosedTotals, SWT.NONE);
		lblTaxTotalHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTaxTotalHeader.setText("Tax:");
		lblTaxTotalHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblTaxTotal = new Label(cmpClosedTotals, SWT.NONE);

		lblMerchTotalHeader = new Label(cmpClosedTotals, SWT.NONE);
		lblMerchTotalHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMerchTotalHeader.setText("Merch:");
		lblMerchTotalHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblMerchTotal = new Label(cmpClosedTotals, SWT.NONE);

		lblLaborTotalHeader = new Label(cmpClosedTotals, SWT.NONE);
		lblLaborTotalHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLaborTotalHeader.setText("Labor:");
		lblLaborTotalHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblLaborTotal = new Label(cmpClosedTotals, SWT.NONE);

		lblTaxExemptTotalHeader = new Label(cmpClosedTotals, SWT.NONE);
		lblTaxExemptTotalHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTaxExemptTotalHeader.setText("Tax Exempt:");
		lblTaxExemptTotalHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblTaxExemptTotal = new Label(cmpClosedTotals, SWT.NONE);

		lblMerchTotalTaxExemptHeader = new Label(cmpClosedTotals, SWT.NONE);
		lblMerchTotalTaxExemptHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMerchTotalTaxExemptHeader.setText("Merch Tax Exempt:");
		lblMerchTotalTaxExemptHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblMerchTotalTaxExempt = new Label(cmpClosedTotals, SWT.NONE);

		tblOrdersClosed = new OrderTableComposite(clpCmpOrdersClosed.composite, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION);
		tblOrdersClosed.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		scrCompositeCenter.setContent(compositeCenter);
		scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	}

	@Override
	public void layout() {
		paymentsTotalComposite.layout();
		clpCmpOverriddenPrices.layout();
		clpCmpOrdersOpened.layout();
		clpCmpOrdersClosed.layout();
		super.layout();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
