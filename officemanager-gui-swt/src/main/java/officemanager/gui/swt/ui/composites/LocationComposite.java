package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.LocationTableComposite;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class LocationComposite extends Composite {
	public ToolBar toolBar;
	public ToolItem tltmEditLocation;
	public ToolItem tltmAddParts;
	public ToolItem tltmRemoveLocation;

	public ScrolledComposite scrolledComposite;
	public Composite composite;
	public Composite compositeInformation;
	public Label lblNameHeader;
	public Label lblAbbreviatedNameHeader;
	public Label lblTypeHeader;
	public Label lblParentLocationHeader;
	public Label lblName;
	public Label lblType;
	public Label lblAbbreviatedName;
	public Label lblParentLocation;

	public CollapsibleComposite cllpCmpChldLocs;
	public LocationTableComposite childrenLocationTable;

	public CollapsibleComposite cllpCmpParts;
	public PartSearchTableComposite partTblCmp;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public LocationComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		setLayout(new GridLayout());

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		tltmEditLocation = new ToolItem(toolBar, SWT.NONE);
		tltmEditLocation.setToolTipText("Edit Location");
		tltmEditLocation.setImage(ResourceManager.getImage(ImageFile.CRATE_EDIT, AppPrefs.ICN_SIZE_PAGETOOLITEM));

		tltmAddParts = new ToolItem(toolBar, SWT.NONE);
		tltmAddParts.setToolTipText("Add/Remove Parts");
		tltmAddParts.setImage(ResourceManager.getImage(ImageFile.PART_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));

		tltmRemoveLocation = new ToolItem(toolBar, SWT.NONE);
		tltmRemoveLocation.setToolTipText("Delete Location");
		tltmRemoveLocation.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));

		scrolledComposite = new ScrolledComposite(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		composite = new Composite(scrolledComposite, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));

		compositeInformation = new Composite(composite, SWT.NONE);
		compositeInformation.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		compositeInformation.setLayout(new GridLayout(4, false));

		lblNameHeader = new Label(compositeInformation, SWT.NONE);
		lblNameHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNameHeader.setText("Name:");
		lblNameHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblName = new Label(compositeInformation, SWT.NONE);

		lblAbbreviatedNameHeader = new Label(compositeInformation, SWT.NONE);
		lblAbbreviatedNameHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAbbreviatedNameHeader.setText("Abbreviated Name:");
		lblAbbreviatedNameHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblAbbreviatedName = new Label(compositeInformation, SWT.NONE);

		lblTypeHeader = new Label(compositeInformation, SWT.NONE);
		lblTypeHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTypeHeader.setText("Type:");
		lblTypeHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblType = new Label(compositeInformation, SWT.NONE);

		lblParentLocationHeader = new Label(compositeInformation, SWT.NONE);
		lblParentLocationHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblParentLocationHeader.setText("Parent Location:");
		lblParentLocationHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblParentLocation = new Label(compositeInformation, SWT.NONE);

		cllpCmpChldLocs = new CollapsibleComposite(composite, SWT.NONE);
		cllpCmpChldLocs.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cllpCmpChldLocs.setTitleText("Child Locations");
		cllpCmpChldLocs.setTitleImage(ResourceManager.getImage(ImageFile.CRATE, AppPrefs.ICN_SIZE_CLPCMP));
		cllpCmpChldLocs.addCollapseListener(l -> {
			layout();
			scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		});

		childrenLocationTable = new LocationTableComposite(cllpCmpChldLocs.composite, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION);

		cllpCmpParts = new CollapsibleComposite(composite, SWT.NONE);
		cllpCmpParts.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cllpCmpParts.setTitleText("Parts");
		cllpCmpParts.setTitleImage(ResourceManager.getImage(ImageFile.PART, AppPrefs.ICN_SIZE_CLPCMP));
		cllpCmpParts.addCollapseListener(l -> {
			layout();
			scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		});

		partTblCmp = new PartSearchTableComposite(cllpCmpParts.composite, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION);

		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
