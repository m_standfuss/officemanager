package officemanager.gui.swt.ui.widgets;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.google.common.base.Objects;
import com.google.common.collect.Maps;

public class HeaderValueCombo<T> extends HeaderValue<T> {

	private static final int WIDTH_DEFAULT = 150;

	private final Map<Integer, T> indexMap;

	private Combo cmbValue;

	public HeaderValueCombo(Composite parent, int style) {
		super(parent, style);
		indexMap = Maps.newHashMap();
	}

	public void populateChoices(List<T> choices) {
		cmbValue.removeAll();
		indexMap.clear();

		int i = 0;
		for (T choice : choices) {
			cmbValue.add(getDisplay(choice));
			indexMap.put(i, choice);
			i++;
		}
	}

	@Override
	public T getEditableValue() {
		int currentSelection = cmbValue.getSelectionIndex();
		return indexMap.get(currentSelection);
	}

	@Override
	protected void setEditableValue(T value) {
		boolean found = false;
		for (int i = 0; i < cmbValue.getItemCount(); i++) {
			Object key = indexMap.get(i);
			if (Objects.equal(key, value)) {
				cmbValue.select(i);
				found = true;
				break;
			}
		}

		if (!found) {
			cmbValue.select(-1);
		}
	}

	@Override
	protected int getEditableWidth() {
		return WIDTH_DEFAULT;
	}

	@Override
	protected Control getEditableControl(Composite parent) {
		cmbValue = new Combo(parent, SWT.READ_ONLY);
		return cmbValue;
	}
}
