package officemanager.gui.swt.ui.wizards.pages;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.biz.records.ClientProductSelectionRecord;
import officemanager.biz.records.FlatRateSearchRecord;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.biz.records.PartSearchRecord;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.biz.records.PriceOverrideRecord.EntityName;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.ClientProductService;
import officemanager.gui.swt.ui.composites.OrderDetailsComposite;
import officemanager.gui.swt.ui.dialogs.PriceOverrideDialog;
import officemanager.gui.swt.ui.tables.ClientProductTableComposite;
import officemanager.gui.swt.ui.widgets.NumberText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;

public class OrderDetailsPage extends OfficeManagerWizardPage {

	private static final String DATA_PRODUCT_SELECTION = "DATA_PRODUCT_SELECTION";

	private static final List<Integer> COLUMNS_TO_KEEP = Lists.newArrayList(
			ClientProductTableComposite.COL_MANUFACTURER, ClientProductTableComposite.COL_MODEL,
			ClientProductTableComposite.COL_PRODUCT, ClientProductTableComposite.COL_SERIAL_NUM);

	private final ClientProductService clientProductService;

	private final List<PartItem> currentParts;
	private final List<FlatRateItem> currentFlatRates;
	private final List<ServiceItem> currentServices;

	private final boolean showProducts;
	private final boolean showFlatRates;
	private final boolean showParts;
	private final boolean showServices;

	private OrderDetailsComposite composite;

	public OrderDetailsPage() {
		this(true, true, true, true);
	}

	public OrderDetailsPage(boolean showProducts, boolean showFlatRates, boolean showParts, boolean showServices) {
		super("orderDetailsPage", "Order Details", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.ORDER_EDIT, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("Fill out details for the order.");
		clientProductService = new ClientProductService(COLUMNS_TO_KEEP);

		currentParts = Lists.newArrayList();
		currentFlatRates = Lists.newArrayList();
		currentServices = Lists.newArrayList();

		this.showProducts = showProducts;
		this.showFlatRates = showFlatRates;
		this.showParts = showParts;
		this.showServices = showServices;
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);
		composite = new OrderDetailsComposite(scrCompositeSideBar, SWT.NONE);

		customizeComposite();

		scrCompositeSideBar.setContent(composite);
		scrCompositeSideBar.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrCompositeSideBar);

		composite.btnAddProductFor.addListener(SWT.Selection, e -> addProductForAll());
		composite.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));

		clientProductService.setComposite(composite.clientProductTableComposite);
	}

	public List<ClientProductSelectionRecord> getClientProducts() {
		return clientProductService.getRecords();
	}

	public List<OrderPartRecord> getParts() {
		List<OrderPartRecord> records = Lists.newArrayList();
		for (PartItem item : currentParts) {
			records.add(item.getOrderPartRecord());
		}
		return records;
	}

	public List<OrderFlatRateServiceRecord> getFlatRates() {
		List<OrderFlatRateServiceRecord> flatRates = Lists.newArrayList();
		for (FlatRateItem item : currentFlatRates) {
			flatRates.add(item.getOrderFlatRateServiceRecord());
		}
		return flatRates;
	}

	public List<OrderServiceRecord> getServices() {
		List<OrderServiceRecord> services = Lists.newArrayList();
		for (ServiceItem item : currentServices) {
			services.add(item.getOrderServiceRecord());
		}
		return services;
	}

	public List<PriceOverrideRecord> getPriceOverrides() {
		List<PriceOverrideRecord> records = Lists.newArrayList();
		for (PartItem item : currentParts) {
			PriceOverrideRecord record = item.getPriceOverrideRecord();
			if (record != null) {
				records.add(record);
			}
		}
		for (FlatRateItem item : currentFlatRates) {
			PriceOverrideRecord record = item.getPriceOverrideRecord();
			if (record != null) {
				records.add(record);
			}
		}
		return records;
	}

	public void populateClientProductSelections(List<ClientProductSelectionRecord> products) {
		List<ClientProductSelectionRecord> currentProducts = clientProductService.getRecords();

		List<ClientProductSelectionRecord> recordsToRemove = Lists.newArrayList();
		for (ClientProductSelectionRecord currentRecord : currentProducts) {
			boolean found = false;
			for (ClientProductSelectionRecord newRecord : products) {
				if (currentRecord.equals(newRecord)) {
					found = true;
					break;
				}
			}
			if (!found) {
				recordsToRemove.add(currentRecord);
			}
		}

		List<ClientProductSelectionRecord> recordsToAdd = Lists.newArrayList();
		for (ClientProductSelectionRecord newRecord : products) {
			boolean found = false;
			for (ClientProductSelectionRecord currentRecord : currentProducts) {
				if (newRecord.equals(currentRecord)) {
					found = true;
					break;
				}
			}
			if (!found) {
				recordsToAdd.add(newRecord);
			}
		}

		for (ClientProductSelectionRecord toRemove : recordsToRemove) {
			removeProduct(toRemove);
		}
		for (ClientProductSelectionRecord toAdd : recordsToAdd) {
			addProduct(toAdd);
		}

		if (composite.clientProductTableComposite.table.getItemCount() == 1) {
			composite.clientProductTableComposite.table.setSelection(0);
		}
		checkPageComplete();
		composite.layout();
	}

	public void populateClientProducts(List<OrderClientProductRecord> clientProducts) {
		List<ClientProductSelectionRecord> clientProductSelections = Lists.newArrayList();
		for (OrderClientProductRecord clientProduct : clientProducts) {
			ClientProductSelectionRecord selectionRecord = new ClientProductSelectionRecord();
			selectionRecord.setClientProductId(clientProduct.getClientProductId());
			selectionRecord.setProductManufacturer(clientProduct.getProductManufacturer());
			selectionRecord.setProductModel(clientProduct.getProductModel());
			selectionRecord.setProductType(clientProduct.getProductType());
			selectionRecord.setSerialNumber(clientProduct.getSerialNumber());
			clientProductSelections.add(selectionRecord);
		}

		populateClientProductSelections(clientProductSelections);
	}

	public void populateParts(List<OrderPartRecord> parts) {
		List<OrderPartRecord> recordsToAdd = Lists.newArrayList();
		for (OrderPartRecord record : parts) {
			boolean found = false;
			for (PartItem partItem : currentParts) {
				if (partItem.partsEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				recordsToAdd.add(record);
			}
		}

		for (int i = currentParts.size() - 1; i >= 0; i--) {
			PartItem partItem = currentParts.get(i);
			boolean found = false;
			for (OrderPartRecord record : parts) {
				if (partItem.partsEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				partItem.dispose();
				currentParts.remove(i);
			}
		}

		for (OrderPartRecord toAdd : recordsToAdd) {
			PartItem partItem = new PartItem(toAdd);
			currentParts.add(partItem);
		}

		((GridData) composite.lblNoPartsHave.getLayoutData()).exclude = !currentParts.isEmpty();
		composite.lblNoPartsHave.setVisible(parts.isEmpty());

		((GridData) composite.cmpPartDetails.getLayoutData()).exclude = currentParts.isEmpty();
		composite.cmpPartDetails.setVisible(!parts.isEmpty());

		checkPageComplete();
		composite.cmpPartDetails.layout(true);
		composite.layout(true);
	}

	public void populatePartSearches(List<PartSearchRecord> parts) {
		List<PartSearchRecord> recordsToAdd = Lists.newArrayList();
		for (PartSearchRecord record : parts) {
			boolean found = false;
			for (PartItem partItem : currentParts) {
				if (partItem.partsEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				recordsToAdd.add(record);
			}
		}

		for (int i = currentParts.size() - 1; i >= 0; i--) {
			PartItem partItem = currentParts.get(i);
			if (partItem.getOrderPartRecord().getOrderToPartId() != null) {
				// Skip any parts that have already been saved to the order.
				continue;
			}
			boolean found = false;
			for (PartSearchRecord record : parts) {
				if (partItem.partsEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				partItem.dispose();
				currentParts.remove(i);
			}
		}

		for (PartSearchRecord toAdd : recordsToAdd) {
			PartItem partItem = new PartItem(toAdd);
			currentParts.add(partItem);
		}

		((GridData) composite.lblNoPartsHave.getLayoutData()).exclude = !currentParts.isEmpty();
		composite.lblNoPartsHave.setVisible(currentParts.isEmpty());

		((GridData) composite.cmpPartDetails.getLayoutData()).exclude = currentParts.isEmpty();
		composite.cmpPartDetails.setVisible(!currentParts.isEmpty());

		checkPageComplete();
		composite.cmpPartDetails.layout(true);
		composite.layout(true);
	}

	public void populateFlatRates(List<OrderFlatRateServiceRecord> flatRates) {
		List<OrderFlatRateServiceRecord> recordsToAdd = Lists.newArrayList();
		for (OrderFlatRateServiceRecord record : flatRates) {
			boolean found = false;
			for (FlatRateItem flatRateItem : currentFlatRates) {
				if (flatRateItem.flatRatesEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				recordsToAdd.add(record);
			}
		}

		for (int i = currentFlatRates.size() - 1; i >= 0; i--) {
			FlatRateItem flatRateItem = currentFlatRates.get(i);
			boolean found = false;
			for (OrderFlatRateServiceRecord record : flatRates) {
				if (flatRateItem.flatRatesEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				flatRateItem.dispose();
				currentFlatRates.remove(i);
			}
		}

		for (OrderFlatRateServiceRecord toAdd : recordsToAdd) {
			FlatRateItem flatRateItem = new FlatRateItem(toAdd);
			currentFlatRates.add(flatRateItem);
		}

		((GridData) composite.lblNoFlatRate.getLayoutData()).exclude = !currentFlatRates.isEmpty();
		composite.lblNoFlatRate.setVisible(currentFlatRates.isEmpty());

		((GridData) composite.cmpFlatRateDetails.getLayoutData()).exclude = currentFlatRates.isEmpty();
		composite.cmpFlatRateDetails.setVisible(!currentFlatRates.isEmpty());

		checkPageComplete();
		composite.cmpFlatRateDetails.layout(true);
		composite.layout(true);
	}

	public void populateFlatRateSearches(List<FlatRateSearchRecord> flatRates) {
		List<FlatRateSearchRecord> recordsToAdd = Lists.newArrayList();
		for (FlatRateSearchRecord record : flatRates) {
			boolean found = false;
			for (FlatRateItem flatRateItem : currentFlatRates) {
				if (flatRateItem.flatRatesEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				recordsToAdd.add(record);
			}
		}

		for (int i = currentFlatRates.size() - 1; i >= 0; i--) {
			FlatRateItem flatRateItem = currentFlatRates.get(i);
			if (flatRateItem.getOrderFlatRateServiceRecord().getOrderToFrsId() != null) {
				// skip any flat rate services previously added.
				continue;
			}
			boolean found = false;
			for (FlatRateSearchRecord record : flatRates) {
				if (flatRateItem.flatRatesEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				flatRateItem.dispose();
				currentFlatRates.remove(i);
			}
		}

		for (FlatRateSearchRecord toAdd : recordsToAdd) {
			FlatRateItem flatRateItem = new FlatRateItem(toAdd);
			currentFlatRates.add(flatRateItem);
		}

		((GridData) composite.lblNoFlatRate.getLayoutData()).exclude = !currentFlatRates.isEmpty();
		composite.lblNoFlatRate.setVisible(currentFlatRates.isEmpty());

		((GridData) composite.cmpFlatRateDetails.getLayoutData()).exclude = currentFlatRates.isEmpty();
		composite.cmpFlatRateDetails.setVisible(!currentFlatRates.isEmpty());

		checkPageComplete();
		composite.cmpFlatRateDetails.layout(true);
		composite.layout(true);
	}

	public void populateServices(List<OrderServiceRecord> services) {
		List<OrderServiceRecord> recordsToAdd = Lists.newArrayList();
		for (OrderServiceRecord record : services) {
			boolean found = false;
			for (ServiceItem serviceItem : currentServices) {
				if (serviceItem.servicesEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				recordsToAdd.add(record);
			}
		}

		for (int i = currentServices.size() - 1; i >= 0; i--) {
			ServiceItem serviceItem = currentServices.get(i);
			if (serviceItem.getOrderServiceRecord().getServiceId() != null) {
				continue;
			}
			boolean found = false;
			for (OrderServiceRecord record : services) {
				if (serviceItem.servicesEqual(record)) {
					found = true;
					break;
				}
			}
			if (!found) {
				serviceItem.dispose();
				currentServices.remove(i);
			}
		}

		for (OrderServiceRecord toAdd : recordsToAdd) {
			ServiceItem item = new ServiceItem(toAdd);
			currentServices.add(item);
		}

		((GridData) composite.lblNoServicesHave.getLayoutData()).exclude = !currentServices.isEmpty();
		composite.lblNoServicesHave.setVisible(currentServices.isEmpty());

		((GridData) composite.cmpServiceDetails.getLayoutData()).exclude = currentServices.isEmpty();
		composite.cmpServiceDetails.setVisible(!currentServices.isEmpty());

		checkPageComplete();
		composite.cmpServiceDetails.layout(true);
		composite.layout(true);
	}

	private void customizeComposite() {
		((GridData) composite.cmpOrderProducts.getLayoutData()).exclude = !showProducts;
		((GridData) composite.cmpFlatRates.getLayoutData()).exclude = !showFlatRates;
		((GridData) composite.cmpParts.getLayoutData()).exclude = !showParts;
		((GridData) composite.cmpServices.getLayoutData()).exclude = !showServices;
		((GridData) composite.lblAllServices.getLayoutData()).exclude = !(showServices || showFlatRates);
	}

	private void addProductForAll() {
		int selectionIndex = composite.clientProductTableComposite.table.getSelectionIndex();
		if (selectionIndex == -1) {
			MessageDialogs.displayError(composite.getShell(), "No Product", "Please select a product.");
			return;
		}
		ClientProductSelectionRecord productRecord = clientProductService.getRecord(selectionIndex);
		for (ServiceItem serviceItem : currentServices) {
			selectClientProduct(serviceItem.getClientProductCombo(), productRecord);
		}
		for (PartItem partItem : currentParts) {
			selectClientProduct(partItem.getClientProductCombo(), productRecord);
		}
		for (FlatRateItem flatRateItem : currentFlatRates) {
			selectClientProduct(flatRateItem.getClientProductCombo(), productRecord);
		}
		checkPageComplete();
	}

	private void checkPageComplete() {
		for (ServiceItem item : currentServices) {
			if (item.getClientProductCombo().getSelectionIndex() == -1) {
				setPageComplete(false);
				setMessage("Please choose a product for all services.");
				return;
			}
		}
		for (FlatRateItem item : currentFlatRates) {
			if (item.getClientProductCombo().getSelectionIndex() == -1) {
				setPageComplete(false);
				setMessage("Please choose a product for all flat rate services.");
				return;
			}
		}

		for (PartItem partItem : currentParts) {
			Long partQuantity = partItem.getQuantityText().getNumber();
			if (partQuantity == null || partQuantity <= 0L) {
				setPageComplete(false);
				setMessage("Please add a valid quantity for all parts.");
				return;
			}
		}

		setPageComplete(true);
		setMessage(null);
	}

	private void selectClientProduct(Combo clientProductCombo, ClientProductSelectionRecord productRecord) {
		Integer selectIndex = null;
		for (int i = 0; i < clientProductCombo.getItemCount(); i++) {
			if ((clientProductCombo.getData(DATA_PRODUCT_SELECTION + i).equals(productRecord))) {
				selectIndex = i;
				break;
			}
		}
		if (selectIndex != null) {
			clientProductCombo.select(selectIndex);
		}
	}

	private void populateComboWithProducts(Combo cmb) {
		List<ClientProductSelectionRecord> currentProducts = clientProductService.getRecords();
		for (ClientProductSelectionRecord product : currentProducts) {
			addProductToCombo(product, cmb);
		}
	}

	private void addProductToCombo(ClientProductSelectionRecord toAdd, Combo cmb) {
		int nextIndex = cmb.getItemCount();
		cmb.add(AppPrefs.FORMATTER.formatClientProductDisplay(toAdd.getProductManufacturer(), toAdd.getProductModel(),
				toAdd.getSerialNumber()));
		cmb.setData(DATA_PRODUCT_SELECTION + nextIndex, toAdd);
	}

	private void selectClientProduct(Combo cmb, Long clientProductId) {
		boolean found = false;
		if (clientProductId != null) {
			for (int i = 0; i < cmb.getItemCount(); i++) {
				ClientProductSelectionRecord clientProduct = (ClientProductSelectionRecord) cmb
						.getData(DATA_PRODUCT_SELECTION + i);
				if (clientProductId.equals(clientProduct.getClientProductId())) {
					cmb.select(i);
					found = true;
					break;
				}
			}
		}
		if (!found) {
			cmb.deselectAll();
		}
	}

	private void removeProductFromCombo(ClientProductSelectionRecord toRemove, Combo cmb) {
		int selectionIndex = cmb.getSelectionIndex();
		Integer indexToRemove = null;
		for (int i = 0; i < cmb.getItemCount(); i++) {
			if (toRemove.equals(cmb.getData(DATA_PRODUCT_SELECTION + i))) {
				indexToRemove = i;
				break;
			}
		}

		if (indexToRemove != null) {
			// Move all of the data selections up an index.
			for (int i = indexToRemove; i < cmb.getItemCount(); i++) {
				cmb.setData(DATA_PRODUCT_SELECTION + i, cmb.getData(DATA_PRODUCT_SELECTION + (i + 1)));
			}
			cmb.remove(indexToRemove);
			if (selectionIndex == indexToRemove) {
				cmb.deselectAll();
			}
		}
	}

	private void addProduct(ClientProductSelectionRecord toAdd) {
		clientProductService.addRecord(toAdd);
		for (FlatRateItem item : currentFlatRates) {
			addProductToCombo(toAdd, item.getClientProductCombo());
		}
		for (PartItem partItem : currentParts) {
			addProductToCombo(toAdd, partItem.getClientProductCombo());
		}
		for (ServiceItem serviceItem : currentServices) {
			addProductToCombo(toAdd, serviceItem.getClientProductCombo());
		}
	}

	private void removeProduct(ClientProductSelectionRecord toRemove) {
		clientProductService.removeRecord(toRemove);
		for (FlatRateItem item : currentFlatRates) {
			removeProductFromCombo(toRemove, item.getClientProductCombo());
		}
		for (PartItem partItem : currentParts) {
			removeProductFromCombo(toRemove, partItem.getClientProductCombo());
		}
		for (ServiceItem serviceItem : currentServices) {
			removeProductFromCombo(toRemove, serviceItem.getClientProductCombo());
		}
	}

	private static OrderPartRecord createNewPartRecord(PartSearchRecord partSearchRecord) {
		OrderPartRecord record = new OrderPartRecord();
		record.setClientProductId(null);
		record.setDescription(partSearchRecord.getPartDescription());
		record.setPartNumber(record.getPartNumber());
		record.setManufacturerDisplay(partSearchRecord.getPartManufacturer());
		record.setOnSale(partSearchRecord.isOnSale());
		record.setOrderToPartId(null);
		record.setPartId(partSearchRecord.getPartId());
		record.setPartName(partSearchRecord.getPartName());
		record.setPartPrice(partSearchRecord.getCurrentPrice());
		record.setQuantity(1);
		record.setTaxAmount(Calculator.calculateTax(partSearchRecord.getCurrentPrice()));
		record.setTaxExempt(false);
		return record;
	}

	private static OrderFlatRateServiceRecord createNewFlatRateRecord(FlatRateSearchRecord flatRateSearchRecord) {
		OrderFlatRateServiceRecord record = new OrderFlatRateServiceRecord();
		record.setFlatRateServiceId(flatRateSearchRecord.getFlatRateServiceId());
		record.setOnSale(flatRateSearchRecord.isOnSale());
		record.setServiceName(flatRateSearchRecord.getServiceName());
		record.setServicePrice(flatRateSearchRecord.getCurrentPrice());
		return record;
	}

	private class PartItem {

		private static final String DATA_PART_PRICE = "DATA_PART_PRICE";

		private final OrderPartRecord record;
		private final Label label;
		private final Combo cmbClientProduct;
		private final NumberText txtQuantity;
		private final Label lblPrice;
		private final Button btnModifyPrice;
		private final Button btnRemove;

		private PriceOverrideRecord overrideRecord;

		private PartItem(PartSearchRecord partSearchRecord) {
			this(createNewPartRecord(partSearchRecord));
		}

		private PartItem(OrderPartRecord record) {
			this.record = record;

			label = new Label(composite.cmpPartDetails, SWT.NONE);
			label.setText(record.getPartName());
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));

			cmbClientProduct = new Combo(composite.cmpPartDetails, SWT.READ_ONLY);
			cmbClientProduct.setEnabled(showProducts);
			GridData gd_cmbClientProduct = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gd_cmbClientProduct.widthHint = 125;
			cmbClientProduct.setLayoutData(gd_cmbClientProduct);
			populateComboWithProducts(cmbClientProduct);
			selectClientProduct(cmbClientProduct, record.getClientProductId());

			txtQuantity = new NumberText(composite.cmpPartDetails, SWT.BORDER);
			txtQuantity.setNumber(record.getQuantity());
			GridData gd_txtQuantity = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gd_txtQuantity.widthHint = 35;
			txtQuantity.setLayoutData(gd_txtQuantity);
			txtQuantity.addListener(SWT.Modify, e -> checkPageComplete());

			lblPrice = new Label(composite.cmpPartDetails, SWT.NONE);
			lblPrice.setText(AppPrefs.FORMATTER.formatMoney(record.getPartPrice()));
			lblPrice.setData(DATA_PART_PRICE, record.getPartPrice());

			btnModifyPrice = new Button(composite.cmpPartDetails, SWT.NONE);
			btnModifyPrice.setText("Change Price");
			btnModifyPrice.setLayoutData(new GridData());
			boolean enabled = AppPrefs.hasPermission(AppPermissions.DISCOUNT_PRICE);
			btnModifyPrice.setEnabled(enabled);
			btnModifyPrice.setVisible(enabled);
			((GridData) btnModifyPrice.getLayoutData()).exclude = !enabled;
			btnModifyPrice.addListener(SWT.Selection, e -> {
				PriceOverrideDialog dialog = new PriceOverrideDialog(getShell(), SWT.NONE);
				if (dialog.open((BigDecimal) lblPrice.getData(DATA_PART_PRICE)) != SWT.SAVE) {
					return;
				}
				overrideRecord = new PriceOverrideRecord();
				overrideRecord.setEntityId(record.getPartId());
				overrideRecord.setEntityName(EntityName.PART);
				overrideRecord.setOriginalAmount((BigDecimal) lblPrice.getData(DATA_PART_PRICE));
				overrideRecord.setOverrideAmount(dialog.getNewPrice());
				overrideRecord.setOverrideDttm(new Date());
				overrideRecord.setOverridePrsnlId(AppPrefs.currentSession.prsnlId);
				overrideRecord.setOverrideReason(dialog.getOverrideReason());
				lblPrice.setData(DATA_PART_PRICE, dialog.getNewPrice());
				lblPrice.setText(AppPrefs.FORMATTER.formatMoney(dialog.getNewPrice()));
			});

			btnRemove = new Button(composite.cmpPartDetails, SWT.NONE);
			btnRemove.setText("Remove");
			btnRemove.setLayoutData(new GridData());
			btnRemove.addListener(SWT.Selection, e -> {
				dispose();
				composite.cmpPartDetails.layout(true);
				composite.layout(true);
				currentParts.remove(this);
			});
		}

		private PriceOverrideRecord getPriceOverrideRecord() {
			return overrideRecord;
		}

		private void dispose() {
			label.dispose();
			cmbClientProduct.dispose();
			txtQuantity.dispose();
			lblPrice.dispose();
			btnModifyPrice.dispose();
			btnRemove.dispose();
		}

		private boolean partsEqual(PartSearchRecord searchRecord) {
			if (record.getOrderToPartId() != null) {
				return false;
			}
			return record.getPartId().equals(searchRecord.getPartId())
					&& record.getPartPrice().equals(searchRecord.getCurrentPrice());
		}

		private boolean partsEqual(OrderPartRecord otherRecord) {
			if (record.getOrderToPartId() == null && otherRecord.getOrderToPartId() == null) {
				return record.getPartId().equals(otherRecord.getPartId())
						&& record.getPartPrice().equals(otherRecord.getPartPrice());
			}
			if (record.getOrderToPartId() == null || otherRecord.getOrderToPartId() == null) {
				return false;
			}
			return record.getOrderToPartId().equals(otherRecord.getOrderToPartId());
		}

		private OrderPartRecord getOrderPartRecord() {
			ClientProductSelectionRecord clientProductRecord = null;
			if (cmbClientProduct != null) {
				clientProductRecord = (ClientProductSelectionRecord) cmbClientProduct
						.getData(DATA_PRODUCT_SELECTION + cmbClientProduct.getSelectionIndex());
			}
			if (clientProductRecord == null) {
				record.setClientProductId(null);
			} else {
				record.setClientProductId(clientProductRecord.getClientProductId());
			}

			record.setQuantity(txtQuantity.getNumber());
			record.setPartPrice((BigDecimal) lblPrice.getData(DATA_PART_PRICE));
			record.setTaxAmount(Calculator.calculateTax((BigDecimal) lblPrice.getData(DATA_PART_PRICE)));
			return record;
		}

		private Combo getClientProductCombo() {
			return cmbClientProduct;
		}

		private NumberText getQuantityText() {
			return txtQuantity;
		}
	}

	private class FlatRateItem {
		private static final String DATA_FLAT_RATE_PRICE = "DATA_FLAT_RATE_PRICE";

		private final OrderFlatRateServiceRecord record;
		private final Label lblFlatRateName;
		private final Combo cmbClientProduct;
		private final Label lblPrice;
		private final Button btnModifyPrice;
		private final Button btnRemove;

		private PriceOverrideRecord overrideRecord;

		private FlatRateItem(FlatRateSearchRecord record) {
			this(createNewFlatRateRecord(record));
		}

		private FlatRateItem(OrderFlatRateServiceRecord record) {
			this.record = record;

			lblFlatRateName = new Label(composite.cmpFlatRateDetails, SWT.NONE);
			lblFlatRateName.setText(record.getServiceName());
			lblFlatRateName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));

			cmbClientProduct = new Combo(composite.cmpFlatRateDetails, SWT.READ_ONLY);
			cmbClientProduct.setEnabled(showProducts);
			GridData gd_cmb = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gd_cmb.widthHint = 125;
			cmbClientProduct.setLayoutData(gd_cmb);
			populateComboWithProducts(cmbClientProduct);
			selectClientProduct(cmbClientProduct, record.getClientProductId());
			cmbClientProduct.addListener(SWT.Selection, e -> checkPageComplete());

			lblPrice = new Label(composite.cmpFlatRateDetails, SWT.NONE);
			lblPrice.setText(AppPrefs.FORMATTER.formatMoney(record.getServicePrice()));
			lblPrice.setData(DATA_FLAT_RATE_PRICE, record.getServicePrice());

			btnModifyPrice = new Button(composite.cmpFlatRateDetails, SWT.NONE);
			btnModifyPrice.setText("Change Price");
			btnModifyPrice.setLayoutData(new GridData());
			boolean enabled = AppPrefs.hasPermission(AppPermissions.DISCOUNT_PRICE);
			btnModifyPrice.setEnabled(enabled);
			btnModifyPrice.setVisible(enabled);
			((GridData) btnModifyPrice.getLayoutData()).exclude = !enabled;
			btnModifyPrice.addListener(SWT.Selection, e -> {
				PriceOverrideDialog dialog = new PriceOverrideDialog(getShell(), SWT.NONE);
				if (dialog.open((BigDecimal) lblPrice.getData(DATA_FLAT_RATE_PRICE)) != SWT.SAVE) {
					return;
				}
				overrideRecord = new PriceOverrideRecord();
				overrideRecord.setEntityId(record.getFlatRateServiceId());
				overrideRecord.setEntityName(EntityName.FLAT_RATE_SERVICE);
				overrideRecord.setOriginalAmount((BigDecimal) lblPrice.getData(DATA_FLAT_RATE_PRICE));
				overrideRecord.setOverrideAmount(dialog.getNewPrice());
				overrideRecord.setOverrideDttm(new Date());
				overrideRecord.setOverridePrsnlId(AppPrefs.currentSession.prsnlId);
				overrideRecord.setOverrideReason(dialog.getOverrideReason());
				lblPrice.setData(DATA_FLAT_RATE_PRICE, dialog.getNewPrice());
				lblPrice.setText(AppPrefs.FORMATTER.formatMoney(dialog.getNewPrice()));
			});

			btnRemove = new Button(composite.cmpFlatRateDetails, SWT.NONE);
			btnRemove.setText("Remove");
			btnRemove.setLayoutData(new GridData());
			btnRemove.addListener(SWT.Selection, e -> {
				dispose();
				composite.cmpFlatRateDetails.layout(true);
				composite.layout(true);
				currentFlatRates.remove(this);
				checkPageComplete();
			});
		}

		private PriceOverrideRecord getPriceOverrideRecord() {
			return overrideRecord;
		}

		private void dispose() {
			lblFlatRateName.dispose();
			cmbClientProduct.dispose();
			lblPrice.dispose();
			btnModifyPrice.dispose();
			btnRemove.dispose();
		}

		private boolean flatRatesEqual(FlatRateSearchRecord searchRecord) {
			if (record.getOrderToFrsId() != null) {
				return false;
			}
			return record.getFlatRateServiceId().equals(searchRecord.getFlatRateServiceId())
					&& record.getServicePrice().equals(searchRecord.getCurrentPrice());
		}

		private boolean flatRatesEqual(OrderFlatRateServiceRecord otherRecord) {
			if (record.getOrderToFrsId() == null && otherRecord.getOrderToFrsId() == null) {
				return record.getFlatRateServiceId().equals(otherRecord.getFlatRateServiceId())
						&& record.getServicePrice().equals(otherRecord.getServicePrice());
			}
			if (record.getOrderToFrsId() == null || otherRecord.getOrderToFrsId() == null) {
				return false;
			}
			return record.getOrderToFrsId().equals(otherRecord.getOrderToFrsId());
		}

		private Combo getClientProductCombo() {
			return cmbClientProduct;
		}

		private OrderFlatRateServiceRecord getOrderFlatRateServiceRecord() {
			ClientProductSelectionRecord clientProductRecord = (ClientProductSelectionRecord) cmbClientProduct
					.getData(DATA_PRODUCT_SELECTION + cmbClientProduct.getSelectionIndex());
			if (clientProductRecord == null) {
				record.setClientProductId(null);
			} else {
				record.setClientProductId(clientProductRecord.getClientProductId());
			}

			record.setServicePrice((BigDecimal) lblPrice.getData(DATA_FLAT_RATE_PRICE));
			return record;
		}
	}

	private class ServiceItem {

		private final OrderServiceRecord record;
		private final Label lblServiceName;
		private final Combo cmbClientProduct;
		private final Button btnRemove;

		public ServiceItem(OrderServiceRecord record) {
			this.record = record;
			lblServiceName = new Label(composite.cmpServiceDetails, SWT.NONE);
			lblServiceName.setText(record.getServiceName());
			lblServiceName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));

			cmbClientProduct = new Combo(composite.cmpServiceDetails, SWT.READ_ONLY);
			cmbClientProduct.setEnabled(showProducts);
			GridData gd_cmb = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gd_cmb.widthHint = 125;
			cmbClientProduct.setLayoutData(gd_cmb);
			populateComboWithProducts(cmbClientProduct);
			selectClientProduct(cmbClientProduct, record.getClientProductId());
			cmbClientProduct.addListener(SWT.Selection, e -> checkPageComplete());

			btnRemove = new Button(composite.cmpServiceDetails, SWT.NONE);
			btnRemove.setText("Remove");
			btnRemove.addListener(SWT.Selection, e -> {
				dispose();
				currentServices.remove(this);
				composite.cmpServiceDetails.layout();
				composite.layout();
				checkPageComplete();
			});
		}

		public Combo getClientProductCombo() {
			return cmbClientProduct;
		}

		public OrderServiceRecord getOrderServiceRecord() {
			ClientProductSelectionRecord clientProductRecord = (ClientProductSelectionRecord) cmbClientProduct
					.getData(DATA_PRODUCT_SELECTION + cmbClientProduct.getSelectionIndex());
			if (clientProductRecord == null) {
				record.setClientProductId(null);
			} else {
				record.setClientProductId(clientProductRecord.getClientProductId());
			}
			return record;
		}

		public void dispose() {
			lblServiceName.dispose();
			cmbClientProduct.dispose();
			btnRemove.dispose();
		}

		public boolean servicesEqual(OrderServiceRecord otherRecord) {
			if (record.getServiceId() == null && otherRecord.getServiceId() == null) {
				return record == otherRecord;
			}
			if (record.getServiceId() == null || otherRecord.getServiceId() == null) {
				return false;
			}
			return record.getServiceId().equals(otherRecord.getServiceId());
		}
	}
}
