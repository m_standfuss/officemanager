package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.util.ResourceManager;

public class LoginComposite extends Composite {
	public final Text txtUsername;
	public final Text txtPassword;
	public final Label lblSolution;
	public final Button btnLogin;
	public final Button btnCancel;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public LoginComposite(Composite parent, int style) {
		super(parent, SWT.NONE);
		setLayout(new GridLayout(2, true));

		Label lblOfficeManager = new Label(this, SWT.NONE);
		lblOfficeManager.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
		lblOfficeManager.setFont(ResourceManager.getFont("Segoe UI", 24, SWT.NORMAL));
		lblOfficeManager.setText("Office Manager");

		lblSolution = new Label(this, SWT.NONE);
		lblSolution.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
		lblSolution.setFont(ResourceManager.getFont("Segoe UI", 17, SWT.NORMAL));
		lblSolution.setText("Solution");

		Label lblUsername = new Label(this, SWT.NONE);
		lblUsername.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
		lblUsername.setFont(ResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblUsername.setText("Username:");
		txtUsername = new Text(this, SWT.BORDER);
		GridData gd_txtUsername = new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1);
		gd_txtUsername.widthHint = 175;
		txtUsername.setLayoutData(gd_txtUsername);

		Label lblPassword = new Label(this, SWT.NONE);
		lblPassword.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		lblPassword.setFont(ResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblPassword.setText("Password:");

		txtPassword = new Text(this, SWT.BORDER | SWT.PASSWORD);
		GridData gd_txtPassword = new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1);
		gd_txtPassword.widthHint = 175;
		txtPassword.setLayoutData(gd_txtPassword);

		btnLogin = new Button(this, SWT.NONE);
		btnLogin.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnLogin.setText("   Login   ");

		btnCancel = new Button(this, SWT.NONE);
		btnCancel.setText("  Cancel  ");
	}
}
