package officemanager.gui.swt.ui.wizards.pages;

import java.util.List;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.SimpleContentProposalProvider;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.AddressRecord;
import officemanager.biz.records.CodeValueRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.AddressService;
import officemanager.gui.swt.ui.composites.AddressEditComposite;
import officemanager.gui.swt.ui.composites.ButtonBarComposite;
import officemanager.gui.swt.ui.tables.AddressTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.StringUtility;

public class AddressesPage extends OfficeManagerWizardPage {
	private static final String ALPHA_LOWER = "abcdefghijklmnopqrstuvwxyz";
	private static final String ALPHA_UPPER = ALPHA_LOWER.toUpperCase();

	private final AddressService addressService;

	private AddressTableComposite addressTbl;
	private AddressEditComposite newAddressComposite;
	private Button btnRemoveSelected;

	public AddressesPage() {
		super("AddressPage", "Address Information", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.ADDRESS, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("A primary address is required.");
		setPageComplete(false);
		addressService = new AddressService();
	}

	@Override
	public void setFocus() {
		newAddressComposite.txtAddressLine1.setFocus();
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		Composite root = new Composite(scrCompositeSideBar, SWT.NONE);
		root.setLayout(new GridLayout());

		CollapsibleComposite clpseCmp = new CollapsibleComposite(root, SWT.NONE);
		clpseCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		clpseCmp.composite.setLayout(new GridLayout());
		clpseCmp.setTitleText("New Address");

		newAddressComposite = new AddressEditComposite(clpseCmp.composite, SWT.NONE);

		ButtonBarComposite btnBar = new ButtonBarComposite(clpseCmp.composite, SWT.NONE);
		btnBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		addressTbl = new AddressTableComposite(root, SWT.NONE, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_phoneTbl = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_phoneTbl.heightHint = 200;
		addressTbl.setLayoutData(gd_phoneTbl);

		btnRemoveSelected = new Button(root, SWT.NONE);
		btnRemoveSelected.setText("Remove Selected");
		btnRemoveSelected.setEnabled(AppPrefs.hasPermission(AppPermissions.REMOVE_PHONE));

		scrCompositeSideBar.setContent(root);
		scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrCompositeSideBar);

		clpseCmp.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT)));
		btnBar.btnSave.addListener(SWT.Selection, l -> addNewAddress());
		btnBar.btnClear.addListener(SWT.Selection, e -> clear());
		btnRemoveSelected.addListener(SWT.Selection, e -> removeSelected());
		addressService.setComposite(addressTbl);
	}

	public void populateCityAutoCompletes(List<CodeValueRecord> cities) {

		String[] cityArray = new String[cities.size()];
		for (int i = 0; i < cities.size(); i++) {
			CodeValueRecord record = cities.get(i);
			cityArray[i] = record.getDisplay();
		}

		SimpleContentProposalProvider proposalProvider = new SimpleContentProposalProvider(cityArray);
		ContentProposalAdapter proposalAdapter = new ContentProposalAdapter(newAddressComposite.txtCity,
				new TextContentAdapter(), proposalProvider, getActivationKeystroke(), getAutoactivationChars());
		proposalProvider.setFiltering(true);
		proposalAdapter.setPropagateKeys(true);
		proposalAdapter.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_REPLACE);
	}

	public void populateAddressTypes(List<CodeValueRecord> addressTypes) {
		newAddressComposite.cmbAddressType.populateSelections(addressTypes);
	}

	public void populateAddresses(List<AddressRecord> records) {
		addressService.populateAddress(records);
		checkPageComplete();
	}

	public List<AddressRecord> getAddresses() {
		List<AddressRecord> addressList = Lists.newArrayList();
		for (TableItem item : addressTbl.table.getItems()) {
			addressList.add((AddressRecord) item.getData(AddressService.DATA_RECORD));
		}
		return addressList;
	}

	private void addNewAddress() {
		List<String> addressLines = Lists.newArrayList();
		if (!StringUtility.isNullOrWhiteSpace(newAddressComposite.txtAddressLine1.getText())) {
			addressLines.add(newAddressComposite.txtAddressLine1.getText());
		}
		if (!StringUtility.isNullOrWhiteSpace(newAddressComposite.txtAddressLine2.getText())) {
			addressLines.add(newAddressComposite.txtAddressLine2.getText());
		}
		if (!StringUtility.isNullOrWhiteSpace(newAddressComposite.txtAddressLine3.getText())) {
			addressLines.add(newAddressComposite.txtAddressLine3.getText());
		}
		if (addressLines.isEmpty()) {
			setErrorMessage("Please fill out the address.");
			return;
		}
		if (newAddressComposite.cmbAddressType.getSelectionIndex() == -1) {
			setErrorMessage("Please select an address type.");
			return;
		}
		setErrorMessage(null);
		AddressRecord address = new AddressRecord();
		address.setAddressLines(addressLines);
		address.setAddressType(newAddressComposite.cmbAddressType.getCodeValueRecord());
		address.setCity(newAddressComposite.txtCity.getText());
		address.setPrimaryAddress(newAddressComposite.btnPrimaryAddress.getSelection());
		address.setState(newAddressComposite.txtState.getText());
		address.setZip(newAddressComposite.txtZip.getText());
		addressService.addAddress(address);
		clear();
		checkPageComplete();
	}

	private void removeSelected() {
		int index = addressTbl.table.getSelectionIndex();
		if (index == -1) {
			return;
		}
		addressTbl.table.remove(index);
		checkPageComplete();
	}

	private void checkPageComplete() {
		boolean isComplete = isComplete();
		setPageComplete(isComplete);
		if (isComplete) {
			setDescription("");
		} else {
			setDescription("A primary address is required.");
		}
	}

	private boolean isComplete() {
		for (TableItem item : addressTbl.table.getItems()) {
			if (((AddressRecord) item.getData(AddressService.DATA_RECORD)).isPrimaryAddress()) {
				return true;
			}
		}
		return false;
	}

	private void clear() {
		newAddressComposite.txtAddressLine1.setText("");
		newAddressComposite.txtAddressLine2.setText("");
		newAddressComposite.txtAddressLine3.setText("");
		newAddressComposite.cmbAddressType.deselectAll();
		newAddressComposite.txtCity.setText("");
		newAddressComposite.txtState.setText("");
		newAddressComposite.txtZip.setText("");
		newAddressComposite.btnPrimaryAddress.setSelection(false);
	}

	private char[] getAutoactivationChars() {
		String delete = new String(new char[] { 8 });
		String allChars = ALPHA_LOWER + ALPHA_UPPER + delete;
		return allChars.toCharArray();
	}

	private KeyStroke getActivationKeystroke() {
		KeyStroke instance = KeyStroke.getInstance(new Integer(SWT.CTRL).intValue(), new Integer(' ').intValue());
		return instance;
	}
}
