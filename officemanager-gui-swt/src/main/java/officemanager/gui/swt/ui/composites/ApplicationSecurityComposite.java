package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ApplicationSecurityComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmNewPart;

	public final List listRoles;
	public final Label lblActiveRoles;
	public final Label lblRolePermissions;
	public final List listPermissions;
	public final Button btnEditRole;
	public final Label lblRolesCanBe;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ApplicationSecurityComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		tlItmNewPart = new ToolItem(toolBar, SWT.NONE);
		tlItmNewPart.setImage(ResourceManager.getImage(ImageFile.USER_SECURITY_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmNewPart.setToolTipText("New Role");
		tlItmNewPart.setEnabled(AppPrefs.hasPermission(AppPermissions.SECURITY_ADD_ROLE));

		lblActiveRoles = new Label(this, SWT.NONE);
		lblActiveRoles.setText("Active Roles");
		lblActiveRoles.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblRolePermissions = new Label(this, SWT.NONE);
		lblRolePermissions.setText("Role Permissions");
		lblRolePermissions.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		listRoles = new List(this, SWT.BORDER | SWT.V_SCROLL);
		GridData gd_list = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_list.heightHint = 200;
		gd_list.widthHint = 250;
		listRoles.setLayoutData(gd_list);

		listPermissions = new List(this, SWT.BORDER | SWT.V_SCROLL);
		GridData gd_listPermissions = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_listPermissions.widthHint = 250;
		gd_listPermissions.heightHint = 200;
		listPermissions.setLayoutData(gd_listPermissions);

		btnEditRole = new Button(this, SWT.NONE);
		GridData gd_btnEditRole = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnEditRole.widthHint = 75;
		btnEditRole.setLayoutData(gd_btnEditRole);
		btnEditRole.setText("Edit Role");
		new Label(this, SWT.NONE);

		lblRolesCanBe = new Label(this, SWT.NONE);
		lblRolesCanBe.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblRolesCanBe.setText("Roles can be assigned to users on their individual information page.");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
