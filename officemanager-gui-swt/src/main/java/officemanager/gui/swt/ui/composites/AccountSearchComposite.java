package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.AccountTableComposite;
import officemanager.gui.swt.ui.widgets.NumberText;
import officemanager.gui.swt.ui.widgets.PhoneNumberText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class AccountSearchComposite extends Composite {
	public final ToolBar toolBar;
	public final ToolItem tlItmAddAccount;

	public final ScrolledComposite scrCompositeSideBarSearch;
	public final Composite compositeSideBarSearch;
	public final Button btnSearch;
	public final Button btnClear;

	public final Composite compositeCenter;
	public final AccountTableComposite searchResultsTable;
	public final Label lblAccountNumber;
	public final NumberText txtAccountNumber;
	public final Label lblNameLast;
	public final UpperCaseText txtNameLast;
	public final Label lblNameFirst;
	public final UpperCaseText txtNameFirst;
	public final Label lblPrimaryPhone;
	public final PhoneNumberText txtPhone;
	public final Composite compositeButtonBar;

	public AccountSearchComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

		tlItmAddAccount = new ToolItem(toolBar, SWT.NONE);
		tlItmAddAccount.setToolTipText("Add Account");
		tlItmAddAccount.setImage(ResourceManager.getImage(ImageFile.USER_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));


		scrCompositeSideBarSearch = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeSideBarSearch.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		scrCompositeSideBarSearch.setExpandVertical(true);
		scrCompositeSideBarSearch.setExpandHorizontal(true);

		compositeSideBarSearch = new Composite(scrCompositeSideBarSearch, SWT.NONE);
		compositeSideBarSearch.setLayout(new GridLayout(1, false));

		lblAccountNumber = new Label(compositeSideBarSearch, SWT.NONE);
		lblAccountNumber.setText("Account Number");
		lblAccountNumber.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtAccountNumber = new NumberText(compositeSideBarSearch, SWT.BORDER);
		GridData gd_txtAccountNumber = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAccountNumber.widthHint = 100;
		txtAccountNumber.setLayoutData(gd_txtAccountNumber);

		lblNameLast = new Label(compositeSideBarSearch, SWT.NONE);
		lblNameLast.setText("Name Last");
		lblNameLast.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtNameLast = new UpperCaseText(compositeSideBarSearch, SWT.BORDER);
		GridData gd_txtNameLast = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtNameLast.widthHint = 150;
		txtNameLast.setLayoutData(gd_txtNameLast);

		lblNameFirst = new Label(compositeSideBarSearch, SWT.NONE);
		lblNameFirst.setText("Name First");
		lblNameFirst.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtNameFirst = new UpperCaseText(compositeSideBarSearch, SWT.BORDER);
		GridData gd_txtNameFirst = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtNameFirst.widthHint = 150;
		txtNameFirst.setLayoutData(gd_txtNameFirst);

		lblPrimaryPhone = new Label(compositeSideBarSearch, SWT.NONE);
		lblPrimaryPhone.setText("Primary Phone");
		lblPrimaryPhone.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtPhone = new PhoneNumberText(compositeSideBarSearch, SWT.BORDER);
		GridData gd_txtPhone = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtPhone.widthHint = 100;
		txtPhone.setLayoutData(gd_txtPhone);

		compositeButtonBar = new Composite(compositeSideBarSearch, SWT.NONE);
		compositeButtonBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		compositeButtonBar.setLayout(new GridLayout(2, false));

		btnSearch = new Button(compositeButtonBar, SWT.NONE);
		GridData gd_btnSearch = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnSearch.widthHint = 80;
		btnSearch.setLayoutData(gd_btnSearch);
		btnSearch.setText("Search");

		btnClear = new Button(compositeButtonBar, SWT.NONE);
		GridData gd_btnClear = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnClear.widthHint = 80;
		btnClear.setLayoutData(gd_btnClear);
		btnClear.setText("Clear");

		scrCompositeSideBarSearch.setContent(compositeSideBarSearch);
		scrCompositeSideBarSearch.setMinSize(compositeSideBarSearch.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		compositeCenter = new Composite(this, SWT.BORDER);
		compositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeCenter.setLayout(new GridLayout(1, false));

		searchResultsTable = new AccountTableComposite(compositeCenter, SWT.NONE, tableStyle);
		searchResultsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public AccountSearchComposite(Composite parent, int style) {
		this(parent, style, SWT.BORDER | SWT.FULL_SELECTION);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
