package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.CodeValueEditTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class CodeSetEditComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmAddCodeValue;

	public final CodeValueEditTableComposite codeValueEditTableComposite;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public CodeSetEditComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout());

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);

		tlItmAddCodeValue = new ToolItem(toolBar, SWT.NONE);
		tlItmAddCodeValue.setImage(ResourceManager.getImage(ImageFile.CODE_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmAddCodeValue.setToolTipText("Add Coded Value");

		codeValueEditTableComposite = new CodeValueEditTableComposite(this, SWT.NONE, SWT.FULL_SELECTION | SWT.BORDER);
		codeValueEditTableComposite.table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		codeValueEditTableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		codeValueEditTableComposite.setLayout(new GridLayout(1, false));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
