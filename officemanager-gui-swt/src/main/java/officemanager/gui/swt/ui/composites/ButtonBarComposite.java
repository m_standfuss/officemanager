package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

public class ButtonBarComposite extends Composite {

	public final Button btnSave;
	public final Button btnClear;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ButtonBarComposite(Composite parent, int style) {
		this(parent, style, true);
	}

	public ButtonBarComposite(Composite parent, int style, boolean rightToLeft) {
		super(parent, style | (rightToLeft ? SWT.RIGHT_TO_LEFT : SWT.NONE));
		setLayout(new GridLayout(2, false));
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 4, 1));

		btnSave = new Button(this, SWT.NONE);
		GridData gd_btnSave = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnSave.widthHint = 100;
		btnSave.setLayoutData(gd_btnSave);
		btnSave.setText("Save");

		btnClear = new Button(this, SWT.NONE);
		GridData gd_btnClear = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_btnClear.widthHint = 100;
		btnClear.setLayoutData(gd_btnClear);
		btnClear.setText("Clear");
		setTabList(new Control[] { btnSave, btnClear });
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
