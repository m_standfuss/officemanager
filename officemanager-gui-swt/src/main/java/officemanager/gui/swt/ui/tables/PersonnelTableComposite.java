package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class PersonnelTableComposite extends Composite {

	public static final int COL_NAME = 0;
	public static final int COL_PHONE = 1;
	public static final int COL_EMAIL = 2;
	public static final int COL_STATUS = 3;
	public static final int COL_TYPE = 4;
	public static final int COL_START_DTTM = 5;
	public static final int COL_W2_ON_FILE = 6;

	public OfficeManagerTable table;
	public TableColumn tblclmnName;
	public TableColumn tblclmnPhone;
	public TableColumn tblclmnEmail;
	public TableColumn tblclmnStatus;
	public TableColumn tblclmnType;
	public TableColumn tblclmnStartDate;
	public TableColumn tblclmnWOnFile;

	public PersonnelTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new FillLayout());
		table = new OfficeManagerTable(this, SWT.BORDER | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(100);
		tblclmnName.setText("Name");

		tblclmnPhone = new TableColumn(table, SWT.NONE);
		tblclmnPhone.setWidth(100);
		tblclmnPhone.setText("Phone");

		tblclmnEmail = new TableColumn(table, SWT.NONE);
		tblclmnEmail.setWidth(100);
		tblclmnEmail.setText("Email");

		tblclmnStatus = new TableColumn(table, SWT.NONE);
		tblclmnStatus.setWidth(100);
		tblclmnStatus.setText("Status");

		tblclmnType = new TableColumn(table, SWT.NONE);
		tblclmnType.setWidth(100);
		tblclmnType.setText("Type");

		tblclmnStartDate = new TableColumn(table, SWT.NONE);
		tblclmnStartDate.setWidth(100);
		tblclmnStartDate.setText("Start Date");

		tblclmnWOnFile = new TableColumn(table, SWT.NONE);
		tblclmnWOnFile.setWidth(100);
		tblclmnWOnFile.setText("W2 On File");
	}
}
