package officemanager.gui.swt.ui.wizards;

import officemanager.biz.delegates.PersonDelegate;
import officemanager.biz.records.PersonRecord;
import officemanager.gui.swt.ui.wizards.pages.AddressesPage;
import officemanager.gui.swt.ui.wizards.pages.PersonInformationPage;
import officemanager.gui.swt.ui.wizards.pages.PhonesPage;

public abstract class PersonWizard extends OfficeManagerWizard {

	private PersonInformationPage personInformationPage;
	private PhonesPage phonePage;
	private AddressesPage addressPage;

	public PersonWizard() {
		super();
	}

	public abstract PersonDelegate getPersonDelegate();

	public abstract PersonRecord getPersonInContext();

	@Override
	public void addPages() {
		personInformationPage = new PersonInformationPage();
		personInformationPage.addFirstShowListener(l -> populatePersonPage());
		addPage(personInformationPage);

		phonePage = new PhonesPage();
		phonePage.addFirstShowListener(l -> populatePhonePage());
		addPage(phonePage);

		addressPage = new AddressesPage();
		addressPage.addFirstShowListener(l -> populateAddressPage());
		addPage(addressPage);
	}

	private void populatePersonPage() {
		personInformationPage.populatePersonInformation(getPersonInContext());
	}

	private void populatePhonePage() {
		phonePage.populatePhoneTypes(getPersonDelegate().getPhoneTypes());
		phonePage.populatePhones(getPersonInContext().getPhoneRecords());
	}

	private void populateAddressPage() {
		addressPage.populateAddressTypes(getPersonDelegate().getAddressTypes());
		addressPage.populateAddresses(getPersonInContext().getAddressRecords());
		addressPage.populateCityAutoCompletes(getPersonDelegate().getCities());
	}

	protected void populatePersonRecord() {
		personInformationPage.populatePersonRecord(getPersonInContext());
		getPersonInContext().getAddressRecords().clear();
		getPersonInContext().getAddressRecords().addAll(addressPage.getAddresses());
		getPersonInContext().getPhoneRecords().clear();
		getPersonInContext().getPhoneRecords().addAll(phonePage.getPhones());
	}

}