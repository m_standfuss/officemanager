package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class ClientEditComposite extends Composite {
	public final Label lblAttentionOfHeader;
	public final UpperCaseText txtAttentionOf;
	public final Label lblJoinedOnHeader;
	public final Text txtJoinedOn;
	public final Button btnPromotionalClub;
	public final Button btnTaxExempt;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ClientEditComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		lblAttentionOfHeader = new Label(this, SWT.NONE);
		lblAttentionOfHeader.setText("Attention Of:");
		lblAttentionOfHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtAttentionOf = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtAttentionOf = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAttentionOf.widthHint = 200;
		txtAttentionOf.setLayoutData(gd_txtAttentionOf);

		lblJoinedOnHeader = new Label(this, SWT.NONE);
		lblJoinedOnHeader.setText("Joined On:");
		lblJoinedOnHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtJoinedOn = new Text(this, SWT.BORDER);
		txtJoinedOn.setEnabled(false);
		txtJoinedOn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		btnPromotionalClub = new Button(this, SWT.CHECK);
		btnPromotionalClub.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnPromotionalClub.setText("Promotional Club");

		btnTaxExempt = new Button(this, SWT.CHECK);
		btnTaxExempt.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnTaxExempt.setText("Tax Exempt");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
