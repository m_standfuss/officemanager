package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

public class BalanceDrawerComposite extends Composite {
	public PaymentsTotalsComposite paymentsTotalsComposite;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public BalanceDrawerComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		setLayout(new GridLayout());

		paymentsTotalsComposite = new PaymentsTotalsComposite(this, SWT.NONE);
		paymentsTotalsComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
