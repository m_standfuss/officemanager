package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ResourceManager;

public class OrderInformationComposite extends Composite {

	private static final Color COLOR_BACKGROUND = AppPrefs.COLOR_ORDER_HEADER;
	private static final Color COLOR_FOREGROUND = AppPrefs.COLOR_ORDER_HEADER_TEXT;

	private static final Font FONT_TITLE = ResourceManager.getFont("Segoe UI", 12, SWT.BOLD);
	private static final Font FONT_TEXT = ResourceManager.getFont("Segoe UI", 11, SWT.NORMAL);

	public final Label lblOrderNumberTitle;
	public final Label lblOrderNumber;
	public final Label lblOrderStatusTitle;
	public final Label lblOrderStatus;
	public final Label lblOpenedOnTitle;
	public final Label lblOpenedOn;
	public final Label lblTotalTitle;
	public final Label lblTotal;
	public final Label lblClosedOnTitle;
	public final Label lblClosedOn;
	public final Label lblClientTitle;
	public final Label lblClient;
	public final Label lblOrderTypeTitle;
	public final Label lblOrderType;
	public final Label lblOpenedByTitle;
	public final Label lblOpenedBy;
	public final Label lblClientPhoneTitle;
	public final Label lblClientPhone;
	public final Label lblClosedByTitle;
	public final Label lblClosedBy;

	public OrderInformationComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout(10, false);
		gridLayout.horizontalSpacing = 10;
		setLayout(gridLayout);

		setBackground(COLOR_BACKGROUND);

		lblOrderNumberTitle = new Label(this, SWT.NONE);
		lblOrderNumberTitle.setText("Order#:");
		lblOrderNumberTitle.setFont(FONT_TITLE);
		lblOrderNumberTitle.setBackground(COLOR_BACKGROUND);
		lblOrderNumberTitle.setForeground(COLOR_FOREGROUND);

		lblOrderNumber = new Label(this, SWT.NONE);
		lblOrderNumber.setFont(FONT_TEXT);
		lblOrderNumber.setBackground(COLOR_BACKGROUND);
		lblOrderNumber.setForeground(COLOR_FOREGROUND);

		lblOrderStatusTitle = new Label(this, SWT.NONE);
		lblOrderStatusTitle.setText("Order Status:");
		lblOrderStatusTitle.setFont(FONT_TITLE);
		lblOrderStatusTitle.setBackground(COLOR_BACKGROUND);
		lblOrderStatusTitle.setForeground(COLOR_FOREGROUND);

		lblOrderStatus = new Label(this, SWT.NONE);
		lblOrderStatus.setFont(FONT_TEXT);
		lblOrderStatus.setBackground(COLOR_BACKGROUND);
		lblOrderStatus.setForeground(COLOR_FOREGROUND);

		lblClientTitle = new Label(this, SWT.NONE);
		lblClientTitle.setText("Client:");
		lblClientTitle.setFont(FONT_TITLE);
		lblClientTitle.setBackground(COLOR_BACKGROUND);
		lblClientTitle.setForeground(COLOR_FOREGROUND);

		lblClient = new Label(this, SWT.NONE);
		lblClient.setFont(FONT_TEXT);
		lblClient.setBackground(COLOR_BACKGROUND);
		lblClient.setForeground(COLOR_FOREGROUND);

		lblOpenedOnTitle = new Label(this, SWT.NONE);
		lblOpenedOnTitle.setText("Opened On:");
		lblOpenedOnTitle.setFont(FONT_TITLE);
		lblOpenedOnTitle.setBackground(COLOR_BACKGROUND);
		lblOpenedOnTitle.setForeground(COLOR_FOREGROUND);

		lblOpenedOn = new Label(this, SWT.NONE);
		lblOpenedOn.setFont(FONT_TEXT);
		lblOpenedOn.setBackground(COLOR_BACKGROUND);
		lblOpenedOn.setForeground(COLOR_FOREGROUND);

		lblClosedOnTitle = new Label(this, SWT.NONE);
		lblClosedOnTitle.setText("Closed On:");
		lblClosedOnTitle.setFont(FONT_TITLE);
		lblClosedOnTitle.setBackground(COLOR_BACKGROUND);
		lblClosedOnTitle.setForeground(COLOR_FOREGROUND);

		lblClosedOn = new Label(this, SWT.NONE);
		lblClosedOn.setFont(FONT_TEXT);
		lblClosedOn.setBackground(COLOR_BACKGROUND);
		lblClosedOn.setForeground(COLOR_FOREGROUND);

		lblTotalTitle = new Label(this, SWT.NONE);
		lblTotalTitle.setText("Total:");
		lblTotalTitle.setFont(FONT_TITLE);
		lblTotalTitle.setBackground(COLOR_BACKGROUND);
		lblTotalTitle.setForeground(COLOR_FOREGROUND);

		lblTotal = new Label(this, SWT.NONE);
		lblTotal.setFont(FONT_TEXT);
		lblTotal.setBackground(COLOR_BACKGROUND);
		lblTotal.setForeground(COLOR_FOREGROUND);

		lblOrderTypeTitle = new Label(this, SWT.NONE);
		lblOrderTypeTitle.setText("Order Type:");
		lblOrderTypeTitle.setFont(FONT_TITLE);
		lblOrderTypeTitle.setBackground(COLOR_BACKGROUND);
		lblOrderTypeTitle.setForeground(COLOR_FOREGROUND);

		lblOrderType = new Label(this, SWT.NONE);
		lblOrderType.setFont(FONT_TEXT);
		lblOrderType.setBackground(COLOR_BACKGROUND);
		lblOrderType.setForeground(COLOR_FOREGROUND);

		lblClientPhoneTitle = new Label(this, SWT.NONE);
		lblClientPhoneTitle.setText("Phone:");
		lblClientPhoneTitle.setFont(FONT_TITLE);
		lblClientPhoneTitle.setBackground(COLOR_BACKGROUND);
		lblClientPhoneTitle.setForeground(COLOR_FOREGROUND);

		lblClientPhone = new Label(this, SWT.NONE);
		lblClientPhone.setFont(FONT_TEXT);
		lblClientPhone.setBackground(COLOR_BACKGROUND);
		lblClientPhone.setForeground(COLOR_FOREGROUND);

		lblOpenedByTitle = new Label(this, SWT.NONE);
		lblOpenedByTitle.setText("Opened By:");
		lblOpenedByTitle.setFont(FONT_TITLE);
		lblOpenedByTitle.setBackground(COLOR_BACKGROUND);
		lblOpenedByTitle.setForeground(COLOR_FOREGROUND);

		lblOpenedBy = new Label(this, SWT.NONE);
		lblOpenedBy.setFont(FONT_TEXT);
		lblOpenedBy.setBackground(COLOR_BACKGROUND);
		lblOpenedBy.setForeground(COLOR_FOREGROUND);

		lblClosedByTitle = new Label(this, SWT.NONE);
		lblClosedByTitle.setText("Closed By:");
		lblClosedByTitle.setFont(FONT_TITLE);
		lblClosedByTitle.setBackground(COLOR_BACKGROUND);
		lblClosedByTitle.setForeground(COLOR_FOREGROUND);

		lblClosedBy = new Label(this, SWT.NONE);
		lblClosedBy.setFont(FONT_TEXT);
		lblClosedBy.setBackground(COLOR_BACKGROUND);
		lblClosedBy.setForeground(COLOR_FOREGROUND);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
