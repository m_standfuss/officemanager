package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class SaleComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmEditSale;
	public final ToolItem tlItmDeleteSale;

	public final Label lblSaleItemHeader;
	public final Label lblAmountOffHeader;
	public final Label lblSaleTypeHeader;
	public final Label lblSaleItem;
	public final Label lblAmountOff;
	public final Label lblSaleType;
	public final Label lblStartingDateHeader;
	public final Label lblEndingDateHeader;
	public final Label lblStartingDate;
	public final Label lblEndingDate;
	public final Label lblSaleNameHeader;
	public final Label lblSaleName;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public SaleComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 25;
		gridLayout.verticalSpacing = 10;
		gridLayout.numColumns = 4;
		setLayout(gridLayout);

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));

		tlItmEditSale = new ToolItem(toolBar, SWT.NONE);
		tlItmEditSale.setImage(ResourceManager.getImage(ImageFile.SALE_EDIT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmEditSale.setToolTipText("Edit Sale");
		tlItmEditSale.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_SALE_DETAILS));

		tlItmDeleteSale = new ToolItem(toolBar, SWT.NONE);
		tlItmDeleteSale.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmDeleteSale.setToolTipText("End Sale");
		tlItmDeleteSale.setEnabled(AppPrefs.hasPermission(AppPermissions.END_SALE));

		lblSaleItemHeader = new Label(this, SWT.NONE);
		lblSaleItemHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSaleItemHeader.setText("Sale Item:");
		lblSaleItemHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblSaleItem = new Label(this, SWT.NONE);

		lblSaleNameHeader = new Label(this, SWT.NONE);
		lblSaleNameHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSaleNameHeader.setText("Sale Name:");
		lblSaleNameHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblSaleName = new Label(this, SWT.NONE);

		lblSaleTypeHeader = new Label(this, SWT.NONE);
		lblSaleTypeHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSaleTypeHeader.setText("Sale Type:");
		lblSaleTypeHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblSaleType = new Label(this, SWT.NONE);

		lblStartingDateHeader = new Label(this, SWT.NONE);
		lblStartingDateHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblStartingDateHeader.setText("Starting Date:");
		lblStartingDateHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblStartingDate = new Label(this, SWT.NONE);

		lblAmountOffHeader = new Label(this, SWT.NONE);
		lblAmountOffHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAmountOffHeader.setText("Amount Off:");
		lblAmountOffHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblAmountOff = new Label(this, SWT.NONE);

		lblEndingDateHeader = new Label(this, SWT.NONE);
		lblEndingDateHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEndingDateHeader.setText("Ending Date:");
		lblEndingDateHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblEndingDate = new Label(this, SWT.NONE);
		Composite btnBarCmp = new Composite(this, SWT.NONE);
		btnBarCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 6, 1));
		btnBarCmp.setLayout(new GridLayout(2, false));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
