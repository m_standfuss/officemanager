package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.DecimalText;
import officemanager.gui.swt.ui.widgets.NumberText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class PartEditComposite extends Composite {
	public final Label lblRequiredFields;
	public final Label lblPartName;
	public final UpperCaseText txtPartName;
	public final Label lblDescription;
	public final UpperCaseText txtDescription;
	public final Label lblBasePrice;
	public final DecimalText txtBasePrice;
	public final Label lblBarcode;
	public final Text txtBarcode;
	public final Label lblInventoryCount;
	public final NumberText txtInventoryCount;
	public final Label lblInventoryThreshold;
	public final NumberText txtInventoryThreshold;
	public final Label lblCategory;
	public final CodeValueCombo cmbCategory;
	public final Label lblProductType;
	public final CodeValueCombo cmbProductType;
	public final Label lblMaufacturer;
	public final CodeValueCombo cmbManufacturer;
	public final Label lblManufacturerId;
	public final Text txtManufacturerID;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PartEditComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 20;
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		lblPartName = new Label(this, SWT.NONE);
		lblPartName.setText("Part Name*");
		lblPartName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblBasePrice = new Label(this, SWT.NONE);
		lblBasePrice.setText("Base Price*");
		lblBasePrice.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtPartName = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtPartName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtPartName.widthHint = 150;
		txtPartName.setLayoutData(gd_txtPartName);

		txtBasePrice = new DecimalText(2, this, SWT.BORDER);
		GridData gd_txtBasePrice = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtBasePrice.widthHint = 75;
		txtBasePrice.setLayoutData(gd_txtBasePrice);
		txtBasePrice.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_PART_PRICE));

		lblCategory = new Label(this, SWT.NONE);
		lblCategory.setText("Category*");
		lblCategory.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblMaufacturer = new Label(this, SWT.NONE);
		lblMaufacturer.setText("Maufacturer*");
		lblMaufacturer.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbCategory = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbCategory = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbCategory.widthHint = 150;
		cmbCategory.setLayoutData(gd_cmbCategory);

		cmbManufacturer = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_combo_1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_combo_1.widthHint = 150;
		cmbManufacturer.setLayoutData(gd_combo_1);

		lblProductType = new Label(this, SWT.NONE);
		lblProductType.setText("Product Type*");
		lblProductType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblManufacturerId = new Label(this, SWT.NONE);
		lblManufacturerId.setText("Manufacturer ID");
		lblManufacturerId.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbProductType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbProductType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbProductType.widthHint = 150;
		cmbProductType.setLayoutData(gd_cmbProductType);

		txtManufacturerID = new Text(this, SWT.BORDER);
		GridData gd_txtManufacturerID = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtManufacturerID.widthHint = 100;
		txtManufacturerID.setLayoutData(gd_txtManufacturerID);

		lblDescription = new Label(this, SWT.NONE);
		lblDescription.setText("Description");
		lblDescription.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblBarcode = new Label(this, SWT.NONE);
		lblBarcode.setText("Barcode");
		lblBarcode.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtDescription = new UpperCaseText(this, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescription = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 5);
		gd_txtDescription.widthHint = 200;
		gd_txtDescription.heightHint = 150;
		txtDescription.setLayoutData(gd_txtDescription);

		txtBarcode = new Text(this, SWT.BORDER);
		GridData gd_txtBarcode = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtBarcode.widthHint = 100;
		txtBarcode.setLayoutData(gd_txtBarcode);

		lblInventoryCount = new Label(this, SWT.NONE);
		lblInventoryCount.setText("Inventory Count");
		lblInventoryCount.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtInventoryCount = new NumberText(this, SWT.BORDER);
		GridData gd_txtInventoryCount = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtInventoryCount.widthHint = 50;
		txtInventoryCount.setLayoutData(gd_txtInventoryCount);

		lblInventoryThreshold = new Label(this, SWT.NONE);
		lblInventoryThreshold.setText("Inventory Threshold");
		lblInventoryThreshold.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtInventoryThreshold = new NumberText(this, SWT.BORDER);
		GridData gd_txtInventoryThreshold = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_txtInventoryThreshold.widthHint = 50;
		txtInventoryThreshold.setLayoutData(gd_txtInventoryThreshold);

		lblRequiredFields = new Label(this, SWT.NONE);
		lblRequiredFields.setText("* Denotes a required field");
		lblRequiredFields.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblRequiredFields.setFont(ResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
