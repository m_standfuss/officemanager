package officemanager.gui.swt.ui.tables;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.MessageDialogs;

public class OfficeManagerTable extends Table {

	public static final int NO_EXTEND_LAST_COLUMN = 131072;
	public static final int NO_ZEBRA_STRIPE = 262144;

	private final boolean extendLastColumn;
	private final boolean zebraStripe;
	private final List<Listener> checkListeners;

	private int maxChecked;

	public OfficeManagerTable(Composite parent, int style) {
		super(parent, style);
		extendLastColumn = (style & NO_EXTEND_LAST_COLUMN) == 0;
		zebraStripe = (style & NO_ZEBRA_STRIPE) == 0;

		maxChecked = Integer.MAX_VALUE;
		addListener(SWT.Selection, e -> {
			if (e.detail != SWT.CHECK) {
				return;
			}
			fireCheckListeners(e);
		});
		addListener(SWT.Resize, e -> extendLastColumn());
		checkListeners = Lists.newArrayList(e -> checkCheckCount(e));
		setLinesVisible(false);
	}

	public void addCheckListener(Listener l) {
		checkNotNull(l);
		checkListeners.add(l);
	}

	/**
	 * Sets the max allowed check items. If one is the max then selecting
	 * another row will toggle the original rows checked state. Else the user
	 * will be blocked from checking another item.
	 * 
	 * @param maxCheckedItems
	 *            The number of items to limit checked rows to.
	 */
	public void setMaxCheckedItems(int maxCheckedItems) {
		maxChecked = maxCheckedItems;
	}

	/**
	 * Packs tables columns.
	 * 
	 * @param table
	 *            The table to pack columns for.
	 * @throws NullPointerException
	 *             if table is null.
	 */
	public void packTableColumns() {
		packTableColumns(Lists.newArrayList());
	}

	/**
	 * Packs the table's columns skipping any column that has in index on the
	 * skipColumns parameter list.
	 * 
	 * Great whenever a column has really long text but you don't want to wrap
	 * it.
	 * 
	 * @param table
	 *            The table to pack columns for.
	 * @param skipColumns
	 *            The columns to skip packing.
	 * @throws NullPointerException
	 *             If either parameter is null.
	 */
	public void packTableColumns(List<Integer> skipColumns) {
		checkNotNull(skipColumns);
		for (int i = 0; i < getColumnCount(); i++) {
			if (skipColumns.contains(i)) {
				continue;
			}
			getColumn(i).pack();
		}
		redraw();
		layout();
		extendLastColumn();
		zebraStripeColumns();
	}

	private void zebraStripeColumns() {
		if (!zebraStripe) {
			return;
		}
		for (int i = 0; i < getItemCount(); i++) {
			getItem(i).setBackground((i % 2 == 0) ? AppPrefs.COLOR_ROW : AppPrefs.COLOR_OFFSET_ROW);
		}
	}

	/**
	 * Hides the column by shrinking its size to 0 and setting its resizable
	 * boolean to false.
	 * 
	 * @param table
	 *            The table to hide the column on.
	 * @param columnIndex
	 *            The column index of the column to hide.
	 * @throws NullPointerException
	 *             if table is null
	 * @throws IllegalArgumentException
	 *             if column index does not exist for table.
	 * 
	 */
	public void hideColumn(int columnIndex) {
		checkArgument(columnIndex >= 0 && columnIndex < getColumnCount());
		getColumn(columnIndex).setWidth(0);
		getColumn(columnIndex).setResizable(false);
	}

	public void hideColumns(List<Integer> columnsToSkip) {
		for (Integer columnIndex : columnsToSkip) {
			hideColumn(columnIndex);
		}
	}

	/**
	 * Unhides the column by defaulting its width to 100 and setting the
	 * resizable boolean to true.
	 * 
	 * @param table
	 *            The table to unhide the column for.
	 * @param columnIndex
	 *            The index of the column.
	 */
	public void unhideColumn(int columnIndex) {
		unhideColumn(columnIndex, 100);
	}

	/**
	 * Unhides the column by setting its width to the provided amount and the
	 * resizable boolean to true.
	 * 
	 * @param table
	 *            The table to unhide the column for.
	 * @param columnIndex
	 *            The index of the column.
	 * @param width
	 *            The width to set the column to.
	 */
	public void unhideColumn(int columnIndex, int width) {
		checkArgument(columnIndex >= 0 && columnIndex < getColumnCount());
		getColumn(columnIndex).setWidth(width);
		getColumn(columnIndex).setResizable(true);
	}

	/**
	 * Sets the checked status of all table items on the table.
	 * 
	 * @param table
	 *            The table to check/uncheck.
	 * @param checked
	 *            Whether to check or uncheck.
	 * @throws NullPointerException
	 *             If table is null.
	 */
	public void checkAllItems(boolean checked) {
		for (final TableItem item : getItems()) {
			item.setChecked(checked);
		}
	}

	public List<TableItem> getCheckedItems() {
		List<TableItem> checked = Lists.newArrayList();
		for (TableItem item : getItems()) {
			if (item.getChecked()) {
				checked.add(item);
			}
		}
		return checked;
	}

	public int getCheckedCount() {
		return getCheckedItems().size();
	}

	/**
	 * Removes all of the items in the table that are not checked.
	 * 
	 * @param table
	 *            The table to remove items from
	 * @throws NullPointerException
	 *             If table is null.
	 */
	public void removeUncheckedItems() {
		for (int i = getItemCount() - 1; i >= 0; i--) {
			if (getItem(i).getChecked()) {
				continue;
			}
			remove(i);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getDataAll(String key) {
		List<T> list = Lists.newArrayList();
		for (TableItem item : getItems()) {
			list.add((T) item.getData(key));
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getCheckedData(String key) {
		List<T> list = Lists.newArrayList();
		for (TableItem item : getItems()) {
			if (item.getChecked()) {
				list.add((T) item.getData(key));
			}
		}
		return list;
	}

	public <T> T getSelectedData(String key) {
		List<T> list = getSelectedDataAll(key);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getSelectedDataAll(String key) {
		List<T> list = Lists.newArrayList();
		for (TableItem item : getSelection()) {
			list.add((T) item.getData(key));
		}
		return list;
	}

	private void fireCheckListeners(Event e) {
		for (Listener l : checkListeners) {
			l.handleEvent(e);
		}
	}

	private void checkCheckCount(Event e) {
		if (!((TableItem) e.item).getChecked()) {
			// If unselecting always allow to happen.
			return;
		}
		if (getCheckedCount() <= maxChecked) {
			// Under the limit dont care.
			return;
		}

		if (maxChecked == 1) {
			// toggle check
			checkAllItems(false);
			((TableItem) e.item).setChecked(true);
		} else {
			// disallow check
			MessageDialogs.displayError(getShell(), "Unable To Check", "Maximum selected allowed.");
			((TableItem) e.item).setChecked(false);
		}
	}

	private void extendLastColumn() {
		if (!extendLastColumn) {
			return;
		}
		int tableWidth = getClientArea().width;
		int totalColumnWidth = 0;
		int lastColumn = -1;
		for (int i = 0; i < getColumnCount(); i++) {
			int columnWidth = getColumn(i).getWidth();
			totalColumnWidth += columnWidth;
			if (columnWidth > 0) {
				lastColumn = i;
			}
		}

		int diff = tableWidth - totalColumnWidth;
		if (diff > 0 && lastColumn != -1) {
			int newWidth = diff + getColumn(lastColumn).getWidth();
			getColumn(lastColumn).setWidth(newWidth);
		}
	}

	@Override
	protected void checkSubclass() {}
}
