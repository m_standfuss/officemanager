package officemanager.gui.swt.ui.dialogs;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.google.common.collect.Lists;

import officemanager.gui.swt.ui.composites.HelpComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.Tuple;

public class HelpDialog extends Dialog {

	private static final Logger logger = LogManager.getLogger(HelpDialog.class);

	private static final int WIDTH = 1000;
	private static final int HEIGHT = 500;

	private static final String HELP_FILE_DIR = "/helpfiles/";
	private static final String HELP_CONTENTS_FILE = HELP_FILE_DIR + "contents.xml";

	private static final String DATA_IS_DOC = "DATA_IS_DOC";
	private static final String DATA_DOC_FILE_NAME = "DATA_DOC_FILE_NAME";

	private Shell shell;
	private HelpComposite composite;

	private boolean documentLoaded;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public HelpDialog(Shell parent, int style) {
		super(parent, style);
		documentLoaded = false;
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public void open() {
		logger.debug("Opening the help dialog.");
		createContents();
		addListeners();
		populateHelpFiles();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.SHELL_TRIM | SWT.BORDER);
		shell.setImage(ResourceManager.getIcon());
		shell.setSize(WIDTH, HEIGHT);
		shell.setText("Help Content");
		shell.setLayout(new GridLayout());

		Rectangle parentSize = getParent().getBounds();

		int locationX, locationY;
		locationX = (parentSize.width - WIDTH) / 2 + parentSize.x;
		locationY = (parentSize.height - HEIGHT) / 2 + parentSize.y;

		shell.setLocation(new Point(locationX, locationY));

		composite = new HelpComposite(shell, SWT.BORDER);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		final GridData gridData = (GridData) composite.sashForm.getLayoutData();
		gridData.verticalAlignment = SWT.FILL;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.grabExcessHorizontalSpace = true;
		composite.setLayout(new GridLayout(1, false));
	}

	private void addListeners() {
		composite.treeFull.addListener(SWT.MouseDoubleClick, e -> openDocument(composite.treeFull));
		composite.treeFiltered.addListener(SWT.MouseDoubleClick, e -> openDocument(composite.treeFiltered));
		composite.txtSearch.addListener(SWT.Modify, e -> search());
		composite.tltmPrint.addListener(SWT.Selection, e -> print());
	}

	private void openDocument(Tree tree) {
		TreeItem item = tree.getSelection()[0];
		if ((boolean) item.getData(DATA_IS_DOC)) {
			String docFile = (String) item.getData(DATA_DOC_FILE_NAME);

			InputStream in = null;
			String documentText = "";
			try {
				in = HelpDialog.class.getResourceAsStream(HELP_FILE_DIR + docFile);
				documentText = IOUtils.toString(in, StandardCharsets.UTF_8);
			} catch (IOException e) {
				MessageDialogs.displayError(shell, "Unable to Show Document", "The help file was unable to be loaded.");
				logger.error("Unable to read in help file.", e);
			} finally {
				IOUtils.closeQuietly(in);
			}
			composite.browser.setText(documentText);
			documentLoaded = true;
		}
	}

	private void search() {
		String searchOn = composite.txtSearch.getText().trim().toLowerCase();
		logger.debug("Searching for documents with '" + searchOn + "'.");
		if (searchOn.trim().isEmpty()) {
			((StackLayout) composite.compTrees.getLayout()).topControl = composite.treeFull;
		} else {
			composite.treeFiltered.removeAll();
			List<TreeItem> documentElements = getAllDocuments();
			documentElements.sort((o1, o2) -> o1.getText().compareTo(o2.getText()));
			documentElements.forEach(item -> {
				if (item.getText().toLowerCase().contains(searchOn)) {
					makeDocumentItem(composite.treeFiltered, item.getText(), (String) item.getData(DATA_DOC_FILE_NAME));
				}
			});
			((StackLayout) composite.compTrees.getLayout()).topControl = composite.treeFiltered;
		}
		composite.compTrees.layout();
	}

	private void print() {
		if (!documentLoaded) {
			MessageDialog.openError(shell, "Unable to Print", "No document is currently selected to be printed.");
			return;
		}
		composite.browser.execute("javascript:window.print();");
	}

	private List<TreeItem> getAllDocuments() {
		List<TreeItem> documentItems = Lists.newArrayList();
		for (TreeItem item : composite.treeFull.getItems()) {
			documentItems.addAll(getDocuments(item));
		}
		return documentItems;
	}

	private List<TreeItem> getDocuments(TreeItem item) {
		List<TreeItem> documents = Lists.newArrayList();
		if ((boolean) item.getData(DATA_IS_DOC)) {
			documents.add(item);
		}
		for (TreeItem child : item.getItems()) {
			documents.addAll(getDocuments(child));
		}
		return documents;
	}

	private void populateHelpFiles() {
		SAXBuilder builder = new SAXBuilder();
		InputStream in = HelpDialog.class.getResourceAsStream(HELP_CONTENTS_FILE);

		try {
			Document xmlDoc = builder.build(in);
			Element rootNode = xmlDoc.getRootElement();
			List<Element> categories = rootNode.getChildren("category");

			List<HelpCategory> categoryItems = Lists.newArrayList();
			for (Element category : categories) {
				HelpCategory categoryItem = new HelpCategory();
				categoryItem.setName(category.getAttributeValue("name"));

				List<Element> subCategories = category.getChildren("sub");
				for (Element subCategory : subCategories) {
					HelpCategory subCategoryItem = new HelpCategory();
					subCategoryItem.setName(subCategory.getAttributeValue("name"));
					List<Element> documents = subCategory.getChildren("document");
					for (Element document : documents) {
						String docName = document.getAttributeValue("name");
						String docFile = document.getValue();
						subCategoryItem.getDocuments().add(Tuple.newTuple(docName, docFile));
					}
					categoryItem.getSubCategories().add(subCategoryItem);
				}

				List<Element> documents = category.getChildren("document");
				for (Element document : documents) {
					String docName = document.getAttributeValue("name");
					String docFile = document.getValue();
					categoryItem.getDocuments().add(Tuple.newTuple(docName, docFile));
				}
				categoryItems.add(categoryItem);
			}

			for (HelpCategory category : categoryItems) {
				TreeItem categoryItem = new TreeItem(composite.treeFull, SWT.NONE);
				categoryItem.setImage(ResourceManager.getImage(ImageFile.BOOK, 16));
				categoryItem.setText(category.getName());
				categoryItem.setData(DATA_IS_DOC, false);
				for (Tuple<String, String> document : category.getDocuments()) {
					makeDocumentItem(categoryItem, document.item1, document.item2);
				}

				for (HelpCategory subCategory : category.getSubCategories()) {
					TreeItem subCategoryItem = new TreeItem(categoryItem, SWT.NONE);
					subCategoryItem.setImage(ResourceManager.getImage(ImageFile.BOOK_OPEN, 16));
					subCategoryItem.setText(subCategory.getName());
					subCategoryItem.setData(DATA_IS_DOC, false);
					for (Tuple<String, String> document : subCategory.getDocuments()) {
						makeDocumentItem(subCategoryItem, document.item1, document.item2);
					}
				}
			}

		} catch (IOException | JDOMException e) {
			logger.error("Unable to parse help content xml file.", e);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	private TreeItem makeDocumentItem(Tree parent, String docName, String docFile) {
		TreeItem documentItem = new TreeItem(parent, SWT.NONE);
		documentItem.setText(docName);
		documentItem.setImage(ResourceManager.getImage(ImageFile.DOCUMENT, 16));
		documentItem.setData(DATA_IS_DOC, true);
		documentItem.setData(DATA_DOC_FILE_NAME, docFile);
		return documentItem;
	}

	private TreeItem makeDocumentItem(TreeItem parent, String docName, String docFile) {
		TreeItem documentItem = new TreeItem(parent, SWT.NONE);
		documentItem.setText(docName);
		documentItem.setImage(ResourceManager.getImage(ImageFile.DOCUMENT, 16));
		documentItem.setData(DATA_IS_DOC, true);
		documentItem.setData(DATA_DOC_FILE_NAME, docFile);
		return documentItem;
	}

	private class HelpCategory {
		private String name = "";
		private final List<HelpCategory> subCategories = Lists.newArrayList();
		private final List<Tuple<String, String>> documents = Lists.newArrayList();

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<HelpCategory> getSubCategories() {
			return subCategories;
		}

		public List<Tuple<String, String>> getDocuments() {
			return documents;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("HelpCategory [name=").append(name).append(", subCategories=").append(subCategories)
					.append(", documents=").append(documents).append("]");
			return builder.toString();
		}
	}

}
