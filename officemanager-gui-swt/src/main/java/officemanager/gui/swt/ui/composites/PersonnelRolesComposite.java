package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.DateAndTime;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelRolesComposite extends Composite {
	private static final Font FONT_TITLE = ResourceManager.getFont("Segoe UI", 8, SWT.BOLD);
	public Label lblAddRole;
	public Label lblSeperator1;
	public Composite compositeNewRole;
	public Label lblRole;
	public Label lblEndEffective;
	public Button btnNeverEnding;
	public Combo cmbRole;
	public DateAndTime dateAndTimeEndEffective;
	public ScrolledComposite scrolledCompositeRoles;
	public Composite compositeRoles;
	public Label lblCurrentRoles;
	public Label lblSeperator2;
	public Button btnAddRole;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PersonnelRolesComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		lblAddRole = new Label(this, SWT.NONE);
		lblAddRole.setText("Add Role");

		lblSeperator1 = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblSeperator1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		compositeNewRole = new Composite(this, SWT.NONE);
		compositeNewRole.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		compositeNewRole.setLayout(new GridLayout(2, false));

		lblRole = new Label(compositeNewRole, SWT.NONE);
		lblRole.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRole.setText("Role:");

		cmbRole = new Combo(compositeNewRole, SWT.READ_ONLY);
		GridData gd_cmbRole = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbRole.widthHint = 150;
		cmbRole.setLayoutData(gd_cmbRole);

		btnNeverEnding = new Button(compositeNewRole, SWT.CHECK);
		btnNeverEnding.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnNeverEnding.setText("Never-Ending");

		lblEndEffective = new Label(compositeNewRole, SWT.NONE);
		lblEndEffective.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblEndEffective.setText("End Effective");

		dateAndTimeEndEffective = new DateAndTime(compositeNewRole, SWT.NONE);
		dateAndTimeEndEffective.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		btnAddRole = new Button(compositeNewRole, SWT.NONE);
		GridData gd_btnAddRole = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_btnAddRole.widthHint = 75;
		btnAddRole.setLayoutData(gd_btnAddRole);
		btnAddRole.setText("Add");
		new Label(compositeNewRole, SWT.NONE);
		new Label(compositeNewRole, SWT.NONE);

		lblCurrentRoles = new Label(this, SWT.NONE);
		lblCurrentRoles.setText("Current Roles");

		lblSeperator2 = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblSeperator2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		scrolledCompositeRoles = new ScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledCompositeRoles.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		scrolledCompositeRoles.setExpandHorizontal(true);
		scrolledCompositeRoles.setExpandVertical(true);

		compositeRoles = new Composite(scrolledCompositeRoles, SWT.NONE);
		compositeRoles.setLayout(new GridLayout(3, false));

		Label lblRoleName = new Label(compositeRoles, SWT.NONE);
		lblRoleName.setText("Role Name");
		lblRoleName.setFont(FONT_TITLE);

		Label lblEndEffective = new Label(compositeRoles, SWT.NONE);
		lblEndEffective.setText("Effective Until");
		lblEndEffective.setFont(FONT_TITLE);
		new Label(compositeRoles, SWT.NONE);

		scrolledCompositeRoles.setContent(compositeRoles);
		scrolledCompositeRoles.setMinSize(compositeRoles.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
