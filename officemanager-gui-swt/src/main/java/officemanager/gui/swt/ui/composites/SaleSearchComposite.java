package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.SaleTableComposite;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class SaleSearchComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmNewSale;

	public final ScrolledComposite scrCompositeSideBar;
	public final Composite compositeSideBar;

	public final Label lblSaleItem;
	public final UpperCaseText txtSaleItem;
	public final Label lblSaleType;
	public final Label lblSaleName;
	public final UpperCaseText txtSaleName;
	public final Button btnSearch;
	public final Button btnClear;

	public final Composite compositeCenter;
	public final Composite compositeDateRange;
	public final Label lblSalesActiveFrom;
	public final Label lblToDate;
	public final DateTime dateTimeStart;
	public final DateTime dateTimeEnd;

	public final SaleTableComposite searchResultsTable;
	public Button btnAmountOff;
	public Button btnPercentageOff;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public SaleSearchComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		tlItmNewSale = new ToolItem(toolBar, SWT.NONE);
		tlItmNewSale.setImage(ResourceManager.getImage(ImageFile.SALE_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmNewSale.setToolTipText("New Sale");
		tlItmNewSale.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_SALE));

		scrCompositeSideBar = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeSideBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		compositeSideBar = new Composite(scrCompositeSideBar, SWT.NONE);
		compositeSideBar.setLayout(new GridLayout(2, false));

		lblSaleItem = new Label(compositeSideBar, SWT.NONE);
		lblSaleItem.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		lblSaleItem.setText("Sale Item");
		lblSaleItem.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtSaleItem = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_txtSaleItem = new GridData(SWT.LEFT, SWT.FILL, true, false, 2, 1);
		gd_txtSaleItem.widthHint = 175;
		txtSaleItem.setLayoutData(gd_txtSaleItem);

		lblSaleName = new Label(compositeSideBar, SWT.NONE);
		lblSaleName.setText("Sale Name");
		lblSaleName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		new Label(compositeSideBar, SWT.NONE);

		txtSaleName = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_txtSaleName = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
		gd_txtSaleName.widthHint = 175;
		txtSaleName.setLayoutData(gd_txtSaleName);

		lblSaleType = new Label(compositeSideBar, SWT.NONE);
		lblSaleType.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblSaleType.setText("Sale Type");
		lblSaleType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		btnAmountOff = new Button(compositeSideBar, SWT.CHECK);
		btnAmountOff.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnAmountOff.setText("Amount Off");
		btnAmountOff.setSelection(true);

		btnPercentageOff = new Button(compositeSideBar, SWT.CHECK);
		btnPercentageOff.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnPercentageOff.setText("Percentage Off");
		btnPercentageOff.setSelection(true);

		btnSearch = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnSearch = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnSearch.widthHint = 100;
		btnSearch.setLayoutData(gd_btnSearch);
		btnSearch.setText("Search");

		btnClear = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnClear = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnClear.widthHint = 100;
		btnClear.setLayoutData(gd_btnClear);
		btnClear.setText("Clear");
		btnClear.addListener(SWT.Selection, e -> clear());

		scrCompositeSideBar.setContent(compositeSideBar);
		scrCompositeSideBar.setMinSize(compositeSideBar.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		compositeCenter = new Composite(this, SWT.BORDER);
		compositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeCenter.setLayout(new GridLayout(1, false));

		compositeDateRange = new Composite(compositeCenter, SWT.NONE);
		compositeDateRange.setLayout(new GridLayout(4, false));
		compositeDateRange.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		compositeDateRange.setSize(64, 64);

		lblSalesActiveFrom = new Label(compositeDateRange, SWT.NONE);
		lblSalesActiveFrom.setText("Sales active from");
		lblSalesActiveFrom.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		dateTimeStart = new DateTime(compositeDateRange, SWT.BORDER | SWT.DROP_DOWN);

		lblToDate = new Label(compositeDateRange, SWT.NONE);
		lblToDate.setText("to");
		lblToDate.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		dateTimeEnd = new DateTime(compositeDateRange, SWT.BORDER | SWT.DROP_DOWN);

		searchResultsTable = new SaleTableComposite(compositeCenter, SWT.NONE, SWT.BORDER | SWT.FULL_SELECTION);
		searchResultsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	private void clear() {
		txtSaleItem.setText("");
		txtSaleName.setText("");
		btnAmountOff.setSelection(true);
		btnPercentageOff.setSelection(true);
	}
}
