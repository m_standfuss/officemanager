package officemanager.gui.swt.ui.wizards.pages;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.LongTextRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.CommentEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class CommentPageNew extends OfficeManagerWizardPage {

	private CommentEditComposite editComposite;

	/**
	 * Create the wizard.
	 */
	public CommentPageNew() {
		super("CommentPageNew", "Add Comment", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.COMMENT, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("Enter a new comment for the order.");
		setPageComplete(false);
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout());

		editComposite = new CommentEditComposite(container, SWT.NONE);
		editComposite.cmbCommentType.addListener(SWT.Selection,
				e -> setPageComplete(editComposite.cmbCommentType.getSelectionIndex() != -1));
		editComposite.txtAuthor.setText(AppPrefs.currentSession.nameFullFormatted);
		editComposite.txtAuthor.setEnabled(false);
		editComposite.txtLastUpdatedBy.setEnabled(false);
		editComposite.txtLastUpdatedOn.setEnabled(false);

		setControl(container);
	}

	@Override
	public void setFocus() {
		editComposite.cmbCommentType.setFocus();
	}

	public OrderCommentRecord getCommentRecord() {
		OrderCommentRecord record = new OrderCommentRecord();
		record.setAuthoredBy(editComposite.txtAuthor.getText());
		record.setCommentType(editComposite.cmbCommentType.getCodeValueRecord());
		record.setLongText(new LongTextRecord(editComposite.txtComment.getText()));
		return record;
	}

	public void populateCommentTypes(List<CodeValueRecord> commentTypes) {
		editComposite.cmbCommentType.populateSelections(commentTypes);
	}
}
