package officemanager.gui.swt.ui.wizards;

import officemanager.biz.delegates.AccountDelegate;
import officemanager.gui.swt.ui.wizards.pages.ClientSelectionPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;

public class AddAccountWizard extends OfficeManagerWizard {

	private final AccountDelegate accountDelegate;

	private ClientSelectionPage page;

	public AddAccountWizard() {
		setWindowTitle("Add Client Account");
		accountDelegate = new AccountDelegate();
	}

	@Override
	public void addPages() {
		page = new ClientSelectionPage(1, true);
		page.addFirstShowListener(l -> page.hideToolbar());
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		if (!MessageDialogs.askQuestion(getShell(), "Confirm Account",
				"Are you sure you wish to create a client credit account for this client?")) {
			return false;
		}
		Long clientId = page.getSelectedClientId();
		if (accountDelegate.hasAccount(clientId)) {
			MessageDialogs.displayError(getShell(), "Account Found",
					"Client choosen already has an account created. "
							+ "The status of that account can be managed in the account's page.");
			return false;
		}
		accountDelegate.addAccountToClient(clientId);
		MessageDialogs.displayMessage(getShell(), "Account Created", "Account was successfully created.");
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}
}
