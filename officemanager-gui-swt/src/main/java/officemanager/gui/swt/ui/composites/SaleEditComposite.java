package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.ui.widgets.DateAndTime;
import officemanager.gui.swt.ui.widgets.DecimalText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class SaleEditComposite extends Composite {
	public final Label lblSaleType;
	public final Button btnFlatAmountOff;
	public final Button btnPercentageOff;
	public final Label lblSaleName;
	public final UpperCaseText txtSaleName;
	public final Label lblAmountOff;
	public final Label lblSaleItemType;
	public final Button btnSearchItems;
	public final Label lblSaleItem;
	public final Text txtSaleItem;
	public final DecimalText txtAmountOff;
	public final Label lblStartingTime;
	public final Label lblEndingTime;
	public final Label lbldenotesARequired;
	public final Button btnNoEnd;
	public final Button btnPart;
	public final Button btnFlatRateService;
	public final Label lblForPercentagesFormat;
	public final DateAndTime dateAndTimeStart;
	public final DateAndTime dateAndTimeEnd;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public SaleEditComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 20;
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		lbldenotesARequired = new Label(this, SWT.NONE);
		lbldenotesARequired.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lbldenotesARequired.setText("*Denotes a required field");
		lbldenotesARequired.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));

		lblSaleType = new Label(this, SWT.NONE);
		lblSaleType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblSaleType.setText("Sale Type:*");

		lblSaleName = new Label(this, SWT.NONE);
		lblSaleName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblSaleName.setText("Sale Name:");

		Composite compositeSaleType = new Composite(this, SWT.NONE);
		compositeSaleType.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 2));
		compositeSaleType.setLayout(new GridLayout(1, false));

		btnFlatAmountOff = new Button(compositeSaleType, SWT.RADIO);
		btnFlatAmountOff.setText("Flat Amount Off");

		btnPercentageOff = new Button(compositeSaleType, SWT.RADIO);
		btnPercentageOff.setText("Percentage Off");

		txtSaleName = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtSaleName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtSaleName.widthHint = 175;
		txtSaleName.setLayoutData(gd_txtSaleName);

		lblSaleItemType = new Label(this, SWT.NONE);
		lblSaleItemType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblSaleItemType.setText("Sale Item Type:*");

		lblAmountOff = new Label(this, SWT.NONE);
		lblAmountOff.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblAmountOff.setText("Amount Off:*");

		Composite compositeSaleItemType = new Composite(this, SWT.NONE);
		compositeSaleItemType.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 2));
		compositeSaleItemType.setLayout(new GridLayout(1, false));

		btnPart = new Button(compositeSaleItemType, SWT.RADIO);
		btnPart.setText("Part");

		btnFlatRateService = new Button(compositeSaleItemType, SWT.RADIO);
		btnFlatRateService.setText("Flat Rate Service");

		txtAmountOff = new DecimalText(2, this, SWT.BORDER);
		GridData gd_txtAmountOff = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAmountOff.widthHint = 50;
		txtAmountOff.setLayoutData(gd_txtAmountOff);

		lblForPercentagesFormat = new Label(this, SWT.WRAP);
		lblForPercentagesFormat.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));
		GridData gd_lblForPercentagesFormat = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblForPercentagesFormat.widthHint = 184;
		lblForPercentagesFormat.setLayoutData(gd_lblForPercentagesFormat);
		lblForPercentagesFormat.setText("For percentages format as whole number (5 is 5% off not .05)");

		lblSaleItem = new Label(this, SWT.NONE);
		lblSaleItem.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblSaleItem.setText("Sale Item:*");

		lblStartingTime = new Label(this, SWT.NONE);
		lblStartingTime.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblStartingTime.setText("Starting Time:*");

		txtSaleItem = new Text(this, SWT.BORDER);
		txtSaleItem.setEditable(false);
		GridData gd_txtSaleItem = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtSaleItem.widthHint = 175;
		txtSaleItem.setLayoutData(gd_txtSaleItem);

		dateAndTimeStart = new DateAndTime(this, SWT.NONE);

		btnSearchItems = new Button(this, SWT.NONE);
		btnSearchItems.setText("Search Items");

		lblEndingTime = new Label(this, SWT.NONE);
		lblEndingTime.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblEndingTime.setText("Ending Time:");
		new Label(this, SWT.NONE);

		btnNoEnd = new Button(this, SWT.CHECK);
		btnNoEnd.setText("No End (can be updated later)");
		new Label(this, SWT.NONE);

		dateAndTimeEnd = new DateAndTime(this, SWT.NONE);
		new Label(this, SWT.NONE);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
