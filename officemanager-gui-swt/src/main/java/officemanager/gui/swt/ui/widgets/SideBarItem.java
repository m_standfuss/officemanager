package officemanager.gui.swt.ui.widgets;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolTip;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ResourceManager;

/**
 * Represents a side bar elements used for selecting a view.</br>
 * Contains space to display an optional icon on the left side of the composite
 * followed by a required message. All colors, fonts and icons are customizable
 * but the size is determined by the parent's layout so the size of the fonts
 * and icons should be taken into consideration when laying out the parent.</br>
 * </br>
 * The icon is given priority to the available space first, then the message
 * label is fit within the remaining space. If the message is too long then an
 * ellipsis will be appened to the portion of the label that will fit and a
 * tooltip for the entire text will be enabled.
 *
 */
public class SideBarItem extends Canvas {
	private static final String ELLIPSIS = "...";
	private static final int MARGIN = 6;
	private static final int ALPHA_HOVERING = 20;

	private static final Color COLOR_BACKGROUND = AppPrefs.COLOR_SIDEBAR;
	private static final Color COLOR_FOREGROUND = AppPrefs.COLOR_SIDEBAR_TEXT;

	private static final Color COLOR_BACKGROUND_SELECTED = AppPrefs.COLOR_SIDEBAR_SELECTED;
	private static final Color COLOR_FOREGROUND_SELECTED = AppPrefs.COLOR_SIDEBAR_SELECTED_TEXT;

	private static final Font FONT_MESSAGE = ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL);

	private final ToolTip tooltip;
	private final List<Listener> selectionListeners;

	private Image icon;

	private boolean messageTooLong;
	private String messageText;

	private boolean hovering;
	private boolean mouseDown;
	private boolean selected;

	/**
	 * Creates a new card composite within the parent. Note: the style is
	 * handled internally for this widget.
	 *
	 * @param parent
	 *            a composite control which will be the parent of the new
	 *            instance (cannot be null)
	 */
	public SideBarItem(Composite parent, int style) {
		super(parent, style);

		messageText = "";

		tooltip = new ToolTip(getShell(), SWT.NONE);
		selectionListeners = new ArrayList<Listener>();
		addListeners();
	}

	/**
	 * Adds a listener to be fired anytime that the selected state of this
	 * widget is changed.
	 *
	 * @param listener
	 *            The listener
	 * @throws IllegalArgumentException
	 *             if listener is null.
	 */
	public void addSelectionListener(Listener listener) {
		checkWidget();
		if (listener == null) {
			SWT.error(SWT.ERROR_NULL_ARGUMENT);
		}
		selectionListeners.add(listener);
	}

	/**
	 * Determines if the card is currently in a selected state or not.
	 *
	 * @return If it is selected.
	 */
	public boolean isSelected() {
		checkWidget();
		return selected;
	}

	/**
	 * Allows for programmatic selection to happen on the card. No event
	 * listeners added through {{@link #addSelectionListener(Listener)} will be
	 * fired.
	 *
	 * @param selected
	 *            Whether to select or unselect the card.
	 */
	public void setSelected(boolean selected) {
		checkWidget();
		this.selected = selected;
		redraw();
	}

	/**
	 * Sets the text of the message being displayed. If null is passed in will
	 * convert to empty string.
	 *
	 * @param text
	 *            The text to display, if null is specified is converted to an
	 *            empty string.
	 */
	public void setMessageText(String text) {
		checkWidget();
		messageText = (text == null ? "" : text);
	}

	/**
	 * Sets the icon to display on the left side of the card. Careful to use an
	 * icon that is correctly sized for the size allowed by the card's parent as
	 * the image will not be scaled to the size of the card. If a null image is
	 * set the image portion of the card will be reallocated.
	 *
	 * @param icon
	 *            The icon to display.
	 */
	public void setIcon(Image icon) {
		checkWidget();
		this.icon = icon;
	}

	/**
	 * Handles the painting of this composite.
	 *
	 * @param gc
	 *            The GC to paint with.
	 */
	private void paint(GC gc) {
		Rectangle bounds = getClientArea();
		int avaiableWidth = bounds.width - (MARGIN * 2);

		Color currentBackground;
		if (selected) {
			currentBackground = COLOR_BACKGROUND_SELECTED;
		} else {
			currentBackground = COLOR_BACKGROUND;
		}
		gc.setBackground(currentBackground);
		gc.setForeground(COLOR_FOREGROUND);
		gc.fillRectangle(bounds);

		int origAlpha = gc.getAlpha();
		gc.setBackground(COLOR_BACKGROUND_SELECTED);
		gc.setAlpha(hovering ? ALPHA_HOVERING : 0);
		gc.fillRectangle(bounds);

		gc.setBackground(currentBackground);
		gc.setAlpha(origAlpha);

		int currentX = MARGIN;
		if (icon != null) {
			Rectangle iconRec = icon.getBounds();

			int iconY = (((bounds.height - (MARGIN * 2)) - iconRec.height) / 2) + MARGIN;
			gc.drawImage(icon, MARGIN, iconY);
			avaiableWidth -= iconRec.width + MARGIN;
			currentX += iconRec.width + MARGIN;
		}

		tooltip.setMessage(messageText);
		String abbrText = messageText;
		messageTooLong = abbrText.length() > ELLIPSIS.length() + 1 && gc.textExtent(abbrText).x > avaiableWidth;
		while (abbrText.length() > ELLIPSIS.length() + 1 && gc.textExtent(abbrText).x > avaiableWidth) {
			abbrText = abbrText.substring(0, abbrText.length() - (ELLIPSIS.length() + 1)) + ELLIPSIS;
		}

		Point textPoint = gc.textExtent(abbrText);
		int messageY = (((bounds.height - (MARGIN * 2)) - textPoint.y) / 2) + MARGIN;

		gc.setBackground(currentBackground);
		gc.setForeground(selected ? COLOR_FOREGROUND_SELECTED : COLOR_FOREGROUND);
		gc.setFont(FONT_MESSAGE);

		gc.drawString(abbrText, currentX, messageY, true);

		gc.dispose();
	}

	/**
	 * Adds any listeners needed for this composite.
	 */
	private void addListeners() {
		addListener(SWT.Paint, e -> paint(e.gc));
		addListener(SWT.MouseEnter, e -> {
			if (messageTooLong) {
				tooltip.setVisible(true);
			}
			hovering = true;
			redraw();

		});
		addListener(SWT.MouseExit, e -> {
			tooltip.setVisible(false);
			mouseDown = false;
			hovering = false;
			redraw();
		});
		addListener(SWT.MouseDown, e -> {
			tooltip.setVisible(false);
			mouseDown = true;
		});
		addListener(SWT.MouseUp, e -> {
			if (mouseDown) {
				select();
			}
		});
	}

	/**
	 * Has the logic for handling when a selection is made on the composite by
	 * the user.
	 */
	private void select() {
		selected = !selected;
		redraw();
		for (Listener l : selectionListeners) {
			l.handleEvent(new Event());
		}
	}
}
