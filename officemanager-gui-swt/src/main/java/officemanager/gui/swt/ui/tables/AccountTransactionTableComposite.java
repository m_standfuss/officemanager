package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class AccountTransactionTableComposite extends Composite {

	public static final int COL_AMOUNT = 0;
	public static final int COL_TYPE = 1;
	public static final int COL_DATE = 2;
	public static final int COL_PAYMENT_TYPE = 3;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnAmount;
	public final TableColumn tblclmnType;
	public final TableColumn tblclmnDate;
	public final TableColumn tblclmnPaymentType;

	public AccountTransactionTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new FillLayout());
		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnAmount = new TableColumn(table, SWT.NONE);
		tblclmnAmount.setWidth(100);
		tblclmnAmount.setText("Amount");

		tblclmnType = new TableColumn(table, SWT.NONE);
		tblclmnType.setWidth(100);
		tblclmnType.setText("Type");

		tblclmnDate = new TableColumn(table, SWT.NONE);
		tblclmnDate.setWidth(100);
		tblclmnDate.setText("Date");

		tblclmnPaymentType = new TableColumn(table, SWT.NONE);
		tblclmnPaymentType.setWidth(100);
		tblclmnPaymentType.setText("Payment Type");
	}

}
