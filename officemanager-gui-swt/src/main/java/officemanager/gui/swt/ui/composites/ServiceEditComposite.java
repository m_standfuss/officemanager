package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.DecimalText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class ServiceEditComposite extends Composite {
	public final Label lblServiceName;
	public final UpperCaseText txtServiceName;
	public final Label lblServiceDescription;
	public final UpperCaseText txtDescription;
	public final Label lblLaborHours;
	public final DecimalText txtHours;
	public final Label lblLaborRate;
	public final DecimalText txtLaborRate;
	public final Label lbldenotesARequired;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ServiceEditComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		setLayout(gridLayout);

		lblServiceName = new Label(this, SWT.NONE);
		lblServiceName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblServiceName.setText("Name*:");
		lblServiceName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtServiceName = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtServiceName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtServiceName.widthHint = 150;
		txtServiceName.setLayoutData(gd_txtServiceName);
		txtServiceName.setTextLimit(50);

		lblLaborHours = new Label(this, SWT.NONE);
		lblLaborHours.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLaborHours.setText("Labor Hours*:");
		lblLaborHours.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtHours = new DecimalText(2, this, SWT.BORDER);
		GridData gd_txtHours = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtHours.widthHint = 50;
		txtHours.setLayoutData(gd_txtHours);

		lblServiceDescription = new Label(this, SWT.NONE);
		lblServiceDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblServiceDescription.setText("Description:");
		lblServiceDescription.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtDescription = new UpperCaseText(this, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescription = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtDescription.heightHint = 100;
		gd_txtDescription.widthHint = 140;
		txtDescription.setLayoutData(gd_txtDescription);

		lblLaborRate = new Label(this, SWT.NONE);
		lblLaborRate.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblLaborRate.setText("Labor Rate*:");
		lblLaborRate.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtLaborRate = new DecimalText(2, this, SWT.BORDER);
		GridData gd_txtLaborRate = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_txtLaborRate.widthHint = 50;
		txtLaborRate.setLayoutData(gd_txtLaborRate);

		lbldenotesARequired = new Label(this, SWT.NONE);
		lbldenotesARequired.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		lbldenotesARequired.setText("*Denotes a required field");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
