package officemanager.gui.swt.ui.wizards;

import java.math.BigDecimal;
import java.util.List;

import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.delegates.TradeInDelegate;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.TradeInRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.OrderPaymentService;
import officemanager.gui.swt.ui.wizards.pages.PaymentsPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;

public class OrderPaymentWizard extends OfficeManagerWizard {

	private final long orderId;

	private final PaymentDelegate paymentDelegate;
	private final TradeInDelegate tradeInDelegate;

	private final OrderPaymentService orderPaymentService;

	private PaymentsPage paymentsPage;
	private boolean canFinish = false;

	public OrderPaymentWizard(long orderId) {
		this.orderId = orderId;
		paymentDelegate = new PaymentDelegate();
		tradeInDelegate = new TradeInDelegate();
		orderPaymentService = new OrderPaymentService();
		orderPaymentService.setOrderId(orderId);
		setWindowTitle("Payments Order #" + AppPrefs.FORMATTER.formatOrderNumber(orderId));
	}

	@Override
	public void addPages() {
		paymentsPage = new PaymentsPage();
		orderPaymentService.setPage(paymentsPage);
		orderPaymentService.addPaymentAddedListener(e -> {
			canFinish = true;
			getContainer().updateButtons();
		});
		addPage(paymentsPage);
	}

	@Override
	public boolean performCancel() {
		if (!orderPaymentService.getPaymentsAdded().isEmpty()) {
			if (!MessageDialogs.askQuestion(getShell(), "Close Payments",
					"Are you sure you wish to cancel? Any new payments added will be lost.")) {
				return false;
			}
		}
		return super.performCancel();
	}

	@Override
	public boolean performFinish() {
		BigDecimal change = orderPaymentService.makeChange();

		List<PaymentRecord> payments = orderPaymentService.getPaymentsAdded();
		for (PaymentRecord record : payments) {
			paymentDelegate.makePaymentOnOrder(orderId, record);
		}

		List<TradeInRecord> tradeIns = orderPaymentService.getTradeInsAdded();
		for (TradeInRecord record : tradeIns) {
			tradeInDelegate.insertTradeIn(record);
		}

		if (change.compareTo(BigDecimal.ZERO) > 0) {
			MessageDialogs.displayMessage(getShell(), "Change Owed",
					"The client is owed " + AppPrefs.FORMATTER.formatMoney(change) + " in change.");
		}
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	@Override
	public boolean canFinish() {
		return canFinish;
	}
}
