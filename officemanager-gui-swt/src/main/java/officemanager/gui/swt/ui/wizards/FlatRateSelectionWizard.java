package officemanager.gui.swt.ui.wizards;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import officemanager.biz.records.FlatRateSearchRecord;
import officemanager.gui.swt.ui.wizards.pages.FlatRateSelectionPage;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.utility.CollectionUtility;

public class FlatRateSelectionWizard extends OfficeManagerWizard {

	private static final Logger logger = LogManager.getLogger(FlatRateSelectionWizard.class);

	private final int maxSelection;

	private List<FlatRateSearchRecord> results;
	private FlatRateSelectionPage page;

	public FlatRateSelectionWizard(int maxSelection) {
		setWindowTitle("Flat Rate Selection");
		this.maxSelection = maxSelection;
	}

	@Override
	public void addPages() {
		page = new FlatRateSelectionPage(maxSelection, true, true);
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		results = page.getSelectedRecords();
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	public List<FlatRateSearchRecord> getResults() {
		return results;
	}

	public FlatRateSearchRecord getResult() {
		if (CollectionUtility.isNullOrEmpty(results)) {
			logger.warn("Getting results but no results have been set. Returning null.");
			return null;
		}
		if (results.size() > 1) {
			logger.warn("Getting one results but multiple were selected returning the first.");
		}
		return results.get(0);
	}

}
