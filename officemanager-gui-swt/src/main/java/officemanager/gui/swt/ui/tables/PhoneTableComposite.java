package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;

import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PhoneTableComposite extends Composite {

	public static final int COL_PRIMARY_IND = 0;
	public static final int COL_PHONE_NUM = 1;
	public static final int COL_EXT = 2;
	public static final int COL_PHONE_TYPE = 3;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnPrimary;
	public final TableColumn tblclmnPhoneNumber;
	public final TableColumn tblclmnExt;
	public final TableColumn tblclmnPhoneType;
	public final Label lblPrimaryPhoneImage;
	public final Label lblPrimaryPhoneText;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PhoneTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		table = new OfficeManagerTable(this, tableStyle);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnPrimary = new TableColumn(table, SWT.NONE);
		tblclmnPrimary.setWidth(100);
		tblclmnPrimary.setText("");

		tblclmnPhoneNumber = new TableColumn(table, SWT.NONE);
		tblclmnPhoneNumber.setWidth(100);
		tblclmnPhoneNumber.setText("Phone Number");

		tblclmnExt = new TableColumn(table, SWT.NONE);
		tblclmnExt.setWidth(100);
		tblclmnExt.setText("Ext.");

		tblclmnPhoneType = new TableColumn(table, SWT.NONE);
		tblclmnPhoneType.setWidth(100);
		tblclmnPhoneType.setText("Phone Type");

		lblPrimaryPhoneImage = new Label(this, SWT.NONE);
		lblPrimaryPhoneImage.setImage(ResourceManager.getImage(ImageFile.CHECK, 16));

		lblPrimaryPhoneText = new Label(this, SWT.NONE);
		lblPrimaryPhoneText.setText("Denotes the primary phone");
	}

}
