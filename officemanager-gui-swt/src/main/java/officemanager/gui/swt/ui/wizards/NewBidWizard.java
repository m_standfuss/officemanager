package officemanager.gui.swt.ui.wizards;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWTError;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.delegates.ClientProductDelegate;
import officemanager.biz.records.ClientProductRecord;
import officemanager.biz.records.ClientProductSelectionRecord;
import officemanager.biz.records.ClientRecord;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderRecord.OrderStatus;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.printing.BidPrint;
import officemanager.gui.swt.printing.PaperclipsPrinter;
import officemanager.gui.swt.ui.wizards.pages.ClientProductPage;
import officemanager.gui.swt.ui.wizards.pages.ClientSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.FlatRateSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.OrderDetailsPage;
import officemanager.gui.swt.ui.wizards.pages.PartSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.ServicesPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.gui.swt.views.OrderView;
import officemanager.utility.Tuple;

public class NewBidWizard extends OfficeManagerWizard {

	private static final Logger logger = LogManager.getLogger(NewBidWizard.class);

	private final ClientDelegate clientDelegate;
	private final ClientProductDelegate clientProductDelegate;

	private ClientSelectionPage clientSelectionPage;
	private ClientProductPage clientProductPage;
	private FlatRateSelectionPage flatRateSelectionPage;
	private PartSelectionPage partSelectionPage;
	private ServicesPage servicePage;
	private OrderDetailsPage orderDetailsPage;

	private Long lastLoadedClientIdProducts;
	private Long lastLoadedClientIdServiceRate;
	private Long orderId;

	public NewBidWizard() {
		setWindowTitle("New Bid");
		clientDelegate = new ClientDelegate();
		clientProductDelegate = new ClientProductDelegate();
	}

	@Override
	public void addPages() {
		clientSelectionPage = new ClientSelectionPage(1, true);
		addPage(clientSelectionPage);

		clientProductPage = new ClientProductPage(true);
		clientProductPage.addFirstShowListener(e -> populateClientProductPage());
		clientProductPage.addIsShownListener(e -> populateClientProducts());
		addPage(clientProductPage);

		flatRateSelectionPage = new FlatRateSelectionPage(Integer.MAX_VALUE, false, false);
		addPage(flatRateSelectionPage);

		servicePage = new ServicesPage();
		servicePage.addIsShownListener(e -> populateLaborRate());
		addPage(servicePage);

		partSelectionPage = new PartSelectionPage(Integer.MAX_VALUE, false, false, true);
		addPage(partSelectionPage);

		orderDetailsPage = new OrderDetailsPage();
		orderDetailsPage.addIsShownListener(e -> updateProductSelectionPage());
		addPage(orderDetailsPage);
	}

	@Override
	public boolean canFinish() {
		return getContainer().getCurrentPage() == orderDetailsPage && orderDetailsPage.isPageComplete();
	}

	@Override
	public boolean performFinish() {
		if (!MessageDialogs.askQuestion(getShell(), "Confirmation", "Do you wish to finalize this bid?")) {
			return false;
		}

		Long clientId = clientSelectionPage.getSelectedClientId();

		List<Tuple<ClientProductRecord, ClientProductSelectionRecord>> clientRecordsToAdd = clientProductPage
				.getProductsAndSelectionsToAdd();
		for (Tuple<ClientProductRecord, ClientProductSelectionRecord> newProduct : clientRecordsToAdd) {
			newProduct.item1.setClientId(clientId);
			Long clientProductId = clientProductDelegate.persistClientProduct(newProduct.item1);
			newProduct.item2.setClientProductId(clientProductId);
		}

		OrderRecord orderRecord = new OrderRecord();
		orderRecord.setClientId(clientId);
		orderRecord.setOrderStatus(OrderStatus.OPEN);
		orderRecord.setCreatedBy(AppPrefs.currentSession.nameFullFormatted);
		orderRecord.setCreatedDttm(new Date());

		List<OrderClientProductRecord> clientProducts = Lists.newArrayList();
		for (ClientProductSelectionRecord clientProduct : orderDetailsPage.getClientProducts()) {
			OrderClientProductRecord clientProductRecord = new OrderClientProductRecord(clientProduct);
			clientProducts.add(clientProductRecord);
		}
		orderRecord.setClientProducts(clientProducts);

		List<OrderPartRecord> partRecords = orderDetailsPage.getParts();
		boolean isTaxExempt = clientDelegate.isTaxExempt(clientId);
		for (OrderPartRecord partRecord : orderDetailsPage.getParts()) {
			partRecord.setTaxExempt(isTaxExempt);
		}
		orderRecord.setParts(partRecords);

		orderRecord.setFlatRates(orderDetailsPage.getFlatRates());
		orderRecord.setServices(orderDetailsPage.getServices());

		ClientRecord clientRecord = null;
		if (clientId != null) {
			clientRecord = clientDelegate.getClientRecord(clientId);
		}

		BidPrint print = new BidPrint(orderRecord, clientRecord);
		try {
			PaperclipsPrinter.promptAndPrint(getShell(), print);
		} catch (SWTError | Exception e) {
			logger.error("Unable to print the bid.", e);
			MessageDialogs.displayError(getShell(), "Unable To Print", "Unexpected error occured while printing.");
			return false;
		}
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		if (orderId == null) {
			return null;
		}
		return new OrderView(orderId);
	}

	private void populateClientProductPage() {
		clientProductPage.populateProductStatus(clientProductDelegate.getProductStatuses());
		clientProductPage.populateProductTypes(clientProductDelegate.getProductTypes());
	}

	private void populateClientProducts() {
		Long currentClientId = clientSelectionPage.getSelectedClientId();
		if (lastLoadedClientIdProducts == currentClientId) {
			logger.debug("Same client id as last loaded products.");
			return;
		}
		List<ClientProductSelectionRecord> productRecords = clientProductDelegate
				.getActiveClientProductSelections(currentClientId);
		lastLoadedClientIdProducts = currentClientId;
		clientProductPage.populateClientProducts(productRecords);
	}

	private void populateLaborRate() {
		Long currentClientId = clientSelectionPage.getSelectedClientId();
		if (lastLoadedClientIdServiceRate == currentClientId) {
			logger.debug("Same client id as last loaded service rate.");
			return;
		}
		boolean hasDiscountRate = clientDelegate.isInPromotionalClub(currentClientId);
		servicePage.populateLaborRate(hasDiscountRate ? AppPrefs.hourlyRateDiscounted : AppPrefs.hourlyRate);
	}

	private void updateProductSelectionPage() {
		orderDetailsPage.populateClientProductSelections(clientProductPage.getSelectedProducts());
		orderDetailsPage.populateFlatRateSearches(flatRateSelectionPage.getSelectedRecords());
		orderDetailsPage.populatePartSearches(partSelectionPage.getSelectedRecords());
		orderDetailsPage.populateServices(servicePage.getServices());
	}
}
