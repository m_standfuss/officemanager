package officemanager.gui.swt.ui.dialogs;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import officemanager.gui.swt.AppPrefs;

public class DateSelectionDialog extends Dialog {
	public Label lblPleaseSelectA;
	public DateTime dateTime;

	private Date resultDate;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public DateSelectionDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(SWT.PRIMARY_MODAL);
	}

	/**
	 * Gets the currently selected date will be set to the beginning of the day
	 * (00:00).
	 * 
	 * @return The date.
	 */
	public Date getDate() {
		return resultDate;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout());

		lblPleaseSelectA = new Label(container, SWT.NONE);
		lblPleaseSelectA.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));
		lblPleaseSelectA.setText("Please select a date.");

		dateTime = new DateTime(container, SWT.CALENDAR);
		dateTime.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		return container;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(250, 250);
	}

	@Override
	protected void okPressed() {
		resultDate = getCurrentDate();
		super.okPressed();
	}

	private Date getCurrentDate() {
		Calendar instance = Calendar.getInstance(AppPrefs.localTimeZone);
		instance.set(Calendar.DAY_OF_MONTH, dateTime.getDay());
		instance.set(Calendar.MONTH, dateTime.getMonth());
		instance.set(Calendar.YEAR, dateTime.getYear());

		instance.set(Calendar.HOUR_OF_DAY, 0);
		instance.set(Calendar.MINUTE, 0);
		instance.set(Calendar.SECOND, 0);
		return instance.getTime();
	}
}
