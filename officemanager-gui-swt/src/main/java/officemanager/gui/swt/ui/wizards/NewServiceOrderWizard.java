package officemanager.gui.swt.ui.wizards;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.delegates.ClientProductDelegate;
import officemanager.biz.delegates.OrderCommentDelegate;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.records.ClientProductRecord;
import officemanager.biz.records.ClientProductSelectionRecord;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderRecord.OrderStatus;
import officemanager.biz.records.OrderRecord.OrderType;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.dialogs.OrderPrintDialog;
import officemanager.gui.swt.ui.wizards.pages.ClientProductPage;
import officemanager.gui.swt.ui.wizards.pages.ClientSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.CommentsPage;
import officemanager.gui.swt.ui.wizards.pages.FlatRateSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.OrderDetailsPage;
import officemanager.gui.swt.ui.wizards.pages.PartSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.ServicesPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.gui.swt.views.OrderView;
import officemanager.utility.Tuple;

public class NewServiceOrderWizard extends OfficeManagerWizard {

	private static final Logger logger = LogManager.getLogger(NewServiceOrderWizard.class);

	private final ClientDelegate clientDelegate;
	private final ClientProductDelegate clientProductDelegate;
	private final OrderCommentDelegate orderCommentDelegate;
	private final OrderDelegate orderDelegate;

	private ClientSelectionPage clientSelectionPage;
	private ClientProductPage clientProductPage;
	private FlatRateSelectionPage flatRateSelectionPage;
	private PartSelectionPage partSelectionPage;
	private ServicesPage servicePage;
	private OrderDetailsPage orderDetailsPage;
	private CommentsPage orderCommentPage;

	private Long lastLoadedClientIdProducts;
	private Long lastLoadedClientIdServiceRate;
	private Long orderId;

	public NewServiceOrderWizard() {
		setWindowTitle("New Service Order");
		clientDelegate = new ClientDelegate();
		clientProductDelegate = new ClientProductDelegate();
		orderCommentDelegate = new OrderCommentDelegate();
		orderDelegate = new OrderDelegate();
	}

	@Override
	public void addPages() {
		clientSelectionPage = new ClientSelectionPage(1, true);
		addPage(clientSelectionPage);

		clientProductPage = new ClientProductPage(true);
		clientProductPage.addFirstShowListener(e -> populateClientProductPage());
		clientProductPage.addIsShownListener(e -> populateClientProducts());
		addPage(clientProductPage);

		flatRateSelectionPage = new FlatRateSelectionPage(Integer.MAX_VALUE, false, false);
		addPage(flatRateSelectionPage);

		servicePage = new ServicesPage();
		servicePage.addIsShownListener(e -> populateLaborRate());
		addPage(servicePage);

		partSelectionPage = new PartSelectionPage(Integer.MAX_VALUE, false, false, true);
		addPage(partSelectionPage);

		orderDetailsPage = new OrderDetailsPage();
		orderDetailsPage.addIsShownListener(e -> updateProductSelectionPage());
		addPage(orderDetailsPage);

		orderCommentPage = new CommentsPage();
		orderCommentPage.addFirstShowListener(e -> populateCommentTypes());
		addPage(orderCommentPage);
	}

	@Override
	public boolean canFinish() {
		return getContainer().getCurrentPage() == orderCommentPage;
	}

	@Override
	public boolean performFinish() {
		if (!MessageDialogs.askQuestion(getShell(), "Confirmation", "Are you sure you wish to create this order?")) {
			return false;
		}

		Long clientId = clientSelectionPage.getSelectedClientId();

		List<Tuple<ClientProductRecord, ClientProductSelectionRecord>> clientRecordsToAdd = clientProductPage
				.getProductsAndSelectionsToAdd();
		for (Tuple<ClientProductRecord, ClientProductSelectionRecord> newProduct : clientRecordsToAdd) {
			newProduct.item1.setClientId(clientId);
			Long clientProductId = clientProductDelegate.persistClientProduct(newProduct.item1);
			newProduct.item2.setClientProductId(clientProductId);
		}

		OrderRecord orderRecord = new OrderRecord();
		orderRecord.setClientId(clientId);
		orderRecord.setOrderStatus(OrderStatus.OPEN);
		orderRecord.setOrderType(OrderType.WORK);

		List<OrderClientProductRecord> clientProducts = Lists.newArrayList();
		for (ClientProductSelectionRecord clientProduct : orderDetailsPage.getClientProducts()) {
			OrderClientProductRecord clientProductRecord = new OrderClientProductRecord();
			clientProductRecord.setClientProductId(clientProduct.getClientProductId());
			clientProducts.add(clientProductRecord);
		}
		orderRecord.setClientProducts(clientProducts);

		List<OrderPartRecord> partRecords = orderDetailsPage.getParts();
		boolean isTaxExempt = clientDelegate.isTaxExempt(clientId);
		for (OrderPartRecord partRecord : orderDetailsPage.getParts()) {
			partRecord.setTaxExempt(isTaxExempt);
		}
		orderRecord.setParts(partRecords);

		orderRecord.setFlatRates(orderDetailsPage.getFlatRates());
		orderRecord.setServices(orderDetailsPage.getServices());
		orderId = orderDelegate.insertUpdateOrder(orderRecord);

		List<PriceOverrideRecord> priceOverrides = orderDetailsPage.getPriceOverrides();
		for (PriceOverrideRecord record : priceOverrides) {
			record.setOrderId(orderId);
			orderDelegate.insertPriceOverride(record);
		}

		List<OrderCommentRecord> commentRecords = orderCommentPage.getComments();
		for (OrderCommentRecord commentRecord : commentRecords) {
			commentRecord.setOrderId(orderId);
			orderCommentDelegate.persistOrderComment(commentRecord);
		}

		OrderPrintDialog printDialog = new OrderPrintDialog(getShell(), orderId);
		printDialog.open();
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		if (orderId == null) {
			return null;
		}
		return new OrderView(orderId);
	}

	private void populateClientProductPage() {
		clientProductPage.populateProductStatus(clientProductDelegate.getProductStatuses());
		clientProductPage.populateProductTypes(clientProductDelegate.getProductTypes());
	}

	private void populateClientProducts() {
		Long currentClientId = clientSelectionPage.getSelectedClientId();
		if (lastLoadedClientIdProducts == currentClientId) {
			logger.debug("Same client id as last loaded products.");
			return;
		}
		List<ClientProductSelectionRecord> productRecords = clientProductDelegate
				.getActiveClientProductSelections(currentClientId);
		lastLoadedClientIdProducts = currentClientId;
		clientProductPage.populateClientProducts(productRecords);
	}

	private void populateLaborRate() {
		Long currentClientId = clientSelectionPage.getSelectedClientId();
		if (lastLoadedClientIdServiceRate == currentClientId) {
			logger.debug("Same client id as last loaded service rate.");
			return;
		}
		boolean hasDiscountRate = clientDelegate.isInPromotionalClub(currentClientId);
		servicePage.populateLaborRate(hasDiscountRate ? AppPrefs.hourlyRateDiscounted : AppPrefs.hourlyRate);
	}

	private void updateProductSelectionPage() {
		orderDetailsPage.populateClientProductSelections(clientProductPage.getSelectedProducts());
		orderDetailsPage.populateFlatRateSearches(flatRateSelectionPage.getSelectedRecords());
		orderDetailsPage.populatePartSearches(partSelectionPage.getSelectedRecords());
		orderDetailsPage.populateServices(servicePage.getServices());
	}

	private void populateCommentTypes() {
		orderCommentPage.populateCommentTypes(orderCommentDelegate.getOrderCommentTypes());
	}
}
