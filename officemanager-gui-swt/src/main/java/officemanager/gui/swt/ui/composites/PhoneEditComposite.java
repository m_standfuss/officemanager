package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.NumberText;
import officemanager.gui.swt.ui.widgets.PhoneNumberText;
import officemanager.gui.swt.util.ResourceManager;

public class PhoneEditComposite extends Composite {
	public final Label lblPhone;
	public final PhoneNumberText txtPhone;
	public final Label lblExtension;
	public final NumberText txtExtension;
	public final Label lblPhoneType;
	public final CodeValueCombo cmbPhoneType;
	public final Button btnPrimaryPhone;
	public final Label lblDenotesA;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PhoneEditComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(4, false));

		lblPhone = new Label(this, SWT.NONE);
		lblPhone.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPhone.setText("Phone*:");
		lblPhone.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtPhone = new PhoneNumberText(this, SWT.BORDER);
		GridData gd_txtPhone = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtPhone.widthHint = 150;
		txtPhone.setLayoutData(gd_txtPhone);

		lblPhoneType = new Label(this, SWT.NONE);
		lblPhoneType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPhoneType.setText("Phone Type*:");
		lblPhoneType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbPhoneType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbPhoneType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbPhoneType.widthHint = 200;
		cmbPhoneType.setLayoutData(gd_cmbPhoneType);

		lblExtension = new Label(this, SWT.NONE);
		lblExtension.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblExtension.setText("Extension:");
		lblExtension.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtExtension = new NumberText(this, SWT.BORDER);
		GridData gd_txtExtension = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtExtension.widthHint = 75;
		txtExtension.setLayoutData(gd_txtExtension);
		txtExtension.setTextLimit(15);

		btnPrimaryPhone = new Button(this, SWT.CHECK);
		btnPrimaryPhone.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnPrimaryPhone.setText("Primary Phone");

		lblDenotesA = new Label(this, SWT.NONE);
		lblDenotesA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		lblDenotesA.setText("* Denotes a required field");

		setTabList(new Control[] { txtPhone, txtExtension, cmbPhoneType, btnPrimaryPhone });
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
