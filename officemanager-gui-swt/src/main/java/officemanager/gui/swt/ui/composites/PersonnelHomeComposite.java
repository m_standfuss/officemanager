package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelHomeComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tltmNewOrderItem;
	public final Menu menuNewOrders;
	public final MenuItem mntmServiceOrder;
	public final MenuItem mntmMerchandiseOrder;
	public final MenuItem mntmBid;
	public final ToolItem tlItmEditInformation;

	public final CollapsibleComposite collapsibleCompositeOrders;
	public final OrderTableComposite ordersTable;

	public final CollapsibleComposite collapsibleCompositeMyInfo;
	public final PersonnelInformationComposite personnelInfoComposite;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PersonnelHomeComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		menuNewOrders = new Menu(parent.getShell(), SWT.POP_UP);

		mntmServiceOrder = new MenuItem(menuNewOrders, SWT.NONE);
		mntmServiceOrder.setText("Service Order");
		mntmServiceOrder.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_SERVICE_ORDER));

		mntmMerchandiseOrder = new MenuItem(menuNewOrders, SWT.NONE);
		mntmMerchandiseOrder.setText("Merchandise Order");
		mntmMerchandiseOrder.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_MERCH_ORDER));

		mntmBid = new MenuItem(menuNewOrders, SWT.NONE);
		mntmBid.setText("Bid");
		mntmBid.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_BID));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		tltmNewOrderItem = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmNewOrderItem.setImage(ResourceManager.getImage(ImageFile.NEW, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tltmNewOrderItem.setToolTipText("New Order");
		tltmNewOrderItem.addListener(SWT.Selection, e -> {
			Rectangle rect = tltmNewOrderItem.getBounds();
			Point pt = new Point(rect.x, rect.y + rect.height);
			pt = toolBar.toDisplay(pt);
			menuNewOrders.setLocation(pt.x, pt.y);
			menuNewOrders.setVisible(true);
		});

		tlItmEditInformation = new ToolItem(toolBar, SWT.NONE);
		tlItmEditInformation.setImage(ResourceManager.getImage(ImageFile.USER_EDIT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmEditInformation.setToolTipText("Edit Information");
		tlItmEditInformation.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_PERSONNEL));

		ScrolledComposite scrolledComposite = new ScrolledComposite(this, SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Composite composite = new Composite(scrolledComposite, style);
		composite.setLayout(new GridLayout());

		collapsibleCompositeOrders = new CollapsibleComposite(composite, SWT.NONE);
		collapsibleCompositeOrders.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		collapsibleCompositeOrders.setTitleText("My Open Orders");
		collapsibleCompositeOrders.setTitleImage(ResourceManager.getImage(ImageFile.ORDER, AppPrefs.ICN_SIZE_CLPCMP));
		collapsibleCompositeOrders.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		ordersTable = new OrderTableComposite(collapsibleCompositeOrders.composite, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION);
		collapsibleCompositeOrders.composite.setLayout(new GridLayout(1, false));
		GridData gd_ordersTable = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_ordersTable.minimumHeight = 150;
		ordersTable.setLayoutData(gd_ordersTable);

		collapsibleCompositeMyInfo = new CollapsibleComposite(composite, SWT.NONE);
		collapsibleCompositeMyInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		collapsibleCompositeMyInfo.setTitleText("My Information");
		collapsibleCompositeMyInfo
				.setTitleImage(ResourceManager.getImage(ImageFile.USER_INFO, AppPrefs.ICN_SIZE_CLPCMP));
		collapsibleCompositeMyInfo.composite.setLayout(new GridLayout(1, false));
		collapsibleCompositeMyInfo.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		personnelInfoComposite = new PersonnelInformationComposite(collapsibleCompositeMyInfo.composite, SWT.NONE);
		personnelInfoComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		collapsibleCompositeOrders.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		collapsibleCompositeMyInfo.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		composite.addListener(SWT.Resize,
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
