package officemanager.gui.swt.ui.dialogs;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.google.common.collect.Maps;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.LongTextRecord;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderRecord.OrderStatus;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PaymentRecord.PaymentType;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.EditOrderServiceComposite;
import officemanager.gui.swt.util.MessageDialogs;

public class EditOrderServiceDialog extends Dialog {

	private static final int HEIGHT_EDITABLE = 400;
	private static final int HEIGHT_READABLE = 250;
	private static final int WIDTH = 330;

	private final Long serviceId;
	private final Long orderId;
	private final OrderDelegate orderDelegate;
	private final PaymentDelegate paymentDelegate;

	private boolean saved;
	private OrderServiceRecord orderServiceRecord;

	private Shell shell;
	private EditOrderServiceComposite composite;
	private Button btnRefund;
	private Button btnRemove;
	private Button btnSave;
	private Button btnCancel;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public EditOrderServiceDialog(Shell parent, Long orderId, Long serviceId) {
		super(parent, SWT.NONE);
		this.orderId = orderId;
		this.serviceId = serviceId;
		orderDelegate = new OrderDelegate();
		paymentDelegate = new PaymentDelegate();
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public boolean open() {
		createContents();
		addListeners();
		populateContents();
		enableContent();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return saved;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.RESIZE | SWT.TITLE | SWT.PRIMARY_MODAL);
		shell.setLayout(new GridLayout());

		Rectangle parentSize = getParent().getBounds();

		int locationX, locationY;
		locationX = (parentSize.width - WIDTH) / 2 + parentSize.x;
		locationY = (parentSize.height - HEIGHT_READABLE) / 2 + parentSize.y;

		shell.setLocation(new Point(locationX, locationY));

		composite = new EditOrderServiceComposite(shell, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		Composite composite = new Composite(shell, SWT.RIGHT_TO_LEFT);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite.setLayout(new GridLayout(4, false));

		btnCancel = new Button(composite, SWT.NONE);
		btnCancel.setLayoutData(new GridData());
		btnCancel.setText("Cancel");

		btnRemove = new Button(composite, SWT.NONE);
		btnRemove.setLayoutData(new GridData());
		btnRemove.setText("Remove");

		btnRefund = new Button(composite, SWT.NONE);
		btnRefund.setLayoutData(new GridData());
		btnRefund.setText("Refund");

		btnSave = new Button(composite, SWT.NONE);
		btnSave.setLayoutData(new GridData());
		btnSave.setText("Save");
	}

	private void addListeners() {
		btnCancel.addListener(SWT.Selection, e -> cancel());
		btnRefund.addListener(SWT.Selection, e -> refund());
		btnRemove.addListener(SWT.Selection, e -> remove());
		btnSave.addListener(SWT.Selection, e -> save());
	}

	private void populateContents() {
		shell.setText("Order #: " + AppPrefs.FORMATTER.formatOrderNumber(orderId));
		List<OrderClientProductRecord> clientProducts = orderDelegate.loadClientProducts(orderId);
		Map<Long, OrderClientProductRecord> clientProductMap = Maps.uniqueIndex(clientProducts,
				OrderClientProductRecord::getClientProductId);
		composite.hdrValClientProduct.populateChoices(clientProducts);

		orderServiceRecord = orderDelegate.loadServiceRecord(serviceId);

		composite.hdrValClientProduct.setValue(clientProductMap.get(orderServiceRecord.getClientProductId()));
		composite.hdrValLaborHours.setValue(orderServiceRecord.getHoursQnt());
		composite.hdrValLaborRate.setValue(orderServiceRecord.getPricePerHour());

		if (orderServiceRecord.getServiceText() != null) {
			composite.hdrValServiceDescription.setValue(orderServiceRecord.getServiceText().getAsPlainText());
		} else {
			composite.hdrValServiceDescription.setValue("");
		}

		composite.hdrValServiceName.setValue(orderServiceRecord.getServiceName());

		((GridData) composite.lblRefunded.getLayoutData()).exclude = !orderServiceRecord.isRefunded();
		composite.lblRefunded.setVisible(orderServiceRecord.isRefunded());
	}

	private void enableContent() {
		OrderStatus orderStatus = orderDelegate.getOrderStatus(orderId);

		composite.hdrValClientProduct.makeEditable(orderStatus == OrderStatus.OPEN);
		composite.hdrValLaborHours.makeEditable(orderStatus == OrderStatus.OPEN);
		composite.hdrValLaborRate.makeEditable(orderStatus == OrderStatus.OPEN);
		composite.hdrValServiceDescription.makeEditable(orderStatus == OrderStatus.OPEN);
		composite.hdrValServiceName.makeEditable(orderStatus == OrderStatus.OPEN);

		if (orderStatus == OrderStatus.OPEN) {
			((GridData) btnRefund.getLayoutData()).exclude = true;
			btnRefund.setVisible(false);
			shell.setSize(WIDTH, HEIGHT_EDITABLE);
		} else {
			btnCancel.setText("Close");

			((GridData) btnSave.getLayoutData()).exclude = true;
			btnSave.setVisible(false);

			((GridData) btnRemove.getLayoutData()).exclude = true;
			btnRemove.setVisible(false);

			if (orderStatus != OrderStatus.CLOSED) {
				((GridData) btnRefund.getLayoutData()).exclude = true;
				btnRefund.setVisible(false);
			} else {
				btnRefund.setEnabled(
						AppPrefs.hasPermission(AppPermissions.REFUND_ORDER) && !orderServiceRecord.isRefunded());
			}
			shell.setSize(WIDTH, HEIGHT_READABLE);
		}
	}

	private void cancel() {
		shell.close();
	}

	private void save() {
		if (!MessageDialogs.askQuestion(shell, "Confirm Save", "Are you sure you wish to save this service?")) {
			return;
		}
		OrderClientProductRecord selectedProduct = composite.hdrValClientProduct.getEditableValue();
		Long clientProductId = null;
		if (selectedProduct != null) {
			clientProductId = selectedProduct.getClientProductId();
		}
		orderServiceRecord.setClientProductId(clientProductId);
		orderServiceRecord.setHoursQnt(composite.hdrValLaborHours.getEditableValue());
		orderServiceRecord.setPricePerHour(composite.hdrValLaborRate.getEditableValue());
		orderServiceRecord.setServiceName(composite.hdrValServiceName.getEditableValue());

		if (composite.hdrValServiceDescription.getEditableValue().isEmpty()) {
			orderServiceRecord.setServiceText(null);
		} else {
			if (orderServiceRecord.getServiceText() == null) {
				orderServiceRecord
						.setServiceText(new LongTextRecord(composite.hdrValServiceDescription.getEditableValue()));
			} else {
				orderServiceRecord.getServiceText().setLongText(composite.hdrValServiceDescription.getEditableValue());
			}
		}
		orderDelegate.insertOrUpdateService(orderId, orderServiceRecord);

		saved = true;
		shell.close();
	}

	private void remove() {
		if (!MessageDialogs.askQuestion(shell, "Confirm Removal", "Are you sure you wish to remove this service?")) {
			return;
		}
		orderDelegate.inactivateService(serviceId);
		saved = true;
		shell.close();
	}

	private void refund() {
		if (!MessageDialogs.askQuestion(shell, "Confirm Refund", "Are you sure you wish to refund this service?")) {
			return;
		}
		orderDelegate.refundService(serviceId);

		BigDecimal refundTotal = Calculator.totalService(orderServiceRecord);
		PaymentRecord paymentRecord = new PaymentRecord();
		paymentRecord.setAmount(Calculator.negate(refundTotal));
		paymentRecord.setPaymentDescription("ORDER REFUND");
		paymentRecord.setPaymentNumber(AppPrefs.FORMATTER.formatOrderNumber(orderId));
		paymentRecord.setPaymentType(PaymentType.REFUND);
		paymentDelegate.makePaymentOnOrder(orderId, paymentRecord);

		saved = true;
		shell.close();
	}
}