package officemanager.gui.swt.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.AppPrefs;

public class PhoneNumberText extends Text {
	private static final int MAX_LENGTH = 11;

	public PhoneNumberText(Composite parent, int style) {
		super(parent, style);

		addListener(SWT.KeyDown, l -> {
			if (isDeleteOrBackSpace(l.keyCode) || isLeftOrRightArrow(l.keyCode)) {
				return;
			}
			if (isAcceptableKeyCode(l.keyCode) && getUnformattedText().length() < MAX_LENGTH) {
				formatText(getText(), getCaretPosition(), l.keyCode);
			}
			l.doit = false;
		});
	}

	@Override
	public void setText(String string) {
		string = AppPrefs.FORMATTER.formatPhoneNumber(string);
		super.setText(string);
	}

	public boolean isValidPhoneNumber() {
		final String unformattedText = getUnformattedText();
		return unformattedText.matches("[0-9]{7}[0-9]*");
	}

	public String getUnformattedText() {
		return unformatText(getText());
	}

	private boolean isDeleteOrBackSpace(int keyCode) {
		return keyCode == 8 || keyCode == 127;
	}

	private boolean isLeftOrRightArrow(int keyCode) {
		return keyCode == 16777219 || keyCode == 16777220;
	}

	private boolean isAcceptableKeyCode(int keyCode) {
		if (keyCode >= 48 && keyCode <= 57) {
			return true;
		}
		if (keyCode >= 16777264 && keyCode <= 16777273) {
			return true;
		}
		return false;
	}

	private String unformatText(String string) {
		return string.replace("-", "").replace("(", "").replace(")", "");
	}

	private void formatText(String text, int caretPosition, int keyCode) {
		final StringBuilder b = new StringBuilder();
		final int origCaretPosition = caretPosition;
		for (int i = 0; i < text.length(); i++) {
			final char c = text.charAt(i);
			if (isAcceptableKeyCode(c)) {
				b.append(c);
			} else {
				if (i <= origCaretPosition) {
					caretPosition--;
				}
			}
		}
		String newText = b.toString();
		newText = newText.substring(0, caretPosition) + (char) keyCode
				+ newText.substring(caretPosition, newText.length());
		caretPosition++;

		final String formattedText = AppPrefs.FORMATTER.formatPhoneNumber(newText);
		super.setText(formattedText);
		setSelection(caretPosition + AppPrefs.FORMATTER.getAddedCharactersAfter_Phone(newText, caretPosition));
	}

	@Override
	protected void checkSubclass() {}
}
