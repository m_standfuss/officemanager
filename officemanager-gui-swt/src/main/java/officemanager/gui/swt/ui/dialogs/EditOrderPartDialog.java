package officemanager.gui.swt.ui.dialogs;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.google.common.collect.Maps;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord.OrderStatus;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PaymentRecord.PaymentType;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.biz.records.PriceOverrideRecord.EntityName;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.EditOrderPartComposite;
import officemanager.gui.swt.util.MessageDialogs;

public class EditOrderPartDialog extends Dialog {

	private static final int HEIGHT = 225;
	private static final int WIDTH = 330;
	private final Long orderToPartId;
	private final Long orderId;
	private final OrderDelegate orderDelegate;
	private final PaymentDelegate paymentDelegate;

	private boolean saved;
	private OrderPartRecord orderPartRecord;
	private PriceOverrideRecord overridePriceRecord;

	private Shell shell;
	private EditOrderPartComposite composite;
	private Button btnRefund;
	private Button btnRemove;
	private Button btnSave;
	private Button btnCancel;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public EditOrderPartDialog(Shell parent, Long orderId, Long orderToPartId) {
		super(parent, SWT.NONE);
		this.orderId = orderId;
		this.orderToPartId = orderToPartId;
		orderDelegate = new OrderDelegate();
		paymentDelegate = new PaymentDelegate();
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public boolean open() {
		createContents();
		addListeners();
		populateContents();
		enableContent();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return saved;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.RESIZE | SWT.TITLE | SWT.PRIMARY_MODAL);
		shell.setSize(WIDTH, HEIGHT);
		shell.setLayout(new GridLayout());

		Rectangle parentSize = getParent().getBounds();

		int locationX, locationY;
		locationX = (parentSize.width - WIDTH) / 2 + parentSize.x;
		locationY = (parentSize.height - HEIGHT) / 2 + parentSize.y;

		shell.setLocation(new Point(locationX, locationY));

		composite = new EditOrderPartComposite(shell, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		Composite composite = new Composite(shell, SWT.RIGHT_TO_LEFT);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite.setLayout(new GridLayout(4, false));

		btnCancel = new Button(composite, SWT.NONE);
		btnCancel.setLayoutData(new GridData());
		btnCancel.setText("Cancel");

		btnRemove = new Button(composite, SWT.NONE);
		btnRemove.setLayoutData(new GridData());
		btnRemove.setText("Remove");

		btnRefund = new Button(composite, SWT.NONE);
		btnRefund.setLayoutData(new GridData());
		btnRefund.setText("Refund");

		btnSave = new Button(composite, SWT.NONE);
		btnSave.setLayoutData(new GridData());
		btnSave.setText("Save");
	}

	private void addListeners() {
		btnCancel.addListener(SWT.Selection, e -> cancel());
		btnRefund.addListener(SWT.Selection, e -> refund());
		btnRemove.addListener(SWT.Selection, e -> remove());
		btnSave.addListener(SWT.Selection, e -> save());
		composite.btnChangePrice.addListener(SWT.Selection, e -> changePrice());
	}

	private void populateContents() {
		shell.setText("Order #: " + AppPrefs.FORMATTER.formatOrderNumber(orderId));
		List<OrderClientProductRecord> clientProducts = orderDelegate.loadClientProducts(orderId);
		Map<Long, OrderClientProductRecord> clientProductMap = Maps.uniqueIndex(clientProducts,
				OrderClientProductRecord::getClientProductId);
		composite.hdrValClientProduct.populateChoices(clientProducts);

		orderPartRecord = orderDelegate.loadOrderPart(orderToPartId);
		composite.hdrValManufacturer.setValue(orderPartRecord.getManufacturerDisplay());
		composite.hdrValPart.setValue(orderPartRecord.getPartName());
		if (orderPartRecord.isRefunded()) {
			composite.hdrValPrice.setValue("REFUNDED");
		} else {
			composite.hdrValPrice.setValue(AppPrefs.FORMATTER.formatMoney(orderPartRecord.getPartPrice()));
		}
		composite.hdrValQnt.setValue((int) orderPartRecord.getQuantity());
		composite.hdrValClientProduct.setValue(clientProductMap.get(orderPartRecord.getClientProductId()));
	}

	private void enableContent() {
		OrderStatus orderStatus = orderDelegate.getOrderStatus(orderId);

		composite.hdrValClientProduct.makeEditable(orderStatus == OrderStatus.OPEN);
		composite.hdrValQnt.makeEditable(orderStatus == OrderStatus.OPEN);

		if (orderStatus == OrderStatus.OPEN) {
			((GridData) btnRefund.getLayoutData()).exclude = true;
			btnRefund.setVisible(false);

		} else {
			btnCancel.setText("Close");

			((GridData) composite.btnChangePrice.getLayoutData()).exclude = true;
			composite.btnChangePrice.setVisible(false);

			((GridData) btnSave.getLayoutData()).exclude = true;
			btnSave.setVisible(false);

			((GridData) btnRemove.getLayoutData()).exclude = true;
			btnRemove.setVisible(false);

			if (orderStatus != OrderStatus.CLOSED) {
				((GridData) btnRefund.getLayoutData()).exclude = true;
				btnRefund.setVisible(false);
			} else {
				btnRefund.setEnabled(
						AppPrefs.hasPermission(AppPermissions.REFUND_ORDER) && !orderPartRecord.isRefunded());
			}
		}
	}

	private void cancel() {
		if (overridePriceRecord != null) {
			if (!MessageDialogs.askQuestion(shell, "Confirm Cancel",
					"A price override was added, are you sure you wish to cancel?")) {
				return;
			}
		}
		shell.close();
	}

	private void save() {
		if (!MessageDialogs.askQuestion(shell, "Confirm Save", "Are you sure you wish to save this part?")) {
			return;
		}
		OrderClientProductRecord selectedProduct = composite.hdrValClientProduct.getEditableValue();
		Long clientProductId = null;
		if (selectedProduct != null) {
			clientProductId = selectedProduct.getClientProductId();
		}
		orderPartRecord.setClientProductId(clientProductId);
		orderPartRecord.setQuantity(composite.hdrValQnt.getEditableValue());

		orderDelegate.insertOrUpdatePart(orderId, orderPartRecord);

		if (overridePriceRecord != null) {
			orderDelegate.insertPriceOverride(overridePriceRecord);
		}

		saved = true;
		shell.close();
	}

	private void remove() {
		if (!MessageDialogs.askQuestion(shell, "Confirm Removal", "Are you sure you wish to remove this part?")) {
			return;
		}
		orderDelegate.inactivatePart(orderToPartId);
		saved = true;
		shell.close();
	}

	private void refund() {
		if (!MessageDialogs.askQuestion(shell, "Confirm Refund", "Are you sure you wish to refund this part?")) {
			return;
		}
		orderDelegate.refundPart(orderToPartId);

		BigDecimal refundTotal = Calculator.totalPart(orderPartRecord.getPartPrice(),
				BigDecimal.valueOf(orderPartRecord.getQuantity()), orderPartRecord.getTaxAmount(),
				orderPartRecord.isTaxExempt());
		PaymentRecord paymentRecord = new PaymentRecord();
		paymentRecord.setAmount(Calculator.negate(refundTotal));
		paymentRecord.setPaymentDescription("ORDER REFUND");
		paymentRecord.setPaymentNumber(AppPrefs.FORMATTER.formatOrderNumber(orderId));
		paymentRecord.setPaymentType(PaymentType.REFUND);
		paymentDelegate.makePaymentOnOrder(orderId, paymentRecord);

		saved = true;
		shell.close();
	}

	private void changePrice() {
		PriceOverrideDialog dialog = new PriceOverrideDialog(shell, SWT.NONE);
		if (dialog.open(orderPartRecord.getPartPrice()) != SWT.SAVE) {
			return;
		}
		overridePriceRecord = new PriceOverrideRecord();
		overridePriceRecord.setEntityId(orderPartRecord.getPartId());
		overridePriceRecord.setEntityName(EntityName.PART);
		overridePriceRecord.setOrderId(orderId);
		overridePriceRecord.setOriginalAmount(orderPartRecord.getPartPrice());
		overridePriceRecord.setOverrideAmount(dialog.getNewPrice());
		overridePriceRecord.setOverrideDttm(new Date());
		overridePriceRecord.setOverridePrsnlId(AppPrefs.currentSession.prsnlId);
		overridePriceRecord.setOverrideReason(dialog.getOverrideReason());

		orderPartRecord.setPartPrice(dialog.getNewPrice());
		composite.hdrValPrice.setValue(AppPrefs.FORMATTER.formatMoney(dialog.getNewPrice()));
	}
}
