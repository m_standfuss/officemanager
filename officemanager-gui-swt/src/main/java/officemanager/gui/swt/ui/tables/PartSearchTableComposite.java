package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class PartSearchTableComposite extends Composite {

	public static final int COL_PART_NAME = 0;
	public static final int COL_MANUF = 1;
	public static final int COL_MANUF_ID = 2;
	public static final int COL_CURRENT_PRICE = 3;
	public static final int COL_BASE_PRICE = 4;
	public static final int COL_INVNTRY_STATUS = 5;
	public static final int COL_INVNTRY_CNT = 6;
	public static final int COL_DESCRIPTION = 7;
	public static final int COL_AMT_SOLD = 8;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnPartName;
	public final TableColumn tblclmnManufacturer;
	public final TableColumn tblclmnManufId;
	public final TableColumn tblclmnPrice;
	public final TableColumn tblclmnBasePrice;
	public final TableColumn tblclmnDescription;
	public final TableColumn tblclmnInventoryStatus;
	public final TableColumn tblclmnInventoryCnt;
	public final TableColumn tblclmnAmountSold;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PartSearchTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new FillLayout());

		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnPartName = new TableColumn(table, SWT.NONE);
		tblclmnPartName.setWidth(100);
		tblclmnPartName.setText("Part Name");

		tblclmnManufacturer = new TableColumn(table, SWT.NONE);
		tblclmnManufacturer.setWidth(100);
		tblclmnManufacturer.setText("Manufacturer");

		tblclmnManufId = new TableColumn(table, SWT.NONE);
		tblclmnManufId.setWidth(100);
		tblclmnManufId.setText("Manuf. ID");

		tblclmnPrice = new TableColumn(table, SWT.NONE);
		tblclmnPrice.setWidth(100);
		tblclmnPrice.setText("Current Price");

		tblclmnBasePrice = new TableColumn(table, SWT.NONE);
		tblclmnBasePrice.setWidth(100);
		tblclmnBasePrice.setText("Base Price");

		tblclmnInventoryStatus = new TableColumn(table, SWT.NONE);
		tblclmnInventoryStatus.setWidth(100);
		tblclmnInventoryStatus.setText("Inventory Status");

		tblclmnInventoryCnt = new TableColumn(table, SWT.NONE);
		tblclmnInventoryCnt.setWidth(100);
		tblclmnInventoryCnt.setText("Inventory Count");

		tblclmnDescription = new TableColumn(table, SWT.NONE);
		tblclmnDescription.setWidth(100);
		tblclmnDescription.setText("Description");

		tblclmnAmountSold = new TableColumn(table, SWT.NONE);
		tblclmnAmountSold.setWidth(100);
		tblclmnAmountSold.setText("Amount Sold");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
