package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.CodeValueTable;
import officemanager.gui.swt.ui.tables.FlatRateTableComposite;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class FlatRateSearchComposite extends Composite {
	public final ToolBar toolBar;
	public final ToolItem tlItmNewFlatRate;

	public final ScrolledComposite scrCompositeSideBar;
	public final Composite compositeSideBar;
	public final Button btnSearch;
	public final Button btnClear;

	public final Composite compositeCenter;
	public final FlatRateTableComposite searchResultsTable;
	public final Label lblServiceName;
	public final UpperCaseText txtServiceName;
	public final Label lblProductType;
	public final Label lblCategory;
	public final CodeValueTable tblProductType;
	public final CodeValueTable tblCategory;
	public final Button btnSaleItems;
	public final Label lblDenotesCurrentSale;

	public FlatRateSearchComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		tlItmNewFlatRate = new ToolItem(toolBar, SWT.NONE);
		tlItmNewFlatRate.setImage(ResourceManager.getImage(ImageFile.SERVICE_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmNewFlatRate.setToolTipText("New Flat Rate Service");
		tlItmNewFlatRate.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_FLAT_RATE));

		scrCompositeSideBar = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeSideBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		compositeSideBar = new Composite(scrCompositeSideBar, SWT.NONE);
		compositeSideBar.setLayout(new GridLayout(2, false));

		lblServiceName = new Label(compositeSideBar, SWT.NONE);
		lblServiceName.setText("Service Name");
		lblServiceName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		new Label(compositeSideBar, SWT.NONE);

		txtServiceName = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_txtServiceName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtServiceName.widthHint = 150;
		txtServiceName.setLayoutData(gd_txtServiceName);

		lblCategory = new Label(compositeSideBar, SWT.NONE);
		lblCategory.setText("Category");
		lblCategory.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		new Label(compositeSideBar, SWT.NONE);

		tblCategory = new CodeValueTable(compositeSideBar);
		tblCategory.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		lblProductType = new Label(compositeSideBar, SWT.NONE);
		lblProductType.setText("Product Type");
		lblProductType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		new Label(compositeSideBar, SWT.NONE);

		tblProductType = new CodeValueTable(compositeSideBar);
		tblProductType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		btnSaleItems = new Button(compositeSideBar, SWT.CHECK);
		btnSaleItems.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnSaleItems.setText("Sale Items");

		btnSearch = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnSearch = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnSearch.widthHint = 100;
		btnSearch.setLayoutData(gd_btnSearch);
		btnSearch.setText("Search");

		btnClear = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnClear = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnClear.widthHint = 100;
		btnClear.setLayoutData(gd_btnClear);
		btnClear.setText("Clear");

		scrCompositeSideBar.setContent(compositeSideBar);
		scrCompositeSideBar.setMinSize(compositeSideBar.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		compositeCenter = new Composite(this, SWT.BORDER);
		compositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeCenter.setLayout(new GridLayout(1, false));

		searchResultsTable = new FlatRateTableComposite(compositeCenter, SWT.NONE, tableStyle);
		searchResultsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		lblDenotesCurrentSale = new Label(compositeCenter, SWT.NONE);
		lblDenotesCurrentSale.setText("Denotes Current Sale Price");
		lblDenotesCurrentSale.setForeground(AppPrefs.COLOR_DISCOUNT_PRICE);
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public FlatRateSearchComposite(Composite parent, int style) {
		this(parent, style, SWT.BORDER | SWT.FULL_SELECTION);
	}

	public void hideToolItems(boolean newFlatRate) {
		if (newFlatRate) {
			tlItmNewFlatRate.dispose();
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
