package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.biz.records.OrderClientProductRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.widgets.HeaderValueCombo;
import officemanager.gui.swt.ui.widgets.HeaderValueMultiLineText;
import officemanager.gui.swt.ui.widgets.HeaderValueText;
import officemanager.gui.swt.util.ResourceManager;

public class EditOrderServiceComposite extends Composite {
	public final HeaderValueText hdrValServiceName;
	public final HeaderValueMultiLineText hdrValServiceDescription;
	public final HeaderValueCombo<OrderClientProductRecord> hdrValClientProduct;
	public final HeaderValueDecimalText hdrValLaborRate;
	public final HeaderValueDecimalText hdrValLaborHours;
	public Label lblRefunded;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public EditOrderServiceComposite(Composite parent, int style) {
		super(parent, style);

		setLayout(new GridLayout());

		hdrValServiceName = new HeaderValueText(this, SWT.HORIZONTAL);
		hdrValServiceName.setHeaderText("Service Name:");

		hdrValServiceDescription = new HeaderValueMultiLineText(this, SWT.VERTICAL);
		hdrValServiceDescription.setHeaderText("Description");

		hdrValClientProduct = new HeaderValueCombo<OrderClientProductRecord>(this, SWT.HORIZONTAL) {
			@Override
			protected String getDisplay(OrderClientProductRecord productRecord) {
				if (productRecord == null) {
					return "";
				}
				return AppPrefs.FORMATTER.formatClientProductDisplay(productRecord.getProductManufacturer(),
						productRecord.getProductModel(), productRecord.getSerialNumber());
			}
		};
		hdrValClientProduct.setHeaderText("Client Product:");

		lblRefunded = new Label(this, SWT.NONE);
		lblRefunded.setLayoutData(new GridData());
		lblRefunded.setText("REFUNDED");
		lblRefunded.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblRefunded.setForeground(AppPrefs.COLOR_DISCOUNT_PRICE);

		hdrValLaborRate = new HeaderValueDecimalText(this, SWT.HORIZONTAL, 2);
		hdrValLaborRate.setHeaderText("Labor Rate:");
		hdrValLaborRate.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_SERVICE_LABOR_RATE));

		hdrValLaborHours = new HeaderValueDecimalText(this, SWT.HORIZONTAL, 2);
		hdrValLaborHours.setHeaderText("Labor Hours:");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
