package officemanager.gui.swt.ui.wizards;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.ui.wizards.pages.PartSelectionPage;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.utility.CollectionUtility;

public class PartSelectionWizard extends OfficeManagerWizard {

	private static final Logger logger = LogManager.getLogger(PartSelectionWizard.class);

	private final int maxSelection;
	private final boolean allowSearch;
	private final boolean removeCurrentPrice;
	private final boolean needsSelection;

	private List<PartSearchRecord> prePopulateParts;
	private List<PartSearchRecord> results;
	private PartSelectionPage page;

	public PartSelectionWizard() {
		this(Integer.MAX_VALUE, true);
	}

	public PartSelectionWizard(int maxSelection, boolean allowSearch) {
		setWindowTitle("Part Selection");
		this.maxSelection = maxSelection;
		this.allowSearch = allowSearch;
		removeCurrentPrice = true;
		needsSelection = true;
	}

	public PartSelectionWizard(int maxSelection, boolean allowSearch, boolean removeCurrentPrice,
			boolean needsSelection) {
		setWindowTitle("Part Selection");
		this.maxSelection = maxSelection;
		this.allowSearch = allowSearch;
		this.removeCurrentPrice = removeCurrentPrice;
		this.needsSelection = needsSelection;
	}

	@Override
	public void addPages() {
		page = new PartSelectionPage(maxSelection, needsSelection, removeCurrentPrice, allowSearch);
		page.addFirstShowListener(l -> {
			if (prePopulateParts != null) {
				page.populateParts(prePopulateParts, true);
			}
		});
		addPage(page);
	}

	@Override
	public boolean performFinish() {
		results = page.getSelectedRecords();
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	public void showParts(List<PartSearchRecord> parts) {
		if (page == null) {
			prePopulateParts = parts;
		} else {
			page.populateParts(parts, true);
		}
	}

	public List<PartSearchRecord> getResults() {
		return results;
	}

	public PartSearchRecord getResult() {
		if (CollectionUtility.isNullOrEmpty(results)) {
			logger.warn("Getting results but no results have been set. Returning null.");
			return null;
		}
		if (results.size() > 1) {
			logger.warn("Getting one results but multiple were selected returning the first.");
		}
		return results.get(0);
	}

}
