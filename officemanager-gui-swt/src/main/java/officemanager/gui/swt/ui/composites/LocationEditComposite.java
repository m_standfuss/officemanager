package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class LocationEditComposite extends Composite {
	public Label lblDisplayName;
	public Label lblAbbreviatedName;
	public UpperCaseText txtDisplayName;
	public UpperCaseText txtAbbrName;
	public Label lblParentLocation;
	public UpperCaseText txtParentLocation;
	public Label lblLocationType;
	public CodeValueCombo cmbLocationType;
	public Button btnSelectLocation;
	public Label lblDenotesA;
	public Button btnRootLocation;
	public Composite compositeParentLocation;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public LocationEditComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		GridLayout gridLayout = new GridLayout(3, false);
		setLayout(gridLayout);

		lblDisplayName = new Label(this, SWT.NONE);
		lblDisplayName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDisplayName.setText("Display Name:*");
		lblDisplayName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtDisplayName = new UpperCaseText(this, SWT.BORDER);
		txtDisplayName.setTextLimit(50);
		GridData gd_txtDisplayName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtDisplayName.widthHint = 150;
		txtDisplayName.setLayoutData(gd_txtDisplayName);

		compositeParentLocation = new Composite(this, SWT.NONE);
		compositeParentLocation.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		GridLayout gl_compositeParentLocation = new GridLayout(3, false);
		gl_compositeParentLocation.marginWidth = 0;
		gl_compositeParentLocation.marginHeight = 0;
		compositeParentLocation.setLayout(gl_compositeParentLocation);

		lblParentLocation = new Label(compositeParentLocation, SWT.NONE);
		lblParentLocation.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblParentLocation.setText("Parent Location:");
		lblParentLocation.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtParentLocation = new UpperCaseText(compositeParentLocation, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_txtParentLocation = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtParentLocation.widthHint = 125;
		txtParentLocation.setLayoutData(gd_txtParentLocation);

		btnSelectLocation = new Button(compositeParentLocation, SWT.NONE);
		btnSelectLocation.setText("Select Location");

		lblAbbreviatedName = new Label(this, SWT.NONE);
		lblAbbreviatedName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAbbreviatedName.setText("Abbreviated Name:*");
		lblAbbreviatedName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtAbbrName = new UpperCaseText(this, SWT.BORDER);
		txtAbbrName.setTextLimit(5);
		GridData gd_txtAbbrName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAbbrName.widthHint = 150;
		txtAbbrName.setLayoutData(gd_txtAbbrName);

		btnRootLocation = new Button(this, SWT.CHECK);
		GridData gd_btnRootLocation = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnRootLocation.horizontalIndent = 5;
		btnRootLocation.setLayoutData(gd_btnRootLocation);
		btnRootLocation.setText("No Parent Location");

		lblLocationType = new Label(this, SWT.NONE);
		lblLocationType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLocationType.setText("Location Type:*");
		lblLocationType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbLocationType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbLocationType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbLocationType.widthHint = 125;
		cmbLocationType.setLayoutData(gd_cmbLocationType);

		lblDenotesA = new Label(this, SWT.NONE);
		lblDenotesA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		lblDenotesA.setText("* Denotes a required field");
		lblDenotesA.setFont(ResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
