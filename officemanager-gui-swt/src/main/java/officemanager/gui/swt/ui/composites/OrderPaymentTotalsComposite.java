package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.util.ResourceManager;

public class OrderPaymentTotalsComposite extends Composite {
	public Label lblTotalHeader;
	public Label lblTotal;
	public Label lblPaymentsHeader;
	public Label lblPayments;
	public Label lblBalanceHeader;
	public Label lblBalance;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public OrderPaymentTotalsComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 6;
		setLayout(gridLayout);

		lblTotalHeader = new Label(this, SWT.NONE);
		lblTotalHeader.setText("Total:");
		lblTotalHeader.setFont(ResourceManager.getFont("Segoe UI", 11, SWT.BOLD));

		lblTotal = new Label(this, SWT.NONE);
		lblTotal.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		lblPaymentsHeader = new Label(this, SWT.NONE);
		lblPaymentsHeader.setText("Payment(s):");
		lblPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 11, SWT.BOLD));

		lblPayments = new Label(this, SWT.NONE);
		lblPayments.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		lblBalanceHeader = new Label(this, SWT.NONE);
		lblBalanceHeader.setText("Balance:");
		lblBalanceHeader.setFont(ResourceManager.getFont("Segoe UI", 11, SWT.BOLD));

		lblBalance = new Label(this, SWT.NONE);
		lblBalance.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
