package officemanager.gui.swt.ui.wizards;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PaymentRecord.PaymentType;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.OrderFlatRateService;
import officemanager.gui.swt.services.OrderPartService;
import officemanager.gui.swt.services.OrderServiceService;
import officemanager.gui.swt.ui.composites.RefundDetailsComposite.PartItem;
import officemanager.gui.swt.ui.wizards.pages.OrderComponentSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.RefundDetailsPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.utility.Tuple;

public class OrderRefundWizard extends OfficeManagerWizard {

	private final Long orderId;

	private final OrderDelegate orderDelegate;
	private final PaymentDelegate paymentDelegate;

	private final OrderFlatRateService orderFlatRateService;
	private final OrderPartService orderPartService;
	private final OrderServiceService orderServiceService;

	private OrderComponentSelectionPage orderCompPage;
	private RefundDetailsPage refundDetailsPage;

	public OrderRefundWizard(Long orderId) {
		this.orderId = orderId;

		orderDelegate = new OrderDelegate();
		paymentDelegate = new PaymentDelegate();

		orderFlatRateService = new OrderFlatRateService();
		orderPartService = new OrderPartService();
		orderServiceService = new OrderServiceService();

		setWindowTitle("Order Refund");
	}

	@Override
	public boolean canFinish() {
		return getContainer().getCurrentPage() == refundDetailsPage;
	}

	@Override
	public void addPages() {
		orderCompPage = new OrderComponentSelectionPage(true);
		orderCompPage.addFirstShowListener(e -> showOrderComponents());
		addPage(orderCompPage);

		refundDetailsPage = new RefundDetailsPage();
		refundDetailsPage.addIsShownListener(e -> populateDetailsPage());
		addPage(refundDetailsPage);
	}

	@Override
	public boolean performFinish() {
		if(!MessageDialogs.askQuestion(getShell(), "Confirm Refund", "Are you sure you wish to refund these items?")){
			return false;
		}

		BigDecimal refundTotal = Calculator.getZero();

		List<OrderPartRecord> parts = orderPartService.getCheckedParts();
		Map<Long, OrderPartRecord> partMap = Maps.newHashMap();
		for(OrderPartRecord opr : parts){
			partMap.put(opr.getOrderToPartId(), opr);
		}

		List<Tuple<Long, PartItem>> partItems = refundDetailsPage.getParts();
		for (Tuple<Long, PartItem> part : partItems) {
			orderDelegate.refundPart(part.item1, (long) part.item2.spnPartQuantity.getSelection());

			OrderPartRecord partRecord = partMap.get(part.item1);
			refundTotal = Calculator.add(refundTotal,
					Calculator.totalPart(partRecord.getPartPrice(), BigDecimal.valueOf(part.item2.spnPartQuantity.getSelection()), partRecord.getTaxAmount(),
							partRecord.isTaxExempt()));
		}

		List<OrderServiceRecord> services = orderServiceService.getCheckedServices();
		for (OrderServiceRecord service : services) {
			orderDelegate.refundService(service.getServiceId());
			refundTotal = Calculator.add(refundTotal,
					Calculator.calculatePrice(service.getPricePerHour(), service.getHoursQnt()));
		}

		List<OrderFlatRateServiceRecord> flatRates = orderFlatRateService.getCheckedFlatRates();
		for (OrderFlatRateServiceRecord flatRate : flatRates) {
			orderDelegate.refundFlatRateService(flatRate.getFlatRateServiceId());
			refundTotal = Calculator.add(refundTotal, flatRate.getServicePrice());
		}

		PaymentRecord paymentRecord = makePaymentRecord(refundTotal);
		paymentDelegate.makePaymentOnOrder(orderId, paymentRecord);

		MessageDialogs.displayMessage(getShell(), "Refund Processed",
				"The client is owed " + AppPrefs.FORMATTER.formatMoney(refundTotal) + " in a refund.");
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	private void showOrderComponents() {
		orderFlatRateService.setComposite(orderCompPage.composite.tblFlatRates);
		orderPartService.setComposite(orderCompPage.composite.tblParts);
		orderServiceService.setComposite(orderCompPage.composite.tblServices);

		OrderRecord record = orderDelegate.loadOrderRecord(orderId);
		orderFlatRateService.populateTable(filterRefundedFlatRates(record.getFlatRates()));
		orderPartService.populateTable(filterRefundedParts(record.getParts()));
		orderServiceService.populateTable(filterRefundedServices(record.getServices()));
	}

	private List<OrderPartRecord> filterRefundedParts(List<OrderPartRecord> parts) {
		List<OrderPartRecord> filteredList = Lists.newArrayList();
		for (OrderPartRecord part : parts) {
			if (part.isRefunded()) {
				continue;
			}
			filteredList.add(part);
		}
		return filteredList;
	}

	private List<OrderServiceRecord> filterRefundedServices(List<OrderServiceRecord> services) {
		List<OrderServiceRecord> filteredList = Lists.newArrayList();
		for (OrderServiceRecord part : services) {
			if (part.isRefunded()) {
				continue;
			}
			filteredList.add(part);
		}
		return filteredList;
	}

	private List<OrderFlatRateServiceRecord> filterRefundedFlatRates(List<OrderFlatRateServiceRecord> flatRates) {
		List<OrderFlatRateServiceRecord> filteredList = Lists.newArrayList();
		for (OrderFlatRateServiceRecord part : flatRates) {
			if (part.isRefunded()) {
				continue;
			}
			filteredList.add(part);
		}
		return filteredList;
	}

	private void populateDetailsPage() {
		List<OrderPartRecord> parts = orderPartService.getCheckedParts();
		refundDetailsPage.populateParts(parts);

		List<OrderFlatRateServiceRecord> flatRates = orderFlatRateService.getCheckedFlatRates();
		refundDetailsPage.populateFlatRates(flatRates);

		List<OrderServiceRecord> services = orderServiceService.getCheckedServices();
		refundDetailsPage.populateServices(services);
	}

	private PaymentRecord makePaymentRecord(BigDecimal refundTotal) {
		PaymentRecord record = new PaymentRecord();
		record.setAmount(Calculator.negate(refundTotal));
		record.setPaymentDescription("ORDER REFUND");
		record.setPaymentNumber(AppPrefs.FORMATTER.formatOrderNumber(orderId));
		record.setPaymentType(PaymentType.REFUND);
		return record;
	}
}
