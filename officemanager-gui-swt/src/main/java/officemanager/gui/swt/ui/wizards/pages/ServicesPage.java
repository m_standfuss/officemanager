package officemanager.gui.swt.ui.wizards.pages;

import java.math.BigDecimal;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.records.LongTextRecord;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.ServiceService;
import officemanager.gui.swt.ui.composites.ButtonBarComposite;
import officemanager.gui.swt.ui.composites.ServiceEditComposite;
import officemanager.gui.swt.ui.tables.ServiceTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ServicesPage extends OfficeManagerWizardPage {

	private final ServiceService serviceService;

	private ServiceTableComposite serviceTbl;
	private ServiceEditComposite newServiceComposite;
	private Button btnRemoveService;

	private BigDecimal laborRate;

	public ServicesPage() {
		super("servicePage", "Services", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("Enter services for the order.");
		setPageComplete(true);
		serviceService = new ServiceService();
		laborRate = BigDecimal.ZERO;
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		Composite root = new Composite(scrCompositeSideBar, SWT.NONE);
		root.setLayout(new GridLayout());

		CollapsibleComposite clpseCmpNewService = new CollapsibleComposite(root, SWT.NONE);
		clpseCmpNewService.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		clpseCmpNewService.composite.setLayout(new GridLayout());
		clpseCmpNewService.setTitleText("New Service");

		newServiceComposite = new ServiceEditComposite(clpseCmpNewService.composite, SWT.NONE);
		newServiceComposite.txtLaborRate.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_SERVICE_LABOR_RATE));

		ButtonBarComposite btnBar = new ButtonBarComposite(clpseCmpNewService.composite, SWT.NONE);
		btnBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		serviceTbl = new ServiceTableComposite(root, SWT.NONE, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_serviceTbl = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_serviceTbl.heightHint = 200;
		serviceTbl.setLayoutData(gd_serviceTbl);

		btnRemoveService = new Button(root, SWT.NONE);
		btnRemoveService.setText("Remove Selected Service");
		btnRemoveService.addListener(SWT.Selection, e -> removeSelectedService());

		scrCompositeSideBar.setContent(root);
		scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrCompositeSideBar);

		clpseCmpNewService.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT)));
		btnBar.btnSave.addListener(SWT.Selection, e -> addNewService());
		btnBar.btnClear.addListener(SWT.Selection, e -> clear());
		serviceService.setComposite(serviceTbl);
	}

	@Override
	public void setFocus() {
		newServiceComposite.txtServiceName.setFocus();
	}

	private void removeSelectedService() {
		int index = serviceTbl.table.getSelectionIndex();
		if (index == -1) {
			return;
		}
		serviceTbl.table.remove(index);
	}

	public void populateLaborRate(BigDecimal laborRate) {
		this.laborRate = laborRate;
		newServiceComposite.txtLaborRate.setValue(laborRate);
	}

	public void populateServices(List<OrderServiceRecord> services) {
		serviceService.populateServiceTable(services);
	}

	public List<OrderServiceRecord> getServices() {
		return serviceTbl.table.getDataAll(ServiceService.DATA_RECORD);
	}

	private void addNewService() {
		if (!validate()) {
			return;
		}
		setErrorMessage(null);
		OrderServiceRecord record = new OrderServiceRecord();
		record.setHoursQnt(newServiceComposite.txtHours.getValue());
		record.setPricePerHour(newServiceComposite.txtLaborRate.getValue());
		record.setServiceName(newServiceComposite.txtServiceName.getText());
		if (!newServiceComposite.txtDescription.getText().isEmpty()) {
			record.setServiceText(new LongTextRecord(newServiceComposite.txtDescription.getText()));
		}

		serviceService.addServiceRow(record);
		clear();
	}

	private boolean validate() {
		if (newServiceComposite.txtHours.getValue() == null) {
			setErrorMessage("Please populate the labor amount.");
			return false;
		}
		if (newServiceComposite.txtLaborRate.getValue() == null) {
			setErrorMessage("Please populate a labor rate.");
			return false;
		}
		if (newServiceComposite.txtServiceName.getText().isEmpty()) {
			setErrorMessage("Please populate a service name.");
			return false;
		}
		return true;
	}

	public void clear() {
		newServiceComposite.txtDescription.setText("");
		newServiceComposite.txtHours.setText("");
		newServiceComposite.txtLaborRate.setText(AppPrefs.FORMATTER.formatMoney(laborRate));
		newServiceComposite.txtServiceName.setText("");
	}

}
