package officemanager.gui.swt.ui.wizards.pages;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.records.PersonRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PersonEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.StringUtility;

public class PersonInformationPage extends OfficeManagerWizardPage {

	private PersonEditComposite composite;
	private boolean fullFormattedChanged;

	public PersonInformationPage() {
		super("PersonInformation", "Personal Information", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.USER_EDIT, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("Required fields must be completed.");
		setPageComplete(false);
		fullFormattedChanged = false;
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		composite = new PersonEditComposite(scrCompositeSideBar, SWT.NULL);

		scrCompositeSideBar.setContent(composite);
		scrCompositeSideBar.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		composite.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));
		setControl(scrCompositeSideBar);
		composite.txtLastName.addModifyListener(l -> checkPageComplete());
		composite.txtFirstName.addListener(SWT.KeyUp, l -> formatFullName());
		composite.txtLastName.addListener(SWT.KeyUp, l -> formatFullName());
		composite.txtFullFormatted.addListener(SWT.KeyUp, l -> fullFormattedChanged = true);
	}

	@Override
	public void setFocus() {
		composite.txtFirstName.setFocus();
	}

	public void populatePersonInformation(PersonRecord record) {
		composite.txtEmail.setText(record.getEmail());
		composite.txtFirstName.setText(record.getNameFirst());
		composite.txtFullFormatted.setText(record.getNameFullFormatted());
		composite.txtLastName.setText(record.getNameLast());
		if (!record.getNameFullFormatted().isEmpty()) {
			fullFormattedChanged = true;
		}
		checkPageComplete();
	}

	public void populatePersonRecord(PersonRecord record) {
		record.setEmail(composite.txtEmail.getText());
		record.setNameFirst(composite.txtFirstName.getText());
		record.setNameFullFormatted(composite.txtFullFormatted.getText());
		record.setNameLast(composite.txtLastName.getText());
	}

	private void formatFullName() {
		if (fullFormattedChanged) {
			return;
		}

		String fullFormattedName = composite.txtLastName.getText().trim();
		if (!fullFormattedName.isEmpty() && !composite.txtFirstName.getText().trim().isEmpty()) {
			fullFormattedName += ", ";
		}
		fullFormattedName += composite.txtFirstName.getText().trim();
		composite.txtFullFormatted.setText(fullFormattedName);
	}

	private void checkPageComplete() {
		boolean isComplete = isComplete();
		setPageComplete(isComplete);
		if (isComplete) {
			setDescription("");
		} else {
			setDescription("Required fields must be completed.");
		}
	}

	private boolean isComplete() {
		return !StringUtility.isNullOrWhiteSpace(composite.txtLastName.getText());
	}
}
