package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ClientProductEditComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmDeleteProduct;

	public final Label lblProductType;
	public final Label lblManufacturer;
	public final Label lblModel;
	public final Label lblSerialNumber;
	public final Label lblProductStatus;
	public final Label lblBarcode;
	public final Label lblWorkCount;
	public final CodeValueCombo cmbProductType;
	public final UpperCaseText txtManufacturer;
	public final CodeValueCombo cmbProductStatus;
	public final Text txtBarcode;
	public final UpperCaseText txtModel;
	public final Text txtSerialNumber;
	public final Text txtWorkCnt;
	public final Label lbldenotesRequiredField;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ClientProductEditComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 30;
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		tlItmDeleteProduct = new ToolItem(toolBar, SWT.NONE);
		tlItmDeleteProduct.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmDeleteProduct.setToolTipText("Delete Product");
		tlItmDeleteProduct.setEnabled(AppPrefs.hasPermission(AppPermissions.REMOVE_CLIENT_PRODUCT));

		lblProductType = new Label(this, SWT.NONE);
		lblProductType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblProductType.setText("Product Type*:");

		lblProductStatus = new Label(this, SWT.NONE);
		lblProductStatus.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblProductStatus.setText("Product Status:");

		cmbProductType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbProductType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbProductType.widthHint = 150;
		cmbProductType.setLayoutData(gd_cmbProductType);

		cmbProductStatus = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbProductStatus = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbProductStatus.widthHint = 150;
		cmbProductStatus.setLayoutData(gd_cmbProductStatus);

		lblManufacturer = new Label(this, SWT.NONE);
		lblManufacturer.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblManufacturer.setText("Manufacturer*:");

		lblBarcode = new Label(this, SWT.NONE);
		lblBarcode.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblBarcode.setText("Barcode:");

		txtManufacturer = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtManufacturer = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtManufacturer.widthHint = 150;
		txtManufacturer.setLayoutData(gd_txtManufacturer);

		txtBarcode = new Text(this, SWT.BORDER);
		GridData gd_txtBarcode = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtBarcode.widthHint = 100;
		txtBarcode.setLayoutData(gd_txtBarcode);

		lblModel = new Label(this, SWT.NONE);
		lblModel.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblModel.setText("Model:");

		lblWorkCount = new Label(this, SWT.NONE);
		lblWorkCount.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblWorkCount.setText("Work Count:");

		txtModel = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtModel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtModel.widthHint = 150;
		txtModel.setLayoutData(gd_txtModel);

		txtWorkCnt = new Text(this, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_txtWorkCnt = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtWorkCnt.widthHint = 35;
		txtWorkCnt.setLayoutData(gd_txtWorkCnt);

		lblSerialNumber = new Label(this, SWT.NONE);
		lblSerialNumber.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblSerialNumber.setText("Serial Number:");
		new Label(this, SWT.NONE);

		txtSerialNumber = new Text(this, SWT.BORDER);
		GridData gd_txtSerialNumber = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtSerialNumber.widthHint = 125;
		txtSerialNumber.setLayoutData(gd_txtSerialNumber);
		new Label(this, SWT.NONE);

		lbldenotesRequiredField = new Label(this, SWT.NONE);
		lbldenotesRequiredField.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lbldenotesRequiredField.setText("*Denotes required field.");
		setTabList(new Control[] { cmbProductType, cmbProductStatus, txtManufacturer, txtBarcode, txtModel,
				txtSerialNumber });
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
