package officemanager.gui.swt.ui.wizards;

import java.math.BigDecimal;
import java.util.List;

import officemanager.biz.Calculator;
import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.delegates.PaymentDelegate;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderRecord.OrderStatus;
import officemanager.biz.records.OrderRecord.OrderType;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.OrderPaymentService;
import officemanager.gui.swt.services.OrderPrintService;
import officemanager.gui.swt.ui.wizards.pages.ClientSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.OrderDetailsPage;
import officemanager.gui.swt.ui.wizards.pages.PartSelectionPage;
import officemanager.gui.swt.ui.wizards.pages.PaymentsPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;
import officemanager.gui.swt.views.OrderView;

public class NewMerchandiseWizard extends OfficeManagerWizard {

	private final ClientDelegate clientDelegate;
	private final PaymentDelegate paymentDelegate;
	private final OrderDelegate orderDelegate;

	private final OrderPaymentService orderPaymentService;
	private final OrderPrintService orderPrintService;

	private ClientSelectionPage clientSelectionPage;
	private PartSelectionPage partSelectionPage;
	private OrderDetailsPage orderDetailsPage;
	private PaymentsPage paymentsPage;

	private Long orderId;
	private boolean canFinish;

	public NewMerchandiseWizard() {
		setWindowTitle("New Merchandise Order");
		clientDelegate = new ClientDelegate();
		paymentDelegate = new PaymentDelegate();
		orderDelegate = new OrderDelegate();

		orderPaymentService = new OrderPaymentService();
		orderPrintService = new OrderPrintService();
		addListeners();
	}

	@Override
	public void addPages() {
		clientSelectionPage = new ClientSelectionPage(1, false);
		addPage(clientSelectionPage);

		partSelectionPage = new PartSelectionPage(Integer.MAX_VALUE, false, false, true);
		addPage(partSelectionPage);

		orderDetailsPage = new OrderDetailsPage(false, false, true, false);
		orderDetailsPage.addIsShownListener(e -> updateProductSelectionPage());
		addPage(orderDetailsPage);

		paymentsPage = new PaymentsPage();
		paymentsPage.addIsShownListener(e -> updatePaymentsPage());
		orderPaymentService.setPage(paymentsPage);
		addPage(paymentsPage);

		orderPrintService.setShell(getShell());
	}

	@Override
	public boolean canFinish() {
		return getContainer().getCurrentPage() == paymentsPage && canFinish;
	}

	@Override
	public boolean performFinish() {
		if (!MessageDialogs.askQuestion(getShell(), "Confirmation",
				"Are you sure you wish to create this transaction?")) {
			return false;
		}

		Long clientId = clientSelectionPage.getSelectedClientId();

		OrderRecord orderRecord = new OrderRecord();
		orderRecord.setClientId(clientId);
		orderRecord.setOrderStatus(OrderStatus.CLOSED);
		orderRecord.setOrderType(OrderType.MERCHANDISE);

		List<OrderPartRecord> partRecords = orderDetailsPage.getParts();
		boolean isTaxExempt = false;
		if (clientId != null) {
			isTaxExempt = clientDelegate.isTaxExempt(clientId);
		}
		for (OrderPartRecord partRecord : orderDetailsPage.getParts()) {
			partRecord.setTaxExempt(isTaxExempt);
		}
		orderRecord.setParts(partRecords);

		orderId = orderDelegate.insertUpdateOrder(orderRecord);
		orderDelegate.closeOrder(orderId, AppPrefs.currentSession.prsnlId);

		List<PriceOverrideRecord> priceOverrides = orderDetailsPage.getPriceOverrides();
		for (PriceOverrideRecord record : priceOverrides) {
			record.setOrderId(orderId);
			orderDelegate.insertPriceOverride(record);
		}

		BigDecimal change = orderPaymentService.makeChange();

		List<PaymentRecord> payments = orderPaymentService.getPaymentsAdded();
		for (PaymentRecord record : payments) {
			paymentDelegate.makePaymentOnOrder(orderId, record);
		}

		if (change.compareTo(BigDecimal.ZERO) > 0) {
			MessageDialogs.displayMessage(getShell(), "Change Owed",
					"The client is owed " + AppPrefs.FORMATTER.formatMoney(change) + " in change.");
		}

		if (MessageDialogs.askQuestion(getShell(), "Print Reciept",
				"Would you like to print a reciept for this order?")) {
			orderPrintService.printReciept(orderId);
		}
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		if (orderId == null) {
			return null;
		}
		return new OrderView(orderId);
	}

	private void updateProductSelectionPage() {
		orderDetailsPage.populatePartSearches(partSelectionPage.getSelectedRecords());
	}

	private void updatePaymentsPage() {
		Long clientId = clientSelectionPage.getSelectedClientId();
		orderPaymentService.setClientId(clientId);

		boolean isTaxExempt = false;
		if (clientId != null) {
			isTaxExempt = clientDelegate.isTaxExempt(clientId);
		}

		List<OrderPartRecord> parts = orderDetailsPage.getParts();
		for (OrderPartRecord part : parts) {
			part.setTaxExempt(isTaxExempt);
		}
		BigDecimal amountOwed = Calculator.totalParts(parts);
		orderPaymentService.setAmountOwed(amountOwed);
		updateFinishButton();
	}

	private void addListeners() {
		orderPaymentService.addPaymentAddedListener(e -> updateFinishButton());
	}

	private void updateFinishButton() {
		canFinish = hasZeroBalance();
		getContainer().updateButtons();
	}

	private boolean hasZeroBalance() {
		BigDecimal balance = orderPaymentService.getBalance();
		return balance.compareTo(BigDecimal.ZERO) <= 0;
	}
}
