package officemanager.gui.swt.ui.dialogs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import officemanager.biz.delegates.LoginDelegate;
import officemanager.biz.records.UserSessionRecord;
import officemanager.biz.records.UserSessionRecord.AuthStatus;
import officemanager.gui.swt.ui.composites.LoginComposite;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.utility.StringUtility;

public class LoginDialog extends Dialog {

	private static final Logger logger = LogManager.getLogger(LoginDialog.class);

	private final LoginDelegate loginDelegate;

	private Shell shell;
	private String username;
	private String password;
	private boolean forceLogin;

	private boolean contentsCreated;
	private LoginComposite loginComposite;
	private UserSessionRecord session;

	/**
	 * Create the dialog.
	 *
	 * @param parent
	 * @param style
	 */
	public LoginDialog(Shell parent, int style) {
		super(parent, style);
		loginDelegate = new LoginDelegate();
	}

	/**
	 * Create the login dialog with a new shell and default style.
	 */
	public LoginDialog() {
		this(new Shell(), SWT.NONE);
	}

	/**
	 * Sets the username of the login dialog.
	 *
	 * @param username
	 *            The username to populate the prompt with.
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Sets the password of the login dialog.
	 *
	 * @param password
	 *            The password to populate the prompt with.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Sets the flag to force a login attempt directly after creation/population
	 * of the prompt.</br>
	 * </br>
	 * <b>Note</b> This should only be used in conjunction with
	 * {@link #setUsername(String)} and {@link #setPassword(String)}
	 *
	 * @param forceLogin
	 */
	public void setForceLogin(boolean forceLogin) {
		this.forceLogin = forceLogin;
	}

	/**
	 * Opens the login dialog.
	 *
	 * @return The resulting session from the login.
	 */
	public UserSessionRecord open() {
		if (contentsCreated) {
			throw new IllegalStateException("The dialog has already been opened. If another dialog is needed "
					+ "it will have to be created from a new instance.");
		}
		createContents();
		contentsCreated = true;

		Display display = getParent().getDisplay();
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		shell.setLocation(x, y);

		shell.open();
		shell.layout();
		if (forceLogin) {
			login();
		}
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return session;
	}

	private void login() {
		logger.debug("Starting login.");
		String username = loginComposite.txtUsername.getText();
		String password = loginComposite.txtPassword.getText();
		session = loginDelegate.login(username, password);
		logger.debug("Login status = " + session.toString());
		if (session.authStatus == AuthStatus.SUCCESSFUL) {
			shell.close();
		} else if (session.authStatus == AuthStatus.USER_LOCKED_OUT) {
			MessageDialogs.displayMessage(shell, "Unable to login.",
					"This user account has been locked. Please contact a system administrator.");
		} else if (session.authStatus == AuthStatus.INVALID_CREDS) {
			MessageDialogs.displayMessage(shell, "Unable to login.", "Invalid username/password. Please try again.");
		} else if (session.authStatus == AuthStatus.PASSWORD_NEEDS_RESET) {
			resetPassword();
		} else if (session.authStatus == AuthStatus.PASSWORD_NEEDS_SET) {
			setPassword();
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.BORDER | SWT.PRIMARY_MODAL | SWT.ON_TOP);
		shell.setSize(447, 293);
		shell.setText(getText());
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));

		loginComposite = new LoginComposite(shell, SWT.NONE);
		loginComposite.btnLogin.addListener(SWT.Selection, event -> login());
		loginComposite.btnCancel.addListener(SWT.Selection, event -> {
			session = getLogoutSession();
			shell.close();
		});

		if (!StringUtility.isNullOrWhiteSpace(username)) {
			loginComposite.txtUsername.setText(username);
		}
		if (!StringUtility.isNullOrWhiteSpace(password)) {
			loginComposite.txtPassword.setText(password);
		}

		KeyListener enterKeyListener = new KeyListener() {

			@Override
			public void keyReleased(KeyEvent arg0) {
				if (arg0.keyCode == SWT.CR) {
					login();
					arg0.doit = false;
				}
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
			}
		};

		loginComposite.txtUsername.addKeyListener(enterKeyListener);
		loginComposite.txtPassword.addKeyListener(enterKeyListener);
	}

	private UserSessionRecord getLogoutSession() {
		return new UserSessionRecord((String) null, AuthStatus.INVALID_CREDS);
	}

	private void setPassword() {
		if (MessageDialogs.askQuestion(shell, "Set Password",
				"Your password needs to be set prior to logging in. Set it now?")) {
			PasswordSetDialog setDialog = new PasswordSetDialog(shell, SWT.NONE);
			if (setDialog.open(session.prsnlId)) {
				logger.debug("Successfully set the password.");
				session = new UserSessionRecord(session, AuthStatus.SUCCESSFUL);
				shell.close();
			}
		}
	}

	private void resetPassword() {
		if (MessageDialogs.askQuestion(shell, "Reset Password", "Your password needs to be reset. Reset it now?")) {
			PasswordResetDialog resetDialog = new PasswordResetDialog(shell, SWT.NONE);
			if (resetDialog.open(session.prsnlId)) {
				logger.debug("Successfully reset the password.");
				session = new UserSessionRecord(session, AuthStatus.SUCCESSFUL);
				shell.close();
			}
		}
	}
}
