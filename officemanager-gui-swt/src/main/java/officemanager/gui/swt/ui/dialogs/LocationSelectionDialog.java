package officemanager.gui.swt.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import officemanager.biz.records.LocationRecord;
import officemanager.gui.swt.services.LocationTreeService;
import officemanager.gui.swt.ui.composites.LocationTreeComposite;

public class LocationSelectionDialog extends Dialog {

	private final LocationTreeService service;

	private LocationTreeComposite composite;
	private LocationRecord selection;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public LocationSelectionDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(SWT.PRIMARY_MODAL);
		service = new LocationTreeService();
	}

	public LocationRecord getSelection() {
		return selection;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));

		composite = new LocationTreeComposite(container, SWT.NONE);
		composite.toolBar.dispose();
		service.setComposite(composite);
		service.loadRootLocations();
		return container;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected void okPressed() {
		selection = service.getSelectedLocation();
		if (selection == null) {
			MessageDialog.openError(getShell(), "Unable To Select", "Please select a location.");
			return;
		}
		super.okPressed();
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(285, 301);
	}
}
