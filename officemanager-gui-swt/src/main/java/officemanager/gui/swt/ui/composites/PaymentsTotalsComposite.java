package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.PaymentTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PaymentsTotalsComposite extends Composite {

	public final CollapsibleComposite clpCmpPayments;
	public final Label lblCashPaymentsHeader;
	public final Label lblCashPayments;
	public final Label lblCheckPaymentsHeader;
	public final Label lblCheckPayments;
	public final Label lblClientCreditPaymentsHeader;
	public final Label lblClientCreditPayments;
	public final Label lblCreditCardPaymentsHeader;
	public final Label lblCreditCardPayments;
	public final Label lblFinancingPaymentsHeader;
	public final Label lblFinancingPayments;
	public final Label lblGiftCardPaymentsHeader;
	public final Label lblGiftCardPayments;
	public final Label lblPendingPaymentsHeader;
	public final Label lblPendingPayments;
	public final Label lblTradeInPaymentsHeader;
	public final Label lblTradeInPayments;
	public final PaymentTableComposite tblCmpPayments;
	public final Composite cmpPaymentTotals;
	public final Label lblTotalPaymentsHeader;
	public final Label lblTotalPayments;
	public final Label lblTotalPaymentQualification;
	public final Label lblRefundsHeader;
	public final Label lblRefunds;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PaymentsTotalsComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);

		clpCmpPayments = new CollapsibleComposite(this, SWT.NONE);
		clpCmpPayments.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		clpCmpPayments.setTitleText("Payments");
		clpCmpPayments.setTitleImage(ResourceManager.getImage(ImageFile.CASH, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpPayments.composite.setLayout(new GridLayout());

		cmpPaymentTotals = new Composite(clpCmpPayments.composite, SWT.NONE);
		cmpPaymentTotals.setLayout(new GridLayout(6, false));

		lblTotalPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblTotalPaymentsHeader.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblTotalPaymentsHeader.setText("Total Payments:");
		lblTotalPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblTotalPayments = new Label(cmpPaymentTotals, SWT.NONE);
		lblTotalPayments.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		new Label(cmpPaymentTotals, SWT.NONE);
		new Label(cmpPaymentTotals, SWT.NONE);

		lblTotalPaymentQualification = new Label(cmpPaymentTotals, SWT.NONE);
		lblTotalPaymentQualification.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 6, 1));
		lblTotalPaymentQualification
				.setText("*Total Payments does not include Client Credit, Gift Card or Pending payments.");
		lblTotalPaymentQualification.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.ITALIC));

		lblCashPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblCashPaymentsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCashPaymentsHeader.setText("Cash:");
		lblCashPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblCashPayments = new Label(cmpPaymentTotals, SWT.NONE);

		lblCheckPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblCheckPaymentsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCheckPaymentsHeader.setText("Check:");
		lblCheckPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblCheckPayments = new Label(cmpPaymentTotals, SWT.NONE);

		lblClientCreditPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblClientCreditPaymentsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblClientCreditPaymentsHeader.setText("Client Credit:");
		lblClientCreditPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblClientCreditPayments = new Label(cmpPaymentTotals, SWT.NONE);

		lblCreditCardPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblCreditCardPaymentsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCreditCardPaymentsHeader.setText("Credit Card:");
		lblCreditCardPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblCreditCardPayments = new Label(cmpPaymentTotals, SWT.NONE);

		lblFinancingPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblFinancingPaymentsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFinancingPaymentsHeader.setText("Financing:");
		lblFinancingPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblFinancingPayments = new Label(cmpPaymentTotals, SWT.NONE);

		lblGiftCardPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblGiftCardPaymentsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblGiftCardPaymentsHeader.setText("Gift Card:");
		lblGiftCardPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblGiftCardPayments = new Label(cmpPaymentTotals, SWT.NONE);

		lblPendingPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblPendingPaymentsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPendingPaymentsHeader.setText("Pending:");
		lblPendingPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblPendingPayments = new Label(cmpPaymentTotals, SWT.NONE);

		lblTradeInPaymentsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblTradeInPaymentsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTradeInPaymentsHeader.setText("Trade In:");
		lblTradeInPaymentsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblTradeInPayments = new Label(cmpPaymentTotals, SWT.NONE);

		lblRefundsHeader = new Label(cmpPaymentTotals, SWT.NONE);
		lblRefundsHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRefundsHeader.setText("Refunds:");
		lblRefundsHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblRefunds = new Label(cmpPaymentTotals, SWT.NONE);

		tblCmpPayments = new PaymentTableComposite(clpCmpPayments.composite, SWT.NONE, SWT.BORDER | SWT.FULL_SELECTION);
		tblCmpPayments.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
