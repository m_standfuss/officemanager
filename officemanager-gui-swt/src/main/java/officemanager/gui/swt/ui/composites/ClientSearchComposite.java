package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.ClientTableComposite;
import officemanager.gui.swt.ui.widgets.PhoneNumberText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ClientSearchComposite extends Composite {
	public final ToolBar toolBar;
	public final ToolItem tlItmNewClient;

	public final ScrolledComposite scrCompositeSideBar;
	public final Composite compositeSideBar;
	public final Button btnSearch;
	public final Button btnClear;

	public final Composite compositeCenter;
	public final ClientTableComposite searchResultsTable;
	public final Label lblNameLast;
	public final UpperCaseText txtNameLast;
	public final Label lblNameFirst;
	public final UpperCaseText txtNameFirst;
	public final Label lblPrimaryPhone;
	public final PhoneNumberText txtPhone;

	public ClientSearchComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		tlItmNewClient = new ToolItem(toolBar, SWT.NONE);
		tlItmNewClient.setImage(ResourceManager.getImage(ImageFile.USER_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmNewClient.setToolTipText("New Client");
		tlItmNewClient.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_CLIENT));

		scrCompositeSideBar = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeSideBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		compositeSideBar = new Composite(scrCompositeSideBar, SWT.NONE);
		compositeSideBar.setLayout(new GridLayout(2, false));

		lblNameLast = new Label(compositeSideBar, SWT.NONE);
		lblNameLast.setText("Name Last");
		lblNameLast.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		new Label(compositeSideBar, SWT.NONE);

		txtNameLast = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_txtNameLast = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtNameLast.widthHint = 150;
		txtNameLast.setLayoutData(gd_txtNameLast);

		lblNameFirst = new Label(compositeSideBar, SWT.NONE);
		lblNameFirst.setText("Name First");
		lblNameFirst.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		new Label(compositeSideBar, SWT.NONE);

		txtNameFirst = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_txtNameFirst = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtNameFirst.widthHint = 150;
		txtNameFirst.setLayoutData(gd_txtNameFirst);

		lblPrimaryPhone = new Label(compositeSideBar, SWT.NONE);
		lblPrimaryPhone.setText("Primary Phone");
		lblPrimaryPhone.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		new Label(compositeSideBar, SWT.NONE);

		txtPhone = new PhoneNumberText(compositeSideBar, SWT.BORDER);
		GridData gd_txtPhone = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtPhone.widthHint = 100;
		txtPhone.setLayoutData(gd_txtPhone);

		btnSearch = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnSearch = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnSearch.widthHint = 100;
		btnSearch.setLayoutData(gd_btnSearch);
		btnSearch.setText("Search");

		btnClear = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnClear = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnClear.widthHint = 100;
		btnClear.setLayoutData(gd_btnClear);
		btnClear.setText("Clear");

		scrCompositeSideBar.setContent(compositeSideBar);
		scrCompositeSideBar.setMinSize(compositeSideBar.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		compositeCenter = new Composite(this, SWT.BORDER);
		compositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeCenter.setLayout(new GridLayout(1, false));

		searchResultsTable = new ClientTableComposite(compositeCenter, SWT.NONE, tableStyle);
		searchResultsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ClientSearchComposite(Composite parent, int style) {
		this(parent, style, SWT.BORDER | SWT.FULL_SELECTION);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
