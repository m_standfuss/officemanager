package officemanager.gui.swt.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

public class HeaderValueLabel extends HeaderValue<String> {

	private Label lblEditable;

	public HeaderValueLabel(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public String getEditableValue() {
		return lblEditable.getText();
	}

	@Override
	protected void setEditableValue(String value) {
		lblEditable.setText(value);
	}

	@Override
	protected Control getEditableControl(Composite parent) {
		lblEditable = new Label(parent, SWT.NONE);
		return lblEditable;
	}
}
