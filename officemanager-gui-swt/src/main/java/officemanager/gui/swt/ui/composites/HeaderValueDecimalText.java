package officemanager.gui.swt.ui.composites;

import java.math.BigDecimal;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import officemanager.gui.swt.ui.widgets.DecimalText;
import officemanager.gui.swt.ui.widgets.HeaderValue;

public class HeaderValueDecimalText extends HeaderValue<BigDecimal> {

	private static final int WIDTH_DEFAULT = 65;

	private DecimalText txt;

	public HeaderValueDecimalText(Composite parent, int style, int numbersAfterDecimal) {
		super(parent, style);
		txt.setNumbersAfterDecimal(numbersAfterDecimal);
	}

	@Override
	public BigDecimal getEditableValue() {
		return txt.getValue();
	}

	@Override
	protected void setEditableValue(BigDecimal value) {
		txt.setValue(value);
	}

	@Override
	protected Control getEditableControl(Composite parent) {
		txt = new DecimalText(parent, SWT.BORDER);
		return txt;
	}

	@Override
	protected int getEditableWidth() {
		return WIDTH_DEFAULT;
	}
}
