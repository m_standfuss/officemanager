package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderTableComposite;
import officemanager.gui.swt.ui.widgets.NumberText;
import officemanager.gui.swt.ui.widgets.PhoneNumberText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class OrderSearchComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tltmNewOrderItem;
	public final Menu menuNewOrders;
	public final MenuItem mntmServiceOrder;
	public final MenuItem mntmMerchandiseOrder;
	public final MenuItem mntmBid;
	public final ToolItem tltmExportOrders;
	public final Menu menuExportOrders;
	public final MenuItem mntmPrintOrders;
	public final MenuItem mntmExportOrders;

	public final ScrolledComposite scrCompositeSideBar;
	public final Composite compositeSideBar;

	public final Label lblOrderNumber;
	public final NumberText txtOrderNumber;
	public final Label lblClientLastCompanyName;
	public final UpperCaseText txtClientLastName;
	public final Label lblClientFirstName;
	public final UpperCaseText txtClientFirstName;
	public final Label lblPrimaryPhone;
	public final PhoneNumberText txtPrimaryPhone;
	public final Button btnMyOrders;
	public final Button btnSearch;
	public final Button btnClear;

	public final Composite compositeCenter;
	public final Composite compositeDateRange;
	public final Label lblStartingDate;
	public final Label lblToDate;
	public final DateTime dateTimeStart;
	public final DateTime dateTimeEnd;

	public final OrderTableComposite searchResultsTable;

	public OrderSearchComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		menuNewOrders = new Menu(parent.getShell(), SWT.POP_UP);

		mntmServiceOrder = new MenuItem(menuNewOrders, SWT.NONE);
		mntmServiceOrder.setText("Service Order");
		mntmServiceOrder.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_SERVICE_ORDER));

		mntmMerchandiseOrder = new MenuItem(menuNewOrders, SWT.NONE);
		mntmMerchandiseOrder.setText("Merchandise Order");
		mntmMerchandiseOrder.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_MERCH_ORDER));

		mntmBid = new MenuItem(menuNewOrders, SWT.NONE);
		mntmBid.setText("Bid");
		mntmBid.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_BID));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		tltmNewOrderItem = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmNewOrderItem.setImage(ResourceManager.getImage(ImageFile.NEW, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tltmNewOrderItem.setToolTipText("New Order");
		tltmNewOrderItem.addListener(SWT.Selection, e -> {
			Rectangle rect = tltmNewOrderItem.getBounds();
			Point pt = new Point(rect.x, rect.y + rect.height);
			pt = toolBar.toDisplay(pt);
			menuNewOrders.setLocation(pt.x, pt.y);
			menuNewOrders.setVisible(true);
		});

		menuExportOrders = new Menu(parent.getShell(), SWT.POP_UP);

		mntmPrintOrders = new MenuItem(menuExportOrders, SWT.NONE);
		mntmPrintOrders.setText("Print");

		mntmExportOrders = new MenuItem(menuExportOrders, SWT.NONE);
		mntmExportOrders.setText("To CSV");

		tltmExportOrders = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmExportOrders.setImage(ResourceManager.getImage(ImageFile.ORDER_EXPORT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tltmExportOrders.setToolTipText("Export Orders");
		tltmExportOrders.addListener(SWT.Selection, e -> {
			Rectangle rect = tltmExportOrders.getBounds();
			Point pt = new Point(rect.x, rect.y + rect.height);
			pt = toolBar.toDisplay(pt);
			menuExportOrders.setLocation(pt.x, pt.y);
			menuExportOrders.setVisible(true);
		});

		scrCompositeSideBar = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeSideBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		compositeSideBar = new Composite(scrCompositeSideBar, SWT.NONE);
		compositeSideBar.setLayout(new GridLayout(2, false));

		lblOrderNumber = new Label(compositeSideBar, SWT.NONE);
		lblOrderNumber.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		lblOrderNumber.setText("Order Number");
		lblOrderNumber.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		txtOrderNumber = new NumberText(compositeSideBar, SWT.BORDER);
		GridData gd_text = new GridData(SWT.LEFT, SWT.FILL, true, false, 2, 1);
		gd_text.widthHint = 150;
		txtOrderNumber.setLayoutData(gd_text);

		lblClientLastCompanyName = new Label(compositeSideBar, SWT.NONE);
		lblClientLastCompanyName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblClientLastCompanyName.setText("Client Last/Company Name");
		lblClientLastCompanyName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		txtClientLastName = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_text_1 = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
		gd_text_1.widthHint = 200;
		txtClientLastName.setLayoutData(gd_text_1);

		lblClientFirstName = new Label(compositeSideBar, SWT.NONE);
		lblClientFirstName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblClientFirstName.setText("Client First Name");
		lblClientFirstName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		txtClientFirstName = new UpperCaseText(compositeSideBar, SWT.BORDER);
		GridData gd_text_2 = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
		gd_text_2.widthHint = 200;
		txtClientFirstName.setLayoutData(gd_text_2);

		lblPrimaryPhone = new Label(compositeSideBar, SWT.NONE);
		lblPrimaryPhone.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblPrimaryPhone.setText("Primary Phone");
		lblPrimaryPhone.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		txtPrimaryPhone = new PhoneNumberText(compositeSideBar, SWT.BORDER);
		GridData gd_text_3 = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
		gd_text_3.widthHint = 150;
		txtPrimaryPhone.setLayoutData(gd_text_3);

		btnMyOrders = new Button(compositeSideBar, SWT.CHECK);
		btnMyOrders.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnMyOrders.setText("My Orders");
		btnMyOrders.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		btnSearch = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnSearch = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnSearch.widthHint = 100;
		btnSearch.setLayoutData(gd_btnSearch);
		btnSearch.setText("Search");

		btnClear = new Button(compositeSideBar, SWT.NONE);
		GridData gd_btnClear = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnClear.widthHint = 100;
		btnClear.setLayoutData(gd_btnClear);
		btnClear.setText("Clear");
		btnClear.addListener(SWT.Selection, e -> clear());

		scrCompositeSideBar.setContent(compositeSideBar);
		scrCompositeSideBar.setMinSize(compositeSideBar.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		compositeCenter = new Composite(this, SWT.BORDER);
		compositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeCenter.setLayout(new GridLayout(1, false));

		compositeDateRange = new Composite(compositeCenter, SWT.NONE);
		compositeDateRange.setLayout(new GridLayout(4, false));
		compositeDateRange.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		compositeDateRange.setSize(64, 64);

		lblStartingDate = new Label(compositeDateRange, SWT.NONE);
		lblStartingDate.setText("Opened on");
		lblStartingDate.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		dateTimeStart = new DateTime(compositeDateRange, SWT.BORDER | SWT.DROP_DOWN);

		lblToDate = new Label(compositeDateRange, SWT.NONE);
		lblToDate.setText("to");
		lblToDate.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		dateTimeEnd = new DateTime(compositeDateRange, SWT.BORDER | SWT.DROP_DOWN);

		searchResultsTable = new OrderTableComposite(compositeCenter, SWT.NONE, SWT.BORDER | SWT.FULL_SELECTION);
		searchResultsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	private void clear() {
		txtOrderNumber.setText("");
		txtClientLastName.setText("");
		txtClientFirstName.setText("");
		txtPrimaryPhone.setText("");
		btnMyOrders.setSelection(false);
	}
}
