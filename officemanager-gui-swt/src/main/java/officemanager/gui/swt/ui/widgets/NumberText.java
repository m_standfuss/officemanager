package officemanager.gui.swt.ui.widgets;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class NumberText extends Text {

	private int maxLength;

	public NumberText(Composite parent, int style) {
		this(Text.LIMIT, parent, style);
	}

	public NumberText(int maxLength, Composite parent, int style) {
		super(parent, style);
		this.maxLength = maxLength;
		addVerifyListener(e -> verifyEvent(e));
	}

	@Override
	public void setText(String string) {
		if (string.isEmpty()) {
			super.setText(string);
		} else {
			try {
				Long.parseLong(string);
				super.setText(string);
			} catch (NumberFormatException e) {}
		}
	}

	/**
	 * Returns the {@link Long} value of the text box. If the value cannot be
	 * parsed into a Long value then null will be returned.
	 * 
	 * @return The number value of the text box.
	 */
	public Long getNumber() {
		try {
			return Long.parseLong(getText());
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public int getNumberInt() {
		try {
			return Integer.parseInt(getText());
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * Sets the number value of the text.
	 * 
	 * @param l
	 *            The number to set to.
	 */
	public void setNumber(long l) {
		checkNotNull(l);
		setText(String.valueOf(l));
	}

	public void setMaxLength(int maxLength) {
		checkArgument(maxLength <= Text.LIMIT);
		this.maxLength = maxLength;
	}

	private void verifyEvent(VerifyEvent e) {
		e.text = e.text.trim();
		if (!e.text.isEmpty()) {
			try {
				Long.parseLong(e.text);
			} catch (final NumberFormatException excep) {
				e.doit = false;
				return;
			}
			e.doit = e.text.length() <= maxLength;
		}
	}

	@Override
	protected void checkSubclass() {}
}
