package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.CodeValueTable;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.ui.widgets.DecimalText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PreferencesComposite extends Composite {

	public CollapsibleComposite clpCmpCompanyInfo;
	public Label lblAddress;
	public Label lblPhone;
	public Label lblWebsite;
	public Text txtCompanyAddress;
	public Text txtCompanyPhone;
	public Text txtCompanyWebsite;

	public CollapsibleComposite clpCmpRateInfo;
	public Label lblHourlyRate;
	public Label lblDiscountedRate;
	public Label lblTaxRate;
	public DecimalText txtHourlyRate;
	public DecimalText txtDiscountedRate;
	public DecimalText txtTaxRate;

	public CollapsibleComposite clpCmpPrinting;
	public Label lblShopTicketComments;
	public CodeValueTable listShopTicketTypes;
	public Button btnEditShopTicketComments;
	public Label lblClaimTicketComments;
	public CodeValueTable listClaimTicketTypes;
	public Button btnEditClaimTicketComments;
	public Label lblRecieptComments;
	public CodeValueTable listRecieptCommentTypes;
	public Button btnEditRecieptComments;
	public Label lblEnterTheTax;

	public CollapsibleComposite clpCmpCodeValues;
	public Label lblTest;
	public Button button;
	public Label lblAsdfasdfasdfadf;
	public Button button_1;
	public ScrolledComposite scrolledComposite;
	public Composite composite;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PreferencesComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		setLayout(new GridLayout());

		scrolledComposite = new ScrolledComposite(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		composite = new Composite(scrolledComposite, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));

		clpCmpCompanyInfo = new CollapsibleComposite(composite, SWT.NONE);
		clpCmpCompanyInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		clpCmpCompanyInfo.setTitleText("Company Information");
		clpCmpCompanyInfo.setTitleImage(ResourceManager.getImage(ImageFile.EDIT, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpCompanyInfo.composite.setLayout(new GridLayout(2, false));

		lblAddress = new Label(clpCmpCompanyInfo.composite, SWT.NONE);
		lblAddress.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblAddress.setText("Address:");

		txtCompanyAddress = new Text(clpCmpCompanyInfo.composite, SWT.BORDER | SWT.MULTI);
		GridData gd_txtCompanyAddress = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtCompanyAddress.heightHint = 100;
		gd_txtCompanyAddress.widthHint = 200;
		txtCompanyAddress.setLayoutData(gd_txtCompanyAddress);

		lblPhone = new Label(clpCmpCompanyInfo.composite, SWT.NONE);
		lblPhone.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPhone.setText("Phone:");

		txtCompanyPhone = new Text(clpCmpCompanyInfo.composite, SWT.BORDER);
		GridData gd_txtCompanyPhone = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtCompanyPhone.widthHint = 150;
		txtCompanyPhone.setLayoutData(gd_txtCompanyPhone);

		lblWebsite = new Label(clpCmpCompanyInfo.composite, SWT.NONE);
		lblWebsite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblWebsite.setText("Website:");

		txtCompanyWebsite = new Text(clpCmpCompanyInfo.composite, SWT.BORDER);
		GridData gd_txtCompanyWebsite = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtCompanyWebsite.widthHint = 150;
		txtCompanyWebsite.setLayoutData(gd_txtCompanyWebsite);

		clpCmpRateInfo = new CollapsibleComposite(composite, SWT.NONE);
		clpCmpRateInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		clpCmpRateInfo.setTitleText("Rate Information");
		clpCmpRateInfo.setTitleImage(ResourceManager.getImage(ImageFile.CASH, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpRateInfo.composite.setLayout(new GridLayout(2, false));

		lblHourlyRate = new Label(clpCmpRateInfo.composite, SWT.NONE);
		lblHourlyRate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblHourlyRate.setText("Hourly Rate:");

		txtHourlyRate = new DecimalText(2, clpCmpRateInfo.composite, SWT.BORDER);
		GridData gd_txtHourlyRate = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtHourlyRate.widthHint = 75;
		txtHourlyRate.setLayoutData(gd_txtHourlyRate);

		lblDiscountedRate = new Label(clpCmpRateInfo.composite, SWT.NONE);
		lblDiscountedRate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDiscountedRate.setText("Discounted Rate:");

		txtDiscountedRate = new DecimalText(2, clpCmpRateInfo.composite, SWT.BORDER);
		GridData gd_txtDiscountedRate = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtDiscountedRate.widthHint = 75;
		txtDiscountedRate.setLayoutData(gd_txtDiscountedRate);

		lblTaxRate = new Label(clpCmpRateInfo.composite, SWT.NONE);
		lblTaxRate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTaxRate.setText("Tax Rate:");

		txtTaxRate = new DecimalText(4, clpCmpRateInfo.composite, SWT.BORDER);
		GridData gd_txtTaxRate = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtTaxRate.widthHint = 75;
		txtTaxRate.setLayoutData(gd_txtTaxRate);

		lblEnterTheTax = new Label(clpCmpRateInfo.composite, SWT.NONE);
		lblEnterTheTax.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblEnterTheTax.setText("Enter the tax rate as a decimal (example .0975)");
		lblEnterTheTax.setFont(ResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));

		clpCmpPrinting = new CollapsibleComposite(composite, SWT.NONE);
		clpCmpPrinting.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		clpCmpPrinting.setTitleText("Printing");
		clpCmpPrinting.setTitleImage(ResourceManager.getImage(ImageFile.PRINT, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpPrinting.composite.setLayout(new GridLayout(2, false));

		lblShopTicketComments = new Label(clpCmpPrinting.composite, SWT.NONE);
		lblShopTicketComments.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblShopTicketComments.setText("Shop Ticket Comments:");

		listShopTicketTypes = new CodeValueTable(clpCmpPrinting.composite, SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_codeValueTable = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 2);
		gd_codeValueTable.heightHint = 100;
		gd_codeValueTable.widthHint = 175;
		listShopTicketTypes.setLayoutData(gd_codeValueTable);
		listShopTicketTypes.setToolTipText("The comment types to print out on the shop ticket for an order.");

		btnEditShopTicketComments = new Button(clpCmpPrinting.composite, SWT.NONE);
		btnEditShopTicketComments.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnEditShopTicketComments.setText("Edit");

		lblShopTicketComments = new Label(clpCmpPrinting.composite, SWT.NONE);
		lblShopTicketComments.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblShopTicketComments.setText("Claim Ticket Comments:");

		listClaimTicketTypes = new CodeValueTable(clpCmpPrinting.composite, SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_cvTblClaimTicket = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 2);
		gd_cvTblClaimTicket.heightHint = 100;
		gd_cvTblClaimTicket.widthHint = 175;
		listClaimTicketTypes.setLayoutData(gd_cvTblClaimTicket);
		listClaimTicketTypes.setToolTipText("The comment types to print out on the claim ticket for an order.");

		btnEditClaimTicketComments = new Button(clpCmpPrinting.composite, SWT.NONE);
		btnEditClaimTicketComments.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnEditClaimTicketComments.setText("Edit");

		lblRecieptComments = new Label(clpCmpPrinting.composite, SWT.NONE);
		lblRecieptComments.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblRecieptComments.setText("Reciept Comments:");

		listRecieptCommentTypes = new CodeValueTable(clpCmpPrinting.composite, SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_cvTblReciept = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 2);
		gd_cvTblReciept.heightHint = 100;
		gd_cvTblReciept.widthHint = 175;
		listRecieptCommentTypes.setLayoutData(gd_cvTblReciept);
		listRecieptCommentTypes.setToolTipText("The comment types to print out on the reciept for an order.");

		btnEditRecieptComments = new Button(clpCmpPrinting.composite, SWT.NONE);
		btnEditRecieptComments.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnEditRecieptComments.setText("Edit");

		clpCmpCodeValues = new CollapsibleComposite(composite, SWT.NONE);
		clpCmpCodeValues.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		clpCmpCodeValues.setTitleText("Coded Values");
		clpCmpCodeValues.setTitleImage(ResourceManager.getImage(ImageFile.CODE, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpCodeValues.composite.setLayout(new GridLayout(4, false));

		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	public Button addCodeSet(String codeSetName) {
		Label label = new Label(clpCmpCodeValues.composite, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label.setText(codeSetName);

		Button button = new Button(clpCmpCodeValues.composite, SWT.NONE);
		button.setText("Modify");
		return button;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
