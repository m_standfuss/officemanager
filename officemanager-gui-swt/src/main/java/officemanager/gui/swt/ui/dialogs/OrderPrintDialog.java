package officemanager.gui.swt.ui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import officemanager.gui.swt.services.OrderPrintService;
import officemanager.gui.swt.util.MessageDialogs;

public class OrderPrintDialog extends Dialog {

	private final Long orderId;
	private final OrderPrintService orderPrintService;

	private Shell shlOrderPrint;
	private Label lblPrompt;
	private Group grpCheckBoxes;
	private Button btnClaimTickets;
	private Button btnShopTickets;
	private Button btnPrint;
	private Button btnCancel;
	private Composite compBtns;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 */
	public OrderPrintDialog(Shell parent, Long orderId) {
		super(parent, SWT.NONE);
		orderPrintService = new OrderPrintService();
		this.orderId = orderId;
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public void open() {
		createContents();
		orderPrintService.setShell(shlOrderPrint);

		Monitor primary = Display.getCurrent().getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shlOrderPrint.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shlOrderPrint.setLocation(x, y);

		shlOrderPrint.open();
		shlOrderPrint.layout();
		Display display = getParent().getDisplay();
		while (!shlOrderPrint.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlOrderPrint = new Shell(getParent(), SWT.TITLE | SWT.PRIMARY_MODAL);
		shlOrderPrint.setSize(300, 170);
		shlOrderPrint.setText("Order Print");
		GridLayout gl_shlOrderPrint = new GridLayout();
		shlOrderPrint.setLayout(gl_shlOrderPrint);

		lblPrompt = new Label(shlOrderPrint, SWT.NONE);
		lblPrompt.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));
		lblPrompt.setText("Would you like to print these items for the order?");

		grpCheckBoxes = new Group(shlOrderPrint, SWT.NONE);
		grpCheckBoxes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpCheckBoxes.setLayout(new GridLayout(1, false));

		btnShopTickets = new Button(grpCheckBoxes, SWT.CHECK);
		btnShopTickets.setSelection(true);
		btnShopTickets.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		btnShopTickets.setText("Shop Ticket(s)");

		btnClaimTickets = new Button(grpCheckBoxes, SWT.CHECK);
		btnClaimTickets.setSelection(true);
		btnClaimTickets.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		btnClaimTickets.setText("Claim Ticket(s)");

		compBtns = new Composite(shlOrderPrint, SWT.RIGHT_TO_LEFT);
		compBtns.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, false, 1, 1));
		compBtns.setLayout(new GridLayout(2, false));

		btnPrint = new Button(compBtns, SWT.NONE);
		btnPrint.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnPrint.setText("Print");
		btnPrint.addListener(SWT.Selection, e -> print());

		btnCancel = new Button(compBtns, SWT.NONE);
		btnCancel.setText("Cancel");
		btnCancel.addListener(SWT.Selection, e -> shlOrderPrint.close());
	}

	private void print() {
		if (btnShopTickets.getSelection() && btnClaimTickets.getSelection()) {
			orderPrintService.printClaimAndShopTicket(orderId);
			shlOrderPrint.close();
		} else if (btnShopTickets.getSelection()) {
			orderPrintService.printShopTicket(orderId);
			shlOrderPrint.close();
		} else if (btnClaimTickets.getSelection()) {
			orderPrintService.printClaimTicket(orderId);
			shlOrderPrint.close();
		} else {
			MessageDialogs.displayError(shlOrderPrint, "Unable To Print",
					"Please select at least one document to print.");
		}
	}
}
