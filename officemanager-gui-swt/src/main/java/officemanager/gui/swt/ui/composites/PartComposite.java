package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.SaleTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PartComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmEditPart;
	public final ToolItem tlItmAddSale;
	public final ToolItem tlItmDeletePart;

	public final CollapsibleComposite partInfoComposite;
	public final CollapsibleComposite saleInfoComposite;

	public final Label lblPartNameHeader;
	public final Label lblPartName;
	public final Label lblPartDescriptionHeader;
	public final Text txtPartDescription;
	public final Label lblBasePriceHeader;
	public final Label lblBasePrice;
	public final Label lblLocationHeader;
	public final Label lblLocation;
	public final Label lblCurrentInventoryHeader;
	public final Label lblCurrentInventory;
	public final Label lblInventoryThresholdHeader;
	public final Label lblInventoryThreshold;
	public final Label lblAmountSoldHeader;
	public final Label lblAmountSold;
	public final Label lblBarcodeHeader;
	public final Label lblBarcode;
	public final Label lblLastUpdatedHeader;
	public final Label lblLastUpdated;
	public final Label lblLastUpdatedByHeader;
	public final Label lblLastUpdatedBy;
	public final Label lblManufacturerHeader;
	public final Label lblManufacturer;
	public final Label lblManufIdHeader;
	public final Label lblManufId;
	public final Label lblCategoryHeader;
	public final Label lblCategory;
	public final Label lblProductTypeHeader;
	public final Label lblProductType;
	public final SaleTableComposite saleTableComposite;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PartComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		tlItmEditPart = new ToolItem(toolBar, SWT.NONE);
		tlItmEditPart.setImage(ResourceManager.getImage(ImageFile.PART_EDIT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmEditPart.setToolTipText("Edit Part");
		tlItmEditPart.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_PART_DETAILS));

		tlItmAddSale = new ToolItem(toolBar, SWT.NONE);
		tlItmAddSale.setImage(ResourceManager.getImage(ImageFile.SALE_ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmAddSale.setToolTipText("Add Sale");
		tlItmAddSale.setEnabled(AppPrefs.hasPermission(AppPermissions.ADD_SALE));

		tlItmDeletePart = new ToolItem(toolBar, SWT.NONE);
		tlItmDeletePart.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmDeletePart.setToolTipText("Delete Part");
		tlItmDeletePart.setEnabled(AppPrefs.hasPermission(AppPermissions.REMOVE_PART));

		ScrolledComposite scrolledComposite = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Composite composite = new Composite(scrolledComposite, style);
		composite.setLayout(new GridLayout());

		partInfoComposite = new CollapsibleComposite(composite, SWT.NONE);
		partInfoComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		partInfoComposite.setTitleText("Part Details");
		GridLayout gl_partInfoComposite = new GridLayout();
		gl_partInfoComposite.verticalSpacing = 15;
		gl_partInfoComposite.horizontalSpacing = 20;
		gl_partInfoComposite.numColumns = 4;
		partInfoComposite.composite.setLayout(gl_partInfoComposite);

		lblPartNameHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblPartNameHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPartNameHeader.setText("Part Name:");
		lblPartNameHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblPartName = new Label(partInfoComposite.composite, SWT.NONE);

		lblBasePriceHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblBasePriceHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblBasePriceHeader.setText("Base Price:");
		lblBasePriceHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblBasePrice = new Label(partInfoComposite.composite, SWT.NONE);

		lblManufacturerHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblManufacturerHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblManufacturerHeader.setText("Manufacturer:");
		lblManufacturerHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblManufacturer = new Label(partInfoComposite.composite, SWT.NONE);

		lblCategoryHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblCategoryHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCategoryHeader.setText("Category:");
		lblCategoryHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblCategory = new Label(partInfoComposite.composite, SWT.NONE);

		lblManufIdHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblManufIdHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblManufIdHeader.setText("Manuf. ID:");
		lblManufIdHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblManufId = new Label(partInfoComposite.composite, SWT.NONE);

		lblProductTypeHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblProductTypeHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblProductTypeHeader.setText("Product Type:");
		lblProductTypeHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblProductType = new Label(partInfoComposite.composite, SWT.NONE);

		lblPartDescriptionHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblPartDescriptionHeader.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblPartDescriptionHeader.setText("Part Description");
		lblPartDescriptionHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblLocationHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblLocationHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLocationHeader.setText("Location:");
		lblLocationHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblLocation = new Label(partInfoComposite.composite, SWT.NONE);

		txtPartDescription = new Text(partInfoComposite.composite, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtPartDescription = new GridData(SWT.LEFT, SWT.FILL, false, false, 2, 6);
		gd_txtPartDescription.heightHint = 100;
		gd_txtPartDescription.widthHint = 200;
		txtPartDescription.setLayoutData(gd_txtPartDescription);
		txtPartDescription.setEditable(false);

		lblCurrentInventoryHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblCurrentInventoryHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCurrentInventoryHeader.setText("Current Inventory:");
		lblCurrentInventoryHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblCurrentInventory = new Label(partInfoComposite.composite, SWT.NONE);

		lblInventoryThresholdHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblInventoryThresholdHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblInventoryThresholdHeader.setText("Inventory Threshold:");
		lblInventoryThresholdHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblInventoryThreshold = new Label(partInfoComposite.composite, SWT.NONE);

		lblAmountSoldHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblAmountSoldHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAmountSoldHeader.setText("Amount Sold:");
		lblAmountSoldHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblAmountSold = new Label(partInfoComposite.composite, SWT.NONE);

		lblBarcodeHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblBarcodeHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblBarcodeHeader.setText("Barcode:");
		lblBarcodeHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblBarcode = new Label(partInfoComposite.composite, SWT.NONE);

		lblLastUpdatedHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblLastUpdatedHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLastUpdatedHeader.setText("Last Updated:");
		lblLastUpdatedHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblLastUpdated = new Label(partInfoComposite.composite, SWT.NONE);

		lblLastUpdatedByHeader = new Label(partInfoComposite.composite, SWT.NONE);
		lblLastUpdatedByHeader.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLastUpdatedByHeader.setText("Last Updated By:");
		lblLastUpdatedByHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblLastUpdatedBy = new Label(partInfoComposite.composite, SWT.NONE);

		saleInfoComposite = new CollapsibleComposite(composite, SWT.BORDER);
		saleInfoComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		saleInfoComposite.composite.setLayout(new FillLayout());
		saleInfoComposite.setTitleText("Active Part Sales");

		saleTableComposite = new SaleTableComposite(saleInfoComposite.composite, SWT.NONE, SWT.FULL_SELECTION);

		composite.layout();
		composite.addListener(SWT.Resize,
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		partInfoComposite.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		saleInfoComposite.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
