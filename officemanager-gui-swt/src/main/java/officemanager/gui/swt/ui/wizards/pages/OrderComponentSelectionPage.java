package officemanager.gui.swt.ui.wizards.pages;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.OrderComponentSelectionComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class OrderComponentSelectionPage extends OfficeManagerWizardPage {

	public OrderComponentSelectionComposite composite;

	/**
	 * Create the wizard.
	 */
	public OrderComponentSelectionPage() {
		this(false);
	}

	public OrderComponentSelectionPage(boolean requireSelection) {
		super("OrderComponentSelectionPage", "Order Components", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.ORDER_EDIT, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("Please select the order components.");
		setPageComplete(!requireSelection);
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandVertical(true);
		scrCompositeSideBar.setExpandHorizontal(true);
		composite = new OrderComponentSelectionComposite(scrCompositeSideBar, SWT.NONE);

		scrCompositeSideBar.setContent(composite);
		scrCompositeSideBar.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrCompositeSideBar);

		composite.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));
		composite.tblFlatRates.table.addCheckListener(e -> checkPageComplete());
		composite.tblParts.table.addCheckListener(e -> checkPageComplete());
		composite.tblServices.table.addCheckListener(e -> checkPageComplete());
	}

	private void checkPageComplete() {
		int checkCnt = 0;
		checkCnt += composite.tblFlatRates.table.getCheckedCount();
		checkCnt += composite.tblParts.table.getCheckedCount();
		checkCnt += composite.tblServices.table.getCheckedCount();
		setPageComplete(checkCnt != 0);
	}

}
