package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;

import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class AddressTableComposite extends Composite {

	public static final int COL_PRIMARY_IND = 0;
	public static final int COL_ADDRESS = 1;
	public static final int COL_CITY = 2;
	public static final int COL_STATE = 3;
	public static final int COL_ZIP = 4;
	public static final int COL_ADDRESS_TYPE = 5;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnPrimary;
	public final TableColumn tblclmnAddress;
	public final TableColumn tblclmnCity;
	public final TableColumn tblclmnState;
	public final TableColumn tblclmnZip;
	public final TableColumn tblclmnAddressType;
	public final Label lblPrimaryAddressImage;
	public final Label lblPrimaryAddressText;

	public AddressTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		table = new OfficeManagerTable(this, tableStyle);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnPrimary = new TableColumn(table, SWT.NONE);
		tblclmnPrimary.setWidth(100);
		tblclmnPrimary.setText("");

		tblclmnAddress = new TableColumn(table, SWT.NONE);
		tblclmnAddress.setWidth(100);
		tblclmnAddress.setText("Address");

		tblclmnCity = new TableColumn(table, SWT.NONE);
		tblclmnCity.setWidth(100);
		tblclmnCity.setText("City");

		tblclmnState = new TableColumn(table, SWT.NONE);
		tblclmnState.setWidth(100);
		tblclmnState.setText("State");

		tblclmnZip = new TableColumn(table, SWT.NONE);
		tblclmnZip.setWidth(100);
		tblclmnZip.setText("Zip");

		tblclmnAddressType = new TableColumn(table, SWT.NONE);
		tblclmnAddressType.setWidth(100);
		tblclmnAddressType.setText("Address Type");

		lblPrimaryAddressImage = new Label(this, SWT.NONE);
		lblPrimaryAddressImage.setImage(ResourceManager.getImage(ImageFile.CHECK, 16));

		lblPrimaryAddressText = new Label(this, SWT.NONE);
		lblPrimaryAddressText.setText("Denotes the primary address");
	}
}
