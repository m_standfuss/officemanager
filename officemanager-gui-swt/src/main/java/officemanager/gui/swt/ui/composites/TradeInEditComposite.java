package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class TradeInEditComposite extends Composite {
	public final Label lblTradeInManufacturer;
	public final Label lblTradeInModel;
	public final Label lblTradeInType;
	public final Label lblTradeInProductType;
	public final UpperCaseText txtTradeInManuf;
	public final UpperCaseText txtTradeInModel;
	public final CodeValueCombo cmbTradeInType;
	public final CodeValueCombo cmbTradeInProductType;
	public final Label lblTradeInDescription;
	public final UpperCaseText txtTradeInDescription;

	public TradeInEditComposite(Composite parent, int style) {
		super(parent, style);

		GridLayout gl_composite = new GridLayout(4, false);
		gl_composite.horizontalSpacing = 10;
		setLayout(gl_composite);

		lblTradeInManufacturer = new Label(this, SWT.NONE);
		lblTradeInManufacturer.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTradeInManufacturer.setText("Manufacturer:");
		lblTradeInManufacturer.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtTradeInManuf = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtTradeInManuf = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtTradeInManuf.widthHint = 150;
		txtTradeInManuf.setLayoutData(gd_txtTradeInManuf);

		lblTradeInDescription = new Label(this, SWT.NONE);
		lblTradeInDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 4));
		lblTradeInDescription.setText("Description:");
		lblTradeInDescription.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtTradeInDescription = new UpperCaseText(this, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtTradeInDescription = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 4);
		gd_txtTradeInDescription.widthHint = 150;
		gd_txtTradeInDescription.heightHint = 100;
		txtTradeInDescription.setLayoutData(gd_txtTradeInDescription);

		lblTradeInModel = new Label(this, SWT.NONE);
		lblTradeInModel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTradeInModel.setText("Model:");
		lblTradeInModel.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtTradeInModel = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtTradeInModel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtTradeInModel.widthHint = 150;
		txtTradeInModel.setLayoutData(gd_txtTradeInModel);

		lblTradeInType = new Label(this, SWT.NONE);
		lblTradeInType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTradeInType.setText("Trade In Type:");
		lblTradeInType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbTradeInType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbTradeInType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbTradeInType.widthHint = 125;
		cmbTradeInType.setLayoutData(gd_cmbTradeInType);

		lblTradeInProductType = new Label(this, SWT.NONE);
		lblTradeInProductType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTradeInProductType.setText("Product Type:");
		lblTradeInProductType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbTradeInProductType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbTradeInProductType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbTradeInProductType.widthHint = 125;
		cmbTradeInProductType.setLayoutData(gd_cmbTradeInProductType);
	}

}
