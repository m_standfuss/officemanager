package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.AddressTableComposite;
import officemanager.gui.swt.ui.tables.PhoneTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmEditInformation;
	public final ToolItem tlItmEditPermissions;

	public final CollapsibleComposite personnelInfoCllpCmp;
	public final PersonnelInformationComposite personnelInformationComposite;
	public final CollapsibleComposite phoneCllpCmp;
	public final PhoneTableComposite phoneTblCmp;
	public final CollapsibleComposite addressCllpCmp;
	public final AddressTableComposite addressTblCmp;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PersonnelComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		tlItmEditInformation = new ToolItem(toolBar, SWT.NONE);
		tlItmEditInformation.setImage(ResourceManager.getImage(ImageFile.USER_EDIT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmEditInformation.setToolTipText("Edit Personnel");
		tlItmEditInformation.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_PERSONNEL));

		tlItmEditPermissions = new ToolItem(toolBar, SWT.NONE);
		tlItmEditPermissions.setImage(ResourceManager.getImage(ImageFile.SECURITY, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmEditPermissions.setToolTipText("Edit Permissions");
		tlItmEditPermissions.setEnabled(AppPrefs.hasPermission(AppPermissions.SECURITY_USER));

		ScrolledComposite scrolledComposite = new ScrolledComposite(this, SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Composite composite = new Composite(scrolledComposite, style);
		composite.setLayout(new GridLayout());

		personnelInfoCllpCmp = new CollapsibleComposite(composite, SWT.BORDER);
		personnelInfoCllpCmp.setTitleText("Personnel Information");
		personnelInfoCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		personnelInfoCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.USER_INFO, AppPrefs.ICN_SIZE_CLPCMP));
		personnelInfoCllpCmp.composite.setLayout(new GridLayout(2, false));

		personnelInformationComposite = new PersonnelInformationComposite(personnelInfoCllpCmp.composite, SWT.NONE);
		personnelInformationComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

		phoneCllpCmp = new CollapsibleComposite(composite, SWT.BORDER);
		phoneCllpCmp.setTitleText("Phones");
		phoneCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		phoneCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.PHONE, AppPrefs.ICN_SIZE_CLPCMP));
		phoneTblCmp = new PhoneTableComposite(phoneCllpCmp.composite, SWT.NONE, SWT.FULL_SELECTION);

		addressCllpCmp = new CollapsibleComposite(composite, SWT.BORDER);
		addressCllpCmp.setTitleText("Addresses");
		addressCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		addressCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.ADDRESS, AppPrefs.ICN_SIZE_CLPCMP));
		addressTblCmp = new AddressTableComposite(addressCllpCmp.composite, SWT.NONE, SWT.FULL_SELECTION);

		composite.layout();
		composite.addListener(SWT.Resize,
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		personnelInfoCllpCmp.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		phoneCllpCmp.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		addressCllpCmp.addCollapseListener(
				e -> scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
