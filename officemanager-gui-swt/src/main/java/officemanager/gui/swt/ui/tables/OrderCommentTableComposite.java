package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class OrderCommentTableComposite extends Composite {

	public static final int COL_COMMENT_TYPE = 0;
	public static final int COL_AUTHOR = 1;
	public static final int COL_DATE = 2;
	public static final int COL_COMMENT = 3;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnCommentType;
	public final TableColumn tblcmlnDate;
	public final TableColumn tblcmlnAuthor;
	public final TableColumn tblcmlnComment;

	public OrderCommentTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new FillLayout());
		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnCommentType = new TableColumn(table, SWT.NONE);
		tblclmnCommentType.setWidth(100);
		tblclmnCommentType.setText("Type");

		tblcmlnAuthor = new TableColumn(table, SWT.NONE);
		tblcmlnAuthor.setWidth(100);
		tblcmlnAuthor.setText("Author");

		tblcmlnDate = new TableColumn(table, SWT.NONE);
		tblcmlnDate.setWidth(100);
		tblcmlnDate.setText("Date");

		tblcmlnComment = new TableColumn(table, SWT.NONE);
		tblcmlnComment.setWidth(100);
		tblcmlnComment.setText("Comment");
	}

}
