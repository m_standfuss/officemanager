package officemanager.gui.swt.ui.wizards.pages;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Monitor;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.ButtonBarComposite;
import officemanager.gui.swt.ui.composites.OrderPaymentTotalsComposite;
import officemanager.gui.swt.ui.composites.PaymentEditComposite;
import officemanager.gui.swt.ui.composites.TradeInEditComposite;
import officemanager.gui.swt.ui.tables.PaymentTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PaymentsPage extends OfficeManagerWizardPage {
	public OrderPaymentTotalsComposite orderPaymentTotalsComposite;

	public ScrolledComposite scrolledComp;
	public Composite scrolledCompComp;

	public CollapsibleComposite newPaymentCllpCmp;
	public PaymentEditComposite paymentEditComposite;

	public CollapsibleComposite tradeInInfoCllpCmp;
	public TradeInEditComposite tradeInComposite;

	public ButtonBarComposite buttonBarComposite;

	public CollapsibleComposite currentPaymentsCllpCmp;
	public PaymentTableComposite paymentTableComposite;

	public PaymentsPage() {
		super("PaymentsPage", "Payments", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.CASH, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setPageComplete(false);
		setDescription("Please enter a payment.");
	}

	/**
	 * Create contents of the wizard.
	 * 
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout gl_container = new GridLayout();
		container.setLayout(gl_container);
		setControl(container);

		orderPaymentTotalsComposite = new OrderPaymentTotalsComposite(container, SWT.NONE);
		orderPaymentTotalsComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		scrolledComp = new ScrolledComposite(container, SWT.BORDER | SWT.V_SCROLL);
		scrolledComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		scrolledComp.setExpandHorizontal(true);
		scrolledComp.setExpandVertical(true);

		scrolledCompComp = new Composite(scrolledComp, SWT.NONE);
		scrolledCompComp.setLayout(new GridLayout());

		newPaymentCllpCmp = new CollapsibleComposite(scrolledCompComp, SWT.NONE);
		newPaymentCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		newPaymentCllpCmp.setTitleText("New Payment");

		paymentEditComposite = new PaymentEditComposite(newPaymentCllpCmp.composite, SWT.NONE);

		tradeInInfoCllpCmp = new CollapsibleComposite(scrolledCompComp, SWT.NONE);
		GridData gd_currentPaymentsCllpCmp = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_currentPaymentsCllpCmp.exclude = true;
		tradeInInfoCllpCmp.setLayoutData(gd_currentPaymentsCllpCmp);
		tradeInInfoCllpCmp.setTitleText("Trade In Information");
		tradeInInfoCllpCmp.remainExpanded();
		tradeInInfoCllpCmp.setVisible(false);

		tradeInComposite = new TradeInEditComposite(tradeInInfoCllpCmp.composite, SWT.NONE);

		buttonBarComposite = new ButtonBarComposite(scrolledCompComp, SWT.None, false);

		currentPaymentsCllpCmp = new CollapsibleComposite(scrolledCompComp, SWT.NONE);
		currentPaymentsCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		currentPaymentsCllpCmp.setTitleText("Current Payments");

		paymentTableComposite = new PaymentTableComposite(currentPaymentsCllpCmp.composite, SWT.NONE,
				SWT.FULL_SELECTION);

		scrolledComp.setContent(scrolledCompComp);
		scrolledComp.setMinSize(scrolledCompComp.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		Point size = getShell().computeSize(600, 500);
		getShell().setSize(size);
		Monitor primary = getShell().getMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = getShell().getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		getShell().setLocation(x, y);
	}
}
