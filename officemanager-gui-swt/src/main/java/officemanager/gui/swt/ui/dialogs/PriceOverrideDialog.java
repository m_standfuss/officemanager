package officemanager.gui.swt.ui.dialogs;

import java.math.BigDecimal;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.ButtonBarComposite;
import officemanager.gui.swt.ui.widgets.DecimalText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.MessageDialogs;

public class PriceOverrideDialog extends Dialog {

	private Shell shell;
	private UpperCaseText txtOverrideReason;
	private DecimalText txtNewPrice;
	private int returnCode;

	private String overrideReason;
	private BigDecimal newPrice;
	private Composite composite;
	private ButtonBarComposite buttonBarComposite;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public PriceOverrideDialog(Shell parent, int style) {
		super(parent, style);
		setText("Override Price");
	}

	public String getOverrideReason() {
		return overrideReason;
	}

	public BigDecimal getNewPrice() {
		return newPrice;
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public int open(BigDecimal originalPrice) {
		createContents(originalPrice);

		Monitor primary = Display.getCurrent().getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		shell.setLocation(x, y);
		shell.setTabList(new Control[] { composite, buttonBarComposite });
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return returnCode;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param originalPrice
	 */
	private void createContents(BigDecimal originalPrice) {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.PRIMARY_MODAL);
		shell.setSize(380, 255);
		shell.setText(getText());
		shell.setLayout(new GridLayout());

		composite = new Composite(shell, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(2, false));

		Label lblOriginalPrice = new Label(composite, SWT.NONE);
		lblOriginalPrice.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblOriginalPrice.setText("Original Price:");

		Text txtOrigPrice = new Text(composite, SWT.BORDER);
		txtOrigPrice.setEditable(false);
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_text.widthHint = 75;
		txtOrigPrice.setLayoutData(gd_text);
		txtOrigPrice.setText(AppPrefs.FORMATTER.formatMoney(originalPrice));

		Label lblNewPrice = new Label(composite, SWT.NONE);
		lblNewPrice.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewPrice.setText("New Price:");

		txtNewPrice = new DecimalText(2, composite, SWT.BORDER);
		GridData gd_txtNewPrice = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtNewPrice.widthHint = 75;
		txtNewPrice.setLayoutData(gd_txtNewPrice);

		Label lblOverrideReason = new Label(composite, SWT.NONE);
		lblOverrideReason.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblOverrideReason.setText("Override Reason:");

		txtOverrideReason = new UpperCaseText(composite, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		GridData gd_txtOverrideReason = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtOverrideReason.heightHint = 100;
		gd_txtOverrideReason.widthHint = 250;
		txtOverrideReason.setLayoutData(gd_txtOverrideReason);
		composite.setTabList(new Control[] { txtNewPrice, txtOverrideReason });

		buttonBarComposite = new ButtonBarComposite(shell, SWT.RIGHT_TO_LEFT);
		final GridData gridData_1 = (GridData) buttonBarComposite.btnClear.getLayoutData();
		gridData_1.verticalAlignment = SWT.CENTER;
		gridData_1.horizontalAlignment = SWT.RIGHT;
		gridData_1.grabExcessVerticalSpace = false;
		gridData_1.grabExcessHorizontalSpace = false;
		final GridData gridData = (GridData) buttonBarComposite.btnSave.getLayoutData();
		gridData.verticalAlignment = SWT.CENTER;
		gridData.horizontalAlignment = SWT.LEFT;
		gridData.grabExcessVerticalSpace = false;
		gridData.grabExcessHorizontalSpace = false;
		buttonBarComposite.setLayout(new GridLayout(2, false));
		buttonBarComposite.btnClear.setText("Cancel");
		buttonBarComposite.btnSave.setText("Ok");

		buttonBarComposite.btnClear.addListener(SWT.Selection, e -> {
			returnCode = SWT.CANCEL;
			shell.close();
		});
		buttonBarComposite.btnSave.addListener(SWT.Selection, e -> save());
	}

	private void save() {
		if (txtNewPrice.getValue() == null) {
			MessageDialogs.displayError(shell, "Cannot Save", "Please fill out a new price.");
			return;
		}
		if (txtOverrideReason.getText().isEmpty()) {
			MessageDialogs.displayError(shell, "Cannot Save", "Please give a reason for overriding the current price.");
			return;
		}
		newPrice = txtNewPrice.getValue();
		overrideReason = txtOverrideReason.getText();
		returnCode = SWT.SAVE;
		shell.close();
	}

}
