package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.ClientProductTableComposite;
import officemanager.gui.swt.ui.tables.OrderCommentTableComposite;
import officemanager.gui.swt.ui.tables.OrderFlatRateTableComposite;
import officemanager.gui.swt.ui.tables.OrderPartTableComposite;
import officemanager.gui.swt.ui.tables.OrderServiceTableComposite;
import officemanager.gui.swt.ui.tables.PaymentTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class OrderComposite extends Composite {
	public final ToolBar toolBar;

	public ToolItem tltmPaymentItem;
	public ToolItem tltmPrintItem;
	public ToolItem tltmAddComment;
	public ToolItem tltmManageParts;
	public ToolItem tltmManageServices;
	public ToolItem tltmCloseOrder;
	public ToolItem tltmDeleteOrder;
	public ToolItem tltmReopenOrder;
	public ToolItem tltmRefund;

	public final Menu menuServices;
	public final MenuItem mntmServices;
	public final MenuItem mntmFlatRateServices;

	public final Menu menuPrintItems;
	public final MenuItem mntmPrintShopTicket;
	public final MenuItem mntmPrintClaimTicket;
	public final MenuItem mntmPrintReciept;

	public final ScrolledComposite scrCompositeTop;

	public final ScrolledComposite scrCompositeCenter;
	public final Composite compositeCenter;

	public final CollapsibleComposite collapsiblePayments;
	public final CollapsibleComposite collapsibleProducts;
	public final CollapsibleComposite collapsibleParts;
	public final CollapsibleComposite collapsibleServices;
	public final CollapsibleComposite collapsibleFlatRateServices;
	public final CollapsibleComposite collapsibleComments;

	public final OrderInformationComposite orderInformationComposite;
	public final ClientProductTableComposite compositeClientProductTable;
	public final OrderPartTableComposite compositePartTable;
	public final OrderServiceTableComposite compositeServiceTable;
	public final OrderFlatRateTableComposite compositeFlatRateTable;
	public final OrderCommentTableComposite compositeCommentTable;
	public final PaymentTableComposite compositePaymentTable;

	public final Label lblNoProducts;
	public final Label lblNoParts;
	public final Label lblNoServices;
	public final Label lblNoFlatRateServices;
	public final Label lblNoComments;

	public final Label lblDiscountPrice;

	public OrderComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, true));

		menuServices = new Menu(getShell(), SWT.POP_UP);

		mntmServices = new MenuItem(menuServices, SWT.NONE);
		mntmServices.setText("Services");
		mntmFlatRateServices = new MenuItem(menuServices, SWT.NONE);
		mntmFlatRateServices.setText("Flat Rate Services");

		menuPrintItems = new Menu(getShell(), SWT.POP_UP);

		mntmPrintShopTicket = new MenuItem(menuPrintItems, SWT.NONE);
		mntmPrintShopTicket.setText("Shop Ticket(s)");
		mntmPrintClaimTicket = new MenuItem(menuPrintItems, SWT.NONE);
		mntmPrintClaimTicket.setText("Claim Ticket");
		mntmPrintReciept = new MenuItem(menuPrintItems, SWT.NONE);
		mntmPrintReciept.setText("Order Reciept");

		toolBar = new ToolBar(this, SWT.FLAT | SWT.WRAP);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		createToolItems();

		scrCompositeTop = new ScrolledComposite(this, SWT.H_SCROLL);
		scrCompositeTop.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		scrCompositeTop.setExpandHorizontal(true);
		scrCompositeTop.setExpandVertical(true);

		orderInformationComposite = new OrderInformationComposite(scrCompositeTop, SWT.BORDER);
		orderInformationComposite.addListener(SWT.Resize,
				e -> scrCompositeTop.setMinSize(orderInformationComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));

		scrCompositeTop.setContent(orderInformationComposite);
		scrCompositeTop.setMinSize(orderInformationComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		scrCompositeCenter = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrCompositeCenter.setExpandHorizontal(true);
		scrCompositeCenter.setExpandVertical(true);

		compositeCenter = new Composite(scrCompositeCenter, SWT.NONE);
		compositeCenter.setLayout(new GridLayout(1, true));

		collapsiblePayments = new CollapsibleComposite(compositeCenter, SWT.NONE);
		collapsiblePayments.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		collapsiblePayments.setTitleText("Payments");
		collapsiblePayments.setTitleImage(ResourceManager.getImage(ImageFile.CASH, AppPrefs.ICN_SIZE_CLPCMP));
		collapsiblePayments.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		collapsiblePayments.composite.setLayout(new GridLayout(1, true));

		compositePaymentTable = new PaymentTableComposite(collapsiblePayments.composite, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_compositePaymentTable = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositePaymentTable.minimumHeight = 50;
		compositePaymentTable.setLayoutData(gd_compositePaymentTable);

		collapsibleProducts = new CollapsibleComposite(compositeCenter, SWT.NONE);
		collapsibleProducts.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		collapsibleProducts.setTitleText("Products");
		collapsibleProducts.setTitleImage(ResourceManager.getImage(ImageFile.PRODUCT, AppPrefs.ICN_SIZE_CLPCMP));
		collapsibleProducts.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		collapsibleProducts.composite.setLayout(new GridLayout(1, true));

		compositeClientProductTable = new ClientProductTableComposite(collapsibleProducts.composite, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_compositeClientProductTable = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositeClientProductTable.minimumHeight = 50;
		compositeClientProductTable.setLayoutData(gd_compositeClientProductTable);

		lblNoProducts = new Label(collapsibleProducts.composite, SWT.NONE);
		GridData gd_lblNoProducts = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblNoProducts.horizontalIndent = 5;
		lblNoProducts.setLayoutData(gd_lblNoProducts);
		lblNoProducts.setText("No client products have been added for this order.");
		lblNoProducts.setFont(ResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));

		collapsibleParts = new CollapsibleComposite(compositeCenter, SWT.NONE);
		collapsibleParts.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		collapsibleParts.setTitleText("Parts");
		collapsibleParts.setTitleImage(ResourceManager.getImage(ImageFile.PART, AppPrefs.ICN_SIZE_CLPCMP));
		collapsibleParts.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		collapsibleParts.composite.setLayout(new GridLayout());

		compositePartTable = new OrderPartTableComposite(collapsibleParts.composite, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_compositePartTable = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositePartTable.minimumHeight = 50;
		compositePartTable.setLayoutData(gd_compositePartTable);

		lblNoParts = new Label(collapsibleParts.composite, SWT.NONE);
		GridData gd_lblNoParts = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblNoParts.horizontalIndent = 5;
		lblNoParts.setLayoutData(gd_lblNoParts);
		lblNoParts.setText("No parts have been added for this order.");
		lblNoParts.setFont(ResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));

		collapsibleServices = new CollapsibleComposite(compositeCenter, SWT.NONE);
		collapsibleServices.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		collapsibleServices.setTitleText("Services");
		collapsibleServices.setTitleImage(ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_CLPCMP));
		collapsibleServices.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		collapsibleServices.composite.setLayout(new GridLayout());

		compositeServiceTable = new OrderServiceTableComposite(collapsibleServices.composite, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_compositeServiceTable = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositeServiceTable.minimumHeight = 50;
		compositeServiceTable.setLayoutData(gd_compositeServiceTable);

		lblNoServices = new Label(collapsibleServices.composite, SWT.NONE);
		GridData gd_lblNoServices = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblNoServices.horizontalIndent = 5;
		lblNoServices.setLayoutData(gd_lblNoServices);
		lblNoServices.setText("No services have been added for this order.");
		lblNoServices.setFont(ResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));

		collapsibleFlatRateServices = new CollapsibleComposite(compositeCenter, SWT.NONE);
		collapsibleFlatRateServices.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		collapsibleFlatRateServices.setTitleText("Flat Rate Services");
		collapsibleFlatRateServices
				.setTitleImage(ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_CLPCMP));
		collapsibleFlatRateServices.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		collapsibleFlatRateServices.composite.setLayout(new GridLayout());

		compositeFlatRateTable = new OrderFlatRateTableComposite(collapsibleFlatRateServices.composite, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_compositeFlatRateTable = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositeFlatRateTable.minimumHeight = 50;
		compositeFlatRateTable.setLayoutData(gd_compositeFlatRateTable);

		lblNoFlatRateServices = new Label(collapsibleFlatRateServices.composite, SWT.NONE);
		GridData gd_lblNoFlatRateServices = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblNoFlatRateServices.horizontalIndent = 5;
		lblNoFlatRateServices.setLayoutData(gd_lblNoFlatRateServices);
		lblNoFlatRateServices.setText("No flat rate services have been added for this order.");
		lblNoFlatRateServices.setFont(ResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));

		collapsibleComments = new CollapsibleComposite(compositeCenter, SWT.NONE);
		collapsibleComments.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		collapsibleComments.setTitleText("Comments");
		collapsibleComments.setTitleImage(ResourceManager.getImage(ImageFile.COMMENT, AppPrefs.ICN_SIZE_CLPCMP));
		collapsibleComments.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(-1, compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		collapsibleComments.composite.setLayout(new GridLayout());

		compositeCommentTable = new OrderCommentTableComposite(collapsibleComments.composite, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER);
		GridData gd_compositeCommentTable = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositeCommentTable.minimumHeight = 50;
		compositeCommentTable.setLayoutData(gd_compositeCommentTable);

		lblNoComments = new Label(collapsibleComments.composite, SWT.NONE);
		GridData gd_lblNoComments = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblNoComments.horizontalIndent = 5;
		lblNoComments.setLayoutData(gd_lblNoComments);
		lblNoComments.setText("No comments have been added for this order.");
		lblNoComments.setFont(ResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));

		scrCompositeCenter.setContent(compositeCenter);
		scrCompositeCenter.setMinSize(compositeCenter.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		lblDiscountPrice = new Label(this, SWT.NONE);
		lblDiscountPrice.setText("Denotes a discounted price");
		lblDiscountPrice.setForeground(AppPrefs.COLOR_DISCOUNT_PRICE);
	}

	public void recreateToolItems(boolean showPayments, boolean showCloseOrder, boolean showPrint, boolean showComments,
			boolean showServices, boolean showParts, boolean showDelete, boolean showReopen, boolean showRefund) {
		disposeToolItems();
		if (showPayments) {
			tltmPaymentItem = new ToolItem(toolBar, SWT.NONE);
			tltmPaymentItem.setImage(ResourceManager.getImage(ImageFile.CASH, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmPaymentItem.setToolTipText("Manage Payments");
		}

		if (showRefund) {
			tltmRefund = new ToolItem(toolBar, SWT.NONE);
			tltmRefund.setImage(ResourceManager.getImage(ImageFile.REFUND, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmRefund.setToolTipText("Refund Components");
		}

		if (showServices) {
			tltmManageServices = new ToolItem(toolBar, SWT.DROP_DOWN);
			tltmManageServices.setImage(ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmManageServices.setToolTipText("Manage Services");
			tltmManageServices.addListener(SWT.Selection, e -> {
				Rectangle rect = tltmManageServices.getBounds();
				Point pt = new Point(rect.x, rect.y + rect.height);
				pt = toolBar.toDisplay(pt);
				menuServices.setLocation(pt.x, pt.y);
				menuServices.setVisible(true);
			});
		}

		if (showParts) {
			tltmManageParts = new ToolItem(toolBar, SWT.NONE);
			tltmManageParts.setImage(ResourceManager.getImage(ImageFile.PART, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmManageParts.setToolTipText("Manage Parts");
		}

		if (showComments) {
			tltmAddComment = new ToolItem(toolBar, SWT.NONE);
			tltmAddComment.setImage(ResourceManager.getImage(ImageFile.COMMENT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmAddComment.setToolTipText("Add Comment");
		}

		if (showPrint) {
			tltmPrintItem = new ToolItem(toolBar, SWT.DROP_DOWN);
			tltmPrintItem.setImage(ResourceManager.getImage(ImageFile.PRINT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmPrintItem.setToolTipText("Print");
			tltmPrintItem.addListener(SWT.Selection, e -> {
				Rectangle rect = tltmPrintItem.getBounds();
				Point pt = new Point(rect.x, rect.y + rect.height);
				pt = toolBar.toDisplay(pt);
				menuPrintItems.setLocation(pt.x, pt.y);
				menuPrintItems.setVisible(true);
			});
		}

		if (showCloseOrder) {
			tltmCloseOrder = new ToolItem(toolBar, SWT.NONE);
			tltmCloseOrder.setImage(ResourceManager.getImage(ImageFile.CLOSE, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmCloseOrder.setToolTipText("Close Order");
		}

		if (showDelete) {
			tltmDeleteOrder = new ToolItem(toolBar, SWT.NONE);
			tltmDeleteOrder.setImage(ResourceManager.getImage(ImageFile.RED_X, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmDeleteOrder.setToolTipText("Delete Order");
		}

		if (showReopen) {
			tltmReopenOrder = new ToolItem(toolBar, SWT.NONE);
			tltmReopenOrder.setImage(ResourceManager.getImage(ImageFile.OPEN, AppPrefs.ICN_SIZE_PAGETOOLITEM));
			tltmReopenOrder.setToolTipText("Reopen Order");
		}

		toolBar.redraw();
		toolBar.layout();
	}

	private void createToolItems() {
		recreateToolItems(true, true, true, true, true, true, true, true, true);
	}

	private void disposeToolItems() {
		if (tltmPaymentItem != null) {
			tltmPaymentItem.dispose();
		}
		if (tltmPrintItem != null) {
			tltmPrintItem.dispose();
		}
		if (tltmAddComment != null) {
			tltmAddComment.dispose();
		}
		if (tltmManageParts != null) {
			tltmManageParts.dispose();
		}
		if (tltmManageServices != null) {
			tltmManageServices.dispose();
		}
		if (tltmCloseOrder != null) {
			tltmCloseOrder.dispose();
		}
		if (tltmDeleteOrder != null) {
			tltmDeleteOrder.dispose();
		}
		if (tltmReopenOrder != null) {
			tltmReopenOrder.dispose();
		}
		if (tltmRefund != null) {
			tltmRefund.dispose();
		}
	}

	@Override
	public void layout() {
		collapsiblePayments.layout();
		collapsibleProducts.layout();
		collapsibleParts.layout();
		collapsibleServices.layout();
		collapsibleFlatRateServices.layout();
		collapsibleComments.layout();
		super.layout();
	}
}
