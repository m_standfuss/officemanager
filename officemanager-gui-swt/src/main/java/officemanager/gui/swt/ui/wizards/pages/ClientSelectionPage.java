package officemanager.gui.swt.ui.wizards.pages;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.Lists;

import officemanager.biz.records.ClientSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.ClientSearchService;
import officemanager.gui.swt.ui.composites.ClientSearchComposite;
import officemanager.gui.swt.ui.tables.ClientTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ClientSelectionPage extends OfficeManagerWizardPage {

	private static final Logger logger = LogManager.getLogger(ClientSelectionPage.class);
	private static final List<Integer> COLUMNS_TO_SHOW = Lists.newArrayList(ClientTableComposite.COL_NAME,
			ClientTableComposite.COL_PHONE, ClientTableComposite.COL_EMAIL, ClientTableComposite.COL_JOINED_DTTM);

	private final int maxSelection;
	private final boolean needsSelection;
	private final ClientSearchService clientSearchService;

	private ClientSearchComposite clientSearchComposite;

	/**
	 * Create the wizard.
	 * 
	 * @wbp.parser.constructor
	 */
	public ClientSelectionPage() {
		this(1, true);
	}

	public ClientSelectionPage(int maxSelection, boolean needsSelection) {
		super("clientSelectionPage", "Client Selection", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.USER_SEARCH, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		checkArgument(maxSelection > 0, "The max selection must be greater than 0.");

		setPageComplete(!needsSelection);
		if (maxSelection == 1) {
			setDescription("Please select a client.");
		} else {
			setDescription("Please select clients.");
		}

		this.maxSelection = maxSelection;
		this.needsSelection = needsSelection;
		clientSearchService = new ClientSearchService(COLUMNS_TO_SHOW);
	}

	@Override
	public void setFocus() {
		clientSearchComposite.txtNameLast.setFocus();
	}

	/**
	 * Create contents of the wizard.
	 * 
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		setControl(container);
		container.setLayout(new GridLayout());

		clientSearchComposite = new ClientSearchComposite(container, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION | SWT.CHECK);
		clientSearchComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		clientSearchComposite.searchResultsTable.table.setMaxCheckedItems(maxSelection);
		clientSearchComposite.searchResultsTable.table.addCheckListener(e -> {
			boolean isPageComplete = !needsSelection
					|| clientSearchComposite.searchResultsTable.table.getCheckedCount() > 0;
					setPageComplete(isPageComplete);
		});

		clientSearchService.setComposite(clientSearchComposite);
	}

	public void hideToolbar() {
		clientSearchComposite.toolBar.dispose();
	}

	public Long getSelectedClientId() {
		logger.debug("Starting getSelectedClientId");
		List<Long> clientIds = getSelectedClientIds();
		if (clientIds.isEmpty()) {
			logger.debug("No client ids have been selected returning null.");
			return null;
		}
		if (clientIds.size() > 1) {
			logger.warn("Multiple client ids have been selected but only requesting one, returning first on list.");
		}
		return clientIds.get(0);
	}

	public List<Long> getSelectedClientIds() {
		logger.debug("Starting getSelectedClientIds.");
		List<ClientSearchRecord> selectedItems = clientSearchComposite.searchResultsTable.table
				.getCheckedData(ClientSearchService.DATA_RECORD);
		logger.debug(selectedItems.size() + " items checked in the table.");
		List<Long> clientIds = Lists.newArrayList();
		for (ClientSearchRecord record : selectedItems) {
			clientIds.add(record.getClientId());
		}
		return clientIds;
	}
}
