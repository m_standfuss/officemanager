package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.util.ResourceManager;

public class AccountInformationComposite extends Composite {
	public Label lblCurrentBalanceHeader;
	public Label lblCurrentBalance;
	public Label lblLastStatementHeader;
	public Label lblLastStatement;
	public Label lblStatusHeader;
	public Label lblStatus;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public AccountInformationComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 15;
		gridLayout.numColumns = 6;
		setLayout(gridLayout);

		lblCurrentBalanceHeader = new Label(this, SWT.NONE);
		lblCurrentBalanceHeader.setText("Current Balance:");
		lblCurrentBalanceHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblCurrentBalance = new Label(this, SWT.NONE);

		lblStatusHeader = new Label(this, SWT.NONE);
		lblStatusHeader.setText("Status:");
		lblStatusHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblStatus = new Label(this, SWT.NONE);

		lblLastStatementHeader = new Label(this, SWT.NONE);
		lblLastStatementHeader.setText("Last Statement:");
		lblLastStatementHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblLastStatement = new Label(this, SWT.NONE);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
