package officemanager.gui.swt.ui.dialogs;

import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import officemanager.gui.swt.util.ResourceManager;

public class AboutDialog extends Dialog {
	private static final Logger logger = LogManager.getLogger(AboutDialog.class);

	private static final String VERSION_FILE = "/version.txt";

	private static final int WIDTH = 250;
	private static final int HEIGHT = 175;

	private Shell shell;
	private Composite composite;
	private Label lblVersion;
	private Label lblBuildDate;
	private Label lblAbout;
	private Button btnClose;
	public Label lblOS;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public AboutDialog(Shell parent, int style) {
		super(parent, style);
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public void open() {
		createContents();
		addListeners();
		populateContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.BORDER | SWT.CLOSE | SWT.PRIMARY_MODAL);
		shell.setSize(WIDTH, HEIGHT);
		shell.setText("About");
		shell.setLayout(new GridLayout());

		Rectangle parentSize = getParent().getBounds();

		int locationX, locationY;
		locationX = (parentSize.width - WIDTH) / 2 + parentSize.x;
		locationY = (parentSize.height - HEIGHT) / 2 + parentSize.y;

		shell.setLocation(new Point(locationX, locationY));

		composite = new Composite(shell, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		lblAbout = new Label(composite, SWT.NONE);
		lblAbout.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblAbout.setText("");
		lblAbout.setFont(ResourceManager.getFont("Segoe UI", 12, SWT.BOLD));

		lblVersion = new Label(composite, SWT.NONE);
		lblVersion.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblVersion.setText("");

		lblOS = new Label(composite, SWT.NONE);
		lblOS.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblOS.setText("");

		lblBuildDate = new Label(composite, SWT.NONE);
		lblBuildDate.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblBuildDate.setText("");

		btnClose = new Button(shell, SWT.NONE);
		btnClose.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnClose.setText("Close");
	}

	private void addListeners() {
		btnClose.addListener(SWT.Selection, e -> shell.close());
	}

	private void populateContents() {
		InputStream in = null;
		Properties prop = new Properties();
		try {
			in = AboutDialog.class.getResourceAsStream(VERSION_FILE);
			prop.load(in);
		} catch (Exception e) {
			logger.error("Unable to get the configuration file.", e);
			return;
		} finally {
			IOUtils.closeQuietly(in);
		}

		String version = prop.getProperty("version");
		String buildDate = prop.getProperty("build.date");
		String os = prop.getProperty("build.os");
		String arch = prop.getProperty("build.arch");

		lblAbout.setText("OfficeManager");
		lblVersion.setText("Version: " + version);
		lblBuildDate.setText("Built On: " + buildDate);
		lblOS.setText("Built For: " + os + "_" + arch);
	}
}
