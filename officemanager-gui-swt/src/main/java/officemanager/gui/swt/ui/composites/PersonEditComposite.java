package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public final class PersonEditComposite extends Composite {
	public final Label lblFirstName;
	public final UpperCaseText txtFirstName;
	public final Label lblLastName;
	public final UpperCaseText txtLastName;
	public final Label lblFullFormatted;
	public final UpperCaseText txtFullFormatted;
	public final Label lblEmail;
	public final UpperCaseText txtEmail;
	private final Label lblDenotesA;

	public PersonEditComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		lblFirstName = new Label(this, SWT.NONE);
		lblFirstName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFirstName.setText("First Name:");
		lblFirstName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtFirstName = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtFirstName = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_txtFirstName.widthHint = 250;
		txtFirstName.setLayoutData(gd_txtFirstName);
		txtFirstName.setTextLimit(30);

		lblLastName = new Label(this, SWT.NONE);
		lblLastName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLastName.setText("Last/Company Name*:");
		lblLastName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtLastName = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtLastName = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_txtLastName.widthHint = 250;
		txtLastName.setLayoutData(gd_txtLastName);
		txtLastName.setTextLimit(50);

		lblFullFormatted = new Label(this, SWT.NONE);
		lblFullFormatted.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFullFormatted.setText("Full Formatted:");
		lblFullFormatted.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtFullFormatted = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtFullFormatted = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_txtFullFormatted.widthHint = 250;
		txtFullFormatted.setLayoutData(gd_txtFullFormatted);
		txtFullFormatted.setTextLimit(80);

		lblEmail = new Label(this, SWT.NONE);
		lblEmail.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEmail.setText("Email:");
		lblEmail.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtEmail = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtEmail = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_txtEmail.widthHint = 250;
		txtEmail.setLayoutData(gd_txtEmail);
		txtEmail.setTextLimit(50);

		lblDenotesA = new Label(this, SWT.NONE);
		lblDenotesA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblDenotesA.setText("* Denotes a required field");
	}
}
