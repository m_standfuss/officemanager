package officemanager.gui.swt.ui.widgets;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.google.common.collect.Lists;

public class ScannerText extends Text {

	private static int FINISHED_KEYCODE = SWT.CR;

	private final List<Listener> entryEnteredListeners;

	public void setFinishedKeycode(int keycode) {
		FINISHED_KEYCODE = keycode;
	}

	public ScannerText(Composite parent, int style) {
		super(parent, style);
		entryEnteredListeners = Lists.newArrayList();
		addListeners();
	}

	public void addEntryEnteredListener(Listener l) {
		checkNotNull(l);
		entryEnteredListeners.add(l);
	}

	@Override
	protected void checkSubclass() {}

	private void addListeners() {
		addListener(SWT.Traverse, e -> {
			if (e.keyCode == FINISHED_KEYCODE) {
				fireEnteredListeners();
				setText("");
				e.doit = false;
			}
		});
	}

	private void fireEnteredListeners() {
		Event e = new Event();
		e.text = getText();
		for (Listener l : entryEnteredListeners) {
			l.handleEvent(e);
		}
	}

}
