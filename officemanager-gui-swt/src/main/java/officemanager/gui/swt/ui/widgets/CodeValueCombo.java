package officemanager.gui.swt.ui.widgets;

import java.util.Collection;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.records.CodeValueRecord;

public class CodeValueCombo extends Combo {

	private static final String DATA_PREPEND = "CODE_VALUE_RECORD";

	public CodeValueCombo(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected void checkSubclass() {
	}

	public void populateSelections(Collection<CodeValueRecord> values) {
		removeAll();
		int i = 0;
		for (CodeValueRecord record : values) {
			add(record.getDisplay());
			setData(DATA_PREPEND + i, record);
			i++;
		}
	}

	public CodeValueRecord getCodeValueRecord() {
		int index = getSelectionIndex();
		return (CodeValueRecord) getData(DATA_PREPEND + index);
	}

	public Long getCodeValue() {
		CodeValueRecord record = getCodeValueRecord();
		if (record == null) {
			return null;
		}
		return record.getCodeValue();
	}

	public void setSelectedValue(CodeValueRecord codeValueRecord) {
		if (codeValueRecord == null) {
			deselectAll();
			return;
		}
		for (int index = 0; index < getItemCount(); index++) {
			if (codeValueRecord.equals(getData(DATA_PREPEND + index))) {
				select(index);
				break;
			}
		}
	}

	public void setSelectedValue(Long codeValue) {
		if (codeValue == null) {
			deselectAll();
			return;
		}
		for (int index = 0; index < getItemCount(); index++) {
			CodeValueRecord record = (CodeValueRecord) getData(DATA_PREPEND + index);
			if (codeValue.equals(record.getCodeValue())) {
				select(index);
				break;
			}
		}
	}
}
