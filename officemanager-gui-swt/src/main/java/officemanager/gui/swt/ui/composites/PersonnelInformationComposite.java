package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.util.ResourceManager;

public class PersonnelInformationComposite extends PersonInformationComposite {

	public Label lblPersonnelTypeTitle;
	public Label lblStatusTitle;
	public Label lblUsernameTitle;
	public Label lblUsername;
	public Label lblStartDateTitle;
	public Label lblStartDate;
	public Label lblTerminatedDateTitle;
	public Label lblTerminatedDate;
	public Label lblW2OnFileTitle;
	public Label lblW2OnFile;
	public Label lblStatus;
	public Label lblPersonnelType;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PersonnelInformationComposite(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public void fillAuxInfo(Composite parent) {
		lblPersonnelTypeTitle = new Label(parent, SWT.NONE);
		lblPersonnelTypeTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPersonnelTypeTitle.setText("Personnel Type:");
		lblPersonnelTypeTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblPersonnelType = new Label(parent, SWT.NONE);

		lblUsernameTitle = new Label(parent, SWT.NONE);
		lblUsernameTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblUsernameTitle.setText("Username:");
		lblUsernameTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblUsername = new Label(parent, SWT.NONE);

		lblStatusTitle = new Label(parent, SWT.NONE);
		lblStatusTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblStatusTitle.setText("Status:");
		lblStatusTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblStatus = new Label(parent, SWT.NONE);

		lblStartDateTitle = new Label(parent, SWT.NONE);
		lblStartDateTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblStartDateTitle.setText("Start Date:");
		lblStartDateTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblStartDate = new Label(parent, SWT.NONE);

		lblTerminatedDateTitle = new Label(parent, SWT.NONE);
		lblTerminatedDateTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTerminatedDateTitle.setText("Terminated Date:");
		lblTerminatedDateTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblTerminatedDate = new Label(parent, SWT.NONE);

		lblW2OnFileTitle = new Label(parent, SWT.NONE);
		lblW2OnFileTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblW2OnFileTitle.setText("W2 On File:");
		lblW2OnFileTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblW2OnFile = new Label(parent, SWT.NONE);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
