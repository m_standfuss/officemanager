package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.ui.widgets.UpperCaseText;

public class CodeValueEditComposite extends Composite {
	public Label lblDisplay;
	public Text txtDisplay;
	public Label lblCollatingSequence;
	public Spinner spnCollationSeq;
	public Label lblCodedValue;
	public Text txtCodedValue;
	public Label lblCodedMeaning;
	public Text txtCodedMeaning;
	public Button btnNoCollation;
	public Composite composite;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public CodeValueEditComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		GridLayout gridLayout = new GridLayout();
		setLayout(gridLayout);

		lblCodedValue = new Label(this, SWT.NONE);
		lblCodedValue.setText("Coded Value");

		txtCodedValue = new Text(this, SWT.BORDER);
		txtCodedValue.setEditable(false);
		GridData gd_text_1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_text_1.widthHint = 75;
		txtCodedValue.setLayoutData(gd_text_1);

		lblCodedMeaning = new Label(this, SWT.NONE);
		lblCodedMeaning.setText("Coded Meaning");

		txtCodedMeaning = new Text(this, SWT.BORDER);
		txtCodedMeaning.setEditable(false);
		GridData gd_text_2 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_text_2.widthHint = 150;
		txtCodedMeaning.setLayoutData(gd_text_2);

		lblDisplay = new Label(this, SWT.NONE);
		lblDisplay.setText("Display");

		txtDisplay = new UpperCaseText(this, SWT.BORDER);
		txtDisplay.setTextLimit(50);
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_text.widthHint = 150;
		txtDisplay.setLayoutData(gd_text);

		lblCollatingSequence = new Label(this, SWT.NONE);
		lblCollatingSequence.setText("Collating Sequence");

		composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(2, false));

		spnCollationSeq = new Spinner(composite, SWT.BORDER);
		spnCollationSeq.setMaximum(32767);

		btnNoCollation = new Button(composite, SWT.CHECK);
		btnNoCollation.setText("No Collation");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
