package officemanager.gui.swt.ui.wizards.pages;

import java.math.BigDecimal;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.PartEditComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PartDetailsPage extends OfficeManagerWizardPage {

	public PartEditComposite composite;

	public PartDetailsPage() {
		super("partDetailsPage", "Part Details", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.PART, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("Enter part details");
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrComposite = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrComposite.setExpandHorizontal(true);
		scrComposite.setExpandVertical(true);

		scrComposite.setContent(composite);
		scrComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrComposite);
		composite.addListener(SWT.Resize,
				l -> scrComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));

		addListeners();
	}

	public void checkPageComplete() {
		setPageComplete(false);
		if (composite.txtPartName.getText().trim().isEmpty()) {
			setMessage("Please populate a part name.");
		} else if (composite.txtBasePrice.getValue() == null) {
			setMessage("Please populate a base price.");
		} else if (composite.txtBasePrice.getValue().compareTo(BigDecimal.ZERO) < 0) {
			setErrorMessage("The base price must be a positive amount.");
		} else if (composite.cmbCategory.getSelectionIndex() == -1) {
			setMessage("Please populate a part category.");
		} else if (composite.cmbManufacturer.getSelectionIndex() == -1) {
			setMessage("Please populate a manufacturer.");
		} else if (composite.cmbProductType.getSelectionIndex() == -1) {
			setMessage("Please populate a product type.");
		} else {
			setPageComplete(true);
			setMessage(null);
			setErrorMessage(null);
		}
	}

	private void addListeners() {
		composite.txtPartName.addListener(SWT.Modify, e -> checkPageComplete());
		composite.txtBasePrice.addListener(SWT.Modify, e -> checkPageComplete());
		composite.cmbManufacturer.addListener(SWT.Selection, e -> checkPageComplete());
		composite.cmbProductType.addListener(SWT.Selection, e -> checkPageComplete());
	}
}
