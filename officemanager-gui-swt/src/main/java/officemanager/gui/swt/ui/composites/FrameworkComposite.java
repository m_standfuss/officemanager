package officemanager.gui.swt.ui.composites;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.mihalis.opal.breadcrumb.Breadcrumb;
import org.mihalis.opal.breadcrumb.BreadcrumbItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class FrameworkComposite extends Composite {

	public final Menu menuBar;

	public final Menu fileMenu;
	public final MenuItem fileMenuItem;
	public final MenuItem logoutMenuItem;
	public final MenuItem exitMenuItem;

	public final Menu helpMenu;
	public final MenuItem helpMenuItem;
	public final MenuItem helpContentItem;
	public final MenuItem aboutMenuItem;

	public final ToolBar toolBar;

	public final ToolItem tltmSaveItem;

	public final ScrolledComposite scrCompositeSideBar;
	public final Composite compositeSideBar;

	public final ToolBar toolBarRefresh;
	public final ToolItem tltmRefresh;
	public final Composite compositeMainStage;
	public final Composite compositeBreadcrumb;
	public final Composite compositeTop;

	// Weird error when trying to clear out breadcrumbs and re add new ones so
	// instead of clearing them out just dispose of the existing breadcrumb and
	// make a new one, so this has to be non final.
	public Breadcrumb breadcrumb;

	public FrameworkComposite(Shell parent, int style) {
		super(parent, style);
		try {
			Field field = BreadcrumbItem.class.getDeclaredField("SELECTED_COLOR");
			field.setAccessible(true);
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
			field.set(null, AppPrefs.COLOR_BREADCRUMB_SELECTED);

			field = Breadcrumb.class.getDeclaredField("START_GRADIENT_COLOR");
			field.setAccessible(true);
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
			field.set(null, AppPrefs.COLOR_BREADCRUMB);

			field = Breadcrumb.class.getDeclaredField("END_GRADIENT_COLOR");
			field.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
			field.set(null, AppPrefs.COLOR_BREADCRUMB_GRADIENT);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}

		menuBar = new Menu(parent, SWT.BAR);
		fileMenuItem = new MenuItem(menuBar, SWT.CASCADE);
		fileMenuItem.setText("&File");
		helpMenuItem = new MenuItem(menuBar, SWT.CASCADE);
		helpMenuItem.setText("&Help");

		fileMenu = new Menu(parent, SWT.DROP_DOWN);
		fileMenuItem.setMenu(fileMenu);
		logoutMenuItem = new MenuItem(fileMenu, SWT.PUSH);
		logoutMenuItem.setText("&Logout");
		exitMenuItem = new MenuItem(fileMenu, SWT.PUSH);
		exitMenuItem.setText("E&xit");

		helpMenu = new Menu(parent, SWT.DROP_DOWN);
		helpMenuItem.setMenu(helpMenu);
		helpContentItem = new MenuItem(helpMenu, SWT.PUSH);
		helpContentItem.setText("&Help Content");
		aboutMenuItem = new MenuItem(helpMenu, SWT.PUSH);
		aboutMenuItem.setText("&About");

		parent.setMenuBar(menuBar);

		setLayout(new GridLayout(2, false));

		scrCompositeSideBar = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeSideBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 2));
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		compositeSideBar = new Composite(scrCompositeSideBar, SWT.NONE);
		compositeSideBar.setBackground(ResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		RowLayout rl_compositeSideBar = new RowLayout(SWT.VERTICAL);
		rl_compositeSideBar.spacing = 1;
		rl_compositeSideBar.marginRight = 0;
		rl_compositeSideBar.marginLeft = 0;
		rl_compositeSideBar.marginTop = 0;
		rl_compositeSideBar.marginBottom = 0;
		rl_compositeSideBar.wrap = false;
		rl_compositeSideBar.fill = true;
		compositeSideBar.setLayout(rl_compositeSideBar);

		scrCompositeSideBar.setContent(compositeSideBar);
		scrCompositeSideBar.setMinSize(compositeSideBar.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		compositeTop = new Composite(this, SWT.BORDER | SWT.EMBEDDED);
		compositeTop.setBackground(ResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		compositeTop.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		GridLayout gl_compositeTop = new GridLayout(3, false);
		gl_compositeTop.marginRight = 5;
		compositeTop.setLayout(gl_compositeTop);

		toolBar = new ToolBar(compositeTop, SWT.FLAT | SWT.RIGHT);
		toolBar.setBackground(ResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		tltmSaveItem = new ToolItem(toolBar, SWT.NONE);
		tltmSaveItem.setEnabled(false);
		tltmSaveItem.setImage(ResourceManager.getImage(ImageFile.SAVE, AppPrefs.ICN_SIZE_MAINTOOLBAR));
		tltmSaveItem.setToolTipText("Save");

		compositeBreadcrumb = new Composite(compositeTop, SWT.NONE);
		compositeBreadcrumb.setBackground(ResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		compositeBreadcrumb.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		GridLayout gl_compositeBreadcrumb = new GridLayout(1, false);
		gl_compositeBreadcrumb.verticalSpacing = 0;
		gl_compositeBreadcrumb.marginWidth = 0;
		gl_compositeBreadcrumb.marginHeight = 0;
		gl_compositeBreadcrumb.horizontalSpacing = 0;
		compositeBreadcrumb.setLayout(gl_compositeBreadcrumb);

		recreateBreadcrumb();

		toolBarRefresh = new ToolBar(compositeTop, SWT.FLAT | SWT.RIGHT);
		toolBarRefresh.setBackground(ResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));

		tltmRefresh = new ToolItem(toolBarRefresh, SWT.NONE);
		tltmRefresh.setImage(ResourceManager.getImage(ImageFile.REFRESH, AppPrefs.ICN_SIZE_MAINTOOLBAR));
		tltmRefresh.setToolTipText("Refresh");

		compositeMainStage = new Composite(this, SWT.BORDER);
		StackLayout sl_compositeMainStage = new StackLayout();
		sl_compositeMainStage.marginHeight = 0;
		sl_compositeMainStage.marginWidth = 0;
		compositeMainStage.setLayout(sl_compositeMainStage);
		compositeMainStage.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	public void recreateBreadcrumb() {
		breadcrumb = new Breadcrumb(compositeBreadcrumb, SWT.BORDER);
		breadcrumb.setBackground(ResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		breadcrumb.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1));
		breadcrumb.setFont(ResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
