package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.OrderFlatRateTableComposite;
import officemanager.gui.swt.ui.tables.OrderPartTableComposite;
import officemanager.gui.swt.ui.tables.OrderServiceTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class OrderComponentSelectionComposite extends Composite {

	public final CollapsibleComposite clpCmpParts;
	public final OrderPartTableComposite tblParts;

	public final CollapsibleComposite clpCmpFlatRates;
	public final OrderFlatRateTableComposite tblFlatRates;

	public final CollapsibleComposite clpCmpServices;
	public final OrderServiceTableComposite tblServices;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public OrderComponentSelectionComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout());

		clpCmpParts = new CollapsibleComposite(this, SWT.NONE);
		clpCmpParts.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		clpCmpParts.setTitleText("Parts");
		clpCmpParts.setTitleImage(ResourceManager.getImage(ImageFile.PART, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpParts.composite.setLayout(new FillLayout(SWT.HORIZONTAL));

		tblParts = new OrderPartTableComposite(clpCmpParts.composite, SWT.NONE,
				SWT.FULL_SELECTION | SWT.CHECK | SWT.BORDER);

		clpCmpFlatRates = new CollapsibleComposite(this, SWT.NONE);
		clpCmpFlatRates.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		clpCmpFlatRates.setTitleText("Flat Rates");
		clpCmpFlatRates.setTitleImage(ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpFlatRates.composite.setLayout(new FillLayout(SWT.HORIZONTAL));

		tblFlatRates = new OrderFlatRateTableComposite(clpCmpFlatRates.composite, SWT.NONE,
				SWT.FULL_SELECTION | SWT.CHECK | SWT.BORDER);

		clpCmpServices = new CollapsibleComposite(this, SWT.NONE);
		clpCmpServices.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		clpCmpServices.setTitleText("Services");
		clpCmpServices.setTitleImage(ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_CLPCMP));
		clpCmpServices.composite.setLayout(new FillLayout(SWT.HORIZONTAL));

		tblServices = new OrderServiceTableComposite(clpCmpServices.composite, SWT.NONE,
				SWT.FULL_SELECTION | SWT.CHECK | SWT.BORDER);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
