package officemanager.gui.swt.ui.dialogs;

import static com.google.common.base.Preconditions.checkNotNull;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class TextDialog extends Dialog {

	private Shell shell;
	private Label lblHeader;
	private Text txtValue;

	private String headerText;
	private String text;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public TextDialog(Shell parent, int style) {
		this(parent, style, "");
	}

	public TextDialog(Shell parent, int style, String dialogTitle) {
		super(parent, style);
		setText(dialogTitle);
	}

	public void setHeaderText(String headerText) {
		checkNotNull(headerText);
		this.headerText = headerText;
	}

	@Override
	public void setText(String text) {
		checkNotNull(text);
		this.text = text;
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public void open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.PRIMARY_MODAL);
		shell.setSize(300, 250);
		shell.setText(getText());
		shell.setLayout(new GridLayout());

		Rectangle screenSize = getParent().getMonitor().getBounds();
		shell.setLocation((screenSize.width - shell.getBounds().width) / 2,
				(screenSize.height - shell.getBounds().height) / 2);

		lblHeader = new Label(shell, SWT.NONE);
		if (headerText != null) {
			lblHeader.setText(headerText);
		}

		txtValue = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		txtValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		if (text != null) {
			txtValue.setText(text);
		}
	}

}
