package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;

import officemanager.gui.swt.ui.widgets.SashFormConstant;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class HelpComposite extends Composite {

	public Composite compTableOfContents;
	public Label lblSearch;
	public Text txtSearch;
	public SashFormConstant sashForm;
	public Composite compTrees;
	public Tree treeFull;
	public Tree treeFiltered;
	public Composite compStage;
	public ToolBar toolBar;
	public ToolItem tltmPrint;
	public Browser browser;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public HelpComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		GridLayout gridLayout = new GridLayout();
		setLayout(gridLayout);

		sashForm = new SashFormConstant(this, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		sashForm.setLeftWidth(200);

		compTableOfContents = new Composite(sashForm, SWT.NONE);
		compTableOfContents.setLayout(new GridLayout(2, false));

		lblSearch = new Label(compTableOfContents, SWT.NONE);
		lblSearch.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSearch.setText("Search:");

		txtSearch = new Text(compTableOfContents, SWT.BORDER);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text.widthHint = 150;
		txtSearch.setLayoutData(gd_text);

		compTrees = new Composite(compTableOfContents, SWT.NONE);
		compTrees.setLayout(new StackLayout());
		compTrees.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 2, 1));

		treeFull = new Tree(compTrees, SWT.BORDER);
		treeFiltered = new Tree(compTrees, SWT.BORDER);
		((StackLayout) compTrees.getLayout()).topControl = treeFull;

		compStage = new Composite(sashForm, SWT.NONE);
		compStage.setLayout(new GridLayout(1, false));

		toolBar = new ToolBar(compStage, SWT.FLAT | SWT.RIGHT | SWT.RIGHT_TO_LEFT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		tltmPrint = new ToolItem(toolBar, SWT.NONE);
		tltmPrint.setToolTipText("Print");
		tltmPrint.setImage(ResourceManager.getImage(ImageFile.PRINT, 16));

		browser = new Browser(compStage, SWT.BORDER);
		browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		sashForm.setWeights(new int[] { 1, 4 });

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
