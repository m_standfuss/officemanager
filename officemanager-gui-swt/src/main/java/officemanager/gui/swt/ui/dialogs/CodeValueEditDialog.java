package officemanager.gui.swt.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import officemanager.biz.delegates.CodeValueDelegate;
import officemanager.biz.records.CodeValueRecord;
import officemanager.gui.swt.ui.composites.CodeValueEditComposite;
import officemanager.gui.swt.util.MessageDialogs;

public class CodeValueEditDialog extends Dialog {

	private final CodeValueDelegate codeValueDelegate;

	private CodeValueRecord recordInContext;
	private CodeValueEditComposite composite;

	public CodeValueEditDialog(Shell parentShell, Long codeSet, Object dummy) {
		this(parentShell, null, codeSet);
	}

	public CodeValueEditDialog(Shell parentShell, Long codeValue) {
		this(parentShell, codeValue, null);
	}

	private CodeValueEditDialog(Shell parentShell, Long codeValue, Long codeSet) {
		super(parentShell);
		setShellStyle(SWT.DIALOG_TRIM);

		codeValueDelegate = new CodeValueDelegate();
		if (codeValue == null) {
			recordInContext = new CodeValueRecord();
			recordInContext.setCodeSet(codeSet);
		} else {
			recordInContext = codeValueDelegate.getCodeValueRecord(codeValue);
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		composite = new CodeValueEditComposite(container, SWT.NONE);
		composite.txtDisplay.forceFocus();
		fillComposite();
		addListeners();
		return container;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Save", true);
		createButton(parent, IDialogConstants.CANCEL_ID, "Cancel", false);
	}

	private void addListeners() {
		composite.btnNoCollation.addListener(SWT.Selection,
				e -> composite.spnCollationSeq.setEnabled(!composite.btnNoCollation.getSelection()));
	}

	@Override
	protected void okPressed() {
		if (composite.txtDisplay.getText().isEmpty()) {
			MessageDialogs.displayError(getParentShell(), "Error", "Please populate a display for this coded value.");
			return;
		}
		Short collationSeq = null;
		if (!composite.btnNoCollation.getSelection()) {
			collationSeq = (short) composite.spnCollationSeq.getSelection();
		}
		recordInContext.setCollationSeq(collationSeq);
		recordInContext.setDisplay(composite.txtDisplay.getText());
		codeValueDelegate.persistCodeValue(recordInContext);
		super.okPressed();
	}

	private void fillComposite() {
		if (recordInContext.getCodeValue() == null) {
			composite.btnNoCollation.setSelection(true);
			composite.spnCollationSeq.setEnabled(false);
		} else {
			if (recordInContext.getCollatingSequence() == null) {
				composite.btnNoCollation.setSelection(true);
				composite.spnCollationSeq.setEnabled(false);
			} else {
				composite.btnNoCollation.setSelection(false);
				composite.spnCollationSeq.setSelection(recordInContext.getCollatingSequence());
			}
			composite.txtCodedMeaning.setText(recordInContext.getCdfMeaning());
			composite.txtCodedValue.setText(String.valueOf(recordInContext.getCodeValue()));
			composite.txtDisplay.setText(recordInContext.getDisplay());
		}
	}
}
