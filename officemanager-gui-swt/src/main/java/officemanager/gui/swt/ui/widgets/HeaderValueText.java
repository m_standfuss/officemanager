package officemanager.gui.swt.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

public class HeaderValueText extends HeaderValue<String> {

	private static final int WIDTH_DEFAULT = 150;

	private Text txtValue;

	public HeaderValueText(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public String getEditableValue() {
		return txtValue.getText();
	}

	@Override
	protected void setEditableValue(String value) {
		txtValue.setText(value);
	}

	@Override
	protected int getEditableWidth() {
		return WIDTH_DEFAULT;
	}

	@Override
	protected Control getEditableControl(Composite parent) {
		txtValue = new UpperCaseText(parent, SWT.BORDER);
		return txtValue;
	}
}
