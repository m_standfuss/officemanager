package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class LocationTableComposite extends Composite {

	public static final int COL_LOCATION_NAME = 0;
	public static final int COL_ABBR_NAME = 1;
	public static final int COL_TYPE = 2;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnLocationName;
	public final TableColumn tblclmnAbbrName;
	public final TableColumn tblclmnType;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public LocationTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new FillLayout());

		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnLocationName = new TableColumn(table, SWT.NONE);
		tblclmnLocationName.setWidth(100);
		tblclmnLocationName.setText("Location Name");

		tblclmnAbbrName = new TableColumn(table, SWT.NONE);
		tblclmnAbbrName.setWidth(100);
		tblclmnAbbrName.setText("Abbr. Name");

		tblclmnType = new TableColumn(table, SWT.NONE);
		tblclmnType.setWidth(100);
		tblclmnType.setText("Type");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
