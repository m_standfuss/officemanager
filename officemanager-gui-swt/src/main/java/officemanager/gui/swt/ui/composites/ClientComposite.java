package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.AddressTableComposite;
import officemanager.gui.swt.ui.tables.ClientProductTableComposite;
import officemanager.gui.swt.ui.tables.OrderTableComposite;
import officemanager.gui.swt.ui.tables.PhoneTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class ClientComposite extends Composite {

	public final ToolBar toolBar;
	public final ToolItem tlItmEditClient;

	public final ScrolledComposite scrComposite;

	public final Composite composite;

	public final CollapsibleComposite clientInfoCllpCmp;
	public final ClientInformationComposite clientInfoComposite;

	public final CollapsibleComposite phoneCllpCmp;
	public final PhoneTableComposite phoneTblCmp;

	public final CollapsibleComposite addressCllpCmp;
	public final AddressTableComposite addressTblCmp;

	public final CollapsibleComposite currentOrdersCllpCmp;
	public final OrderTableComposite currentOrdersTblCmp;

	public final CollapsibleComposite pastOrdersCllpCmp;
	public final OrderTableComposite pastOrdersTblCmp;

	public final CollapsibleComposite productCllpCmp;
	public final ClientProductTableComposite productTblCmp;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ClientComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		tlItmEditClient = new ToolItem(toolBar, SWT.NONE);
		tlItmEditClient.setImage(ResourceManager.getImage(ImageFile.USER_EDIT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmEditClient.setToolTipText("Edit Client");
		tlItmEditClient.setEnabled(AppPrefs.hasPermission(AppPermissions.EDIT_CLIENT));

		scrComposite = new ScrolledComposite(this, SWT.V_SCROLL);
		scrComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrComposite.setExpandHorizontal(true);
		scrComposite.setExpandVertical(true);

		composite = new Composite(scrComposite, SWT.NONE);
		composite.setLayout(new GridLayout());

		clientInfoCllpCmp = new CollapsibleComposite(composite, SWT.NONE);
		clientInfoCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		clientInfoCllpCmp.setTitleText("Client Information");
		clientInfoCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.USER_INFO, AppPrefs.ICN_SIZE_CLPCMP));
		clientInfoCllpCmp.composite.setLayout(new GridLayout(1, false));
		clientInfoComposite = new ClientInformationComposite(clientInfoCllpCmp.composite, SWT.NONE);

		phoneCllpCmp = new CollapsibleComposite(composite, SWT.NONE);
		phoneCllpCmp.setTitleText("Phones");
		phoneCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		phoneCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.PHONE, AppPrefs.ICN_SIZE_CLPCMP));
		phoneTblCmp = new PhoneTableComposite(phoneCllpCmp.composite, SWT.NONE, SWT.FULL_SELECTION);

		addressCllpCmp = new CollapsibleComposite(composite, SWT.NONE);
		addressCllpCmp.setTitleText("Addresses");
		addressCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		addressCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.ADDRESS, AppPrefs.ICN_SIZE_CLPCMP));
		addressTblCmp = new AddressTableComposite(addressCllpCmp.composite, SWT.NONE, SWT.FULL_SELECTION);

		currentOrdersCllpCmp = new CollapsibleComposite(composite, SWT.NONE);
		currentOrdersCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		currentOrdersCllpCmp.setTitleText("Current Orders");
		currentOrdersCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.ORDER, AppPrefs.ICN_SIZE_CLPCMP));
		currentOrdersTblCmp = new OrderTableComposite(currentOrdersCllpCmp.composite, SWT.NONE,
				SWT.FULL_SELECTION);

		pastOrdersCllpCmp = new CollapsibleComposite(composite, SWT.NONE);
		pastOrdersCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		pastOrdersCllpCmp.setTitleText("Past Orders (30 days)");
		pastOrdersCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.ORDER, AppPrefs.ICN_SIZE_CLPCMP));
		pastOrdersTblCmp = new OrderTableComposite(pastOrdersCllpCmp.composite, SWT.NONE, SWT.FULL_SELECTION);

		productCllpCmp = new CollapsibleComposite(composite, SWT.NONE);
		productCllpCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		productCllpCmp.setTitleText("Products");
		productCllpCmp.setTitleImage(ResourceManager.getImage(ImageFile.PRODUCT, AppPrefs.ICN_SIZE_CLPCMP));
		productTblCmp = new ClientProductTableComposite(productCllpCmp.composite, SWT.BORDER, SWT.FULL_SELECTION);

		composite.addListener(SWT.Resize,
				e -> scrComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		clientInfoCllpCmp.addCollapseListener(
				e -> scrComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		phoneCllpCmp.addCollapseListener(
				e -> scrComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		addressCllpCmp.addCollapseListener(
				e -> scrComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		pastOrdersCllpCmp.addCollapseListener(
				e -> scrComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));
		currentOrdersCllpCmp
		.addCollapseListener(
				e -> scrComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y));

		scrComposite.setContent(composite);
		scrComposite.setMinSize(-1, composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
