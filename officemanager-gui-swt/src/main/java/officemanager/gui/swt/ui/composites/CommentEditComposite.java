package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class CommentEditComposite extends Composite {
	public Label lblType;
	public Label lblAuthor;
	public Label lblLastUpdatedOn;
	public Label lblLastUpdatedBy;
	public CodeValueCombo cmbCommentType;
	public UpperCaseText txtAuthor;
	public Text txtLastUpdatedOn;
	public UpperCaseText txtLastUpdatedBy;
	public Label lblComment_1;
	public UpperCaseText txtComment;
	public Label lbldenotesARequired;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public CommentEditComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 10;
		gridLayout.numColumns = 4;
		setLayout(gridLayout);

		lblType = new Label(this, SWT.NONE);
		lblType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblType.setText("Type*:");
		lblType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbCommentType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbCommentType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbCommentType.widthHint = 125;
		cmbCommentType.setLayoutData(gd_cmbCommentType);

		lblComment_1 = new Label(this, SWT.NONE);
		lblComment_1.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 4));
		lblComment_1.setText("Comment:");
		lblComment_1.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtComment = new UpperCaseText(this, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtComment = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 4);
		gd_txtComment.heightHint = 150;
		gd_txtComment.widthHint = 200;
		txtComment.setLayoutData(gd_txtComment);

		lblAuthor = new Label(this, SWT.NONE);
		lblAuthor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAuthor.setText("Author:");
		lblAuthor.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtAuthor = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtAuthor = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAuthor.widthHint = 125;
		txtAuthor.setLayoutData(gd_txtAuthor);

		lblLastUpdatedOn = new Label(this, SWT.NONE);
		lblLastUpdatedOn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLastUpdatedOn.setText("Last Updated On:");
		lblLastUpdatedOn.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtLastUpdatedOn = new Text(this, SWT.BORDER);
		GridData gd_txtLastUpdatedOn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtLastUpdatedOn.widthHint = 125;
		txtLastUpdatedOn.setLayoutData(gd_txtLastUpdatedOn);

		lblLastUpdatedBy = new Label(this, SWT.NONE);
		lblLastUpdatedBy.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblLastUpdatedBy.setText("Last Updated By:");
		lblLastUpdatedBy.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtLastUpdatedBy = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtLastUpdatedBy = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_txtLastUpdatedBy.widthHint = 125;
		txtLastUpdatedBy.setLayoutData(gd_txtLastUpdatedBy);

		lbldenotesARequired = new Label(this, SWT.NONE);
		lbldenotesARequired.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		lbldenotesARequired.setText("*Denotes a required field");
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
