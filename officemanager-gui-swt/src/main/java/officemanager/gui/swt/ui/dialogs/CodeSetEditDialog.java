package officemanager.gui.swt.ui.dialogs;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import officemanager.biz.delegates.CodeValueDelegate;
import officemanager.biz.delegates.Delegate;
import officemanager.biz.records.CodeValueRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.composites.CodeSetEditComposite;
import officemanager.gui.swt.ui.tables.CodeValueEditTableComposite;
import officemanager.gui.swt.util.MessageDialogs;

public class CodeSetEditDialog extends Dialog {

	private final Long codeSet;
	private final CodeValueDelegate codeValueDelegate;

	private CodeSetEditComposite composite;
	private TableViewer tableViewer;

	public CodeSetEditDialog(Shell parentShell, Long codeSet) {
		super(parentShell);
		this.codeSet = codeSet;
		codeValueDelegate = new CodeValueDelegate();
		setShellStyle(SWT.DIALOG_TRIM);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));

		composite = new CodeSetEditComposite(container, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));

		addListeners();
		addTableViewer();

		composite.codeValueEditTableComposite.table.packTableColumns(
				Lists.newArrayList(CodeValueEditTableComposite.COL_ACTIVATE, CodeValueEditTableComposite.COL_EDIT));

		return container;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID, "Close", true);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(750, 400);
	}

	private void addListeners() {
		composite.tlItmAddCodeValue.addListener(SWT.Selection, e -> {
			CodeValueEditDialog dialog = new CodeValueEditDialog(getParentShell(), codeSet, null);
			if (dialog.open() == Window.OK) {
				Delegate.clearCodeValueCache();
				tableViewer.setInput(codeValueDelegate.getCodeValueRecords(codeSet));
			}
		});
	}

	private void addTableViewer() {
		tableViewer = new TableViewer(composite.codeValueEditTableComposite.table);

		TableViewerColumn displayColumn = new TableViewerColumn(tableViewer,
				composite.codeValueEditTableComposite.tblclmnDisplay);
		displayColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				CodeValueRecord record = (CodeValueRecord) element;
				return record.getDisplay();
			}
		});

		TableViewerColumn codeValueColumn = new TableViewerColumn(tableViewer,
				composite.codeValueEditTableComposite.tblclmnCodeValue);
		codeValueColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				CodeValueRecord record = (CodeValueRecord) element;
				return String.valueOf(record.getCodeValue());
			}
		});

		TableViewerColumn cdfMeaningColumn = new TableViewerColumn(tableViewer,
				composite.codeValueEditTableComposite.tblclmnCdfMeaning);
		cdfMeaningColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				CodeValueRecord record = (CodeValueRecord) element;
				return record.getCdfMeaning();
			}
		});

		TableViewerColumn activeColumn = new TableViewerColumn(tableViewer,
				composite.codeValueEditTableComposite.tblclmnActive);
		activeColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				CodeValueRecord record = (CodeValueRecord) element;
				return Boolean.toString(record.isActive());
			}
		});

		TableViewerColumn updtDttmColumn = new TableViewerColumn(tableViewer,
				composite.codeValueEditTableComposite.tblclmnUpdtDttm);
		updtDttmColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				CodeValueRecord record = (CodeValueRecord) element;
				return AppPrefs.dateFormatter.formatFullDate_DB(record.getUpdatedOn());
			}
		});

		TableViewerColumn editColumn = new TableViewerColumn(tableViewer,
				composite.codeValueEditTableComposite.tblclmEdit);
		editColumn.setLabelProvider(new ColumnLabelProvider() {
			Map<Object, Button> buttons = new HashMap<Object, Button>();

			@Override
			public void update(ViewerCell cell) {
				TableItem item = (TableItem) cell.getItem();
				CodeValueRecord record = (CodeValueRecord) cell.getElement();

				Button btnEdit;
				if (buttons.containsKey(cell.getItem())) {
					btnEdit = buttons.get(cell.getItem());
				} else {
					btnEdit = new Button((Composite) cell.getViewerRow().getControl(), SWT.NONE);
					btnEdit.setText("Edit");
					btnEdit.addListener(SWT.Selection, e -> editCodeValue(record));
					btnEdit.pack();
					buttons.put(cell.getItem(), btnEdit);
				}

				TableEditor editor = new TableEditor(item.getParent());
				editor.grabHorizontal = true;
				editor.grabVertical = true;
				editor.setEditor(btnEdit, item, cell.getColumnIndex());
				editor.layout();
			}
		});

		TableViewerColumn activateColumn = new TableViewerColumn(tableViewer,
				composite.codeValueEditTableComposite.tblclmActivate);
		activateColumn.setLabelProvider(new ColumnLabelProvider() {
			Map<Object, Button> buttons = Maps.newHashMap();
			Map<Object, Listener> listeners = Maps.newHashMap();

			@Override
			public void update(ViewerCell cell) {
				TableItem item = (TableItem) cell.getItem();
				CodeValueRecord record = (CodeValueRecord) cell.getElement();

				Button btnRemove;
				if (buttons.containsKey(cell.getItem())) {
					btnRemove = buttons.get(cell.getItem());
					btnRemove.removeListener(SWT.Selection, listeners.get(cell.getItem()));
				} else {
					btnRemove = new Button(composite.codeValueEditTableComposite.table, SWT.PUSH);
					buttons.put(cell.getItem(), btnRemove);
				}

				Listener l = e -> activateCodeValue(record);
				btnRemove.addListener(SWT.Selection, l);
				listeners.put(cell.getItem(), l);
				if (record.isActive()) {
					btnRemove.setText("Remove");
				} else {
					btnRemove.setText("Reactivate");

				}
				btnRemove.pack();

				TableEditor editor = new TableEditor(item.getParent());
				editor.grabHorizontal = true;
				editor.grabVertical = true;
				editor.setEditor(btnRemove, item, cell.getColumnIndex());
				editor.layout();
			}
		});

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setInput(codeValueDelegate.getCodeValueRecords(codeSet));
	}

	private void activateCodeValue(CodeValueRecord record) {
		if (record.isActive()) {
			removeCodeValue(record);
		} else {
			reactivateCodeValue(record);
		}
	}

	private void reactivateCodeValue(CodeValueRecord record) {
		codeValueDelegate.reactivateCodeValue(record.getCodeValue());
		tableViewer.setInput(codeValueDelegate.getCodeValueRecords(codeSet));
	}

	private void removeCodeValue(CodeValueRecord record) {
		if (!record.getCdfMeaning().isEmpty()) {
			MessageDialogs.displayError(getParentShell(), "Cannot Remove",
					"This code value is one that cannot be removed.");
			return;
		}
		codeValueDelegate.inactivateCodeValue(record.getCodeValue());
		tableViewer.setInput(codeValueDelegate.getCodeValueRecords(codeSet));
	}

	private void editCodeValue(CodeValueRecord record) {
		CodeValueEditDialog dialog = new CodeValueEditDialog(getParentShell(), record.getCodeValue());
		if (dialog.open() == Window.OK) {
			Delegate.clearCodeValueCache();
			tableViewer.setInput(codeValueDelegate.getCodeValueRecords(codeSet));
		}
	}
}
