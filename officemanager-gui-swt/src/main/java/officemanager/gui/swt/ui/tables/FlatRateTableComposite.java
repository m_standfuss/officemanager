package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class FlatRateTableComposite extends Composite {

	public static final int COL_SERVICE_NAME = 0;
	public static final int COL_PRODUCT_TYPE = 1;
	public static final int COL_CATEGORY = 2;
	public static final int COL_CURRENT_PRICE = 3;
	public static final int COL_BASE_PRICE = 4;
	public static final int COL_AMT_SOLD = 5;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnServiceName;
	public final TableColumn tblclmnProductType;
	public final TableColumn tblclmnCategory;
	public final TableColumn tblclmnPrice;
	public final TableColumn tblclmnBasePrice;
	public final TableColumn tblclmnAmountSold;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public FlatRateTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new FillLayout());

		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnServiceName = new TableColumn(table, SWT.NONE);
		tblclmnServiceName.setWidth(100);
		tblclmnServiceName.setText("Service Name");

		tblclmnProductType = new TableColumn(table, SWT.NONE);
		tblclmnProductType.setWidth(100);
		tblclmnProductType.setText("Product Type");

		tblclmnCategory = new TableColumn(table, SWT.NONE);
		tblclmnCategory.setWidth(100);
		tblclmnCategory.setText("Category");

		tblclmnPrice = new TableColumn(table, SWT.NONE);
		tblclmnPrice.setWidth(100);
		tblclmnPrice.setText("Current Price");

		tblclmnBasePrice = new TableColumn(table, SWT.NONE);
		tblclmnBasePrice.setWidth(100);
		tblclmnBasePrice.setText("Base Price");

		tblclmnAmountSold = new TableColumn(table, SWT.NONE);
		tblclmnAmountSold.setWidth(100);
		tblclmnAmountSold.setText("Amount Sold");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
