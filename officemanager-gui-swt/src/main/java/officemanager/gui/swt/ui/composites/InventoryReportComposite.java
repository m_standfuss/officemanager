package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class InventoryReportComposite extends Composite {
	public final ToolBar toolBar;
	public final ToolItem tlItmPrint;

	public final ScrolledComposite scrCompositeCenter;
	public final Composite composite;

	public final CollapsibleComposite cllpCmpOutOfStock;
	public final PartSearchTableComposite tblOutOfStock;

	public final CollapsibleComposite cllpCmpLowStock;
	public final PartSearchTableComposite tblLowStock;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public InventoryReportComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout());

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		tlItmPrint = new ToolItem(toolBar, SWT.NONE);
		tlItmPrint.setImage(ResourceManager.getImage(ImageFile.PRINT, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmPrint.setToolTipText("Print Report");

		scrCompositeCenter = new ScrolledComposite(this, SWT.BORDER | SWT.V_SCROLL);
		scrCompositeCenter.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrCompositeCenter.setExpandHorizontal(true);
		scrCompositeCenter.setExpandVertical(true);

		composite = new Composite(scrCompositeCenter, SWT.NONE);
		composite.setLayout(new GridLayout(1, true));

		addListener(SWT.Resize, e -> scrCompositeCenter.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));

		cllpCmpOutOfStock = new CollapsibleComposite(composite, SWT.NONE);
		cllpCmpOutOfStock.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cllpCmpOutOfStock.setTitleText("Out of Stock");
		cllpCmpOutOfStock.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));

		tblOutOfStock = new PartSearchTableComposite(cllpCmpOutOfStock.composite, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION);

		cllpCmpLowStock = new CollapsibleComposite(composite, SWT.NONE);
		cllpCmpLowStock.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cllpCmpLowStock.setTitleText("Low Stock");
		cllpCmpLowStock.addListener(SWT.Resize,
				e -> scrCompositeCenter.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT)));

		tblLowStock = new PartSearchTableComposite(cllpCmpLowStock.composite, SWT.NONE,
				SWT.BORDER | SWT.FULL_SELECTION);

		scrCompositeCenter.setContent(composite);
		scrCompositeCenter.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
