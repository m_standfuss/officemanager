package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class OrderTableComposite extends Composite {
	public static final int COL_ORDER_NUM = 0;
	public static final int COL_ORDER_TYPE = 1;
	public static final int COL_CLIENT_NAME = 2;
	public static final int COL_CLIENT_PHONE = 3;
	public static final int COL_AMOUNT = 4;
	public static final int COL_ORDER_STATUS = 5;
	public static final int COL_OPENED_DTTM = 6;
	public static final int COL_OPENED_BY = 7;
	public static final int COL_PRODUCT_CNT = 8;
	public static final int COL_CLOSED_DTTM = 9;
	public static final int COL_CLOSED_BY = 10;

	public final OfficeManagerTable table;
	public final TableColumn columnOrderNumber;
	public final TableColumn columnOrderType;
	public final TableColumn columnClientName;
	public final TableColumn columnClientPhone;
	public final TableColumn columnAmount;
	public final TableColumn columnOrderStatus;
	public final TableColumn columnOpenedDate;
	public final TableColumn columnOpenedBy;
	public final TableColumn columnProductCount;
	public final TableColumn columnClosedDate;
	public final TableColumn columnClosedBy;

	public OrderTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new GridLayout());

		table = new OfficeManagerTable(this, tableStyle);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		columnOrderNumber = new TableColumn(table, SWT.NONE);
		columnOrderNumber.setWidth(100);
		columnOrderNumber.setText("Order Number");

		columnOrderType = new TableColumn(table, SWT.NONE);
		columnOrderType.setWidth(100);
		columnOrderType.setText("Order Type");

		columnClientName = new TableColumn(table, SWT.NONE);
		columnClientName.setWidth(100);
		columnClientName.setText("Client Name");

		columnClientPhone = new TableColumn(table, SWT.NONE);
		columnClientPhone.setWidth(100);
		columnClientPhone.setText("Phone");

		columnAmount = new TableColumn(table, SWT.NONE);
		columnAmount.setWidth(100);
		columnAmount.setText("Amount");

		columnOrderStatus = new TableColumn(table, SWT.NONE);
		columnOrderStatus.setWidth(100);
		columnOrderStatus.setText("Order Status");

		columnOpenedDate = new TableColumn(table, SWT.NONE);
		columnOpenedDate.setWidth(100);
		columnOpenedDate.setText("Opened Date");

		columnOpenedBy = new TableColumn(table, SWT.NONE);
		columnOpenedBy.setWidth(100);
		columnOpenedBy.setText("Opened By");

		columnProductCount = new TableColumn(table, SWT.NONE);
		columnProductCount.setWidth(100);
		columnProductCount.setText("Product Count");

		columnClosedDate = new TableColumn(table, SWT.NONE);
		columnClosedDate.setWidth(100);
		columnClosedDate.setText("Closed Date");

		columnClosedBy = new TableColumn(table, SWT.NONE);
		columnClosedBy.setWidth(100);
		columnClosedBy.setText("Closed By");
	}
}
