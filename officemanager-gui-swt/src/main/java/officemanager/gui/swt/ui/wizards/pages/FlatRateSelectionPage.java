package officemanager.gui.swt.ui.wizards.pages;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.Lists;

import officemanager.biz.records.FlatRateSearchRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.FlatRateSearchService;
import officemanager.gui.swt.ui.composites.FlatRateSearchComposite;
import officemanager.gui.swt.ui.tables.FlatRateTableComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class FlatRateSelectionPage extends OfficeManagerWizardPage {

	private static final List<Integer> COLUMNS_TO_KEEP = Lists.newArrayList(FlatRateTableComposite.COL_SERVICE_NAME,
			FlatRateTableComposite.COL_PRODUCT_TYPE, FlatRateTableComposite.COL_CATEGORY,
			FlatRateTableComposite.COL_CURRENT_PRICE, FlatRateTableComposite.COL_BASE_PRICE,
			FlatRateTableComposite.COL_AMT_SOLD);

	private final boolean removeCurrentPrice;
	private final int maxSelection;
	private final boolean needsSelection;
	private final FlatRateSearchService flatRateSearchService;

	private FlatRateSearchComposite flatRateSearchComposite;

	/**
	 * Create the wizard.
	 * 
	 * @wbp.parser.constructor
	 */
	public FlatRateSelectionPage() {
		this(1, true, false);
	}

	public FlatRateSelectionPage(int maxSelection, boolean needsSelection, boolean removeCurrentPrice) {
		super("flatRateSelectionPage", "Flat Rate Selection", ImageDescriptor
				.createFromImage(ResourceManager.getImage(ImageFile.SERVICE, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		checkArgument(maxSelection > 0, "The max selection must be greater than 0.");

		setPageComplete(!needsSelection);

		if (maxSelection == 1) {
			setDescription("Please select a flat rate service.");
		} else {
			setDescription("Please select flat rate services.");
		}
		List<Integer> columnsToKeep = Lists.newArrayList(COLUMNS_TO_KEEP);
		if (removeCurrentPrice) {
			columnsToKeep.remove(FlatRateTableComposite.COL_CURRENT_PRICE);
		}
		flatRateSearchService = new FlatRateSearchService(columnsToKeep);
		this.removeCurrentPrice = removeCurrentPrice;
		this.maxSelection = maxSelection;
		this.needsSelection = needsSelection;
	}

	/**
	 * Create contents of the wizard.
	 * 
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		flatRateSearchComposite = new FlatRateSearchComposite(container, SWT.NONE,
				SWT.BORDER | SWT.CHECK | SWT.FULL_SELECTION);
		flatRateSearchComposite.hideToolItems(true);
		flatRateSearchComposite.searchResultsTable.table.setMaxCheckedItems(maxSelection);
		flatRateSearchComposite.searchResultsTable.table.addCheckListener(e -> {
			boolean isPageComplete = !needsSelection
					|| flatRateSearchComposite.searchResultsTable.table.getCheckedCount() > 0;
					setPageComplete(isPageComplete);
		});

		if (removeCurrentPrice) {
			flatRateSearchComposite.lblDenotesCurrentSale.setText("");
		}
		flatRateSearchService.setComposite(flatRateSearchComposite);

		setControl(container);
	}

	@Override
	public void setFocus() {
		flatRateSearchComposite.txtServiceName.setFocus();
	}

	public List<FlatRateSearchRecord> getSelectedRecords() {
		return flatRateSearchComposite.searchResultsTable.table.getCheckedData(FlatRateSearchService.DATA_RECORD);
	}
}
