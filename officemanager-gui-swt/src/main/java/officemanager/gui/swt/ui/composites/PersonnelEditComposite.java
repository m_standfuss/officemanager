package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.util.ResourceManager;

public class PersonnelEditComposite extends Composite {
	public Label lblPersonnelType;
	public CodeValueCombo cmbPersonnelType;
	public Label lblPersonnelStatus;
	public CodeValueCombo cmbPersonnelStatus;
	private Label lblDenotesA;
	public Label lblUsername;
	public Text txtUsername;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PersonnelEditComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		setLayout(new GridLayout(2, false));

		lblPersonnelType = new Label(this, SWT.NONE);
		lblPersonnelType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPersonnelType.setText("Personnel Type:*");
		lblPersonnelType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbPersonnelType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbPersonnelType = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbPersonnelType.widthHint = 200;
		cmbPersonnelType.setLayoutData(gd_cmbPersonnelType);

		lblPersonnelStatus = new Label(this, SWT.NONE);
		lblPersonnelStatus.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPersonnelStatus.setText("Personnel Status:*");
		lblPersonnelStatus.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbPersonnelStatus = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbPersonnelStatus = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbPersonnelStatus.widthHint = 200;
		cmbPersonnelStatus.setLayoutData(gd_cmbPersonnelStatus);

		lblUsername = new Label(this, SWT.NONE);
		lblUsername.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblUsername.setText("Username:*");
		lblUsername.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtUsername = new Text(this, SWT.BORDER);
		GridData gd_text = new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1);
		gd_text.widthHint = 150;
		txtUsername.setLayoutData(gd_text);

		lblDenotesA = new Label(this, SWT.NONE);
		lblDenotesA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblDenotesA.setText("* Denotes a required field");
	}

}
