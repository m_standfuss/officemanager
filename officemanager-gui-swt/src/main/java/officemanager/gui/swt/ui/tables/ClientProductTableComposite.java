package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class ClientProductTableComposite extends Composite {
	public static final int COL_MANUFACTURER = 0;
	public static final int COL_MODEL = 1;
	public static final int COL_PRODUCT = 2;
	public static final int COL_SERIAL_NUM = 3;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnManufacturer;
	public final TableColumn tblclmnModel;
	public final TableColumn tblclmnProduct;
	public final TableColumn tblclmnSerialNumber;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ClientProductTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));

		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnProduct = new TableColumn(table, SWT.NONE);
		tblclmnProduct.setWidth(100);
		tblclmnProduct.setText("Product");

		tblclmnManufacturer = new TableColumn(table, SWT.NONE);
		tblclmnManufacturer.setWidth(100);
		tblclmnManufacturer.setText("Manufacturer");

		tblclmnModel = new TableColumn(table, SWT.NONE);
		tblclmnModel.setWidth(100);
		tblclmnModel.setText("Model");

		tblclmnSerialNumber = new TableColumn(table, SWT.NONE);
		tblclmnSerialNumber.setWidth(100);
		tblclmnSerialNumber.setText("Serial Number");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
