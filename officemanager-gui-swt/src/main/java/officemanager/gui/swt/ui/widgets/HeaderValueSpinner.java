package officemanager.gui.swt.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Spinner;

public class HeaderValueSpinner extends HeaderValue<Integer> {

	private Spinner spn;

	public HeaderValueSpinner(Composite parent, int style) {
		super(parent, style);
		setValue(1);
	}

	@Override
	public Integer getEditableValue() {
		return spn.getSelection();
	}

	@Override
	protected void setEditableValue(Integer value) {
		spn.setSelection(value);
	}

	@Override
	protected Control getEditableControl(Composite parent) {
		spn = new Spinner(parent, SWT.BORDER);
		return spn;
	}
}
