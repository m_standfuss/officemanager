package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class LocationTreeComposite extends Composite {
	public Tree treeLocations;
	public ToolBar toolBar;
	public ToolItem tltmNewLocation;

	public LocationTreeComposite(Composite parent, int style) {
		this(parent, style, SWT.BORDER | SWT.SINGLE);
	}

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public LocationTreeComposite(Composite parent, int style, int treeStyle) {
		super(parent, style);
		setLayout(new GridLayout());

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		tltmNewLocation = new ToolItem(toolBar, SWT.NONE);
		tltmNewLocation.setImage(ResourceManager.getImage(ImageFile.CRATE_NEW, AppPrefs.ICN_SIZE_PAGETOOLITEM));

		treeLocations = new Tree(this, treeStyle);
		treeLocations.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	public void showToolbar(boolean show) {
		((GridData) toolBar.getLayoutData()).exclude = !show;
		toolBar.setVisible(show);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
