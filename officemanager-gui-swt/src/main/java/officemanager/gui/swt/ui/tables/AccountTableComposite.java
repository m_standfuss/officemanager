package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class AccountTableComposite extends Composite {

	public static final int COL_ACCNT_NUMBER = 0;
	public static final int COL_NAME = 1;
	public static final int COL_STATUS = 2;
	public static final int COL_PHONE = 3;
	public static final int COL_EMAIL = 4;

	public OfficeManagerTable table;
	public TableColumn tblclmnAcctNum;
	public TableColumn tblclmnName;
	public TableColumn tblclmnStatus;
	public TableColumn tblclmnPhone;
	public TableColumn tblclmnEmail;

	public AccountTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new FillLayout());
		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnAcctNum = new TableColumn(table, SWT.NONE);
		tblclmnAcctNum.setWidth(100);
		tblclmnAcctNum.setText("Account #");

		tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(100);
		tblclmnName.setText("Name");

		tblclmnStatus = new TableColumn(table, SWT.NONE);
		tblclmnStatus.setWidth(100);
		tblclmnStatus.setText("Status");

		tblclmnPhone = new TableColumn(table, SWT.NONE);
		tblclmnPhone.setWidth(100);
		tblclmnPhone.setText("Phone");

		tblclmnEmail = new TableColumn(table, SWT.NONE);
		tblclmnEmail.setWidth(100);
		tblclmnEmail.setText("Email");
	}
}
