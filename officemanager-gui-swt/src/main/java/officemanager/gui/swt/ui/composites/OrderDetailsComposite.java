package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.tables.ClientProductTableComposite;
import officemanager.gui.swt.util.ResourceManager;

public class OrderDetailsComposite extends Composite {
	public final Label lblFlatRateServices;
	public final Label lblNoFlatRate;
	public final Label lblServices;
	public final Label lblNoServicesHave;
	public final Label lblParts;
	public final Label lblNoPartsHave;
	public final Composite cmpOrderProducts;
	public final Label lblCurrentOrderProducts;
	public final ClientProductTableComposite clientProductTableComposite;
	public final Label lblAllServices;
	public final Button btnAddProductFor;
	public final Composite cmpServiceDetails;
	public final Composite cmpFlatRateDetails;
	public final Composite cmpPartDetails;
	public final Composite cmpServices;
	public final Composite cmpFlatRates;
	public final Composite cmpParts;
	public final Label lblService;
	public final Label lblServiceFlatRate;
	public final Label lblPrice;
	public final Label lblPartFlatRate;
	public final Label lblClientProductPart;
	public final Label lblClientProductFlatRate;
	public final Label lblClientProductService;
	public final Label lblPricePart;
	public final Label lblQuantity;
	public final Label lblSeperator;

	public OrderDetailsComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		cmpOrderProducts = new Composite(this, SWT.NONE);
		cmpOrderProducts.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		cmpOrderProducts.setLayout(new GridLayout(1, false));

		lblCurrentOrderProducts = new Label(cmpOrderProducts, SWT.NONE);
		lblCurrentOrderProducts.setText("Current Order Products");
		lblCurrentOrderProducts.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		clientProductTableComposite = new ClientProductTableComposite(cmpOrderProducts, SWT.NONE,
				SWT.SINGLE | SWT.FULL_SELECTION | SWT.BORDER);
		clientProductTableComposite.setLayout(new GridLayout(1, false));

		btnAddProductFor = new Button(cmpOrderProducts, SWT.NONE);
		btnAddProductFor.setText("Add Product For All Items");

		cmpServices = new Composite(this, SWT.NONE);
		GridData gd_cmpServices = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_cmpServices.exclude = true;
		cmpServices.setLayoutData(gd_cmpServices);
		cmpServices.setLayout(new GridLayout(1, false));

		lblServices = new Label(cmpServices, SWT.NONE);
		lblServices.setText("Services*");
		lblServices.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblNoServicesHave = new Label(cmpServices, SWT.NONE);
		lblNoServicesHave.setText("No Services Have Been Selected");
		lblNoServicesHave.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		cmpServiceDetails = new Composite(cmpServices, SWT.NONE);
		cmpServiceDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cmpServiceDetails.setLayout(new GridLayout(3, false));

		lblService = new Label(cmpServiceDetails, SWT.NONE);
		lblService.setText("Service");
		lblService.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		lblClientProductService = new Label(cmpServiceDetails, SWT.NONE);
		lblClientProductService.setText("Client Product");
		lblClientProductService.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		new Label(cmpServiceDetails, SWT.NONE);

		cmpFlatRates = new Composite(this, SWT.NONE);
		GridData gd_cmpFlatRates = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_cmpFlatRates.exclude = true;
		cmpFlatRates.setLayoutData(gd_cmpFlatRates);
		cmpFlatRates.setLayout(new GridLayout(1, false));

		lblFlatRateServices = new Label(cmpFlatRates, SWT.NONE);
		lblFlatRateServices.setText("Flat Rate Services*");
		lblFlatRateServices.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblNoFlatRate = new Label(cmpFlatRates, SWT.NONE);
		lblNoFlatRate.setText("No Flat Rate Services Have Been Selected");
		lblNoFlatRate.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		cmpFlatRateDetails = new Composite(cmpFlatRates, SWT.NONE);
		cmpFlatRateDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cmpFlatRateDetails.setLayout(new GridLayout(5, false));

		lblServiceFlatRate = new Label(cmpFlatRateDetails, SWT.NONE);
		lblServiceFlatRate.setText("Service");
		lblServiceFlatRate.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		lblClientProductFlatRate = new Label(cmpFlatRateDetails, SWT.NONE);
		lblClientProductFlatRate.setText("Client Product");
		lblClientProductFlatRate.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		lblPrice = new Label(cmpFlatRateDetails, SWT.NONE);
		lblPrice.setText("Price");
		lblPrice.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		new Label(cmpFlatRateDetails, SWT.NONE);
		new Label(cmpFlatRateDetails, SWT.NONE);

		cmpParts = new Composite(this, SWT.NONE);
		GridData gd_cmpParts = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_cmpParts.exclude = true;
		cmpParts.setLayoutData(gd_cmpParts);
		cmpParts.setLayout(new GridLayout(1, false));

		lblParts = new Label(cmpParts, SWT.NONE);
		lblParts.setText("Parts");
		lblParts.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblNoPartsHave = new Label(cmpParts, SWT.NONE);
		lblNoPartsHave.setText("No Parts Have Been Selected");

		cmpPartDetails = new Composite(cmpParts, SWT.NONE);
		cmpPartDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		cmpPartDetails.setLayout(new GridLayout(6, false));

		lblPartFlatRate = new Label(cmpPartDetails, SWT.NONE);
		lblPartFlatRate.setText("Part");
		lblPartFlatRate.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		lblClientProductPart = new Label(cmpPartDetails, SWT.NONE);
		lblClientProductPart.setText("Client Product");
		lblClientProductPart.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		lblQuantity = new Label(cmpPartDetails, SWT.NONE);
		lblQuantity.setText("Quantity");
		lblQuantity.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		lblPricePart = new Label(cmpPartDetails, SWT.NONE);
		lblPricePart.setText("Price");
		lblPricePart.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));

		new Label(cmpPartDetails, SWT.NONE);
		new Label(cmpPartDetails, SWT.NONE);

		lblSeperator = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblSeperator.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		lblAllServices = new Label(this, SWT.NONE);
		lblAllServices.setText("* All services must be assigned to a client product");
		lblAllServices.setLayoutData(new GridData());
	}
}
