package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class SaleTableComposite extends Composite {

	public static final int COL_ENTITY_NAME = 0;
	public static final int COL_SALE_NAME = 1;
	public static final int COL_SALE_TYPE = 2;
	public static final int COL_AMOUNT_OFF = 3;
	public static final int COL_BEGIN_DTTM = 4;
	public static final int COL_END_DTTM = 5;

	public final OfficeManagerTable table;
	public final TableColumn tblclmnEntityName;
	public final TableColumn tblclmnSaleType;
	public final TableColumn tblclmnAmountOff;
	public final TableColumn tblclmnBeginDate;
	public final TableColumn tblclmnEndDate;
	public TableColumn tblclmnName;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public SaleTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);
		setLayout(new FillLayout());

		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnEntityName = new TableColumn(table, SWT.NONE);
		tblclmnEntityName.setWidth(100);
		tblclmnEntityName.setText("Sale Item");

		tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(100);
		tblclmnName.setText("Name");

		tblclmnSaleType = new TableColumn(table, SWT.NONE);
		tblclmnSaleType.setWidth(100);
		tblclmnSaleType.setText("Sale Type");

		tblclmnAmountOff = new TableColumn(table, SWT.NONE);
		tblclmnAmountOff.setWidth(100);
		tblclmnAmountOff.setText("Amount Off");

		tblclmnBeginDate = new TableColumn(table, SWT.NONE);
		tblclmnBeginDate.setWidth(100);
		tblclmnBeginDate.setText("Begin Date");

		tblclmnEndDate = new TableColumn(table, SWT.NONE);
		tblclmnEndDate.setWidth(100);
		tblclmnEndDate.setText("End Date");
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
