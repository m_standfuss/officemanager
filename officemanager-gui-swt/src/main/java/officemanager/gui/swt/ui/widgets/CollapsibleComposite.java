package officemanager.gui.swt.ui.widgets;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.mihalis.opal.titledSeparator.TitledSeparator;

import com.google.common.collect.Lists;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class CollapsibleComposite extends Composite {
	private static final Image ARROW_UP = ResourceManager.getImage(ImageFile.ARROW_UP, 16);
	private static final Image ARROW_DOWN = ResourceManager.getImage(ImageFile.ARROW_DOWN, 16);
	private static final Color COLOR_BACKGROUND = AppPrefs.COLOR_COLLAPSIBLE_HEADER;
	private static final Color COLOR_FOREGROUND = AppPrefs.COLOR_COLLAPSIBLE_HEADER_TEXT;

	private final TitledSeparator titledSeparator;
	public final ToolBar toolBar;
	public final ToolItem tltmArrowItem;

	private final List<Listener> collapseListeners;
	public Composite composite;
	private boolean compositeEnabled;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public CollapsibleComposite(Composite parent, int style) {
		super(parent, SWT.BORDER);
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.horizontalSpacing = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);
		setBackground(COLOR_BACKGROUND);

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		toolBar.setBackground(COLOR_BACKGROUND);

		tltmArrowItem = new ToolItem(toolBar, SWT.NONE);
		tltmArrowItem.setWidth(16);
		tltmArrowItem.setImage(ARROW_UP);
		tltmArrowItem.addListener(SWT.Selection, l -> toggleComposite());

		titledSeparator = new TitledSeparator(this, SWT.NONE);
		titledSeparator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		titledSeparator.setText("Example");
		titledSeparator.setBackground(COLOR_BACKGROUND);
		titledSeparator.setForeground(COLOR_FOREGROUND);

		composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		composite.setLayout(new FillLayout());
		layout();

		compositeEnabled = true;
		collapseListeners = Lists.newArrayList();
	}

	public void remainExpanded() {
		setCollapsed(false);
		((GridData) toolBar.getLayoutData()).exclude = true;
		toolBar.setVisible(false);
		toolBar.getParent().layout();
	}

	public void addCollapseListener(Listener l) {
		collapseListeners.add(l);
	}

	public void setTitleText(String title) {
		titledSeparator.setText(title);
	}

	public void setTitleBackground(Color color) {
		titledSeparator.setBackground(color);
		toolBar.setBackground(color);
	}

	public void setTitleForeground(Color color) {
		titledSeparator.setForeground(color);
	}

	public void setTitleImage(Image image) {
		titledSeparator.setImage(image);
		getParent().layout();
		getParent().redraw();
		getParent().update();
	}

	public void setCollapsed(boolean collapsed) {
		compositeEnabled = !collapsed;
		((GridData) composite.getLayoutData()).exclude = !compositeEnabled;
		composite.setVisible(compositeEnabled);
		tltmArrowItem.setImage(compositeEnabled ? ARROW_UP : ARROW_DOWN);
		getParent().layout();
		getParent().redraw();
		getParent().update();
		fireCollapseListeners();
	}

	@Override
	public void layout() {
		composite.layout();
		super.layout();
		getParent().layout();
		getParent().redraw();
		getParent().update();
	}

	private void fireCollapseListeners() {
		for (Listener l : collapseListeners) {
			Event event = new Event();
			event.widget = this;
			event.item = this;
			l.handleEvent(event);
		}
	}

	private void toggleComposite() {
		setCollapsed(compositeEnabled);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
