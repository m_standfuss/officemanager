package officemanager.gui.swt.ui.dialogs;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.MessageDialogs;

public class DateTimeSelectionDialog extends Dialog {
	public Label lblPleaseSelectAnd;
	public DateTime dateTimeDate;
	public DateTime dateTimeTime;

	private Date initialDate;
	private Date disallowBefore;
	private Date disallowAfter;
	private Date result;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public DateTimeSelectionDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(SWT.PRIMARY_MODAL);
	}

	public void setDisallowBefore(Date disallowBefore) {
		this.disallowBefore = disallowBefore;
	}

	public void setDisallowAfter(Date disallowAfter) {
		this.disallowAfter = disallowAfter;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * If the ok button was pressed the date of the dialog saved.
	 * 
	 * @return The date.
	 */
	public Date getResultDate() {
		return result;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gl_container = new GridLayout();
		gl_container.makeColumnsEqualWidth = true;
		gl_container.numColumns = 2;
		container.setLayout(gl_container);

		lblPleaseSelectAnd = new Label(container, SWT.NONE);
		lblPleaseSelectAnd.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
		lblPleaseSelectAnd.setText("Please select and date and time.");

		dateTimeDate = new DateTime(container, SWT.BORDER | SWT.DROP_DOWN);
		dateTimeDate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		dateTimeDate.addListener(SWT.Selection, l -> validate());

		dateTimeTime = new DateTime(container, SWT.BORDER | SWT.TIME | SWT.SHORT);
		dateTimeTime.addListener(SWT.Selection, l -> validate());

		if (initialDate != null) {
			setDate(initialDate);
		}
		return container;
	}

	@Override
	protected void okPressed() {
		result = getCurrentDate();
		super.okPressed();
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(210, 105);
	}

	private Date getCurrentDate() {
		Calendar instance = Calendar.getInstance(AppPrefs.localTimeZone);
		instance.set(Calendar.DAY_OF_MONTH, dateTimeDate.getDay());
		instance.set(Calendar.MONTH, dateTimeDate.getMonth());
		instance.set(Calendar.YEAR, dateTimeDate.getYear());

		instance.set(Calendar.HOUR_OF_DAY, dateTimeTime.getHours());
		instance.set(Calendar.MINUTE, dateTimeTime.getMinutes());
		instance.set(Calendar.SECOND, dateTimeTime.getSeconds());
		return instance.getTime();
	}

	private void setDate(Date date) {
		Calendar instance = Calendar.getInstance(AppPrefs.localTimeZone);
		instance.setTime(date);

		dateTimeDate.setDay(instance.get(Calendar.DAY_OF_MONTH));
		dateTimeDate.setMonth(instance.get(Calendar.MONTH));
		dateTimeDate.setYear(instance.get(Calendar.YEAR));

		dateTimeTime.setHours(instance.get(Calendar.HOUR_OF_DAY));
		dateTimeTime.setMinutes(instance.get(Calendar.MINUTE));
		dateTimeTime.setSeconds(instance.get(Calendar.SECOND));
	}

	private void validate() {
		Date currentSelection = getCurrentDate();
		if (disallowAfter != null) {
			if (currentSelection.after(disallowAfter)) {
				MessageDialogs.displayError(getShell(), "Cannot Select Date",
						"The maximum allowed date is " + AppPrefs.dateFormatter.formatFullDate_DB(disallowAfter));
				setDate(disallowAfter);
			}
		}
		if (disallowBefore != null) {
			if (currentSelection.before(disallowBefore)) {
				MessageDialogs.displayError(getShell(), "Cannot Select Date",
						"The minimum allowed date is " + AppPrefs.dateFormatter.formatFullDate_DB(disallowBefore));
				setDate(disallowBefore);
			}
		}
	}
}
