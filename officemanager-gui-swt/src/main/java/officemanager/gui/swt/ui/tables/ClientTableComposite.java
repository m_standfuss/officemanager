package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class ClientTableComposite extends Composite {

	public static final int COL_NAME = 0;
	public static final int COL_PHONE = 1;
	public static final int COL_EMAIL = 2;
	public static final int COL_JOINED_DTTM = 3;

	public OfficeManagerTable table;
	public TableColumn tblclmnName;
	public TableColumn tblclmnPhone;
	public TableColumn tblclmnEmail;
	public TableColumn tblclmnJoinedDttm;

	public ClientTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new FillLayout());
		table = new OfficeManagerTable(this, tableStyle);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(100);
		tblclmnName.setText("Name");

		tblclmnPhone = new TableColumn(table, SWT.NONE);
		tblclmnPhone.setWidth(100);
		tblclmnPhone.setText("Phone");

		tblclmnEmail = new TableColumn(table, SWT.NONE);
		tblclmnEmail.setWidth(100);
		tblclmnEmail.setText("Email");

		tblclmnJoinedDttm = new TableColumn(table, SWT.NONE);
		tblclmnJoinedDttm.setWidth(100);
		tblclmnJoinedDttm.setText("Client Since");
	}

}
