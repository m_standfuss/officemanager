package officemanager.gui.swt.ui.wizards.pages;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PhoneRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.services.PhoneService;
import officemanager.gui.swt.ui.composites.ButtonBarComposite;
import officemanager.gui.swt.ui.composites.PhoneEditComposite;
import officemanager.gui.swt.ui.tables.PhoneTableComposite;
import officemanager.gui.swt.ui.widgets.CollapsibleComposite;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class PhonesPage extends OfficeManagerWizardPage {
	private final PhoneService phoneService;

	private PhoneTableComposite phoneTbl;
	private PhoneEditComposite newPhoneComposite;
	private Button btnRemoveSelected;

	public PhonesPage() {
		super("phonePage", "Phone Information",
				ImageDescriptor.createFromImage(ResourceManager.getImage(ImageFile.PHONE, AppPrefs.ICN_SIZE_WIZARD_HEADER)));
		setDescription("A primary phone number is required.");
		setPageComplete(false);
		phoneService = new PhoneService();
	}

	@Override
	public void createControl(Composite parent) {
		ScrolledComposite scrCompositeSideBar = new ScrolledComposite(parent, SWT.V_SCROLL);
		scrCompositeSideBar.setExpandHorizontal(true);
		scrCompositeSideBar.setExpandVertical(true);

		Composite root = new Composite(scrCompositeSideBar, SWT.NONE);
		root.setLayout(new GridLayout());

		CollapsibleComposite clpseCmpNewPhone = new CollapsibleComposite(root, SWT.NONE);
		clpseCmpNewPhone.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		clpseCmpNewPhone.composite.setLayout(new GridLayout());
		clpseCmpNewPhone.setTitleText("New Phone");

		newPhoneComposite = new PhoneEditComposite(clpseCmpNewPhone.composite, SWT.NONE);
		newPhoneComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		ButtonBarComposite btnBar = new ButtonBarComposite(clpseCmpNewPhone.composite, SWT.NONE);
		btnBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		phoneTbl = new PhoneTableComposite(root, SWT.NONE, SWT.BORDER | SWT.FULL_SELECTION | SWT.SINGLE);
		GridData gd_phoneTbl = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_phoneTbl.heightHint = 200;
		phoneTbl.setLayoutData(gd_phoneTbl);

		btnRemoveSelected = new Button(root, SWT.NONE);
		btnRemoveSelected.setText("Remove Selected");
		btnRemoveSelected.setEnabled(AppPrefs.hasPermission(AppPermissions.REMOVE_PHONE));

		scrCompositeSideBar.setContent(root);
		scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		setControl(scrCompositeSideBar);

		clpseCmpNewPhone.addListener(SWT.Resize,
				l -> scrCompositeSideBar.setMinSize(root.computeSize(SWT.DEFAULT, SWT.DEFAULT)));
		btnBar.btnSave.addListener(SWT.Selection, l -> addNewPhone());
		btnBar.btnClear.addListener(SWT.Selection, e -> clear());
		btnRemoveSelected.addListener(SWT.Selection, e -> removeSelected());
		phoneService.setComposite(phoneTbl);
	}

	@Override
	public void setFocus() {
		newPhoneComposite.txtPhone.setFocus();
	}

	public void populatePhoneTypes(List<CodeValueRecord> phoneTypes) {
		newPhoneComposite.cmbPhoneType.populateSelections(phoneTypes);
	}

	public void populatePhones(List<PhoneRecord> phones) {
		phoneService.populatePhoneTable(phones);
		checkPageComplete();
	}

	public List<PhoneRecord> getPhones() {
		List<PhoneRecord> phoneList = Lists.newArrayList();
		for (TableItem item : phoneTbl.table.getItems()) {
			phoneList.add((PhoneRecord) item.getData(PhoneService.DATA_RECORD));
		}
		return phoneList;
	}

	private void addNewPhone() {
		if (!newPhoneComposite.txtPhone.isValidPhoneNumber()) {
			setErrorMessage("Invalid phone number.");
			return;
		}
		if (newPhoneComposite.cmbPhoneType.getSelectionIndex() == -1) {
			setErrorMessage("Please select a phone type.");
			return;
		}
		setErrorMessage(null);
		PhoneRecord phone = new PhoneRecord();
		phone.setExtension(newPhoneComposite.txtExtension.getText());
		phone.setPhoneNumber(newPhoneComposite.txtPhone.getUnformattedText());
		phone.setPhoneType(newPhoneComposite.cmbPhoneType.getCodeValueRecord());
		phone.setPrimaryPhone(newPhoneComposite.btnPrimaryPhone.getSelection());
		phoneService.addPhoneRow(phone);
		clear();
		checkPageComplete();
	}

	private void removeSelected() {
		int index = phoneTbl.table.getSelectionIndex();
		if (index == -1) {
			return;
		}
		phoneTbl.table.remove(index);
		checkPageComplete();
	}

	private void checkPageComplete() {
		boolean isComplete = isComplete();
		setPageComplete(isComplete);
		if (isComplete) {
			setDescription("");
		} else {
			setDescription("A primary phone number is required.");
		}
	}

	private boolean isComplete() {
		for (TableItem item : phoneTbl.table.getItems()) {
			if (((PhoneRecord) item.getData(PhoneService.DATA_RECORD)).isPrimaryPhone()) {
				return true;
			}
		}
		return false;
	}

	public void clear() {
		newPhoneComposite.txtPhone.setText("");
		newPhoneComposite.txtExtension.setText("");
		newPhoneComposite.cmbPhoneType.deselectAll();
		newPhoneComposite.btnPrimaryPhone.setSelection(false);
	}
}
