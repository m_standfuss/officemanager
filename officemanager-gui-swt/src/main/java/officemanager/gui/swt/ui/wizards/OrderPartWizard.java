package officemanager.gui.swt.ui.wizards;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.wizards.pages.OrderDetailsPage;
import officemanager.gui.swt.ui.wizards.pages.PartSelectionPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;

public class OrderPartWizard extends OfficeManagerWizard {

	private static final Logger logger = LogManager.getLogger(OrderPartWizard.class);

	private final ClientDelegate clientDelegate;
	private final OrderDelegate orderDelegate;
	private final Long orderId;

	private OrderRecord record;
	private PartSelectionPage partSelectionPage;
	private OrderDetailsPage orderDetailsPage;

	public OrderPartWizard(Long orderId) {
		logger.debug("Opening an order part wizard for orderId=" + orderId);
		checkNotNull(orderId);
		this.orderId = orderId;
		setWindowTitle("Part Management - Order#" + AppPrefs.FORMATTER.formatOrderNumber(orderId));
		clientDelegate = new ClientDelegate();
		orderDelegate = new OrderDelegate();
	}

	@Override
	public boolean canFinish() {
		return getContainer().getCurrentPage() == orderDetailsPage && orderDetailsPage.isPageComplete();
	}

	@Override
	public void addPages() {
		record = orderDelegate.loadOrderRecord(orderId);

		partSelectionPage = new PartSelectionPage(Integer.MAX_VALUE, false, false, true);
		partSelectionPage.setMessage("Add new parts for the order.");
		addPage(partSelectionPage);

		orderDetailsPage = new OrderDetailsPage(true, false, true, false);
		orderDetailsPage.addFirstShowListener(e -> populateOrderDetailsPage());
		orderDetailsPage.addIsShownListener(e -> updateOrderDetailsPage());
		addPage(orderDetailsPage);
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	@Override
	public boolean performFinish() {
		logger.debug("Starting preform finish.");
		if (!MessageDialogs.askQuestion(getShell(), "Continue", "Do you wish to save the order's parts?")) {
			return false;
		}
		List<OrderPartRecord> partRecords = orderDetailsPage.getParts();
		boolean isTaxExempt = false;
		if (record.getClientId() != null) {
			logger.debug("Checking client's tax exempt status.");
			isTaxExempt = clientDelegate.isTaxExempt(record.getClientId());
			logger.debug("Tax exempt status of " + isTaxExempt);
		}
		for (OrderPartRecord partRecord : orderDetailsPage.getParts()) {
			partRecord.setTaxExempt(isTaxExempt);
		}

		logger.debug("Updating order details.");
		orderDelegate.insertOrUpdateParts(orderId, partRecords);

		List<PriceOverrideRecord> priceOverrides = orderDetailsPage.getPriceOverrides();
		logger.debug("Inserting " + priceOverrides.size() + " price overrides.");
		for (PriceOverrideRecord record : priceOverrides) {
			record.setOrderId(orderId);
			orderDelegate.insertPriceOverride(record);
		}
		MessageDialogs.displayMessage(getShell(), "Successfully Saved", "Order parts were successfully saved.");
		return true;
	}

	private void populateOrderDetailsPage() {
		logger.debug(
				"Showing order details page for the first time populating with client products and existing parts.");
		orderDetailsPage.populateClientProducts(record.getClientProducts());
		orderDetailsPage.populateParts(record.getParts());
	}

	private void updateOrderDetailsPage() {
		logger.debug("Showing order details page, updating new part selections.");
		orderDetailsPage.populatePartSearches(partSelectionPage.getSelectedRecords());
	}
}
