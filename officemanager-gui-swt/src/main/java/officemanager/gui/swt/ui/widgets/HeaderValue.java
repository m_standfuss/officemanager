package officemanager.gui.swt.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.util.ResourceManager;

public abstract class HeaderValue<T> extends Composite {

	private final Label lblHeader;
	private final Label lblValue;
	private final Control ctrValue;

	private T currentValue;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public HeaderValue(Composite parent, int style) {
		super(parent, SWT.NONE);

		currentValue = null;

		int columns = 2;
		if ((style & SWT.VERTICAL) != 0) {
			columns = 1;
		}
		GridLayout gridLayout = new GridLayout(columns, false);
		gridLayout.marginHeight = 0;
		setLayout(gridLayout);

		lblHeader = new Label(this, SWT.NONE);
		lblHeader.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		lblHeader.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblValue = new Label(this, SWT.NONE);
		lblValue.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		ctrValue = getEditableControl(this);
		GridData gd_ctrValue = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_ctrValue.widthHint = getEditableWidth();
		gd_ctrValue.heightHint = getEditableHeight();
		gd_ctrValue.exclude = true;
		ctrValue.setLayoutData(gd_ctrValue);
	}

	public void setHeaderText(String value) {
		lblHeader.setText(value);
	}

	public void setValueForeground(Color foreground) {
		lblValue.setForeground(foreground);
	}

	public void makeEditable(boolean editable) {
		((GridData) lblValue.getLayoutData()).exclude = editable;
		lblValue.setVisible(!editable);

		((GridData) ctrValue.getLayoutData()).exclude = !editable;
		ctrValue.setVisible(editable);

		if (editable) {
			setEditableValue(currentValue);
		}
	}

	public void setValue(T value) {
		currentValue = value;
		setEditableValue(value);
		lblValue.setText(getDisplay(value));
	}

	@Override
	public void setEnabled(boolean enabled) {
		ctrValue.setEnabled(enabled);
	}

	protected String getDisplay(T object) {
		if (object == null) {
			return "null";
		}
		return object.toString();
	}

	protected int getEditableWidth() {
		return -1;
	}

	protected int getEditableHeight() {
		return -1;
	}

	public abstract T getEditableValue();

	protected abstract void setEditableValue(T value);

	protected abstract Control getEditableControl(Composite parent);

	@Override
	protected void checkSubclass() {}

}
