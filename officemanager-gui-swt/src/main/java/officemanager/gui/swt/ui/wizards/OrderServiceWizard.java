package officemanager.gui.swt.ui.wizards;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import officemanager.biz.delegates.ClientDelegate;
import officemanager.biz.delegates.OrderDelegate;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.wizards.pages.OrderDetailsPage;
import officemanager.gui.swt.ui.wizards.pages.ServicesPage;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.views.OfficeManagerView;

public class OrderServiceWizard extends OfficeManagerWizard {

	private static final Logger logger = LogManager.getLogger(OrderServiceWizard.class);

	private final OrderDelegate orderDelegate;
	private final ClientDelegate clientDelegate;
	private final Long orderId;

	private OrderRecord record;
	private ServicesPage servicesPage;
	private OrderDetailsPage orderDetailsPage;

	public OrderServiceWizard(Long orderId) {
		logger.debug("Opening an order service wizard for orderId=" + orderId);
		checkNotNull(orderId);
		this.orderId = orderId;
		setWindowTitle("Service Management - Order#" + AppPrefs.FORMATTER.formatOrderNumber(orderId));
		orderDelegate = new OrderDelegate();
		clientDelegate = new ClientDelegate();
	}

	@Override
	public boolean canFinish() {
		return getContainer().getCurrentPage() == orderDetailsPage && orderDetailsPage.isPageComplete();
	}

	@Override
	public void addPages() {
		record = orderDelegate.loadOrderRecord(orderId);

		servicesPage = new ServicesPage();
		servicesPage.setMessage("Add new services for the order.");
		servicesPage.addFirstShowListener(e -> populateLaborRate());
		addPage(servicesPage);

		orderDetailsPage = new OrderDetailsPage(true, false, false, true);
		orderDetailsPage.addFirstShowListener(e -> populateOrderDetailsPage());
		orderDetailsPage.addIsShownListener(e -> updateOrderDetailsPage());
		addPage(orderDetailsPage);
	}

	@Override
	public boolean performFinish() {
		logger.debug("Starting preform finish.");
		if (!MessageDialogs.askQuestion(getShell(), "Continue", "Do you wish to save the order's services?")) {
			return false;
		}
		List<OrderServiceRecord> services = orderDetailsPage.getServices();

		logger.debug("Updating order details.");
		orderDelegate.insertOrUpdateServices(orderId, services);

		MessageDialogs.displayMessage(getShell(), "Successfully Saved", "Order services were successfully saved.");
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	private void populateLaborRate() {
		Long currentClientId = orderDelegate.getClientId(orderId);
		boolean hasDiscountRate = clientDelegate.isInPromotionalClub(currentClientId);
		servicesPage.populateLaborRate(hasDiscountRate ? AppPrefs.hourlyRateDiscounted : AppPrefs.hourlyRate);
	}

	private void populateOrderDetailsPage() {
		logger.debug(
				"Showing order details page for the first time populating with client products and existing parts.");
		orderDetailsPage.populateClientProducts(record.getClientProducts());
		orderDetailsPage.populateServices(record.getServices());
	}

	private void updateOrderDetailsPage() {
		logger.debug("Showing order details page, updating new part selections.");
		orderDetailsPage.populateServices(servicesPage.getServices());
	}
}
