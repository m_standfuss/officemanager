package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.DecimalText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class FlatRateEditComposite extends Composite {
	public final Label lblRequiredFields;
	public final Label lblServiceName;
	public final UpperCaseText txtServiceName;
	public final Label lblDescription;
	public final UpperCaseText txtDescription;
	public final Label lblBasePrice;
	public final DecimalText txtBasePrice;
	public final Label lblBarcode;
	public final Text txtBarcode;
	public final Label lblCategory;
	public final CodeValueCombo cmbCategory;
	public final Label lblProductType;
	public final CodeValueCombo cmbProductType;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public FlatRateEditComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 20;
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		lblRequiredFields = new Label(this, SWT.NONE);
		lblRequiredFields.setText("* Denotes a required field");
		lblRequiredFields.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));

		lblServiceName = new Label(this, SWT.NONE);
		lblServiceName.setText("Service Name*");
		lblServiceName.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblDescription = new Label(this, SWT.NONE);
		lblDescription.setText("Description");
		lblDescription.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtServiceName = new UpperCaseText(this, SWT.BORDER);
		GridData gd_txtServiceName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtServiceName.widthHint = 150;
		txtServiceName.setLayoutData(gd_txtServiceName);

		txtDescription = new UpperCaseText(this, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescription = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 9);
		gd_txtDescription.widthHint = 200;
		gd_txtDescription.heightHint = 150;
		txtDescription.setLayoutData(gd_txtDescription);

		lblBasePrice = new Label(this, SWT.NONE);
		lblBasePrice.setText("Base Price*");
		lblBasePrice.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtBasePrice = new DecimalText(2, this, SWT.BORDER);
		GridData gd_txtBasePrice = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtBasePrice.widthHint = 75;
		txtBasePrice.setLayoutData(gd_txtBasePrice);

		lblCategory = new Label(this, SWT.NONE);
		lblCategory.setText("Category*");
		lblCategory.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbCategory = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbCategory = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbCategory.widthHint = 150;
		cmbCategory.setLayoutData(gd_cmbCategory);

		lblProductType = new Label(this, SWT.NONE);
		lblProductType.setText("Product Type*");
		lblProductType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbProductType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbProductType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbProductType.widthHint = 150;
		cmbProductType.setLayoutData(gd_cmbProductType);

		lblBarcode = new Label(this, SWT.NONE);
		lblBarcode.setText("Barcode");
		lblBarcode.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtBarcode = new Text(this, SWT.BORDER);
		GridData gd_txtBarcode = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtBarcode.widthHint = 100;
		txtBarcode.setLayoutData(gd_txtBarcode);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
