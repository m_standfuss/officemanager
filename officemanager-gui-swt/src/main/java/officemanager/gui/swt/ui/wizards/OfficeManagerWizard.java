package officemanager.gui.swt.ui.wizards;

import org.eclipse.jface.wizard.Wizard;

import officemanager.gui.swt.views.OfficeManagerView;

public abstract class OfficeManagerWizard extends Wizard {

	public abstract OfficeManagerView getReturnView();
}
