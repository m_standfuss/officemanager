package officemanager.gui.swt.ui.tables;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;

public class PaymentTableComposite extends Composite {

	public static final int COL_AMOUNT = 0;
	public static final int COL_DATE = 1;
	public static final int COL_TYPE = 2;
	public static final int COL_NUMBER = 3;
	public static final int COL_DESCRIPTION = 4;

	public OfficeManagerTable table;
	public TableColumn tblclmnAmount;
	public TableColumn tblclmnDate;
	public TableColumn tblclmnType;
	public TableColumn tblclmnNumber;
	public TableColumn tblclmnDescription;

	public PaymentTableComposite(Composite parent, int style, int tableStyle) {
		super(parent, style);

		setLayout(new GridLayout());
		table = new OfficeManagerTable(this, tableStyle);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tblclmnAmount = new TableColumn(table, SWT.NONE);
		tblclmnAmount.setWidth(100);
		tblclmnAmount.setText("Amount");

		tblclmnDate = new TableColumn(table, SWT.NONE);
		tblclmnDate.setWidth(100);
		tblclmnDate.setText("Date");

		tblclmnType = new TableColumn(table, SWT.NONE);
		tblclmnType.setWidth(100);
		tblclmnType.setText("Type");

		tblclmnNumber = new TableColumn(table, SWT.NONE);
		tblclmnNumber.setWidth(100);
		tblclmnNumber.setText("Nbr.");

		tblclmnDescription = new TableColumn(table, SWT.NONE);
		tblclmnDescription.setWidth(100);
		tblclmnDescription.setText("Description");
	}
}
