package officemanager.gui.swt.ui.wizards;

import officemanager.biz.delegates.OrderCommentDelegate;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.gui.swt.ui.wizards.pages.CommentPageNew;
import officemanager.gui.swt.views.OfficeManagerView;

public class OrderCommentWizard extends OfficeManagerWizard {

	private final long orderId;
	private final OrderCommentDelegate orderCommentDelegate;

	private CommentPageNew orderCommentPage;

	public OrderCommentWizard(long orderId) {
		this.orderId = orderId;
		orderCommentDelegate = new OrderCommentDelegate();
		setWindowTitle("Order Comment");
	}

	@Override
	public void addPages() {
		orderCommentPage = new CommentPageNew();
		orderCommentPage.addFirstShowListener(e -> populateCommentTypes());
		addPage(orderCommentPage);
	}

	@Override
	public boolean performFinish() {
		OrderCommentRecord record = orderCommentPage.getCommentRecord();
		record.setOrderId(orderId);
		orderCommentDelegate.persistOrderComment(record);
		return true;
	}

	@Override
	public OfficeManagerView getReturnView() {
		return null;
	}

	private void populateCommentTypes() {
		orderCommentPage.populateCommentTypes(orderCommentDelegate.getOrderCommentTypes());
	}
}
