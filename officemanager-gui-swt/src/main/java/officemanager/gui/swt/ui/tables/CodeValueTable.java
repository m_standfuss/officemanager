package officemanager.gui.swt.ui.tables;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.records.CodeValueRecord;

public class CodeValueTable extends OfficeManagerTable {
	private static final String DATA_CODE_VALUE_RECORD = "DATA_CODE_VALUE_RECORD";

	public CodeValueTable(Composite parent) {
		this(parent, SWT.CHECK | SWT.FULL_SELECTION | SWT.BORDER | OfficeManagerTable.NO_ZEBRA_STRIPE);
	}

	public CodeValueTable(Composite parent, int style) {
		super(parent, style);
		setHeaderVisible(false);
		setLinesVisible(false);
	}

	public void addCodeValue(CodeValueRecord record) {
		checkNotNull(record);
		_addCodeValue(record);
	}

	public void populateList(List<CodeValueRecord> records) {
		checkNotNull(records);
		checkArgument(!records.contains(null));
		removeAll();
		for (CodeValueRecord record : records) {
			_addCodeValue(record);
		}
	}

	public void checkCodeValues(List<CodeValueRecord> codeValues) {
		Set<CodeValueRecord> set = Sets.newHashSet(codeValues);
		for (TableItem item : getItems()) {
			item.setChecked(set.contains(item.getData(DATA_CODE_VALUE_RECORD)));
		}
	}

	public List<Long> getCheckedCodeValues() {
		List<CodeValueRecord> checkedItems = getCheckedCodeValueRecords();
		List<Long> codeValues = Lists.newArrayList();
		for (CodeValueRecord record : checkedItems) {
			codeValues.add(record.getCodeValue());
		}
		return codeValues;
	}

	public List<CodeValueRecord> getCodeValueRecords() {
		return getDataAll(DATA_CODE_VALUE_RECORD);
	}

	public List<CodeValueRecord> getCheckedCodeValueRecords() {
		return getCheckedData(DATA_CODE_VALUE_RECORD);
	}

	@Override
	protected void checkSubclass() {}

	private void _addCodeValue(CodeValueRecord record) {
		TableItem item = new TableItem(this, SWT.NONE);
		item.setText(record.getDisplay());
		item.setData(DATA_CODE_VALUE_RECORD, record);
	}
}
