package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.util.ResourceManager;

public abstract class PersonInformationComposite extends Composite {
	public Composite cmpInformation;
	public Label lblFirstNameTitle;
	public Label lblLastNameTitle;
	public Label lblFullFormattedTitle;
	public Label lblEmailTitle;
	public Label lblPrimaryPhoneTitle;
	public Label lblPrimaryAddressTitle;
	public Label lblFirstName;
	public Label lblLastName;
	public Label lblFullFormatted;
	public Label lblEmail;
	public Label lblPrimaryAddress;
	public Label lblPrimaryPhone;
	public Composite cmpAuxInformation;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PersonInformationComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	public abstract void fillAuxInfo(Composite parent);

	private void createContents() {
		setLayout(new GridLayout());

		cmpInformation = new Composite(this, SWT.NONE);
		GridLayout gridLayout = new GridLayout(4, false);
		gridLayout.verticalSpacing = 10;
		gridLayout.horizontalSpacing = 20;
		cmpInformation.setLayout(gridLayout);

		lblFirstNameTitle = new Label(cmpInformation, SWT.NONE);
		lblFirstNameTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFirstNameTitle.setText("First Name:");
		lblFirstNameTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblFirstName = new Label(cmpInformation, SWT.NONE);

		lblPrimaryPhoneTitle = new Label(cmpInformation, SWT.NONE);
		lblPrimaryPhoneTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPrimaryPhoneTitle.setText("Primary Phone:");
		lblPrimaryPhoneTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblPrimaryPhone = new Label(cmpInformation, SWT.NONE);

		lblLastNameTitle = new Label(cmpInformation, SWT.NONE);
		lblLastNameTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLastNameTitle.setText("Last Name:");
		lblLastNameTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblLastName = new Label(cmpInformation, SWT.NONE);

		lblPrimaryAddressTitle = new Label(cmpInformation, SWT.NONE);
		lblPrimaryAddressTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 3));
		lblPrimaryAddressTitle.setText("Primary Address:");
		lblPrimaryAddressTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblPrimaryAddress = new Label(cmpInformation, SWT.NONE);
		lblPrimaryAddress.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 3));

		lblFullFormattedTitle = new Label(cmpInformation, SWT.NONE);
		lblFullFormattedTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFullFormattedTitle.setText("Full Formatted:");
		lblFullFormattedTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblFullFormatted = new Label(cmpInformation, SWT.NONE);

		lblEmailTitle = new Label(cmpInformation, SWT.NONE);
		lblEmailTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEmailTitle.setText("Email:");
		lblEmailTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblEmail = new Label(cmpInformation, SWT.NONE);

		Label seperator = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		seperator.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		cmpAuxInformation = new Composite(this, SWT.NONE);
		GridLayout gl_cmpAuxInformation = new GridLayout(4, false);
		gl_cmpAuxInformation.verticalSpacing = 10;
		gl_cmpAuxInformation.horizontalSpacing = 20;
		cmpAuxInformation.setLayout(gl_cmpAuxInformation);
		fillAuxInfo(cmpAuxInformation);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
