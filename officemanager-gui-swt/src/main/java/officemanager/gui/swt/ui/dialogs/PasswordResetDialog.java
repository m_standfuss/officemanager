package officemanager.gui.swt.ui.dialogs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import officemanager.biz.delegates.LoginDelegate;
import officemanager.gui.swt.ui.composites.PasswordResetComposite;
import officemanager.gui.swt.util.MessageDialogs;

public class PasswordResetDialog extends Dialog {

	private static final Logger logger = LogManager.getLogger(PasswordResetDialog.class);

	private final LoginDelegate loginDelegate;
	private Shell shell;
	private Long personnelId;
	private boolean passwordReset;
	private PasswordResetComposite composite;

	/**
	 * Create the reset password dialog.
	 * 
	 * @param parent
	 *            The parent shell to use to open the dialog.
	 * @param style
	 *            The style to apply.
	 */
	public PasswordResetDialog(Shell parent, int style) {
		super(parent, style);
		loginDelegate = new LoginDelegate();
		setText("Reset Password");
	}

	/**
	 * Opens the dialog box for the given personnel id.
	 * 
	 * @return If the user successfully reset their password.
	 */
	public boolean open(Long personnelId) {
		logger.debug("Opening a reset password dialog for personnel id=" + personnelId);
		this.personnelId = personnelId;
		passwordReset = false;
		createContents();

		Display display = getParent().getDisplay();
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		shell.setLocation(x, y);

		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return passwordReset;
	}

	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM);
		shell.setSize(439, 219);
		shell.setText(getText());
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));

		composite = new PasswordResetComposite(shell, SWT.NONE);

		composite.addCancelListener(event -> shell.close());
		composite.addOkListener(event -> changePassword());

		KeyListener enterKeyListener = new KeyListener() {

			@Override
			public void keyReleased(KeyEvent arg0) {
				if (arg0.keyCode == SWT.CR) {
					changePassword();
					arg0.doit = false;
				}
			}

			@Override
			public void keyPressed(KeyEvent arg0) {}
		};
		composite.addNewKeyListener(enterKeyListener);
	}

	private void changePassword() {
		String oldPassword = composite.getCurrentPassword();
		String newPassword = composite.getNewPassword();
		String confirmPassword = composite.getConfirmNewPassword();

		if (!newPassword.equals(confirmPassword)) {
			logger.debug("The passwords do not match.");
			MessageDialogs.displayError(shell, "Unable to Reset Password.",
					"The new password and confirmation password do not match.");
			return;
		}

		if (!loginDelegate.checkPassword(personnelId, oldPassword)) {
			logger.debug("User entered password does not match current password.");
			MessageDialogs.displayError(shell, "Unable to Reset Password.", "The current password is incorrect.");
			return;
		}

		logger.debug("Updating the password");
		loginDelegate.updatePassword(personnelId, newPassword);
		MessageDialogs.displayMessage(shell, "Updated Password.", "Successfully reset the password.");
		passwordReset = true;
		shell.close();
	}
}
