package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;

import officemanager.gui.swt.util.ResourceManager;

public class ReportsComposite extends Composite {
	public Link lnkBalanceDrawer;
	public Link lnkTotals;
	public Link lnkInventory;
	public Label lblReports;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ReportsComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		setLayout(new GridLayout());

		lblReports = new Label(this, SWT.NONE);
		lblReports.setText("Reports");
		lblReports.setFont(ResourceManager.getFont("Segoe UI", 14, SWT.BOLD));

		lnkBalanceDrawer = new Link(this, SWT.NONE);
		GridData gd_lnkBalanceDrawer = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		lnkBalanceDrawer.setLayoutData(gd_lnkBalanceDrawer);
		lnkBalanceDrawer.setText("\u2022 <a>Balance Drawer</a>");
		lnkBalanceDrawer.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		lnkTotals = new Link(this, SWT.NONE);
		GridData gd_lnkTotals = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		lnkTotals.setLayoutData(gd_lnkTotals);
		lnkTotals.setText("\u2022 <a>Totals</a>");
		lnkTotals.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));

		lnkInventory = new Link(this, SWT.NONE);
		GridData gd_lnkInventory = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		lnkInventory.setLayoutData(gd_lnkInventory);
		lnkInventory.setText("\u2022 <a>Current Inventory</a>");
		lnkInventory.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
