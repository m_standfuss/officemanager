package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import officemanager.gui.swt.util.ResourceManager;

public class PasswordResetComposite extends Composite {
	private Text txtCurrentPassword;
	private Text txtNewPassword;
	private Text txtConfirmNewPassword;
	private Button btnOk;
	private Button btnCancel;
	private Label lblPasswordsMustBe;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PasswordResetComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	public String getCurrentPassword() {
		return txtCurrentPassword.getText();
	}

	public String getNewPassword() {
		return txtNewPassword.getText();
	}

	public String getConfirmNewPassword() {
		return txtConfirmNewPassword.getText();
	}

	public void addOkListener(Listener listener) {
		btnOk.addListener(SWT.Selection, listener);
	}

	public void addCancelListener(Listener listener) {
		btnCancel.addListener(SWT.Selection, listener);
	}

	public void addNewKeyListener(KeyListener listener) {
		txtConfirmNewPassword.addKeyListener(listener);
	}

	private void createContents() {
		setLayout(new GridLayout(3, false));

		Label lblCurrentPassword = new Label(this, SWT.NONE);
		lblCurrentPassword.setText("Current Password:");

		txtCurrentPassword = new Text(this, SWT.BORDER | SWT.PASSWORD);
		GridData gd_txtCurrentPassword = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtCurrentPassword.widthHint = 175;
		txtCurrentPassword.setLayoutData(gd_txtCurrentPassword);

		Label lblNewPassword = new Label(this, SWT.NONE);
		lblNewPassword.setText("New Password:");

		txtNewPassword = new Text(this, SWT.BORDER | SWT.PASSWORD);
		GridData gd_txtNewPassword = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtNewPassword.widthHint = 175;
		txtNewPassword.setLayoutData(gd_txtNewPassword);

		Label lblConfirmNewPassword = new Label(this, SWT.NONE);
		lblConfirmNewPassword.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblConfirmNewPassword.setText("Confirm New Password:");

		txtConfirmNewPassword = new Text(this, SWT.BORDER | SWT.PASSWORD);
		GridData gd_txtConfirmNewPassword = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_txtConfirmNewPassword.widthHint = 175;
		txtConfirmNewPassword.setLayoutData(gd_txtConfirmNewPassword);
		new Label(this, SWT.NONE);

		btnOk = new Button(this, SWT.RIGHT_TO_LEFT);
		btnOk.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnOk.setText("     OK     ");

		btnCancel = new Button(this, SWT.NONE);
		btnCancel.setText("   Cancel   ");
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);

		lblPasswordsMustBe = new Label(this, SWT.NONE);
		lblPasswordsMustBe.setFont(ResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
		lblPasswordsMustBe.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		lblPasswordsMustBe.setText("*Passwords must be between 6-25 characters.");
	}

	@Override
	public void dispose() {
		ResourceManager.disposeFonts();
		super.dispose();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
