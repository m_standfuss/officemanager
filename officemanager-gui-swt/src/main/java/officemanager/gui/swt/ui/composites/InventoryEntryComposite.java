package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.tables.PartSearchTableComposite;
import officemanager.gui.swt.ui.widgets.ScannerText;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;

public class InventoryEntryComposite extends Composite {
	public ToolBar toolBar;
	public ToolItem tltmManuallyAdd;
	public ToolItem tlItmScanPart;
	public Label lblDirections;
	public PartSearchTableComposite partSearchTableComposite;
	public ScannerText scannerText;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public InventoryEntryComposite(Composite parent, int style) {
		super(parent, style);
		createContents();
	}

	private void createContents() {
		setLayout(new GridLayout());

		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		tlItmScanPart = new ToolItem(toolBar, SWT.NONE);
		tlItmScanPart.setImage(ResourceManager.getImage(ImageFile.SCAN, AppPrefs.ICN_SIZE_PAGETOOLITEM));
		tlItmScanPart.setToolTipText("Scan Part");

		tltmManuallyAdd = new ToolItem(toolBar, SWT.NONE);
		tltmManuallyAdd.setToolTipText("Manually Add Part");
		tltmManuallyAdd.setImage(ResourceManager.getImage(ImageFile.ADD, AppPrefs.ICN_SIZE_PAGETOOLITEM));

		lblDirections = new Label(this, SWT.NONE);
		lblDirections.setText("Please scan parts to be entered into inventory.");
		lblDirections.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		scannerText = new ScannerText(this, SWT.NONE);
		GridData gd_scannerText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_scannerText.exclude = true;
		scannerText.setLayoutData(gd_scannerText);

		partSearchTableComposite = new PartSearchTableComposite(this, SWT.NONE, SWT.FULL_SELECTION | SWT.BORDER);
		partSearchTableComposite.table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		partSearchTableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		partSearchTableComposite.setLayout(new GridLayout(1, false));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
