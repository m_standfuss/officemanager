package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.ui.widgets.CodeValueCombo;
import officemanager.gui.swt.ui.widgets.DecimalText;
import officemanager.gui.swt.ui.widgets.NumberText;
import officemanager.gui.swt.ui.widgets.UpperCaseText;
import officemanager.gui.swt.util.ResourceManager;

public class PaymentEditComposite extends Composite {
	public final Label lblType;
	public final CodeValueCombo cmbType;
	public final Label lblAmount;
	public final DecimalText txtAmount;
	public final Label lblDescription;
	public final UpperCaseText txtDescription;
	public final Label lblNumber;
	public final CodeValueCombo cmbCreditCardCompanies;
	public final NumberText txtNumber;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public PaymentEditComposite(Composite parent, int style) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout();
		gridLayout.horizontalSpacing = 10;
		gridLayout.numColumns = 4;
		setLayout(gridLayout);

		lblType = new Label(this, SWT.NONE);
		lblType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblType.setText("Type:");
		lblType.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		cmbType = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmbType.widthHint = 125;
		cmbType.setLayoutData(gd_cmbType);

		lblAmount = new Label(this, SWT.NONE);
		lblAmount.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAmount.setText("Amount:");
		lblAmount.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtAmount = new DecimalText(2, this, SWT.BORDER);
		GridData gd_txtAmount = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtAmount.widthHint = 75;
		txtAmount.setLayoutData(gd_txtAmount);

		lblDescription = new Label(this, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblDescription.setText("Description:");
		lblDescription.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtDescription = new UpperCaseText(this, SWT.BORDER);
		txtDescription.setTextLimit(80);
		GridData gd_txtDescription = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtDescription.widthHint = 150;
		txtDescription.setLayoutData(gd_txtDescription);

		cmbCreditCardCompanies = new CodeValueCombo(this, SWT.READ_ONLY);
		GridData gd_cmbCreditCardCompanies = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_cmbCreditCardCompanies.exclude = true;
		gd_cmbCreditCardCompanies.widthHint = 125;
		cmbCreditCardCompanies.setLayoutData(gd_cmbCreditCardCompanies);

		lblNumber = new Label(this, SWT.NONE);
		lblNumber.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblNumber.setText("Number:");
		lblNumber.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		txtNumber = new NumberText(this, SWT.BORDER);
		txtNumber.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
