package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import officemanager.gui.swt.util.ResourceManager;

public class ClientInformationComposite extends PersonInformationComposite {

	public Label lblAttentionOfTitle;
	public Label lblAttentionOf;
	public Label lblPromoClubTitle;
	public Label lblPromoClub;
	public Label lblJoinedOnTitle;
	public Label lblJoinedOn;
	public Label lblTaxExemptTitle;
	public Label lblTaxExempt;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ClientInformationComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout());
	}

	@Override
	public void fillAuxInfo(Composite parent) {
		lblAttentionOfTitle = new Label(parent, SWT.NONE);
		lblAttentionOfTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAttentionOfTitle.setText("Attention Of:");
		lblAttentionOfTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblAttentionOf = new Label(parent, SWT.NONE);

		lblPromoClubTitle = new Label(parent, SWT.NONE);
		lblPromoClubTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPromoClubTitle.setText("Promo Club:");
		lblPromoClubTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblPromoClub = new Label(parent, SWT.NONE);

		lblJoinedOnTitle = new Label(parent, SWT.NONE);
		lblJoinedOnTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblJoinedOnTitle.setText("Joined On:");
		lblJoinedOnTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblJoinedOn = new Label(parent, SWT.NONE);

		lblTaxExemptTitle = new Label(parent, SWT.NONE);
		lblTaxExemptTitle.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTaxExemptTitle.setText("Tax Exempt:");
		lblTaxExemptTitle.setFont(ResourceManager.getFont("Segoe UI", 10, SWT.BOLD));

		lblTaxExempt = new Label(parent, SWT.NONE);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
