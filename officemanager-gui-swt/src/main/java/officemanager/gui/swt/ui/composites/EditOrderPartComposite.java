package officemanager.gui.swt.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import officemanager.biz.records.OrderClientProductRecord;
import officemanager.gui.swt.AppPermissions;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.ui.widgets.HeaderValueCombo;
import officemanager.gui.swt.ui.widgets.HeaderValueLabel;
import officemanager.gui.swt.ui.widgets.HeaderValueSpinner;

public class EditOrderPartComposite extends Composite {

	public final HeaderValueLabel hdrValPart;
	public final HeaderValueLabel hdrValManufacturer;
	public final HeaderValueCombo<OrderClientProductRecord> hdrValClientProduct;
	public final HeaderValueSpinner hdrValQnt;
	public final HeaderValueLabel hdrValPrice;
	public final Button btnChangePrice;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public EditOrderPartComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		setLayout(gridLayout);

		hdrValPart = new HeaderValueLabel(this, SWT.HORIZONTAL);
		hdrValPart.setHeaderText("Part:");

		hdrValManufacturer = new HeaderValueLabel(this, SWT.HORIZONTAL);
		hdrValManufacturer.setHeaderText("Manufacturer:");

		hdrValClientProduct = new HeaderValueCombo<OrderClientProductRecord>(this, SWT.HORIZONTAL) {
			@Override
			protected String getDisplay(OrderClientProductRecord productRecord) {
				if (productRecord == null) {
					return "";
				}
				return AppPrefs.FORMATTER.formatClientProductDisplay(productRecord.getProductManufacturer(),
						productRecord.getProductModel(), productRecord.getSerialNumber());
			}
		};
		hdrValClientProduct.setHeaderText("Client Product:");

		hdrValQnt = new HeaderValueSpinner(this, SWT.HORIZONTAL);
		hdrValQnt.setHeaderText("Quantity:");

		Composite cmpPrice = new Composite(this, SWT.NONE);
		cmpPrice.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		GridLayout gl_composite = new GridLayout(2, false);
		gl_composite.marginWidth = 0;
		gl_composite.marginHeight = 0;
		gl_composite.horizontalSpacing = 0;
		cmpPrice.setLayout(gl_composite);

		hdrValPrice = new HeaderValueLabel(cmpPrice, SWT.HORIZONTAL);
		hdrValPrice.setHeaderText("Price:");

		btnChangePrice = new Button(cmpPrice, SWT.NONE);
		btnChangePrice.setLayoutData(new GridData());
		btnChangePrice.setText("Change Price");
		btnChangePrice.setEnabled(AppPrefs.hasPermission(AppPermissions.DISCOUNT_PRICE));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
