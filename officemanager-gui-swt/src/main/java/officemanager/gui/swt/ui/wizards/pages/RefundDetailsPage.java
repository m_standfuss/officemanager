package officemanager.gui.swt.ui.wizards.pages;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.Calculator;
import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.gui.swt.ui.composites.RefundDetailsComposite;
import officemanager.gui.swt.ui.composites.RefundDetailsComposite.PartItem;
import officemanager.gui.swt.ui.composites.RefundDetailsComposite.ServiceItem;
import officemanager.utility.Tuple;

public class RefundDetailsPage extends OfficeManagerWizardPage {

	public RefundDetailsComposite composite;

	private final Map<Long, PartItem> currentParts;
	private final Map<Long, ServiceItem> currentFlatRates;
	private final Map<Long, ServiceItem> currentServices;

	/**
	 * Create the wizard.
	 */
	public RefundDetailsPage() {
		super("RefundDetailsPage");
		setTitle("Refund Details");
		setDescription("Please verify the details of the refund.");

		currentParts = Maps.newHashMap();
		currentFlatRates = Maps.newHashMap();
		currentServices = Maps.newHashMap();
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		composite = new RefundDetailsComposite(parent, SWT.NONE);
		setControl(composite);
	}

	public List<Tuple<Long, PartItem>> getParts() {
		return Tuple.toTuples(currentParts);
	}

	public List<Tuple<Long, ServiceItem>> getServices() {
		return Tuple.toTuples(currentServices);
	}

	public List<Tuple<Long, ServiceItem>> getFlatRates() {
		return Tuple.toTuples(currentFlatRates);
	}

	public void populateParts(List<OrderPartRecord> orderParts) {
		List<OrderPartRecord> partsToAdd = Lists.newArrayList();
		Set<Long> activeParts = Sets.newHashSet();
		for (OrderPartRecord part : orderParts) {
			activeParts.add(part.getOrderToPartId());
			if (!currentParts.containsKey(part.getOrderToPartId())) {
				partsToAdd.add(part);
			}
		}

		List<Long> partsToRemove = Lists.newArrayList();
		for (Long currentPart : currentParts.keySet()) {
			if (!activeParts.contains(currentPart)) {
				partsToRemove.add(currentPart);
			}
		}

		for (OrderPartRecord newPart : partsToAdd) {
			PartItem item = composite.addPart(newPart.getPartName(), newPart.getPartPrice(), newPart.getQuantity());
			currentParts.put(newPart.getOrderToPartId(), item);
		}

		for (Long toRemove : partsToRemove) {
			PartItem item = currentParts.get(toRemove);
			item.dispose();
			currentParts.remove(toRemove);
		}
	}

	public void populateServices(List<OrderServiceRecord> orderServices) {
		List<OrderServiceRecord> servicesToAdd = Lists.newArrayList();
		Set<Long> activeServices = Sets.newHashSet();
		for (OrderServiceRecord service : orderServices) {
			activeServices.add(service.getServiceId());
			if (!currentServices.containsKey(service.getServiceId())) {
				servicesToAdd.add(service);
			}
		}

		List<Long> servicesToRemove = Lists.newArrayList();
		for (Long currentService : currentServices.keySet()) {
			if (!activeServices.contains(currentService)) {
				servicesToRemove.add(currentService);
			}
		}

		for (OrderServiceRecord newService : servicesToAdd) {
			ServiceItem item = composite.addService(newService.getServiceName(), Calculator.totalService(newService));
			currentServices.put(newService.getServiceId(), item);
		}

		for (Long toRemove : servicesToRemove) {
			ServiceItem item = currentServices.get(toRemove);
			item.dispose();
			currentServices.remove(toRemove);
		}
	}

	public void populateFlatRates(List<OrderFlatRateServiceRecord> orderFlatRates) {
		List<OrderFlatRateServiceRecord> servicesToAdd = Lists.newArrayList();
		Set<Long> activeServices = Sets.newHashSet();
		for (OrderFlatRateServiceRecord service : orderFlatRates) {
			activeServices.add(service.getOrderToFrsId());
			if (!currentFlatRates.containsKey(service.getOrderToFrsId())) {
				servicesToAdd.add(service);
			}
		}

		List<Long> servicesToRemove = Lists.newArrayList();
		for (Long currentService : currentFlatRates.keySet()) {
			if (!activeServices.contains(currentService)) {
				servicesToRemove.add(currentService);
			}
		}

		for (OrderFlatRateServiceRecord newService : servicesToAdd) {
			ServiceItem item = composite.addService(newService.getServiceName(),
					Calculator.totalFlatRateService(newService));
			currentFlatRates.put(newService.getOrderToFrsId(), item);
		}

		for (Long toRemove : servicesToRemove) {
			ServiceItem item = currentFlatRates.get(toRemove);
			item.dispose();
			currentFlatRates.remove(toRemove);
		}
	}
}
