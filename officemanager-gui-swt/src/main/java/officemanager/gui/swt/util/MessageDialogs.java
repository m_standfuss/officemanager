package officemanager.gui.swt.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class MessageDialogs {

	public static void displayMessage(Shell parent, String title, String message) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_INFORMATION | SWT.OK);
		dialog.setText(title);
		dialog.setMessage(message);
		dialog.open();
	}

	public static void displayError(Shell parent, String title, String message) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_ERROR | SWT.OK);
		dialog.setText(title);
		dialog.setMessage(message);
		dialog.open();
	}

	public static boolean askQuestion(Shell parent, String title, String question) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		dialog.setText(title);
		dialog.setMessage(question);
		return dialog.open() == SWT.YES;
	}

	public static boolean canContinue(Shell parent, String title, String message) {
		MessageBox dialog = new MessageBox(parent, SWT.OK | SWT.CANCEL);
		dialog.setText(title);
		dialog.setMessage(message);
		return dialog.open() == SWT.OK;
	}
}
