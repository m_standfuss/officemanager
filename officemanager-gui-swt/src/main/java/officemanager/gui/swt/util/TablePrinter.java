package officemanager.gui.swt.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.gui.swt.printing.PaperclipsPrinter;
import officemanager.gui.swt.printing.TablePrint;
import officemanager.gui.swt.ui.tables.OfficeManagerTable;

public class TablePrinter {

	private static final Logger logger = LogManager.getLogger(TablePrinter.class);

	private final OfficeManagerTable table;

	public TablePrinter(OfficeManagerTable table) {
		this.table = table;
	}

	public void printTable(String title) {
		List<Integer> columns = Lists.newArrayList();
		for (int i = 0; i < table.getColumnCount(); i++) {
			columns.add(i);
		}
		printTable(title, columns);
	}

	public void printTable(String title, List<Integer> columnsToPrint) {

		checkNotNull(title);
		checkNotNull(columnsToPrint);

		String[] headers = new String[columnsToPrint.size()];
		for (int i = 0; i < columnsToPrint.size(); i++) {
			Integer columnIndex = columnsToPrint.get(i);
			headers[i] = table.getColumn(columnIndex).getText();
		}

		List<String[]> rows = Lists.newArrayListWithCapacity(table.getItemCount());
		for (TableItem item : table.getItems()) {
			String[] row = new String[columnsToPrint.size()];
			for (int i = 0; i < columnsToPrint.size(); i++) {
				Integer columnIndex = columnsToPrint.get(i);
				row[i] = item.getText(columnIndex);
			}
			rows.add(row);
		}

		TablePrint print = new TablePrint(title, headers, rows);

		PrinterData printerData = PaperclipsPrinter.getPrinterData(table.getShell());
		if (printerData != null) {
			Boolean shrinkAsNeeded = null;
			while (true) {
				try {
					PaperclipsPrinter.print(print, printerData);
				} catch (SWTError e) {
					if (shrinkAsNeeded == null) {
						shrinkAsNeeded = MessageDialogs.askQuestion(table.getShell(), "Unable To Print",
								"Unable to fit entire contents on a single page. Reprint with a smaller font size?");
						if (!shrinkAsNeeded) {
							break;
						}
					}
					try {
						print.decreaseFontSize();
					} catch (Exception e2) {
						logger.error(e2);
						MessageDialogs.displayError(table.getShell(), "Unable To Print",
								"Unable to print shop ticket on a single page.");
						break;
					}
					continue;
				} catch (Exception e) {
					logger.error("Unable to print the shop ticket.", e);
					MessageDialogs.displayError(table.getShell(), "Unable To Print",
							"Unexpected error occured while printing.");
					break;
				}
				break;
			}
		}
	}
}
