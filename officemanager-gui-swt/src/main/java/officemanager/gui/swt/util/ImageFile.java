package officemanager.gui.swt.util;

public class ImageFile {
	public static final ImageFile ADD = new ImageFile("add", "png");
	public static final ImageFile ADDRESS = new ImageFile("address", "png");
	public static final ImageFile APP_PREFS = new ImageFile("appPrefs", "png");
	public static final ImageFile ARROW_DOWN = new ImageFile("arrowDown", "png");
	public static final ImageFile ARROW_UP = new ImageFile("arrowUp", "png");
	public static final ImageFile BOOK = new ImageFile("book", "png");
	public static final ImageFile BOOK_OPEN = new ImageFile("bookOpen", "png");
	public static final ImageFile BUILDING = new ImageFile("building", "png");
	public static final ImageFile CASH = new ImageFile("cash", "png");
	public static final ImageFile CHECK = new ImageFile("check", "png");
	public static final ImageFile CLOSE = new ImageFile("close", "png");
	public static final ImageFile CODE = new ImageFile("code", "png");
	public static final ImageFile CODE_ADD = new ImageFile("code_add", "png");
	public static final ImageFile COMMENT = new ImageFile("comment", "png");
	public static final ImageFile CRATE = new ImageFile("crate", "png");
	public static final ImageFile CRATE_EDIT = new ImageFile("crate_edit", "png");
	public static final ImageFile CRATE_NEW = new ImageFile("crate_new", "png");
	public static final ImageFile DOCUMENT = new ImageFile("document", "png");
	public static final ImageFile EDIT = new ImageFile("edit", "png");
	public static final ImageFile HOME = new ImageFile("home", "png");
	public static final ImageFile INVENTORY = new ImageFile("inventory", "png");
	public static final ImageFile LOGO = new ImageFile("logo", ".ico");
	public static final ImageFile LOGO_HEADER = new ImageFile("logoHeader", "jpeg");
	public static final ImageFile NEW = new ImageFile("new", "png");
	public static final ImageFile OPEN = new ImageFile("open", "png");
	public static final ImageFile ORDER = new ImageFile("order", "png");
	public static final ImageFile ORDER_EDIT = new ImageFile("orderEdit", "png");
	public static final ImageFile ORDER_EXPORT = new ImageFile("order_export", "png");
	public static final ImageFile ORDER_SEARCH = new ImageFile("orderSearch", "png");
	public static final ImageFile PART = new ImageFile("part", "png");
	public static final ImageFile PART_ADD = new ImageFile("part_add", "png");
	public static final ImageFile PART_EDIT = new ImageFile("part_edit", "png");
	public static final ImageFile PHONE = new ImageFile("phone", "png");
	public static final ImageFile PRINT = new ImageFile("print", "png");
	public static final ImageFile PRODUCT = new ImageFile("product", "png");
	public static final ImageFile RED_X = new ImageFile("redX", "png");
	public static final ImageFile REFRESH = new ImageFile("refresh", "png");
	public static final ImageFile REFUND = new ImageFile("refund", "png");
	public static final ImageFile REPORTS = new ImageFile("reports", "png");
	public static final ImageFile ROOM = new ImageFile("room", "png");
	public static final ImageFile SALE = new ImageFile("sale", "png");
	public static final ImageFile SALE_ADD = new ImageFile("sale_add", "png");
	public static final ImageFile SALE_EDIT = new ImageFile("sale_edit", "png");
	public static final ImageFile SAVE = new ImageFile("save", "png");
	public static final ImageFile SCAN = new ImageFile("scan", "png");
	public static final ImageFile SCAN_CANCEL = new ImageFile("scan_cancel", "png");
	public static final ImageFile SECURITY = new ImageFile("security", "png");
	public static final ImageFile SEARCH = new ImageFile("search", "png");
	public static final ImageFile SERVICE = new ImageFile("service", "png");
	public static final ImageFile SERVICE_ADD = new ImageFile("service_add", "png");
	public static final ImageFile SERVICE_EDIT = new ImageFile("service_edit", "png");
	public static final ImageFile SHELF = new ImageFile("shelf", "png");
	public static final ImageFile SWAP = new ImageFile("swap", "png");
	public static final ImageFile TRADE_IN = new ImageFile("tradeIn", "png");
	public static final ImageFile TRANSACTION = new ImageFile("transaction", "png");
	public static final ImageFile USER_ADD = new ImageFile("userAdd", "png");
	public static final ImageFile USER_EDIT = new ImageFile("userEdit", "png");
	public static final ImageFile USER_INFO = new ImageFile("userInfo", "png");
	public static final ImageFile USER_SEARCH = new ImageFile("userSearch", "png");
	public static final ImageFile USER_SECURITY = new ImageFile("userSecurity", "png");
	public static final ImageFile USER_SECURITY_ADD = new ImageFile("userSecurity_add", "png");

	private final String imageName;
	private final String extension;

	private ImageFile(String imageName, String extension) {
		this.imageName = imageName;
		this.extension = extension;
	}

	String imageName() {
		return imageName;
	}

	String extension() {
		return extension;
	}
}
