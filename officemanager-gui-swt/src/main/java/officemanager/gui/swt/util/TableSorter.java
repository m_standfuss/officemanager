package officemanager.gui.swt.util;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.gui.swt.ui.tables.OfficeManagerTable;
import officemanager.utility.Tuple;

public class TableSorter {

	private final OfficeManagerTable table;
	private List<String> dataKeys;
	private List<Integer> skipColumns;

	/**
	 * Creates a new TableHelper to perform actions on the table provided.
	 * 
	 * @param table
	 *            The table.
	 */
	public TableSorter(OfficeManagerTable table) {
		checkNotNull(table);
		this.table = table;
		dataKeys = Lists.newArrayList();
		skipColumns = Lists.newArrayList();
	}

	/**
	 * Sets the keys of the data objects to pull off the rows and place back on
	 * the new rows after sorting.
	 * 
	 * @param dataKeys
	 *            The keys to use to get data off the row to place on the new
	 *            row after sorting.
	 */
	public void setDataToSave(List<String> dataKeys) {
		checkNotNull(dataKeys);
		this.dataKeys = dataKeys;
	}

	/**
	 * Adds a key of the data objects to pull off the rows and place back on the
	 * new rows after sorting.
	 * 
	 * @param dataKey
	 *            The key to use to get data off the row to place on the new row
	 *            after sorting.
	 */
	public void addDataToSave(String dataKey) {
		checkNotNull(dataKey);
		dataKeys.add(dataKey);
	}

	/**
	 * Sets the columns to be skipped when packing the table after a sort. For
	 * use when a column is hidden/disabled.
	 * 
	 * @param columnsToSkip
	 *            The columns to skip.
	 */
	public void setColumnsToSkipOnPack(List<Integer> columnsToSkip) {
		checkNotNull(columnsToSkip);
		skipColumns = columnsToSkip;
	}

	/**
	 * Adds a column index to skip when packing the table after a sort. For use
	 * when a column is hidden/disabled.
	 * 
	 * @param columnToSkip
	 *            The index of the column to skip.
	 */
	public void addColumnToSkipOnPack(int columnToSkip) {
		skipColumns.add(columnToSkip);
	}

	/**
	 * Adds a default sorting listener (by the text of the cells) to the columns
	 * header click
	 * 
	 * @param columnIndex
	 *            The index of the column to add the listener to.
	 */
	public void addDefaultSortListener(int columnIndex) {
		checkArgument(columnIndex >= 0 && columnIndex < table.getColumnCount());
		table.getColumn(columnIndex).addListener(SWT.Selection, new Listener() {

			private boolean reverse;

			@Override
			public void handleEvent(Event event) {
				sortTable(columnIndex, reverse);
				packTableColumns();
				reverse = !reverse;
			}
		});
	}

	/**
	 * Adds a sorter based on some data object on the table item. If the data
	 * object does not implement Comparable then the sorting is unrealiable.
	 * 
	 * @param columnIndex
	 *            The column index to add the sorting listener to.
	 * @param key
	 *            The key of the data object to use to sort.
	 */
	public void addDataBasedSortListener(int columnIndex, String key) {
		checkArgument(columnIndex >= 0 && columnIndex < table.getColumnCount());
		table.getColumn(columnIndex).addListener(SWT.Selection, new Listener() {

			private boolean reverse;

			@Override
			public void handleEvent(Event event) {
				sortTable(key, reverse);
				packTableColumns();
				reverse = !reverse;
			}
		});
	}

	/**
	 * Sorts the table based on the text of the cell at the index.
	 * 
	 * @param index
	 *            The index to sort by text on.
	 */
	public void sortTable(int index, boolean ascending) {
		checkArgument(index >= 0 && index < table.getColumnCount());
		final List<TableItem> tableItems = Lists.newArrayList(table.getItems());
		Collections.sort(tableItems, new TableItemComparator(index, ascending));
		repopulateTable(tableItems);
	}

	/**
	 * Sorts the rows of the Table based on getting data from the key provided.
	 * 
	 * Note that if the data Object does not implement Comparable then the
	 * sorting is unreliable.
	 * 
	 * @param key
	 *            The key to sort on.
	 */
	public void sortTable(String key, boolean ascending) {
		checkNotNull(key);
		final List<TableItem> tableItems = Lists.newArrayList(table.getItems());
		Collections.sort(tableItems, new TableItemComparator(key, ascending));
		repopulateTable(tableItems);
	}

	/**
	 * Packs the table's columns skipping any column that has in index on the
	 * skipColumns parameter list.
	 * 
	 * Great whenever a column has really long text but you don't want to wrap
	 * it.
	 */
	private void packTableColumns() {
		for (int i = 0; i < table.getColumnCount(); i++) {
			if (skipColumns.contains(i)) {
				continue;
			}
			table.getColumn(i).pack();
		}
	}

	/**
	 * Removes all the items on the table and re-populates them in the order
	 * provided by the list, saving all the data in the keys dataKeys list.
	 * 
	 * @param tableItems
	 *            The order of the new table items to populate.
	 */
	private void repopulateTable(List<TableItem> tableItems) {
		final List<TableRow> rows = Lists.newArrayList();
		for (final TableItem item : tableItems) {
			final String[] rowText = getRowText(item);
			final List<Tuple<String, Object>> keysAndObjects = getKeysAndObjects(item);
			final TableRow row = new TableRow(rowText, keysAndObjects);
			rows.add(row);
		}

		table.removeAll();

		for (final TableRow row : rows) {
			final TableItem newItem = new TableItem(table, SWT.NONE);
			newItem.setText(row.getRowText());
			if (dataKeys != null) {
				for (final Tuple<String, Object> dataKeyAndObject : row.getKeysAndObjects()) {
					newItem.setData(dataKeyAndObject.item1, dataKeyAndObject.item2);
				}
			}
		}
	}

	/**
	 * Gets the rows text for all the columns.
	 * 
	 * @param item
	 *            The items to get text for.
	 * @return The rows text.
	 */
	private String[] getRowText(TableItem item) {
		final String[] rowText = new String[table.getColumnCount()];
		for (int i = 0; i < table.getColumnCount(); i++) {
			rowText[i] = item.getText(i);
		}
		return rowText;
	}

	/**
	 * Gets the data keys and objects off the table item.
	 * 
	 * @param item
	 *            The item to get data off of.
	 * @return The list of tuples from key to data.
	 */
	private List<Tuple<String, Object>> getKeysAndObjects(TableItem item) {
		final List<Tuple<String, Object>> keysAndObjects = Lists.newArrayList();
		for (final String key : dataKeys) {
			final Object data = item.getData(key);
			keysAndObjects.add(new Tuple<String, Object>(key, data));
		}
		return keysAndObjects;
	}

	/**
	 * Inner private class for making the
	 * 
	 * @author Mike
	 *
	 */
	private class TableItemComparator implements Comparator<TableItem> {

		private final boolean ascending;
		private final String key;
		private final Integer index;

		public TableItemComparator(int index, boolean ascending) {
			this.ascending = ascending;
			key = null;
			this.index = index;
		}

		public TableItemComparator(String key, boolean ascending) {
			this.ascending = ascending;
			this.key = key;
			index = null;
		}

		@Override
		public int compare(TableItem item1, TableItem item2) {
			if (index == null && key == null) {
				// Shouldnt happen since this is a private class we control
				// when/how its used but just incase
				return 0;
			}
			if (key == null) {
				// Use the index to compare the column's text
				return qualifier() * item1.getText(index).compareTo(item2.getText(index));
			}
			// key is not null use it to sort.
			final Object object1 = item1.getData(key);
			final Object object2 = item2.getData(key);
			return qualifier() * compareObjects(object1, object2);
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		private int compareObjects(Object object1, Object object2) {
			if (object1 == null) {
				if (object2 == null) {
					return 0;
				}
				if (!(object2 instanceof Comparable)) {
					return 0;
				}
				return -1;
			}
			if (object2 == null) {
				if (!(object1 instanceof Comparable)) {
					return 0;
				}
				return 1;
			}
			if (!(object1 instanceof Comparable && object2 instanceof Comparable)) {
				return 0;
			}
			return ((Comparable) object1).compareTo(object2);
		}

		private int qualifier() {
			return ascending ? 1 : -1;
		}
	}

	private class TableRow {

		private final String[] rowText;
		private final List<Tuple<String, Object>> keysAndObjects;

		public TableRow(String[] rowText, List<Tuple<String, Object>> keysAndObjects) {
			this.rowText = rowText;
			this.keysAndObjects = keysAndObjects;
		}

		public String[] getRowText() {
			return rowText;
		}

		public List<Tuple<String, Object>> getKeysAndObjects() {
			return keysAndObjects;
		}
	}
}
