package officemanager.gui.swt.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.TableItem;

import com.google.common.collect.Lists;

import officemanager.gui.swt.ui.tables.OfficeManagerTable;

public class TableWriter {

	private static final Logger logger = LogManager.getLogger(TableWriter.class);

	private final OfficeManagerTable table;

	public TableWriter(OfficeManagerTable table) {
		this.table = table;
	}

	public void writeToFile() {
		List<Integer> columns = Lists.newArrayList();
		for (int i = 0; i < table.getColumnCount(); i++) {
			columns.add(i);
		}
		writeToFile(columns);
	}

	public void writeToFile(List<Integer> columns) {
		String file = getFileName();
		if (file == null) {
			return;
		}
		CSVPrinter printer = null;
		FileWriter fileWriter = null;
		try {
			try {
				fileWriter = new FileWriter(file);
				printer = new CSVPrinter(fileWriter, CSVFormat.EXCEL);
				printer.printRecord(getHeaders(columns));
				for (TableItem item : table.getItems()) {
					printer.printRecord(getRowValues(item, columns));
				}
			} catch (IOException e) {
				MessageDialogs.displayError(table.getShell(), "Error Occured",
						"An error occured while exporting the values.");
				logger.error("An error occurred while attempting to export a table.", e);
				return;
			}
		} finally {
			if (fileWriter != null) {
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e) {
					logger.error("An error occurred while attempting close the file writer.", e);
				}
			}
			if (printer != null) {
				try {
					printer.close();
				} catch (IOException e) {
					logger.error("An error occurred while attempting close the csv printer.", e);
				}
			}
		}
		MessageDialogs.displayMessage(table.getShell(), "Successfully Exported",
				"The data was successfully exported to file.");
	}

	private List<String> getHeaders(List<Integer> columns) {
		List<String> headers = Lists.newArrayList();
		for (int i = 0; i < table.getColumnCount(); i++) {
			if (columns.contains(i)) {
				headers.add(table.getColumn(i).getText());
			}
		}
		return headers;
	}

	private List<String> getRowValues(TableItem item, List<Integer> columns) {
		List<String> values = Lists.newArrayList();
		for (int i = 0; i < table.getColumnCount(); i++) {
			if (columns.contains(i)) {
				values.add(item.getText(i));
			}
		}
		return values;
	}

	private String getFileName() {
		FileDialog dialog = new FileDialog(table.getShell(), SWT.SAVE);
		dialog.setFilterNames(new String[] { "CSV" });
		dialog.setFilterExtensions(new String[] { "*.csv;" });
		dialog.setFilterPath("/");
		dialog.setFileName("export");
		return dialog.open();
	}
}
