package officemanager.gui.swt.printing;

import java.util.Date;
import java.util.List;

import net.sf.paperclips.DefaultGridLook;
import net.sf.paperclips.EmptyPrint;
import net.sf.paperclips.GridPrint;
import net.sf.paperclips.Print;
import net.sf.paperclips.TextPrint;
import officemanager.biz.PaymentsTotal;
import officemanager.biz.records.OrderSearchRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.gui.swt.AppPrefs;

public class TotalsReportPrint extends Printable {

	private final Date startDttm;
	private final Date endDttm;
	private final PaymentsTotal paymentsTotal;
	private final List<PaymentRecord> payments;
	private final List<OrderSearchRecord> openedOrders;
	private final List<OrderSearchRecord> closedOrders;
	private final List<PriceOverrideRecord> priceOverrides;

	public TotalsReportPrint(Date startDttm, Date endDttm, PaymentsTotal paymentsTotal, List<PaymentRecord> payments,
			List<OrderSearchRecord> openedOrders, List<OrderSearchRecord> closedOrders,
			List<PriceOverrideRecord> priceOverrides) {
		this.startDttm = startDttm;
		this.endDttm = endDttm;
		this.paymentsTotal = paymentsTotal;
		this.payments = payments;
		this.openedOrders = openedOrders;
		this.closedOrders = closedOrders;
		this.priceOverrides = priceOverrides;
	}

	@Override
	public Print getPrint() {
		return createPrint();
	}

	private Print createPrint() {
		GridPrint masterGrid = new GridPrint("c:d:g", NO_BORDER_LOOK);
		masterGrid.add(makeHeaderGrid());
		if (paymentsTotal != null || payments != null) {
			masterGrid.add(new EmptyPrint(8, 8));
			masterGrid.add(makePaymentsGrid());
		}
		if (priceOverrides != null) {
			masterGrid.add(new EmptyPrint(8, 8));
			masterGrid.add(makePriceOverridesGrid());
		}
		if (openedOrders != null) {
			masterGrid.add(new EmptyPrint(8, 8));
			masterGrid.add(makeOpenOrdersGrid());
		}
		if (closedOrders != null) {
			masterGrid.add(new EmptyPrint(8, 8));
			masterGrid.add(makeClosedOrdersGrid());
		}
		return masterGrid;
	}

	private GridPrint makeHeaderGrid() {
		GridPrint header = new GridPrint("c:d:g", NO_BORDER_LOOK);
		header.add(new TextPrint("TOTALS REPORT", STYLE_HEADER_BOLD_UNDERLINED));
		header.add(new TextPrint(
				AppPrefs.dateFormatter.formatDate(startDttm) + "-" + AppPrefs.dateFormatter.formatDate(endDttm),
				FONT_VALUE));
		return header;
	}

	private GridPrint makePaymentsGrid() {
		GridPrint grid = new GridPrint("l:d:g", NO_BORDER_LOOK);
		grid.add(new TextPrint("Payments", STYLE_HEADER2_BOLD_UNDERLINED));

		if (paymentsTotal != null) {
			grid.add(makeTotalsGrid());
			grid.add(new EmptyPrint(8, 8));
		}
		if (payments != null) {
			grid.add(makePaymentsTable());
		}

		return grid;
	}

	private GridPrint makeTotalsGrid() {
		DefaultGridLook look = new DefaultGridLook();
		look.setCellPadding(4, 2);
		GridPrint totalsGrid = new GridPrint("d, d, d, d, d, d, d, d", look);
		totalsGrid.add(new TextPrint("Total:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.total), STYLE_VALUE));
		totalsGrid.add(new TextPrint(), 6);
		totalsGrid.add(new TextPrint("Cash:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.cashTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Check:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.checkTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Client Credit:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.clientCreditTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Credit Card:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.creditCardTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Financing:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.financingTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Gift Card:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.giftCardTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Pending:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.pendingTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Trade In:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.tradeInTotal), STYLE_VALUE));

		totalsGrid.add(
				new TextPrint("*The Total does not include Client Credit, Gift Card or Pending payments.", FONT_FINE),
				8);
		return totalsGrid;
	}

	private GridPrint makePaymentsTable() {
		GridPrint table;
		if (payments.isEmpty()) {
			table = new GridPrint("d", NO_BORDER_LOOK);
			table.add(new TextPrint("No payments were made.", FONT_TITLE));
		} else {
			table = new GridPrint("d, d, d, d, d", BORDER_LOOK);
			table.add(new TextPrint("Amount", FONT_TITLE));
			table.add(new TextPrint("Date", FONT_TITLE));
			table.add(new TextPrint("Type", FONT_TITLE));
			table.add(new TextPrint("Nbr.", FONT_TITLE));
			table.add(new TextPrint("Description", FONT_TITLE));
			for (PaymentRecord payment : payments) {
				table.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(payment.getAmount()), FONT_VALUE));
				table.add(
						new TextPrint(AppPrefs.dateFormatter.formatFullDate_DB(payment.getCreatedDtTm()), FONT_VALUE));
				table.add(new TextPrint(payment.getPaymentTypeDisplay(), FONT_VALUE));
				table.add(new TextPrint(payment.getPaymentNumber(), FONT_VALUE));
				table.add(new TextPrint(payment.getPaymentDescription(), FONT_VALUE));
			}
		}
		return table;
	}

	private GridPrint makeOpenOrdersGrid() {
		GridPrint table;
		if (openedOrders.isEmpty()) {
			table = new GridPrint("d", NO_BORDER_LOOK);
			table.add(new TextPrint("No orders were opened.", FONT_TITLE));
		} else {
			table = new GridPrint("d, d, d, d, d, d", BORDER_LOOK);
			table.add(new TextPrint("Order #", FONT_TITLE));
			table.add(new TextPrint("Order Type", FONT_TITLE));
			table.add(new TextPrint("Order Status", FONT_TITLE));
			table.add(new TextPrint("Client", FONT_TITLE));
			table.add(new TextPrint("Opened On", FONT_TITLE));
			table.add(new TextPrint("Opened By", FONT_TITLE));

			for (OrderSearchRecord record : openedOrders) {
				table.add(new TextPrint(AppPrefs.FORMATTER.formatOrderNumber(record.getOrderId()), FONT_VALUE));
				table.add(new TextPrint(record.getOrderType(), FONT_VALUE));
				table.add(new TextPrint(record.getOrderStatus(), FONT_VALUE));
				table.add(new TextPrint(record.getClientName(), FONT_VALUE));
				table.add(new TextPrint(AppPrefs.dateFormatter.formatFullDate_DB(record.getOpenedDttm()), FONT_VALUE));
				table.add(new TextPrint(record.getOpenedBy(), FONT_VALUE));
			}
		}

		GridPrint grid = new GridPrint("l:d:g", NO_BORDER_LOOK);
		grid.add(new TextPrint("Orders Opened", STYLE_HEADER2_BOLD_UNDERLINED));
		grid.add(table);
		return grid;
	}

	private GridPrint makeClosedOrdersGrid() {
		GridPrint table;
		if (closedOrders.isEmpty()) {
			table = new GridPrint("d", NO_BORDER_LOOK);
			table.add(new TextPrint("No orders were closed.", FONT_TITLE));
		} else {
			table = new GridPrint("d, d, d, d, d, d", BORDER_LOOK);
			table.add(new TextPrint("Order #", FONT_TITLE));
			table.add(new TextPrint("Order Type", FONT_TITLE));
			table.add(new TextPrint("Order Status", FONT_TITLE));
			table.add(new TextPrint("Client", FONT_TITLE));
			table.add(new TextPrint("Closed On", FONT_TITLE));
			table.add(new TextPrint("Closed By", FONT_TITLE));

			for (OrderSearchRecord record : closedOrders) {
				table.add(new TextPrint(AppPrefs.FORMATTER.formatOrderNumber(record.getOrderId()), FONT_VALUE));
				table.add(new TextPrint(record.getOrderType(), FONT_VALUE));
				table.add(new TextPrint(record.getOrderStatus(), FONT_VALUE));
				table.add(new TextPrint(record.getClientName(), FONT_VALUE));
				table.add(new TextPrint(AppPrefs.dateFormatter.formatFullDate_DB(record.getClosedDttm()), FONT_VALUE));
				table.add(new TextPrint(record.getClosedBy(), FONT_VALUE));
			}
		}

		GridPrint grid = new GridPrint("l:d:g", NO_BORDER_LOOK);
		grid.add(new TextPrint("Closed Orders", STYLE_HEADER2_BOLD_UNDERLINED));
		grid.add(table);
		return grid;
	}

	private GridPrint makePriceOverridesGrid() {
		GridPrint table;
		if (priceOverrides.isEmpty()) {
			table = new GridPrint("d", NO_BORDER_LOOK);
			table.add(new TextPrint("No prices were overriden.", FONT_TITLE));
		} else {
			table = new GridPrint("d, d, d, d, d, d", BORDER_LOOK);
			table.add(new TextPrint("Order #", FONT_TITLE));
			table.add(new TextPrint("Orig. Price", FONT_TITLE));
			table.add(new TextPrint("New Price", FONT_TITLE));
			table.add(new TextPrint("Date", FONT_TITLE));
			table.add(new TextPrint("Personnel", FONT_TITLE));
			table.add(new TextPrint("Reason", FONT_TITLE));
			for (PriceOverrideRecord record : priceOverrides) {
				table.add(new TextPrint(AppPrefs.FORMATTER.formatOrderNumber(record.getOrderId()), FONT_VALUE));
				table.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(record.getOriginalAmount()), FONT_VALUE));
				table.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(record.getOverrideAmount()), FONT_VALUE));
				table.add(
						new TextPrint(AppPrefs.dateFormatter.formatFullDate_DB(record.getOverrideDttm()), FONT_VALUE));
				table.add(new TextPrint(record.getOverridePrsnlName(), FONT_VALUE));
				table.add(new TextPrint(record.getOverrideReason(), FONT_VALUE));
			}
		}

		GridPrint grid = new GridPrint("l:d:g", NO_BORDER_LOOK);
		grid.add(new TextPrint("Prices Overriden", STYLE_HEADER2_BOLD_UNDERLINED));
		grid.add(table);
		return grid;
	}
}
