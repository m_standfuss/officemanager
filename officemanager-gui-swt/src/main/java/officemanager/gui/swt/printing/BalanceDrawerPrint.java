package officemanager.gui.swt.printing;

import java.util.Date;
import java.util.List;

import net.sf.paperclips.DefaultGridLook;
import net.sf.paperclips.EmptyPrint;
import net.sf.paperclips.GridPrint;
import net.sf.paperclips.Print;
import net.sf.paperclips.TextPrint;
import officemanager.biz.PaymentsTotal;
import officemanager.biz.records.PaymentRecord;
import officemanager.gui.swt.AppPrefs;

public class BalanceDrawerPrint extends Printable {

	private final PaymentsTotal paymentsTotal;
	private final List<PaymentRecord> payments;

	public BalanceDrawerPrint(PaymentsTotal paymentsTotal, List<PaymentRecord> payments) {
		this.paymentsTotal = paymentsTotal;
		this.payments = payments;
	}

	@Override
	public Print getPrint() {
		return createPrint();
	}

	private Print createPrint() {
		GridPrint masterGrid = new GridPrint("c:d:g", NO_BORDER_LOOK);
		masterGrid.add(makeHeaderGrid());
		if (paymentsTotal != null || payments != null) {
			masterGrid.add(new EmptyPrint(8, 8));
			masterGrid.add(makePaymentsGrid());
		}
		return masterGrid;
	}

	private GridPrint makeHeaderGrid() {
		GridPrint header = new GridPrint("c:d:g", NO_BORDER_LOOK);
		header.add(new TextPrint("DRAWER REPORT", STYLE_HEADER_BOLD_UNDERLINED));
		header.add(new TextPrint(AppPrefs.dateFormatter.formatDate(new Date())));
		return header;
	}

	private GridPrint makePaymentsGrid() {
		GridPrint grid = new GridPrint("l:d:g", NO_BORDER_LOOK);
		grid.add(new TextPrint("Payments", STYLE_HEADER2_BOLD_UNDERLINED));

		if (paymentsTotal != null) {
			grid.add(makeTotalsGrid());
			grid.add(new EmptyPrint(12, 12));
		}
		if (payments != null) {
			grid.add(makePaymentsTable());
		}

		return grid;
	}

	private GridPrint makeTotalsGrid() {
		DefaultGridLook look = new DefaultGridLook();
		look.setCellPadding(4, 2);
		GridPrint totalsGrid = new GridPrint("d, d, d, d, d, d, d, d", look);
		totalsGrid.add(new TextPrint("Total:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.total), STYLE_VALUE));
		totalsGrid.add(new TextPrint(), 6);
		totalsGrid.add(new TextPrint("Cash:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.cashTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Check:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.checkTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Client Credit:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.clientCreditTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Credit Card:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.creditCardTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Financing:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.financingTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Gift Card:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.giftCardTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Pending:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.pendingTotal), STYLE_VALUE));
		totalsGrid.add(new TextPrint("Trade In:", STYLE_TITLE));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(paymentsTotal.tradeInTotal), STYLE_VALUE));

		totalsGrid.add(
				new TextPrint("*The Total does not include Client Credit, Gift Card or Pending payments.", FONT_FINE),
				8);
		return totalsGrid;
	}

	private GridPrint makePaymentsTable() {
		GridPrint table;
		if (payments.isEmpty()) {
			table = new GridPrint("d", NO_BORDER_LOOK);
			table.add(new TextPrint("No payments were made.", FONT_TITLE));
		} else {
			table = new GridPrint("d, d, d, d, d", BORDER_LOOK);
			table.add(new TextPrint("Amount", FONT_TITLE));
			table.add(new TextPrint("Date", FONT_TITLE));
			table.add(new TextPrint("Type", FONT_TITLE));
			table.add(new TextPrint("Nbr.", FONT_TITLE));
			table.add(new TextPrint("Description", FONT_TITLE));
			for (PaymentRecord payment : payments) {
				table.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(payment.getAmount()), FONT_VALUE));
				table.add(
						new TextPrint(AppPrefs.dateFormatter.formatFullDate_DB(payment.getCreatedDtTm()), FONT_VALUE));
				table.add(new TextPrint(payment.getPaymentTypeDisplay(), FONT_VALUE));
				table.add(new TextPrint(payment.getPaymentNumber(), FONT_VALUE));
				table.add(new TextPrint(payment.getPaymentDescription(), FONT_VALUE));
			}
		}
		return table;
	}

}
