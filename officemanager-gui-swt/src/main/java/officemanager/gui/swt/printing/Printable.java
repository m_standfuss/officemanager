package officemanager.gui.swt.printing;

import java.util.Date;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;

import net.sf.paperclips.DefaultGridLook;
import net.sf.paperclips.EmptyPrint;
import net.sf.paperclips.GridLook;
import net.sf.paperclips.GridPrint;
import net.sf.paperclips.ImagePrint;
import net.sf.paperclips.LineBorder;
import net.sf.paperclips.PageDecoration;
import net.sf.paperclips.PaperClips;
import net.sf.paperclips.Print;
import net.sf.paperclips.SimplePageDecoration;
import net.sf.paperclips.TextPrint;
import net.sf.paperclips.TextStyle;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.gui.swt.util.ImageFile;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public abstract class Printable implements IPrintable {

	protected static final int DEFAULT_MARGINS = 72;

	protected static final String FONT = "Helvetica";

	protected static final Image LOGO_HEADER = ResourceManager.getImage(ImageFile.LOGO_HEADER, 400);

	protected static final RGB TEXT_FAINT = new RGB(125, 125, 125);

	protected static final FontData FONT_HEADER = new FontData(FONT, 14, SWT.NORMAL);
	protected static final FontData FONT_HEADER_BOLD = new FontData(FONT, 14, SWT.BOLD);

	protected static final FontData FONT_HEADER2 = new FontData(FONT, 12, SWT.NORMAL);
	protected static final FontData FONT_HEADER2_BOLD = new FontData(FONT, 12, SWT.BOLD);

	protected static final FontData FONT_TITLE = new FontData(FONT, 11, SWT.BOLD);
	protected static final FontData FONT_VALUE = new FontData(FONT, 10, SWT.NORMAL);

	protected static final FontData FONT_FINE = new FontData(FONT, 7, SWT.ITALIC);

	protected static final TextStyle STYLE_HEADER_BOLD_UNDERLINED = new TextStyle().font(FONT_HEADER_BOLD).underline();
	protected static final TextStyle STYLE_HEADER2_BOLD_UNDERLINED = new TextStyle().font(FONT_HEADER2_BOLD)
			.underline();
	protected static final TextStyle STYLE_TITLE = new TextStyle().font(FONT_TITLE);
	protected static final TextStyle STYLE_VALUE = new TextStyle().font(FONT_VALUE);
	protected static final TextStyle STYLE_FOOTER_VALUE = new TextStyle().font(FONT_VALUE).fontStyle(SWT.ITALIC)
			.foreground(TEXT_FAINT);

	protected static final DefaultGridLook NO_BORDER_LOOK = new DefaultGridLook();
	protected static final DefaultGridLook BORDER_LOOK = new DefaultGridLook();

	static {
		BORDER_LOOK.setCellBorder(new LineBorder());
	}

	@Override
	public int getMargins() {
		return DEFAULT_MARGINS;
	}

	@Override
	public int getOrientation() {
		return PaperClips.ORIENTATION_DEFAULT;
	}

	protected GridPrint makeCompanyHeader() {
		GridPrint header = new GridPrint("c:p:g", NO_BORDER_LOOK);
		header.add(new ImagePrint(LOGO_HEADER.getImageData(), LOGO_HEADER.getDevice().getDPI()));
		if (!StringUtility.isNullOrWhiteSpace(AppPrefs.companyAddress)) {
			String[] lines = AppPrefs.companyAddress.split(System.lineSeparator());
			for (String line : lines) {
				header.add(new TextPrint(line, FONT_HEADER));
			}
		}
		if (!StringUtility.isNullOrWhiteSpace(AppPrefs.companyPhone)) {
			header.add(new TextPrint(AppPrefs.companyPhone, FONT_HEADER_BOLD));
		}
		if (!StringUtility.isNullOrWhiteSpace(AppPrefs.companyWebsite)) {
			header.add(new TextPrint(AppPrefs.companyWebsite, FONT_HEADER));
		}
		return header;
	}

	protected TextStyle decreaseFontSize(TextStyle style, int amount) {
		FontData fd = style.getFontData();
		if (fd.getHeight() <= amount) {
			throw new IllegalStateException("Unable to decrease the font size by specified amount.");
		}
		return style.font(fd.getName(), fd.getHeight() - amount, fd.getStyle());
	}

	protected Print makeCommentGrid(List<OrderCommentRecord> comments) {
		return makeCommentGrid(comments, STYLE_TITLE, STYLE_VALUE);
	}

	protected Print makeCommentGrid(List<OrderCommentRecord> comments, TextStyle titleStyle, TextStyle valueStyle) {
		return makeCommentGrid(comments, BORDER_LOOK, titleStyle, valueStyle);
	}

	protected Print makeCommentGrid(List<OrderCommentRecord> comments, GridLook gridLook, TextStyle titleStyle,
			TextStyle valueStyle) {
		GridPrint print = new GridPrint("d:g", NO_BORDER_LOOK);
		if (!CollectionUtility.isNullOrEmpty(comments)) {
			for (OrderCommentRecord orderComment : comments) {
				StringBuilder sb = new StringBuilder();
				sb.append(orderComment.getAuthoredBy()).append("-")
						.append(AppPrefs.dateFormatter.formatFullDate_DB(orderComment.getLastUpdtOn()));
				print.add(new TextPrint(sb.toString(), valueStyle));
				print.add(new TextPrint(orderComment.getLongText().getLongText(), valueStyle));
				print.add(new EmptyPrint(5, 5));
			}
		}
		GridPrint master = new GridPrint("d:g", gridLook);
		master.addHeader(new TextPrint("COMMENTS", titleStyle));
		master.add(print);
		return master;
	}

	protected PageDecoration makePrintedOnFooter() {
		GridPrint footer = new GridPrint("l:d:g", NO_BORDER_LOOK);
		footer.add(new EmptyPrint(8, 8));
		footer.add(SWT.RIGHT, SWT.BOTTOM, new TextPrint(
				"Printed On: " + AppPrefs.dateFormatter.formatFullDate_DB(new Date()), STYLE_FOOTER_VALUE));
		PageDecoration footers = new SimplePageDecoration(footer);
		return footers;
	}
}
