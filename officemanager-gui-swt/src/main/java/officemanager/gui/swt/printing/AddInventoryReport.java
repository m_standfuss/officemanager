package officemanager.gui.swt.printing;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

import net.sf.paperclips.EmptyPrint;
import net.sf.paperclips.GridPrint;
import net.sf.paperclips.Print;
import net.sf.paperclips.StyledTextPrint;
import net.sf.paperclips.TextPrint;
import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.AppPrefs;

public class AddInventoryReport extends Printable {

	private final List<PartSearchRecord> inventoryAdded;

	public AddInventoryReport(List<PartSearchRecord> inventoryAdded) {
		this.inventoryAdded = inventoryAdded;
	}

	@Override
	public Print getPrint() {
		return createPrint();
	}

	private Print createPrint() {
		GridPrint masterGrid = new GridPrint("l:d:g", NO_BORDER_LOOK);
		masterGrid.add(makeHeaderGrid());

		if (inventoryAdded != null) {
			masterGrid.add(new EmptyPrint(8, 8));
			if (inventoryAdded.isEmpty()) {
				masterGrid.add(new TextPrint("No parts added to inventory.", FONT_FINE));
			} else {
				masterGrid.add(makePartsGrid(inventoryAdded));
			}
		}
		return masterGrid;
	}

	private Print makeHeaderGrid() {
		return new StyledTextPrint().append("INVENTORY ADDED", STYLE_HEADER_BOLD_UNDERLINED)
				.append(" added on " + AppPrefs.dateFormatter.formatDate(new Date()), STYLE_VALUE);
	}

	private GridPrint makePartsGrid(List<PartSearchRecord> parts) {

		Map<Long, PartSearchRecord> partMap = Maps.newHashMap();
		Map<Long, Integer> partAmount = Maps.newHashMap();

		for (PartSearchRecord part : parts) {
			partMap.put(part.getPartId(), part);
			Integer count = partAmount.get(part.getPartId());
			if (count == null) {
				count = 1;
			} else {
				count = count + 1;
			}
			partAmount.put(part.getPartId(), count);
		}

		GridPrint table = new GridPrint("d, d, d, d", BORDER_LOOK);
		table.add(new TextPrint("Part", FONT_TITLE));
		table.add(new TextPrint("Manuf.", FONT_TITLE));
		table.add(new TextPrint("Manuf. ID", FONT_TITLE));
		table.add(new TextPrint("Amount", FONT_TITLE));
		for (PartSearchRecord part : partMap.values()) {
			table.add(new TextPrint(part.getPartName(), FONT_VALUE));
			table.add(new TextPrint(part.getPartManufacturer(), FONT_VALUE));
			table.add(new TextPrint(part.getManufacturerId(), FONT_VALUE));
			Integer amount = partAmount.get(part.getPartId());
			table.add(new TextPrint(String.valueOf(amount), FONT_VALUE));
		}
		return table;
	}
}
