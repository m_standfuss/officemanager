package officemanager.gui.swt.printing;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import net.sf.paperclips.GridPrint;
import net.sf.paperclips.PagePrint;
import net.sf.paperclips.PaperClips;
import net.sf.paperclips.Print;
import net.sf.paperclips.StyledTextPrint;
import net.sf.paperclips.TextPrint;
import net.sf.paperclips.TextStyle;

public class TablePrint extends Printable {

	private static final int MARGINS = 36;

	private final String title;
	private final String[] headers;
	private final List<String[]> rows;

	private TextStyle currentHeaderTextStyle;
	private TextStyle currentTitleTextStyle;
	private TextStyle currentValueTextStyle;

	public TablePrint(String title, String[] headers, List<String[]> rows) {
		checkNotNull(title);
		checkNotNull(headers);
		checkNotNull(rows);

		checkArgument(headers.length > 0, "Must have at least one column to print.");
		for (String[] row : rows) {
			checkArgument(headers.length == row.length,
					"The row values must have the same number of columns as the header.");
		}

		this.title = title;
		this.headers = headers;
		this.rows = rows;

		currentHeaderTextStyle = decreaseFontSize(STYLE_HEADER_BOLD_UNDERLINED, 1);
		currentTitleTextStyle = decreaseFontSize(STYLE_TITLE, 1);
		currentValueTextStyle = decreaseFontSize(STYLE_VALUE, 1);
	}

	@Override
	public Print getPrint() {
		return createPrint();
	}

	@Override
	public int getMargins() {
		return MARGINS;
	}

	@Override
	public int getOrientation() {
		return PaperClips.ORIENTATION_LANDSCAPE;
	}

	public void decreaseFontSize() {
		currentHeaderTextStyle = decreaseFontSize(currentHeaderTextStyle, 1);
		currentTitleTextStyle = decreaseFontSize(currentTitleTextStyle, 1);
		currentValueTextStyle = decreaseFontSize(currentValueTextStyle, 1);
	}

	private Print createPrint() {
		GridPrint masterGrid = new GridPrint("l:d:g", NO_BORDER_LOOK);
		masterGrid.add(makeHeaderGrid());
		masterGrid.add(makeTableGrid());
		return new PagePrint(masterGrid, makePrintedOnFooter());
	}

	private Print makeHeaderGrid() {
		return new StyledTextPrint().append(title, currentHeaderTextStyle);
	}

	private Print makeTableGrid() {
		String columns = StringUtils.repeat("d", ", ", headers.length);

		GridPrint table = new GridPrint(columns, BORDER_LOOK);
		for (String header : headers) {
			table.addHeader(new TextPrint(header, STYLE_TITLE));
		}

		for (String[] row : rows) {
			for (String rowValue : row) {
				table.add(new TextPrint(rowValue, STYLE_VALUE));
			}
		}
		return table;
	}
}
