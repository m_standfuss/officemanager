package officemanager.gui.swt.printing;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.eclipse.swt.SWT;

import net.sf.paperclips.EmptyPrint;
import net.sf.paperclips.GridPrint;
import net.sf.paperclips.NoBreakPrint;
import net.sf.paperclips.PaperClips;
import net.sf.paperclips.Print;
import net.sf.paperclips.SeriesPrint;
import net.sf.paperclips.StyledTextPrint;
import net.sf.paperclips.TextPrint;
import net.sf.paperclips.TextStyle;
import officemanager.biz.records.ClientRecord;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.biz.records.PhoneRecord;
import officemanager.gui.swt.AppPrefs;

public class ShopTicketPrint extends Printable {

	private static final int MARGINS = 18;

	private static final int HALF_PAGE = 400;
	private static final int QUARTER_PAGE = 200;

	private final OrderRecord order;
	private final ClientRecord client;
	private final List<OrderCommentRecord> comments;

	private TextStyle currentHeaderTextStyle;
	private TextStyle currentTitleTextStyle;
	private TextStyle currentValueTextStyle;

	public ShopTicketPrint(OrderRecord order, ClientRecord client, List<OrderCommentRecord> comments) {
		checkNotNull(order);
		this.order = order;
		this.client = client;
		this.comments = comments;

		currentHeaderTextStyle = STYLE_HEADER_BOLD_UNDERLINED;
		currentTitleTextStyle = STYLE_TITLE;
		currentValueTextStyle = STYLE_VALUE;
	}

	public void decreaseFontSize() {
		currentHeaderTextStyle = decreaseFontSize(currentHeaderTextStyle, 1);
		currentTitleTextStyle = decreaseFontSize(currentTitleTextStyle, 1);
		currentValueTextStyle = decreaseFontSize(currentValueTextStyle, 1);
	}

	@Override
	public Print getPrint() {
		return createPrint();
	}

	@Override
	public int getMargins() {
		return MARGINS;
	}

	@Override
	public int getOrientation() {
		return PaperClips.ORIENTATION_LANDSCAPE;
	}

	private Print createPrint() {
		SeriesPrint masterGrid = new SeriesPrint();
		for (OrderClientProductRecord record : order.getClientProducts()) {

			GridPrint pagePrint = new GridPrint(HALF_PAGE + ", 36, " + HALF_PAGE, NO_BORDER_LOOK);
			GridPrint ticket = makeTicket(record);
			pagePrint.add(ticket);
			pagePrint.add(new EmptyPrint());
			pagePrint.add(ticket);
			masterGrid.add(new NoBreakPrint(pagePrint));
		}
		return masterGrid;
	}

	private Print makeHeaderGrid() {
		GridPrint header = new GridPrint("c:d:g", NO_BORDER_LOOK);
		StyledTextPrint headerText = new StyledTextPrint();
		headerText.append("SHOP TICKET", currentHeaderTextStyle);
		header.add(SWT.CENTER, headerText);
		return header;
	}

	private GridPrint makeTicket(OrderClientProductRecord record) {
		GridPrint ticket = new GridPrint("d:g", NO_BORDER_LOOK);
		ticket.add(makeHeaderGrid());
		ticket.add(new EmptyPrint(5, 5));
		ticket.add(makeTicketGrid(record));
		if (!AppPrefs.shopTicketCommentTypes.isEmpty()) {
			ticket.add(new EmptyPrint(5, 5));
			ticket.add(makeCommentGrid(comments, currentTitleTextStyle, currentValueTextStyle));
		}
		return ticket;
	}

	private Print makeTicketGrid(OrderClientProductRecord record) {
		GridPrint print = new GridPrint(QUARTER_PAGE + ", " + QUARTER_PAGE, BORDER_LOOK);
		GridPrint leftSide = new GridPrint("d:g", NO_BORDER_LOOK);

		if (client != null) {
			StyledTextPrint clientName = new StyledTextPrint();
			clientName.append("CLIENT: ", currentTitleTextStyle).append(client.getNameFullFormatted(),
					currentValueTextStyle);
			leftSide.add(clientName);

			PhoneRecord primaryPhone = client.getPrimaryPhone();
			if (primaryPhone != null) {
				StyledTextPrint phone = new StyledTextPrint();
				phone.append("PHONE: ", currentTitleTextStyle).append(AppPrefs.FORMATTER.formatPhoneNumber(
						primaryPhone.getPhoneNumber(), primaryPhone.getExtension()), currentValueTextStyle);
				leftSide.add(phone);
			}
		}

		StyledTextPrint orderNum = new StyledTextPrint();
		orderNum.append("ORDER #: ", currentTitleTextStyle)
				.append(AppPrefs.FORMATTER.formatOrderNumber(order.getOrderId()), currentValueTextStyle);
		leftSide.add(orderNum);

		StyledTextPrint openedDate = new StyledTextPrint();
		openedDate.append("OPENED ON: ", currentTitleTextStyle)
				.append(AppPrefs.dateFormatter.formatFullDate_DB(order.getCreatedDttm()), currentValueTextStyle);
		leftSide.add(openedDate);

		leftSide.add(new TextPrint(""));

		StyledTextPrint product = new StyledTextPrint();
		product.append("PRODUCT: ", currentTitleTextStyle).append(record.getProductType(), currentValueTextStyle);
		leftSide.add(product);

		StyledTextPrint manufacturer = new StyledTextPrint();
		manufacturer.append("MANUF.: ", currentTitleTextStyle).append(record.getProductManufacturer(),
				currentValueTextStyle);
		leftSide.add(manufacturer);

		StyledTextPrint model = new StyledTextPrint();
		model.append("MODEL: ", currentTitleTextStyle).append(record.getProductModel(), currentValueTextStyle);
		leftSide.add(model);

		print.add(leftSide);

		GridPrint rightSide = new GridPrint("d:g", NO_BORDER_LOOK);

		StyledTextPrint services = new StyledTextPrint();
		services.append("SERVICE(S): ", currentTitleTextStyle);
		rightSide.add(services);

		for (OrderServiceRecord service : order.getServices()) {
			if (record.getClientProductId().equals(service.getClientProductId())) {
				StringBuilder sb = new StringBuilder("- ");
				sb.append(service.getServiceName()).append(", ");
				if (service.getServiceText() != null && !service.getServiceText().getLongText().isEmpty()) {
					sb.append(service.getServiceText().getLongText());
				}

				rightSide.add(new TextPrint(sb.toString(), currentValueTextStyle));
			}
		}
		for (OrderFlatRateServiceRecord service : order.getFlatRates()) {
			if (record.getClientProductId().equals(service.getClientProductId())) {
				rightSide.add(new TextPrint("- " + service.getServiceName(), currentValueTextStyle));
			}
		}

		print.add(rightSide);
		return print;
	}
}
