package officemanager.gui.swt.printing;

import net.sf.paperclips.Print;

public interface IPrintable {
	public Print getPrint();

	public int getMargins();

	public int getOrientation();
}
