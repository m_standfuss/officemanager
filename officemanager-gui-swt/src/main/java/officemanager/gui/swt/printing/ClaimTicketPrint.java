package officemanager.gui.swt.printing;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import net.sf.paperclips.EmptyPrint;
import net.sf.paperclips.GridPrint;
import net.sf.paperclips.Print;
import net.sf.paperclips.StyledTextPrint;
import net.sf.paperclips.TextPrint;
import officemanager.biz.records.ClientRecord;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.PhoneRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.utility.StringUtility;

public class ClaimTicketPrint extends Printable {

	private final OrderRecord order;
	private final ClientRecord client;
	private final List<OrderCommentRecord> comments;

	public ClaimTicketPrint(OrderRecord order, ClientRecord client, List<OrderCommentRecord> comments) {
		checkNotNull(order);
		this.order = order;
		this.client = client;
		this.comments = comments;
	}

	@Override
	public Print getPrint() {
		return createPrint();
	}

	private Print createPrint() {
		GridPrint masterGrid = new GridPrint("d:g", NO_BORDER_LOOK);
		masterGrid.add(makeCompanyHeader());
		masterGrid.add(new EmptyPrint(16, 16));
		masterGrid.add(makeTicket());
		if (!AppPrefs.claimTicketCommentTypes.isEmpty()) {
			masterGrid.add(new EmptyPrint(8, 8));
			masterGrid.add(makeCommentGrid(comments));
		}
		return masterGrid;
	}

	private GridPrint makeTicket() {
		GridPrint ticket = new GridPrint("c:d:g", NO_BORDER_LOOK);
		ticket.add(new TextPrint("CLAIM TICKET", STYLE_HEADER_BOLD_UNDERLINED));
		ticket.add(new EmptyPrint(5, 5));

		GridPrint ticketGrid = new GridPrint("d:g(1), d:g(1)", BORDER_LOOK);

		GridPrint leftSide = new GridPrint("d:g", NO_BORDER_LOOK);

		if (client != null) {
			leftSide.add(new StyledTextPrint().append("NAME: ", STYLE_TITLE).append(client.getNameFullFormatted(),
					STYLE_VALUE));

			PhoneRecord primaryPhone = client.getPrimaryPhone();
			if (primaryPhone != null) {
				leftSide.add(new StyledTextPrint().append("PHONE: ", STYLE_TITLE).append(AppPrefs.FORMATTER
						.formatPhoneNumber(primaryPhone.getPhoneNumber(), primaryPhone.getExtension()), STYLE_VALUE));
				leftSide.add(new TextPrint());
			}
		}

		leftSide.add(new StyledTextPrint().append("ORDER #: ", STYLE_TITLE)
				.append(AppPrefs.FORMATTER.formatOrderNumber(order.getOrderId()), STYLE_VALUE));
		leftSide.add(new StyledTextPrint().append("DATE: ", STYLE_TITLE)
				.append(AppPrefs.dateFormatter.formatFullDate_DB(order.getCreatedDttm()), STYLE_VALUE));
		leftSide.add(new TextPrint());

		ticketGrid.add(leftSide);

		GridPrint rightSide = new GridPrint("d:g", NO_BORDER_LOOK);

		rightSide.add(new TextPrint("PRODUCT(S)", STYLE_TITLE));
		for (OrderClientProductRecord product : order.getClientProducts()) {
			StringBuilder productDisplay = new StringBuilder("(").append(product.getProductType()).append(") ")
					.append(product.getProductManufacturer());
			if (!StringUtility.isNullOrWhiteSpace(product.getProductModel())) {
				productDisplay.append(" - ").append(product.getProductModel());
			}
			rightSide.add(new TextPrint(productDisplay.toString(), STYLE_VALUE));
		}

		ticketGrid.add(rightSide);

		ticket.add(ticketGrid);
		return ticket;
	}
}
