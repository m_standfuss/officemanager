package officemanager.gui.swt.printing;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.sf.paperclips.CellBackgroundProvider;
import net.sf.paperclips.DefaultGridLook;
import net.sf.paperclips.EmptyPrint;
import net.sf.paperclips.GridPrint;
import net.sf.paperclips.LineBorder;
import net.sf.paperclips.PagePrint;
import net.sf.paperclips.Print;
import net.sf.paperclips.StyledTextPrint;
import net.sf.paperclips.TextPrint;
import net.sf.paperclips.TextStyle;
import officemanager.biz.Calculator;
import officemanager.biz.OrderTotal;
import officemanager.biz.records.AddressRecord;
import officemanager.biz.records.ClientRecord;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PhoneRecord;
import officemanager.gui.swt.AppPrefs;
import officemanager.utility.StringUtility;

public class OrderRecieptPrint extends Printable {

	private static final RGB BLACK = new RGB(0, 0, 0);
	private static final RGB WHITE = new RGB(255, 255, 255);
	private static final RGB ROW_DARK = new RGB(158, 159, 159);
	private static final RGB ROW_LIGHTER = new RGB(255, 255, 255);
	private static final RGB ROW_LIGHT = new RGB(210, 210, 211);

	private static final FontData FONT_TABLE_HEADER = new FontData(FONT, 9, SWT.BOLD);
	private static final FontData FONT_TABLE_SUBHEADER = new FontData(FONT, 8, SWT.BOLD);
	private static final FontData FONT_TABLE_VALUE = new FontData(FONT, 8, SWT.NORMAL);

	private static final TextStyle STYLE_TABLE_HEADER = new TextStyle().font(FONT_TABLE_HEADER).background(BLACK)
			.foreground(WHITE);
	private static final TextStyle STYLE_TABLE_VALUE = new TextStyle().font(FONT_TABLE_VALUE);

	private final OrderRecord order;
	private final ClientRecord client;
	private final List<PaymentRecord> payments;
	private final List<OrderCommentRecord> comments;

	public OrderRecieptPrint(OrderRecord order, ClientRecord client, List<PaymentRecord> payments,
			List<OrderCommentRecord> comments) {
		this.order = order;
		this.client = client;
		this.payments = payments;
		this.comments = comments;
	}

	@Override
	public Print getPrint() {
		return createPrint();
	}

	private Print createPrint() {
		GridPrint masterGrid = new GridPrint("c:d:g", NO_BORDER_LOOK);
		masterGrid.add(makeCompanyHeader());
		masterGrid.add(new EmptyPrint(16, 16));
		masterGrid.add(new TextPrint("Reciept" + "", STYLE_HEADER_BOLD_UNDERLINED));
		masterGrid.add(new EmptyPrint(8, 8));
		masterGrid.add(makeInfoGrid());
		masterGrid.add(new EmptyPrint(8, 8));
		masterGrid.add(makeContentTable());
		masterGrid.add(new EmptyPrint(8, 8));
		masterGrid.add(makePaymentsGrid());
		masterGrid.add(new EmptyPrint(8, 8));
		if (!AppPrefs.orderRecieptCommentTypes.isEmpty()) {
			DefaultGridLook tableLook = new DefaultGridLook();
			tableLook.setHeaderBackground(BLACK);
			tableLook.setCellBorder(new LineBorder());
			masterGrid.add(makeCommentGrid(comments, tableLook, STYLE_TABLE_HEADER, STYLE_TABLE_VALUE));
		}

		return new PagePrint(masterGrid, makePrintedOnFooter());
	}

	private GridPrint makeContentTable() {
		Map<Long, List<OrderFlatRateServiceRecord>> flatRateServiceMap = order.makeFlatRateMap();
		Map<Long, List<OrderServiceRecord>> serviceMap = order.makeServiceMap();
		Map<Long, List<OrderPartRecord>> partMap = order.makePartMap();

		List<Integer> productRowIndexes = getProductIndexes(flatRateServiceMap, serviceMap, partMap);

		DefaultGridLook tableLook = new DefaultGridLook();
		tableLook.setHeaderBackground(BLACK);
		tableLook.setCellBorder(new LineBorder());
		tableLook.setBodyBackgroundProvider(new ProductBodyCellBackgroundStriper(productRowIndexes));

		GridPrint contentGrid = new GridPrint("d:g, d, d, d, d", tableLook);
		contentGrid.addHeader(new StyledTextPrint().append("DESCRIPTION", STYLE_TABLE_HEADER));
		contentGrid.addHeader(new StyledTextPrint().append("PART #", STYLE_TABLE_HEADER));
		contentGrid.addHeader(new StyledTextPrint().append("QNT/HRS", STYLE_TABLE_HEADER));
		contentGrid.addHeader(new StyledTextPrint().append("PRICE", STYLE_TABLE_HEADER));
		contentGrid.addHeader(new StyledTextPrint().append("TOTAL", STYLE_TABLE_HEADER));

		for (OrderClientProductRecord product : order.getClientProducts()) {
			contentGrid.add(new TextPrint(makeProductDisplay(product), FONT_TABLE_SUBHEADER), 5);
			List<OrderFlatRateServiceRecord> flatRates = flatRateServiceMap.get(product.getClientProductId());
			if (flatRates != null) {
				for (OrderFlatRateServiceRecord flatRate : flatRates) {
					addFlatRateService(contentGrid, flatRate);
				}
			}

			List<OrderServiceRecord> services = serviceMap.get(product.getClientProductId());
			if (services != null) {
				for (OrderServiceRecord service : services) {
					addService(contentGrid, service);
				}
			}

			List<OrderPartRecord> parts = partMap.get(product.getClientProductId());
			if (parts != null) {
				for (OrderPartRecord part : parts) {
					addPart(contentGrid, part);
				}
			}
		}
		List<OrderPartRecord> parts = partMap.get(null);
		if (parts != null) {
			contentGrid.add(new TextPrint("MISC. PARTS", FONT_TABLE_VALUE), 5);
			for (OrderPartRecord part : parts) {
				addPart(contentGrid, part);
			}
		}
		return contentGrid;
	}

	private void addPart(GridPrint masterGrid, OrderPartRecord part) {
		masterGrid.add(new TextPrint(part.getPartName(), FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint(part.getPartNumber(), FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint(String.valueOf(part.getQuantity()), FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(part.getPartPrice()), FONT_TABLE_VALUE));
		if (part.isRefunded()) {
			masterGrid.add(new TextPrint("REFUNDED", FONT_TABLE_VALUE));
		} else {
			BigDecimal total = Calculator.totalPart(part);
			masterGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(total), FONT_TABLE_VALUE));
		}
	}

	private void addService(GridPrint masterGrid, OrderServiceRecord service) {
		masterGrid.add(new TextPrint(service.getServiceName(), FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint("", FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint(AppPrefs.FORMATTER.formatDecimal(service.getHoursQnt(), 2), FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(service.getPricePerHour()), FONT_TABLE_VALUE));
		if (service.isRefunded()) {
			masterGrid.add(new TextPrint("REFUNDED", FONT_TABLE_VALUE));
		} else {
			BigDecimal total = Calculator.totalService(service);
			masterGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(total), FONT_TABLE_VALUE));
		}
	}

	private void addFlatRateService(GridPrint masterGrid, OrderFlatRateServiceRecord flatRate) {
		masterGrid.add(new TextPrint(flatRate.getServiceName(), FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint("", FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint("1", FONT_TABLE_VALUE));
		masterGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(flatRate.getServicePrice()), FONT_TABLE_VALUE));
		if (flatRate.isRefunded()) {
			masterGrid.add(new TextPrint("REFUNDED", FONT_TABLE_VALUE));
		} else {
			masterGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(Calculator.totalFlatRateService(flatRate)),
					FONT_TABLE_VALUE));
		}
	}

	private List<Integer> getProductIndexes(Map<Long, List<OrderFlatRateServiceRecord>> flatRateServiceMap,
			Map<Long, List<OrderServiceRecord>> serviceMap, Map<Long, List<OrderPartRecord>> partMap) {
		List<Integer> productRowIndexes = Lists.newArrayList();
		int rowCount = 0;
		for (OrderClientProductRecord product : order.getClientProducts()) {
			productRowIndexes.add(rowCount);
			rowCount++;
			if (flatRateServiceMap.containsKey(product.getClientProductId())) {
				rowCount += flatRateServiceMap.get(product.getClientProductId()).size();
			}
			if (partMap.containsKey(product.getClientProductId())) {
				rowCount += partMap.get(product.getClientProductId()).size();
			}
			if (serviceMap.containsKey(product.getClientProductId())) {
				rowCount += serviceMap.get(product.getClientProductId()).size();
			}
		}

		if (partMap.containsKey(null)) {
			productRowIndexes.add(rowCount);
		}
		return productRowIndexes;
	}

	private GridPrint makeInfoGrid() {
		GridPrint infoGrid = new GridPrint("l:p:g, r:p:g", NO_BORDER_LOOK);
		if (client == null) {
			infoGrid.add(new EmptyPrint(0, 0));
		} else {
			infoGrid.add(makeBillTo());
		}
		infoGrid.add(makeOrderInfo());
		return infoGrid;
	}

	private GridPrint makeBillTo() {
		GridPrint grid = new GridPrint("l:p:g", NO_BORDER_LOOK);
		grid.add(new TextPrint("Client:", FONT_TITLE));
		grid.add(new TextPrint(client.getNameFullFormatted(), FONT_VALUE));
		if (!client.getAttentionOf().isEmpty()) {
			grid.add(new TextPrint(client.getAttentionOf(), FONT_VALUE));
		}

		AddressRecord primaryAddress = client.getPrimaryAddress();
		if (primaryAddress != null) {
			List<String> addressLines = AppPrefs.FORMATTER.formatAddressStrings(primaryAddress.getAddressLines(),
					primaryAddress.getCity(), primaryAddress.getState(), primaryAddress.getZip());

			for (String addressLine : addressLines) {
				if (StringUtility.isNullOrWhiteSpace(addressLine)) {
					continue;
				}
				grid.add(new TextPrint(addressLine, FONT_VALUE));
			}
		}

		PhoneRecord primaryPhone = client.getPrimaryPhone();
		if (primaryPhone != null) {
			String phone = AppPrefs.FORMATTER.formatPhoneNumber(primaryPhone.getPhoneNumber(),
					primaryPhone.getExtension());
			grid.add(new TextPrint(phone, FONT_VALUE));
		}
		return grid;
	}

	private GridPrint makeOrderInfo() {
		GridPrint grid = new GridPrint("r:p, 8,  r:p", NO_BORDER_LOOK);

		grid.add(new TextPrint("Order Number:", STYLE_TITLE));
		grid.add(new TextPrint());
		grid.add(SWT.LEFT, SWT.CENTER,
				new TextPrint(AppPrefs.FORMATTER.formatOrderNumber(order.getOrderId()), STYLE_VALUE));

		grid.add(new TextPrint("Opened By:", STYLE_TITLE));
		grid.add(new TextPrint());
		grid.add(SWT.LEFT, SWT.CENTER, new TextPrint(order.getCreatedBy(), STYLE_VALUE));

		grid.add(new TextPrint("Opened On:", STYLE_TITLE));
		grid.add(new TextPrint());
		grid.add(SWT.LEFT, SWT.CENTER,
				new TextPrint(AppPrefs.dateFormatter.formatFullDate_DB(order.getCreatedDttm()), STYLE_VALUE));

		if (order.getClosedDttm() != null) {
			grid.add(new TextPrint("Closed On:", STYLE_TITLE));
			grid.add(new TextPrint());
			grid.add(SWT.LEFT, SWT.CENTER,
					new TextPrint(AppPrefs.dateFormatter.formatFullDate_DB(order.getClosedDttm()), STYLE_VALUE));
		}

		GridPrint wrapper = new GridPrint("r:p:g", NO_BORDER_LOOK);
		wrapper.add(grid);
		return wrapper;
	}

	private Print makePaymentsGrid() {
		DefaultGridLook paymentTableLook = new DefaultGridLook();
		paymentTableLook.setHeaderBackground(BLACK);
		paymentTableLook.setCellBorder(new LineBorder());
		paymentTableLook.setBodyBackgroundProvider(new BodyCellBackgroundStriper());

		GridPrint paymentTable = new GridPrint("d, d, d, d:g, d", paymentTableLook);

		paymentTable.addHeader(new StyledTextPrint().append("DATE", STYLE_TABLE_HEADER));
		paymentTable.addHeader(new StyledTextPrint().append("PAYMENT", STYLE_TABLE_HEADER));
		paymentTable.addHeader(new StyledTextPrint().append("NUM.", STYLE_TABLE_HEADER));
		paymentTable.addHeader(new StyledTextPrint().append("DESCRIPTION.", STYLE_TABLE_HEADER));
		paymentTable.addHeader(new StyledTextPrint().append("AMOUNT", STYLE_TABLE_HEADER));

		if (payments != null) {
			for (PaymentRecord payment : payments) {
				paymentTable.add(new TextPrint(AppPrefs.dateFormatter.formatFullDate_DB(payment.getCreatedDtTm()),
						FONT_TABLE_VALUE));
				paymentTable.add(new TextPrint(payment.getPaymentTypeDisplay(), FONT_TABLE_VALUE));
				paymentTable.add(new TextPrint(payment.getPaymentNumber(), FONT_TABLE_VALUE));
				paymentTable.add(new TextPrint(payment.getPaymentDescription(), FONT_TABLE_VALUE));
				paymentTable.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(payment.getAmount()), FONT_TABLE_VALUE));
			}
		}

		OrderTotal orderTotal = Calculator.getOrderTotal(order);

		DefaultGridLook tableLook = new DefaultGridLook();
		tableLook.setHeaderBackground(BLACK);
		tableLook.setCellBorder(new LineBorder());
		tableLook.setBodyBackgroundProvider(new OrderTableCellBackground());

		GridPrint totalsGrid = new GridPrint("d, d", tableLook);

		totalsGrid.add(new TextPrint("LABOR", STYLE_TABLE_HEADER));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(orderTotal.laborTotal), FONT_TABLE_VALUE));
		totalsGrid.add(new TextPrint("PARTS", STYLE_TABLE_HEADER));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(orderTotal.merchandiseTotal), FONT_TABLE_VALUE));
		totalsGrid.add(new TextPrint("TAX", STYLE_TABLE_HEADER));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(orderTotal.taxTotal), FONT_TABLE_VALUE));
		totalsGrid.add(new TextPrint("TOTAL", STYLE_TABLE_HEADER));
		totalsGrid.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(orderTotal.orderTotal), FONT_TABLE_VALUE));

		GridPrint grid = new GridPrint("l:p:g, 8, r:p:g", NO_BORDER_LOOK);
		grid.add(paymentTable);
		grid.add(new EmptyPrint());
		grid.add(totalsGrid);
		return grid;
	}

	private String makeProductDisplay(OrderClientProductRecord product) {
		StringBuilder productDisplay = new StringBuilder("(").append(product.getProductType()).append(") ")
				.append(product.getProductManufacturer());
		if (!StringUtility.isNullOrWhiteSpace(product.getProductModel())) {
			productDisplay.append(" - ").append(product.getProductModel());
		}
		return productDisplay.toString();
	}

	private class ProductBodyCellBackgroundStriper implements CellBackgroundProvider {

		private final List<Integer> productRows;
		private final Map<Integer, RGB> colorMap;

		public ProductBodyCellBackgroundStriper(List<Integer> headerRows) {
			productRows = Lists.newArrayList(headerRows);
			Collections.sort(productRows);
			colorMap = Maps.newHashMap();
		}

		@Override
		public RGB getCellBackground(int row, int column, int colspan) {
			if (colorMap.get(row) != null) {
				return colorMap.get(row);
			}
			RGB color = null;
			int closestProduct = 0;
			for (int productRow : productRows) {
				if (productRow == row) {
					color = ROW_DARK;
					break;
				}
				if (productRow > row) {
					break;
				}
				closestProduct = productRow;
			}
			if (color == null) {
				color = ((row - closestProduct) % 2 == 0 ? ROW_LIGHT : ROW_LIGHTER);
			}
			colorMap.put(row, color);
			return color;
		}
	}

	private class BodyCellBackgroundStriper implements CellBackgroundProvider {
		@Override
		public RGB getCellBackground(int row, int column, int colspan) {
			return row % 2 == 0 ? ROW_LIGHTER : ROW_LIGHT;
		}
	}

	private class OrderTableCellBackground implements CellBackgroundProvider {
		@Override
		public RGB getCellBackground(int row, int column, int colspan) {
			return column == 0 ? BLACK : WHITE;
		}
	}
}
