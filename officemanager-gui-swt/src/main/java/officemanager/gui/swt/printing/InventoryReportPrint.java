package officemanager.gui.swt.printing;

import java.util.Date;
import java.util.List;

import net.sf.paperclips.EmptyPrint;
import net.sf.paperclips.GridPrint;
import net.sf.paperclips.Print;
import net.sf.paperclips.TextPrint;
import officemanager.biz.records.PartSearchRecord;
import officemanager.gui.swt.AppPrefs;

public class InventoryReportPrint extends Printable {

	private final List<PartSearchRecord> lowStockParts;
	private final List<PartSearchRecord> outOfStockParts;

	public InventoryReportPrint(List<PartSearchRecord> lowStockParts, List<PartSearchRecord> outOfStockParts) {
		this.lowStockParts = lowStockParts;
		this.outOfStockParts = outOfStockParts;
	}

	@Override
	public Print getPrint() {
		return createPrint();
	}

	private Print createPrint() {
		GridPrint masterGrid = new GridPrint("c:d:g", NO_BORDER_LOOK);
		masterGrid.add(makeHeaderGrid());

		GridPrint bodyGrid = new GridPrint("l:d:g", NO_BORDER_LOOK);
		if (outOfStockParts != null) {
			bodyGrid.add(new EmptyPrint(16, 16));
			bodyGrid.add(new TextPrint("OUT OF STOCK", STYLE_HEADER2_BOLD_UNDERLINED));
			bodyGrid.add(new EmptyPrint(8, 8));
			if (outOfStockParts.isEmpty()) {
				bodyGrid.add(new TextPrint("No parts are currently out of stock.", FONT_FINE));
			} else {
				bodyGrid.add(makePartsGrid(outOfStockParts));
			}
		}
		if (lowStockParts != null) {
			bodyGrid.add(new EmptyPrint(16, 16));
			bodyGrid.add(new TextPrint("LOW STOCK", STYLE_HEADER2_BOLD_UNDERLINED));
			bodyGrid.add(new EmptyPrint(8, 8));
			if (lowStockParts.isEmpty()) {
				bodyGrid.add(new TextPrint("No parts are currently low on stock.", FONT_FINE));
			} else {
				bodyGrid.add(makePartsGrid(lowStockParts));
			}
		}
		masterGrid.add(bodyGrid);
		return masterGrid;
	}

	private GridPrint makeHeaderGrid() {
		GridPrint header = new GridPrint("c:d:g", NO_BORDER_LOOK);
		header.add(new TextPrint("INVENTORY REPORT", STYLE_HEADER_BOLD_UNDERLINED));
		header.add(new TextPrint(AppPrefs.dateFormatter.formatDate(new Date()), FONT_VALUE));
		return header;
	}

	private GridPrint makePartsGrid(List<PartSearchRecord> parts) {
		GridPrint table = new GridPrint("d, d, d, d, d, d", BORDER_LOOK);
		table.add(new TextPrint("Part", FONT_TITLE));
		table.add(new TextPrint("Manuf.", FONT_TITLE));
		table.add(new TextPrint("Manuf. ID", FONT_TITLE));
		table.add(new TextPrint("Inv. Cnt.", FONT_TITLE));
		table.add(new TextPrint("Price", FONT_TITLE));
		table.add(new TextPrint("Amt. Sold", FONT_TITLE));
		for (PartSearchRecord part : parts) {
			table.add(new TextPrint(part.getPartName(), FONT_VALUE));
			table.add(new TextPrint(part.getPartManufacturer(), FONT_VALUE));
			table.add(new TextPrint(part.getManufacturerId(), FONT_VALUE));
			table.add(new TextPrint(String.valueOf(part.getInventoryCount()), FONT_VALUE));
			table.add(new TextPrint(AppPrefs.FORMATTER.formatMoney(part.getBasePrice()), FONT_VALUE));
			table.add(new TextPrint(String.valueOf(part.getAmountSold()), FONT_VALUE));
		}
		return table;
	}
}
