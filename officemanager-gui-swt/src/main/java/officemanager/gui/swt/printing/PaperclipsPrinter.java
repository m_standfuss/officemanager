package officemanager.gui.swt.printing;

import org.eclipse.swt.SWT;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Shell;

import net.sf.paperclips.PaperClips;
import net.sf.paperclips.PrintJob;

public class PaperclipsPrinter {

	private static final String PRINT_TXT = "OfficeManager Print";

	public static void promptAndPrint(Shell shell, IPrintable printable) {
		PrinterData data = getPrinterData(shell);
		if (data != null) {
			print(printable, data);
		}
	}

	public static PrinterData getPrinterData(Shell shell) {
		PrintDialog printDialog = new PrintDialog(shell, SWT.NONE);
		printDialog.setText(PRINT_TXT);
		return printDialog.open();
	}

	public static void print(IPrintable printable, PrinterData printerData) {
		PrintJob printJob = new PrintJob(PRINT_TXT, printable.getPrint());
		printJob.setMargins(printable.getMargins());
		printJob.setOrientation(printable.getOrientation());
		PaperClips.print(printJob, printerData);
	}
}
