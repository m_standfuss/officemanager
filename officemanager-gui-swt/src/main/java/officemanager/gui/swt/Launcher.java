package officemanager.gui.swt;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import officemanager.biz.delegates.SecurityDelegate;
import officemanager.biz.records.UserSessionRecord;
import officemanager.biz.records.UserSessionRecord.AuthStatus;
import officemanager.data.access.Dao;
import officemanager.gui.swt.services.AppPrefLoader;
import officemanager.gui.swt.ui.dialogs.LoginDialog;
import officemanager.gui.swt.util.MessageDialogs;
import officemanager.gui.swt.util.ResourceManager;
import officemanager.utility.DateFormatter;

public class Launcher {

	private static final Logger logger = LogManager.getLogger(Launcher.class);

	/**
	 * Exit codes
	 */
	private static final int SUCCESSFULLY_TERMINATED = 0;
	private static final int ERR_PROPERTIES = 1;
	private static final int ERR_DB_PARAMETER = 2;
	private static final int ERR_DB_CONNECTION = 3;
	private static final int ERR_LOGIN = 4;
	private static final int ERR_SHUTDOWN = 5;
	private static final int ERR_UNEXPECTED = 6;

	/**
	 * Properties
	 */
	private static String dbHostName;
	private static String loginUsername;
	private static String loginPassword;
	private static boolean forceLogin;

	private static Display display;
	private static Shell shell;

	public static void main(String[] args) throws ParseException {
		int exitCode = SUCCESSFULLY_TERMINATED;

		try {
			display = new Display();
			parseAppConfig();
			testDbConnection();
			setAppPreferences();
			runApplication();
		} catch (Throwable e) {
			logger.error("Unexpected exception caught crashing application.", e);
			exitCode = ERR_UNEXPECTED;
		}

		try {
			shutDown();
		} catch (Exception e) {
			logger.error("An error occured during shutdown procedures.", e);
			exitCode = ERR_SHUTDOWN;
		}
		System.exit(exitCode);
	}

	private static void runApplication() {
		boolean loggedOut = true;
		while (loggedOut) {
			login();
			shell = new Shell(display);
			shell.setImage(ResourceManager.getIcon());
			shell.setText("OfficeManager");
			shell.setMaximized(true);
			OfficeManagerApplication application = new OfficeManagerApplication(shell);
			shell.open();

			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
			loggedOut = application.getExitStatus() == OfficeManagerApplication.EXIT_LOGOUT;
			/*
			 * Second and after time around we dont want to force the login
			 * attempt.
			 */
			forceLogin = false;
		}
		display.dispose();
	}

	private static void parseAppConfig() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("officemanager.config");
			prop.load(input);
		} catch (IOException e) {
			logger.error("Unable to get the configuration file.", e);
			System.exit(ERR_PROPERTIES);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					logger.error("Unable to close the configuration file.", e);
				}
			}
		}

		dbHostName = prop.getProperty("databaseHostname");
		if (dbHostName == null) {
			MessageDialogs.displayError(shell, "Unable to start application.",
					"The database hostname was not defined in the properties file.");
			System.exit(ERR_DB_PARAMETER);
		}
		loginUsername = prop.getProperty("user");
		loginPassword = prop.getProperty("password");
		String forceLoginS = prop.getProperty("forceLogin");
		if (forceLoginS != null) {
			forceLogin = Boolean.parseBoolean(forceLoginS);
		}
		String loggingLevel = "ERROR";
		if (prop.getProperty("loggingLevel") != null) {
			loggingLevel = prop.getProperty("loggingLevel");
		}
		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration config = ctx.getConfiguration();
		LoggerConfig loggerConfig = config.getLoggerConfig("officemanager");
		loggerConfig.setLevel(Level.valueOf(loggingLevel));
		ctx.updateLoggers();
	}

	private static void testDbConnection() {
		Dao.setHostName(dbHostName);
		if (!Dao.testConnection()) {
			logger.error("Unable to make the connection to the database. Cannot open application.");
			MessageDialogs.displayError(shell, "Unable to start application.",
					"A connection to the database cannot be made. Please check network connectivity and restart the application.");
			System.exit(ERR_DB_CONNECTION);
		}
	}

	private static void login() {
		SecurityDelegate securityDelegate = new SecurityDelegate();
		LoginDialog dialog = new LoginDialog();
		dialog.setUsername(loginUsername);
		dialog.setPassword(loginPassword);
		dialog.setForceLogin(forceLogin);
		UserSessionRecord record = dialog.open();
		if (record == null) {
			logger.info("Login attempt was canceled.");
			System.exit(SUCCESSFULLY_TERMINATED);
		}
		if (record.authStatus != AuthStatus.SUCCESSFUL) {
			logger.info("Unable to successfully authenticate user cannot open the application.");
			System.exit(ERR_LOGIN);
		}
		AppPrefs.currentSession = record;
		AppPrefs.currentSessionPermissions = securityDelegate
				.getPermissionsForPersonnel(AppPrefs.currentSession.prsnlId);
		Dao.setCurrentUser(AppPrefs.currentSession.prsnlId);
	}

	private static void setAppPreferences() {
		AppPrefs.localTimeZone = TimeZone.getDefault();
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		AppPrefs.dateFormatter = DateFormatter.getInstance(AppPrefs.localTimeZone);
		new AppPrefLoader().loadApplicationPreferences();
	}

	private static void shutDown() {
		ResourceManager.dispose();
		Dao.closeConnections();
	}
}
