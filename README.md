Office Manager is a small business application for small business service shop. It includes limited capabilities in the following areas:

    - Inventory maintaince
        - Add parts to the system along with current pricing and details.
        - Ability to use barcode scanning to search directly for a part. 
        - Assign sale prices for part to run for given begin/end dates. 
        - Track part history, how many times sold, sale history etc. 
        - Keep a current amount in inventory count that is updated as new inventory is added or used to fulfill orders. 
    - Creation/Modification of service tickets
        - Assign clients' products (car, mower etc.) to services to be completed.
        - Add parts direclty to ticket.
        - Track progress on tickets by assigning the current status for it. 
        - Update and track service tickets through employee/customer comments.
    - Creation of one time orders
        - Good for when clients can buy something off-the-shelf instead of bringing in a product for service. 
    - Printing Tickets
        - Shop tickets for service work to be attached to a product, can include comments made on the order.
        - Client claim tickets for products dropped off for service. 
        - Reciepts of work done and payments made. 
    - Maintain your client list along with any contact information for a client. 
    - View and print daily/weekly/monthly/yearly totals for money in and out. 
    - Balance the till at the end of a shift. 
    - Create and manage client accounts:
        - Add multiple people to a single account.
        - Add a credit limit to allow for clients to run a balance and make monthly payments. 
        - View/Print account statements. 
        - Make payments to an account and print reciepts on payment. 
    - Security 
        - Ability to lock down/open up functionality for a subset of employees.
    - Customization
        - Ability to customize the environment to suite the needs of the company using it. 

The application is ran ontop of a MySQL database that can be installed locally to the device or remotley on a dedicated server. The application 
itself can be installed on any number of devices and pointed to the MySQL instance allowing for concurrent use of the application. 

The Issues board can be found here https://bitbucket.org/m_standfuss/officemanager/issues. Feel free to log/comment on any you see out there. 

This application started out specific for one of my parent's businesses, but I tried to make the code as generic/reusable as possible. If there are
any questions about the code, you want to use it or contribute to the codebase feel free to reach out to me at m_standfuss@hotmail.com. 

Cheers!