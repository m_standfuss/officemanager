package officemanager.biz;

import java.math.BigDecimal;

public class OrderTotal {
	public final BigDecimal orderTotal;
	public final BigDecimal merchandiseTotal;
	public final BigDecimal laborTotal;
	public final BigDecimal taxTotal;
	public final BigDecimal taxExempt;
	public final BigDecimal merchTaxExempt;

	public OrderTotal(BigDecimal orderTotal, BigDecimal merchandiseTotal, BigDecimal merchTaxExempt, BigDecimal laborTotal,
			BigDecimal taxTotal, BigDecimal taxExempt) {
		this.orderTotal = orderTotal;
		this.merchandiseTotal = merchandiseTotal;
		this.merchTaxExempt = merchTaxExempt;
		this.laborTotal = laborTotal;
		this.taxTotal = taxTotal;
		this.taxExempt = taxExempt;
	}
}