package officemanager.biz.delegates;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.records.CodeValueRecord;
import officemanager.data.access.CodeValueDao;
import officemanager.data.models.CodeValue;

public class CodeValueDelegate extends Delegate {

	private static final Logger logger = LogManager.getLogger(CodeValueDelegate.class);

	private final CodeValueDao codeValueDao;

	public CodeValueDelegate() {
		codeValueDao = new CodeValueDao();
	}

	public Long persistCodeValue(CodeValueRecord record) {
		logger.debug("Starting persistCodeValue for codeValue " + record);

		CodeValue codeValue;
		if (record.getCodeValue() == null) {
			logger.debug("Entering a new code value.");
			codeValue = new CodeValue();
		} else {
			logger.debug("Attempting to update an existing code value.");
			codeValue = codeValueDao.findById(record.getCodeValue());
			if (codeValue == null) {
				logger.error("No code value was found with code value = " + record.getCodeValue()
						+ " to update, instead inserting new.");
				codeValue = new CodeValue();
			}
		}
		codeValue.setCodeSet(record.getCodeSet());
		codeValue.setCollationSeq(record.getCollatingSequence());
		codeValue.setDisplay(record.getDisplay());
		codeValue.setCdfMeaning(record.getCdfMeaning());
		codeValueDao.persist(codeValue);
		return codeValue.getCodeValue();
	}

	@Override
	public List<CodeValueRecord> getCodeValueRecords(Long codeSet) {
		return super.getCodeValueRecords(codeSet);
	}

	@Override
	public List<CodeValueRecord> getActiveCodeValueRecords(Long codeSet) {
		return super.getActiveCodeValueRecords(codeSet);
	}

	@Override
	public CodeValueRecord getCodeValueRecord(Long codeValue) {
		return super.getCodeValueRecord(codeValue);
	}

	public void inactivateCodeValue(Long codeValue) {
		codeValueDao.inactivate(codeValue);
		clearCodeValueCache();
	}

	public void reactivateCodeValue(Long codeValue) {
		codeValueDao.reactivate(codeValue);
		clearCodeValueCache();
	}

	@Override
	protected List<Long> warmCache() {
		return Lists.newArrayList();
	}
}
