package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.contrib.org.apache.commons.codec_1_3.binary.Base64;

import officemanager.biz.Encryptor;
import officemanager.biz.records.UserSessionRecord;
import officemanager.biz.records.UserSessionRecord.AuthStatus;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PersonnelDao;
import officemanager.data.models.Person;
import officemanager.data.models.Personnel;

/**
 * Handles all login business logic.
 *
 * @author Mike
 *
 */
public class LoginDelegate {
	private static Logger logger = LogManager.getLogger(LoginDelegate.class);
	private static final byte[] password = { 83, 71, 70, 121, 99, 110, 107, 103, 97, 71, 70, 107, 73, 71, 74, 108, 90,
			87, 52, 103, 98, 50, 52, 103, 100, 71, 104, 108, 73, 69, 100, 121, 101, 87, 90, 109, 97, 87, 53, 107, 98,
			51, 73, 103, 83, 71, 57, 49, 99, 50, 85, 103, 85, 88, 86, 112, 90, 71, 82, 112, 100, 71, 78, 111, 73, 72,
			82, 108, 89, 87, 48, 103, 90, 88, 90, 108, 99, 105, 66, 122, 97, 87, 53, 106, 90, 83, 66, 111, 97, 88, 77,
			103, 90, 109, 108, 121, 99, 51, 81, 103, 101, 87, 86, 104, 99, 105, 66, 104, 100, 67, 66, 73, 98, 50, 100,
			51, 89, 88, 74, 48, 99, 121, 66, 104, 98, 109, 81, 103, 98, 51, 100, 117, 90, 87, 81, 103, 98, 50, 53, 108,
			73, 71, 57, 109, 73, 72, 82, 111, 90, 83, 66, 105, 90, 88, 78, 48, 73, 72, 74, 104, 89, 50, 108, 117, 90,
			121, 66, 105, 99, 109, 57, 118, 98, 88, 77, 103, 73, 71, 108, 117, 73, 67, 66, 48, 97, 71, 85, 103, 73, 72,
			100, 118, 99, 109, 120, 107, 76, 67, 65, 103, 89, 83, 65, 103, 82, 109, 108, 121, 90, 87, 74, 118, 98, 72,
			81, 117 };

	private final PersonnelDao personnelDao;
	private final PersonDao personDao;
	private final Encryptor encryptor;

	/**
	 * Default constructor.
	 */
	public LoginDelegate() {
		personnelDao = new PersonnelDao();
		personDao = new PersonDao();

		encryptor = new Encryptor(new String(Base64.decodeBase64(password), StandardCharsets.UTF_8));
	}

	/**
	 * Authenticated the given username and password.
	 *
	 * @param username
	 *            The username to attempt to authenticate.
	 * @param password
	 *            The password to attempt to authenticate.
	 * @return The resulting session. The authStatus of the session will tell to
	 *         what degree the user was authenticated.
	 * @throws NullPointerException
	 *             If username or password is null.
	 */
	public UserSessionRecord login(String username, String password) {
		logger.debug("Attempting to log in for username '" + username + "'.");
		checkNotNull(username, password);
		final Personnel personnel = personnelDao.findByUsername(username);
		if (personnel == null) {
			logger.debug("No personnel row found with that username.");
			return new UserSessionRecord(username, AuthStatus.INVALID_CREDS);
		}

		Person person = personDao.findById(personnel.getPersonId());
		String nameFirst = "";
		String nameFullFormatted = "";
		if (person == null) {
			logger.warn("Unable to find the corresponding person for the personnel.");
		} else {
			nameFirst = person.getNameFirst();
			nameFullFormatted = person.getNameFullFormatted();
		}

		if (personnel.getPassword() == null) {
			logger.debug("The password has not been set for this user.");

			return new UserSessionRecord(username, AuthStatus.PASSWORD_NEEDS_SET, nameFirst, nameFullFormatted,
					personnel.getPersonnelId());
		}

		if (!encryptor.matches(personnel.getPassword(), password)) {
			logger.debug("The users credentials did not match.");
			return new UserSessionRecord(username, AuthStatus.INVALID_CREDS);
		}

		if (personnel.isLockedOut()) {
			logger.debug("The personnel has been locked out.");
			return new UserSessionRecord(username, AuthStatus.USER_LOCKED_OUT, nameFirst, nameFullFormatted,
					personnel.getPersonnelId());
		}

		if (personnel.isPasswordNeedsReset()) {
			logger.debug("The personnel's password needs to be reset.");
			return new UserSessionRecord(username, AuthStatus.PASSWORD_NEEDS_RESET, nameFirst, nameFullFormatted,
					personnel.getPersonnelId());
		}

		// The User has been successfully authenticated at this point.
		logger.debug("The login was successful.");
		return new UserSessionRecord(username, AuthStatus.SUCCESSFUL, nameFirst, nameFullFormatted,
				personnel.getPersonnelId());
	}

	/**
	 * Updates the password for a given personnel id. First finds the personnel
	 * then calls {@link #updatePassword(Personnel, String)}.
	 *
	 * @param personnelId
	 *            The personnel id.
	 * @param password
	 *            The plain text string to update the password to.
	 * @throws NullPointerException
	 *             If personnelId is null.
	 * @throws IllegalArgumentException
	 *             If the personnel cannot be found.
	 */
	public void updatePassword(Long personnelId, String password) {
		logger.debug("Attempting to update password for personnel_id=" + personnelId);
		checkNotNull(personnelId);
		checkNotNull(password);
		personnelDao.updatePassword(personnelId, encryptor.encryptToBytes(password));
	}

	/**
	 * Checks whether or not the password belongs to the personnel id.
	 *
	 * @param personnelId
	 *            The personnel id of the personnel to check.
	 * @param password
	 *            The password of the personnel to check.
	 * @return If the personnel exists for the id and its password matches the
	 *         one provided.
	 * @throws NullPointerException
	 *             If the personnelId or password is null.
	 */
	public boolean checkPassword(Long personnelId, String password) {
		logger.debug("Checking the password for personnel id = " + personnelId);
		checkNotNull(personnelId, password);
		Personnel personnel = personnelDao.findById(personnelId);
		if (personnel == null) {
			logger.debug("The personnel could not be found.");
			return false;
		}
		return encryptor.matches(personnel.getPassword(), password);
	}
}
