package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.Calculator;
import officemanager.biz.EntityHelper;
import officemanager.biz.maps.EntityMap;
import officemanager.biz.records.AccountRecord;
import officemanager.biz.records.AccountRecord.AccountStatus;
import officemanager.biz.records.AccountSearchRecord;
import officemanager.biz.records.AccountTransactionRecord;
import officemanager.biz.searchingcriteria.AccountSearchCriteria;
import officemanager.biz.searchservices.PersonSearchService;
import officemanager.data.access.AccountDao;
import officemanager.data.access.AccountTransactionDao;
import officemanager.data.access.ClientDao;
import officemanager.data.access.PaymentDao;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PhoneDao;
import officemanager.data.models.Account;
import officemanager.data.models.AccountTransaction;
import officemanager.data.models.Client;
import officemanager.data.models.Payment;
import officemanager.data.models.Person;
import officemanager.data.models.Phone;

public class AccountDelegate extends Delegate {

	private static final Logger logger = LogManager.getLogger(AccountDelegate.class);

	private static final String ACTIVE_STATUS_CDF = "ACTIVE";
	private static final String CLOSED_STATUS_CDF = "CLOSED";
	private static final String CLOSED_AWAITING_PAYMENT_CDF = "CLOSED_AWT_PYMT";
	private static final String SUSPEND_CDF = "SUSPENDED";

	private static final String CREDIT_CDF = "CREDIT";
	private static final String DEBIT_CDF = "DEBIT";

	private final AccountDao accountDao;
	private final AccountTransactionDao accountTransactionDao;
	private final ClientDao clientDao;
	private final PaymentDao paymentDao;
	private final PersonDao personDao;
	private final PhoneDao phoneDao;

	private final PersonSearchService personSearchService;

	public AccountDelegate() {
		accountDao = new AccountDao();
		accountTransactionDao = new AccountTransactionDao();
		clientDao = new ClientDao();
		paymentDao = new PaymentDao();
		personDao = new PersonDao();
		phoneDao = new PhoneDao();

		personSearchService = new PersonSearchService();
	}

	public boolean hasAccount(Long clientId) {
		logger.debug("Starting hasAccount for clientId = " + clientId);
		Account account = accountDao.findActiveAccountByClientId(clientId);
		logger.debug("Account found '" + account != null + "'.");
		return account != null;
	}

	/**
	 * Adds a new account for the client.
	 *
	 * @param clientId
	 *            The client id of the client to add an account for.
	 * @return The account id of the new
	 * @throws IllegalArgumentException
	 *             If the client id does not correspond to a Client, or if that
	 *             client already has an active account.
	 */
	public Long addAccountToClient(Long clientId) {
		logger.debug("Starting addAccountToClient for clientId=" + clientId);
		checkNotNull(clientId);
		Client client = clientDao.findById(clientId);
		if (client == null) {
			logger.error("The client identifier specified to create an account for could not be found. ");
			throw new IllegalArgumentException("No client was found for the client id specified.");
		}
		if (accountDao.findActiveAccountByClientId(clientId) != null) {
			logger.error("The client already has an account created for it.");
			throw new IllegalArgumentException("The client specified to create an account for already has an account.");
		}

		Account account = new Account();
		account.setClientId(clientId);
		account.setLastStatementDtTm(null);
		account.setStatusCd(getCodeValue(CLIENT_CRDT_STATUS_CDSET, ACTIVE_STATUS_CDF));
		accountDao.persist(account);
		return account.getAccountId();
	}

	public void reopenAccount(Long accountId) {
		logger.debug("Starting reopenAccount for accountId= " + accountId);
		checkNotNull(accountId);

		accountDao.updateStatus(accountId, getCodeValue(CLIENT_CRDT_STATUS_CDSET, ACTIVE_STATUS_CDF));
	}

	/**
	 * Puts the client's account into a closed status. Either closed (if the
	 * current balance is zero or less) or closed awaiting payment if there is a
	 * remaining balanace to be paid off.
	 *
	 * @param accountId
	 *            The id of the account to close.
	 */
	public void closeAccount(Long accountId) {
		logger.debug("Starting closeAccount for accountId=" + accountId);
		checkNotNull(accountId);

		Long accountStatusCd = getAccountStatusCd(accountId);
		AccountStatus currentStatus = getAccountStatus(accountStatusCd);
		logger.debug("Current account status = " + currentStatus);
		if (currentStatus == AccountStatus.CLOSED || currentStatus == AccountStatus.CLOSED_AWAITING_PAYMENT) {
			logger.debug("Account is already closed nothing to do.");
			return;
		}

		BigDecimal balance = getBalance(accountId);
		logger.debug("Current balance of account = " + balance);

		Long newStatusCd;
		if (balance.compareTo(Calculator.getZero()) <= 0 || Calculator.isZeroAmount(balance)) {
			logger.debug("Account balance is zero closing account.");
			newStatusCd = getCodeValue(CLIENT_CRDT_STATUS_CDSET, CLOSED_STATUS_CDF);
		} else {
			logger.debug("Account has a remaining balance, closing awaiting payment.");
			newStatusCd = getCodeValue(CLIENT_CRDT_STATUS_CDSET, CLOSED_AWAITING_PAYMENT_CDF);
		}
		accountDao.updateStatus(accountId, newStatusCd);
	}

	/**
	 * Suspends the account. This can be used if an account need to be
	 * temporarily suspended but plans to be resumed at a later day.
	 *
	 * @param accountId
	 *            The account id to suspend.
	 */
	public void suspendAccount(Long accountId) {
		logger.debug("Starting suspendAccount for accountId=" + accountId);
		checkNotNull(accountId);

		Long accountStatusCd = getAccountStatusCd(accountId);
		AccountStatus currentStatus = getAccountStatus(accountStatusCd);
		logger.debug("Current account status = " + currentStatus);
		if (currentStatus == AccountStatus.SUSPEND) {
			logger.debug("Account is already suspended nothing to do.");
			return;
		}
		if (currentStatus == AccountStatus.CLOSED || currentStatus == AccountStatus.CLOSED_AWAITING_PAYMENT) {
			throw new IllegalStateException(
					"The account is currently in a closed or closed awaiting payment status cannot suspend.");
		}

		Long statusCd = getCodeValue(CLIENT_CRDT_STATUS_CDSET, SUSPEND_CDF);
		accountDao.updateStatus(accountId, statusCd);
	}

	public List<AccountSearchRecord> searchAccounts(AccountSearchCriteria criteria) {
		logger.debug("Starting searchAccounts for criteria=" + criteria);
		if (criteria.isEmpty()) {
			logger.debug("Searching criteria empty not preforming search.");
			return Lists.newArrayList();
		}
		List<Long> clientIds = null;
		if (criteria.personSearchCriteria != null && !criteria.personSearchCriteria.isEmpty()) {
			List<Long> personIds = personSearchService.searchForPersonIds(criteria.personSearchCriteria);
			List<Client> clients = clientDao.findActiveByPersonIds(personIds);
			clientIds = EntityHelper.getPrimaryKeyList(Long.class, clients);
		}

		Long accountId = criteria.accountId;
		List<Account> accounts = accountDao.findBySearchCriteria(accountId, clientIds);
		return makeSearchRecords(accounts);
	}

	public Long getClientId(Long accountId) {
		logger.debug("Starting getClientId for accountId = " + accountId);
		Account account = accountDao.findById(accountId);
		if (account == null) {
			logger.error("No account was found for the id = " + accountId);
			return null;
		}
		return account.getClientId();
	}

	public BigDecimal getAccountBalance(Long accountId) {
		logger.debug("Starting getAccountBalance for accountId = " + accountId);
		return getBalance(accountId);
	}

	/**
	 * Loads the account record for the given account id.
	 *
	 * @param accountId
	 *            The account id of the account.
	 * @return The account record.
	 */
	public AccountRecord getAccountRecord(Long accountId) {
		logger.debug("Starting loadAccountRecord for accountId=" + accountId);
		checkNotNull(accountId);

		Account account = accountDao.findById(accountId);
		if (account == null) {
			logger.debug("No account was found.");
			return null;
		}

		AccountRecord record = new AccountRecord();
		record.setAccountId(accountId);
		record.setLastStatementDttm(account.getLastStatementDtTm());
		record.setClientId(account.getClientId());
		record.setAccountStatus(getAccountStatus(account.getStatusCd()));
		record.setAccountStatusDisplay(getCodeValueRecord(account.getStatusCd()).getDisplay());

		record.setBalance(getBalance(accountId));
		return record;
	}

	/**
	 * Loads the account record for the given client id.
	 *
	 * @param clientId
	 *            The client id of the client for the account.
	 * @return The account record.
	 */
	public AccountRecord getAccountForClient(Long clientId) {
		logger.debug("Starting getAccountForClient with clientId=" + clientId);
		checkNotNull(clientId);
		Account account = accountDao.findActiveAccountByClientId(clientId);
		if (account == null) {
			logger.debug("No account was found for that client id.");
			return null;
		}
		return getAccountRecord(account.getAccountId());
	}

	/**
	 * Gets a list of the transactions for the given account that fall between
	 * the timeframe specified.
	 *
	 * @param accountId
	 *            The account id to get transactions for.
	 * @param startDttm
	 *            The time to get any transactions after.
	 * @param endDttm
	 *            The time to get any transactions before.
	 * @return The list of transactions that meet the requirements.
	 */
	public List<AccountTransactionRecord> getAccountTransactions(Long accountId, Date startDttm, Date endDttm) {
		logger.debug("Starting getAccountTransactions for accountId=" + accountId + ", starting date=" + startDttm
				+ ", ending date=" + endDttm);
		checkNotNull(accountId);
		checkNotNull(startDttm);
		checkNotNull(endDttm);

		List<AccountTransaction> accountTransactions = accountTransactionDao.findActiveByAccountIdAndDates(accountId,
				startDttm, endDttm);
		EntityMap<Payment> paymentMap = getPaymentMap(accountTransactions);

		List<AccountTransactionRecord> recordList = Lists.newArrayList();
		for (AccountTransaction at : accountTransactions) {
			Payment payment = paymentMap.getEntity(at.getPaymentId());
			if (payment == null) {
				logger.error("Missing corresponding payment for account transaction=" + at.getAccountTransactionId());
			}

			AccountTransactionRecord record = new AccountTransactionRecord();
			record.setTransactionDttm(at.getTransactionDttm());
			record.setTransactionType(getCodeValueRecord(at.getTransactionTypeCd()));
			if (payment != null) {
				record.setAmount(payment.getAmount());
				record.setPaymentDescription(payment.getPaymentDesc());
				record.setPaymentNumber(payment.getPaymentNbr());
				record.setPaymentType(getCodeValueRecord(payment.getPaymentTypeCd()));
			}
			recordList.add(record);
		}
		return recordList;
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeValues = Lists.newArrayList();
		codeValues.add(PAYMENT_TYPE_CDSET);
		codeValues.add(CLIENT_CRDT_TRANS_TYPE_CDSET);
		codeValues.add(CLIENT_CRDT_STATUS_CDSET);
		return codeValues;
	}

	/**
	 * Gets the corresponding payment map for the specified account
	 * transactions.
	 *
	 * @param accountTransactions
	 *            The account transactions to get payments for.
	 * @return The payment map.
	 */
	private EntityMap<Payment> getPaymentMap(List<AccountTransaction> accountTransactions) {
		Set<Long> paymentIds = Sets.newHashSet();
		for (AccountTransaction at : accountTransactions) {
			paymentIds.add(at.getPaymentId());
		}
		List<Payment> payments = paymentDao.findByIdList(paymentIds);
		return EntityMap.newEntityMap(payments);
	}

	/**
	 * Gets the total balance of the account.
	 *
	 * @param accountId
	 *            The id of the account.
	 * @return The total balance calculated.
	 */
	private BigDecimal getBalance(Long accountId) {
		List<AccountTransaction> accountTransactions = accountTransactionDao.findActiveByAccountId(accountId);
		EntityMap<Payment> paymentMap = getPaymentMap(accountTransactions);
		return getBalance(accountTransactions, paymentMap);
	}

	/**
	 * Gets the balance of the account based upon the passed in account
	 * transactions, and payment map.
	 *
	 * @param openingBalance
	 *            The opening balance to use to start calculations.
	 * @param accountTransactions
	 *            The list of account transactions for the account.
	 * @param paymentMap
	 *            The payment map, should have all the corresponding payments of
	 *            the transactions.
	 * @return The balance calculated.
	 */
	private BigDecimal getBalance(List<AccountTransaction> accountTransactions, EntityMap<Payment> paymentMap) {
		List<BigDecimal> credits = Lists.newArrayList();
		List<BigDecimal> debits = Lists.newArrayList();
		for (AccountTransaction at : accountTransactions) {
			Payment payment = paymentMap.getEntity(at.getPaymentId());
			if (payment == null) {
				logger.error("The payment for the account transaction was not found. paymentId=" + at.getPaymentId()
						+ ", accountTransactionId=" + at.getAccountTransactionId());
				continue;
			}
			if (at.getTransactionTypeCd() == null) {
				logger.error(
						"The account transaction has a null transaction type cd, unable to use for balance calculation.");
				continue;
			}
			if (at.getTransactionTypeCd().equals(getCodeValue(CLIENT_CRDT_TRANS_TYPE_CDSET, CREDIT_CDF))) {
				credits.add(payment.getAmount());
			} else if (at.getTransactionTypeCd().equals(getCodeValue(CLIENT_CRDT_TRANS_TYPE_CDSET, DEBIT_CDF))) {
				debits.add(payment.getAmount());
			} else {
				logger.error("Unrecognized transaction type code value '" + at.getTransactionTypeCd()
						+ "'. Unable to use for balance calculation.");
			}
		}
		return Calculator.calculateAccountBalance(credits, debits);
	}

	/**
	 * Gets the account status code value for the account. If the account could
	 * not be found will return null.
	 *
	 * @param accountId
	 *            The identifier of the account to get status for.
	 * @return The account status cd.
	 */
	private Long getAccountStatusCd(Long accountId) {
		Account account = accountDao.findById(accountId);
		if (account == null) {
			logger.error("No account was found for account id=" + accountId);
			return null;
		}
		return account.getStatusCd();
	}

	private AccountStatus getAccountStatus(Long statusCd) {
		if (statusCd == null) {
			logger.error("Null account status was found.");
			return null;
		}
		final String cdfMeaning = getCdfMeaning(statusCd);
		switch (cdfMeaning) {
		case ACTIVE_STATUS_CDF:
			return AccountStatus.ACTIVE;
		case CLOSED_STATUS_CDF:
			return AccountStatus.CLOSED;
		case CLOSED_AWAITING_PAYMENT_CDF:
			return AccountStatus.CLOSED_AWAITING_PAYMENT;
		case SUSPEND_CDF:
			return AccountStatus.SUSPEND;
		default:
			logger.error("Unexpected account status cd '" + statusCd + "'.");
			return null;
		}
	}

	private List<AccountSearchRecord> makeSearchRecords(List<Account> accounts) {

		Set<Long> clientIds = Sets.newHashSet();
		for (Account account : accounts) {
			clientIds.add(account.getClientId());
		}

		EntityMap<Client> clientMap = EntityMap.newEntityMap(clientDao.findByIdList(clientIds));
		Set<Long> personIds = Sets.newHashSet();
		for (Client client : clientMap) {
			personIds.add(client.getPersonId());
		}

		EntityMap<Person> personMap = EntityMap.newEntityMap(personDao.findByIdList(personIds));

		List<Phone> primaryPhones = phoneDao.findPrimaryPhone(personIds);
		Map<Long, Phone> primaryPhoneMap = Maps.newHashMap();
		for (Phone primaryPhone : primaryPhones) {
			primaryPhoneMap.put(primaryPhone.getPersonId(), primaryPhone);
		}

		List<AccountSearchRecord> records = Lists.newArrayList();
		for (Account account : accounts) {
			AccountSearchRecord record = new AccountSearchRecord();
			record.setAccountId(account.getAccountId());
			record.setClientId(account.getClientId());
			record.setStatus(getCodeValueRecord(account.getStatusCd()));

			Client client = clientMap.getEntity(account.getClientId());
			if (client != null) {
				Person person = personMap.getEntity(client.getPersonId());
				record.setEmail(person.getEmail());
				record.setNameFullFormatted(person.getNameFullFormatted());

				Phone primaryPhone = primaryPhoneMap.get(client.getPersonId());
				if (primaryPhone != null) {
					record.setPrimaryPhone(primaryPhone.getPhoneNumber());
				}
			}
			records.add(record);
		}
		return records;
	}
}
