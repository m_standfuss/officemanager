package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.biz.maps.EntityMap;
import officemanager.biz.maps.OrderMap;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.OrderPaymentsRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.PaymentRecord.PaymentType;
import officemanager.data.access.AccountDao;
import officemanager.data.access.AccountTransactionDao;
import officemanager.data.access.Dao;
import officemanager.data.access.OrderToPaymentDao;
import officemanager.data.access.PaymentDao;
import officemanager.data.models.Account;
import officemanager.data.models.AccountTransaction;
import officemanager.data.models.OrderToPayment;
import officemanager.data.models.Payment;
import officemanager.utility.CollectionUtility;

public class PaymentDelegate extends OrderHelperDelegate {

	private static final Logger logger = LogManager.getLogger(PaymentDelegate.class);

	private static final String ACTIVE_CDF = "ACTIVE";

	private static final String DEBIT_CDF = "DEBIT";
	private static final String CREDIT_CDF = "CREDIT";

	private static final String CASH_CDF = "CASH";
	private static final String CHECK_CDF = "CHECK";
	private static final String CLIENT_CREDIT_CDF = "CLIENT_CREDIT";
	private static final String CREDIT_CARD_CDF = "CREDIT_CARD";
	private static final String FINANCING_CDF = "FINANCING";
	private static final String GIFT_CARD_CDF = "GIFT_CARD";
	private static final String PENDING_CDF = "PENDING";
	private static final String TRADE_IN_CDF = "TRADE_IN";
	private static final String REFUND_CDF = "REFUND";

	private final AccountDao accountDao;
	private final AccountTransactionDao accountTransactionDao;
	private final OrderToPaymentDao orderToPaymentDao;
	private final PaymentDao paymentDao;

	public PaymentDelegate() {
		accountDao = new AccountDao();
		accountTransactionDao = new AccountTransactionDao();
		orderToPaymentDao = new OrderToPaymentDao();
		paymentDao = new PaymentDao();
	}

	public List<CodeValueRecord> getPaymentOptions(boolean includeClientCredit, boolean includeTradeIn) {
		List<CodeValueRecord> records = getActiveCodeValueRecords(PAYMENT_TYPE_CDSET);

		List<CodeValueRecord> rtrn = Lists.newArrayList();
		for (CodeValueRecord record : records) {
			if (record.getCodeValue().equals(getCodeValue(PAYMENT_TYPE_CDSET, REFUND_CDF))) {
				continue;
			}
			if (!includeClientCredit
					&& record.getCodeValue().equals(getCodeValue(PAYMENT_TYPE_CDSET, CLIENT_CREDIT_CDF))) {
				continue;
			}
			if (!includeTradeIn && record.getCodeValue().equals(getCodeValue(PAYMENT_TYPE_CDSET, TRADE_IN_CDF))) {
				continue;
			}
			rtrn.add(record);
		}
		return rtrn;
	}

	public List<CodeValueRecord> getCreditCardCompanies() {
		return getActiveCodeValueRecords(CRDT_CARD_COMPANIES_CDSET);
	}

	public PaymentType getPaymentType(Long codeValue) {
		return convertToPaymentType(codeValue);
	}

	/**
	 * Checks if a client has an active and open account. For determining
	 * whether or not to display the client credit payment option.
	 * 
	 * @param clientId
	 *            The client id to check for.
	 * @return Whether the client has an open, active account or not.
	 */
	public boolean hasClientCreditOption(Long clientId) {
		logger.debug("Starting hasClientCreditOption for clientId=" + clientId);
		checkNotNull(clientId);
		Account account = accountDao.findActiveAccountByClientId(clientId);
		if (account == null) {
			logger.debug("No account was found for the client.");
			return false;
		}
		if (account.getStatusCd() == null) {
			logger.debug("The account has a null status code value, unable to determine if account is currently open.");
			return false;
		}
		boolean hasActiveAccount = account.getStatusCd().equals(getCodeValue(CLIENT_CRDT_STATUS_CDSET, ACTIVE_CDF));
		logger.debug("Client account status = " + account.getStatusCd() + ", active = " + hasActiveAccount + ".");
		return hasActiveAccount;
	}

	public Long getAccountId(Long clientId){
		logger.debug("Starting getAccountId for clientId = " + clientId);
		checkNotNull(clientId);
		Account account = accountDao.findActiveAccountByClientId(clientId);
		if (account == null) {
			logger.error("No active account was found for the client id = " + clientId);
			return null;
		}
		return account.getAccountId();
	}

	public List<PaymentRecord> getPayments(Date startDttm, Date endDttm) {
		logger.debug("Starting getPayments with startDttm = " + startDttm + " and endDttm = " + endDttm);
		checkArgument(startDttm != null, "startDttm cannot be null");
		checkArgument(endDttm != null, "endDttm cannot be null");

		List<Payment> payments = paymentDao.findActiveByDate(startDttm, endDttm);
		List<PaymentRecord> records = Lists.newArrayList();
		for (Payment payment : payments) {
			records.add(makePaymentRecord(payment));
		}
		return records;
	}

	/**
	 * Gets a list of all the payments made towards an order.
	 * 
	 * @param orderId
	 *            The order id to get payments for.
	 * @return The list of payments for that order.
	 */
	public List<PaymentRecord> getPaymentsForOrder(Long orderId) {
		logger.debug("Starting getOrderPayments for orderId=" + orderId);
		checkNotNull(orderId);
		List<OrderToPayment> otpList = orderToPaymentDao.getActiveByOrderId(orderId);
		if (CollectionUtility.isNullOrEmpty(otpList)) {
			logger.debug("No active payments were found for the order.");
			return Lists.newArrayList();
		}

		EntityMap<Payment> paymentMap = getPaymentMap(otpList);
		List<PaymentRecord> records = Lists.newArrayList();
		for (OrderToPayment otp : otpList) {
			Payment payment = paymentMap.getEntity(otp.getPaymentId());
			if (payment == null) {
				logger.error("Unable to find the expected payment row with payment id = " + otp.getPaymentId());
				continue;
			}
			records.add(makePaymentRecord(payment));
		}
		return records;
	}

	/**
	 * Gets the current balance of the given order.
	 * 
	 * @param orderId
	 *            The order id to get a balance for.
	 * @return The balance of the order.
	 */
	public BigDecimal getOrderBalance(Long orderId) {
		logger.debug("Starting getOrderBalance for order id=" + orderId);
		checkNotNull(orderId);
		return getBalance(getOrderTotal(orderId), getPaymentsForOrder(orderId));
	}

	public OrderPaymentsRecord getOrderPaymentsRecord(Long orderId) {
		logger.debug("Starting getOrderPaymentsRecord for order id=" + orderId);
		checkNotNull(orderId);
		List<PaymentRecord> payments = getPaymentsForOrder(orderId);
		OrderPaymentsRecord record = new OrderPaymentsRecord();
		record.setOrderTotal(getOrderTotal(orderId));
		record.setPayments(payments);
		return record;
	}

	/**
	 * Creates a new payment for the order.</br>
	 * </br>
	 * *Note* Payments can only be inserted not updated. If a payment id is
	 * specified on the record and exception will be thrown.
	 * 
	 * @param orderId
	 *            The order id to add a payment too.
	 * @param paymentRecord
	 *            The payment record to add to the client.
	 * @return The payment id of the payment made.
	 * @throws NullPointerException
	 *             If the order id or record is null.
	 * @throws IllegalArgumentException
	 *             If the record is not in a valid state.
	 */
	public Long makePaymentOnOrder(Long orderId, PaymentRecord paymentRecord) {
		logger.debug(
				"Starting makePaymentOnOrder for orderId=" + orderId + ", and payment=" + paymentRecord.toString());
		checkNotNull(orderId);
		throwIfInvalid(paymentRecord);
		if (paymentRecord.getPaymentId() != null) {
			throw new IllegalStateException("A payment cannot be updated.");
		}
		Payment payment = createPayment(paymentRecord);

		Dao.openTransaction();
		try {
			paymentDao.persist(payment);

			if (paymentRecord.getPaymentType() == PaymentType.CLIENT_CREDIT) {
				AccountTransaction accntTrans = new AccountTransaction();
				accntTrans.setAccountId(paymentRecord.getAccountId());
				accntTrans.setPaymentId(payment.getPaymentId());
				accntTrans.setTransactionTypeCd(getCodeValue(CLIENT_CRDT_TRANS_TYPE_CDSET, DEBIT_CDF));
				accountTransactionDao.persist(accntTrans);
			}
			OrderToPayment otp = new OrderToPayment();
			otp.setOrderId(orderId);
			otp.setPaymentId(payment.getPaymentId());
			orderToPaymentDao.persist(otp);
		} finally {
			Dao.closeTransaction();
		}
		return payment.getPaymentId();
	}

	/**
	 * Creates a new payment for the given account. </br>
	 * </br>
	 * *Note* Payments can only be inserted not updated. If a payment id is
	 * specified on the record an exception will be thrown.
	 * 
	 * @param accountId
	 *            The account id to add a payment too.
	 * @param paymentRecord
	 *            The payment to add to the account.
	 * @return The payment id of the payment made.
	 * @throws NullPointerException
	 *             If the accountId or paymentRecord are null.
	 * @throws IllegalArgumentException
	 *             If the payment record is invalid, has a payment id specified
	 *             or is of type CLIENT_CREDIT.
	 */
	public Long makePaymentOnAccount(Long accountId, PaymentRecord paymentRecord) {
		logger.debug("Starting makePaymentOnAccount for accountId=" + accountId + ", and payment="
				+ paymentRecord.toString());
		checkNotNull(accountId);
		throwIfInvalid(paymentRecord);
		if (paymentRecord.getPaymentId() != null) {
			throw new IllegalStateException("A payment cannot be updated.");
		}
		checkArgument(paymentRecord.getPaymentType() != PaymentType.CLIENT_CREDIT,
				"Cannot make a client credit payment on a client credit account.");
		Dao.openTransaction();
		Payment payment = createPayment(paymentRecord);
		try {
			paymentDao.persist(payment);

			AccountTransaction accntTrans = new AccountTransaction();
			accntTrans.setAccountId(accountId);
			accntTrans.setPaymentId(payment.getPaymentId());
			accntTrans.setTransactionTypeCd(getCodeValue(CLIENT_CRDT_TRANS_TYPE_CDSET, CREDIT_CDF));
			accountTransactionDao.persist(accntTrans);
		} finally {
			Dao.closeTransaction();
		}
		return payment.getPaymentId();
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeSetsAndCdfMeanings = Lists.newArrayList();
		codeSetsAndCdfMeanings.add(PAYMENT_TYPE_CDSET);
		codeSetsAndCdfMeanings.add(CLIENT_CRDT_TRANS_TYPE_CDSET);
		codeSetsAndCdfMeanings.add(CLIENT_CRDT_STATUS_CDSET);
		return codeSetsAndCdfMeanings;
	}

	/**
	 * Calculates a balance based upon the total supplied and the list of
	 * payments that have been made against that balance.
	 * 
	 * @param cost
	 *            The total of the cost.
	 * @param payments
	 *            The payments made against that cost.
	 * @return The balance.
	 */
	private BigDecimal getBalance(BigDecimal cost, List<PaymentRecord> payments) {
		BigDecimal paymentTotal = Calculator.calculatePaymentTotal(payments);
		logger.debug("Payment total " + paymentTotal.toString());
		return Calculator.calculateBalance(cost, payments);
	}

	/**
	 * Gets the total cost for an order.
	 * 
	 * @param orderId
	 *            The order identifier.
	 * @return The cost of that order.
	 */
	private BigDecimal getOrderTotal(Long orderId) {
		OrderMap map = getOrderMap(Lists.newArrayList(orderId));
		BigDecimal orderTotal = map.getOrderTotal(orderId);
		logger.debug("Order total " + orderTotal.toString());
		return orderTotal;
	}

	/**
	 * Creates an entity map of all the active payments tied to a list of order
	 * to payments.
	 * 
	 * @param otpList
	 *            The list of order to payments to get payments for.
	 * @return The map of payments.
	 */
	private EntityMap<Payment> getPaymentMap(List<OrderToPayment> otpList) {
		if (CollectionUtility.isNullOrEmpty(otpList)) {
			logger.debug("No payments found for order.");
			return EntityMap.newEntityMap();
		}

		List<Long> paymentIds = Lists.newArrayList();
		for (OrderToPayment otp : otpList) {
			paymentIds.add(otp.getPaymentId());
		}
		List<Payment> payments = paymentDao.findByIdList(paymentIds);
		return EntityMap.newEntityMap(payments);
	}

	/**
	 * Creates a new payment record by using the supplied parameters.
	 * 
	 * @param payment
	 *            The payment model to use.
	 * @return The resulting payment record.
	 */
	private PaymentRecord makePaymentRecord(Payment payment) {
		PaymentRecord record = new PaymentRecord();
		record.setPaymentId(payment.getPaymentId());
		record.setAmount(payment.getAmount());
		record.setCreatedDtTm(payment.getCreatedDttm());
		record.setPaymentDescription(payment.getPaymentDesc());
		record.setPaymentNumber(payment.getPaymentNbr());
		record.setPaymentType(convertToPaymentType(payment.getPaymentTypeCd()));
		record.setPaymentTypeDisplay(getDisplay(payment.getPaymentTypeCd()));
		return record;
	}

	/**
	 * Creates a new Payment model by using the supplied parameters.
	 * 
	 * @param paymentRecord
	 *            The payment record to use.
	 * @return The resulting payment model.
	 */
	private Payment createPayment(PaymentRecord paymentRecord) {
		Payment payment = new Payment();
		payment.setAmount(paymentRecord.getAmount());
		payment.setPaymentDesc(paymentRecord.getPaymentDescription());
		payment.setPaymentNbr(paymentRecord.getPaymentNumber());
		payment.setPaymentTypeCd(convertToPaymentTypeCd(paymentRecord.getPaymentType()));
		return payment;
	}

	/**
	 * Converts a payment type to its corresponding code value.
	 * 
	 * @param paymentType
	 *            The payment type
	 * @return The code value, will be null if unable to match or null payment
	 *         type.
	 */
	private Long convertToPaymentTypeCd(PaymentType paymentType) {
		if (paymentType == null) {
			logger.error("Unable to convert a null payment type to a code value.");
			return null;
		}
		switch (paymentType) {
		case CASH:
			return getCodeValue(PAYMENT_TYPE_CDSET, CASH_CDF);
		case CHECK:
			return getCodeValue(PAYMENT_TYPE_CDSET, CHECK_CDF);
		case CLIENT_CREDIT:
			return getCodeValue(PAYMENT_TYPE_CDSET, CLIENT_CREDIT_CDF);
		case CREDIT_CARD:
			return getCodeValue(PAYMENT_TYPE_CDSET, CREDIT_CARD_CDF);
		case FINANCING:
			return getCodeValue(PAYMENT_TYPE_CDSET, FINANCING_CDF);
		case GIFT_CARD:
			return getCodeValue(PAYMENT_TYPE_CDSET, GIFT_CARD_CDF);
		case PENDING:
			return getCodeValue(PAYMENT_TYPE_CDSET, PENDING_CDF);
		case TRADE_IN:
			return getCodeValue(PAYMENT_TYPE_CDSET, TRADE_IN_CDF);
		case REFUND:
			return getCodeValue(PAYMENT_TYPE_CDSET, REFUND_CDF);
		default:
			logger.error(
					"Unable to match a payment type '" + paymentType.name() + "' to its corresponding code value.");
			return null;
		}
	}

	/**
	 * Converts a payment type code value to its corresponding payment type
	 * enum.
	 * 
	 * @param paymentTypeCd
	 *            The code value for the payment type.
	 * @return The payment type enum.
	 */
	private PaymentType convertToPaymentType(Long paymentTypeCd) {
		if (paymentTypeCd == null) {
			logger.error("A null payment type was found.");
			return null;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, CASH_CDF))) {
			return PaymentType.CASH;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, CHECK_CDF))) {
			return PaymentType.CHECK;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, CREDIT_CARD_CDF))) {
			return PaymentType.CREDIT_CARD;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, CLIENT_CREDIT_CDF))) {
			return PaymentType.CLIENT_CREDIT;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, PENDING_CDF))) {
			return PaymentType.PENDING;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, GIFT_CARD_CDF))) {
			return PaymentType.GIFT_CARD;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, FINANCING_CDF))) {
			return PaymentType.FINANCING;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, TRADE_IN_CDF))) {
			return PaymentType.TRADE_IN;
		}
		if (paymentTypeCd.equals(getCodeValue(PAYMENT_TYPE_CDSET, REFUND_CDF))) {
			return PaymentType.REFUND;
		}
		logger.error("Unable to match the payment type cd to a payment type.");
		return null;
	}
}