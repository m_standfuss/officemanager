package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.EntityHelper;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PartRecord;
import officemanager.biz.records.PartSearchRecord;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.searchingcriteria.PartSearchCriteria;
import officemanager.data.access.LocationDao;
import officemanager.data.access.PartDao;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PersonnelDao;
import officemanager.data.models.Location;
import officemanager.data.models.Part;
import officemanager.data.models.Person;
import officemanager.data.models.Personnel;
import officemanager.utility.CollectionUtility;

public class PartDelegate extends SellableDelegate {

	private static final Logger logger = LogManager.getLogger(PartDelegate.class);

	private static final String OUT_OF_STOCK_CDF = "OUTOFSTOCK";
	private static final String LOW_STOCK_CDF = "LOWSTOCK";
	private static final String IN_STOCK_CDF = "INSTOCK";

	private final LocationDao locationDao;
	private final PartDao partDao;
	private final PersonDao personDao;
	private final PersonnelDao personnelDao;

	/**
	 * Default constructor
	 */
	public PartDelegate() {
		locationDao = new LocationDao();
		partDao = new PartDao();
		personnelDao = new PersonnelDao();
		personDao = new PersonDao();
	}

	public List<CodeValueRecord> getPartInventoryStatus() {
		return getActiveCodeValueRecords(INVNTRY_STATUS_CDSET);
	}

	public List<CodeValueRecord> getProductTypes() {
		return getActiveCodeValueRecords(MERC_PRODUCT_TYPE_CDSET);
	}

	public List<CodeValueRecord> getPartManufacturers() {
		return getActiveCodeValueRecords(MERC_MANUF_CDSET);
	}

	public List<CodeValueRecord> getPartCategories() {
		return getActiveCodeValueRecords(MERC_CATEGORIES_CDSET);
	}

	/**
	 * Persists the PartRecord into the datastore. If the
	 * {@link PartRecord#getPartId()} is null then a new Part model will be
	 * added else the existing Part will be updated. If a new model is used then
	 * any fields not persistible will be defaulted to the initial value or if
	 * updated will use existing persisted value.
	 *
	 * @param record
	 *            The record to persist to the database.
	 * @return The partId of the record persisted.
	 */
	public Long insertOrUpdatePart(PartRecord record) {
		logger.debug("Start insertOrUpdatePart for part record = " + record.toString());
		record.validateRecord().throwIfInvalid();
		Part part;
		if (record.getPartId() == null) {
			logger.debug("Inserting a new Part.");
			part = Part.getDefaultPart();
		} else {
			logger.debug("Updating existing part.");
			part = partDao.findById(record.getPartId());
			if (part == null) {
				logger.error("Unable to find a part row with part_id=" + record.getPartId() + ". Inserting a new row.");
				part = Part.getDefaultPart();
			}
		}
		fillOutPart(part, record);
		partDao.persist(part);
		return part.getPartId();
	}

	public void updatePartLocation(Long locationId, List<Long> partIds) {
		logger.debug("Starting updatePartLocation with locationId=" + locationId + " for partIds=" + partIds);
		partDao.clearLocation(locationId);
		if (!CollectionUtility.isNullOrEmpty(partIds)) {
			partDao.setLocation(locationId, partIds);
		}
	}

	/**
	 * Gets a part record for the given part id. Will return null if the part id
	 * does not belong to a part.
	 *
	 * @param partId
	 *            The identifier of the part.
	 * @return The part record. Null if the part is not found.
	 */
	public PartRecord getPart(Long partId) {
		logger.debug("Starting getPart for partId=" + partId);
		Part part = partDao.findById(partId);
		Location location = null;
		if (part.getLocationId() != null) {
			location = locationDao.findById(part.getLocationId());
		}
		Personnel personnel = personnelDao.findById(part.getUpdtPrsnlId());
		Person person = null;
		if (personnel != null && personnel.getPersonId() != null) {
			person = personDao.findById(personnel.getPersonId());
		}

		return convertToPartRecord(part, location, person);
	}

	public void deactivePart(long partId) {
		logger.debug("Starting deactivatePart for partId=" + partId);
		partDao.inactivate(partId);
		deactivateSalesForItem(partId);
	}

	public List<SaleRecord> getActiveSales(Long partId) {
		logger.debug("Starting getActiveSales for partId=" + partId);
		return getActiveSaleRecords(partId);
	}

	/**
	 * Loads a list of part selection records for the provided searching
	 * criteria.
	 *
	 * @param searchCritieria
	 *            The searching criteria for the parts.
	 * @return The list of PartSelectionRecords for the searching criteria.
	 */
	public List<PartSearchRecord> searchParts(PartSearchCriteria searchCritieria) {
		logger.debug("Starting searchParts with searching criteria = " + searchCritieria);
		if (searchCritieria.isEmpty()) {
			logger.debug("No searching parameters were specified, returning no results.");
			return Lists.newArrayList();
		}

		List<Part> partResults = partDao.findBySearchCriteria(searchCritieria.partName, searchCritieria.partDescription,
				searchCritieria.partCategoryCds, searchCritieria.partManufacturerCds, searchCritieria.productTypeCds,
				searchCritieria.onlyInStock, searchCritieria.partManufacturerId);
		return makeSearchRecords(partResults);
	}

	/**
	 * Searches parts for the specified barcode.
	 *
	 * @param barcode
	 *            The barcode to match.
	 * @return The active parts matching the barcode.
	 */
	public List<PartSearchRecord> searchParts(String barcode) {
		logger.debug("Starting searchParts for barcode = " + barcode);
		checkNotNull(barcode);
		List<Part> parts = partDao.findByBarcode(barcode);
		return makeSearchRecords(parts);
	}

	public List<PartSearchRecord> searchParts(Long locationId) {
		logger.debug("Starting searchParts for locationId=" + locationId);
		checkNotNull(locationId);
		List<Part> parts = partDao.findByLocation(locationId);
		return makeSearchRecords(parts);
	}

	/**
	 * Will return the parts that currently have a low inventory designation,
	 * where there current inventory count is lower then their inventory
	 * threshold, but greater then 0.
	 *
	 * @return The list of parts that meet the inventory criteria.
	 */
	public List<PartSearchRecord> getLowStockParts() {
		logger.debug("Starting getLowInventoryParts.");
		List<Part> parts = partDao.findLowInventory();
		return makeSearchRecords(parts);
	}

	/**
	 * Will return the parts that are currently out of stock, where there
	 * current inventory count is 0 or less.
	 *
	 * @return The list of parts that meet the inventory criteria.
	 **/
	public List<PartSearchRecord> getOutOfStockParts() {
		logger.debug("Starting getOutOfStockParts.");
		List<Part> parts = partDao.findOutOfStock();
		return makeSearchRecords(parts);
	}

	/**
	 * Adds inventory counts to parts.
	 *
	 * @param inventoryCountMap
	 *            The part identifier to the amount to add to its inventory.
	 */
	public void addInventory(Map<Long, Integer> inventoryCountMap) {
		logger.debug("Starting addInventory for inventoryCountMap=" + inventoryCountMap);
		for (Map.Entry<Long, Integer> inventoryEntry : inventoryCountMap.entrySet()) {
			partDao.addInventory(inventoryEntry.getKey(), inventoryEntry.getValue());
		}
	}

	@Override
	protected String getParentEntityName() {
		return "PART";
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeSetsAndCdfMeanings = Lists.newArrayList();
		codeSetsAndCdfMeanings.add(INVNTRY_STATUS_CDSET);
		codeSetsAndCdfMeanings.add(MERC_PRODUCT_TYPE_CDSET);
		codeSetsAndCdfMeanings.add(MERC_MANUF_CDSET);
		codeSetsAndCdfMeanings.add(MERC_CATEGORIES_CDSET);
		return codeSetsAndCdfMeanings;
	}

	private List<PartSearchRecord> makeSearchRecords(List<Part> partResults) {
		if (partResults.isEmpty()) {
			logger.debug("No parts were found.");
			return Lists.newArrayList();
		}
		List<Long> partIds = EntityHelper.getPrimaryKeyList(Long.class, partResults);
		Map<Long, List<SaleRecord>> saleMap = getActiveSalesForEntities(partIds);

		List<PartSearchRecord> recordList = Lists.newArrayList();
		for (Part part : partResults) {
			recordList.add(makePartSearchRecord(part, saleMap.get(part.getPartId())));
		}
		return recordList;
	}

	/**
	 * Gets the display for the inventory status of the part based on its
	 * inventory count and inventory threshold.
	 *
	 * @param part
	 *            The part to get a inventory status for.
	 * @return The inventory status.
	 */
	private String getInventoryStatus(Part part) {
		if (part.getInventoryCount() <= 0) {
			return getDisplay(INVNTRY_STATUS_CDSET, OUT_OF_STOCK_CDF);
		}
		if (part.getInventoryCount() < part.getInventoryThreshold()) {
			return getDisplay(INVNTRY_STATUS_CDSET, LOW_STOCK_CDF);
		}
		return getDisplay(INVNTRY_STATUS_CDSET, IN_STOCK_CDF);
	}

	/**
	 * Creates a part selection record for the part and the list of sales.
	 *
	 * @param part
	 *            The part to create a selection record for.
	 * @param saleList
	 *            The list of sales to apply to part.
	 * @return The part selection record.
	 */
	private PartSearchRecord makePartSearchRecord(Part part, List<SaleRecord> saleList) {
		PartSearchRecord record = new PartSearchRecord(part.getPartId());
		record.setAmountSold(part.getAmountSold());
		record.setBasePrice(part.getBasePrice());
		record.setCurrentPrice(getPrice(part.getBasePrice(), saleList));
		record.setInventoryStatus(getInventoryStatus(part));
		record.setInventoryCount(part.getInventoryCount());
		record.setManufacturerId(part.getManufPartId());
		record.setOnSale(!CollectionUtility.isNullOrEmpty(saleList));
		record.setPartDescription(part.getDescription());
		record.setPartManufacturer(getDisplay(part.getManufacturerCd()));
		record.setPartName(part.getPartName());
		return record;
	}

	/**
	 * Creates a PartRecord out of the given parameters.
	 *
	 * @param part
	 *            The part model.
	 * @param location
	 *            The location model tied to the part.
	 * @param personnel
	 *            The last updated personnel for the part.
	 * @return The created PartRecord.
	 */
	private PartRecord convertToPartRecord(Part part, Location location, Person lastUpdated) {
		PartRecord record = new PartRecord(part.getPartId());
		record.setCategoryType(getCodeValueRecord(part.getCategoryTypeCd()));
		record.setManufacturer(getCodeValueRecord(part.getManufacturerCd()));
		record.setProductType(getCodeValueRecord(part.getProductTypeCd()));
		record.setAmountSold(part.getAmountSold());
		record.setBarcode(part.getBarcode());
		record.setBasePrice(part.getBasePrice());
		record.setDescription(part.getDescription());
		record.setInventoryCount(part.getInventoryCount());
		record.setInventoryThreshold(part.getInventoryThreshold());
		record.setLocationId(part.getLocationId());
		record.setManufPartId(part.getManufPartId());
		record.setPartName(part.getPartName());
		record.setUpdtDtTm(part.getUpdtDttm());

		if (location == null) {
			logger.debug("The location for part could not be found.");
		} else {
			record.setLocationDisplay(location.getDisplay());
			record.setLocationDisplay(location.getDisplay());
		}

		if (lastUpdated == null) {
			logger.debug("The last updated person could not be found.");
		} else {
			record.setLastUpdatedBy(lastUpdated.getNameFullFormatted());
		}
		return record;
	}

	/**
	 * Takes the part supplied and populates it with the values provided by the
	 * record.
	 *
	 * @param part
	 *            The part to fill out.
	 * @param record
	 *            The record to use to fill it.
	 */
	private void fillOutPart(Part part, PartRecord record) {
		part.setPartId(record.getPartId());
		part.setCategoryTypeCd(record.getCategoryType().getCodeValue());
		part.setManufacturerCd(record.getManufacturer().getCodeValue());
		part.setProductTypeCd(record.getProductType().getCodeValue());
		part.setBarcode(record.getBarcode());
		part.setBasePrice(record.getBasePrice());
		part.setDescription(record.getDescription());
		part.setInventoryCount(record.getInventoryCount());
		part.setInventoryThreshold(record.getInventoryThreshold());
		part.setLocationId(record.getLocationId());
		part.setManufPartId(record.getManufPartId());
		part.setPartName(record.getPartName());
	}
}
