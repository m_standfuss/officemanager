package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.records.ClientProductRecord;
import officemanager.biz.records.ClientProductSelectionRecord;
import officemanager.biz.records.CodeValueRecord;
import officemanager.data.access.ClientProductDao;
import officemanager.data.models.ClientProduct;

public class ClientProductDelegate extends Delegate {

	private static final Logger logger = LogManager.getLogger(ClientProductDelegate.class);

	private final ClientProductDao clientProductDao;

	public ClientProductDelegate() {
		clientProductDao = new ClientProductDao();
	}

	public List<CodeValueRecord> getProductStatuses() {
		return getActiveCodeValueRecords(PRODUCT_STATUS_CDSET);
	}

	public List<CodeValueRecord> getProductTypes() {
		return getActiveCodeValueRecords(PRODUCT_TYPE_CDSET);
	}

	/**
	 * Inserts a new client product into the data store.
	 * 
	 * @param record
	 *            The client product to insert or update depending on if the
	 *            client product id is set.
	 * @return The primary key returned.
	 * @throws IllegalArgumentException
	 *             If the record is in an invalid state.
	 */
	public Long persistClientProduct(ClientProductRecord record) {
		logger.debug("Starting persistClientProduct for record=" + record);
		checkNotNull(record);
		throwIfInvalid(record);

		ClientProduct clientProduct;
		if (record.getClientProductId() == null) {
			clientProduct = new ClientProduct();
		} else {
			clientProduct = clientProductDao.findById(record.getClientProductId());
		}
		return persistClientProduct(record, clientProduct);
	}

	/**
	 * Gets the active client product selection records for the given client.
	 * Used for loading the minimal needed to display options for choosing
	 * products.
	 * 
	 * @param clientId
	 *            The client identifier to get products for.
	 * @return The list of client products selections.
	 */
	public List<ClientProductSelectionRecord> getActiveClientProductSelections(Long clientId) {
		logger.debug("Loading active client products for client id=" + clientId);

		final List<ClientProduct> activeProducts = clientProductDao.findActiveProductsByClientId(clientId);
		logger.debug(activeProducts.size() + " active products found for the client.");

		final List<ClientProductSelectionRecord> recordList = Lists.newArrayList();
		for (final ClientProduct cp : activeProducts) {
			recordList.add(makeClientProductSelection(cp));
		}
		return recordList;
	}

	/**
	 * Gets the active client product records for the given client. Used for
	 * loading the entire client product record, if only needed for selecting
	 * products then use {@link #getActiveClientProductSelections(Long)}
	 * instead.
	 * 
	 * @param clientId
	 *            The client identifier to get products for.
	 * @return The list of client product records.
	 */
	public List<ClientProductRecord> getActiveClientProducts(Long clientId) {
		logger.debug("Starting getActiveClientProducts for clientId=" + clientId);

		List<ClientProduct> activeProducts = clientProductDao.findActiveProductsByClientId(clientId);
		List<ClientProductRecord> records = Lists.newArrayList();
		for (ClientProduct clientProduct : activeProducts) {
			records.add(makeClientProductRecord(clientProduct));
		}
		return records;
	}

	public ClientProductRecord getClientProduct(Long clientProductId) {
		logger.debug("Starting getClientProduct for clientProductId = " + clientProductId);
		ClientProduct cp = clientProductDao.findById(clientProductId);
		if (cp == null) {
			logger.error("Unable to find client product with id = " + clientProductId);
			return null;
		}
		return makeClientProductRecord(cp);
	}

	public void deactivateProduct(Long clientProductId) {
		logger.debug("Starting deactivateProduct for clientProductId = " + clientProductId);
		clientProductDao.inactivate(clientProductId);
	}

	/**
	 * Gets the active client product records for the given client. Used for
	 * loading the entire client product record, if only needed for selecting
	 * products then use {@link #getActiveClientProductSelections(Long)}
	 * instead.
	 * 
	 * @param clientProductIds
	 *            The client product identifiers to get products for.
	 * @return The list of client product records.
	 */
	public List<ClientProductRecord> getClientProducts(List<Long> clientProductIds) {
		logger.debug("Starting getClientProducts for clientProductIds=" + clientProductIds);

		List<ClientProduct> products = clientProductDao.findByIdList(clientProductIds);
		List<ClientProductRecord> records = Lists.newArrayList();
		for (ClientProduct clientProduct : products) {
			records.add(makeClientProductRecord(clientProduct));
		}
		return records;
	}

	@Override
	protected List<Long> warmCache() {
		final List<Long> codeSetAndCdfs = Lists.newArrayList();
		codeSetAndCdfs.add(PRODUCT_STATUS_CDSET);
		codeSetAndCdfs.add(PRODUCT_TYPE_CDSET);
		return codeSetAndCdfs;
	}

	/**
	 * Converts a client product record into the corresponding client product
	 * data model.
	 * 
	 * @param record
	 *            The record to convert.
	 * @return The data model.
	 */
	private Long persistClientProduct(ClientProductRecord record, ClientProduct clientProduct) {
		clientProduct.setClientProductId(record.getClientProductId());
		clientProduct.setClientId(record.getClientId());
		clientProduct.setProductBarcode(record.getBarcode());
		clientProduct.setProductSerialNum(record.getSerialNum());
		clientProduct.setProductManuf(record.getManufacturer());
		clientProduct.setProductModel(record.getModel());
		clientProduct.setProductStatusCd(record.getProductStatus().getCodeValue());
		clientProduct.setProductTypeCd(record.getProductType().getCodeValue());
		clientProduct.setProductSerialNum(record.getSerialNum());
		clientProductDao.persist(clientProduct);
		return clientProduct.getClientProductId();
	}

	/**
	 * Creates a product selection record based on the client product model and
	 * global maps.
	 * 
	 * @param cp
	 *            The client product model to create a selection record for.
	 * @return The selection record.
	 */
	private ClientProductSelectionRecord makeClientProductSelection(ClientProduct cp) {
		final ClientProductSelectionRecord record = new ClientProductSelectionRecord(cp.getClientProductId());
		record.setSerialNumber(cp.getProductSerialNum());
		record.setProductManufacturer(cp.getProductManuf());
		record.setProductModel(cp.getProductModel());
		record.setProductType(getDisplay(cp.getProductTypeCd()));
		return record;
	}

	private ClientProductRecord makeClientProductRecord(ClientProduct clientProduct) {
		ClientProductRecord record = new ClientProductRecord(clientProduct.getClientProductId());
		record.setBarcode(clientProduct.getProductBarcode());
		record.setClientId(clientProduct.getClientId());
		record.setSerialNum(clientProduct.getProductSerialNum());
		record.setManufacturer(clientProduct.getProductManuf());
		record.setModel(clientProduct.getProductModel());
		record.setProductStatus(getCodeValueRecord(clientProduct.getProductStatusCd()));
		record.setProductType(getCodeValueRecord(clientProduct.getProductTypeCd()));
		record.setSerialNum(clientProduct.getProductSerialNum());
		record.setWorkCount(new Integer(clientProduct.getWorkCnt()));
		return record;
	}
}
