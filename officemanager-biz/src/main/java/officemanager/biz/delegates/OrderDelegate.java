package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.maps.EntityMap;
import officemanager.biz.maps.OrderMap;
import officemanager.biz.records.OrderClientProductRecord;
import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderRecord.OrderStatus;
import officemanager.biz.records.OrderRecord.OrderType;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.biz.records.PriceOverrideRecord;
import officemanager.data.access.ClientProductDao;
import officemanager.data.access.Dao;
import officemanager.data.access.FlatRateServiceDao;
import officemanager.data.access.OrderDao;
import officemanager.data.access.OrderToClientProductDao;
import officemanager.data.access.OrderToFlatRateServiceDao;
import officemanager.data.access.OrderToPartDao;
import officemanager.data.access.PartDao;
import officemanager.data.access.PersonnelDao;
import officemanager.data.access.PriceOverrideDao;
import officemanager.data.access.ServiceDao;
import officemanager.data.models.Client;
import officemanager.data.models.ClientProduct;
import officemanager.data.models.FlatRateService;
import officemanager.data.models.Order;
import officemanager.data.models.OrderToClientProduct;
import officemanager.data.models.OrderToFlatRateService;
import officemanager.data.models.OrderToPart;
import officemanager.data.models.Part;
import officemanager.data.models.Person;
import officemanager.data.models.Personnel;
import officemanager.data.models.Phone;
import officemanager.data.models.PriceOverride;
import officemanager.data.models.Service;
import officemanager.utility.CollectionUtility;

public class OrderDelegate extends OrderHelperDelegate {

	static final Logger logger = LogManager.getLogger(OrderDelegate.class);

	private static final String WORK_CDF = "WORK";
	private static final String MERCHANDISE_CDF = "MERCHANDISE";

	private static final String CLOSED_CDF = "CLOSED";
	private static final String INERROR_CDF = "INERROR";
	private static final String OPEN_CDF = "OPEN";

	private final ClientProductDao clientProductDao;
	private final FlatRateServiceDao flatRateServiceDao;
	private final OrderDao orderDao;
	private final OrderToClientProductDao orderToClientProductDao;
	private final OrderToFlatRateServiceDao orderToFlatRateServiceDao;
	private final OrderToPartDao orderToPartDao;
	private final PartDao partDao;
	private final PersonnelDao personnelDao;
	private final PriceOverrideDao priceOverrideDao;
	private final ServiceDao serviceDao;

	public OrderDelegate() {
		clientProductDao = new ClientProductDao();
		flatRateServiceDao = new FlatRateServiceDao();
		orderDao = new OrderDao();
		orderToClientProductDao = new OrderToClientProductDao();
		orderToFlatRateServiceDao = new OrderToFlatRateServiceDao();
		orderToPartDao = new OrderToPartDao();
		partDao = new PartDao();
		personnelDao = new PersonnelDao();
		priceOverrideDao = new PriceOverrideDao();
		serviceDao = new ServiceDao();
	}

	public OrderStatus getOrderStatus(Long orderId) {
		logger.debug("Starting getOrderStatus for orderId=" + orderId);
		checkNotNull(orderId);

		Order order = orderDao.findById(orderId);
		if (order == null) {
			logger.error("Unable to find order model for orderId = " + orderId);
			return OrderStatus.UNKNOWN;
		}

		return getOrderStatusEnum(order.getOrderStatusCd());
	}

	public void closeOrder(Long orderId, Long prsnlId) {
		logger.debug("Starting closeOrder for order id = " + orderId + " and closing personnel=" + prsnlId);
		checkNotNull(orderId);
		checkNotNull(prsnlId);
		Order order = orderDao.findById(orderId);
		if (order == null) {
			throw new IllegalArgumentException("The order id " + orderId + " could not be found.");
		}
		order.setClosedDttm(new Date());
		order.setClosedPrsnlId(prsnlId);
		order.setOrderStatusCd(getOrderStatusCd(OrderStatus.CLOSED));
		orderDao.persist(order);
		incrementSoldAmounts(orderId);
	}

	public void reopenOrder(Long orderId) {
		logger.debug("Starting reopenOrder for order id = " + orderId);
		checkNotNull(orderId);
		Order order = orderDao.findById(orderId);
		if (order == null) {
			throw new IllegalArgumentException("The order id " + orderId + " could not be found.");
		}
		order.setClosedDttm(null);
		order.setClosedPrsnlId(null);
		order.setOrderStatusCd(getOrderStatusCd(OrderStatus.OPEN));
		orderDao.persist(order);
		decrementSoldAmounts(orderId);
	}

	public void markInError(Long orderId) {
		logger.debug("Starting markInError for order id = " + orderId);
		checkNotNull(orderId);
		Order order = orderDao.findById(orderId);
		if (order == null) {
			throw new IllegalArgumentException("The order id " + orderId + " could not be found.");
		}
		order.setOrderStatusCd(getOrderStatusCd(OrderStatus.INERROR));
		orderDao.persist(order);
	}

	public Long getClientId(Long orderId) {
		logger.debug("Starting getClientId for order id=" + orderId);
		checkNotNull(orderId);
		Order order = orderDao.findById(orderId);
		if (order == null) {
			logger.error("Unable to find order with id = " + orderId);
			return null;
		}
		return order.getClientId();
	}

	public Long insertPriceOverride(PriceOverrideRecord record) {
		logger.debug("Starting insertPriceOverride for " + record);
		throwIfInvalid(record);
		PriceOverride priceOverride = new PriceOverride();
		priceOverride.setOrderId(record.getOrderId());
		priceOverride.setOriginalAmount(record.getOriginalAmount());
		priceOverride.setOverrideAmount(record.getOverrideAmount());
		priceOverride.setOverrideDttm(record.getOverrideDttm());
		priceOverride.setOverridePrsnlId(record.getOverridePrsnlId());
		priceOverride.setOverrideReason(record.getOverrideReason());
		priceOverride.setParentEntityId(record.getEntityId());
		priceOverride.setParentEntityName(record.getEntityName().name());

		priceOverrideDao.persist(priceOverride);
		return priceOverride.getPriceOverrideId();
	}

	public List<PriceOverrideRecord> getOverridenPrices(Date startDttm, Date endDttm) {
		logger.debug("Starting getOverriddenPrices for startDttm=" + startDttm + " and endDttm=" + endDttm);
		List<PriceOverride> priceOverrides = priceOverrideDao.findActiveByDates(startDttm, endDttm);

		Set<Long> prsnlIds = Sets.newHashSet();
		for (PriceOverride po : priceOverrides) {
			prsnlIds.add(po.getOverridePrsnlId());
		}
		List<Personnel> personnels = personnelDao.findByIdList(prsnlIds);

		Map<Long, Person> personMap = getPrsnlToPersonMap(personnels);

		List<PriceOverrideRecord> records = Lists.newArrayList();
		for (PriceOverride po : priceOverrides) {
			PriceOverrideRecord record = new PriceOverrideRecord();
			record.setEntityId(po.getParentEntityId());
			record.setEntityName(po.getParentEntityName());
			record.setOrderId(po.getOrderId());
			record.setOriginalAmount(po.getOriginalAmount());
			record.setOverrideAmount(po.getOverrideAmount());
			record.setOverrideDttm(po.getOverrideDttm());
			record.setOverridePrsnlId(po.getOverridePrsnlId());
			record.setOverrideReason(po.getOverrideReason());

			Person person = personMap.get(po.getOverridePrsnlId());
			if (person != null) {
				record.setOverridePrsnlName(person.getNameFullFormatted());
			}
			records.add(record);
		}
		return records;
	}

	/**
	 * Inserts or updated the core order tables based on the values in the
	 * supplied order record. Will preform an insert if the order id field is
	 * null, else will attempt to update the existing order. </br</br>
	 * 
	 * If updating an order any order_to_* will be inactivated that is not
	 * included on the record. <b>*Note</b> this does not include order to
	 * payments.
	 * 
	 * @param record
	 *            The record to insert or update.
	 * @return The corresponding order id that was inserted or updated.
	 * @throws IllegalArgumentException
	 *             If the record is in an invalid state via the
	 *             {@link OrderRecord#validateRecord()} method.
	 * @throws NullPointerException
	 *             If the record is null.
	 */
	public Long insertUpdateOrder(OrderRecord record) {
		checkNotNull(record);
		throwIfInvalid(record);
		Dao.openTransaction();
		Order order;
		try {
			if (record.getOrderId() != null) {
				order = orderDao.findById(record.getOrderId());
				inactiveComponents(record);
			} else {
				order = makeNewOrder(record);
			}
			orderDao.persist(order);
			persistClientProducts(order.getOrderId(), record.getClientProducts());
			persistFlatRates(order.getOrderId(), record.getFlatRates());
			persistParts(order.getOrderId(), record.getParts());
			persistServices(order.getOrderId(), record.getServices());
		} finally {
			Dao.closeTransaction();
		}
		return order.getOrderId();
	}

	public void insertOrUpdatePart(Long orderId, OrderPartRecord part) {
		checkNotNull(orderId);
		persistParts(orderId, Lists.newArrayList(part));
	}

	public void insertOrUpdateParts(Long orderId, List<OrderPartRecord> parts) {
		checkNotNull(orderId);
		throwIfInvalid(parts);
		inactivateOrderParts(orderId, parts);
		persistParts(orderId, parts);
	}

	public void insertOrUpdateService(Long orderId, OrderServiceRecord service) {
		checkNotNull(service);
		persistServices(orderId, Lists.newArrayList(service));
	}

	public void insertOrUpdateServices(Long orderId, List<OrderServiceRecord> services) {
		checkNotNull(orderId, services);
		throwIfInvalid(services);
		inactivateServices(orderId, services);
		persistServices(orderId, services);
	}

	public void insertOrUpdateFlatRate(Long orderId, OrderFlatRateServiceRecord flatRate) {
		checkNotNull(flatRate);
		throwIfInvalid(flatRate);
		persistFlatRates(orderId, Lists.newArrayList(flatRate));
	}

	public void insertOrUpdateFlatRates(Long orderId, List<OrderFlatRateServiceRecord> flatRates) {
		checkNotNull(orderId, flatRates);
		throwIfInvalid(flatRates);
		inactivateFlatRateServices(orderId, flatRates);
		persistFlatRates(orderId, flatRates);
	}

	public List<OrderRecord> loadOrderRecords(Collection<Long> orderIds) {
		logger.debug("Loading order record for orderIds=" + orderIds);
		checkNotNull(orderIds);
		checkArgument(!orderIds.contains(null));
		List<Order> orders = orderDao.findByIdList(orderIds);

		Set<Long> prsnlIds = Sets.newHashSet();
		Set<Long> clientIds = Sets.newHashSet();
		for (Order order : orders) {
			prsnlIds.add(order.getUpdtPrsnlId());
			prsnlIds.add(order.getCreatedPrsnlId());
			prsnlIds.add(order.getClosedPrsnlId());
			clientIds.add(order.getClientId());
		}

		EntityMap<Order> orderMap = EntityMap.newEntityMap(orders);
		OrderMap orderDetailsMap = getOrderMap(orderIds);
		EntityMap<Client> clientMap = makeClientMap(clientIds);
		EntityMap<Personnel> personnelMap = makePersonnelMap(prsnlIds);
		EntityMap<Part> partMap = makePartMap(orderDetailsMap.getParts());
		EntityMap<FlatRateService> flatRateMap = makeFlatRateMap(orderDetailsMap.getFlatRateServices());

		Collection<Client> clients = clientMap.convertToCollection();
		EntityMap<Person> personMap = makePersonMap(personnelMap.convertToCollection(), clients);

		Set<Long> clientPersonIds = Sets.newHashSet();
		clients.forEach(c -> clientPersonIds.add(c.getPersonId()));
		Map<Long, Phone> primaryPhoneMap = makePrimaryPhoneMap(clientPersonIds);

		List<OrderRecord> records = Lists.newArrayList();
		for (Long orderId : orderIds) {
			Order order = orderMap.getEntity(orderId);
			OrderRecord record = new OrderRecord();
			record.setOrderId(orderId);
			record.setClientId(order.getClientId());
			record.setClosedDttm(order.getClosedDttm());
			record.setCreatedDttm(order.getCreatedDttm());
			record.setLastUpdatedDttm(order.getUpdtDttm());
			record.setOrderStatus(getOrderStatusEnum(order.getOrderStatusCd()));
			record.setOrderStatusDisplay(getDisplay(order.getOrderStatusCd()));
			record.setOrderType(getOrderType(order.getOrderTypeCd()));
			record.setOrderTypeDisplay(getDisplay(order.getOrderTypeCd()));

			Personnel createdPrsnl = personnelMap.getEntity(order.getCreatedPrsnlId());
			Personnel updatedPrsnl = personnelMap.getEntity(order.getUpdtPrsnlId());
			Personnel closedPrsnl = personnelMap.getEntity(order.getClosedPrsnlId());

			Client client = clientMap.getEntity(order.getClientId());

			if (createdPrsnl != null) {
				Person person = personMap.getEntity(createdPrsnl.getPersonId());
				if (person != null) {
					record.setCreatedBy(person.getNameFullFormatted());
				}
			}
			if (updatedPrsnl != null) {
				Person person = personMap.getEntity(updatedPrsnl.getPersonId());
				if (person != null) {
					record.setLastUpdatedBy(person.getNameFullFormatted());
				}
			}
			if (closedPrsnl != null) {
				Person person = personMap.getEntity(closedPrsnl.getPersonId());
				if (person != null) {
					record.setClosedBy(person.getNameFullFormatted());
				}
			}
			if (client != null) {
				Person person = personMap.getEntity(client.getPersonId());
				if (person != null) {
					record.setClientName(person.getNameFullFormatted());
				}

				Phone primaryPhone = primaryPhoneMap.get(client.getPersonId());
				if (primaryPhone != null) {
					record.setClientPhone(primaryPhone.getPhoneNumber());
				}
			}

			List<OrderToClientProduct> clientProducts = orderDetailsMap.getClientProducts(orderId);
			EntityMap<ClientProduct> clientProductMap = makeClientProductMap(clientProducts);
			List<OrderClientProductRecord> clientProductRecords = Lists.newArrayList();
			for (OrderToClientProduct otcp : clientProducts) {
				clientProductRecords
						.add(makeOrderProductRecord(otcp, clientProductMap.getEntity(otcp.getClientProductId())));
			}
			record.setClientProducts(clientProductRecords);

			List<OrderPartRecord> partRecords = Lists.newArrayList();
			for (OrderToPart otp : orderDetailsMap.getParts(orderId)) {
				partRecords.add(makeOrderPartRecord(otp, partMap.getEntity(otp.getPartId()),
						clientProductMap.getEntity(otp.getClientProductId())));
			}
			record.setParts(partRecords);

			List<OrderFlatRateServiceRecord> flatRateRecords = Lists.newArrayList();
			for (OrderToFlatRateService otfrs : orderDetailsMap.getFlatRateServices(orderId)) {
				flatRateRecords
						.add(makeOrderFlatRateServiceRecord(otfrs, flatRateMap.getEntity(otfrs.getFlatRateServiceId()),
								clientProductMap.getEntity(otfrs.getClientProductId())));
			}
			record.setFlatRates(flatRateRecords);

			List<OrderServiceRecord> serviceRecords = Lists.newArrayList();
			for (Service s : orderDetailsMap.getServices(orderId)) {
				serviceRecords.add(makeOrderServiceRecord(s, clientProductMap.getEntity(s.getClientProductId())));
			}
			record.setServices(serviceRecords);
			records.add(record);
		}
		return records;
	}

	/**
	 * Loads an order record for the given order id.
	 * 
	 * @param orderId
	 *            The order id to load for.
	 * @return The order record populated with the order's details. Null if the
	 *         order could not be found.
	 */
	public OrderRecord loadOrderRecord(Long orderId) {
		logger.debug("Loading order record for orderId=" + orderId);
		checkNotNull(orderId);
		List<OrderRecord> list = loadOrderRecords(Lists.newArrayList(orderId));
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * Gets the client products for an order.
	 * 
	 * @param orderId
	 *            The order id.
	 * @return The list of client products for a given order id.
	 */
	public List<OrderClientProductRecord> loadClientProducts(Long orderId) {
		logger.debug("Starting loadClientProducts for orderId=" + orderId);
		checkNotNull(orderId);
		Map<Long, List<OrderToClientProduct>> clientProductsMap = loadClientProductsMap(Lists.newArrayList(orderId));

		List<OrderToClientProduct> clientProducts = clientProductsMap.get(orderId);
		EntityMap<ClientProduct> clientProductMap = makeClientProductMap(clientProducts);
		List<OrderClientProductRecord> clientProductRecords = Lists.newArrayList();
		for (OrderToClientProduct otcp : clientProducts) {
			clientProductRecords
					.add(makeOrderProductRecord(otcp, clientProductMap.getEntity(otcp.getClientProductId())));
		}
		return clientProductRecords;
	}

	public OrderPartRecord loadOrderPart(Long orderToPartId) {
		logger.debug("Starting loadOrderPart for orderToPartId=" + orderToPartId);
		checkNotNull(orderToPartId);

		OrderToPart otp = orderToPartDao.findById(orderToPartId);
		if (otp == null) {
			logger.error("Unable to find the order to part model for orderToPartId=" + orderToPartId);
			return null;
		}

		Part part = partDao.findById(otp.getPartId());
		ClientProduct clientProduct = null;
		if (otp.getClientProductId() != null) {
			clientProduct = clientProductDao.findById(otp.getClientProductId());
		}

		return makeOrderPartRecord(otp, part, clientProduct);
	}

	public void refundPart(Long orderToPartId) {
		logger.debug("Starting refundPart for orderToPartId=" + orderToPartId);
		OrderToPart otp = orderToPartDao.findActiveById(orderToPartId);
		if (otp == null) {
			throw new IllegalArgumentException("The order to part could not be found.");
		}
		otp.setRefundedInd(true);
		orderToPartDao.persist(otp);
		incrementPartAmountSold(otp.getPartId(), -(int) otp.getQuantity());
	}

	/**
	 * Refunds the amount from the order to part record.
	 * 
	 * @param orderToPartId
	 *            The part to refund.
	 * @param quantity
	 *            The quantity to refund.
	 * 
	 * @throw NullPointerException if either argument is null.
	 * @throw IllegalArgumentException if the orderToPartId cannot be found.
	 * @throw IllegalStateException if the quantity attempting to refund is more
	 *        than the part quantity.
	 */
	public void refundPart(Long orderToPartId, Long quantity) {
		logger.debug("Starting refundPart for orderToPartId=" + orderToPartId + " and quantity=" + quantity);
		OrderToPart otpOriginal = orderToPartDao.findActiveById(orderToPartId);
		if (otpOriginal == null) {
			throw new IllegalArgumentException("The order to part could not be found.");
		}
		if (otpOriginal.getQuantity() < quantity) {
			throw new IllegalStateException("The order part quantity was less than the amount attempting to refund.");
		}

		long origQuantity = otpOriginal.getQuantity();
		long newQuantity = origQuantity - quantity;
		otpOriginal.setQuantity(quantity);
		otpOriginal.setRefundedInd(true);

		List<OrderToPart> otps = Lists.newArrayList();
		otps.add(otpOriginal);
		if (newQuantity > 0) {
			OrderToPart otpNew = new OrderToPart();
			otpNew.setClientProductId(otpOriginal.getClientProductId());
			otpNew.setOnSaleInd(otpOriginal.isOnSaleInd());
			otpNew.setOrderId(otpOriginal.getOrderId());
			otpNew.setPartId(otpOriginal.getPartId());
			otpNew.setPartPrice(otpOriginal.getPartPrice());
			otpNew.setQuantity(newQuantity);
			otpNew.setRefundedInd(false);
			otpNew.setTaxedAmount(otpOriginal.getTaxedAmount());
			otpNew.setTaxExemptInd(otpOriginal.isTaxExemptInd());
			otps.add(otpNew);
		}
		for (OrderToPart otp : otps) {
			orderToPartDao.persist(otp);
		}
		incrementPartAmountSold(otpOriginal.getPartId(), (int) -otpOriginal.getQuantity());
	}

	public void inactivatePart(Long orderToPartId) {
		logger.debug("Starting inactivatePart for orderToPartId=" + orderToPartId);
		orderToPartDao.inactivate(orderToPartId);
	}

	public OrderServiceRecord loadServiceRecord(Long serviceId) {
		logger.debug("Starting loadServiceRecord for serviceId=" + serviceId);
		checkNotNull(serviceId);

		Service service = serviceDao.findById(serviceId);
		if (service == null) {
			logger.error("Unable to find the order service for serviceId=" + serviceId);
			return null;
		}

		ClientProduct clientProduct = null;
		if (service.getClientProductId() != null) {
			clientProduct = clientProductDao.findById(service.getClientProductId());
		}

		return makeOrderServiceRecord(service, clientProduct);
	}

	public void refundService(Long serviceId) {
		logger.debug("Starting refundService for serviceId = " + serviceId);
		Service service = serviceDao.findById(serviceId);
		if (service == null) {
			throw new IllegalArgumentException("The service could not be found.");
		}

		service.setRefundedInd(true);
		serviceDao.persist(service);
	}

	public void inactivateService(Long serviceId) {
		logger.debug("Starting inactivateService for serviceId=" + serviceId);
		serviceDao.inactivate(serviceId);
	}

	public OrderFlatRateServiceRecord loadFlatRateServiceRecord(Long orderToFlatRateServiceId) {
		logger.debug("Starting loadFlatRateServiceRecord for orderToFlatRateServiceId=" + orderToFlatRateServiceId);
		checkNotNull(orderToFlatRateServiceId);

		OrderToFlatRateService otfrs = orderToFlatRateServiceDao.findById(orderToFlatRateServiceId);
		if (otfrs == null) {
			logger.error("Unable to find the order to flat rate service for orderToFlatRateServiceId="
					+ orderToFlatRateServiceId);
			return null;
		}

		FlatRateService frs = flatRateServiceDao.findById(otfrs.getFlatRateServiceId());

		ClientProduct clientProduct = null;
		if (otfrs.getClientProductId() != null) {
			clientProduct = clientProductDao.findById(otfrs.getClientProductId());
		}

		return makeOrderFlatRateServiceRecord(otfrs, frs, clientProduct);
	}

	public void refundFlatRateService(Long orderToFlatRateServiceId) {
		logger.debug("Starting refundFlatRateService for orderToFlatRateServiceId = " + orderToFlatRateServiceId);
		OrderToFlatRateService otfrs = orderToFlatRateServiceDao.findById(orderToFlatRateServiceId);
		if (otfrs == null) {
			throw new IllegalArgumentException("The order to flat rate service could not be found.");
		}

		otfrs.setRefundedInd(true);
		orderToFlatRateServiceDao.persist(otfrs);
		incrementFlatRateServiceAmountSold(otfrs.getFlatRateServiceId(), -1);
	}

	public void inactivateFlatRateService(Long orderToFlatRateServiceId) {
		logger.debug("Starting inactivateFlatRateService for orderToFlatRateServiceId=" + orderToFlatRateServiceId);
		orderToFlatRateServiceDao.inactivate(orderToFlatRateServiceId);
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeSetsAndCdfs = Lists.newArrayList();
		codeSetsAndCdfs.add(PRODUCT_TYPE_CDSET);
		codeSetsAndCdfs.add(MERC_MANUF_CDSET);
		codeSetsAndCdfs.add(ORDER_TYPE_CDSET);
		codeSetsAndCdfs.add(TXT_FRMT_CDSET);
		codeSetsAndCdfs.add(ORDER_STATUS_CDSET);
		return codeSetsAndCdfs;
	}

	private void incrementSoldAmounts(Long orderId) {
		List<OrderToPart> orderParts = loadOrderParts(orderId);
		for (OrderToPart otp : orderParts) {
			incrementPartAmountSold(otp.getPartId(), (int) otp.getQuantity());
		}

		List<OrderToFlatRateService> orderFlatRates = loadOrderFlatRates(orderId);
		for (OrderToFlatRateService otfrs : orderFlatRates) {
			incrementFlatRateServiceAmountSold(otfrs.getFlatRateServiceId(), 1);
		}
	}

	private void decrementSoldAmounts(Long orderId) {
		List<OrderToPart> orderParts = loadOrderParts(orderId);
		for (OrderToPart otp : orderParts) {
			incrementPartAmountSold(otp.getPartId(), -(int) otp.getQuantity());
		}

		List<OrderToFlatRateService> orderFlatRates = loadOrderFlatRates(orderId);
		for (OrderToFlatRateService otfrs : orderFlatRates) {
			incrementFlatRateServiceAmountSold(otfrs.getFlatRateServiceId(), -1);
		}
	}

	private void incrementPartAmountSold(Long partId, int amount) {
		partDao.addAmountSold(partId, amount);
	}

	private void incrementFlatRateServiceAmountSold(Long flatRateServiceId, int amount) {
		flatRateServiceDao.addAmountSold(flatRateServiceId, amount);
	}

	/**
	 * Inactivates all the components that are no longer referenced in the
	 * order.
	 * 
	 * @param record
	 *            The order to use to inactivate.
	 */
	private void inactiveComponents(OrderRecord record) {
		inactivateClientProducts(record.getOrderId(), record.getClientProducts());
		inactivateOrderParts(record.getOrderId(), record.getParts());
		inactivateServices(record.getOrderId(), record.getServices());
		inactivateFlatRateServices(record.getOrderId(), record.getFlatRates());
	}

	/**
	 * Inactivates any client products tied to the order that are not in the
	 * list of client product records.
	 * 
	 * @param orderId
	 *            The order id of the order to inactivate client products for.
	 * @param clientProducts
	 *            The client products to not inactivate.
	 */
	private void inactivateClientProducts(Long orderId, List<OrderClientProductRecord> clientProducts) {
		logger.debug("Starting inactivate client products for orderId=" + orderId);
		if (CollectionUtility.isNullOrEmpty(clientProducts)) {
			orderToClientProductDao.inactivateByOrderId(orderId);
		} else {
			List<Long> otcpIds = Lists.newArrayList();
			for (OrderClientProductRecord clientProduct : clientProducts) {
				otcpIds.add(clientProduct.getOrderToClientProductId());
			}
			orderToClientProductDao.inactivateByOrderIdAndPrimaryKeysNotIn(orderId, otcpIds);
		}
	}

	/**
	 * Inactivates any parts tied to the order that are not in the list of part
	 * records.
	 * 
	 * @param orderId
	 *            The order id of the order to inactivate parts for.
	 * @param parts
	 *            The parts to not inactivate.
	 */
	private void inactivateOrderParts(Long orderId, List<OrderPartRecord> parts) {
		logger.debug("Starting inactivate client products for orderId=" + orderId);

		List<Long> otpIds = Lists.newArrayList();
		for (OrderPartRecord otp : parts) {
			if (otp.getOrderToPartId() != null) {
				otpIds.add(otp.getOrderToPartId());
			}
		}
		if (otpIds.isEmpty()) {
			orderToPartDao.inactivateByOrderId(orderId);
		} else {
			orderToPartDao.inactivateByOrderIdAndPrimaryKeysNotIn(orderId, otpIds);
		}
	}

	/**
	 * Inactivates any services tied to the order that are not in the list of
	 * service records.
	 * 
	 * @param orderId
	 *            The order id of the order to inactivate services for.
	 * @param services
	 *            The services to not inactivate.
	 */
	private void inactivateServices(Long orderId, List<OrderServiceRecord> services) {
		logger.debug("Starting inactivate services for orderId=" + orderId);

		List<Long> serviceIds = Lists.newArrayList();
		for (OrderServiceRecord service : services) {
			if (service.getServiceId() != null) {
				serviceIds.add(service.getServiceId());
			}
		}
		if (serviceIds.isEmpty()) {
			serviceDao.inactivateByOrderId(orderId);
		} else {
			serviceDao.inactivateByOrderIdAndPrimaryKeyNotIn(orderId, serviceIds);
		}
	}

	/**
	 * Inactivates any flat rate services tied to the order that are not in the
	 * list of flat rate service records.
	 * 
	 * @param orderId
	 *            The order id of the order to inactivate flat rate services
	 *            for.
	 * @param flatRates
	 *            The flat rates to not inactivate.
	 */
	private void inactivateFlatRateServices(Long orderId, List<OrderFlatRateServiceRecord> flatRates) {
		logger.debug("Starting inactivate client products for orderId=" + orderId);
		if (CollectionUtility.isNullOrEmpty(flatRates)) {
			orderToFlatRateServiceDao.inactivateByOrderId(orderId);
		} else {
			List<Long> serviceIds = Lists.newArrayList();
			for (OrderFlatRateServiceRecord serviceRecord : flatRates) {
				serviceIds.add(serviceRecord.getOrderToFrsId());
			}
			orderToFlatRateServiceDao.inactivateByOrderIdAndPrimaryKeyNotIn(orderId, serviceIds);
		}
	}

	/**
	 * Persists any order to client products on the record into the datastore.
	 * 
	 * @param orderId
	 *            The order id of the order to persist client products for.
	 * @param clientProducts
	 *            The client products to persist.
	 */
	private void persistClientProducts(Long orderId, List<OrderClientProductRecord> clientProducts) {

		for (OrderClientProductRecord clientProductRecord : clientProducts) {
			OrderToClientProduct otcp = new OrderToClientProduct();
			otcp.setClientProductId(clientProductRecord.getClientProductId());
			otcp.setOrderId(orderId);
			otcp.setOrderToClientProductId(clientProductRecord.getOrderToClientProductId());
			orderToClientProductDao.persist(otcp);
		}
	}

	/**
	 * Persists any order to flat rate services on the record into the
	 * datastore.
	 * 
	 * @param orderId
	 *            The order id to persist flat rates for.
	 * @param flatRates
	 *            The flat rate services to persist.
	 */
	private void persistFlatRates(Long orderId, List<OrderFlatRateServiceRecord> flatRates) {
		for (OrderFlatRateServiceRecord flatRateRecord : flatRates) {
			OrderToFlatRateService otfrs = new OrderToFlatRateService();
			otfrs.setClientProductId(flatRateRecord.getClientProductId());
			otfrs.setFlatRateServiceId(flatRateRecord.getFlatRateServiceId());
			otfrs.setOnSaleInd(flatRateRecord.isOnSale());
			otfrs.setOrderId(orderId);
			otfrs.setOrderToFrsId(flatRateRecord.getOrderToFrsId());
			otfrs.setRefundedInd(false);
			otfrs.setServicePrice(flatRateRecord.getServicePrice());
			orderToFlatRateServiceDao.persist(otfrs);
		}
	}

	private void persistParts(Long orderId, List<OrderPartRecord> parts) {
		for (OrderPartRecord record : parts) {
			OrderToPart otp = fillOrderPart(record, orderId);
			orderToPartDao.persist(otp);
		}
	}

	private OrderToPart fillOrderPart(OrderPartRecord record, Long orderId) {
		OrderToPart otp = new OrderToPart();
		otp.setClientProductId(record.getClientProductId());
		otp.setOnSaleInd(record.isOnSale());
		otp.setOrderId(orderId);
		otp.setOrderToPartId(record.getOrderToPartId());
		otp.setPartId(record.getPartId());
		otp.setPartPrice(record.getPartPrice());
		otp.setQuantity(record.getQuantity());
		otp.setRefundedInd(record.isRefunded());
		otp.setTaxExemptInd(record.isTaxExempt());
		otp.setTaxedAmount(record.getTaxAmount());
		return otp;
	}

	/**
	 * Persists any order services on the record into the datastore.
	 * 
	 * @param orderId
	 *            The order id of the order to persist services for.
	 * @param services
	 *            The services to persist.
	 */
	private void persistServices(Long orderId, List<OrderServiceRecord> services) {
		for (OrderServiceRecord record : services) {
			Long longTextId = null;
			if (record.getServiceText() != null) {
				longTextId = persistLongText(record.getServiceText());
			}

			Service service = new Service();
			service.setServiceName(record.getServiceName());
			service.setClientProductId(record.getClientProductId());
			service.setHoursQnt(record.getHoursQnt());
			service.setOrderId(orderId);
			service.setPricePerHour(record.getPricePerHour());
			service.setRefundedInd(false);
			service.setServiceId(record.getServiceId());
			service.setServiceLongTextId(longTextId);
			serviceDao.persist(service);
		}
	}

	/**
	 * Creates a Order model and fills out any of the corresponding fields from
	 * the record. If the order id is specified on the order record then will be
	 * considered an existing order, if set to null or not set will be
	 * considered a new order.
	 * 
	 * @param record
	 *            The order record to use.
	 * @return The resulting Order model.
	 */
	private Order makeNewOrder(OrderRecord record) {
		Order order = new Order();
		order.setOrderId(record.getOrderId());
		order.setClientId(record.getClientId());
		order.setOrderStatusCd(getOrderStatusCd(record.getOrderStatus()));
		order.setOrderTypeCd(getOrderTypeCd(record.getOrderType()));
		return order;
	}

	/**
	 * Creates a new service record and populates it with the corresponding
	 * fields from the supplied parameters.
	 * 
	 * @param s
	 *            The service to use to populate the record with.
	 * @param longText
	 *            The long text of the service's text description.
	 * @return The populated record.
	 */
	private OrderServiceRecord makeOrderServiceRecord(Service s, ClientProduct clientProduct) {
		OrderServiceRecord record = new OrderServiceRecord();
		record.setServiceId(s.getServiceId());
		record.setServiceName(s.getServiceName());
		record.setClientProductId(s.getClientProductId());
		record.setHoursQnt(s.getHoursQnt());
		record.setPricePerHour(s.getPricePerHour());
		record.setRefunded(s.isRefundedInd());

		if (s.getServiceLongTextId() != null) {
			record.setServiceText(getLongTextRecord(s.getServiceLongTextId()));
		}

		if (clientProduct == null) {
			logger.error("Missing client product for order to service id " + s.getServiceId());
		} else {
			record.setClientProductManufacturer(clientProduct.getProductManuf());
			record.setClientProductModel(clientProduct.getProductModel());
			record.setClientProductSerialNumber(clientProduct.getProductSerialNum());
		}
		return record;
	}

	/**
	 * Creates a new flat rate service record and populates it with the
	 * corresponding fields from the supplied parameters.
	 * 
	 * @param otfrs
	 *            The order to flat rate service to use to populate the record
	 *            with.
	 * @param longText
	 *            The corresponding flat rate service.
	 * @return The populated record.
	 */
	private OrderFlatRateServiceRecord makeOrderFlatRateServiceRecord(OrderToFlatRateService otfrs, FlatRateService frs,
			ClientProduct clientProduct) {
		OrderFlatRateServiceRecord record = new OrderFlatRateServiceRecord();
		record.setOrderToFrsId(otfrs.getOrderToFrsId());
		record.setClientProductId(otfrs.getClientProductId());
		record.setServicePrice(otfrs.getServicePrice());
		record.setOnSale(otfrs.isOnSaleInd());
		record.setRefunded(otfrs.isRefundedInd());

		if (frs == null) {
			logger.error("Missing flat rate for order to flat rate service id " + otfrs.getOrderToFrsId());
		} else {
			record.setServiceName(frs.getServiceName());
			record.setFlatRateServiceId(frs.getFlatRateServiceId());
		}
		if (clientProduct == null) {
			logger.error("Missing client product for order to flat rate id " + otfrs.getOrderToFrsId());
		} else {
			record.setClientProductManufacturer(clientProduct.getProductManuf());
			record.setClientProductModel(clientProduct.getProductModel());
			record.setClientProductSerialNumber(clientProduct.getProductSerialNum());
		}
		return record;
	}

	/**
	 * Creates a new order part record and populates it with the corresponding
	 * fields from the supplied parameters.
	 * 
	 * @param otp
	 *            The order to part model.
	 * @param part
	 *            The corresponding part model.
	 * @return The populated record.
	 */
	private OrderPartRecord makeOrderPartRecord(OrderToPart otp, Part part, ClientProduct clientProduct) {
		OrderPartRecord record = new OrderPartRecord();
		record.setOrderToPartId(otp.getOrderToPartId());
		record.setClientProductId(otp.getClientProductId());
		record.setPartPrice(otp.getPartPrice());
		record.setOnSale(otp.isOnSaleInd());
		record.setQuantity(otp.getQuantity());
		if (part == null) {
			logger.error("Missing part for order to part id " + otp.getOrderToPartId());
		} else {
			record.setPartId(part.getPartId());
			record.setPartName(part.getPartName());
			record.setDescription(part.getDescription());
			record.setPartNumber(part.getManufPartId());
			record.setManufacturerDisplay(getDisplay(part.getManufacturerCd()));
			record.setTaxAmount(otp.getTaxedAmount());
			record.setTaxExempt(otp.isTaxExemptInd());
			record.setRefunded(otp.isRefundedInd());
		}
		if (otp.getClientProductId() != null) {
			if (clientProduct == null) {
				logger.error("Missing client product for order to part id " + otp.getOrderToPartId());
			} else {
				record.setClientProductManufacturer(clientProduct.getProductManuf());
				record.setClientProductModel(clientProduct.getProductModel());
				record.setClientProductSerialNumber(clientProduct.getProductSerialNum());
			}
		}
		return record;
	}

	private OrderClientProductRecord makeOrderProductRecord(OrderToClientProduct otcp, ClientProduct clientProduct) {
		OrderClientProductRecord record = new OrderClientProductRecord();
		record.setOrderToClientProductId(otcp.getOrderToClientProductId());
		if (clientProduct == null) {
			logger.error("No client product found for order to client product id " + otcp.getOrderToClientProductId());
		} else {
			record.setClientProductId(clientProduct.getClientProductId());
			record.setProductManufacturer(clientProduct.getProductManuf());
			record.setProductModel(clientProduct.getProductModel());
			record.setProductType(getDisplay(clientProduct.getProductTypeCd()));
		}
		return record;
	}

	private OrderType getOrderType(Long orderTypeCd) {
		if (orderTypeCd == null) {
			return OrderType.UNKNOWN;
		}
		if (orderTypeCd.equals(getCodeValue(ORDER_TYPE_CDSET, WORK_CDF))) {
			return OrderType.WORK;
		}
		if (orderTypeCd.equals(getCodeValue(ORDER_TYPE_CDSET, MERCHANDISE_CDF))) {
			return OrderType.MERCHANDISE;
		}
		return OrderType.UNKNOWN;
	}

	private Long getOrderTypeCd(OrderType orderType) {
		if (orderType == null) {
			return 0L;
		}
		switch (orderType) {
		case WORK:
			return getCodeValue(ORDER_TYPE_CDSET, WORK_CDF);
		case MERCHANDISE:
			return getCodeValue(ORDER_TYPE_CDSET, MERCHANDISE_CDF);
		default:
			return 0L;
		}
	}

	private OrderStatus getOrderStatusEnum(Long orderStatusCd) {
		if (orderStatusCd == null) {
			return OrderStatus.UNKNOWN;
		}
		if (orderStatusCd.equals(getCodeValue(ORDER_STATUS_CDSET, CLOSED_CDF))) {
			return OrderStatus.CLOSED;
		}
		if (orderStatusCd.equals(getCodeValue(ORDER_STATUS_CDSET, INERROR_CDF))) {
			return OrderStatus.INERROR;
		}
		if (orderStatusCd.equals(getCodeValue(ORDER_STATUS_CDSET, OPEN_CDF))) {
			return OrderStatus.OPEN;
		}
		return OrderStatus.UNKNOWN;
	}

	private Long getOrderStatusCd(OrderStatus orderStatus) {
		if (orderStatus == null) {
			return 0L;
		}
		switch (orderStatus) {
		case CLOSED:
			return getCodeValue(ORDER_STATUS_CDSET, CLOSED_CDF);
		case INERROR:
			return getCodeValue(ORDER_STATUS_CDSET, INERROR_CDF);
		case OPEN:
			return getCodeValue(ORDER_STATUS_CDSET, OPEN_CDF);
		default:
			return 0L;
		}
	}
}
