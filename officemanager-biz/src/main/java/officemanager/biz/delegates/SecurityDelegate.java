package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.maps.EntityMap;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PermissionRecord;
import officemanager.biz.records.PersonnelRoleRecord;
import officemanager.biz.records.RolePermissionRecord;
import officemanager.biz.records.RoleRecord;
import officemanager.biz.records.RoleSelectionRecord;
import officemanager.biz.records.UserPermissionRecord;
import officemanager.biz.records.UserSecurityRecord;
import officemanager.data.access.PermissionDao;
import officemanager.data.access.PersonnelRoleDao;
import officemanager.data.access.RoleDao;
import officemanager.data.access.RolePermissionDao;
import officemanager.data.models.Permission;
import officemanager.data.models.PersonnelRole;
import officemanager.data.models.Role;
import officemanager.data.models.RolePermission;

public class SecurityDelegate extends Delegate {

	private static final Logger logger = LogManager.getLogger(SecurityDelegate.class);

	private final PermissionDao permissionDao;
	private final PersonnelRoleDao personnelRoleDao;
	private final RoleDao roleDao;
	private final RolePermissionDao rolePermissionDao;

	public SecurityDelegate() {
		permissionDao = new PermissionDao();
		personnelRoleDao = new PersonnelRoleDao();
		roleDao = new RoleDao();
		rolePermissionDao = new RolePermissionDao();
	}

	public List<CodeValueRecord> getPermissionCategories() {
		return getActiveCodeValueRecords(PERMISSION_CATEGORY_CDSET);
	}

	public List<RoleSelectionRecord> getActiveRoles() {
		logger.debug("Starting getActiveRoles.");
		List<Role> roles = roleDao.findActive();
		List<RoleSelectionRecord> records = Lists.newArrayList();
		for (Role role : roles) {
			RoleSelectionRecord record = new RoleSelectionRecord();
			record.setRoleId(role.getRoleId());
			record.setRoleName(role.getName());
			records.add(record);
		}
		return records;
	}

	public List<PersonnelRoleRecord> getActivePersonnelRoles(Long personnelId) {
		logger.debug("Starting getActivePersonnelRoles for personnelId=" + personnelId);

		List<PersonnelRole> activePersonnelRoles = personnelRoleDao.getActiveByPersonnelId(personnelId, new Date());
		Set<Long> roleIds = Sets.newHashSet();
		for (PersonnelRole personnelRole : activePersonnelRoles) {
			roleIds.add(personnelRole.getRoleId());
		}
		EntityMap<Role> roleMap = EntityMap.newEntityMap(roleDao.findByIdList(roleIds));

		List<PersonnelRoleRecord> records = Lists.newArrayList();
		for (PersonnelRole personnelRole : activePersonnelRoles) {
			PersonnelRoleRecord record = new PersonnelRoleRecord();
			record.setEndEffectiveDttm(personnelRole.getEndEffectiveDttm());
			record.setPersonnelId(personnelRole.getPersonnelId());
			record.setPersonnelRoleId(personnelRole.getPersonnelRoleId());
			record.setRoleId(personnelRole.getRoleId());
			Role role = roleMap.getEntity(personnelRole.getRoleId());
			if (role != null) {
				record.setRoleName(role.getName());
			}
			records.add(record);
		}
		return records;
	}

	public void addRole(PersonnelRoleRecord record) {
		logger.debug("Starting addRole for " + record);
		throwIfInvalid(record);

		PersonnelRole pr = new PersonnelRole();
		pr.setBeginEffectiveDttm(new Date());
		pr.setEndEffectiveDttm(record.getEndEffectiveDttm());
		pr.setPersonnelId(record.getPersonnelId());
		pr.setRoleId(record.getRoleId());
		personnelRoleDao.persist(pr);
	}

	public void removeRole(PersonnelRoleRecord record) {
		logger.debug("Starting removeRole for " + record);
		if (record.getPersonnelRoleId() == null) {
			logger.debug("No primary key was specified this record is not present in the datasource.");
			return;
		}

		PersonnelRole pr = personnelRoleDao.findById(record.getPersonnelRoleId());
		if (pr == null) {
			logger.debug("No primary key was specified this record is not present in the datasource.");
			return;
		}
		pr.setEndEffectiveDttm(new Date());
		personnelRoleDao.inactivate(pr);
	}

	public List<PermissionRecord> getActivePermissions() {
		logger.debug("Starting getActivePermissions.");
		List<Permission> permissions = permissionDao.findActive();

		List<PermissionRecord> records = Lists.newArrayList();
		for (Permission permission : permissions) {
			PermissionRecord record = new PermissionRecord();
			record.setPermissionName(permission.getPermissionName());
			record.setDisplay(permission.getDisplayName());
			record.setCategory(getCodeValueRecord(permission.getCategoryCd()));
			records.add(record);
		}
		return records;
	}

	public Long persistRoleRecord(RoleRecord record) {
		logger.debug("Starting persistRole for " + record);
		throwIfInvalid(record);

		Long roleId = persistRole(record);
		List<Long> rolePermissionIds = Lists.newArrayList();
		for (RolePermissionRecord permissionRecord : record.getPermissionRecords()) {
			permissionRecord.setRoleId(roleId);
			throwIfInvalid(permissionRecord);
			rolePermissionIds.add(persistRolePermission(permissionRecord));
		}
		rolePermissionDao.inactivateByRoleIdExcept(roleId, rolePermissionIds);
		return roleId;
	}

	public RoleRecord getRole(long roleId) {
		logger.debug("Starting getRoleRecord for roleId=" + roleId);
		checkNotNull(roleId);

		Role role = roleDao.findById(roleId);
		RoleRecord record = new RoleRecord();
		record.setRoleId(role.getRoleId());
		record.setRoleName(role.getName());
		record.setPermissionRecords(getPermissionsForRole(roleId));
		return record;
	}

	public List<RolePermissionRecord> getPermissionsForRole(Long roleId) {
		logger.debug("Starting getPermissionsForRole roleId=" + roleId);
		checkNotNull(roleId);

		List<RolePermission> rolePermissions = rolePermissionDao.findActiveByRoleId(roleId);

		List<String> permissionNames = Lists.newArrayList();
		for (RolePermission rp : rolePermissions) {
			permissionNames.add(rp.getPermissionName());
		}
		EntityMap<Permission> permissionMap = EntityMap.newEntityMap(permissionDao.findByIdList(permissionNames));

		List<RolePermissionRecord> records = Lists.newArrayList();
		for (RolePermission rp : rolePermissions) {
			Permission p = permissionMap.getEntity(rp.getPermissionName());

			RolePermissionRecord record = new RolePermissionRecord();
			record.setPermissionName(rp.getPermissionName());
			record.setRoleId(rp.getRoleId());
			record.setRolePermissionId(rp.getRolePermissionId());
			if (p != null) {
				record.setPermissionDisplay(p.getDisplayName());
			}
			records.add(record);
		}
		return records;
	}

	public UserSecurityRecord getPermissionsForPersonnel(Long personnelId) {
		logger.debug("Starting getPermissionsForPersonnel personnelId=" + personnelId);
		checkNotNull(personnelId);

		List<PersonnelRole> activePersonnelRoles = personnelRoleDao.getActiveByPersonnelId(personnelId, new Date());
		if (activePersonnelRoles.isEmpty()) {
			logger.debug("No active roles were found for the personnel.");
			return new UserSecurityRecord(personnelId);
		}

		Set<Long> roleIds = Sets.newHashSet();
		Map<Long, Date> roleToExpireDateMap = Maps.newHashMap();
		for (PersonnelRole pr : activePersonnelRoles) {
			roleIds.add(pr.getRoleId());
			roleToExpireDateMap.put(pr.getRoleId(), pr.getEndEffectiveDttm());
		}

		List<RolePermission> activeRolePermissions = rolePermissionDao.findActiveByRoleIds(roleIds);
		Map<String, List<Long>> permissionIdToRoleIdsMap = Maps.newHashMap();
		Set<String> permissionNames = Sets.newHashSet();
		for (RolePermission rp : activeRolePermissions) {
			permissionNames.add(rp.getPermissionName());
			if (!permissionIdToRoleIdsMap.containsKey(rp.getPermissionName())) {
				permissionIdToRoleIdsMap.put(rp.getPermissionName(), Lists.newArrayList());
			}
			permissionIdToRoleIdsMap.get(rp.getPermissionName()).add(rp.getRoleId());
		}

		List<Permission> activePermissions = permissionDao.findActiveByIdList(permissionNames);
		UserSecurityRecord permissionRecord = new UserSecurityRecord(personnelId);
		for (Permission p : activePermissions) {
			Date expireDate = new Date();
			List<Long> permissionRoleIds = permissionIdToRoleIdsMap.get(p.getPermissionName());
			for (Long roleId : permissionRoleIds) {
				Date roleExpireDate = roleToExpireDateMap.get(roleId);
				if (roleExpireDate == null) {
					expireDate = null;
					logger.debug("Found an unexpiring role for permission '" + p.getPermissionName() + "'.");
					break;
				}

				if (roleExpireDate.after(expireDate)) {
					expireDate = roleExpireDate;
				}
			}

			UserPermissionRecord record = new UserPermissionRecord(p.getPermissionName(), expireDate);
			permissionRecord.addPermission(record);
		}
		return permissionRecord;
	}

	@Override
	protected List<Long> warmCache() {
		return Lists.newArrayList(PERMISSION_CATEGORY_CDSET);
	}

	private Long persistRole(RoleRecord record) {
		logger.debug("Starting persistRole for " + record);
		Role role;
		if (record.getRoleId() == null) {
			logger.debug("Inserting a new role.");
			role = new Role();
		} else {
			logger.debug("Existing role found attempting to update.");
			role = roleDao.findById(record.getRoleId());
			if (role == null) {
				logger.warn("Could not find role with role id=" + record.getRoleId() + " in database inserting new.");
				role = new Role();
			}
		}
		role.setName(record.getRoleName());
		roleDao.persist(role);
		return role.getRoleId();
	}

	private Long persistRolePermission(RolePermissionRecord record) {
		logger.debug("Starting persistRolePermission for " + record);
		RolePermission rp;
		if (record.getRolePermissionId() == null) {
			logger.debug("Inserting a new role permission.");
			rp = new RolePermission();
		} else {
			logger.debug("Existing role permission attempting to update.");
			rp = rolePermissionDao.findById(record.getRolePermissionId());
			if (rp == null) {
				logger.warn("Unable to find existing role permission, inserting a new row.");
				rp = new RolePermission();
			}
		}
		rp.setPermissionName(record.getPermissionName());
		rp.setRoleId(record.getRoleId());
		rolePermissionDao.persist(rp);
		return rp.getRolePermissionId();
	}
}
