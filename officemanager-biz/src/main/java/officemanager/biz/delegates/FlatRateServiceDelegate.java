package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.EntityHelper;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.FlatRateSearchRecord;
import officemanager.biz.records.FlatRateServiceRecord;
import officemanager.biz.records.LongTextRecord;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.searchingcriteria.FlatRateSearchCriteria;
import officemanager.data.access.FlatRateServiceDao;
import officemanager.data.models.FlatRateService;
import officemanager.utility.CollectionUtility;

public class FlatRateServiceDelegate extends SellableDelegate {
	private static final Logger logger = LogManager.getLogger(FlatRateServiceDelegate.class);

	private final FlatRateServiceDao flatRateServiceDao;

	public FlatRateServiceDelegate() {
		flatRateServiceDao = new FlatRateServiceDao();
	}

	public List<CodeValueRecord> getProductTypes() {
		return getActiveCodeValueRecords(MERC_PRODUCT_TYPE_CDSET);
	}

	public List<CodeValueRecord> getFlatRateServiceCategories() {
		return getActiveCodeValueRecords(FLTRT_SERV_CATEGORIES_CDSET);
	}

	public FlatRateServiceRecord getFlatRateService(Long flatRateServiceId) {
		logger.debug("Starting getFlatRateService for flatRateServiceId=" + flatRateServiceId);
		checkNotNull(flatRateServiceId);
		FlatRateService flatRateService = flatRateServiceDao.findById(flatRateServiceId);
		if (flatRateService == null) {
			logger.error("No flat rate service was found for the id=" + flatRateServiceId + " specified.");
			return null;
		}

		LongTextRecord longText = null;
		if (flatRateService.getDescriptionLongTextId() != null) {
			longText = getLongTextRecord(flatRateService.getDescriptionLongTextId());
		}

		return makeFlatRateServiceRecord(flatRateService, longText);
	}

	public Long persistFlatRateService(FlatRateServiceRecord record) {
		logger.debug("Persisting " + record);
		checkNotNull(record);
		record.validateRecord().throwIfInvalid();
		FlatRateService flatRateService;
		if (record.getFlatRateServiceId() == null) {
			logger.debug("Inserting a new flat rate service.");
			flatRateService = new FlatRateService();
		} else {
			logger.debug("Updating existing flat rate service.");
			flatRateService = flatRateServiceDao.findById(record.getFlatRateServiceId());
			if (flatRateService == null) {
				logger.error("Unable to find a flat rate service row with flat_rate_service_id="
						+ record.getFlatRateServiceId() + ". Inserting a new row instead.");
				flatRateService = new FlatRateService();
			}
		}

		Long longTextId = null;
		if (record.getServiceDescription() != null) {
			longTextId = persistLongText(record.getServiceDescription());
		}

		fillOutFlatRateService(flatRateService, record, longTextId);
		flatRateServiceDao.persist(flatRateService);
		return flatRateService.getFlatRateServiceId();
	}

	public List<FlatRateSearchRecord> searchFlatRateServices(FlatRateSearchCriteria criteria) {
		logger.debug("Starting searchFlatRateServices with criteria=" + criteria);
		if (criteria.isEmpty()) {
			logger.debug("Searching criteria is empty returning an empty result set.");
			return Lists.newArrayList();
		}

		List<FlatRateService> flatRateServices = flatRateServiceDao.findBySearchCriteria(criteria.serviceName,
				criteria.serviceCategoryCds, criteria.productTypeCds);
		if (CollectionUtility.isNullOrEmpty(flatRateServices)) {
			logger.debug("No services were found matching that searching criteria.");
			return Lists.newArrayList();
		}

		List<Long> flatServiceIds = EntityHelper.getPrimaryKeyList(Long.class, flatRateServices);
		Map<Long, List<SaleRecord>> activeSales = getActiveSalesForEntities(flatServiceIds);
		List<FlatRateSearchRecord> recordList = Lists.newArrayList();
		for (FlatRateService frs : flatRateServices) {
			List<SaleRecord> activeServiceSales = activeSales.get(frs.getFlatRateServiceId());
			if (criteria.onlyOnSale && CollectionUtility.isNullOrEmpty(activeServiceSales)) {
				logger.debug("Only searching for active sales and service with id=" + frs.getFlatRateServiceId()
						+ " has no sales");
				continue;
			}
			recordList.add(makeFlatRateSelectionRecord(frs, activeServiceSales));
		}
		return recordList;
	}

	public void deactiveService(long flatRateServiceId) {
		logger.debug("Starting deactiveService for flatRateServiceId=" + flatRateServiceId);
		flatRateServiceDao.inactivate(flatRateServiceId);
		deactivateSalesForItem(flatRateServiceId);
	}

	public List<SaleRecord> getActiveSales(Long flatRateServiceId) {
		logger.debug("Starting getActiveSales for flatRateServiceId=" + flatRateServiceId);
		return getActiveSaleRecords(flatRateServiceId);
	}

	@Override
	protected String getParentEntityName() {
		return "FLAT_RATE_SERVICE";
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeValuesNeeded = Lists.newArrayList();
		codeValuesNeeded.add(MERC_PRODUCT_TYPE_CDSET);
		codeValuesNeeded.add(FLTRT_SERV_CATEGORIES_CDSET);
		return codeValuesNeeded;
	}

	private FlatRateServiceRecord makeFlatRateServiceRecord(FlatRateService frs, LongTextRecord longText) {
		FlatRateServiceRecord record = new FlatRateServiceRecord(frs.getFlatRateServiceId());
		record.setAmountSold(frs.getAmountSold());
		record.setBarcode(frs.getBarcode());
		record.setBasePrice(frs.getBasePrice());
		record.setCategory(getCodeValueRecord(frs.getCategoryTypeCd()));
		record.setProductType(getCodeValueRecord(frs.getProductTypeCd()));
		record.setServiceName(frs.getServiceName());
		record.setServiceDescription(longText);
		return record;
	}

	private void fillOutFlatRateService(FlatRateService flatRateService, FlatRateServiceRecord record,
			Long longTextId) {
		flatRateService.setAmountSold((short) record.getAmountSold());
		flatRateService.setBarcode(record.getBarcode());
		flatRateService.setBasePrice(record.getBasePrice());
		flatRateService.setCategoryTypeCd(record.getCategory().getCodeValue());
		flatRateService.setDescriptionLongTextId(longTextId);
		flatRateService.setFlatRateServiceId(record.getFlatRateServiceId());
		flatRateService.setProductTypeCd(record.getProductType().getCodeValue());
		flatRateService.setServiceName(record.getServiceName());
	}

	private FlatRateSearchRecord makeFlatRateSelectionRecord(FlatRateService frs, List<SaleRecord> saleList) {
		FlatRateSearchRecord record = new FlatRateSearchRecord(frs.getFlatRateServiceId());
		record.setProductType(getDisplay(frs.getProductTypeCd()));
		record.setCategory(getDisplay(frs.getCategoryTypeCd()));
		record.setServiceName(frs.getServiceName());
		record.setCurrentPrice(getPrice(frs.getBasePrice(), saleList));
		record.setBasePrice(frs.getBasePrice());
		record.setOnSale(!CollectionUtility.isNullOrEmpty(saleList));
		record.setAmountSold(frs.getAmountSold());
		return record;
	}
}
