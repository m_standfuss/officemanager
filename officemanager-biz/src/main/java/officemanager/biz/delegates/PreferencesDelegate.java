package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.jboss.logging.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.records.ApplicationPreferencesRecord;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PreferenceRecord;
import officemanager.biz.records.PreferenceRecord.PrefType;
import officemanager.data.access.PreferenceDao;
import officemanager.data.models.Preference;

public class PreferencesDelegate extends Delegate {

	private static final Logger logger = Logger.getLogger(PreferencesDelegate.class);

	private static final String MEANING_HOURLY_RATE = "HOURLY_RATE";
	private static final String MEANING_HOURLY_RATE_DISCNT = "HOURLY_RATE_DISCOUNT";
	private static final String MEANING_TAX_RATE = "TAX_RATE";

	private static final String MEANING_CMP_ADDRESS = "COMPANY_ADDRESS";
	private static final String MEANING_CMP_PHONE = "COMPANY_PHONE";
	private static final String MEANING_CMP_WEBSITE = "COMPANY_WEBSITE";

	private static final String MEANING_CLAIM_TICKET_CMMT_TYPES = "CLM_TICKT_CMMT_TYPES";
	private static final String MEANING_SHOP_TICKET_CMMT_TYPES = "SHP_TICKT_CMMT_TYPES";
	private static final String MEANING_RECIEPT_CMMT_TYPES = "RECIEPT_CMMT_TYPES";

	private final PreferenceDao preferenceDao;

	public PreferencesDelegate() {
		preferenceDao = new PreferenceDao();
	}

	@Override
	public CodeValueRecord getCodeValueRecord(Long codeValue) {
		return super.getCodeValueRecord(codeValue);
	}

	public ApplicationPreferencesRecord getApplicationPreferences() {
		logger.debug("Starting getApplicationPreferences.");
		List<Preference> preferences = preferenceDao.findActiveByMeanings(getApplicationPreferenceMeanings());
		if (preferences.isEmpty()) {
			logger.debug("No preferences found.");
		}

		ApplicationPreferencesRecord record = new ApplicationPreferencesRecord();
		for (Preference pref : preferences) {
			PreferenceRecord prefRecord = makePreferenceRecord(pref);
			switch (pref.getPrefMeaning()) {
			case MEANING_HOURLY_RATE:
				prefRecord.setPrefType(PrefType.HOURLY_RATE);
				record.setHourlyRate(prefRecord);
				break;
			case MEANING_HOURLY_RATE_DISCNT:
				prefRecord.setPrefType(PrefType.HOURLY_RATE_DISCOUNT);
				record.setHourlyRateDiscounted(prefRecord);
				break;
			case MEANING_TAX_RATE:
				prefRecord.setPrefType(PrefType.TAX_RATE);
				record.setTaxRate(prefRecord);
				break;

			case MEANING_CMP_ADDRESS:
				prefRecord.setPrefType(PrefType.COMPANY_ADDRESS);
				record.setCompanyAddress(prefRecord);
				break;
			case MEANING_CMP_PHONE:
				prefRecord.setPrefType(PrefType.COMPANY_PHONE);
				record.setCompanyPhone(prefRecord);
				break;
			case MEANING_CMP_WEBSITE:
				prefRecord.setPrefType(PrefType.COMPANY_WEBSITE);
				record.setCompanyWebsite(prefRecord);
				break;

			case MEANING_CLAIM_TICKET_CMMT_TYPES:
				prefRecord.setPrefType(PrefType.CLAIM_TIX_CMMT_TYPE);
				if (record.getClaimTicketCommentTypes() == null) {
					record.setClaimTicketCommentTypes(Lists.newArrayList(prefRecord));
				} else {
					record.getClaimTicketCommentTypes().add(prefRecord);
				}
				break;
			case MEANING_SHOP_TICKET_CMMT_TYPES:
				prefRecord.setPrefType(PrefType.SHOP_TIX_CMMT_TYPE);
				if (record.getShopTicketCommentTypes() == null) {
					record.setShopTicketCommentTypes(Lists.newArrayList(prefRecord));
				} else {
					record.getShopTicketCommentTypes().add(prefRecord);
				}
				break;
			case MEANING_RECIEPT_CMMT_TYPES:
				prefRecord.setPrefType(PrefType.RECIEPT_CMMT_TYPE);
				if (record.getOrderRecieptCommentTypes() == null) {
					record.setOrderRecieptCommentTypes(Lists.newArrayList(prefRecord));
				} else {
					record.getOrderRecieptCommentTypes().add(prefRecord);
				}
				break;
			default:
				logger.error("Unexpected preference meaning found '" + pref.getPrefMeaning() + "'.");
			}
		}
		return record;
	}

	public void clearPreferences(PrefType prefType) {
		logger.debug("Starting clearPreferences for prefType = " + prefType);
		checkNotNull(prefType);
		String meaning = getPrefMeaning(prefType);
		preferenceDao.inactivateByMeaning(meaning);
	}

	public Long insertOrUpdatePreferenceRecord(PreferenceRecord record) {
		logger.debug("Starting insertOrUpdatePreferenceRecord for " + record);
		throwIfInvalid(record);
		Preference preference = makePreference(record);
		preferenceDao.persist(preference);
		return preference.getPreferenceId();
	}

	@Override
	protected List<Long> warmCache() {
		return Lists.newArrayList(ORDER_CMMT_TYPE_CDSET);
	}

	private List<String> getApplicationPreferenceMeanings() {
		return Lists.newArrayList(MEANING_HOURLY_RATE, MEANING_HOURLY_RATE_DISCNT, MEANING_CMP_ADDRESS,
				MEANING_CMP_PHONE, MEANING_CMP_WEBSITE, MEANING_CLAIM_TICKET_CMMT_TYPES, MEANING_SHOP_TICKET_CMMT_TYPES,
				MEANING_RECIEPT_CMMT_TYPES, MEANING_TAX_RATE);
	}

	private PreferenceRecord makePreferenceRecord(Preference pref) {
		PreferenceRecord record = new PreferenceRecord();
		record.setPreferenceId(pref.getPreferenceId());
		record.setPrefType(getPrefType(pref.getPrefMeaning()));
		record.setPrefPrsnlId(pref.getPrefPersonnelId());
		record.setPrefValueBoolean(pref.getPrefValueBool());
		record.setPrefValueDecimal(pref.getPrefValueDecimal());
		record.setPrefValueNumber(pref.getPrefValueInt());
		record.setPrefValueText(pref.getPrefValueText());
		return record;
	}

	private Preference makePreference(PreferenceRecord record) {
		Preference preference = new Preference();
		preference.setPreferenceId(record.getPreferenceId());
		preference.setPrefMeaning(getPrefMeaning(record.getPrefType()));
		preference.setPrefPersonnelId(record.getPrefPrsnlId());
		preference.setPrefValueBool(record.isPrefValueBoolean());
		preference.setPrefValueDecimal(record.getPrefValueDecimal());
		preference.setPrefValueInt(record.getPrefValueNumber());
		preference.setPrefValueText(record.getPrefValueText());
		return preference;
	}

	private PrefType getPrefType(String prefMeaning) {
		switch (prefMeaning) {
		case MEANING_CLAIM_TICKET_CMMT_TYPES:
			return PrefType.CLAIM_TIX_CMMT_TYPE;
		case MEANING_CMP_ADDRESS:
			return PrefType.COMPANY_ADDRESS;
		case MEANING_CMP_PHONE:
			return PrefType.COMPANY_PHONE;
		case MEANING_CMP_WEBSITE:
			return PrefType.COMPANY_WEBSITE;
		case MEANING_HOURLY_RATE:
			return PrefType.HOURLY_RATE;
		case MEANING_HOURLY_RATE_DISCNT:
			return PrefType.HOURLY_RATE_DISCOUNT;
		case MEANING_RECIEPT_CMMT_TYPES:
			return PrefType.RECIEPT_CMMT_TYPE;
		case MEANING_SHOP_TICKET_CMMT_TYPES:
			return PrefType.SHOP_TIX_CMMT_TYPE;
		case MEANING_TAX_RATE:
			return PrefType.TAX_RATE;
		default:
			logger.error("Unidentified preference meaning '" + prefMeaning + "' returning a null pref type.");
			return null;
		}
	}

	private String getPrefMeaning(PrefType prefType) {
		switch (prefType) {
		case CLAIM_TIX_CMMT_TYPE:
			return MEANING_CLAIM_TICKET_CMMT_TYPES;
		case COMPANY_ADDRESS:
			return MEANING_CMP_ADDRESS;
		case COMPANY_PHONE:
			return MEANING_CMP_PHONE;
		case COMPANY_WEBSITE:
			return MEANING_CMP_WEBSITE;
		case HOURLY_RATE:
			return MEANING_HOURLY_RATE;
		case HOURLY_RATE_DISCOUNT:
			return MEANING_HOURLY_RATE_DISCNT;
		case RECIEPT_CMMT_TYPE:
			return MEANING_RECIEPT_CMMT_TYPES;
		case SHOP_TIX_CMMT_TYPE:
			return MEANING_SHOP_TICKET_CMMT_TYPES;
		case TAX_RATE:
			return MEANING_TAX_RATE;
		default:
			logger.error("Unexpected PrefType '" + prefType.name() + "' returning null preference meaning.");
			return null;
		}
	}
}