package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.LocationRecord;
import officemanager.biz.records.LocationType;
import officemanager.biz.searchingcriteria.LocationSearchCriteria;
import officemanager.data.access.LocationDao;
import officemanager.data.access.PartDao;
import officemanager.data.models.Location;

public class LocationDelegate extends Delegate {

	private static final Logger logger = LogManager.getLogger(LocationDelegate.class);

	private static final String CDF_INV_LOC = "INVLOC";
	private static final String CDF_BUILDING = "BUILDING";
	private static final String CDF_ROOM = "ROOM";

	private final LocationDao locationDao;
	private final PartDao partDao;

	public LocationDelegate() {
		locationDao = new LocationDao();
		partDao = new PartDao();
	}

	@Override
	protected List<Long> warmCache() {
		return Lists.newArrayList(LOCATION_TYPE_CDSET);
	}

	public List<CodeValueRecord> getLocationTypes() {
		return getActiveCodeValueRecords(LOCATION_TYPE_CDSET);
	}

	public LocationType getLocationType(Long codeValue) {
		checkNotNull(codeValue);
		if (codeValue.equals(getCodeValue(LOCATION_TYPE_CDSET, CDF_BUILDING))) {
			return LocationType.BUILDING;
		}
		if (codeValue.equals(getCodeValue(LOCATION_TYPE_CDSET, CDF_INV_LOC))) {
			return LocationType.INVENTORY_LOC;
		}
		if (codeValue.equals(getCodeValue(LOCATION_TYPE_CDSET, CDF_ROOM))) {
			return LocationType.ROOM;
		}
		logger.error("Unrecognized location type cd '" + codeValue + "'.");
		return null;
	}

	/**
	 * Gets the full abbreviated location display for this inventory location.
	 * Example 'PRNT1-PRNT2-LOCA'
	 * 
	 * @param locationId
	 *            The location to get a display for.
	 * @return The fullly qualified abbreviated location display.
	 */
	public String getFullLocationDisplay(Long locationId) {
		logger.debug("Starting getLocationDisplay for locationId=" + locationId);

		Location location = locationDao.findById(locationId);
		if (location == null) {
			logger.error("No location could be found for id = " + locationId);
			return null;
		}

		StringBuilder sb = new StringBuilder(location.getAbbrDisplay());

		Long parentLocationId = location.getParentLocationId();
		while (parentLocationId != null) {
			Location parentLocation = locationDao.findById(parentLocationId);
			if (parentLocation == null) {
				logger.error("Unable to find parent location with id=" + parentLocationId);
				break;
			}
			sb.insert(0, parentLocation.getAbbrDisplay() + '-');
			parentLocationId = parentLocation.getParentLocationId();
		}
		return sb.toString();
	}

	public List<LocationRecord> getRootLocations() {
		logger.debug("Starting getRootLocations.");

		List<Location> rootLocations = locationDao.findActiveRootLocations();
		List<LocationRecord> recordList = Lists.newArrayList();
		for (Location location : rootLocations) {
			recordList.add(makeLocationRecord(location));
		}
		return recordList;
	}

	public List<LocationRecord> getChildrenLocations(Long parentLocationId) {
		logger.debug("Starting getChildrenLocations for parentLocationId = " + parentLocationId);
		checkNotNull(parentLocationId);
		return getChildrenLocations(Lists.newArrayList(parentLocationId));
	}

	public List<LocationRecord> getChildrenLocations(List<Long> parentLocationIds) {
		logger.debug("Starting getChildrenLocations for parentLocationIds = " + parentLocationIds);
		checkNotNull(parentLocationIds);
		checkArgument(!parentLocationIds.contains(null));

		List<Location> childrenLocation = locationDao.findActiveChildrenLocations(parentLocationIds);
		List<LocationRecord> recordList = Lists.newArrayList();
		for (Location location : childrenLocation) {
			recordList.add(makeLocationRecord(location));
		}
		return recordList;
	}

	/**
	 * Gets a location record based on the id provided.
	 * 
	 * @param locationId
	 *            The identifier of the location.
	 * @return The location record, null if the location could not be found.
	 */
	public LocationRecord getLocationRecord(Long locationId) {
		logger.debug("Starting getLocationRecord for locationId = " + locationId);
		checkNotNull(locationId);

		Location location = locationDao.findById(locationId);
		if (location == null) {
			logger.error("Unable to find location with identifier " + locationId);
			return null;
		}

		return makeLocationRecord(location);
	}

	public List<LocationRecord> searchActiveLocations(LocationSearchCriteria searchCriteria) {
		logger.debug("Starting searchLocations for searchCriteria=" + searchCriteria);
		if (searchCriteria.isEmpty()) {
			logger.debug("No searching criteria provided returning without querying.");
			return Lists.newArrayList();
		}

		List<Location> locationList = locationDao.searchLocations(searchCriteria.display, searchCriteria.abbrDisplay,
				convertToCodeValues(searchCriteria.locationTypeCds));
		List<LocationRecord> recordList = Lists.newArrayList();
		for (Location location : locationList) {
			recordList.add(makeLocationRecord(location));
		}
		return recordList;
	}

	public Long insertUpdateLocation(LocationRecord record) {
		logger.debug("Starting insertUpdateLocation for record " + record);
		checkNotNull(record);
		record.validateRecord().throwIfInvalid();

		Location location;
		if (record.getLocationId() == null) {
			logger.debug("Inserting a new location.");
			location = new Location();
		} else {
			logger.debug("Existing location found attempting to update.");
			location = locationDao.findById(record.getLocationId());
			if (location == null) {
				logger.error("Unable to find existing location with id '" + record.getLocationId()
						+ "' inserting a new one instead.");
				location = new Location();
			}
		}

		fillOutLocation(location, record);
		locationDao.persist(location);
		return location.getLocationId();
	}

	public void deleteLocation(Long locationId) {
		logger.debug("Starting deleteLocation for locationId = " + locationId);
		Location location = locationDao.findById(locationId);
		if (location == null) {
			logger.error("No location with that id could be found.");
			return;
		}
		locationDao.inactivate(location);
		locationDao.updateParentLocation(locationId, location.getParentLocationId());
		partDao.clearLocation(locationId);
	}

	private LocationRecord makeLocationRecord(Location location) {
		LocationRecord record = new LocationRecord();
		record.setLocationId(location.getLocationId());
		record.setAbbrDisplay(location.getAbbrDisplay());
		record.setDisplay(location.getDisplay());
		record.setLocationType(getCodeValueRecord(location.getLocationTypeCd()));
		record.setParentLocationId(location.getParentLocationId());
		return record;
	}

	private void fillOutLocation(Location location, LocationRecord record) {
		location.setAbbrDisplay(record.getAbbrDisplay());
		location.setDisplay(record.getDisplay());
		location.setLocationTypeCd(record.getLocationType().getCodeValue());
		location.setParentLocationId(record.getParentLocationId());
	}
}