package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.maps.EntityMap;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PersonRecord;
import officemanager.biz.records.PersonnelRecord;
import officemanager.biz.records.PersonnelSearchRecord;
import officemanager.biz.searchingcriteria.PersonnelSearchCriteria;
import officemanager.biz.searchservices.PersonSearchService;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PersonnelDao;
import officemanager.data.access.PhoneDao;
import officemanager.data.models.Person;
import officemanager.data.models.Personnel;
import officemanager.data.models.Phone;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class PersonnelDelegate extends PersonDelegate {

	private static final Logger logger = LogManager.getLogger(PersonnelDelegate.class);

	private final PersonDao personDao;
	private final PersonnelDao personnelDao;
	private final PhoneDao phoneDao;
	private final PersonSearchService searchService;

	/**
	 * Default constructor
	 */
	public PersonnelDelegate() {
		personDao = new PersonDao();
		personnelDao = new PersonnelDao();
		phoneDao = new PhoneDao();
		searchService = new PersonSearchService();
	}

	public List<CodeValueRecord> getPersonnelStatus() {
		return getActiveCodeValueRecords(PRSNL_STATUS_TYPE_CDSET);
	}

	public List<CodeValueRecord> getPersonnelTypes() {
		return getActiveCodeValueRecords(PRSNL_TYPE_CDSET);
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeValues = super.warmCache();
		codeValues.add(PRSNL_STATUS_TYPE_CDSET);
		codeValues.add(PRSNL_TYPE_CDSET);
		return codeValues;
	}

	public boolean usernameTaken(String username) {
		logger.debug("Checking username availability.");
		Personnel personnel = personnelDao.findByUsername(username);
		return personnel != null;
	}

	public boolean usernameTaken(Long personnelId, String username) {
		logger.debug("Checking if username is availabile for personnel " + personnelId);
		Personnel personnel = personnelDao.findByUsername(username);
		if (personnel == null) {
			return false;
		}
		return !personnelId.equals(personnel.getPersonnelId());
	}

	/**
	 * Gets a personnel record for the personnel id.
	 *
	 * @param personnelId
	 *            The personnel identifier.
	 * @return The personnel record corresponding to the identifier supplied.
	 * @throws NullPointerException
	 *             If personnelId is null.
	 * @throws IllegalArgumentException
	 *             If the personnel identifier does not exist.
	 */
	public PersonnelRecord getPersonnelRecord(Long personnelId) {
		logger.debug("Starting getPersonnelRecord for personnelId=" + personnelId);
		checkNotNull(personnelId);

		Personnel personnel = personnelDao.findById(personnelId);
		if (personnel == null) {
			throw new IllegalArgumentException("The personnel could not be found.");
		}

		PersonRecord personRecord = getPersonRecord(personnel.getPersonId());
		PersonnelRecord record = new PersonnelRecord(personRecord);
		record.setPersonnelId(personnel.getPersonnelId());
		record.setUsername(personnel.getUsername());
		record.setStartDttm(personnel.getStartDttm());
		record.setTerminatedDttm(personnel.getTerminateDttm());
		record.setW2OnFile(personnel.isW2OnFile());
		record.setPersonnelStatus(getCodeValueRecord(personnel.getPersonnelStatusCd()));
		record.setPersonnelType(getCodeValueRecord(personnel.getPersonnelTypeCd()));
		return record;
	}

	public List<PersonnelSearchRecord> searchPersonnel(PersonnelSearchCriteria criteria) {
		logger.debug("Starting searchPersonnel with criteria=" + criteria);
		checkNotNull(criteria);

		List<Collection<Long>> personIds = Lists.newArrayList();

		if (!criteria.isPersonSearchEmpty()) {
			personIds.add(searchService.searchForPersonIds(criteria));
		}
		if (!StringUtility.isNullOrWhiteSpace(criteria.username)
				|| !CollectionUtility.isNullOrEmpty(criteria.personnelStatusCds)
				|| !CollectionUtility.isNullOrEmpty(criteria.personnelTypeCds)) {
			List<Personnel> searchPersonnel = personnelDao.searchPersonnel(criteria.username,
					criteria.personnelStatusCds, criteria.personnelTypeCds);
			if (searchPersonnel.isEmpty()) {
				logger.debug("No personnel rows found for the given criteria.");
				return Lists.newArrayList();
			}

			Set<Long> personnelPersonIds = Sets.newHashSet();
			for (Personnel personnel : searchPersonnel) {
				personnelPersonIds.add(personnel.getPersonId());
			}
			personIds.add(personnelPersonIds);
		}

		Collection<Long> finalIds = CollectionUtility.intersectCollections(personIds);
		if (finalIds.isEmpty()) {
			logger.debug("No personnels could be found for that searching criteria.");
			return Lists.newArrayList();
		}
		return makePersonnelSearchRecords(finalIds);
	}

	public Long persistPersonnelRecord(PersonnelRecord record) {
		logger.debug("Starting persistPersonnelRecord for record=" + record);
		checkNotNull(record);

		Long personId = persistPersonRecord(record);
		return persistPersonnel(record, personId);
	}

	public void terminatePersonnel(Long personnelId) {
		logger.debug("Starting terminatePersonnel for personnelId=" + personnelId);
		checkNotNull(personnelId);

		Personnel personnel = personnelDao.findById(personnelId);
		if (personnel == null) {
			throw new IllegalArgumentException("No personnel could be found with that identifier.");
		}
		personnel.setTerminateDttm(new Date());
		personnelDao.inactivate(personnel);
	}

	private Long persistPersonnel(PersonnelRecord record, Long personId) {
		Personnel personnel = null;
		if (record.getPersonnelId() == null) {
			personnel = new Personnel();
		} else {
			personnel = personnelDao.findById(record.getPersonnelId());
			if (personnel == null) {
				logger.error("Unable to find existing personnel row with personnel id=" + record.getPersonnelId());
				personnel = new Personnel();
			}
		}
		personnel.setPersonnelId(record.getPersonnelId());
		personnel.setPersonId(personId);
		personnel.setPersonnelStatusCd(record.getPersonnelStatus().getCodeValue());
		personnel.setPersonnelTypeCd(record.getPersonnelType().getCodeValue());
		personnel.setUsername(record.getUsername());
		personnel.setStartDttm(record.getStartDttm());
		personnel.setTerminateDttm(record.getTerminatedDttm());
		personnel.setW2OnFile(record.isW2OnFile());
		personnelDao.persist(personnel);
		return personnel.getPersonnelId();
	}

	private List<PersonnelSearchRecord> makePersonnelSearchRecords(Collection<Long> personIds) {
		List<Person> personList = personDao.findActiveByIdList(personIds);
		List<Personnel> personnelList = personnelDao.findByPersonIds(personIds);
		List<Phone> primaryPhones = phoneDao.findPrimaryPhone(personIds);

		EntityMap<Person> personMap = EntityMap.newEntityMap(personList);
		Map<Long, Phone> primaryPhoneMap = Maps.newHashMap();
		for (Phone phone : primaryPhones) {
			if (primaryPhoneMap.put(phone.getPersonId(), phone) != null) {
				logger.warn("Multiple primary phone numbers found for person id = " + phone.getPersonId());
			}
		}

		List<PersonnelSearchRecord> personnelRecords = Lists.newArrayList();
		for (Personnel personnel : personnelList) {
			Person person = personMap.getEntity(personnel.getPersonId());
			Phone primaryPhone = primaryPhoneMap.get(personnel.getPersonId());

			PersonnelSearchRecord record = new PersonnelSearchRecord();
			if (person != null) {
				record.setEmail(person.getEmail());
				record.setNameFullFormatted(person.getNameFullFormatted());
			}
			if (primaryPhone != null) {
				record.setPrimaryPhone(primaryPhone.getPhoneNumber());
			}

			record.setPersonnelId(personnel.getPersonnelId());
			record.setPersonnelStatus(getCodeValueRecord(personnel.getPersonnelStatusCd()));
			record.setPersonnelType(getCodeValueRecord(personnel.getPersonnelTypeCd()));
			record.setStartDttm(personnel.getStartDttm());
			record.setW2OnFile(personnel.isW2OnFile());
			personnelRecords.add(record);
		}
		return personnelRecords;
	}
}
