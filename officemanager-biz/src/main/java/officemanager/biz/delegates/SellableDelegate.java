package officemanager.biz.delegates;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.records.SaleRecord.SaleType;
import officemanager.utility.CollectionUtility;

abstract class SellableDelegate extends Delegate {
	private static final Logger logger = LogManager.getLogger(SellableDelegate.class);

	private final SaleDelegate saleDelegate;

	protected abstract String getParentEntityName();

	public SellableDelegate() {
		saleDelegate = new SaleDelegate();
	}

	protected List<SaleRecord> getActiveSaleRecords(Long entityId) {
		return saleDelegate.getActiveSaleRecords(getParentEntityName(), entityId);
	}

	protected Map<Long, List<SaleRecord>> getActiveSalesForEntities(List<Long> entityIds) {
		return saleDelegate.getActiveSaleRecords(getParentEntityName(), entityIds);
	}

	protected void deactivateSalesForItem(long partId) {
		saleDelegate.deactivateSalesForItem(getParentEntityName(), partId);
	}

	/**
	 * Applies a list of sales to the entity's base price returning the result.
	 * 
	 * Note all flat rate discounts will be deducted first then the percentage
	 * offs, if there are multiple.
	 * 
	 * @param basePrice
	 *            The base price of the entity to apply sale prices for.
	 * @param saleList
	 *            The list of active sales for the entity.
	 * @return The resulting current price of the entity.
	 */
	protected BigDecimal getPrice(BigDecimal basePrice, List<SaleRecord> saleList) {
		if (CollectionUtility.isNullOrEmpty(saleList)) {
			logger.debug("No sales found returning the base price");
			return basePrice;
		}
		logger.debug(saleList.size() + " sale(s) found.");

		List<SaleRecord> flatRateOffSales = Lists.newArrayList();
		List<SaleRecord> percentageOffSales = Lists.newArrayList();
		for (SaleRecord sale : saleList) {
			if (sale == null) {
				logger.error("The sale or sale type cd was null unable to add it to price.");
				continue;
			}
			if (sale.getSaleType() == SaleType.FLAT_RATE_OFF) {
				flatRateOffSales.add(sale);
			} else if (sale.getSaleType() == SaleType.PERCENTAGE_OFF) {
				percentageOffSales.add(sale);
			} else {
				logger.error("Unable to match the sale type cd to a known sale type, cannot add sale to price.");
			}
		}
		return Calculator.calculateSalePrice(basePrice, flatRateOffSales, percentageOffSales);
	}
}
