package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import officemanager.biz.records.AddressRecord;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.PersonRecord;
import officemanager.biz.records.PhoneRecord;
import officemanager.biz.searchingcriteria.PersonSearchCriteria;
import officemanager.biz.searchservices.PersonSearchService;
import officemanager.data.access.AddressDao;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PhoneDao;
import officemanager.data.models.Address;
import officemanager.data.models.Person;
import officemanager.data.models.Phone;
import officemanager.utility.CollectionUtility;

public class PersonDelegate extends Delegate {

	private static final Logger logger = LogManager.getLogger(PersonDelegate.class);

	private final AddressDao addressDao;
	private final PersonDao personDao;
	private final PhoneDao phoneDao;
	private final PersonSearchService searchService;

	public PersonDelegate() {
		personDao = new PersonDao();
		addressDao = new AddressDao();
		phoneDao = new PhoneDao();
		searchService = new PersonSearchService();
	}

	public List<CodeValueRecord> getCities() {
		return getActiveCodeValueRecords(CITY_CDSET);
	}

	public List<CodeValueRecord> getAddressTypes() {
		return getActiveCodeValueRecords(ADDRESS_TYPE_CDSET);
	}

	public List<CodeValueRecord> getPhoneTypes() {
		return getActiveCodeValueRecords(PHONE_TYPE_CDSET);
	}

	public PhoneRecord getPhoneRecord(Long phoneId) {
		logger.debug("Starting getPhoneRecord for phoneId=" + phoneId);
		Phone phone = phoneDao.findById(phoneId);
		return makePhoneRecord(phone);
	}

	public Long persistPhoneRecord(PhoneRecord record) {
		logger.debug("Starting persistPhoneRecord for record=" + record);
		throwIfInvalid(record);

		Phone phone;
		if (record.getPhoneId() == null) {
			logger.debug("No phone id specified creating a new phone.");
			phone = new Phone();
		} else {
			logger.debug("Existing phone id found, attempting to update that row.");
			phone = phoneDao.findById(record.getPhoneId());
			if (phone == null) {
				throw new IllegalArgumentException("No phone was found for the given phone id.");
			}
		}
		Long phoneId = persistPhoneRecord(phone, record);
		if (record.isPrimaryPhone()) {
			phoneDao.updatePrimaryPhoneForPerson(phoneId, record.getPersonId());
		}
		return phoneId;
	}

	public void deactivatePhone(Long phoneId) {
		logger.debug("Starting deactivatePhone for phoneId = " + phoneId);
		phoneDao.inactivate(phoneId);
	}

	public AddressRecord getAddressRecord(Long addressId) {
		logger.debug("Starting getAddressRecord for addressId=" + addressId);
		Address address = addressDao.findById(addressId);
		return makeAddressRecord(address);
	}

	public Long persistAddressRecord(AddressRecord record) {
		logger.debug("Starting persistAddressRecord for record=" + record);
		throwIfInvalid(record);

		Address address;
		if (record.getAddressId() == null) {
			logger.debug("No address id found creating a new address.");
			address = new Address();
		} else {
			logger.debug("Address id specified attempting to update existing address row.");
			address = addressDao.findById(record.getAddressId());
			if (address == null) {
				throw new IllegalArgumentException("No address was found for the given address id.");
			}
		}
		Long addressId = persistAddressRecord(address, record);
		if (record.isPrimaryAddress()) {
			addressDao.updatePrimaryAddressForPerson(addressId, record.getPersonId());
		}
		return addressId;
	}

	public void deactivateAddress(Long addressId) {
		logger.debug("Starting deactivateAddress for addressId = " + addressId);
		addressDao.inactivate(addressId);
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeSetsAndCdfs = Lists.newArrayList();
		codeSetsAndCdfs.add(PHONE_TYPE_CDSET);
		codeSetsAndCdfs.add(ADDRESS_TYPE_CDSET);
		return codeSetsAndCdfs;
	}

	protected PersonRecord getPersonRecord(Long personId) {
		logger.debug("Starting getPersonRecord for personId=" + personId);
		checkNotNull(personId);

		List<PersonRecord> personRecords = makePersonRecords(Lists.newArrayList(personId));
		if (CollectionUtility.isNullOrEmpty(personRecords)) {
			logger.debug("No person was found for personId=" + personId);
			return null;
		}
		return personRecords.get(0);
	}

	/**
	 * Finds person records that meet the person searching criteria.
	 *
	 * @param criteria
	 *            The searching criteria to meet.
	 * @return The list of person records that meet that criteria.
	 */
	protected List<PersonRecord> findByPersonSearchCriteria(PersonSearchCriteria criteria) {
		logger.debug("Starting findByPersonSearchCriteria with criteria=" + criteria);
		checkNotNull(criteria);

		List<Long> personIds = searchService.searchForPersonIds(criteria);
		return makePersonRecords(personIds);
	}

	/**
	 * Gets the address records for the list of person ids.
	 *
	 * @param personIds
	 *            The person identifiers to get addresses for.
	 * @return The list of address records.
	 */
	protected List<AddressRecord> getAddressRecords(List<Long> personIds) {
		logger.debug("Starting getAddressRecords for personIds=" + personIds);
		checkNotNull(personIds);
		if (personIds.isEmpty()) {
			logger.debug("No person id's specified.");
			return Lists.newArrayList();
		}
		List<Address> addressList = addressDao.findActiveAddress(personIds);
		return makeAddressRecords(addressList);
	}

	/**
	 * Gets the address records for the list of person ids and
	 *
	 * @param personIds
	 *            The person identifiers to get primary addresses for.
	 * @return The list of only primary addresses for the persons.
	 */
	protected List<AddressRecord> getPrimaryAddressRecords(List<Long> personIds) {
		logger.debug("Starting getPrimaryAddressRecords for personIds=" + personIds);
		checkNotNull(personIds);
		if (personIds.isEmpty()) {
			logger.debug("No person id's specified.");
			return Lists.newArrayList();
		}
		List<Address> addressList = addressDao.findPrimaryAddress(personIds);
		return makeAddressRecords(addressList);
	}

	/**
	 * Gets the phone records for the list of person ids.
	 *
	 * @param personIds
	 *            The person identifiers to get phones for.
	 * @return The list of phone records.
	 */
	protected List<PhoneRecord> getPhoneRecords(List<Long> personIds) {
		logger.debug("Starting getPhoneRecords for personIds=" + personIds);
		checkNotNull(personIds);
		if (personIds.isEmpty()) {
			logger.debug("No person ids specified.");
			return Lists.newArrayList();
		}
		List<Phone> phoneList = phoneDao.findActivePhones(personIds);
		return makePhoneRecords(phoneList);
	}

	/**
	 * Gets the primary phone records for the list of person ids.
	 *
	 * @param personIds
	 *            The person identifiers to get phones for.
	 * @return The list of primary phone records.
	 */
	protected List<PhoneRecord> getPrimaryPhoneRecords(List<Long> personIds) {
		logger.debug("Starting getPrimaryPhoneRecords for personIds=" + personIds);
		checkNotNull(personIds);
		if (personIds.isEmpty()) {
			logger.debug("No person ids specified.");
			return Lists.newArrayList();
		}
		List<Phone> phoneList = phoneDao.findPrimaryPhone(personIds);
		return makePhoneRecords(phoneList);
	}

	/**
	 * Persists a Person record into the database. Along with any subsequent
	 * phone/address records that it has.
	 *
	 * @param record
	 *            The record to persist to the database.
	 * @return The person id of the person.
	 * @throws NullPointerException
	 *             If the person record is null.
	 * @throws IllegalArgumentException
	 *             If the person is invalid.
	 */
	protected Long persistPersonRecord(PersonRecord record) {
		logger.debug("Starting persistPersonRecord with record=" + record);
		checkNotNull(record);
		throwIfInvalid(record);

		Person person;
		if (record.getPersonId() == null) {
			logger.debug("No person id specified inserting a new row.");
			person = new Person();
		} else {
			logger.debug("Person id specified attempting to update existing person row.");
			person = personDao.findById(record.getPersonId());
			if (person == null) {
				throw new IllegalArgumentException("The person id could not be found.");
			}
		}

		Long personId = persistPerson(person, record);

		Long primaryAddressId = null;
		List<Long> activeAddressIds = Lists.newArrayList();
		for (AddressRecord address : record.getAddressRecords()) {
			Long addressId = persistAddressRecord(personId, address);
			activeAddressIds.add(addressId);
			if (address.isPrimaryAddress()) {
				primaryAddressId = addressId;
			}
		}
		if (primaryAddressId == null) {
			addressDao.disablePrimaryAddressForPerson(personId);
		}

		if (activeAddressIds.isEmpty()) {
			addressDao.disableAddressesForPerson(personId);
		} else {
			addressDao.disableAddressesForPerson(personId, activeAddressIds);
		}

		Long primaryPhoneId = null;
		List<Long> activePhoneIds = Lists.newArrayList();
		for (PhoneRecord phone : record.getPhoneRecords()) {
			Long phoneId = persistPhoneRecord(personId, phone);
			activePhoneIds.add(phoneId);
			if (phone.isPrimaryPhone()) {
				primaryPhoneId = phoneId;
			}
		}
		if (primaryPhoneId == null) {
			phoneDao.disablePrimaryPhone(personId);
		}

		if (activeAddressIds.isEmpty()) {
			phoneDao.disablePhonesForPerson(personId);
		} else {
			phoneDao.disablePhonesForPerson(personId, activePhoneIds);
		}

		return personId;
	}

	/**
	 * Creates a copy of the phone record passed in, then persists the record to
	 * the database. If the phone is designated as the primary phone number then
	 * all other phone numbers for the person id will be un-primaried.
	 *
	 * @param record
	 *            The phone record to persist.
	 * @return The phone id of the corresponding row updated or inserted.
	 */
	protected Long persistPhoneRecord(Long personId, PhoneRecord record) {
		logger.debug("Starting persistPhoneRecord with record=" + record);
		checkNotNull(record);
		throwIfInvalid(record);

		record = new PhoneRecord(record);
		record.setPersonId(personId);
		return persistPhoneRecord(record);
	}

	/**
	 * Creates a copy of the record passed in, sets the person id then persists
	 * the record to the database. If the address is designated as the primary
	 * address then all other addresses for the person id will be un-primaried.
	 *
	 * @param record
	 *            The address record to persist.
	 * @return The address id of the corresponding row updated or inserted.
	 */
	protected Long persistAddressRecord(Long personId, AddressRecord record) {
		logger.debug("Starting persistAddressRecord with record=" + record);
		checkNotNull(record);
		throwIfInvalid(record);

		record = new AddressRecord(record);
		record.setPersonId(personId);
		return persistAddressRecord(record);
	}

	private List<PersonRecord> makePersonRecords(List<Long> personIds) {
		List<Person> personList = personDao.findActiveByPersonIds(personIds);
		List<Phone> phoneList = phoneDao.findActivePhones(personIds);
		List<Address> addressList = addressDao.findActiveAddress(personIds);

		Map<Long, List<Phone>> phoneMap = Maps.newHashMap();
		if (!CollectionUtility.isNullOrEmpty(phoneList)) {
			for (Phone phone : phoneList) {
				if (phoneMap.get(phone.getPersonId()) == null) {
					phoneMap.put(phone.getPersonId(), Lists.newArrayList());
				}
				phoneMap.get(phone.getPersonId()).add(phone);
			}
		}

		Map<Long, List<Address>> addressMap = Maps.newHashMap();
		if (!CollectionUtility.isNullOrEmpty(addressList)) {
			for (Address address : addressList) {
				if (addressMap.get(address.getPersonId()) == null) {
					addressMap.put(address.getPersonId(), Lists.newArrayList());
				}
				addressMap.get(address.getPersonId()).add(address);
			}
		}

		List<PersonRecord> personRecords = Lists.newArrayList();
		for (Person person : personList) {

			List<Phone> phones = phoneMap.get(person.getPersonId());
			List<Address> addresses = addressMap.get(person.getPersonId());
			personRecords.add(makePersonRecord(person, phones, addresses));
		}
		return personRecords;
	}

	private PersonRecord makePersonRecord(Person person, List<Phone> phoneList, List<Address> addressList) {
		PersonRecord record = new PersonRecord(person.getPersonId());
		record.setEmail(person.getEmail());
		record.setNameFirst(person.getNameFirst());
		record.setNameFullFormatted(person.getNameFullFormatted());
		record.setNameLast(person.getNameLast());
		if (phoneList != null) {
			for (Phone phone : phoneList) {
				PhoneRecord phoneRecord = makePhoneRecord(phone);
				record.getPhoneRecords().add(phoneRecord);
			}

		}
		if (addressList != null) {
			for (Address address : addressList) {
				AddressRecord addressRecord = makeAddressRecord(address);
				record.getAddressRecords().add(addressRecord);
			}
		}
		return record;
	}

	private AddressRecord makeAddressRecord(Address address) {
		AddressRecord record = new AddressRecord(address.getAddressId());
		record.setAddressLines(Lists.newArrayList(address.getAddress1(), address.getAddress2(), address.getAddress3()));
		record.setAddressType(getCodeValueRecord(address.getAddressTypeCd()));
		record.setCity(address.getCity());
		record.setPersonId(address.getPersonId());
		record.setPrimaryAddress(address.isPrimaryAddressInd());
		record.setState(address.getState());
		record.setZip(address.getZip());
		return record;
	}

	private PhoneRecord makePhoneRecord(Phone phone) {
		PhoneRecord record = new PhoneRecord(phone.getPhoneId());
		record.setExtension(phone.getExtension());
		record.setPersonId(phone.getPersonId());
		record.setPhoneNumber(phone.getPhoneNumber());
		record.setPhoneType(getCodeValueRecord(phone.getPhoneTypeCd()));
		record.setPrimaryPhone(phone.isPrimaryPhoneInd());
		return record;
	}

	private List<AddressRecord> makeAddressRecords(List<Address> addressList) {
		List<AddressRecord> records = Lists.newArrayList();
		for (Address address : addressList) {
			AddressRecord record = makeAddressRecord(address);
			records.add(record);
		}
		return records;
	}

	private List<PhoneRecord> makePhoneRecords(List<Phone> phoneList) {
		List<PhoneRecord> records = Lists.newArrayList();
		for (Phone phone : phoneList) {
			PhoneRecord record = makePhoneRecord(phone);
			records.add(record);
		}
		return records;
	}

	private Long persistPerson(Person person, PersonRecord record) {
		person.setEmail(record.getEmail());
		person.setNameFirst(record.getNameFirst());
		person.setNameFullFormatted(record.getNameFullFormatted());
		person.setNameLast(record.getNameLast());
		personDao.persist(person);
		return person.getPersonId();
	}

	private Long persistPhoneRecord(Phone phone, PhoneRecord record) {
		phone.setExtension(record.getExtension());
		phone.setPersonId(record.getPersonId());
		phone.setPhoneId(record.getPhoneId());
		phone.setPhoneNumber(record.getPhoneNumber());
		phone.setPhoneTypeCd(record.getPhoneType().getCodeValue());
		phoneDao.persist(phone);
		return phone.getPhoneId();
	}

	private Long persistAddressRecord(Address address, AddressRecord record) {
		if (record.getAddressLines().size() > 0) {
			address.setAddress1(record.getAddressLines().get(0));
		} else {
			address.setAddress1(null);
		}

		if (record.getAddressLines().size() > 1) {
			address.setAddress2(record.getAddressLines().get(1));
		} else {
			address.setAddress2(null);
		}

		if (record.getAddressLines().size() > 2) {
			StringBuilder b = new StringBuilder();
			String SPACE = "";
			for (int i = 2; i < record.getAddressLines().size(); i++) {
				b.append(record.getAddressLines().get(i)).append(SPACE);
				SPACE = " ";
			}
			address.setAddress3(b.toString());
		} else {
			address.setAddress3(null);
		}
		address.setAddressId(record.getAddressId());
		address.setAddressTypeCd(record.getAddressType().getCodeValue());
		address.setCity(record.getCity());
		address.setPersonId(record.getPersonId());
		address.setPrimaryAddressInd(record.isPrimaryAddress());
		address.setState(record.getState());
		address.setZip(record.getZip());
		addressDao.persist(address);
		return address.getAddressId();
	}
}
