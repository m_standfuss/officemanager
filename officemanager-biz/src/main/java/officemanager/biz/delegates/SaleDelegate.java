package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.EntityHelper;
import officemanager.biz.records.SaleRecord;
import officemanager.biz.records.SaleRecord.SaleProductType;
import officemanager.biz.records.SaleRecord.SaleType;
import officemanager.biz.searchingcriteria.SaleSearchCritieria;
import officemanager.data.access.FlatRateServiceDao;
import officemanager.data.access.PartDao;
import officemanager.data.access.SaleDao;
import officemanager.data.models.FlatRateService;
import officemanager.data.models.Part;
import officemanager.data.models.Sale;
import officemanager.data.models.SaleEntityName;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;
import officemanager.utility.Tuple;

public class SaleDelegate extends Delegate {
	private static final Logger logger = LogManager.getLogger(SaleDelegate.class);

	private static final String SET_PRICE_OFF_CDF = "SET_PRICE_OFF";
	private static final String PERCENTAGE_OFF_CDF = "PERCENTAGE_OFF";

	private static final String PART_ENTITY_NAME = "PART";
	private static final String FLAT_RATE_SERVICE_ENTITY_NAME = "FLAT_RATE_SERVICE";

	private final SaleDao saleDao;
	private final PartDao partDao;
	private final FlatRateServiceDao flatRateServiceDao;

	public SaleDelegate() {
		saleDao = new SaleDao();
		partDao = new PartDao();
		flatRateServiceDao = new FlatRateServiceDao();
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeSetsAndCdfMeanings = Lists.newArrayList();
		codeSetsAndCdfMeanings.add(SALE_TYPE_CDSET);
		return codeSetsAndCdfMeanings;
	}

	public SaleRecord getSaleRecord(Long saleId) {
		logger.debug("Starting getSaleRecord for saleId=" + saleId);
		Sale sale = saleDao.findById(saleId);
		if (sale == null) {
			logger.error("Unable to find sale with specified id.");
			return null;
		}

		Map<Tuple<String, Long>, String> entityNameMap = makeEntityNameMap(Lists.newArrayList(sale));
		String entityName = entityNameMap.get(Tuple.newTuple(sale.getParentEntityName(), sale.getParentEntityId()));
		return makeSaleRecord(sale, entityName);
	}

	public String getSaleItemName(SaleProductType type, Long productId) {
		logger.debug("Starting getSaleItemName for type=" + type + " and productId=" + productId);
		checkNotNull(type, productId);

		if (type == SaleProductType.PART) {
			Part part = partDao.findById(productId);
			if (part == null) {
				logger.error("No part found for id=" + productId);
				return null;
			}
			return part.getPartName();
		} else {
			FlatRateService frs = flatRateServiceDao.findById(productId);
			if (frs == null) {
				logger.error("No flat rate service found for id=" + productId);
				return null;
			}
			return frs.getServiceName();
		}

		// for (Part p : partList) {
		// entityNameMap.put(Tuple.newTuple(PART_ENTITY_NAME, p.getPartId()),
		// p.getPartName());
		// }
		// for (FlatRateService frs : flatRateList) {
		// entityNameMap.put(Tuple.newTuple(FLAT_RATE_SERVICE_ENTITY_NAME,
		// frs.getFlatRateServiceId()),
		// frs.getServiceName());
		// }
	}

	public List<SaleRecord> searchSaleRecords(SaleSearchCritieria searchCriteria) {
		logger.debug("Starting searchSaleRecords for searchCriteria=" + searchCriteria);
		if (searchCriteria.isEmpty()) {
			logger.debug("No searching criteria provided returning without querying.");
			return Lists.newArrayList();
		}

		List<Tuple<String, List<Long>>> parentEntities = null;
		if (!StringUtility.isNullOrWhiteSpace(searchCriteria.saleItemName)) {
			parentEntities = Lists.newArrayList();
			List<Part> partList = partDao.findByPartNameLike(searchCriteria.saleItemName);
			if (!CollectionUtility.isNullOrEmpty(partList)) {
				List<Long> partIds = EntityHelper.getPrimaryKeyList(Long.class, partList);
				parentEntities.add(Tuple.newTuple(PART_ENTITY_NAME, partIds));
			}
			List<FlatRateService> flatRateServices = flatRateServiceDao
					.findByDescriptionLike(searchCriteria.saleItemName);
			if (!CollectionUtility.isNullOrEmpty(flatRateServices)) {
				List<Long> flatRateServiceIds = EntityHelper.getPrimaryKeyList(Long.class, flatRateServices);
				parentEntities.add(Tuple.newTuple(FLAT_RATE_SERVICE_ENTITY_NAME, flatRateServiceIds));
			}
			if (parentEntities.isEmpty()) {
				logger.debug("No sale items with that name were found.");
				return Lists.newArrayList();
			}
		}

		List<Long> saleTypeCds = Lists.newArrayList();
		if (searchCriteria.amountOffSale) {
			saleTypeCds.add(getCodeValue(SALE_TYPE_CDSET, SET_PRICE_OFF_CDF));
		}
		if (searchCriteria.percentageOffSale) {
			saleTypeCds.add(getCodeValue(SALE_TYPE_CDSET, PERCENTAGE_OFF_CDF));
		}
		List<Sale> activeSales = saleDao.searchActiveSales(searchCriteria.saleName, saleTypeCds, parentEntities,
				searchCriteria.begActiveDttm, searchCriteria.endActiveDttm);
		if (activeSales.isEmpty()) {
			logger.debug("No sales were found.");
			return Lists.newArrayList();
		}
		Map<Tuple<String, Long>, String> entityNameMap = makeEntityNameMap(activeSales);
		List<SaleRecord> saleRecords = Lists.newArrayList();
		for (Sale sale : activeSales) {
			String entityName = entityNameMap.get(Tuple.newTuple(sale.getParentEntityName(), sale.getParentEntityId()));
			saleRecords.add(makeSaleRecord(sale, entityName));
		}
		return saleRecords;
	}

	public Long insertOrUpdateSale(SaleRecord record) {
		logger.debug("Start insertOrUpdateSale for sale record = " + record.toString());
		record.validateRecord().throwIfInvalid();
		Sale sale;
		if (record.getSaleId() == null) {
			logger.debug("inserting a new sale.");
			sale = Sale.getDefaultSale();
		} else {
			logger.debug("Updating an existing sale.");
			sale = saleDao.findById(record.getSaleId());
			if (sale == null) {
				logger.error(
						"The sale with sale id = " + record.getSaleId() + " could not be found, inserting a new row.");
				sale = Sale.getDefaultSale();
			}
		}
		fillOutSale(sale, record);
		saleDao.persist(sale);
		return sale.getSaleId();
	}

	/**
	 * Resets the sale's end date to the newly provided date.
	 * 
	 * @param saleId
	 *            The identifier of the sale.
	 * @param endDttm
	 *            The new end date of the sale.
	 */
	public void changeEndDate(Long saleId, Date endDttm) {
		logger.debug("Starting changeEndDate for saleId=" + saleId + " and endDttm" + endDttm);
		checkNotNull(saleId);
		checkNotNull(endDttm);
		Sale sale = saleDao.findById(saleId);
		if (sale == null) {
			logger.error("Unable to find the sale with id=" + saleId + " cannot update the end date.");
			return;
		}
		sale.setEndDttm(endDttm);
		saleDao.persist(sale);
	}

	/**
	 * Ends the sale by setting its end date to now.
	 * 
	 * @param saleId
	 *            The id of the sale to update.
	 */
	public void endSale(Long saleId) {
		logger.debug("Starting endSale for saleId=" + saleId);
		checkNotNull(saleId);
		changeEndDate(saleId, new Date());
	}

	public void deactivateSalesForItem(String parentEntityName, long entityId) {
		logger.debug("Starting deactivateSalesForItem for parentEntityName=" + parentEntityName + " and entityId="
				+ entityId);
		saleDao.inactivateByParentEntity(parentEntityName, entityId);
	}

	/**
	 * Gets a list of the active sales (where the start date time is in the past
	 * and the end date time is in the future) for a specified entity.
	 * 
	 * @param entityId
	 *            The identifier of the entity to get sales for.
	 * @return The list of sales for the entity. Will be an empty list if not
	 *         sales were found.
	 */
	public List<SaleRecord> getActiveSaleRecords(String parentEntityName, Long entityId) {
		logger.debug(
				"Starting getActiveSaleRecords for parentEntityName=" + parentEntityName + ", entityId=" + entityId);
		Map<Long, List<SaleRecord>> saleMap = getActiveSaleRecords(parentEntityName, Lists.newArrayList(entityId));
		List<SaleRecord> sales = saleMap.get(entityId);
		return sales == null ? Lists.newArrayList() : sales;
	}

	public Map<Long, List<SaleRecord>> getActiveSaleRecords(String parentEntityName, List<Long> entityIds) {
		logger.debug("Starting getActiveSaleRecords for parentEntityName=" + parentEntityName + ", entityIds="
				+ Arrays.toString(entityIds.toArray()));
		Map<Long, List<Sale>> saleMap = getActiveSalesForEntities(parentEntityName, entityIds);
		Map<Long, List<SaleRecord>> saleRecordMap = Maps.newHashMap();
		for (Map.Entry<Long, List<Sale>> entitySales : saleMap.entrySet()) {
			List<SaleRecord> recordList = Lists.newArrayList();
			for (Sale sale : entitySales.getValue()) {
				recordList.add(makeSaleRecord(sale));
			}
			saleRecordMap.put(entitySales.getKey(), recordList);
		}
		return saleRecordMap;
	}

	/**
	 * Loads a map of entity ids to list of active Sales for that entity. Sales
	 * are listed in no entityicular order.
	 * 
	 * @param entityIds
	 *            The list of entity ids to search for Sales for.
	 * @return The map of entity id to list of active sales.
	 */
	private Map<Long, List<Sale>> getActiveSalesForEntities(String parentEntityName, List<Long> entityIds) {
		if (entityIds.isEmpty()) {
			logger.debug("No entity ids to query for sales for.");
			return Maps.newHashMap();
		}
		List<Sale> sales = saleDao.getSaleForDateAndParentEntityIdList(parentEntityName, entityIds, new Date());
		if (sales.isEmpty()) {
			logger.debug("No active sales for the entity ids specified.");
			return Maps.newHashMap();
		}

		Map<Long, List<Sale>> saleMap = Maps.newHashMap();
		for (Sale sale : sales) {
			if (!saleMap.containsKey(sale.getParentEntityId())) {
				saleMap.put(sale.getParentEntityId(), Lists.newArrayList());
			}
			saleMap.get(sale.getParentEntityId()).add(sale);
		}
		return saleMap;
	}

	/**
	 * Creates a map of the effected sale item entity name and entity id to the
	 * entity's name.
	 * 
	 * @param sales
	 *            The list of sales to get entity items for.
	 * @return The entity name map.
	 */
	private Map<Tuple<String, Long>, String> makeEntityNameMap(List<Sale> sales) {
		Set<Long> partIds = Sets.newHashSet();
		Set<Long> flatRateServiceIds = Sets.newHashSet();
		for (Sale sale : sales) {
			if (PART_ENTITY_NAME.equals(sale.getParentEntityName())) {
				partIds.add(sale.getParentEntityId());
			} else if (FLAT_RATE_SERVICE_ENTITY_NAME.equals(sale.getParentEntityName())) {
				flatRateServiceIds.add(sale.getParentEntityId());
			} else {
				logger.error("Unrecognized parent entity name '" + sale.getParentEntityName()
				+ "', unable to get the entity name.");
				continue;
			}
		}

		List<Part> partList = partDao.findByIdList(partIds);
		List<FlatRateService> flatRateList = flatRateServiceDao.findByIdList(flatRateServiceIds);

		Map<Tuple<String, Long>, String> entityNameMap = Maps.newHashMap();
		for (Part p : partList) {
			entityNameMap.put(Tuple.newTuple(PART_ENTITY_NAME, p.getPartId()), p.getPartName());
		}
		for (FlatRateService frs : flatRateList) {
			entityNameMap.put(Tuple.newTuple(FLAT_RATE_SERVICE_ENTITY_NAME, frs.getFlatRateServiceId()),
					frs.getServiceName());
		}
		return entityNameMap;
	}

	/**
	 * Creates a SaleRecord based upon the parameters provided.
	 * 
	 * @param sale
	 *            The sale to create a record for.
	 * @param entityName
	 * @return The record populated.
	 */
	private SaleRecord makeSaleRecord(Sale sale, String entityName) {
		SaleRecord record = new SaleRecord();
		record.setSaleId(sale.getSaleId());
		record.setAmountOff(sale.getAmountOff());
		record.setEndDttm(sale.getEndDttm());
		record.setEntityId(sale.getParentEntityId());
		record.setEntityName(entityName);
		record.setSaleName(sale.getSaleName());
		record.setSaleProductType(convertToEntityType(sale.getParentEntityName()));
		record.setSaleType(convertToSaleType(sale.getSaleTypeCd()));
		record.setSaleTypeDisplay(getDisplay(sale.getSaleTypeCd()));
		record.setStartDttm(sale.getStartDttm());
		return record;
	}

	private SaleRecord makeSaleRecord(Sale sale) {
		return makeSaleRecord(sale, null);
	}

	/**
	 * Takes the sale supplied and populates it with the values provided by the
	 * record.
	 * 
	 * @param sale
	 *            The sale to fill out.
	 * @param record
	 *            The record to use to fill it.
	 */
	private void fillOutSale(Sale sale, SaleRecord record) {
		sale.setAmountOff(record.getAmountOff());
		sale.setEndDttm(record.getEndDttm());
		sale.setParentEntityId(record.getEntityId());
		sale.setParentEntityName(convertToEntityName(record.getSaleProductType()));
		sale.setSaleId(record.getSaleId());
		sale.setSaleName(record.getSaleName());
		sale.setSaleTypeCd(convertToSaleTypeCd(record.getSaleType()));
		sale.setStartDttm(record.getStartDttm());
	}

	/**
	 * Converts a {@link SaleEntityName} (The data model) to its corresponding
	 * {@link SaleProductType} (business model).
	 * 
	 * @param saleEntityName
	 *            The enum to convert.
	 * @return The corresponding enum.
	 */
	private SaleProductType convertToEntityType(String saleEntityName) {
		return SaleProductType.valueOf(saleEntityName);
	}

	/**
	 * Converts a {@link SaleProductType} (business model) to its corresponding
	 * {@link SaleEntityName} (The data model).
	 * 
	 * @param saleEntityName
	 *            The enum to convert.
	 * @return The corresponding enum.
	 */
	private String convertToEntityName(SaleProductType saleProductType) {
		return saleProductType.name();
	}

	private SaleType convertToSaleType(Long saleTypeCd) {
		String cdfMeaning = getCdfMeaning(saleTypeCd);
		if (SET_PRICE_OFF_CDF.equals(cdfMeaning)) {
			return SaleType.FLAT_RATE_OFF;
		} else if (PERCENTAGE_OFF_CDF.equals(cdfMeaning)) {
			return SaleType.PERCENTAGE_OFF;
		}
		logger.warn(
				"The code value (" + saleTypeCd + ") for the sale type could not be matched to a corresponding type.");
		return null;
	}

	private Long convertToSaleTypeCd(SaleType saleType) {
		if (saleType == SaleType.FLAT_RATE_OFF) {
			return getCodeValue(SALE_TYPE_CDSET, SET_PRICE_OFF_CDF);
		} else if (saleType == SaleType.PERCENTAGE_OFF) {
			return getCodeValue(SALE_TYPE_CDSET, PERCENTAGE_OFF_CDF);
		}
		logger.warn("The sale type could not be matched to a corresponding code value.");
		return null;
	}
}
