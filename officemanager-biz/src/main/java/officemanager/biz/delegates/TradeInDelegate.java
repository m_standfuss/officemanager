package officemanager.biz.delegates;

import java.util.List;

import org.jboss.logging.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.TradeInRecord;
import officemanager.data.access.TradeInDao;
import officemanager.data.models.TradeIn;

public class TradeInDelegate extends Delegate {
	private static final Logger logger = Logger.getLogger(TradeInDelegate.class);

	private final TradeInDao tradeInDao;

	public TradeInDelegate() {
		tradeInDao = new TradeInDao();
	}

	public List<CodeValueRecord> getProductTypes() {
		return getActiveCodeValueRecords(PRODUCT_TYPE_CDSET);
	}

	public List<CodeValueRecord> getTradeInTypes() {
		return getActiveCodeValueRecords(TRADE_IN_TYPE_CDSET);
	}

	public TradeInRecord getTradeIn(Long tradeInId) {
		logger.debug("Starting getTradeIn for tradeInId=" + tradeInId);
		TradeIn tradeIn = tradeInDao.findById(tradeInId);
		if (tradeIn == null) {
			logger.error("No trade in was found for the id = " + tradeInId);
			return null;
		}
		TradeInRecord record = new TradeInRecord();
		record.setAmount(tradeIn.getAmount());
		record.setClientId(tradeIn.getClientId());
		record.setCreatedDttm(tradeIn.getCreatedDtTm());
		record.setCreatePrsnlId(tradeIn.getClientId());
		record.setDescription(tradeIn.getDescription());
		record.setManufacturer(tradeIn.getManufacturer());
		record.setModel(tradeIn.getModel());
		record.setPaymentId(tradeIn.getPaymentId());
		record.setProductTypeCd(getCodeValueRecord(tradeIn.getProductTypeCd()));
		record.setTradeInId(tradeIn.getTradeInId());
		record.setTradeInType(getCodeValueRecord(tradeIn.getTradeInTypeCd()));
		return record;
	}

	public Long insertTradeIn(TradeInRecord record) {
		logger.debug("Starting insertTradeIn for " + record);
		throwIfInvalid(record);
		TradeIn tradeIn = new TradeIn();
		tradeIn.setAmount(record.getAmount());
		tradeIn.setClientId(record.getClientId());
		tradeIn.setDescription(record.getDescription());
		tradeIn.setManufacturer(record.getManufacturer());
		tradeIn.setModel(record.getModel());
		tradeIn.setPaymentId(record.getPaymentId());
		tradeIn.setProductTypeCd(record.getProductTypeCd().getCodeValue());
		tradeIn.setTradeInTypeCd(record.getTradeInType().getCodeValue());
		tradeInDao.persist(tradeIn);
		return tradeIn.getTradeInId();
	}

	@Override
	protected List<Long> warmCache() {
		return Lists.newArrayList(TRADE_IN_TYPE_CDSET, PRODUCT_TYPE_CDSET);
	}
}
