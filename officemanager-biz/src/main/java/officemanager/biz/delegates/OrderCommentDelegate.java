package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.maps.EntityMap;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.LongTextRecord;
import officemanager.biz.records.OrderCommentRecord;
import officemanager.data.access.OrderCommentDao;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PersonnelDao;
import officemanager.data.models.OrderComment;
import officemanager.data.models.Person;
import officemanager.data.models.Personnel;
import officemanager.utility.CollectionUtility;

public class OrderCommentDelegate extends Delegate {

	private static final Logger logger = LogManager.getLogger(OrderCommentDelegate.class);

	private final OrderCommentDao orderCommentDao;
	private final PersonnelDao personnelDao;
	private final PersonDao personDao;

	public OrderCommentDelegate() {
		orderCommentDao = new OrderCommentDao();
		personnelDao = new PersonnelDao();
		personDao = new PersonDao();
	}

	public List<CodeValueRecord> getOrderCommentTypes() {
		return getActiveCodeValueRecords(ORDER_CMMT_TYPE_CDSET);
	}

	public Long persistOrderComment(OrderCommentRecord record) {
		logger.debug("Starting persistComment for comment=" + record);
		throwIfInvalid(record);

		OrderComment comment;
		if(record.getOrderCommentId()==null){
			logger.debug("Creating a new order comment.");
			comment = new OrderComment();
		} else{
			logger.debug("Updating existing order comment.");
			comment = orderCommentDao.findById(record.getOrderCommentId());
			if(comment ==null){
				logger.warn("Unable to find existing order comment inserting new instead.");
				comment = new OrderComment();
			}
		}
		if (record.getLongText() != null) {
			comment.setLongTextId(persistLongText(record.getLongText()));
		}
		comment.setOrderCommentTypeCd(record.getCommentType().getCodeValue());
		comment.setOrderId(record.getOrderId());
		orderCommentDao.persist(comment);
		return comment.getOrderCommentId();
	}

	public List<OrderCommentRecord> loadOrderComments(Long orderId) {
		checkNotNull(orderId);
		logger.debug("loadOrderComments for orderId=" + orderId);

		final List<OrderComment> orderCommentList = orderCommentDao.findActiveByOrderId(orderId);
		return makeOrderComments(orderId, orderCommentList);
	}

	public List<OrderCommentRecord> loadOrderComments(Long orderId, List<Long> commentTypes) {
		checkNotNull(orderId);
		checkNotNull(commentTypes);
		logger.debug("loadOrderComments for orderId=" + orderId + ", and commentTypes=" + commentTypes);

		final List<OrderComment> orderCommentList = orderCommentDao.findActiveByOrderIdAndTypes(orderId, commentTypes);
		return makeOrderComments(orderId, orderCommentList);
	}

	private List<OrderCommentRecord> makeOrderComments(Long orderId, final List<OrderComment> orderCommentList) {
		if (orderCommentList.isEmpty()) {
			logger.debug("No order comments were found for the order id=" + orderId);
			return Lists.newArrayList();
		}

		EntityMap<Personnel> personnelMap = makePersonnelMap(orderCommentList);
		EntityMap<Person> personMap = makePersonMap(Lists.newArrayList(personnelMap.convertToCollection()));
		final List<OrderCommentRecord> recordList = Lists.newArrayList();
		for (final OrderComment oc : orderCommentList) {
			Personnel updtPrsnl = personnelMap.getEntity(oc.getUpdtPrsnlId());
			Person updtPerson = null;
			if (updtPrsnl != null) {
				updtPerson = personMap.getEntity(updtPrsnl.getPersonId());
			}
			Personnel authorPrsnl = personnelMap.getEntity(oc.getAuthorId());
			Person authorPerson = null;
			if (authorPrsnl != null) {
				authorPerson = personMap.getEntity(updtPrsnl.getPersonId());
			}
			recordList.add(makeRecord(oc, authorPerson, updtPerson));
		}
		return recordList;
	}

	private EntityMap<Person> makePersonMap(List<Personnel> personnelList) {
		if (personnelList.isEmpty()) {
			return EntityMap.newEntityMap();
		}
		List<Long> personIds = Lists.newArrayList();
		for (Personnel personnel : personnelList) {
			personIds.add(personnel.getPersonId());
		}
		return EntityMap.newEntityMap(personDao.findByIdList(personIds));
	}

	@Override
	protected List<Long> warmCache() {
		return Lists.newArrayList(ORDER_CMMT_TYPE_CDSET);
	}

	/**
	 * Gets an entity map of personnels who last updated a comment.
	 * 
	 * @param orderCommentList
	 *            The comments to get personnel from.
	 * @return The entity map.
	 */
	private EntityMap<Personnel> makePersonnelMap(List<OrderComment> orderCommentList) {
		logger.debug("Getting personnel values for comment.");

		if (CollectionUtility.isNullOrEmpty(orderCommentList)) {
			logger.debug("No comments found for the order.");
			return EntityMap.newEntityMap();
		}
		Set<Long> personnelIds = Sets.newHashSet();
		for (OrderComment oc : orderCommentList) {
			personnelIds.add(oc.getUpdtPrsnlId());
			personnelIds.add(oc.getAuthorId());
		}

		List<Personnel> personnelList = personnelDao.findByIdList(personnelIds);
		return EntityMap.newEntityMap(personnelList);
	}

	/**
	 * Creates an order comment record for the order comment entity.
	 * 
	 * @param oc
	 *            The entity to create a record for.
	 * @return The record.
	 */
	private OrderCommentRecord makeRecord(OrderComment oc, Person author, Person lastUpdated) {
		final OrderCommentRecord record = new OrderCommentRecord();
		record.setOrderCommentId(oc.getOrderCommentId());
		record.setOrderId(oc.getOrderId());
		record.setLastUpdtOn(oc.getUpdtDttm());
		record.setCommentType(getCodeValueRecord(oc.getOrderCommentTypeCd()));

		if (author != null) {
			record.setAuthoredBy(author.getNameFullFormatted());
		}

		if (lastUpdated != null) {
			record.setLastUpdtBy(lastUpdated.getNameFullFormatted());
		}

		if (oc.getLongTextId() != null) {
			LongTextRecord ltRecord = getLongTextRecord(oc.getLongTextId());
			record.setLongText(ltRecord);
		}
		return record;
	}
}
