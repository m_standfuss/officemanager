package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.maps.EntityMap;
import officemanager.biz.maps.OrderMap;
import officemanager.data.access.ClientDao;
import officemanager.data.access.ClientProductDao;
import officemanager.data.access.FlatRateServiceDao;
import officemanager.data.access.OrderToClientProductDao;
import officemanager.data.access.OrderToFlatRateServiceDao;
import officemanager.data.access.OrderToPartDao;
import officemanager.data.access.PartDao;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PersonnelDao;
import officemanager.data.access.PhoneDao;
import officemanager.data.access.ServiceDao;
import officemanager.data.models.Client;
import officemanager.data.models.ClientProduct;
import officemanager.data.models.FlatRateService;
import officemanager.data.models.OrderToClientProduct;
import officemanager.data.models.OrderToFlatRateService;
import officemanager.data.models.OrderToPart;
import officemanager.data.models.Part;
import officemanager.data.models.Person;
import officemanager.data.models.Personnel;
import officemanager.data.models.Phone;
import officemanager.data.models.Service;
import officemanager.utility.CollectionUtility;

/**
 * An business object to contain all common order logic within as to not produce
 * duplicate code. Can be extended by any other Order related business logic
 * objects. Handles the loading of all the basic building blocks of an order (ie
 * the orderTo* models) that allow for the order total to be calculated.
 * 
 * This business class does not populate any sort of business models itself but
 * instead acts as a mediator to the data source that stores common query
 * results in maps for other delegates to use.
 * 
 * @author Mike
 *
 */
abstract class OrderHelperDelegate extends Delegate {

	private static final Logger logger = LogManager.getLogger(OrderHelperDelegate.class);

	private final ClientDao clientDao;
	private final ClientProductDao clientProductDao;
	private final FlatRateServiceDao flatRateServiceDao;
	private final OrderToClientProductDao orderToClientProductDao;
	private final OrderToFlatRateServiceDao orderToFlatRateServiceDao;
	private final OrderToPartDao orderToPartDao;
	private final PartDao partDao;
	private final PersonDao personDao;
	private final PersonnelDao personnelDao;
	private final PhoneDao phoneDao;
	private final ServiceDao serviceDao;

	/**
	 * Default Constructor.
	 */
	public OrderHelperDelegate() {
		clientDao = new ClientDao();
		clientProductDao = new ClientProductDao();
		flatRateServiceDao = new FlatRateServiceDao();
		orderToClientProductDao = new OrderToClientProductDao();
		orderToFlatRateServiceDao = new OrderToFlatRateServiceDao();
		orderToPartDao = new OrderToPartDao();
		partDao = new PartDao();
		personDao = new PersonDao();
		personnelDao = new PersonnelDao();
		phoneDao = new PhoneDao();
		serviceDao = new ServiceDao();
	}

	/**
	 * Loads all the order helper related information based on the list of order
	 * ids.
	 * 
	 * @param orderIds
	 *            The order ids to load for.
	 */
	protected OrderMap getOrderMap(Collection<Long> orderIds) {
		logger.debug("loadOrderMap for orderIds=" + orderIds);
		checkNotNull(orderIds);
		final OrderMap orderMap = new OrderMap(orderIds);

		orderMap.setClientProductMap(loadClientProductsMap(orderIds));
		orderMap.setOrderFlatRatesMap(loadOrderFlatRatesMap(orderIds));
		orderMap.setOrderPartsMap(loadOrderPartsMap(orderIds));
		orderMap.setOrderServicesMap(loadOrderServicesMap(orderIds));
		return orderMap;
	}

	protected List<ClientProduct> getClientProducts(Long orderId) {
		Map<Long, List<OrderToClientProduct>> clientProductMap = loadClientProductsMap(Lists.newArrayList(orderId));
		List<OrderToClientProduct> otcps = clientProductMap.get(orderId);
		if (otcps == null) {
			return Lists.newArrayList();
		}
		return makeClientProductList(otcps);
	}

	/**
	 * Populates the client product map for the provided order ids.
	 * 
	 * @param orderIds
	 *            The order ids to populate active client products for.
	 * @return
	 */
	protected Map<Long, List<OrderToClientProduct>> loadClientProductsMap(Collection<Long> orderIds) {
		final List<OrderToClientProduct> orderToClientProductList = orderToClientProductDao
				.findActiveByOrderIds(orderIds);
		Map<Long, List<OrderToClientProduct>> clientProductsMap = Maps.newHashMap();
		for (OrderToClientProduct otcp : orderToClientProductList) {
			if (!clientProductsMap.containsKey(otcp.getOrderId())) {
				clientProductsMap.put(otcp.getOrderId(), Lists.newArrayList(otcp));
			} else {
				clientProductsMap.get(otcp.getOrderId()).add(otcp);
			}
		}
		return clientProductsMap;
	}

	protected List<ClientProduct> makeClientProductList(List<OrderToClientProduct> orderToClientProducts) {
		if (CollectionUtility.isNullOrEmpty(orderToClientProducts)) {
			return Lists.newArrayList();
		}

		Set<Long> clientProductIds = Sets.newHashSet();
		for (OrderToClientProduct otcp : orderToClientProducts) {
			clientProductIds.add(otcp.getClientProductId());
		}
		return clientProductDao.findByIdList(clientProductIds);
	}

	protected EntityMap<ClientProduct> makeClientProductMap(List<OrderToClientProduct> orderToClientProductList) {
		return EntityMap.newEntityMap(makeClientProductList(orderToClientProductList));
	}

	protected List<OrderToFlatRateService> loadOrderFlatRates(Long orderId) {
		Map<Long, List<OrderToFlatRateService>> map = loadOrderFlatRatesMap(Lists.newArrayList(orderId));
		List<OrderToFlatRateService> orderFlatRates = map.get(orderId);
		return orderFlatRates == null ? Lists.newArrayList() : orderFlatRates;
	}

	/**
	 * Populates the map for the order to order flat rate services map.
	 * 
	 * @param orderIds
	 *            The order ids to populate active client products for.
	 * @return
	 */
	protected Map<Long, List<OrderToFlatRateService>> loadOrderFlatRatesMap(Collection<Long> orderIds) {
		List<OrderToFlatRateService> otfrsList = orderToFlatRateServiceDao.findActiveByOrderIds(orderIds);
		Map<Long, List<OrderToFlatRateService>> orderFlatRatesMap = Maps.newHashMap();
		for (OrderToFlatRateService otfrs : otfrsList) {
			if (!orderFlatRatesMap.containsKey(otfrs.getOrderId())) {
				orderFlatRatesMap.put(otfrs.getOrderId(), Lists.newArrayList(otfrs));
			} else {
				orderFlatRatesMap.get(otfrs.getOrderId()).add(otfrs);
			}
		}
		return orderFlatRatesMap;
	}

	protected List<OrderToPart> loadOrderParts(Long orderId) {
		Map<Long, List<OrderToPart>> map = loadOrderPartsMap(Lists.newArrayList(orderId));
		List<OrderToPart> orderParts = map.get(orderId);
		return orderParts == null ? Lists.newArrayList() : orderParts;
	}

	/**
	 * Populates the map for the order to parts map.
	 * 
	 * @param orderIds
	 *            The order ids to populate active client products for.
	 * @return
	 */
	protected Map<Long, List<OrderToPart>> loadOrderPartsMap(Collection<Long> orderIds) {
		List<OrderToPart> otpList = orderToPartDao.findActiveByOrderIds(orderIds);
		Map<Long, List<OrderToPart>> orderPartsMap = Maps.newHashMap();
		for (final OrderToPart otp : otpList) {
			if (!orderPartsMap.containsKey(otp.getOrderId())) {
				orderPartsMap.put(otp.getOrderId(), Lists.newArrayList(otp));
			} else {
				orderPartsMap.get(otp.getOrderId()).add(otp);
			}
		}
		return orderPartsMap;
	}

	/**
	 * Populates the map for the order to services map.
	 * 
	 * @param orderIds
	 *            The order ids to populate active client products for.
	 * @return
	 */
	protected Map<Long, List<Service>> loadOrderServicesMap(Collection<Long> orderIds) {
		final List<Service> serviceList = serviceDao.findActiveByOrderIds(orderIds);
		final Map<Long, List<Service>> orderServicesMap = Maps.newHashMap();
		for (final Service service : serviceList) {
			if (!orderServicesMap.containsKey(service.getOrderId())) {
				orderServicesMap.put(service.getOrderId(), Lists.newArrayList(service));
			} else {
				orderServicesMap.get(service.getOrderId()).add(service);
			}
		}
		return orderServicesMap;
	}

	protected EntityMap<Personnel> makePersonnelMap(Collection<Long> prsnlIds) {
		List<Personnel> personnelList = personnelDao.findByIdList(prsnlIds);
		return new EntityMap<Personnel>(personnelList);
	}

	protected EntityMap<Client> makeClientMap(Collection<Long> clientIds) {
		List<Client> clients = clientDao.findByIdList(clientIds);
		return EntityMap.newEntityMap(clients);
	}

	protected EntityMap<Person> makePersonMap(Collection<Personnel> personnelList, Collection<Client> clients) {
		List<Long> personIds = Lists.newArrayList();
		if (personnelList != null) {
			for (Personnel personnel : personnelList) {
				if (personnel == null) {
					continue;
				}
				personIds.add(personnel.getPersonId());
			}
		}
		if (clients != null) {
			for (Client client : clients) {
				if (client == null) {
					continue;
				}
				personIds.add(client.getPersonId());
			}
		}
		return EntityMap.newEntityMap(personDao.findByIdList(personIds));
	}

	protected Map<Long, Phone> makePrimaryPhoneMap(Collection<Long> personIds) {
		logger.debug("Getting primary phone map.");
		List<Phone> primaryPhones = phoneDao.findPrimaryPhone(personIds);
		Map<Long, Phone> phoneMap = Maps.newHashMap();
		primaryPhones.forEach(p -> phoneMap.put(p.getPersonId(), p));
		return phoneMap;
	}

	/**
	 * Makes a entity part map of parts that for the supplied order to part
	 * services.
	 * 
	 * @param parts
	 *            The list of order to parts to get that parts for.
	 * @return The entity map of parts.
	 */
	protected EntityMap<Part> makePartMap(List<OrderToPart> parts) {
		if (CollectionUtility.isNullOrEmpty(parts)) {
			logger.debug("No order to parts found.");
			return EntityMap.newEntityMap();
		}
		Set<Long> partIds = Sets.newHashSet();
		for (OrderToPart otp : parts) {
			partIds.add(otp.getPartId());
		}
		List<Part> partList = partDao.findByIdList(partIds);
		return EntityMap.newEntityMap(partList);
	}

	/**
	 * Makes an entity map of the flat rate services for the supplied order to
	 * flat rate services.
	 * 
	 * @param flatRates
	 *            The order to flat rate services to get services for.
	 * @return The entity map.
	 */
	protected EntityMap<FlatRateService> makeFlatRateMap(List<OrderToFlatRateService> flatRates) {
		if (CollectionUtility.isNullOrEmpty(flatRates)) {
			logger.debug("No flat rate services found for order.");
			return EntityMap.newEntityMap();
		}
		Set<Long> flatRateIds = Sets.newHashSet();
		for (OrderToFlatRateService otfrs : flatRates) {
			flatRateIds.add(otfrs.getFlatRateServiceId());
		}
		List<FlatRateService> flatRateList = flatRateServiceDao.findByIdList(flatRateIds);
		return EntityMap.newEntityMap(flatRateList);
	}
}
