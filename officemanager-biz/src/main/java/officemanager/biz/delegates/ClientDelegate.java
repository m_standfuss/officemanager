package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.maps.EntityMap;
import officemanager.biz.records.ClientRecord;
import officemanager.biz.records.ClientSearchRecord;
import officemanager.biz.records.PersonRecord;
import officemanager.biz.searchingcriteria.PersonSearchCriteria;
import officemanager.biz.searchservices.PersonSearchService;
import officemanager.data.access.ClientDao;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PhoneDao;
import officemanager.data.models.Client;
import officemanager.data.models.Person;
import officemanager.data.models.Phone;

/**
 * Handles all business logic surrounding client information.
 *
 * @author Mike
 *
 */
public class ClientDelegate extends PersonDelegate {

	private static final Logger logger = LogManager.getLogger(ClientDelegate.class);

	private final ClientDao clientDao;
	private final PersonDao personDao;
	private final PhoneDao phoneDao;
	private final PersonSearchService searchService;

	/**
	 * Default constructor
	 */
	public ClientDelegate() {
		clientDao = new ClientDao();
		personDao = new PersonDao();
		phoneDao = new PhoneDao();
		searchService = new PersonSearchService();
	}

	/**
	 * Searches for the ClientSelectionRecords that meet the criteria passed in.
	 *
	 * If the criteria is empty the search will not be performed and an empty
	 * list will be returned.
	 *
	 * @param criteria
	 *            The criteria to search for.
	 * @return The list of ClientSearchRecords that meet the criteria.
	 */
	public List<ClientSearchRecord> searchForClientSelections(PersonSearchCriteria criteria) {
		logger.debug("searchClientSelection with criteria= " + criteria);
		if (criteria.isEmpty()) {
			return Lists.newArrayList();
		}

		List<Long> personIdList = searchService.searchForPersonIds(criteria);
		List<Client> activeClients = clientDao.findActiveByPersonIds(personIdList);

		final List<ClientSearchRecord> resultList = populateClientSelection(activeClients);
		logger.debug(resultList.size() + " results found.");
		return resultList;
	}

	/**
	 * Loads the client display record for the provided client id.
	 *
	 * @param clientId
	 *            The client id to load record for.
	 * @return The display record, null if the client cannot be found.
	 */
	public ClientRecord getClientRecord(Long clientId) {
		checkNotNull(clientId);
		logger.debug("loadClientDisplay for clientId=" + clientId);

		Client client = clientDao.findById(clientId);
		if (client == null) {
			logger.error("Unable to find a client with id = " + clientId);
			return null;
		}

		PersonRecord personRecord = getPersonRecord(client.getPersonId());

		ClientRecord record = new ClientRecord(personRecord);
		record.setClientId(client.getClientId());
		record.setJoinedOn(client.getJoinedDttm());
		record.setAttentionOf(client.getAttentionOf());
		record.setPromotionClub(client.isPromotionClub());
		record.setTaxExempt(client.isTaxExempt());
		return record;
	}

	/**
	 * Persists a client model into data store. As side effects the person id
	 * will be set on the client record, along with the primary address/phone
	 * records.
	 *
	 * @param record
	 *            The client model to persist.
	 * @return The client id of the model persisted.
	 * @throws IllegalArgumentException
	 *             If the record is invalid.
	 */
	public Long persistClientRecord(ClientRecord record) {
		logger.debug("insertOrUpdate for clientRecord=" + record);
		checkNotNull(record);
		throwIfInvalid(record);

		Long personId = persistPersonRecord(record);
		return persistClient(record, personId);
	}

	public boolean isInPromotionalClub(Long clientId) {
		logger.debug("Starting isInPromotionalClub for clientId = " + clientId);
		checkNotNull(clientId);

		Client client = clientDao.findById(clientId);
		if (client == null) {
			logger.error("No client was found for id " + clientId);
			return false;
		}
		return client.isPromotionClub();
	}

	public boolean isTaxExempt(Long clientId) {
		logger.debug("Starting isInPromotionalClub for clientId = " + clientId);
		checkNotNull(clientId);

		Client client = clientDao.findById(clientId);
		if (client == null) {
			logger.error("No client was found for id " + clientId);
			return false;
		}
		return client.isTaxExempt();
	}

	@Override
	protected List<Long> warmCache() {
		return super.warmCache();
	}

	/**
	 * Populates a list of ClientSearchRecords based on a list of clientIds.
	 *
	 * @param clientIdList
	 *            The list of client ids to generate records for.
	 * @return The list of ClientSelectionRecords
	 */
	private List<ClientSearchRecord> populateClientSelection(List<Client> clientList) {
		if (clientList.isEmpty()) {
			return Lists.newArrayList();
		}

		Set<Long> personIdList = Sets.newHashSet();
		for (Client client : clientList) {
			personIdList.add(client.getPersonId());
		}

		List<Phone> primaryPhones = phoneDao.findPrimaryPhone(personIdList);
		Map<Long, Phone> phoneMap = Maps.newHashMap();
		for (Phone phone : primaryPhones) {
			if (phoneMap.containsKey(phone.getPersonId())) {
				logger.warn("Multiple primary phone numbers found for the person id " + phone.getPersonId());
			}
			phoneMap.put(phone.getPersonId(), phone);
		}

		List<Person> personList = personDao.findActiveByPersonIds(personIdList);
		EntityMap<Person> personMap = EntityMap.newEntityMap(personList);

		List<ClientSearchRecord> recordList = Lists.newArrayList();
		for (Client client : clientList) {
			Phone primaryPhone = phoneMap.get(client.getPersonId());
			Person person = personMap.getEntity(client.getPersonId());

			ClientSearchRecord record = new ClientSearchRecord();
			record.setClientId(client.getClientId());
			record.setJoinedOnDttm(client.getJoinedDttm());

			if (primaryPhone == null) {
				logger.debug("No primary phone number found for clientId=" + client.getClientId());
			} else {
				record.setPrimaryPhone(primaryPhone.getPhoneNumber());
			}

			if (person == null) {
				logger.warn("No person was found for clientId=" + client.getClientId());
			} else {
				record.setNameFullFormatted(person.getNameFullFormatted());
				record.setEmail(person.getEmail());
			}

			recordList.add(record);
		}
		return recordList;
	}

	/**
	 * Persists the Client data model to the data store.
	 *
	 * @param record
	 *            The client record to persist.
	 * @return The identifier of the client model persisted.
	 */
	private Long persistClient(ClientRecord record, Long personId) {
		logger.debug("Starting persistClient with record=" + record);
		checkNotNull(record);
		checkNotNull(personId);
		throwIfInvalid(record);

		Client client;
		if (record.getClientId() == null) {
			logger.debug("No client id specified inserting a new row.");
			client = new Client();
		} else {
			logger.debug("Client id specified attempting to update existing row.");
			client = clientDao.findById(record.getClientId());
			if (client == null) {
				throw new IllegalArgumentException("The client id could not be found.");
			}
		}

		client.setClientId(record.getClientId());
		client.setPersonId(personId);
		client.setAttentionOf(record.getAttentionOf());
		client.setJoinedDttm(record.getJoinedOn());
		client.setPromotionClub(record.isPromotionClub());
		client.setTaxExempt(record.isTaxExempt());
		clientDao.persist(client);

		return client.getClientId();
	}
}
