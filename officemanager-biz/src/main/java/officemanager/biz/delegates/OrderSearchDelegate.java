package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.biz.EntityHelper;
import officemanager.biz.maps.EntityMap;
import officemanager.biz.maps.OrderMap;
import officemanager.biz.records.OrderSearchRecord;
import officemanager.biz.searchingcriteria.OrderSearchCriteria;
import officemanager.biz.searchingcriteria.PersonSearchCriteria;
import officemanager.data.access.ClientDao;
import officemanager.data.access.OrderDao;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PersonnelDao;
import officemanager.data.access.PhoneDao;
import officemanager.data.models.Client;
import officemanager.data.models.Order;
import officemanager.data.models.Person;
import officemanager.data.models.Personnel;
import officemanager.data.models.Phone;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

/**
 * Handles all the business logic for searching orders.
 * 
 * @author Mike
 *
 */
public class OrderSearchDelegate extends OrderHelperDelegate {

	private static final Logger logger = LogManager.getLogger(OrderSearchDelegate.class);

	private static final String INPROGRESS_CDF = "INPROGRESS";
	private static final String OPEN_CDF = "OPEN";
	private static final List<String> OPEN_ORDER_CDFS = Lists.newArrayList(INPROGRESS_CDF, OPEN_CDF);

	private static final String CLOSED_CDF = "CLOSED";
	private static final String INERROR_CDF = "INERROR";
	private static final String REFUNDED_CDF = "REFUNDED";
	private static final List<String> CLOSED_ORDER_CDFS = Lists.newArrayList(CLOSED_CDF, INERROR_CDF, REFUNDED_CDF);

	private final ClientDao clientDao;
	private final OrderDao orderDao;
	private final PersonDao personDao;
	private final PersonnelDao personnelDao;
	private final PhoneDao phoneDao;

	/**
	 * Default constructor.
	 */
	public OrderSearchDelegate() {
		clientDao = new ClientDao();
		orderDao = new OrderDao();
		personDao = new PersonDao();
		personnelDao = new PersonnelDao();
		phoneDao = new PhoneDao();
	}

	public List<OrderSearchRecord> searchOrders(OrderSearchCriteria orderCriteria) {
		logger.debug("searchOrders for the criteria=" + orderCriteria);

		List<Long> orderStatusCdList = getCodeValueList(ORDER_STATUS_CDSET);
		List<Order> orderResults = searchOrders(orderCriteria, orderStatusCdList, null);

		return makeOrderSearchRecords(orderResults);
	}

	/**
	 * Searches for open orders (OPEN, INPROGRESS or BID) based on the criteria
	 * provided.
	 * 
	 * @param orderCriteria
	 *            The searching criteria.
	 * @return The list of records.
	 */
	public List<OrderSearchRecord> searchCurrentOrders(OrderSearchCriteria orderCriteria) {
		logger.debug("searchCurrentOrders for the criteria=" + orderCriteria);
		Collection<Long> searchedClientIds = searchClients(orderCriteria.clientId, orderCriteria.personSearchCriteria);

		List<String> closedOrderStatusCdfList = Lists.newArrayList(OPEN_ORDER_CDFS);
		List<Long> openOrderStatusCdList = getCodeValueList(ORDER_STATUS_CDSET, closedOrderStatusCdfList);

		List<Order> orderResults = searchOrders(orderCriteria, openOrderStatusCdList, searchedClientIds);

		return makeOrderSearchRecords(orderResults);
	}

	/**
	 * Searches for closed orders (CLOSED, REFUNDED, MERCHANDISE or INERROR)
	 * based on the criteria provided.
	 * 
	 * @param orderCriteria
	 *            The searching criteria.
	 * @return The list of records.
	 */
	public List<OrderSearchRecord> searchClosedOrders(OrderSearchCriteria orderCriteria) {
		logger.debug("searchClosedOrders for criteria=" + orderCriteria);
		Collection<Long> searchedClientIds = searchClients(orderCriteria.clientId, orderCriteria.personSearchCriteria);

		List<Long> closedOrderStatusCdList = getCodeValueList(ORDER_STATUS_CDSET, CLOSED_ORDER_CDFS);

		List<Order> orderResults = searchOrders(orderCriteria, closedOrderStatusCdList, searchedClientIds);

		return makeOrderSearchRecords(orderResults);
	}

	@Override
	protected List<Long> warmCache() {
		List<Long> codeSetsAndCdfMeanings = Lists.newArrayList();
		codeSetsAndCdfMeanings.add(ORDER_STATUS_CDSET);
		codeSetsAndCdfMeanings.add(ORDER_TYPE_CDSET);
		return codeSetsAndCdfMeanings;
	}

	/**
	 * Creates the order search records based on a list of orders.
	 * 
	 * @param orderList
	 *            The orders to create records for.
	 * @return The records.
	 */
	private List<OrderSearchRecord> makeOrderSearchRecords(List<Order> orderList) {
		List<Long> orderIds = EntityHelper.getPrimaryKeyList(Long.class, orderList);
		logger.debug("Creating order search record for order ids " + orderIds);

		Set<Long> personnelIds = Sets.newHashSet();
		Set<Long> clientIds = Sets.newHashSet();
		for (Order order : orderList) {
			personnelIds.add(order.getCreatedPrsnlId());
			personnelIds.add(order.getClosedPrsnlId());

			clientIds.add(order.getClientId());
		}
		EntityMap<Client> clientMap = EntityMap.newEntityMap(clientDao.findByIdList(clientIds));
		EntityMap<Personnel> personnelMap = EntityMap.newEntityMap(personnelDao.findByIdList(personnelIds));

		Set<Long> personIds = Sets.newHashSet();
		Set<Long> clientPersonIds = Sets.newHashSet();
		for (Client client : clientMap.convertToCollection()) {
			personIds.add(client.getPersonId());
			clientPersonIds.add(client.getPersonId());
		}
		for (Personnel personnel : personnelMap.convertToCollection()) {
			personIds.add(personnel.getPersonId());
		}
		EntityMap<Person> personMap = EntityMap.newEntityMap(personDao.findByIdList(personIds));
		OrderMap orderMap = getOrderMap(orderIds);
		Map<Long, Phone> phoneMap = makePrimaryPhoneMap(clientPersonIds);

		List<OrderSearchRecord> recordList = Lists.newArrayList();
		for (final Order order : orderList) {
			recordList.add(makeOrderSearchRecord(order, orderMap, clientMap, personnelMap, personMap, phoneMap));
		}
		logger.debug("Returning " + recordList.size() + " results.");
		return recordList;
	}

	/**
	 * Searches for the clients based on name criteria and a phone number.
	 * 
	 * @param nameCriteria
	 *            The name criteria to search on.
	 * @param clientPhone
	 *            The phone number to search on.
	 * @return The list of client ids for the searching criteria. Null if the
	 *         search was not performed.
	 */
	private Collection<Long> searchClients(Long clientId, PersonSearchCriteria personCriteria) {
		if (personCriteria == null || personCriteria.isEmpty()) {
			logger.debug("Not searching for client parameters.");
			if (clientId == null) {
				return null;
			}
			return Lists.newArrayList(clientId);
		}
		logger.debug("Searching for clients with criteria=" + personCriteria);
		List<Long> personIds = searchPersons(personCriteria);
		List<Client> clients = clientDao.findActiveByPersonIds(personIds);
		List<List<Long>> allClientIds = Lists.newArrayList();
		allClientIds.add(EntityHelper.getPrimaryKeyList(Long.class, clients));
		if (clientId != null) {
			allClientIds.add(Lists.newArrayList(clientId));
		}
		return CollectionUtility.intersectCollections(allClientIds);
	}

	/**
	 * Runs the query against the order table based on the criteria provided.
	 * 
	 * @param criteria
	 *            The order search criteria
	 * @param orderStatusCdList
	 *            The order status cd list
	 * @param clientIdList
	 *            The client id list.
	 * @return The list of orders for the criteria.
	 */
	private List<Order> searchOrders(OrderSearchCriteria criteria, Collection<Long> orderStatusCdList,
			Collection<Long> clientIdList) {
		logger.debug("Quering for orders with order status " + orderStatusCdList + ", and client ids " + clientIdList);
		return orderDao.findBySearchCriteria(criteria.orderId, criteria.begOpenedDttm, criteria.endOpenedDttm,
				criteria.begClosedDttm, criteria.endClosedDttm, clientIdList, orderStatusCdList, criteria.openedPrsnlId,
				criteria.closedPrsnlId);
	}

	/**
	 * Finds the person ids that are reflective of the searching criteria.
	 * 
	 * @param searchCriteria
	 *            The searching criteria.
	 * @return The list of person ids.
	 */
	protected List<Long> searchPersons(PersonSearchCriteria searchCriteria) {
		logger.debug("Starting searchForPersonIds with searchCriteria=" + searchCriteria);
		checkNotNull(searchCriteria);

		if (searchCriteria.isEmpty()) {
			logger.debug("No person search criteria specified.");
			return Lists.newArrayList();
		}

		List<List<Long>> listOfPersonIdResults = Lists.newArrayList();
		if (!StringUtility.isNullOrWhiteSpace(searchCriteria.nameFirst)
				|| !StringUtility.isNullOrWhiteSpace(searchCriteria.nameLast)) {
			logger.debug("A person's first and/or last name specified for searching.");
			List<Person> personList = personDao.findActiveByNameFirstAndLastLike(searchCriteria.nameFirst,
					searchCriteria.nameLast);
			listOfPersonIdResults.add(EntityHelper.getPrimaryKeyList(Long.class, personList));
		}

		if (!StringUtility.isNullOrWhiteSpace(searchCriteria.primaryPhone)) {
			logger.debug("A client's phone specified, searching.");
			List<Phone> phoneList = phoneDao.findByPhoneNumber(searchCriteria.primaryPhone);

			List<Long> personIdList = Lists.newArrayList();
			for (Phone phone : phoneList) {
				personIdList.add(phone.getPersonId());
			}
			listOfPersonIdResults.add(personIdList);
		}

		return Lists.newArrayList(CollectionUtility.intersectCollections(listOfPersonIdResults));
	}

	/**
	 * Creates a OrderSearchRecord based on the Order provided using the global
	 * maps.
	 * 
	 * @param order
	 *            The Order to create a record for.
	 * @return The record.
	 */
	private OrderSearchRecord makeOrderSearchRecord(Order order, OrderMap orderMap, EntityMap<Client> clientMap,
			EntityMap<Personnel> personnelMap, EntityMap<Person> personMap, Map<Long, Phone> primaryPhoneMap) {
		Long orderId = order.getOrderId();
		Date openedDttm = order.getCreatedDttm();
		BigDecimal amount = orderMap.getOrderTotal(order.getOrderId());
		String orderStatus = getDisplay(order.getOrderStatusCd());
		String orderType = getDisplay(order.getOrderTypeCd());
		int productCount = orderMap.getClientProducts(order.getOrderId()).size();
		Date closedDttm = order.getClosedDttm();

		String clientName = "";
		String primaryPhone = "";
		Client client = clientMap.getEntity(order.getClientId());
		if (client != null) {
			Person person = personMap.getEntity(client.getPersonId());
			if (person != null) {
				clientName = person.getNameFullFormatted();
			}

			Phone phone = primaryPhoneMap.get(client.getPersonId());
			if (phone != null) {
				primaryPhone = phone.getPhoneNumber();
			}
		}

		String closedBy = "";
		Personnel closedPrsnl = personnelMap.getEntity(order.getClosedPrsnlId());
		if (closedPrsnl != null) {
			Person person = personMap.getEntity(closedPrsnl.getPersonId());
			if (person != null) {
				closedBy = person.getNameFullFormatted();
			}
		}

		String openedBy = "";
		Personnel openedPrsnl = personnelMap.getEntity(order.getCreatedPrsnlId());
		if (openedPrsnl != null) {
			Person person = personMap.getEntity(openedPrsnl.getPersonId());
			if (person != null) {
				openedBy = person.getNameFullFormatted();
			}
		}

		return new OrderSearchRecord(orderId, clientName, primaryPhone, openedDttm, openedBy, amount, orderStatus,
				orderType, productCount, closedDttm, closedBy);
	}
}
