package officemanager.biz.delegates;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import officemanager.biz.CodeValueCache;
import officemanager.biz.EntityHelper;
import officemanager.biz.maps.EntityMap;
import officemanager.biz.records.CodeValueRecord;
import officemanager.biz.records.IValidateRecord;
import officemanager.biz.records.LongTextRecord;
import officemanager.biz.records.LongTextRecord.TextFormat;
import officemanager.biz.records.ValidationResults;
import officemanager.data.access.LongTextDao;
import officemanager.data.access.PersonDao;
import officemanager.data.models.CodeValue;
import officemanager.data.models.LongText;
import officemanager.data.models.Person;
import officemanager.data.models.Personnel;
import officemanager.data.models.Sale;

/**
 * The base delegate class. Implements things that all (or most) delegates have
 * in common or build on top of. </br>
 * </br>
 * Examples are:
 * <ul>
 * <li>Loading code values</li>
 * <li>Loading long text</li>
 * <li>Persisting long text</li>
 * </ul>
 * 
 * @author MS025633
 *
 */
public abstract class Delegate {

	public static final Long PRODUCT_STATUS_CDSET = 2L;
	public static final Long PRODUCT_TYPE_CDSET = 3L;
	public static final Long SALE_TYPE_CDSET = 4L;
	public static final Long INVNTRY_STATUS_CDSET = 5L;
	public static final Long MERC_PRODUCT_TYPE_CDSET = 6L;
	public static final Long MERC_MANUF_CDSET = 7L;
	public static final Long MERC_CATEGORIES_CDSET = 8L;
	public static final Long FLTRT_SERV_CATEGORIES_CDSET = 9L;
	public static final Long ORDER_TYPE_CDSET = 10L;
	public static final Long TXT_FRMT_CDSET = 11L;
	public static final Long PAYMENT_TYPE_CDSET = 12L;
	public static final Long CRDT_CARD_COMPANIES_CDSET = 13L;
	public static final Long CLIENT_CRDT_TRANS_TYPE_CDSET = 14L;
	public static final Long PRSNL_STATUS_TYPE_CDSET = 15L;
	public static final Long PRSNL_TYPE_CDSET = 16L;
	public static final Long CITY_CDSET = 17L;
	public static final Long TRADE_IN_TYPE_CDSET = 18L;
	public static final Long LOCATION_TYPE_CDSET = 19L;
	public static final Long ORDER_CMMT_TYPE_CDSET = 21L;
	public static final Long PHONE_TYPE_CDSET = 22L;
	public static final Long ADDRESS_TYPE_CDSET = 23L;
	public static final Long ORDER_STATUS_CDSET = 24L;
	public static final Long CLIENT_CRDT_STATUS_CDSET = 25L;
	public static final Long PERMISSION_CATEGORY_CDSET = 26L;

	/**
	 * These are now set through user permissions. The tabs are displayed in
	 * alphabetical order.
	 */
	@Deprecated
	public static final Long TAB_PREFERENCE_CDSET = 1L;

	/**
	 * Use the
	 * {@link Sale#setParentEntityName(officemanager.data.models.SaleEntityName)}
	 * instead of specifying a code value.
	 */
	@Deprecated
	protected static final Long PARENT_ENTY_SALE_TYPE = 20L;

	private static final Logger logger = LogManager.getLogger(Delegate.class);

	private static final String HTML_CDF = "HTML";
	private static final String RTF_CDF = "RTF";
	private static final String PLAIN_TEXT_CDF = "PLAINTEXT";

	private static final CodeValueCache codeValueCache = new CodeValueCache();

	private final LongTextDao longTextDao;
	private final PersonDao personDao;

	public static void clearCodeValueCache() {
		codeValueCache.clearCache();
	}

	protected Delegate() {
		longTextDao = new LongTextDao();
		personDao = new PersonDao();
		List<Long> codeSets = warmCache();
		if (codeSets != null) {
			codeValueCache.getCodeValuesByCodeSet(codeSets);
		}
	}

	/**
	 * Gets the code values that are needed for the delegate.
	 * 
	 * @return The list of code sets deemed important enough to pre-load the
	 *         code value cache with.
	 */
	protected abstract List<Long> warmCache();

	/**
	 * Gets a list of code values given the code set and cdf meanings.
	 * 
	 * @param codeSet
	 *            The code set to get values for.
	 * @param cdfMeanings
	 *            The list of cdf meanings to get values for.
	 * @return The list of code values found that meet the criteria.
	 */
	protected List<Long> getCodeValueList(Long codeSet, List<String> cdfMeanings) {
		List<CodeValue> codeValues = codeValueCache.getCodeValuesBySetAndCdf(codeSet, cdfMeanings);
		return EntityHelper.getPrimaryKeyList(Long.class, codeValues);
	}

	protected List<Long> getCodeValueList(Long codeSet) {
		List<CodeValue> codeValues = codeValueCache.getCodeValuesByCodeSet(codeSet);
		return EntityHelper.getPrimaryKeyList(Long.class, codeValues);
	}

	/**
	 * Gets a code value given the code set and cdf meaning.</br>
	 * </br>
	 * <b>Note:</b> This should only be used when the cdf meaning is distinct
	 * for the code set. Has undetermined outcome if more then one code value in
	 * the set share the same cdf meaning.
	 * 
	 * @param codeSet
	 *            The code set.
	 * @param cdfMeaning
	 *            The cdf meaning.
	 * @return The code value.
	 */
	protected Long getCodeValue(Long codeSet, String cdfMeaning) {
		CodeValue model = codeValueCache.getCodeValue(codeSet, cdfMeaning);
		if (model == null) {
			return null;
		}
		return model.getCodeValue();
	}

	/**
	 * Gets the display value for the given criteria, never null. </br>
	 * </br>
	 * <b>Note:</b> This should only be used when the cdf meaning is distinct
	 * for the code set. Has undetermined outcome if more then one code value in
	 * the set share the same cdf meaning.
	 * 
	 * @param codeSet
	 *            The code set.
	 * @param cdfMeaning
	 *            The cdf meaning.
	 * @return The not-null display value for the code value. If the code value
	 *         was not found then an empty string is returned.
	 */
	protected String getDisplay(Long codeSet, String cdfMeaning) {
		CodeValue model = codeValueCache.getCodeValue(codeSet, cdfMeaning);
		if (model == null) {
			return "";
		}
		return model.getDisplay() == null ? "" : model.getDisplay();
	}

	/**
	 * Gets the display value for the given criteria, never null.
	 * 
	 * @param codeValue
	 *            The code value.
	 * @return The not-null display value for the code value. If the code value
	 *         was not found then an empty string is returned.
	 */
	protected String getDisplay(Long codeValue) {
		CodeValue model = codeValueCache.getCodeValue(codeValue);
		if (model == null) {
			return "";
		}
		return model.getDisplay() == null ? "" : model.getDisplay();
	}

	/**
	 * Gets the cdf meaning for the given code value.
	 * 
	 * @param codeValue
	 *            The code value.
	 * @return The cdf meaning, no safe guards against nulls. Will return null
	 *         if code value was not found.
	 */
	protected String getCdfMeaning(Long codeValue) {
		CodeValue model = codeValueCache.getCodeValue(codeValue);
		if (model == null) {
			return null;
		}
		return model.getCdfMeaning();
	}

	/**
	 * Creates a code value record for the given code value.
	 * 
	 * @param codeValue
	 *            The code value to make a record for.
	 * @return The code value record.
	 */
	protected CodeValueRecord getCodeValueRecord(Long codeValue) {
		logger.debug("Starting getCodeValueRecord for codeValue=" + codeValue);
		checkNotNull(codeValue);

		CodeValue codeValueModel = codeValueCache.getCodeValue(codeValue);
		return makeCodeValueRecord(codeValueModel);
	}

	protected List<CodeValueRecord> getCodeValueRecords(Long codeSet) {
		logger.debug("Starting getCodeValueRecords for codeSet=" + codeSet);
		checkNotNull(codeSet);

		List<CodeValue> codeValues = codeValueCache.getCodeValuesByCodeSet(codeSet);
		List<CodeValueRecord> recordList = Lists.newArrayList();
		for (CodeValue cv : codeValues) {
			recordList.add(makeCodeValueRecord(cv));
		}
		Collections.sort(recordList);
		return recordList;
	}

	/**
	 * Creates active code value records for the given code set. Always returns
	 * them sorted by collation seq.
	 * 
	 * @param codeSet
	 *            The code set to make a records for.
	 * @return The code value records.
	 */
	protected List<CodeValueRecord> getActiveCodeValueRecords(Long codeSet) {
		logger.debug("Starting getCodeValueRecords for codeSet=" + codeSet);
		checkNotNull(codeSet);

		List<CodeValue> codeValues = codeValueCache.getCodeValuesByCodeSet(codeSet);
		List<CodeValueRecord> recordList = Lists.newArrayList();
		for (CodeValue cv : codeValues) {
			if (!cv.isActiveInd()) {
				continue;
			}
			recordList.add(makeCodeValueRecord(cv));
		}
		Collections.sort(recordList);
		return recordList;
	}

	private CodeValueRecord makeCodeValueRecord(CodeValue cv) {
		CodeValueRecord record = new CodeValueRecord(cv.getCodeValue(), cv.getDisplay(), cv.getCollationSeq());
		record.setCodeSet(cv.getCodeSet());
		record.setActive(cv.isActiveInd());
		record.setCdfMeaning(cv.getCdfMeaning());
		record.setUpdatedOn(cv.getUpdtDttm());
		return record;
	}

	protected List<Long> convertToCodeValues(List<CodeValueRecord> records) {
		if (records == null) {
			return null;
		}
		List<Long> codeValues = Lists.newArrayList();
		for (CodeValueRecord record : records) {
			codeValues.add(record.getCodeValue());
		}
		return codeValues;
	}

	/**
	 * Persists a long text record into the datastore and returns its
	 * identifier.
	 * 
	 * @param record
	 *            The record to persist.
	 * @return The identifier of the record.
	 */
	protected Long persistLongText(LongTextRecord record) {
		logger.debug("Starting persistLongText with record=" + record);
		throwIfInvalid(record);

		LongText longText;
		if (record.getLongTextId() == null) {
			longText = new LongText();
		} else {
			longText = longTextDao.findById(record.getLongTextId());
			if (longText == null) {
				logger.error("No long text row was found with long_text_id=" + record.getLongTextId()
						+ ". Instead inserting a new row.");
				longText = new LongText();
			}
		}

		longText.setFormatCd(getFormatCd(record.getFormat()));
		longText.setLongText(record.getLongText());

		longTextDao.persist(longText);
		return longText.getLongTextId();
	}

	/**
	 * Gets a long text record based on the long text identifier specified.
	 * 
	 * @param longTextId
	 *            The identifier of the long text to get.
	 * @return The long text record for the identifier specified, null if the
	 *         long text model could not be found.
	 */
	protected LongTextRecord getLongTextRecord(Long longTextId) {
		checkNotNull(longTextId);
		logger.debug("Starting getLongTextRecord for longTextId=" + longTextId);

		LongText longText = longTextDao.findById(longTextId);
		if (longText == null) {
			throw new IllegalArgumentException("The long text row could not be found.");
		}

		LongTextRecord record = new LongTextRecord();
		record.setLongTextId(longText.getLongTextId());
		record.setFormat(getTextFormat(longText.getFormatCd()));
		record.setLongText(longText.getLongText());
		return record;
	}

	/**
	 * Creates an entity map of long text models based on the primary keys
	 * supplied.
	 * 
	 * @param longTextIds
	 *            The long text identifiers.
	 * @return The entity map of loaded long text models.
	 */
	protected EntityMap<LongText> getLongTextMap(Set<Long> longTextIds) {
		checkNotNull(longTextIds);
		return EntityMap.newEntityMap(longTextDao.findByIdList(longTextIds));
	}

	/**
	 * Throws an illegal arugument exception if the validation results are not
	 * valid.
	 * 
	 * @param validateRecord
	 *            The results to validate.
	 */
	protected void throwIfInvalid(IValidateRecord validateRecord) {
		checkNotNull(validateRecord);
		ValidationResults result = validateRecord.validateRecord();
		checkNotNull(result);
		checkArgument(result.isValid(), result.convertToDisplayString());
	}

	protected void throwIfInvalid(Collection<? extends IValidateRecord> recordList) {
		checkNotNull(recordList);
		for (IValidateRecord record : recordList) {
			throwIfInvalid(record);
		}
	}

	protected Map<Long, Person> getPrsnlToPersonMap(Collection<Personnel> personnels) {
		checkNotNull(personnels);
		if (personnels.isEmpty()) {
			return Maps.newHashMap();
		}

		Set<Long> personIds = Sets.newHashSet();
		for (Personnel p : personnels) {
			personIds.add(p.getPersonId());
		}

		EntityMap<Person> personMap = EntityMap.newEntityMap(personDao.findByIdList(personIds));
		Map<Long, Person> map = Maps.newHashMap();
		for (Personnel personnel : personnels) {
			Person person = personMap.getEntity(personnel.getPersonId());
			map.put(personnel.getPersonnelId(), person);
		}
		return map;
	}

	/**
	 * Gets the corresponding code value relative to the text format. If the
	 * code value map has not been loaded with the text format code set then
	 * will query the database directly to obtain the code value.
	 * 
	 * @param format
	 *            The format to convert to a code value.
	 * @return The converted code value.
	 */
	private Long getFormatCd(TextFormat format) {
		String cdfMeaning;
		switch (format) {
		case HTML:
			cdfMeaning = HTML_CDF;
			break;
		case RTF:
			cdfMeaning = RTF_CDF;
			break;
		case PLAIN_TEXT:
			cdfMeaning = PLAIN_TEXT_CDF;
			break;
		default:
			logger.error("Unexpected text format type " + format + ". Defaulting to plain text.");
			cdfMeaning = PLAIN_TEXT_CDF;
			break;
		}
		CodeValue codeValue = codeValueCache.getCodeValue(TXT_FRMT_CDSET, cdfMeaning);
		if (codeValue == null) {
			logger.warn("Unable to find the specified text format code value");
			return 0L;
		}
		return codeValue.getCodeValue();
	}

	private TextFormat getTextFormat(Long formatCd) {
		CodeValue formatCodeValue = codeValueCache.getCodeValue(formatCd);
		if (formatCodeValue == null) {
			logger.warn("Format code value was not found.");
			return null;
		}

		final String cdfMeaning = formatCodeValue.getCdfMeaning();
		switch (cdfMeaning) {
		case "":
			return TextFormat.HTML;
		case RTF_CDF:
			return TextFormat.RTF;
		case PLAIN_TEXT_CDF:
			return TextFormat.PLAIN_TEXT;
		default:
			logger.error("Unexpected text format code value " + formatCd + ". Defaulting to plain text.");
			return TextFormat.PLAIN_TEXT;
		}
	}
}
