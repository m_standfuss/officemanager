package officemanager.biz.maps;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import officemanager.biz.EntityHelper;
import officemanager.data.models.CodeValue;
import officemanager.utility.CollectionUtility;
import officemanager.utility.Tuple;

/**
 * A class to store all the Code Values needed for class. With easy access
 * methods to find the code values needed.
 * 
 * @author Mike
 *
 */
public class CodeValueMap {
	private static final Logger logger = LogManager.getLogger(CodeValueMap.class);

	private final EntityMap<CodeValue> entityMap;
	private final Map<Long, List<CodeValue>> codeSetMap;
	private final Map<Tuple<Long, String>, List<CodeValue>> codeSetAndCdfMap;

	/**
	 * Creates a new empty map.
	 */
	public CodeValueMap() {
		entityMap = EntityMap.newEntityMap();
		codeSetMap = Maps.newHashMap();
		codeSetAndCdfMap = Maps.newHashMap();
	}

	/**
	 * Constructor allows for a batch initial upload.
	 * 
	 * @param codeValueList
	 *            The list to load into the map.
	 */
	public CodeValueMap(List<CodeValue> codeValueList) {
		this();
		addCodeValues(codeValueList);
	}

	/**
	 * Safely checks (without warnings or errors) if a code value has been
	 * loaded for or not.
	 * 
	 * @param codeSet
	 *            The code set of the code value to check for.
	 * @param cdfMeaning
	 *            The cdf meaning of the code value to check for.
	 * @return If the code value is available.
	 */
	public boolean hasCodeValue(Long codeSet, String cdfMeaning) {
		logger.debug("Checking if map has a code value in code set " + codeSet + " and a cdf meaning of " + cdfMeaning);
		List<CodeValue> codeValues = codeSetAndCdfMap.get(Tuple.newTuple(codeSet, cdfMeaning));
		return !CollectionUtility.isNullOrEmpty(codeValues);
	}

	/**
	 * Safely checks (without warnings or errors) if a code value has been
	 * loaded for or not.
	 * 
	 * @param codeValue
	 *            The code value to check for.
	 * @return If the code value is available.
	 */
	public boolean hasCodeValue(Long codeValue) {
		logger.debug("Checking if map has code value " + codeValue);
		return entityMap.getEntity(codeValue) != null;
	}

	/**
	 * Used for batch loading of code value list. Calls the add method for each
	 * of list.
	 * 
	 * @param codeValueList
	 *            The list to add.
	 */
	public void addCodeValues(List<CodeValue> codeValueList) {
		checkNotNull(codeValueList);
		checkArgument(!codeValueList.contains(null));
		for (final CodeValue cv : codeValueList) {
			add(cv);
		}
	}

	/**
	 * Adds a code value to the map.
	 * 
	 * @param codeValue
	 *            The code value to add to the map
	 */
	public void add(CodeValue codeValue) {
		logger.debug("Adding code value " + codeValue + " to the map.");
		checkNotNull(codeValue);
		entityMap.add(codeValue);

		if (!codeSetMap.containsKey(codeValue.getCodeSet())) {
			codeSetMap.put(codeValue.getCodeSet(), Lists.newArrayList(codeValue));
		} else {
			codeSetMap.get(codeValue.getCodeSet()).add(codeValue);
		}

		final Tuple<Long, String> key = new Tuple<Long, String>(codeValue.getCodeSet(), codeValue.getCdfMeaning());
		if (!codeSetAndCdfMap.containsKey(key)) {
			codeSetAndCdfMap.put(key, Lists.newArrayList(codeValue));
		} else {
			codeSetAndCdfMap.get(key).add(codeValue);
		}
	}

	/**
	 * Gets the {@link CodeValue} model for the given code value.
	 * 
	 * @param codeValue
	 *            The code value to load a model for.
	 * @return The code value model.
	 */
	public CodeValue getCodeValueModel(Long codeValue) {
		logger.debug("Starting getCodeValueModel for codeValue=" + codeValue);
		checkNotNull(codeValue);

		CodeValue codeValueModel = entityMap.getEntity(codeValue);
		if (codeValueModel == null) {
			logger.warn("The codeValue = " + codeValue
					+ ", could not be found on the map. Either it was not properly loaded for or it is missing from the data source.");
		}
		return codeValueModel;
	}

	/**
	 * Gets the {@link CodeValue} data models for the given code set.
	 * 
	 * @param codeSet
	 *            The code set to load a model for.
	 * @return The list of code value models for the set.
	 */
	public List<CodeValue> getCodeValueModels(Long codeSet) {
		logger.debug("Starting getCodeValueModels for codeSet=" + codeSet);
		checkNotNull(codeSet);

		List<CodeValue> codeValueModels = codeSetMap.get(codeSet);
		if (codeValueModels == null) {
			logger.warn("The codeSet = " + codeSet
					+ ", could not be found on the map. Either it was not properly loaded for or it is missing from the data source.");
		}
		return codeValueModels;
	}

	/**
	 * Returns a null safe display value for the code value. ie if not found or
	 * is null display returns "".
	 * 
	 * @param codeValue
	 *            The code value to get a display for.
	 * @return The display.
	 */
	public String getDisplay(Long codeValue) {
		checkNotNull(codeValue);
		CodeValue entity = entityMap.getEntity(codeValue);
		if (entity == null) {
			logger.warn("The codeValue = " + codeValue
					+ ", could not be found on the map. Either it was not properly loaded for or it is missing from the data source.");
			return "";
		}
		return makeNotNull(entity.getDisplay());
	}

	/**
	 * Returns a null safe display value for the code value. ie if not found or
	 * is null display returns "".
	 * 
	 * @param codeSet
	 *            The code set of the code value to get a display for.
	 * @param cdfMeaning
	 *            The cdf meaning of the code value to get a display for.
	 * @return The display.
	 */
	public String getDisplay(Long codeSet, String cdfMeaning) {
		Long codeValue = getCodeValue(codeSet, cdfMeaning);
		if (codeValue == null) {
			logger.warn(
					"The code value with code set=" + codeSet + ", and cdf meaning=" + cdfMeaning + " was not found.");
			return "";
		}
		return getDisplay(codeValue);
	}

	/**
	 * Returns the actual display value for the code value. ie if not found or
	 * null will return a null value.
	 * 
	 * @param codeValue
	 *            The code value to get a display for.
	 * @return The display.
	 */
	public String getDisplayNullable(Long codeValue) {
		checkNotNull(codeValue);
		CodeValue entity = entityMap.getEntity(codeValue);
		if (entity == null) {
			logger.warn("The codeValue = " + codeValue
					+ ", could not be found on the map. Either it was not properly loaded for or it is missing from the data source.");
			return null;
		}
		return entity.getDisplay();
	}

	/**
	 * Returns the actual display value for the code value. ie if not found or
	 * null will return a null value.
	 * 
	 * @param codeSet
	 *            The code set of the code value to get a display for.
	 * @param cdfMeaning
	 *            The cdf meaning of the code value to get a display for.
	 * @return The display.
	 */
	public String getDisplayNullable(Long codeSet, String cdfMeaning) {
		Long codeValue = getCodeValue(codeSet, cdfMeaning);
		if (codeValue == null) {
			logger.warn("The code value map does not contain a code value with codeSet=" + codeSet + " and cdfMeaning="
					+ cdfMeaning);
			return null;
		}
		return getDisplayNullable(codeValue);
	}

	/**
	 * Returns the code value for the code set and cdf meaning. Note if multiple
	 * code values exist for the same code set and cdf meaning then the first
	 * will be returned, and the method getCodeValueList should be called
	 * instead.
	 * 
	 * @param codeSet
	 *            The code set looking for.
	 * @param cdfMeaning
	 *            The cdf meaning looking for.
	 * @return The code value's code value if found, null if none found.
	 */
	public Long getCodeValue(Long codeSet, String cdfMeaning) {
		checkNotNull(codeSet);
		checkNotNull(cdfMeaning);
		final List<CodeValue> codeValues = codeSetAndCdfMap.get(new Tuple<Long, String>(codeSet, cdfMeaning));
		if (codeValues == null || codeValues.isEmpty()) {
			logger.warn("The code value map does not contain a code value for codeSet=" + codeSet + " and cdfMeaning="
					+ cdfMeaning);
			return null;
		}
		if (codeValues.size() > 1) {
			logger.warn("Multiple code values were found with the code set and cdf meaning looking for.");
		}
		return codeValues.get(0).getCodeValue();
	}

	/**
	 * Returns the code values for the code set and cdf meaning.
	 * 
	 * @param codeSet
	 *            The code set looking for.
	 * @param cdfMeaning
	 *            The cdf meaning looking for.
	 * @return The list of code value's code value if found, empty list if none
	 *         found.
	 */
	public List<Long> getCodeValueList(Long codeSet, String cdfMeaning) {
		checkNotNull(codeSet);
		checkNotNull(cdfMeaning);
		final List<CodeValue> codeValues = codeSetAndCdfMap.get(new Tuple<Long, String>(codeSet, cdfMeaning));
		if (codeValues == null || codeValues.isEmpty()) {
			logger.warn("The code value map does not contain a code value for codeSet=" + codeSet + " and cdfMeaning="
					+ cdfMeaning);
			return Lists.newArrayList();
		}
		return EntityHelper.getPrimaryKeyList(Long.class, codeValues);
	}

	/**
	 * Returns the code values for the code set and a list of cdf meanings.
	 * 
	 * @param codeSet
	 *            The cdf meaning looking for.
	 * @param cdfMeaningList
	 *            The list of cdf meanings looking for.
	 * @return The list of code value's code value if found, empty list if none
	 *         found.
	 */
	public List<Long> getCodeValueList(Long codeSet, List<String> cdfMeaningList) {
		checkNotNull(codeSet);
		checkNotNull(cdfMeaningList);

		final List<Long> allCodeValues = Lists.newArrayList();
		for (final String cdfMeaning : cdfMeaningList) {
			allCodeValues.addAll(getCodeValueList(codeSet, cdfMeaning));
		}
		return allCodeValues;
	}

	/**
	 * Gets a List of code values and their non null displays for a code set.
	 * 
	 * @param codeSet
	 *            The code set to get a list of code values and displays.
	 * @return The list of code values and displays. If no code values are found
	 *         then an empty list is returned.
	 */
	public List<Tuple<Long, String>> getActiveCodeValueAndDisplay(Long codeSet) {
		final List<CodeValue> codeValues = codeSetMap.get(codeSet);
		if (codeValues == null) {
			return Lists.newArrayList();
		}
		final List<Tuple<Long, String>> rtrn = Lists.newArrayList();
		for (final CodeValue cv : codeValues) {
			if (!cv.isActiveInd()) {
				continue;
			}
			rtrn.add(new Tuple<Long, String>(cv.getCodeValue(), makeNotNull(cv.getDisplay())));
		}
		return rtrn;
	}

	private String makeNotNull(String string) {
		return string == null ? "" : string;
	}
}
