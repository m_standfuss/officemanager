package officemanager.biz.maps;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.Calculator;
import officemanager.data.models.OrderToClientProduct;
import officemanager.data.models.OrderToFlatRateService;
import officemanager.data.models.OrderToPart;
import officemanager.data.models.Service;

public class OrderMap {
	private static final Logger logger = LogManager.getLogger(OrderMap.class);

	private final Collection<Long> orderIds;
	private Map<Long, List<OrderToClientProduct>> clientProductsMap;
	private Map<Long, List<OrderToFlatRateService>> orderFlatRatesMap;
	private Map<Long, List<OrderToPart>> orderPartsMap;
	private Map<Long, List<Service>> orderServicesMap;

	public OrderMap(Collection<Long> orderIds) {
		this.orderIds = orderIds;
	}

	public void setClientProductMap(Map<Long, List<OrderToClientProduct>> clientProductsMap) {
		this.clientProductsMap = clientProductsMap;
	}

	public void setOrderFlatRatesMap(Map<Long, List<OrderToFlatRateService>> orderFlatRatesMap) {
		this.orderFlatRatesMap = orderFlatRatesMap;
	}

	public void setOrderPartsMap(Map<Long, List<OrderToPart>> orderPartsMap) {
		this.orderPartsMap = orderPartsMap;
	}

	public void setOrderServicesMap(Map<Long, List<Service>> orderServicesMap) {
		this.orderServicesMap = orderServicesMap;
	}

	public BigDecimal getOrderTotal(Long orderId) {
		checkOrderId(orderId);
		return Calculator.calculateOrderTotal(orderFlatRatesMap.get(orderId), orderPartsMap.get(orderId),
				orderServicesMap.get(orderId));
	}

	/**
	 * Gets the list of client products for the order id.
	 * 
	 * @param orderId
	 *            The order id to get products for.
	 * @return The list of client products for the order, empty list if order
	 *         has none or was not loaded.
	 */
	public List<OrderToClientProduct> getClientProducts(Long orderId) {
		checkOrderId(orderId);
		checkNotNull(clientProductsMap);
		logger.debug("Getting the client products for orderId=" + orderId);
		if (clientProductsMap.get(orderId) != null) {
			return clientProductsMap.get(orderId);
		}
		logger.debug("The order id was not found on the client product map.");
		return Lists.newArrayList();
	}

	public List<OrderToFlatRateService> getFlatRateServices() {
		logger.debug("Starting getFlatRateServices for entire map.");
		List<OrderToFlatRateService> flatRates = Lists.newArrayList();
		for (Map.Entry<Long, List<OrderToFlatRateService>> entry : orderFlatRatesMap.entrySet()) {
			if (entry.getValue() != null) {
				flatRates.addAll(entry.getValue());
			}
		}
		return flatRates;
	}

	/**
	 * Gets the flat rate services for the order id.
	 * 
	 * @param orderId
	 *            The order id to get flat rate services for.
	 * @return The list of flat rate services
	 */
	public List<OrderToFlatRateService> getFlatRateServices(Long orderId) {
		checkOrderId(orderId);
		checkNotNull(orderFlatRatesMap);
		logger.debug("Getting the flat rate services for orderId=" + orderId);
		if (orderFlatRatesMap.get(orderId) != null) {
			return orderFlatRatesMap.get(orderId);
		}
		logger.warn("The order id was not found on the flat rate services map.");
		return Lists.newArrayList();
	}

	public List<OrderToPart> getParts() {
		logger.debug("Starting getParts for entire map.");
		List<OrderToPart> parts = Lists.newArrayList();
		for (Map.Entry<Long, List<OrderToPart>> entry : orderPartsMap.entrySet()) {
			if (entry.getValue() != null) {
				parts.addAll(entry.getValue());
			}
		}
		return parts;
	}

	/**
	 * Gets a list of parts for the order id.
	 * 
	 * @param orderId
	 *            The order id.
	 * @return The list of parts.
	 */
	public List<OrderToPart> getParts(Long orderId) {
		checkOrderId(orderId);
		checkNotNull(orderPartsMap);
		logger.debug("Getting the parts for orderId=" + orderId);
		if (orderPartsMap.get(orderId) != null) {
			return orderPartsMap.get(orderId);
		}
		logger.warn("The order id was not found on the part map.");
		return Lists.newArrayList();
	}

	public List<Service> getServices() {
		logger.debug("Starting getServices for entire map.");
		List<Service> flatRates = Lists.newArrayList();
		for (Map.Entry<Long, List<Service>> entry : orderServicesMap.entrySet()) {
			if (entry.getValue() != null) {
				flatRates.addAll(entry.getValue());
			}
		}
		return flatRates;
	}

	/**
	 * Gets a list of services for the order id.
	 * 
	 * @param orderId
	 *            The order id.
	 * @return The list of services.
	 */
	public List<Service> getServices(Long orderId) {
		checkOrderId(orderId);
		checkNotNull(orderServicesMap);
		logger.debug("Getting the services for orderId=" + orderId);
		if (orderServicesMap.get(orderId) != null) {
			return orderServicesMap.get(orderId);
		}
		logger.warn("The order id was not found on the service map.");
		return Lists.newArrayList();
	}

	/**
	 * Checks if the order id is null or was loaded. If not throws exception
	 * that way we do not give false results ie return an empty list of parts
	 * cause we didnt load the order when there is actually parts for that
	 * order.
	 * 
	 * @param orderId
	 *            The order id to check
	 * 
	 * @throws IllegalStateException
	 *             if the order has not been loaded for.
	 */
	private void checkOrderId(Long orderId) {
		checkNotNull(orderId);
		if (!orderIds.contains(orderId)) {
			throw new IllegalStateException("The orderId=" + orderId + " has not been loaded for.");
		}
	}
}
