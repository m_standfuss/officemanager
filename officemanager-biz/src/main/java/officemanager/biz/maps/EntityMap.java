package officemanager.biz.maps;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.google.common.collect.Maps;

import officemanager.data.models.OfficeManagerEntity;

/**
 * A map that automatically handles the mapping of the primary key to the entity
 * object. Provides a wrapper for the common HashMap that can handle
 * automatically loading any object that extends the OfficeManagerEntity
 * interface.
 *
 * @param <T>
 *            The type of entity to store on the map.
 */
public class EntityMap<T extends OfficeManagerEntity> implements Iterable<T> {

	private Map<Serializable, T> entityMap;

	public static <T extends OfficeManagerEntity> EntityMap<T> newEntityMap() {
		return new EntityMap<T>();
	}

	public static <T extends OfficeManagerEntity> EntityMap<T> newEntityMap(Collection<T> entities) {
		return new EntityMap<T>(entities);
	}

	/**
	 * Creates a new empty entity map.
	 */
	public EntityMap() {
		entityMap = Maps.newHashMap();
	}

	/**
	 * Creates a new entity map and populates it with the collection of entities
	 * by calling the loadMap method.
	 *
	 * @param entities
	 *            The entities to populate the map with
	 * @throws IllegalArgumentException
	 *             If the list is null or contains null.
	 */
	public EntityMap(Collection<T> entities) {
		this();
		load(entities);
	}

	/**
	 * Adds a single entity to the entity map.
	 *
	 * @param entity
	 *            The entity to add.
	 */
	public void add(T entity) {
		checkNotNull(entity);
		entityMap.put(entity.getPrimaryKey(), entity);
	}

	/**
	 * Populates the map with the supplied entities.
	 *
	 * @param entities
	 *            The entities to populate the map with
	 * @throws IllegalArgumentException
	 *             If the list is null or contains null.
	 */
	public void load(Collection<T> entities) {
		checkNotNull(entities);
		checkArgument(!entities.contains(null));
		for (final T entity : entities) {
			entityMap.put(entity.getPrimaryKey(), entity);
		}
	}

	/**
	 * Returns the mapped to entity for the id, null if none is found.
	 *
	 * @param id
	 *            The identifier of the entity looking to get.
	 * @return The entity if one such exists, null if not.
	 */
	public T getEntity(Serializable id) {
		return entityMap.get(id);
	}

	/**
	 * Removes all of the entries of the map. The map will be empty after this
	 * call returns.
	 */
	public void clear() {
		entityMap.clear();
	}

	/**
	 * Gets the collection representation of the entity map's values.
	 *
	 * @return The collection of values.
	 */
	public Collection<T> convertToCollection() {
		return entityMap.values();
	}

	@Override
	public String toString() {
		return "EntityMap [entityMap=" + entityMap + "]";
	}

	@Override
	public Iterator<T> iterator() {
		return entityMap.values().iterator();
	}
}
