package officemanager.biz.maps;

import java.util.Map;

import com.google.common.collect.Maps;

public class DisplayMap<T> {

	private final Map<T, String> map;

	public DisplayMap() {
		map = Maps.newHashMap();
	}

	public String put(T key, String value) {
		return map.put(key, value);
	}

	public String get(T key) {
		String value = map.get(key);
		return value == null ? "" : value;
	}

	public void clear() {
		map.clear();
	}
}
