package officemanager.biz.searchingcriteria;

import java.util.ArrayList;
import java.util.List;

import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class PartSearchCriteria implements SearchCriteria {

	public String partName;
	public String partDescription;
	public String partManufacturerId;

	public List<Long> partCategoryCds;
	public List<Long> productTypeCds;
	public List<Long> partManufacturerCds;

	public boolean onlyInStock;

	public PartSearchCriteria() {
		partName = null;
		partDescription = null;
		partManufacturerId = null;

		partCategoryCds = new ArrayList<Long>();
		productTypeCds = new ArrayList<Long>();
		partManufacturerCds = new ArrayList<Long>();
		onlyInStock = false;
	}

	@Override
	public boolean isEmpty() {
		if (!StringUtility.isNullOrWhiteSpace(partName)) {
			return false;
		}
		if (!StringUtility.isNullOrWhiteSpace(partDescription)) {
			return false;
		}
		if (!StringUtility.isNullOrWhiteSpace(partManufacturerId)) {
			return false;
		}
		if (!CollectionUtility.isNullOrEmpty(partCategoryCds)) {
			return false;
		}
		if (!CollectionUtility.isNullOrEmpty(productTypeCds)) {
			return false;
		}
		if (!CollectionUtility.isNullOrEmpty(partManufacturerCds)) {
			return false;
		}
		if (onlyInStock == true) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PartSearchCriteria [partName=" + partName + ", partDescription=" + partDescription
				+ ", partCategoryCds=" + partCategoryCds + ", productTypeCds=" + productTypeCds
				+ ", partManufacturerCds=" + partManufacturerCds + ", onlyInStock=" + onlyInStock + "]";
	}
}
