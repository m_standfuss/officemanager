package officemanager.biz.searchingcriteria;

import java.util.List;

import officemanager.biz.records.CodeValueRecord;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class LocationSearchCriteria implements SearchCriteria {

	public String display;
	public String abbrDisplay;
	public List<CodeValueRecord> locationTypeCds;

	@Override
	public boolean isEmpty() {
		return StringUtility.isNullOrWhiteSpace(display) && StringUtility.isNullOrWhiteSpace(abbrDisplay)
				&& CollectionUtility.isNullOrEmpty(locationTypeCds);
	}

	@Override
	public String toString() {
		return "LocationSearchCriteria [display=" + display + ", abbrDisplay=" + abbrDisplay + ", locationTypeCds="
				+ locationTypeCds + "]";
	}
}
