package officemanager.biz.searchingcriteria;

import java.util.Date;

public class SaleSearchCritieria implements SearchCriteria {

	public String saleName;
	public String saleItemName;
	public boolean amountOffSale;
	public boolean percentageOffSale;
	public Date begActiveDttm;
	public Date endActiveDttm;

	@Override
	public boolean isEmpty() {
		if (amountOffSale == false && percentageOffSale == false) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "SaleSearchCritieria [saleName=" + saleName + ", saleItemName=" + saleItemName + ", amountOffSale="
				+ amountOffSale + ", percentageOffSale=" + percentageOffSale + ", begActiveDttm=" + begActiveDttm
				+ ", endActiveDttm=" + endActiveDttm + "]";
	}
}
