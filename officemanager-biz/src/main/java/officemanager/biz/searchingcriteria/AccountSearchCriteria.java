package officemanager.biz.searchingcriteria;

public class AccountSearchCriteria implements SearchCriteria {

	public Long accountId;
	public PersonSearchCriteria personSearchCriteria;

	@Override
	public boolean isEmpty() {
		return accountId == null && (personSearchCriteria == null || personSearchCriteria.isEmpty());
	}

	@Override
	public String toString() {
		return "AccountSearchCriteria [accountId=" + accountId + ", personSearchCriteria=" + personSearchCriteria + "]";
	}
}
