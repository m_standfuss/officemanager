package officemanager.biz.searchingcriteria;

/**
 * Interface for searching criteria to implement to making verifying the search
 * against.
 * 
 * @author Mike
 *
 */
interface SearchCriteria {

	/**
	 * Defines if the searching criteria has anything to search on. If this
	 * returns true then instead of actually querying the data source an empty
	 * result will be returned.
	 * 
	 * @return A boolean value if the search should be preformed or not.
	 */
	boolean isEmpty();
}
