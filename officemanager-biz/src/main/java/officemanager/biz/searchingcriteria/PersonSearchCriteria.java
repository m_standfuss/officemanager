package officemanager.biz.searchingcriteria;

import java.util.List;

public class PersonSearchCriteria implements SearchCriteria {

	public String nameFirst;
	public String nameLast;
	public String primaryPhone;

	public PersonSearchCriteria() {
		this(null, null, null, null);
	}

	public PersonSearchCriteria(String nameFirst, String nameLast, String primaryPhone) {
		this(null, nameFirst, nameLast, primaryPhone);
	}

	public PersonSearchCriteria(List<Long> personIds, String nameFirst, String nameLast, String primaryPhone) {
		this.nameFirst = nameFirst;
		this.nameLast = nameLast;
		this.primaryPhone = primaryPhone;
	}

	@Override
	public boolean isEmpty() {
		return (nameFirst == null || nameFirst.isEmpty())
				&& (nameLast == null || nameLast.isEmpty() && (primaryPhone == null || primaryPhone.isEmpty()));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PersonSearchCriteria [nameFirst=").append(nameFirst).append(", nameLast=").append(nameLast)
		.append(", primaryPhone=").append(primaryPhone).append("]");
		return builder.toString();
	}
}
