package officemanager.biz.searchingcriteria;

import java.util.List;

import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class FlatRateSearchCriteria implements SearchCriteria {

	public String serviceName;

	public List<Long> serviceCategoryCds;
	public List<Long> productTypeCds;

	public boolean onlyOnSale;

	public FlatRateSearchCriteria() {
		serviceName = null;
		onlyOnSale = false;
		serviceCategoryCds = null;
		productTypeCds = null;
	}

	@Override
	public boolean isEmpty() {
		if (!StringUtility.isNullOrWhiteSpace(serviceName)) {
			return false;
		}
		if (!CollectionUtility.isNullOrEmpty(serviceCategoryCds)) {
			return false;
		}
		if (!CollectionUtility.isNullOrEmpty(productTypeCds)) {
			return false;
		}
		if (onlyOnSale) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FlatRateSearchCriteria [serviceName=" + serviceName + ", serviceCategoryCds=" + serviceCategoryCds
				+ ", productTypeCds=" + productTypeCds + ", onlyOnSale=" + onlyOnSale + "]";
	}

}
