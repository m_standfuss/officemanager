package officemanager.biz.searchingcriteria;

import java.util.List;

import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class PersonnelSearchCriteria extends PersonSearchCriteria {

	public String username;
	public List<Long> personnelTypeCds;
	public List<Long> personnelStatusCds;

	public boolean isPersonSearchEmpty() {
		return super.isEmpty();
	}

	@Override
	public boolean isEmpty() {
		if (super.isEmpty()) {
			return StringUtility.isNullOrWhiteSpace(username) && CollectionUtility.isNullOrEmpty(personnelTypeCds)
					&& CollectionUtility.isNullOrEmpty(personnelStatusCds);
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PersonnelSearchCriteria [username=").append(username).append(", personnelTypeCds=")
				.append(personnelTypeCds).append(", personnelStatusCds=").append(personnelStatusCds)
				.append(", toString()=").append(super.toString()).append("]");
		return builder.toString();
	}
}
