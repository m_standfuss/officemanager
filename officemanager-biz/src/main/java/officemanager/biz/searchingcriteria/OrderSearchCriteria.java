package officemanager.biz.searchingcriteria;

import java.util.Date;

/**
 * Searching criteria for the Orders table. Any field that is null is left out
 * of the query.
 *
 */
public class OrderSearchCriteria implements SearchCriteria {

	public Long orderId = null;

	public Date begOpenedDttm = null;
	public Date endOpenedDttm = null;
	public Date begClosedDttm = null;
	public Date endClosedDttm = null;

	public Long openedPrsnlId = null;
	public Long closedPrsnlId = null;

	public Long clientId = null;

	public PersonSearchCriteria personSearchCriteria = null;

	@Override
	public boolean isEmpty() {
		return clientId == null && orderId == null && begOpenedDttm == null && endOpenedDttm == null
				&& begClosedDttm == null && endClosedDttm == null && openedPrsnlId == null && closedPrsnlId == null
				&& (personSearchCriteria == null || personSearchCriteria.isEmpty());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrderSearchCriteria [orderId=").append(orderId).append(", begOpenedDttm=").append(begOpenedDttm)
				.append(", endOpenedDttm=").append(endOpenedDttm).append(", begClosedDttm=").append(begClosedDttm)
				.append(", endClosedDttm=").append(endClosedDttm).append(", openedPrsnlId=").append(openedPrsnlId)
				.append(", closedPrsnlId=").append(closedPrsnlId).append(", clientId=").append(clientId)
				.append(", personSearchCriteria=").append(personSearchCriteria).append("]");
		return builder.toString();
	}
}
