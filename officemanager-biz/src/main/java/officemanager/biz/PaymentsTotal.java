package officemanager.biz;

import java.math.BigDecimal;

public class PaymentsTotal {
	public final BigDecimal total;
	public final BigDecimal cashTotal;
	public final BigDecimal checkTotal;
	public final BigDecimal creditCardTotal;
	public final BigDecimal clientCreditTotal;
	public final BigDecimal pendingTotal;
	public final BigDecimal giftCardTotal;
	public final BigDecimal financingTotal;
	public final BigDecimal tradeInTotal;
	public final BigDecimal refundTotal;

	public PaymentsTotal(BigDecimal total, BigDecimal cashTotal, BigDecimal checkTotal, BigDecimal creditCardTotal,
			BigDecimal clientCreditTotal, BigDecimal pendingTotal, BigDecimal giftCardTotal, BigDecimal financingTotal,
			BigDecimal tradeInTotal, BigDecimal refundTotal) {
		this.total = total;
		this.cashTotal = cashTotal;
		this.checkTotal = checkTotal;
		this.creditCardTotal = creditCardTotal;
		this.clientCreditTotal = clientCreditTotal;
		this.pendingTotal = pendingTotal;
		this.giftCardTotal = giftCardTotal;
		this.financingTotal = financingTotal;
		this.tradeInTotal = tradeInTotal;
		this.refundTotal = refundTotal;
	}

}
