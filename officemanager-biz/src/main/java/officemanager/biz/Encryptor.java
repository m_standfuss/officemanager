package officemanager.biz;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.salt.StringFixedSaltGenerator;

/**
 * Handles all encrypting logic for the application.
 * 
 * @author Mike
 *
 */
public class Encryptor {

	private static Logger logger = LogManager.getLogger(Encryptor.class);
	private static final String SALT = "OfficeManagerApplication";
	private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

	public final StandardPBEStringEncryptor encryptor;

	public Encryptor(String password) {
		encryptor = new StandardPBEStringEncryptor();
		encryptor.setAlgorithm("PBEWithMD5AndDES");
		encryptor.setSaltGenerator(new StringFixedSaltGenerator(SALT, "UTF-8"));
		encryptor.setPassword(password);
	}

	public boolean matches(String encrypted, String decrypted) {
		return stringsMatch(encrypted, decrypted);
	}

	public boolean matches(byte[] encrypted, String decrypted) {
		return stringsMatch(new String(encrypted, DEFAULT_CHARSET), decrypted);
	}

	public boolean matches(byte[] encrypted, byte[] decrypted) {
		return stringsMatch(new String(encrypted, DEFAULT_CHARSET), new String(decrypted, DEFAULT_CHARSET));
	}

	public boolean matches(String encrypted, byte[] decrypted) {
		return stringsMatch(encrypted, new String(decrypted, DEFAULT_CHARSET));
	}

	public String encryptToString(String toEncrypt) {
		return encrypt(toEncrypt);
	}

	public String encryptToString(byte[] toEncrypt) {
		return encrypt(new String(toEncrypt, DEFAULT_CHARSET));
	}

	public byte[] encryptToBytes(String toEncrypt) {
		return encrypt(toEncrypt).getBytes(DEFAULT_CHARSET);
	}

	public byte[] encryptToBytes(byte[] toEncrypt) {
		return encrypt(new String(toEncrypt, DEFAULT_CHARSET)).getBytes(DEFAULT_CHARSET);
	}

	public String decryptToString(String toDecrypt) {
		return decrypt(toDecrypt);
	}

	public String decryptToString(byte[] toDecrypt) {
		return decrypt(new String(toDecrypt, DEFAULT_CHARSET));
	}

	public byte[] decryptToBytes(byte[] toDecrypt) {
		return decrypt(new String(toDecrypt, DEFAULT_CHARSET)).getBytes(DEFAULT_CHARSET);
	}

	public byte[] decryptToBytes(String toDecrypt) {
		return decrypt(toDecrypt).getBytes(DEFAULT_CHARSET);
	}

	private boolean stringsMatch(String encrypted, String decrypted) {
		logger.debug("Starting match.");
		byte[] toMatch = encryptToBytes(decrypted);
		byte[] toMatchAgainst = encrypted.getBytes(DEFAULT_CHARSET);
		return compareBytes(toMatch, toMatchAgainst);
	}

	private String decrypt(String toDecrypt) {
		logger.debug("Starting decrypt.");
		return encryptor.decrypt(toDecrypt);
	}

	private String encrypt(String toEncrypt) {
		logger.debug("Starting encrypt.");
		return encryptor.encrypt(toEncrypt);
	}

	private boolean compareBytes(byte[] toMatch, byte[] toMatchAgainst) {
		if (toMatch.length != toMatchAgainst.length) {
			return false;
		}
		for (int i = 0; i < toMatch.length; i++) {
			if (toMatch[i] != toMatchAgainst[i]) {
				return false;
			}
		}
		return true;
	}
}
