package officemanager.biz.records;

public class PhoneRecord extends Record implements IValidateRecord {
	private final Long phoneId;
	private Long personId;
	private boolean primaryPhone;
	private String extension;
	private String phoneNumber;
	private CodeValueRecord phoneType;

	public PhoneRecord() {
		this((Long) null);
	}

	public PhoneRecord(Long phoneId) {
		this.phoneId = phoneId;
	}

	public PhoneRecord(Long phoneId, PhoneRecord toCopy) {
		this(phoneId);
		copyConstructor(toCopy);
	}

	public PhoneRecord(PhoneRecord toCopy) {
		phoneId = toCopy.phoneId;
		copyConstructor(toCopy);
	}

	private void copyConstructor(PhoneRecord toCopy) {
		personId = toCopy.personId;
		primaryPhone = toCopy.primaryPhone;
		extension = toCopy.extension;
		phoneNumber = toCopy.phoneNumber;
		phoneType = new CodeValueRecord(toCopy.phoneType);
	}

	public Long getPhoneId() {
		return phoneId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public boolean isPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(boolean primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getExtension() {
		return makeNotNull(extension);
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getPhoneNumber() {
		return makeNotNull(phoneNumber);
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public CodeValueRecord getPhoneType() {
		return makeNotNull(phoneType);
	}

	public void setPhoneType(CodeValueRecord phoneType) {
		this.phoneType = phoneType;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(phoneNumber, "phone number"));
		results.addResults(validateCodeValueSet(phoneType, "phone type"));
		results.addResults(validateStringLength(phoneNumber, "phone number", 20, null));
		results.addResults(validateStringLength(extension, "phone number", 15, null));
		return results;
	}

}
