package officemanager.biz.records;

import java.util.List;

import com.google.common.collect.Lists;

public class RoleRecord extends Record implements IValidateRecord {

	private Long roleId;
	private String roleName;
	private List<RolePermissionRecord> permissionRecords;

	public RoleRecord() {}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return makeNotNull(roleName);
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<RolePermissionRecord> getPermissionRecords() {
		return permissionRecords == null ? Lists.newArrayList() : permissionRecords;
	}

	public void setPermissionRecords(List<RolePermissionRecord> permissionRecords) {
		this.permissionRecords = permissionRecords;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateStringIsSet(roleName, "role name"));
		results.addResults(validateStringLength(roleName, "role name", 255, null));
		return results;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RoleRecord [roleId=").append(roleId).append(", roleName=").append(roleName)
		.append(", permissionRecords=").append(permissionRecords).append("]");
		return builder.toString();
	}
}
