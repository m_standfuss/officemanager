package officemanager.biz.records;

import java.util.Date;

public class CodeValueRecord extends Record implements IValidateRecord, Comparable<CodeValueRecord> {

	private Long codeValue;
	private Long codeSet;
	private String display;
	private Short collationSeq;
	private boolean active;
	private String cdfMeaning;
	private Date updatedOn;

	public CodeValueRecord() {}

	public CodeValueRecord(Long codeValue, String display, Short collationSeq) {
		this.codeValue = codeValue;
		this.display = display;
		this.collationSeq = collationSeq;
	}

	public CodeValueRecord(CodeValueRecord toCopy) {
		this(toCopy.getCodeValue(), toCopy.getDisplay(), toCopy.collationSeq);
		setActive(toCopy.isActive());
		setCodeSet(toCopy.getCodeSet());
		setCdfMeaning(toCopy.getCdfMeaning());
		setUpdatedOn(toCopy.getUpdatedOn());
	}

	public Long getCodeSet() {
		return codeSet;
	}

	public void setCodeSet(Long codeSet) {
		this.codeSet = codeSet;
	}

	public void setCodeValue(Long codeValue) {
		this.codeValue = codeValue;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public void setCollationSeq(Short collationSeq) {
		this.collationSeq = collationSeq;
	}

	public Long getCodeValue() {
		return codeValue;
	}

	public String getDisplay() {
		return makeNotNull(display);
	}

	public Short getCollatingSequence() {
		return collationSeq;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCdfMeaning() {
		return makeNotNull(cdfMeaning);
	}

	public void setCdfMeaning(String cdfMeaning) {
		this.cdfMeaning = cdfMeaning;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public int compareTo(CodeValueRecord other) {
		if (this == other) {
			return 0;
		}

		if (collationSeq == null) {
			if (other.collationSeq == null) {
				return getDisplay().compareTo(other.getDisplay());
			}
			return 1;
		} else if (other.collationSeq == null) {
			return -1;
		}

		// primitive numbers follow this form
		if (collationSeq < other.collationSeq) {
			return -1;
		}
		if (collationSeq > other.collationSeq) {
			return 1;
		}
		return getDisplay().compareTo(other.getDisplay());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codeValue == null) ? 0 : codeValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CodeValueRecord other = (CodeValueRecord) obj;
		if (codeValue == null) {
			if (other.codeValue != null) {
				return false;
			}
		} else if (!codeValue.equals(other.codeValue)) {
			return false;
		}
		return true;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(codeSet, "code set"));
		results.addResults(validateStringLength(display, "display", 50, null));
		return results;
	}
}
