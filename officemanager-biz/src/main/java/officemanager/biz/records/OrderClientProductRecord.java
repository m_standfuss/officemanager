package officemanager.biz.records;

import static com.google.common.base.Preconditions.checkNotNull;

public class OrderClientProductRecord extends Record implements IValidateRecord {

	private Long orderToClientProductId;
	private Long clientProductId;
	private String productManufacturer;
	private String productModel;
	private String productType;
	private String serialNumber;

	public OrderClientProductRecord() {}

	public OrderClientProductRecord(ClientProductSelectionRecord clientProduct) {
		checkNotNull(clientProduct);
		clientProductId = clientProduct.getClientProductId();
		productManufacturer = clientProduct.getProductManufacturer();
		productModel = clientProduct.getProductModel();
		productType = clientProduct.getProductType();
		serialNumber = clientProduct.getSerialNumber();
	}

	public Long getOrderToClientProductId() {
		return orderToClientProductId;
	}

	public void setOrderToClientProductId(Long orderToClientProductId) {
		this.orderToClientProductId = orderToClientProductId;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public String getProductManufacturer() {
		return makeNotNull(productManufacturer);
	}

	public void setProductManufacturer(String productManufacturer) {
		this.productManufacturer = productManufacturer;
	}

	public String getProductModel() {
		return makeNotNull(productModel);
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getProductType() {
		return makeNotNull(productType);
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getSerialNumber() {
		return makeNotNull(serialNumber);
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(clientProductId, "client product"));
		return results;
	}
}
