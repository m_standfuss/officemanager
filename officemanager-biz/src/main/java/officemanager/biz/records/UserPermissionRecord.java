package officemanager.biz.records;

import java.util.Date;

public class UserPermissionRecord extends Record {

	private final String permissionName;
	private final Date endEffectiveDttm;

	public UserPermissionRecord(String permissionName, Date endEffectiveDttm) {
		this.permissionName = permissionName;
		this.endEffectiveDttm = endEffectiveDttm;
	}

	public String getPermissionName() {
		return makeNotNull(permissionName);
	}

	public Date getEndEffectiveDttm() {
		return endEffectiveDttm;
	}

}
