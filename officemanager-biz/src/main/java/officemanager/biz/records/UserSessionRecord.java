package officemanager.biz.records;

/**
 * Stores the session information for a login attempt into the application.
 * 
 * @author Mike
 *
 */
public class UserSessionRecord {

	public final String username;
	public final Long prsnlId;
	public final String nameFirst;
	public final String nameFullFormatted;
	public final AuthStatus authStatus;

	public UserSessionRecord(String username, AuthStatus authStatus, String nameFirst, String nameFullFormatted,
			Long prsnlId) {
		this.username = username;
		this.nameFirst = nameFirst;
		this.nameFullFormatted = nameFullFormatted;
		this.authStatus = authStatus;
		this.prsnlId = prsnlId;
	}

	public UserSessionRecord(String username, AuthStatus authStatus) {
		this.username = username;
		this.authStatus = authStatus;
		prsnlId = null;
		nameFirst = null;
		nameFullFormatted = null;
	}

	/**
	 * Clones a UserSessionRecord but changes the auth status. Used when logging
	 * in but redirecting to set/reset password.
	 * 
	 * @param toClone
	 *            The session to clone.
	 * @param authStatus
	 *            The new status.
	 */
	public UserSessionRecord(UserSessionRecord toClone, AuthStatus authStatus) {
		username = toClone.username;
		nameFirst = toClone.nameFirst;
		nameFullFormatted = toClone.nameFullFormatted;
		prsnlId = toClone.prsnlId;
		this.authStatus = authStatus;
	}

	public enum AuthStatus {
		SUCCESSFUL, PASSWORD_NEEDS_SET, PASSWORD_NEEDS_RESET, USER_LOCKED_OUT, INVALID_CREDS;
	}

	@Override
	public String toString() {
		return "UserSessionRecord [username=" + username + ", prsnlId=" + prsnlId + ", nameFirst=" + nameFirst
				+ ", nameFullFormatted=" + nameFullFormatted + ", authStatus=" + authStatus + "]";
	}
}
