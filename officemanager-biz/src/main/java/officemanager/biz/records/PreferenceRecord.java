package officemanager.biz.records;

import java.math.BigDecimal;

public class PreferenceRecord extends Record implements IValidateRecord {

	public enum PrefType {
		HOURLY_RATE, HOURLY_RATE_DISCOUNT, TAX_RATE, COMPANY_PHONE, COMPANY_ADDRESS, COMPANY_WEBSITE, RECIEPT_CMMT_TYPE, SHOP_TIX_CMMT_TYPE, CLAIM_TIX_CMMT_TYPE
	}

	private Long preferenceId;
	private Long prefPrsnlId;
	private PrefType prefType;
	private String prefValueText;
	private Long prefValueNumber;
	private Boolean prefValueBoolean;
	private BigDecimal prefValueDecimal;

	@Override
	public ValidationResults validateRecord() {
		return new ValidationResults().addResults(validateNotNull(prefType, "preference type"))
				.addResults(validateStringLength(prefValueText, "preference text", 255, null));
	}

	public Long getPreferenceId() {
		return preferenceId;
	}

	public void setPreferenceId(Long preferenceId) {
		this.preferenceId = preferenceId;
	}

	public Long getPrefPrsnlId() {
		return prefPrsnlId;
	}

	public void setPrefPrsnlId(Long prefPrsnlId) {
		this.prefPrsnlId = prefPrsnlId;
	}

	public PrefType getPrefType() {
		return prefType;
	}

	public void setPrefType(PrefType prefType) {
		this.prefType = prefType;
	}

	public String getPrefValueText() {
		return makeNotNull(prefValueText);
	}

	public void setPrefValueText(String prefValueText) {
		this.prefValueText = prefValueText;
	}

	public Long getPrefValueNumber() {
		return prefValueNumber;
	}

	public void setPrefValueNumber(Long prefValueNumber) {
		this.prefValueNumber = prefValueNumber;
	}

	public Boolean isPrefValueBoolean() {
		return prefValueBoolean;
	}

	public void setPrefValueBoolean(Boolean prefValueBoolean) {
		this.prefValueBoolean = prefValueBoolean;
	}

	public BigDecimal getPrefValueDecimal() {
		return prefValueDecimal;
	}

	public void setPrefValueDecimal(BigDecimal prefValueDecimal) {
		this.prefValueDecimal = prefValueDecimal;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PreferenceRecord [preferenceId=").append(preferenceId).append(", prefPrsnlId=")
				.append(prefPrsnlId).append(", prefType=").append(prefType).append(", prefValueText=")
				.append(prefValueText).append(", prefValueNumber=").append(prefValueNumber)
				.append(", prefValueBoolean=").append(prefValueBoolean).append(", prefValueDecimal=")
				.append(prefValueDecimal).append("]");
		return builder.toString();
	}

}