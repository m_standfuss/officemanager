package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.Date;

public class PaymentRecord extends Record implements IValidateRecord {

	public enum PaymentType {
		CHECK, CLIENT_CREDIT, CREDIT_CARD, FINANCING, GIFT_CARD, CASH, PENDING, TRADE_IN, REFUND
	}

	private Long paymentId;
	private Long accountId;
	private BigDecimal amount;
	private Date createdDtTm;
	private String paymentDescription;
	private String paymentNumber;
	private PaymentType paymentType;
	private String paymentTypeDisplay;

	public PaymentRecord() {}

	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	public String getPaymentDescription() {
		return makeNotNull(paymentDescription);
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public String getPaymentNumber() {
		return makeNotNull(paymentNumber);
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentTypeDisplay() {
		return makeNotNull(paymentTypeDisplay);
	}

	public void setPaymentTypeDisplay(String paymentTypeDisplay) {
		this.paymentTypeDisplay = paymentTypeDisplay;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(amount, "amount"));
		results.addResults(validateNotNull(paymentType, "payment type"));
		switch (paymentType) {
		case CHECK:
			results.addResults(validateStringIsSet(paymentNumber, "check number"));
			break;
		case CLIENT_CREDIT:
			results.addResults(validateNotNull(accountId, "account for client credit"));
			break;
		case CREDIT_CARD:
			results.addResults(validateStringIsSet(paymentNumber, "card number"));
			results.addResults(validateStringIsSet(paymentDescription, "card company"));
			break;
		case FINANCING:
			results.addResults(validateStringIsSet(paymentDescription, "financing company"));
			break;
		case GIFT_CARD:
			results.addResults(validateStringIsSet(paymentNumber, "gift card number"));
			break;
		default:
			break;
		}
		results.addResults(validateStringLength(paymentNumber, "payment number", 50, null));
		results.addResults(validateStringLength(paymentNumber, "payment description", 50, null));
		return results;
	}

	@Override
	public String toString() {
		return "PaymentRecord [paymentId=" + paymentId + ", accountId=" + accountId + ", amount=" + amount
				+ ", createdDtTm=" + createdDtTm + ", paymentDescription=" + paymentDescription + ", paymentNumber="
				+ paymentNumber + ", paymentType=" + paymentType + ", paymentTypeDisplay=" + paymentTypeDisplay + "]";
	}
}
