package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.Date;

public class TradeInRecord extends Record implements IValidateRecord {

	private Long tradeInId;
	private String manufacturer;
	private String model;
	private BigDecimal amount;
	private Long paymentId;
	private String description;
	private CodeValueRecord tradeInType;
	private CodeValueRecord productTypeCd;
	private Long clientId;
	private Date createdDttm;
	private Long createPrsnlId;

	public Long getTradeInId() {
		return tradeInId;
	}

	public void setTradeInId(Long tradeInId) {
		this.tradeInId = tradeInId;
	}

	public String getManufacturer() {
		return makeNotNull(manufacturer);
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return makeNotNull(model);
	}

	public void setModel(String model) {
		this.model = model;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	public String getDescription() {
		return makeNotNull(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CodeValueRecord getTradeInType() {
		return makeNotNull(tradeInType);
	}

	public void setTradeInType(CodeValueRecord tradeInType) {
		this.tradeInType = tradeInType;
	}

	public CodeValueRecord getProductTypeCd() {
		return makeNotNull(productTypeCd);
	}

	public void setProductTypeCd(CodeValueRecord productTypeCd) {
		this.productTypeCd = productTypeCd;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Date getCreatedDttm() {
		return createdDttm;
	}

	public void setCreatedDttm(Date createdDttm) {
		this.createdDttm = createdDttm;
	}

	public Long getCreatePrsnlId() {
		return createPrsnlId;
	}

	public void setCreatePrsnlId(Long createPrsnlId) {
		this.createPrsnlId = createPrsnlId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TradeInRecord [tradeInId=").append(tradeInId).append(", manufacturer=").append(manufacturer)
				.append(", model=").append(model).append(", amount=").append(amount).append(", paymentId=")
				.append(paymentId).append(", description=").append(description).append(", tradeInType=")
				.append(tradeInType).append(", productTypeCd=").append(productTypeCd).append(", clientId=")
				.append(clientId).append(", createdDttm=").append(createdDttm).append(", createPrsnlId=")
				.append(createPrsnlId).append("]");
		return builder.toString();
	}

	@Override
	public ValidationResults validateRecord() {
		return validateCodeValueSet(tradeInType, "trade in type").addResults(validateNotNull(clientId, "client id"))
				.addResults(validateStringIsSet(manufacturer, "manufacturer"))
				.addResults(validateStringLength(manufacturer, "manufacturer", 50, null))
				.addResults(validateStringLength(model, "model", 50, null))
				.addResults(validateStringLength(description, "description", 80, null))
				.addResults(validateIsPositiveOrZero(amount, "amount"));
	}
}
