package officemanager.biz.records;

import java.util.Date;

public class PersonnelRoleRecord extends Record implements IValidateRecord {

	private Long personnelRoleId;
	private Long personnelId;
	private Long roleId;
	private String roleName;
	private Date endEffectiveDttm;

	public Long getPersonnelRoleId() {
		return personnelRoleId;
	}

	public void setPersonnelRoleId(Long personnelRoleId) {
		this.personnelRoleId = personnelRoleId;
	}

	public Long getPersonnelId() {
		return personnelId;
	}

	public void setPersonnelId(Long personnelId) {
		this.personnelId = personnelId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return makeNotNull(roleName);
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Date getEndEffectiveDttm() {
		return endEffectiveDttm;
	}

	public void setEndEffectiveDttm(Date endEffectiveDttm) {
		this.endEffectiveDttm = endEffectiveDttm;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(personnelId, "personnel id"));
		results.addResults(validateNotNull(roleId, "role id"));
		return results;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PersonnelRoleRecord [personnelRoleId=").append(personnelRoleId).append(", personnelId=")
		.append(personnelId).append(", roleId=").append(roleId).append(", roleName=").append(roleName)
		.append(", endEffectiveDttm=").append(endEffectiveDttm).append("]");
		return builder.toString();
	}
}
