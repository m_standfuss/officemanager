package officemanager.biz.records;

import java.util.List;

import com.google.common.collect.Lists;

public class PersonRecord extends Record implements IValidateRecord {

	private final Long personId;
	private String nameLast;
	private String nameFirst;
	private String nameFullFormatted;
	private String email;

	private final List<PhoneRecord> phoneRecords;
	private final List<AddressRecord> addressRecords;

	/**
	 * Creates a new person record.
	 */
	public PersonRecord() {
		this((Long) null);
	}

	/**
	 * Creates a person record that corresponds to an existing person record.
	 * 
	 * @param personId
	 *            The identifier of the person record.
	 */
	public PersonRecord(Long personId) {
		this.personId = personId;
		phoneRecords = Lists.newArrayList();
		addressRecords = Lists.newArrayList();
	}

	/**
	 * Copy constructor, creates a local copy of each field passed in.
	 * 
	 * @param toCopy
	 *            The person record to copy.
	 */
	public PersonRecord(PersonRecord toCopy) {
		personId = toCopy.personId;
		nameLast = toCopy.nameLast;
		nameFirst = toCopy.nameFirst;
		nameFullFormatted = toCopy.nameFullFormatted;
		email = toCopy.email;

		addressRecords = Lists.newArrayList();
		for (AddressRecord address : toCopy.addressRecords) {
			addressRecords.add(new AddressRecord(address));
		}

		phoneRecords = Lists.newArrayList();
		for (PhoneRecord phone : toCopy.phoneRecords) {
			phoneRecords.add(new PhoneRecord(phone));
		}
	}

	public Long getPersonId() {
		return personId;
	}

	public String getNameLast() {
		return makeNotNull(nameLast);
	}

	public void setNameLast(String nameLast) {
		this.nameLast = nameLast;
	}

	public String getNameFirst() {
		return makeNotNull(nameFirst);
	}

	public void setNameFirst(String nameFirst) {
		this.nameFirst = nameFirst;
	}

	public String getNameFullFormatted() {
		return makeNotNull(nameFullFormatted);
	}

	public void setNameFullFormatted(String nameFullFormatted) {
		this.nameFullFormatted = nameFullFormatted;
	}

	public String getEmail() {
		return makeNotNull(email);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public PhoneRecord getPrimaryPhone() {
		if (phoneRecords != null) {
			for (PhoneRecord phone : phoneRecords) {
				if (phone.isPrimaryPhone()) {
					return phone;
				}
			}
		}
		return null;
	}

	public List<PhoneRecord> getPhoneRecords() {
		return phoneRecords;
	}

	public AddressRecord getPrimaryAddress() {
		if (addressRecords != null) {
			for (AddressRecord address : addressRecords) {
				if (address.isPrimaryAddress()) {
					return address;
				}
			}
		}
		return null;
	}

	public List<AddressRecord> getAddressRecords() {
		return addressRecords;
	}

	@Override
	public ValidationResults validateRecord() {
		final ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(nameLast, "last name"));
		results.addResults(validateStringLength(nameLast, "last name", 50, null));
		results.addResults(validateStringLength(nameFirst, "first name", 30, null));
		results.addResults(validateStringLength(nameFullFormatted, "name fully formatted", 80, null));
		results.addResults(validateStringLength(email, "email", 50, null));
		for (PhoneRecord phone : phoneRecords) {
			results.addResults(phone.validateRecord());
		}
		for (AddressRecord address : addressRecords) {
			results.addResults(address.validateRecord());
		}
		return results;
	}
}
