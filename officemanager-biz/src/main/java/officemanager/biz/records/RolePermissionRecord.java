package officemanager.biz.records;

public class RolePermissionRecord extends Record implements IValidateRecord, Comparable<RolePermissionRecord> {
	private Long rolePermissionId;
	private Long roleId;
	private String permissionName;
	private String permissionDisplay;

	public RolePermissionRecord() {}

	public Long getRolePermissionId() {
		return rolePermissionId;
	}

	public void setRolePermissionId(Long rolePermissionId) {
		this.rolePermissionId = rolePermissionId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getPermissionDisplay() {
		return permissionDisplay;
	}

	public void setPermissionDisplay(String permissionDisplay) {
		this.permissionDisplay = permissionDisplay;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RolePermissionRecord [rolePermissionId=").append(rolePermissionId).append(", roleId=")
		.append(roleId).append(", permissionName=").append(permissionName).append(", permissionDisplay=")
		.append(permissionDisplay).append("]");
		return builder.toString();
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(roleId, "role identifier"));
		results.addResults(validateStringIsSet(permissionName, "permission name"));
		return results;
	}

	@Override
	public int compareTo(RolePermissionRecord o) {
		return getPermissionDisplay().compareTo(o.getPermissionDisplay());
	}
}
