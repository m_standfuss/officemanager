package officemanager.biz.records;

import java.math.BigDecimal;

public class FlatRateSearchRecord extends Record {

	private final Long flatRateServiceId;
	private String serviceName;
	private String productType;
	private String category;

	private int amountSold;
	private BigDecimal currentPrice;
	private BigDecimal basePrice;
	private boolean isOnSale;

	public FlatRateSearchRecord(Long flatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;
	}

	public String getServiceName() {
		return makeNotNull(serviceName);
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getProductType() {
		return makeNotNull(productType);
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getAmountSold() {
		return amountSold;
	}

	public void setAmountSold(int amountSold) {
		this.amountSold = amountSold;
	}

	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(BigDecimal price) {
		currentPrice = price;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public boolean isOnSale() {
		return isOnSale;
	}

	public void setOnSale(boolean isOnSale) {
		this.isOnSale = isOnSale;
	}

	public Long getFlatRateServiceId() {
		return flatRateServiceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flatRateServiceId == null) ? 0 : flatRateServiceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FlatRateSearchRecord)) {
			return false;
		}
		FlatRateSearchRecord other = (FlatRateSearchRecord) obj;
		if (flatRateServiceId == null) {
			if (other.flatRateServiceId != null) {
				return false;
			}
		} else if (!flatRateServiceId.equals(other.flatRateServiceId)) {
			return false;
		}
		return true;
	}
}
