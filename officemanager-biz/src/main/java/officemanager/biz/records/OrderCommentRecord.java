package officemanager.biz.records;

import java.util.Date;

public class OrderCommentRecord extends Record implements IValidateRecord {

	private Long orderCommentId;
	private Long orderId;
	private LongTextRecord longText;
	private CodeValueRecord commentType;
	private String authoredBy;
	private String lastUpdtBy;
	private Date lastUpdtOn;

	public OrderCommentRecord() {}

	public Long getOrderCommentId() {
		return orderCommentId;
	}

	public void setOrderCommentId(Long orderCommentId) {
		this.orderCommentId = orderCommentId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public LongTextRecord getLongText() {
		return longText;
	}

	public void setLongText(LongTextRecord longText) {
		this.longText = longText;
	}

	public CodeValueRecord getCommentType() {
		return makeNotNull(commentType);
	}

	public void setCommentType(CodeValueRecord commentType) {
		this.commentType = commentType;
	}

	public String getAuthoredBy() {
		return makeNotNull(authoredBy);
	}

	public void setAuthoredBy(String authoredBy) {
		this.authoredBy = authoredBy;
	}

	public String getLastUpdtBy() {
		return makeNotNull(lastUpdtBy);
	}

	public void setLastUpdtBy(String lastUpdtBy) {
		this.lastUpdtBy = lastUpdtBy;
	}

	public Date getLastUpdtOn() {
		return lastUpdtOn;
	}

	public void setLastUpdtOn(Date lastUpdtOn) {
		this.lastUpdtOn = lastUpdtOn;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults()
				.addResults(validateCodeValueSet(commentType, "comment type"));

		if (longText != null) {
			results.addResults(longText.validateRecord());
		}
		return results;
	}
}
