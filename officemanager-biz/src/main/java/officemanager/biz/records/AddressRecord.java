package officemanager.biz.records;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.collect.Lists;

import officemanager.utility.StringUtility;

public class AddressRecord extends Record implements IValidateRecord {

	private final Long addressId;
	private Long personId;
	private boolean primaryAddressInd;
	private List<String> addressLines;
	private String city;
	private String state;
	private String zip;
	private CodeValueRecord addressType;

	public AddressRecord() {
		this((Long) null);
	}

	public AddressRecord(Long addressId) {
		this.addressId = addressId;
	}

	public AddressRecord(Long addressId, AddressRecord record) {
		this(addressId);
		copyConstructor(record);
	}

	/**
	 * Copy constructor, creates a local copy of all the fields.
	 * 
	 * @param addressRecord
	 *            The record to copy.
	 */
	public AddressRecord(AddressRecord toCopy) {
		checkNotNull(toCopy);
		addressId = toCopy.addressId;
		copyConstructor(toCopy);
	}

	private void copyConstructor(AddressRecord toCopy) {
		personId = toCopy.personId;
		primaryAddressInd = toCopy.primaryAddressInd;
		addressLines = Lists.newArrayList(toCopy.addressLines);
		city = toCopy.city;
		state = toCopy.state;
		zip = toCopy.zip;
		addressType = new CodeValueRecord(toCopy.addressType);
	}

	public Long getAddressId() {
		return addressId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public boolean isPrimaryAddress() {
		return primaryAddressInd;
	}

	public void setPrimaryAddress(boolean primaryAddress) {
		primaryAddressInd = primaryAddress;
	}

	public String getAddressCombined() {
		String SPACE = "";
		StringBuilder sb = new StringBuilder();
		for (String addressLine : addressLines) {
			if (StringUtility.isNullOrWhiteSpace(addressLine)) {
				continue;
			}
			sb.append(SPACE);
			sb.append(addressLine);
			SPACE = " ";
		}
		return sb.toString();
	}

	public List<String> getAddressLines() {
		return addressLines;
	}

	public void setAddressLines(List<String> addressLines) {
		this.addressLines = Lists.newArrayList(addressLines);
		this.addressLines.removeAll(Lists.newArrayList((String) null));
	}

	public String getCity() {
		return makeNotNull(city);
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return makeNotNull(state);
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return makeNotNull(zip);
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public CodeValueRecord getAddressType() {
		return makeNotNull(addressType);
	}

	public void setAddressType(CodeValueRecord addressType) {
		this.addressType = addressType;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(addressLines, "address"));
		results.addResults(validateCodeValueSet(addressType, "address type"));
		if (addressLines != null) {
			results.addResults(validate(addressLines.size() > 0, "At least one address line must be specified"));
			for (String addressLine : addressLines) {
				results.addResults(validateStringLength(addressLine, "address line", 70, null));
			}
		}
		results.addResults(validateStringLength(city, "city", 30, null));
		results.addResults(validateStringLength(zip, "zipcode", 20, null));
		results.addResults(validateStringLength(state, "state", 10, null));
		return results;
	}
}
