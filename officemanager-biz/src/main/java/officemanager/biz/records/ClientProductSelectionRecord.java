package officemanager.biz.records;

public class ClientProductSelectionRecord extends Record {

	private Long clientProductId;
	private String productManufacturer;
	private String productModel;
	private String productType;
	private String serialNumber;

	public ClientProductSelectionRecord() {}

	public ClientProductSelectionRecord(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public final Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public final String getProductManufacturer() {
		return makeNotNull(productManufacturer);
	}

	public final void setProductManufacturer(String productManufacturer) {
		this.productManufacturer = productManufacturer;
	}

	public final String getProductModel() {
		return makeNotNull(productModel);
	}

	public final void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public final String getProductType() {
		return makeNotNull(productType);
	}

	public final void setProductType(String productType) {
		this.productType = productType;
	}

	public final String getSerialNumber() {
		return makeNotNull(serialNumber);
	}

	public final void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (clientProductId == null ? 0 : clientProductId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ClientProductSelectionRecord)) {
			return false;
		}
		final ClientProductSelectionRecord other = (ClientProductSelectionRecord) obj;
		if (clientProductId == null || other.clientProductId == null) {
			return false;
		}
		return clientProductId.equals(other.clientProductId);
	}
}
