package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.Date;

public class PartRecord extends Record implements IValidateRecord {

	private Long partId;
	private CodeValueRecord categoryType;
	private CodeValueRecord manufacturer;
	private CodeValueRecord productType;
	private int amountSold;
	private String barcode;
	private BigDecimal basePrice;
	private String description;
	private int inventoryCount;
	private int inventoryThreshold;
	private Long locationId;
	private String locationDisplay;
	private String manufPartId;
	private String partName;
	private Date updtDtTm;
	private String lastUpdatedBy;

	public PartRecord() {
		this(null);
	}

	public PartRecord(Long partId) {
		this.partId = partId;
	}

	public Long getPartId() {
		return partId;
	}

	public void setPartId(Long partId) {
		this.partId = partId;
	}

	public CodeValueRecord getCategoryType() {
		return makeNotNull(categoryType);
	}

	public void setCategoryType(CodeValueRecord categoryType) {
		this.categoryType = categoryType;
	}

	public CodeValueRecord getManufacturer() {
		return makeNotNull(manufacturer);
	}

	public void setManufacturer(CodeValueRecord manufacturer) {
		this.manufacturer = manufacturer;
	}

	public CodeValueRecord getProductType() {
		return makeNotNull(productType);
	}

	public void setProductType(CodeValueRecord productType) {
		this.productType = productType;
	}

	public int getAmountSold() {
		return amountSold;
	}

	public void setAmountSold(int amountSold) {
		this.amountSold = amountSold;
	}

	public String getBarcode() {
		return makeNotNull(barcode);
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public String getDescription() {
		return makeNotNull(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getInventoryCount() {
		return inventoryCount;
	}

	public void setInventoryCount(int inventoryCount) {
		this.inventoryCount = inventoryCount;
	}

	public int getInventoryThreshold() {
		return inventoryThreshold;
	}

	public void setInventoryThreshold(int inventoryThreshold) {
		this.inventoryThreshold = inventoryThreshold;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getLocationDisplay() {
		return makeNotNull(locationDisplay);
	}

	public void setLocationDisplay(String locationDisplay) {
		this.locationDisplay = locationDisplay;
	}

	public String getManufPartId() {
		return makeNotNull(manufPartId);
	}

	public void setManufPartId(String manufPartId) {
		this.manufPartId = manufPartId;
	}

	public String getPartName() {
		return makeNotNull(partName);
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public Date getUpdtDtTm() {
		return updtDtTm;
	}

	public void setUpdtDtTm(Date updtDtTm) {
		this.updtDtTm = updtDtTm;
	}

	public String getLastUpdatedBy() {
		return makeNotNull(lastUpdatedBy);
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateCodeValueSet(categoryType, "category"));
		results.addResults(validateCodeValueSet(manufacturer, "manufacturer"));
		results.addResults(validateCodeValueSet(productType, "product type"));
		results.addResults(validateNotNull(basePrice, "base price"));
		results.addResults(validateNotNull(description, "description"));
		results.addResults(validateNotNull(partName, "part name"));

		results.addResults(validateIsPositive(basePrice, "base price"));

		results.addResults(validateStringLength(partName, "part name", 50, null));
		results.addResults(validateStringLength(manufPartId, "manufacturer's part id", 50, null));
		results.addResults(validateStringLength(description, "part description", 80, null));
		results.addResults(validateStringLength(barcode, "barcode", 25, null));
		return results;
	}
}
