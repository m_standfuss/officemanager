package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.collect.Lists;

import officemanager.biz.records.LongTextRecord.TextFormat;
import officemanager.utility.StringUtility;

public abstract class Record {
	protected String makeNotNull(String string) {
		return string == null ? "" : string;
	}

	protected <T> List<T> makeNotNull(List<T> list) {
		if (list == null) {
			return Lists.newArrayList();
		}
		return list;
	}

	protected CodeValueRecord makeNotNull(CodeValueRecord codeValue) {
		return codeValue == null ? new CodeValueRecord(0L, "", null) : codeValue;
	}

	protected LongTextRecord makeNotNull(LongTextRecord longTextRecord) {
		if (longTextRecord == null) {
			LongTextRecord blankRecord = new LongTextRecord();
			blankRecord.setFormat(TextFormat.PLAIN_TEXT);
			return blankRecord;
		}
		return longTextRecord;
	}

	/**
	 * The default validation method if a helper method cannot be found then
	 * this can check against a boolean directly and prompt with any error
	 * message needed.
	 * 
	 * @param toCheck
	 *            The boolean condition to verify is true.
	 * @param errorMessage
	 *            The message to prompt with if the boolean to check is false.
	 * @return The validation results of the check.
	 */
	protected ValidationResults validate(boolean toCheck, String errorMessage) {
		ValidationResults results = new ValidationResults();
		if (!toCheck) {
			results.logFailure(this.getClass().getSimpleName() + ": " + errorMessage);
		}
		return results;
	}

	/**
	 * Checks if an input has been set or not. By testing if the Object is null.
	 * 
	 * @param toCheck
	 *            The input to check.
	 * @param name
	 *            The name to call the input.
	 * @return The validation results of the check.
	 */
	protected ValidationResults validateNotNull(Object toCheck, String name) {
		return validate(toCheck != null, "The " + name + " must be specified.");
	}

	/**
	 * Validates that a string is not null or empty white space.
	 * 
	 * @param toCheck
	 *            The string to check.
	 * @param name
	 *            The displayable name of the string.
	 * @return The validation results of the check.
	 */
	protected ValidationResults validateStringIsSet(String toCheck, String name) {
		return validate(!StringUtility.isNullOrWhiteSpace(toCheck), "The " + name + " must be specified.");
	}

	/**
	 * Checks a String input for length specifications.
	 * 
	 * @param toCheck
	 *            The string value to check.
	 * @param name
	 *            The name to call the parameter.
	 * @param max
	 *            The max length, null means no max.
	 * @param min
	 *            The min length, null mean no min.
	 * @return The list of failure messages.
	 */
	protected ValidationResults validateStringLength(String toCheck, String name, Integer max, Integer min) {
		ValidationResults results = new ValidationResults();
		if (toCheck == null) {
			return results;
		}

		if (max != null && toCheck.length() > max) {
			results.addResults(
					validate(toCheck.length() <= max, "The " + name + " cannot be over " + max + " characters."));
		}

		if (min != null && toCheck.length() < min) {
			results.addResults(
					validate(toCheck.length() >= min, "The " + name + " cannot be less than " + min + " characters."));
		}
		return results;
	}

	/**
	 * Checks that a BigDecimal value is greater than 0.
	 * 
	 * @param value
	 *            The value to check.
	 * @param name
	 *            The displayable name of the parameter.
	 * @return The validation results.
	 */
	protected ValidationResults validateIsPositive(BigDecimal value, String name) {
		if (value == null) {
			return new ValidationResults();
		}

		return validate(value.compareTo(BigDecimal.ZERO) > 0, "The " + name + " must be greater than 0.");
	}

	/**
	 * Checks the a Long value is greater than 0.
	 * 
	 * @param value
	 *            The value to check.
	 * @param name
	 *            The displayable name of the paramter.
	 * @return The validation results.
	 */
	protected ValidationResults validateIsPositive(Long value, String name) {
		if (value == null) {
			return new ValidationResults();
		}
		return validate(value > 0, "The " + name + " must be greater than 0.");
	}

	/**
	 * Checks that a BigDecimal value is greater than or equal to 0.
	 * 
	 * @param value
	 *            The value to check.
	 * @param name
	 *            The displayable name of the parameter.
	 * @return The validation results.
	 */
	protected ValidationResults validateIsPositiveOrZero(BigDecimal value, String name) {
		if (value == null) {
			return new ValidationResults();
		}

		return validate(value.compareTo(BigDecimal.ZERO) >= 0, "The " + name + " must be 0 or greater.");
	}

	/**
	 * Checks that a Long value is greater than or equal to 0.
	 * 
	 * @param value
	 *            The value to check.
	 * @param name
	 *            The displayable name of the parameter.
	 * @return The validation results.
	 */
	protected ValidationResults validateIsPositiveOrZero(Long value, String name) {
		if (value == null) {
			return new ValidationResults();
		}

		return validate(value >= 0L, "The " + name + " must be 0 or greater.");
	}

	protected ValidationResults validateIsPositiveOrZero(int value, String name) {
		return validateIsPositiveOrZero((long) value, name);
	}

	/**
	 * Verifies that the code value record is set.
	 * 
	 * @param record
	 *            The code value record to check.
	 * @param name
	 *            The name to display if not valid.
	 * @return The validation results.
	 */
	protected ValidationResults validateCodeValueSet(CodeValueRecord record, String name) {
		return validate(record != null && record.getCodeValue() != null, "The " + name + " must be set.");
	}
}
