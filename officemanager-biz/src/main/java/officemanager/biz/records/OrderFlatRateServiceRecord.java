package officemanager.biz.records;

import java.math.BigDecimal;

public class OrderFlatRateServiceRecord extends Record implements IValidateRecord {

	private Long orderToFrsId;
	private Long clientProductId;
	private BigDecimal servicePrice;
	private String serviceName;
	private Long flatRateServiceId;
	private boolean onSale;
	private String clientProductManufacturer;
	private String clientProductModel;
	private String clientProductSerialNumber;
	private boolean refunded;

	public Long getOrderToFrsId() {
		return orderToFrsId;
	}

	public void setOrderToFrsId(Long orderToFrsId) {
		this.orderToFrsId = orderToFrsId;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public BigDecimal getServicePrice() {
		return servicePrice;
	}

	public void setServicePrice(BigDecimal servicePrice) {
		this.servicePrice = servicePrice;
	}

	public String getServiceName() {
		return makeNotNull(serviceName);
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Long getFlatRateServiceId() {
		return flatRateServiceId;
	}

	public void setFlatRateServiceId(Long flatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;
	}

	public boolean isOnSale() {
		return onSale;
	}

	public void setOnSale(boolean onSale) {
		this.onSale = onSale;
	}

	public String getClientProductManufacturer() {
		return makeNotNull(clientProductManufacturer);
	}

	public void setClientProductManufacturer(String clientProductManufacturer) {
		this.clientProductManufacturer = clientProductManufacturer;
	}

	public String getClientProductModel() {
		return makeNotNull(clientProductModel);
	}

	public void setClientProductModel(String clientProductModel) {
		this.clientProductModel = clientProductModel;
	}

	public String getClientProductSerialNumber() {
		return makeNotNull(clientProductSerialNumber);
	}

	public void setClientProductSerialNumber(String clientProductSerialNumber) {
		this.clientProductSerialNumber = clientProductSerialNumber;
	}

	public boolean isRefunded() {
		return refunded;
	}

	public void setRefunded(boolean refunded) {
		this.refunded = refunded;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(clientProductId, "client product"));
		results.addResults(validateNotNull(servicePrice, "service price"));
		results.addResults(validateNotNull(flatRateServiceId, "flat rate service"));

		results.addResults(validateIsPositiveOrZero(servicePrice, "service price"));
		return results;
	}
}
