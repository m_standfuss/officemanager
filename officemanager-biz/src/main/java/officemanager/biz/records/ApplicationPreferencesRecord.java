package officemanager.biz.records;

import java.util.List;

import com.google.common.collect.Lists;

public class ApplicationPreferencesRecord extends Record {

	private PreferenceRecord hourlyRate;
	private PreferenceRecord hourlyRateDiscounted;
	private PreferenceRecord taxRate;

	private PreferenceRecord companyAddress;
	private PreferenceRecord companyPhone;
	private PreferenceRecord companyWebsite;
	private List<PreferenceRecord> claimTicketCommentTypes;
	private List<PreferenceRecord> shopTicketCommentTypes;
	private List<PreferenceRecord> orderRecieptCommentTypes;

	public ApplicationPreferencesRecord() {
		claimTicketCommentTypes = Lists.newArrayList();
		shopTicketCommentTypes = Lists.newArrayList();
		orderRecieptCommentTypes = Lists.newArrayList();
	}

	public PreferenceRecord getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(PreferenceRecord hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public PreferenceRecord getHourlyRateDiscounted() {
		return hourlyRateDiscounted;
	}

	public void setHourlyRateDiscounted(PreferenceRecord hourlyRateDiscounted) {
		this.hourlyRateDiscounted = hourlyRateDiscounted;
	}

	public PreferenceRecord getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(PreferenceRecord taxRate) {
		this.taxRate = taxRate;
	}

	public PreferenceRecord getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(PreferenceRecord companyAddress) {
		this.companyAddress = companyAddress;
	}

	public PreferenceRecord getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(PreferenceRecord companyPhone) {
		this.companyPhone = companyPhone;
	}

	public PreferenceRecord getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(PreferenceRecord companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public List<PreferenceRecord> getClaimTicketCommentTypes() {
		return makeNotNull(claimTicketCommentTypes);
	}

	public void setClaimTicketCommentTypes(List<PreferenceRecord> claimTicketCommentTypes) {
		this.claimTicketCommentTypes = claimTicketCommentTypes;
	}

	public List<PreferenceRecord> getShopTicketCommentTypes() {
		return makeNotNull(shopTicketCommentTypes);
	}

	public void setShopTicketCommentTypes(List<PreferenceRecord> shopTicketCommentTypes) {
		this.shopTicketCommentTypes = shopTicketCommentTypes;
	}

	public List<PreferenceRecord> getOrderRecieptCommentTypes() {
		return makeNotNull(orderRecieptCommentTypes);
	}

	public void setOrderRecieptCommentTypes(List<PreferenceRecord> orderRecieptCommentTypes) {
		this.orderRecieptCommentTypes = orderRecieptCommentTypes;
	}

}
