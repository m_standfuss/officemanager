package officemanager.biz.records;

public class LocationRecord extends Record implements IValidateRecord {

	private Long locationId;
	private String abbrDisplay;
	private String display;
	private Long parentLocationId;
	private CodeValueRecord locationType;

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getAbbrDisplay() {
		return abbrDisplay;
	}

	public void setAbbrDisplay(String abbrDisplay) {
		this.abbrDisplay = abbrDisplay;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public Long getParentLocationId() {
		return parentLocationId;
	}

	public void setParentLocationId(Long parentLocationId) {
		this.parentLocationId = parentLocationId;
	}

	public CodeValueRecord getLocationType() {
		return makeNotNull(locationType);
	}

	public void setLocationType(CodeValueRecord locationType) {
		this.locationType = locationType;
	}

	public Long getLocationId() {
		return locationId;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = validateNotNull(abbrDisplay, "abbreviated display");
		results.addResults(validateStringIsSet(display, "display"));
		results.addResults(validateNotNull(locationType, "location type"));
		results.addResults(validateStringLength(abbrDisplay, "abbreviated display", 5, null));
		results.addResults(validateStringLength(display, "display", 50, null));
		return results;
	}

	@Override
	public String toString() {
		return "LocationRecord [locationId=" + locationId + ", abbrDisplay=" + abbrDisplay + ", display=" + display
				+ ", parentLocationId=" + parentLocationId + ", locationType=" + locationType + "]";
	}
}
