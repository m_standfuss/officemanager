package officemanager.biz.records;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class UserSecurityRecord {
	private static final Logger logger = LogManager.getLogger(UserSecurityRecord.class);

	private final Long personnelId;
	private final Map<String, UserPermissionRecord> permissions;

	public UserSecurityRecord(Long personnelId) {
		checkNotNull(personnelId);
		this.personnelId = personnelId;
		permissions = Maps.newHashMap();
	}

	public void addPermission(UserPermissionRecord record) {
		checkNotNull(record);
		permissions.put(record.getPermissionName(), record);
	}

	public Long getPersonnelId() {
		return personnelId;
	}

	public Collection<UserPermissionRecord> getPermissions() {
		return Lists.newArrayList(permissions.values());
	}

	/**
	 * Determines if the user has this specific permission or the admin
	 * permission which grants all permissions at current time.
	 * 
	 * @param permissionName
	 *            The permission name to check.
	 * @return If the user permission model has that permission.
	 */
	public boolean hasPermission(String permissionName) {
		logger.debug("Checking for permission '" + permissionName + "'.");
		if (!_hasPermission(permissionName)) {
			logger.debug("User does not permission.");
			return false;
		}
		logger.debug("User has permission");
		return true;
	}

	private boolean _hasPermission(String permissionName) {
		UserPermissionRecord record = permissions.get(permissionName);
		if (record == null) {
			logger.debug("Unable to find the permission in user's map.");
			return false;
		}
		if (record.getEndEffectiveDttm() == null) {
			logger.debug("Unexpiring permission");
			return true;
		}
		Date now = new Date();
		if (record.getEndEffectiveDttm().before(now)) {
			logger.debug("User permission has expired.");
			return false;
		}
		return true;
	}
}
