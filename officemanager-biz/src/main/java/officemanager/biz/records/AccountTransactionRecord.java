package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.Date;

public class AccountTransactionRecord extends Record {

	private BigDecimal amount;
	private Date transactionDttm;
	private CodeValueRecord transactionType;
	private String paymentDescription;
	private String paymentNumber;
	private CodeValueRecord paymentType;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getTransactionDttm() {
		return transactionDttm;
	}

	public void setTransactionDttm(Date transactionDttm) {
		this.transactionDttm = transactionDttm;
	}

	public CodeValueRecord getTransactionType() {
		return makeNotNull(transactionType);
	}

	public void setTransactionType(CodeValueRecord transactionType) {
		this.transactionType = transactionType;
	}

	public String getPaymentDescription() {
		return makeNotNull(paymentDescription);
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public String getPaymentNumber() {
		return makeNotNull(paymentNumber);
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public CodeValueRecord getPaymentType() {
		return makeNotNull(paymentType);
	}

	public void setPaymentType(CodeValueRecord paymentType) {
		this.paymentType = paymentType;
	}
}
