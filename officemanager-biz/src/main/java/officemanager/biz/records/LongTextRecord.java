package officemanager.biz.records;

public class LongTextRecord extends Record implements IValidateRecord {
	public enum TextFormat {
		PLAIN_TEXT, HTML, RTF
	}

	private Long longTextId;
	private String longText;
	private TextFormat format;

	public LongTextRecord() {}

	public LongTextRecord(String longText) {
		longTextId = null;
		this.longText = longText;
		format = TextFormat.PLAIN_TEXT;
	}

	public Long getLongTextId() {
		return longTextId;
	}

	public void setLongTextId(Long longTextId) {
		this.longTextId = longTextId;
	}

	public String getLongText() {
		return makeNotNull(longText);
	}

	public void setLongText(String longText) {
		this.longText = longText;
	}

	public TextFormat getFormat() {
		return format;
	}

	public void setFormat(TextFormat format) {
		this.format = format;
	}

	public String getAsPlainText() {
		// TODO implement this functionality.
		return longText;
	}

	public String getAsRTF() {
		// TODO implement this functionality.
		return longText;
	}

	public String getAsHTML() {
		// TODO implement this functionality.
		return longText;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(format, "text format"));
		return results;
	}
}
