package officemanager.biz.records;

public class AccountSearchRecord extends Record {

	private Long accountId;
	private Long clientId;

	private String nameFullFormatted;
	private String primaryPhone;
	private String email;
	private CodeValueRecord status;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getNameFullFormatted() {
		return makeNotNull(nameFullFormatted);
	}

	public void setNameFullFormatted(String nameFullFormatted) {
		this.nameFullFormatted = nameFullFormatted;
	}

	public String getPrimaryPhone() {
		return makeNotNull(primaryPhone);
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getEmail() {
		return makeNotNull(email);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CodeValueRecord getStatus() {
		return makeNotNull(status);
	}

	public void setStatus(CodeValueRecord status) {
		this.status = status;
	}
}
