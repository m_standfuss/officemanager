package officemanager.biz.records;

import java.util.Date;

public class ClientSearchRecord {

	private Long clientId;
	private String nameFullFormatted;
	private String primaryPhone;
	private String email;
	private Date joinedOnDttm;

	public ClientSearchRecord() {}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getNameFullFormatted() {
		return nameFullFormatted;
	}

	public void setNameFullFormatted(String nameFullFormatted) {
		this.nameFullFormatted = nameFullFormatted;
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getJoinedOnDttm() {
		return joinedOnDttm;
	}

	public void setJoinedOnDttm(Date joinedOnDttm) {
		this.joinedOnDttm = joinedOnDttm;
	}
}
