package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.Date;

public class SaleRecord extends Record implements IValidateRecord {

	public enum SaleProductType {
		PART, FLAT_RATE_SERVICE
	}

	public enum SaleType {
		FLAT_RATE_OFF, PERCENTAGE_OFF
	}

	private Long saleId;
	private Long entityId;
	private String entityName;
	private String saleName;
	private SaleProductType saleProductType;
	private BigDecimal amountOff;
	private Date endDttm;
	private SaleType saleType;
	private String saleTypeDisplay;
	private Date startDttm;

	public Long getSaleId() {
		return saleId;
	}

	public void setSaleId(Long saleId) {
		this.saleId = saleId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return makeNotNull(entityName);
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getSaleName() {
		return makeNotNull(saleName);
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public SaleProductType getSaleProductType() {
		return saleProductType;
	}

	public void setSaleProductType(SaleProductType saleProductType) {
		this.saleProductType = saleProductType;
	}

	public BigDecimal getAmountOff() {
		return amountOff;
	}

	public void setAmountOff(BigDecimal amountOff) {
		this.amountOff = amountOff;
	}

	public Date getEndDttm() {
		return endDttm;
	}

	public void setEndDttm(Date endDttm) {
		this.endDttm = endDttm;
	}

	public SaleType getSaleType() {
		return saleType;
	}

	public void setSaleType(SaleType saleType) {
		this.saleType = saleType;
	}

	public String getSaleTypeDisplay() {
		return makeNotNull(saleTypeDisplay);
	}

	public void setSaleTypeDisplay(String saleTypeDisplay) {
		this.saleTypeDisplay = saleTypeDisplay;
	}

	public Date getStartDttm() {
		return startDttm;
	}

	public void setStartDttm(Date startDttm) {
		this.startDttm = startDttm;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(entityId, "sale product"));
		results.addResults(validateNotNull(saleProductType, "sale product type"));
		results.addResults(validateNotNull(saleName, "sale name"));
		results.addResults(validateStringLength(saleName, "sale name", 30, null));
		results.addResults(validateNotNull(amountOff, "amount"));
		results.addResults(validateNotNull(startDttm, "starting date and time"));
		results.addResults(validateNotNull(saleType, "sale type"));
		results.addResults(validateIsPositive(amountOff, "amount"));
		return results;
	}
}
