package officemanager.biz.records;

import java.math.BigDecimal;

public class FlatRateOrderRecord extends Record {

	private final Long flatRateServiceId;
	private final Long orderToFlatRateServiceId;

	private String serviceDescription;

	private Long clientProductId;
	private String clientProductDisplay;

	private BigDecimal price;
	private boolean isOnSale;

	public FlatRateOrderRecord(Long flatRateServiceId, Long orderToFlatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;
		this.orderToFlatRateServiceId = orderToFlatRateServiceId;
	}

	public String getServiceDescription() {
		return makeNotNull(serviceDescription);
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isOnSale() {
		return isOnSale;
	}

	public void setOnSale(boolean isOnSale) {
		this.isOnSale = isOnSale;
	}

	public Long getFlatRateServiceId() {
		return flatRateServiceId;
	}

	public Long getOrderToFlatRateServiceId() {
		return orderToFlatRateServiceId;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public String getClientProductDisplay() {
		return makeNotNull(clientProductDisplay);
	}

	public void setClientProductDisplay(String clientProductDisplay) {
		this.clientProductDisplay = clientProductDisplay;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderToFlatRateServiceId == null) ? 0 : orderToFlatRateServiceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FlatRateOrderRecord other = (FlatRateOrderRecord) obj;
		if (orderToFlatRateServiceId != null) {
			if (other.orderToFlatRateServiceId != null) {
				return orderToFlatRateServiceId.equals(other.orderToFlatRateServiceId);
			}
		}
		return false;
	}
}
