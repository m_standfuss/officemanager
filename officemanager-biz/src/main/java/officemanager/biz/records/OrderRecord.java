package officemanager.biz.records;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class OrderRecord extends Record implements IValidateRecord {

	public enum OrderType {
		UNKNOWN, MERCHANDISE, WORK
	}

	public enum OrderStatus {
		UNKNOWN, OPEN, CLOSED, INERROR
	}

	private Long orderId;
	private Long clientId;
	private String clientName;
	private String clientPhone;
	private OrderStatus orderStatus;
	private String orderStatusDisplay;
	private OrderType orderType;
	private String orderTypeDisplay;
	private String createdBy;
	private String lastUpdatedBy;
	private String closedBy;
	private Date createdDttm;
	private Date lastUpdatedDttm;
	private Date closedDttm;
	private List<OrderClientProductRecord> clientProducts;
	private List<OrderFlatRateServiceRecord> flatRates;
	private List<OrderPartRecord> parts;
	private List<OrderServiceRecord> services;

	private Map<Long, OrderClientProductRecord> clientProductMap;

	public OrderRecord() {
		clientProducts = Lists.newArrayList();
		flatRates = Lists.newArrayList();
		parts = Lists.newArrayList();
		services = Lists.newArrayList();
	}

	public Map<Long, OrderClientProductRecord> getClientProductMap() {
		if (clientProductMap != null) {
			return clientProductMap;
		}
		Map<Long, OrderClientProductRecord> map = Maps.newHashMap();
		for (OrderClientProductRecord product : clientProducts) {
			map.put(product.getClientProductId(), product);
		}
		return clientProductMap = map;
	}

	public Map<Long, List<OrderFlatRateServiceRecord>> makeFlatRateMap() {
		Map<Long, OrderClientProductRecord> productMap = getClientProductMap();
		Map<Long, List<OrderFlatRateServiceRecord>> map = Maps.newHashMap();
		for (OrderFlatRateServiceRecord service : flatRates) {
			OrderClientProductRecord product = productMap.get(service.getClientProductId());
			if (map.containsKey(product.getClientProductId())) {
				map.get(product.getClientProductId()).add(service);
			} else {
				map.put(product.getClientProductId(), Lists.newArrayList(service));
			}
		}
		return map;
	}

	public Map<Long, List<OrderPartRecord>> makePartMap() {
		Map<Long, OrderClientProductRecord> productMap = getClientProductMap();
		Map<Long, List<OrderPartRecord>> map = Maps.newHashMap();
		for (OrderPartRecord part : parts) {
			OrderClientProductRecord product = productMap.get(part.getClientProductId());
			Long productId = null;
			if (product != null) {
				productId = product.getClientProductId();
			}
			if (map.containsKey(productId)) {
				map.get(productId).add(part);
			} else {
				map.put(productId, Lists.newArrayList(part));
			}
		}
		return map;
	}

	public Map<Long, List<OrderServiceRecord>> makeServiceMap() {
		Map<Long, OrderClientProductRecord> productMap = getClientProductMap();
		Map<Long, List<OrderServiceRecord>> map = Maps.newHashMap();
		for (OrderServiceRecord service : services) {
			OrderClientProductRecord product = productMap.get(service.getClientProductId());
			if (map.containsKey(product.getClientProductId())) {
				map.get(product.getClientProductId()).add(service);
			} else {
				map.put(product.getClientProductId(), Lists.newArrayList(service));
			}
		}
		return map;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return makeNotNull(clientName);
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientPhone() {
		return makeNotNull(clientPhone);
	}

	public void setClientPhone(String clientPhone) {
		this.clientPhone = clientPhone;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderStatusDisplay() {
		return makeNotNull(orderStatusDisplay);
	}

	public void setOrderStatusDisplay(String orderStatusDisplay) {
		this.orderStatusDisplay = orderStatusDisplay;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public String getOrderTypeDisplay() {
		return makeNotNull(orderTypeDisplay);
	}

	public void setOrderTypeDisplay(String orderTypeDisplay) {
		this.orderTypeDisplay = orderTypeDisplay;
	}

	public String getCreatedBy() {
		return makeNotNull(createdBy);
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastUpdatedBy() {
		return makeNotNull(lastUpdatedBy);
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getClosedBy() {
		return makeNotNull(closedBy);
	}

	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}

	public Date getCreatedDttm() {
		return createdDttm;
	}

	public void setCreatedDttm(Date createdDttm) {
		this.createdDttm = createdDttm;
	}

	public Date getLastUpdatedDttm() {
		return lastUpdatedDttm;
	}

	public void setLastUpdatedDttm(Date lastUpdatedDttm) {
		this.lastUpdatedDttm = lastUpdatedDttm;
	}

	public Date getClosedDttm() {
		return closedDttm;
	}

	public void setClosedDttm(Date closedDttm) {
		this.closedDttm = closedDttm;
	}

	public List<OrderClientProductRecord> getClientProducts() {
		return clientProducts;
	}

	public void setClientProducts(List<OrderClientProductRecord> clientProducts) {
		this.clientProducts = clientProducts;
		clientProductMap = null;
	}

	public List<OrderFlatRateServiceRecord> getFlatRates() {
		return flatRates;
	}

	public void setFlatRates(List<OrderFlatRateServiceRecord> flatRates) {
		this.flatRates = flatRates;
	}

	public List<OrderPartRecord> getParts() {
		return parts;
	}

	public void setParts(List<OrderPartRecord> parts) {
		this.parts = parts;
	}

	public List<OrderServiceRecord> getServices() {
		return services;
	}

	public void setServices(List<OrderServiceRecord> services) {
		this.services = services;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(orderStatus, "order status"));
		results.addResults(validateNotNull(orderType, "order type"));

		if (orderType != OrderType.MERCHANDISE) {
			results.addResults(validateNotNull(clientId, "client"));
		}

		for (OrderClientProductRecord record : clientProducts) {
			results.addResults(record.validateRecord());
		}
		for (OrderFlatRateServiceRecord record : flatRates) {
			results.addResults(record.validateRecord());
		}
		for (OrderPartRecord record : parts) {
			results.addResults(record.validateRecord());
		}
		for (OrderServiceRecord record : services) {
			results.addResults(record.validateRecord());
		}
		return results;
	}
}
