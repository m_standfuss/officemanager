package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.List;

public class OrderPaymentsRecord extends Record {

	private BigDecimal orderTotal;
	private List<PaymentRecord> payments;

	public BigDecimal getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(BigDecimal orderTotal) {
		this.orderTotal = orderTotal;
	}

	public List<PaymentRecord> getPayments() {
		return payments;
	}

	public void setPayments(List<PaymentRecord> payments) {
		this.payments = payments;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrderPaymentRecord [orderTotal=").append(orderTotal).append(", payments=").append(payments)
		.append("]");
		return builder.toString();
	}
}
