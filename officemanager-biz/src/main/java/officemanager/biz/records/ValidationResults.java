package officemanager.biz.records;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.collect.Lists;

public class ValidationResults {

	private final List<String> failureMessages;
	private final List<String> warningMessages;

	public ValidationResults() {
		failureMessages = Lists.newArrayList();
		warningMessages = Lists.newArrayList();
	}

	public ValidationResults addResults(ValidationResults toAdd) {
		failureMessages.addAll(toAdd.failureMessages);
		warningMessages.addAll(toAdd.warningMessages);
		return this;
	}

	public void logFailure(String failure) {
		checkNotNull(failure);
		failureMessages.add(failure);
	}

	public void logWarning(String warning) {
		checkNotNull(warning);
		warningMessages.add(warning);
	}

	public boolean isValid() {
		return failureMessages.size() == 0;
	}

	public final List<String> getFailureMessages() {
		return failureMessages;
	}

	public final List<String> getWarningMessages() {
		return warningMessages;
	}

	/**
	 * Converts any failures or warnings into a single displayable string,
	 * acceptable for end user displaying.
	 * 
	 * @return The display string.
	 */
	public String convertToDisplayString() {
		final StringBuilder b = new StringBuilder();

		if (getFailureMessages().size() > 0) {
			b.append("The following error(s) were were found:").append(System.lineSeparator())
					.append(System.lineSeparator());
		}
		for (final String fail : getFailureMessages()) {
			b.append(fail).append(System.lineSeparator());
		}

		if (getWarningMessages().size() > 0) {
			b.append("The following warning(s) were found:").append(System.lineSeparator())
					.append(System.lineSeparator());
		}
		for (final String warn : getWarningMessages()) {
			b.append(warn).append(System.lineSeparator());
		}
		return b.toString();
	}

	/**
	 * Throws an {@link IllegalStateException} if the validation result is
	 * invalid.
	 * 
	 * @throws IllegalStateException
	 *             If the results are invalid.
	 */
	public void throwIfInvalid() {
		if (isValid()) {
			return;
		}
		throw new IllegalStateException(convertToDisplayString());
	}
}
