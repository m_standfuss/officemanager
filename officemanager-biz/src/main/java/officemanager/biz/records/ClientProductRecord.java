package officemanager.biz.records;

public class ClientProductRecord extends Record implements IValidateRecord {

	private final Long clientProductId;
	private Long clientId;
	private String manufacturer;
	private CodeValueRecord productType;
	private CodeValueRecord productStatus;
	private String serialNum;
	private String barcode;
	private String model;
	private Integer workCount;

	/**
	 * Default constructor.
	 */
	public ClientProductRecord() {
		this(null);
	}

	public ClientProductRecord(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public ClientProductRecord(Long clientProductId, ClientProductRecord toCpy) {
		this(clientProductId);
		clientId = toCpy.clientId;
		manufacturer = toCpy.manufacturer;
		productType = new CodeValueRecord(toCpy.productType);
		productStatus = new CodeValueRecord(toCpy.productStatus);
		serialNum = toCpy.serialNum;
		barcode = toCpy.barcode;
		model = toCpy.model;
		workCount = toCpy.workCount;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getManufacturer() {
		return makeNotNull(manufacturer);
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public CodeValueRecord getProductType() {
		return makeNotNull(productType);
	}

	public void setProductType(CodeValueRecord productType) {
		this.productType = productType;
	}

	public CodeValueRecord getProductStatus() {
		return makeNotNull(productStatus);
	}

	public void setProductStatus(CodeValueRecord productStatus) {
		this.productStatus = productStatus;
	}

	public String getSerialNum() {
		return makeNotNull(serialNum);
	}

	public void setSerialNum(String vin) {
		serialNum = vin;
	}

	public String getBarcode() {
		return makeNotNull(barcode);
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getModel() {
		return makeNotNull(model);
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getWorkCount() {
		return workCount;
	}

	public void setWorkCount(Integer workCount) {
		this.workCount = workCount;
	}

	@Override
	public ValidationResults validateRecord() {
		final ValidationResults validation = new ValidationResults();
		validation.addResults(validateNotNull(clientId, "client"));
		validation.addResults(validateStringIsSet(manufacturer, "product manufacturer"));

		validation.addResults(validateStringLength(manufacturer, "product manufacturer", 50, null));
		validation.addResults(validateCodeValueSet(productType, "product type"));
		validation.addResults(validateStringLength(barcode, "product barcode", 32, null));
		validation.addResults(validateStringLength(model, "product model", 50, null));
		validation.addResults(validateStringLength(serialNum, "product serial number", 50, null));
		return validation;
	}
}
