package officemanager.biz.records;

import java.util.Date;

public class ClientRecord extends PersonRecord implements IValidateRecord {

	private Long clientId;
	private String attentionOf;
	private Date joinedOn;

	private boolean promotionClub;
	private boolean taxExempt;

	/**
	 * Constructor for creating a new client record for a given person. Defaults
	 * the client id to null.
	 * 
	 * @param personId
	 *            The identifier of the person to create a client record for.
	 */
	public ClientRecord() {
		super();
		joinedOn = new Date();
	}

	public ClientRecord(PersonRecord toCopy) {
		super(toCopy);
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Date getJoinedOn() {
		return joinedOn;
	}

	public void setJoinedOn(Date joinedOn) {
		this.joinedOn = joinedOn;
	}

	public String getAttentionOf() {
		return makeNotNull(attentionOf);
	}

	public void setAttentionOf(String attentionOf) {
		this.attentionOf = attentionOf;
	}

	public boolean isPromotionClub() {
		return promotionClub;
	}

	public void setPromotionClub(boolean promotionClub) {
		this.promotionClub = promotionClub;
	}

	public boolean isTaxExempt() {
		return taxExempt;
	}

	public void setTaxExempt(boolean taxExempt) {
		this.taxExempt = taxExempt;
	}

	@Override
	public ValidationResults validateRecord() {
		final ValidationResults results = super.validateRecord();
		results.addResults(validateStringLength(attentionOf, "attention of", 50, null));
		return results;
	}
}
