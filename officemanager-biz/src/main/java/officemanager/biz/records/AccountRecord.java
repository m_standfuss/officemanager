package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.Date;

public class AccountRecord extends Record {

	public enum AccountStatus {
		ACTIVE, CLOSED, CLOSED_AWAITING_PAYMENT, SUSPEND
	}

	private Long accountId;
	private Long clientId;
	private Date lastStatementDttm;
	private AccountStatus accountStatus;
	private String accountStatusDisplay;
	private BigDecimal balance;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Date getLastStatementDttm() {
		return lastStatementDttm;
	}

	public void setLastStatementDttm(Date lastStatementDttm) {
		this.lastStatementDttm = lastStatementDttm;
	}

	public AccountStatus getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(AccountStatus accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getAccountStatusDisplay() {
		return makeNotNull(accountStatusDisplay);
	}

	public void setAccountStatusDisplay(String accountStatusDisplay) {
		this.accountStatusDisplay = accountStatusDisplay;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
}
