package officemanager.biz.records;

import java.math.BigDecimal;

public class OrderPartRecord extends Record implements IValidateRecord {

	private Long orderToPartId;

	private Long partId;
	private Long clientProductId;
	private BigDecimal partPrice;
	private long quantity;
	private boolean isOnSale;
	private String partName;
	private String description;
	private String partNumber;
	private String manufacturerDisplay;
	private boolean isTaxExempt;
	private BigDecimal taxAmount;
	private String clientProductManufacturer;
	private String clientProductModel;
	private String clientProductSerialNumber;
	private boolean refunded;

	public Long getOrderToPartId() {
		return orderToPartId;
	}

	public void setOrderToPartId(Long orderToPartId) {
		this.orderToPartId = orderToPartId;
	}

	public Long getPartId() {
		return partId;
	}

	public void setPartId(Long partId) {
		this.partId = partId;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public BigDecimal getPartPrice() {
		return partPrice;
	}

	public void setPartPrice(BigDecimal partPrice) {
		this.partPrice = partPrice;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public boolean isOnSale() {
		return isOnSale;
	}

	public void setOnSale(boolean isOnSale) {
		this.isOnSale = isOnSale;
	}

	public String getPartName() {
		return makeNotNull(partName);
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getDescription() {
		return makeNotNull(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPartNumber() {
		return makeNotNull(partNumber);
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getManufacturerDisplay() {
		return makeNotNull(manufacturerDisplay);
	}

	public void setManufacturerDisplay(String manufacturerDisplay) {
		this.manufacturerDisplay = manufacturerDisplay;
	}

	public boolean isTaxExempt() {
		return isTaxExempt;
	}

	public void setTaxExempt(boolean isTaxExempt) {
		this.isTaxExempt = isTaxExempt;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getClientProductManufacturer() {
		return makeNotNull(clientProductManufacturer);
	}

	public void setClientProductManufacturer(String clientProductManufacturer) {
		this.clientProductManufacturer = clientProductManufacturer;
	}

	public String getClientProductModel() {
		return makeNotNull(clientProductModel);
	}

	public void setClientProductModel(String clientProductModel) {
		this.clientProductModel = clientProductModel;
	}

	public String getClientProductSerialNumber() {
		return makeNotNull(clientProductSerialNumber);
	}

	public void setClientProductSerialNumber(String clientProductSerialNumber) {
		this.clientProductSerialNumber = clientProductSerialNumber;
	}

	public boolean isRefunded() {
		return refunded;
	}

	public void setRefunded(boolean refunded) {
		this.refunded = refunded;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateNotNull(partId, "part identifer"));
		results.addResults(validateNotNull(quantity, "quantity"));
		results.addResults(validateNotNull(partPrice, "price"));
		results.addResults(validateNotNull(taxAmount, "taxed amount"));
		results.addResults(validateIsPositive(quantity, "quantity"));
		results.addResults(validateIsPositiveOrZero(partPrice, "price"));
		results.addResults(validateIsPositiveOrZero(taxAmount, "taxed amount"));
		return results;
	}
}
