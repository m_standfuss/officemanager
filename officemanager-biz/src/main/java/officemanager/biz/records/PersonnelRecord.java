package officemanager.biz.records;

import java.util.Date;

public class PersonnelRecord extends PersonRecord implements IValidateRecord {

	private Long personnelId;
	private String username;
	private Date startDttm;
	private Date terminatedDttm;
	private boolean w2OnFile;
	private CodeValueRecord personnelStatus;
	private CodeValueRecord personnelType;

	public PersonnelRecord() {
	}

	public PersonnelRecord(PersonRecord record) {
		super(record);
	}

	public Long getPersonnelId() {
		return personnelId;
	}

	public void setPersonnelId(Long personnelId) {
		this.personnelId = personnelId;
	}

	public CodeValueRecord getPersonnelStatus() {
		return makeNotNull(personnelStatus);
	}

	public void setPersonnelStatus(CodeValueRecord personnelStatus) {
		this.personnelStatus = personnelStatus;
	}

	public CodeValueRecord getPersonnelType() {
		return personnelType;
	}

	public void setPersonnelType(CodeValueRecord personnelType) {
		this.personnelType = personnelType;
	}

	public String getUsername() {
		return makeNotNull(username);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getStartDttm() {
		return startDttm;
	}

	public void setStartDttm(Date startDttm) {
		this.startDttm = startDttm;
	}

	public Date getTerminatedDttm() {
		return terminatedDttm;
	}

	public void setTerminatedDttm(Date terminatedDttm) {
		this.terminatedDttm = terminatedDttm;
	}

	public boolean isW2OnFile() {
		return w2OnFile;
	}

	public void setW2OnFile(boolean w2OnFile) {
		this.w2OnFile = w2OnFile;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(super.validateRecord());
		results.addResults(validateCodeValueSet(personnelStatus, "personnel status"));
		results.addResults(validateCodeValueSet(personnelType, "personnel type"));
		return super.validateRecord();
	}
}
