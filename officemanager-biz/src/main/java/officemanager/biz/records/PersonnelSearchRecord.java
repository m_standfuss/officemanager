package officemanager.biz.records;

import java.util.Date;

public class PersonnelSearchRecord extends Record {

	private Long personnelId;
	private String nameFullFormatted;
	private String primaryPhone;
	private String email;
	private CodeValueRecord personnelStatus;
	private CodeValueRecord personnelType;

	private Date startDttm;
	private boolean w2OnFile;

	public Long getPersonnelId() {
		return personnelId;
	}

	public void setPersonnelId(Long personnelId) {
		this.personnelId = personnelId;
	}

	public String getNameFullFormatted() {
		return makeNotNull(nameFullFormatted);
	}

	public void setNameFullFormatted(String nameFullFormatted) {
		this.nameFullFormatted = nameFullFormatted;
	}

	public String getPrimaryPhone() {
		return makeNotNull(primaryPhone);
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getEmail() {
		return makeNotNull(email);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CodeValueRecord getPersonnelStatus() {
		return makeNotNull(personnelStatus);
	}

	public void setPersonnelStatus(CodeValueRecord personnelStatus) {
		this.personnelStatus = personnelStatus;
	}

	public CodeValueRecord getPersonnelType() {
		return makeNotNull(personnelType);
	}

	public void setPersonnelType(CodeValueRecord personnelType) {
		this.personnelType = personnelType;
	}

	public Date getStartDttm() {
		return startDttm;
	}

	public void setStartDttm(Date startDttm) {
		this.startDttm = startDttm;
	}

	public boolean getW2OnFile() {
		return w2OnFile;
	}

	public void setW2OnFile(boolean w2OnFile) {
		this.w2OnFile = w2OnFile;
	}
}
