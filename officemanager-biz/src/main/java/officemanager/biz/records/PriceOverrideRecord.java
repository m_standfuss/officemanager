package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.Date;

public class PriceOverrideRecord extends Record implements IValidateRecord {

	public enum EntityName {
		PART, FLAT_RATE_SERVICE
	};

	private Long orderId;
	private EntityName entityName;
	private Long entityId;
	private String overrideReason;
	private BigDecimal originalAmount;
	private BigDecimal overrideAmount;
	private Date overrideDttm;
	private Long overridePrsnlId;
	private String overridePrsnlName;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public EntityName getEntityName() {
		return entityName;
	}

	public void setEntityName(EntityName entityName) {
		this.entityName = entityName;
	}

	public void setEntityName(String entityName) {
		try {
			this.entityName = EntityName.valueOf(entityName);
		} catch (IllegalArgumentException e) {}
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getOverrideReason() {
		return overrideReason;
	}

	public void setOverrideReason(String overrideReason) {
		this.overrideReason = overrideReason;
	}

	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}

	public BigDecimal getOverrideAmount() {
		return overrideAmount;
	}

	public void setOverrideAmount(BigDecimal overrideAmount) {
		this.overrideAmount = overrideAmount;
	}

	public Date getOverrideDttm() {
		return overrideDttm;
	}

	public void setOverrideDttm(Date overrideDttm) {
		this.overrideDttm = overrideDttm;
	}

	public Long getOverridePrsnlId() {
		return overridePrsnlId;
	}

	public void setOverridePrsnlId(Long overridePrsnlId) {
		this.overridePrsnlId = overridePrsnlId;
	}

	public String getOverridePrsnlName() {
		return makeNotNull(overridePrsnlName);
	}

	public void setOverridePrsnlName(String overridePrsnlName) {
		this.overridePrsnlName = overridePrsnlName;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(
				validateNotNull(orderId, "order identifier").addResults(validateNotNull(entityName, "entity name"))
				.addResults(validateNotNull(entityId, "entity id"))
				.addResults(validateStringIsSet(overrideReason, "override reason"))
				.addResults(validateNotNull(originalAmount, "original price amount"))
				.addResults(validateNotNull(overrideAmount, "overridden price amount"))
				.addResults(validateNotNull(overrideDttm, "override date and time"))
				.addResults(validateNotNull(overridePrsnlId, "override personnel id"))
				.addResults(validateStringLength(overrideReason, "override reason", 255, null)));
		return results;
	}
}
