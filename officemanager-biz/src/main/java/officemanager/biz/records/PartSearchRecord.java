package officemanager.biz.records;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;

public class PartSearchRecord extends Record {

	private final Long partId;

	private String partName;
	private String partManufacturer;
	private String partDescription;
	private String inventoryStatus;
	private String manufacturerId;

	private BigDecimal basePrice;
	private BigDecimal currentPrice;
	private boolean isOnSale;

	private int amountSold;
	private int inventoryCount;

	/**
	 * Constructor for creating a part selection record for a part that is not
	 * already a part of an order.
	 * 
	 * @param partId
	 */
	public PartSearchRecord(Long partId) {
		checkNotNull(partId);
		this.partId = partId;
	}

	public Long getPartId() {
		return partId;
	}

	public String getPartName() {
		return makeNotNull(partName);
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getPartManufacturer() {
		return makeNotNull(partManufacturer);
	}

	public void setPartManufacturer(String partManufacturer) {
		this.partManufacturer = partManufacturer;
	}

	public String getPartDescription() {
		return makeNotNull(partDescription);
	}

	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}

	public String getInventoryStatus() {
		return makeNotNull(inventoryStatus);
	}

	public void setInventoryStatus(String inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}

	public int getInventoryCount() {
		return inventoryCount;
	}

	public void setInventoryCount(int inventoryCount) {
		this.inventoryCount = inventoryCount;
	}

	public String getManufacturerId() {
		return makeNotNull(manufacturerId);
	}

	public void setManufacturerId(String manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(BigDecimal price) {
		currentPrice = price;
	}

	public boolean isOnSale() {
		return isOnSale;
	}

	public void setOnSale(boolean isOnSale) {
		this.isOnSale = isOnSale;
	}

	public int getAmountSold() {
		return amountSold;
	}

	public void setAmountSold(int amountSold) {
		this.amountSold = amountSold;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (partId == null ? 0 : partId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PartSearchRecord)) {
			return false;
		}
		PartSearchRecord other = (PartSearchRecord) obj;
		if (partId == null) {
			if (other.partId != null) {
				return false;
			}
		} else if (!partId.equals(other.partId)) {
			return false;
		}
		return true;
	}
}
