package officemanager.biz.records;

public interface IValidateRecord {

	/**
	 * A method used to validate input and return error messages that an end
	 * user would understand as to why their action cannot be performed.
	 * 
	 * This should only be used to validate things set/provided by the user. Any
	 * programmatically set values should be validated in another way, ie
	 * through constructor.
	 * 
	 * @return The validation results.
	 */
	public ValidationResults validateRecord();
}
