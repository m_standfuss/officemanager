package officemanager.biz.records;

import java.math.BigDecimal;

public class FlatRateServiceRecord extends Record implements IValidateRecord {

	private Long flatRateServiceId;
	private int amountSold;
	private String barcode;
	private BigDecimal basePrice;
	private CodeValueRecord category;
	private CodeValueRecord productType;
	private String serviceName;
	private LongTextRecord serviceDescription;

	public FlatRateServiceRecord() {
		this(null);
	}

	public FlatRateServiceRecord(Long flatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;
	}

	public int getAmountSold() {
		return amountSold;
	}

	public void setAmountSold(int amountSold) {
		this.amountSold = amountSold;
	}

	public String getBarcode() {
		return makeNotNull(barcode);
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public CodeValueRecord getCategory() {
		return makeNotNull(category);
	}

	public void setCategory(CodeValueRecord category) {
		this.category = category;
	}

	public CodeValueRecord getProductType() {
		return makeNotNull(productType);
	}

	public void setProductType(CodeValueRecord productType) {
		this.productType = productType;
	}

	public String getServiceName() {
		return makeNotNull(serviceName);
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public LongTextRecord getServiceDescription() {
		return makeNotNull(serviceDescription);
	}

	public void setServiceDescription(LongTextRecord serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public Long getFlatRateServiceId() {
		return flatRateServiceId;
	}

	public void setFlatRateServiceId(Long flatRateServiceId) {
		this.flatRateServiceId = flatRateServiceId;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateStringIsSet(serviceName, "service name"));
		results.addResults(validateNotNull(basePrice, "price"));
		results.addResults(validateCodeValueSet(productType, "product type"));
		results.addResults(validateCodeValueSet(category, "category"));

		results.addResults(validateStringLength(serviceName, "service name", 50, null));
		results.addResults(validateIsPositiveOrZero(amountSold, "amount sold"));
		results.addResults(validateStringLength(barcode, "barcode", 25, null));
		results.addResults(validateIsPositive(basePrice, "price"));

		if (serviceDescription != null) {
			results.addResults(serviceDescription.validateRecord());
		}
		return results;
	}

	@Override
	public String toString() {
		return "FlatRateServiceRecord [flatRateServiceId=" + flatRateServiceId + ", amountSold=" + amountSold
				+ ", barcode=" + barcode + ", basePrice=" + basePrice + ", category=" + category + ", productType="
				+ productType + ", serviceName=" + serviceName + ", serviceDescription=" + serviceDescription + "]";
	}
}
