package officemanager.biz.records;

public class PermissionRecord extends Record {

	private String permissionName;
	private String display;
	private CodeValueRecord category;

	public String getPermissionName() {
		return makeNotNull(permissionName);
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getDisplay() {
		return makeNotNull(display);
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public CodeValueRecord getCategory() {
		return category;
	}

	public void setCategory(CodeValueRecord category) {
		this.category = category;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PermissionRecord [permissionName=").append(permissionName).append(", display=").append(display)
				.append(", category=").append(category).append("]");
		return builder.toString();
	}
}
