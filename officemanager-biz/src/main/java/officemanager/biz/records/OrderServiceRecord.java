package officemanager.biz.records;

import java.math.BigDecimal;

public class OrderServiceRecord extends Record implements IValidateRecord {

	private Long serviceId;
	private String serviceName;
	private Long clientProductId;
	private BigDecimal hoursQnt;
	private BigDecimal pricePerHour;
	private LongTextRecord serviceText;
	private String clientProductManufacturer;
	private String clientProductModel;
	private String clientProductSerialNumber;
	private boolean refunded;

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return makeNotNull(serviceName);
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Long getClientProductId() {
		return clientProductId;
	}

	public void setClientProductId(Long clientProductId) {
		this.clientProductId = clientProductId;
	}

	public BigDecimal getHoursQnt() {
		return hoursQnt;
	}

	public void setHoursQnt(BigDecimal hoursQnt) {
		this.hoursQnt = hoursQnt;
	}

	public BigDecimal getPricePerHour() {
		return pricePerHour;
	}

	public void setPricePerHour(BigDecimal pricePerHour) {
		this.pricePerHour = pricePerHour;
	}

	public LongTextRecord getServiceText() {
		return serviceText;
	}

	public void setServiceText(LongTextRecord serviceText) {
		this.serviceText = serviceText;
	}

	public String getClientProductManufacturer() {
		return makeNotNull(clientProductManufacturer);
	}

	public void setClientProductManufacturer(String clientProductManufacturer) {
		this.clientProductManufacturer = clientProductManufacturer;
	}

	public String getClientProductModel() {
		return makeNotNull(clientProductModel);
	}

	public void setClientProductModel(String clientProductModel) {
		this.clientProductModel = clientProductModel;
	}

	public String getClientProductSerialNumber() {
		return makeNotNull(clientProductSerialNumber);
	}

	public void setClientProductSerialNumber(String clientProductSerialNumber) {
		this.clientProductSerialNumber = clientProductSerialNumber;
	}

	public boolean isRefunded() {
		return refunded;
	}

	public void setRefunded(boolean refunded) {
		this.refunded = refunded;
	}

	@Override
	public ValidationResults validateRecord() {
		ValidationResults results = new ValidationResults();
		results.addResults(validateStringIsSet(serviceName, "service name"));
		results.addResults(validateStringLength(serviceName, "service name", 50, null));
		results.addResults(validateNotNull(clientProductId, "client product"));
		results.addResults(validateNotNull(hoursQnt, "hours"));
		results.addResults(validateNotNull(pricePerHour, "price per hour"));

		if (serviceText != null) {
			results.addResults(serviceText.validateRecord());
		}

		results.addResults(validateIsPositiveOrZero(hoursQnt, "hours"));
		results.addResults(validateIsPositiveOrZero(pricePerHour, "price"));
		return results;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((serviceId == null) ? 0 : serviceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OrderServiceRecord)) {
			return false;
		}
		OrderServiceRecord other = (OrderServiceRecord) obj;
		if (serviceId == null || other.serviceId == null) {
			return false;
		}
		return serviceId.equals(other.serviceId);
	}
}
