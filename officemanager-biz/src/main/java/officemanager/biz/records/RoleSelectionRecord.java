package officemanager.biz.records;

public class RoleSelectionRecord extends Record implements Comparable<RoleSelectionRecord> {

	private Long roleId;
	private String roleName;

	public RoleSelectionRecord() {}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return makeNotNull(roleName);
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public int compareTo(RoleSelectionRecord o) {
		return getRoleName().compareTo(o.getRoleName());
	}

}
