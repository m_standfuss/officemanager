package officemanager.biz.records;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Represents a order search record.
 * 
 * @author Mike
 *
 */
public class OrderSearchRecord extends Record {

	private final Long orderId;
	private final String clientName;
	private final String clientPhone;
	private final Date openedDttm;
	private final String openedBy;
	private final BigDecimal amount;
	private final String orderStatus;
	private final String orderType;
	private final int productCount;
	private final Date closedDttm;
	private final String closedBy;

	public OrderSearchRecord(Long orderId, String clientName, String clientPhone, Date openedDttm, String openedBy,
			BigDecimal amount, String orderStatus, String orderType, int productCount, Date closedDttm,
			String closedBy) {
		this.orderId = orderId;
		this.clientName = clientName;
		this.clientPhone = clientPhone;
		this.openedDttm = openedDttm;
		this.openedBy = openedBy;
		this.amount = amount;
		this.orderStatus = orderStatus;
		this.orderType = orderType;
		this.productCount = productCount;
		this.closedDttm = closedDttm;
		this.closedBy = closedBy;
	}

	public long getOrderId() {
		return orderId;
	}

	public String getClientName() {
		return makeNotNull(clientName);
	}

	public String getClientPhone() {
		return makeNotNull(clientPhone);
	}

	public Date getOpenedDttm() {
		return openedDttm;
	}

	public String getOpenedBy() {
		return makeNotNull(openedBy);
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getOrderStatus() {
		return makeNotNull(orderStatus);
	}

	public String getOrderType() {
		return makeNotNull(orderType);
	}

	public int getProductCount() {
		return productCount;
	}

	public Date getClosedDttm() {
		return closedDttm;
	}

	public String getClosedBy() {
		return makeNotNull(closedBy);
	}
}
