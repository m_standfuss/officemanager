package officemanager.biz;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;

import officemanager.biz.records.OrderFlatRateServiceRecord;
import officemanager.biz.records.OrderPartRecord;
import officemanager.biz.records.OrderRecord;
import officemanager.biz.records.OrderServiceRecord;
import officemanager.biz.records.PaymentRecord;
import officemanager.biz.records.SaleRecord;
import officemanager.data.models.OrderToFlatRateService;
import officemanager.data.models.OrderToPart;
import officemanager.data.models.Service;
import officemanager.utility.CollectionUtility;

/**
 * A business class to handle all of the calculations when it comes to currency
 * related so that all rounding and scaling is contained in a single class
 * throughout the application.
 * 
 * @author Mike
 *
 */
public class Calculator {
	/**
	 * All calculations should use this context in order to ensure proper
	 * rounding patterns.
	 */
	private static RoundingMode DEFAULT_ROUNDING = RoundingMode.UP;
	private static MathContext DEFAULT_CONTEXT = new MathContext(6, DEFAULT_ROUNDING);
	private static int DEFAULT_SCALE = 4;

	private static BigDecimal TAX_RATE;

	/**
	 * Sets the current tax rate to be used to calculate the tax on parts.
	 * 
	 * @param taxRate
	 *            The tax rate. Should be expressed as a percentage ie 7.95%
	 *            would be .0795 not 7.95.
	 * 
	 * @throws IllegalArgumentException
	 *             If the specified value is not greater than
	 *             {@link BigDecimal#ZERO} and less than {@link BigDecimal#ONE}.
	 */
	public static void setTaxRate(BigDecimal taxRate) {
		checkArgument(taxRate.compareTo(BigDecimal.ZERO) > 0, "The tax rate should be greater than zero.");
		checkArgument(taxRate.compareTo(BigDecimal.ONE) < 0, "The tax rate should be less than one.");
		TAX_RATE = taxRate;
	}

	public static BigDecimal add(BigDecimal bd1, BigDecimal bd2) {
		return bd1.add(bd2, DEFAULT_CONTEXT);
	}

	public static BigDecimal getZero() {
		return BigDecimal.ZERO.setScale(DEFAULT_SCALE, RoundingMode.UP);
	}

	public static boolean isZeroAmount(BigDecimal amount) {
		return amountsMatch(getZero(), amount);
	}

	public static BigDecimal negate(BigDecimal amount) {
		return amount.multiply(new BigDecimal("-1").setScale(DEFAULT_SCALE, RoundingMode.UP));
	}

	public static boolean amountsMatch(BigDecimal amount1, BigDecimal amount2){
		checkNotNull(amount1);
		checkNotNull(amount2);
		BigDecimal diff = amount1.subtract(amount2, DEFAULT_CONTEXT);
		return diff.abs().compareTo(new BigDecimal(".005")) < 0;
	}

	/**
	 * Takes all the parts of an order an calculates the total for the order.
	 * 
	 * @param frsList
	 *            The flat rate services for the order.
	 * @param parts
	 *            The parts for the order.
	 * @param services
	 *            The services for the order.
	 * 
	 * @return The total for the order.
	 */
	public static BigDecimal calculateOrderTotal(OrderRecord record) {
		BigDecimal total = getZero();

		if (record.getFlatRates() != null) {
			for (final OrderFlatRateServiceRecord otfrs : record.getFlatRates()) {
				total = total.add(totalFlatRateService(otfrs), DEFAULT_CONTEXT);
			}
		}

		if (record.getParts() != null) {
			for (final OrderPartRecord otp : record.getParts()) {
				if (otp.isRefunded()) {
					continue;
				}
				total = total.add(totalPart(otp), DEFAULT_CONTEXT);
			}
		}

		if (record.getServices() != null) {
			for (final OrderServiceRecord service : record.getServices()) {
				total = total.add(totalService(service), DEFAULT_CONTEXT);
			}
		}
		return total;
	}

	public static OrderTotal getOrdersTotal(List<OrderRecord> records){
		OrderTotal orderTotal = new OrderTotal(getZero(), getZero(), getZero(), getZero(), getZero(), getZero());
		for (OrderRecord record : records) {
			orderTotal = addOrderTotals(orderTotal, getOrderTotal(record));
		}
		return orderTotal;
	}

	private static OrderTotal addOrderTotals(OrderTotal total1, OrderTotal total2) {
		BigDecimal orderTotal = total1.orderTotal.add(total2.orderTotal, DEFAULT_CONTEXT);
		BigDecimal partTotal = total1.merchandiseTotal.add(total2.merchandiseTotal, DEFAULT_CONTEXT);
		BigDecimal partTaxExemptTotal = total1.merchTaxExempt.add(total2.merchTaxExempt, DEFAULT_CONTEXT);
		BigDecimal laborTotal = total1.laborTotal.add(total2.laborTotal, DEFAULT_CONTEXT);
		BigDecimal taxTotal = total1.taxTotal.add(total2.taxTotal, DEFAULT_CONTEXT);
		BigDecimal taxExemptTotal = total1.taxExempt.add(total2.taxExempt, DEFAULT_CONTEXT);

		return new OrderTotal(orderTotal, partTotal, partTaxExemptTotal, laborTotal, taxTotal, taxExemptTotal);
	}

	public static OrderTotal getOrderTotal(OrderRecord record) {
		BigDecimal orderTotal = getZero();
		BigDecimal partTotal = getZero();
		BigDecimal partTaxExemptTotal = getZero();
		BigDecimal laborTotal = getZero();
		BigDecimal taxTotal = getZero();
		BigDecimal taxExemptTotal = getZero();

		if (record.getFlatRates() != null) {
			for (final OrderFlatRateServiceRecord otfrs : record.getFlatRates()) {
				laborTotal = laborTotal.add(totalFlatRateService(otfrs), DEFAULT_CONTEXT);
			}
		}

		if (record.getServices() != null) {
			for (final OrderServiceRecord service : record.getServices()) {
				laborTotal = laborTotal.add(totalService(service), DEFAULT_CONTEXT);
			}
		}

		if (record.getParts() != null) {
			for (final OrderPartRecord otp : record.getParts()) {
				if (otp.isRefunded()) {
					continue;
				}
				BigDecimal partPrice = calculatePartPreTax(otp);
				BigDecimal tax = calculatePrice(otp.getTaxAmount(), otp.getQuantity());

				partTotal = partTotal.add(partPrice, DEFAULT_CONTEXT);
				if(otp.isTaxExempt()){
					taxExemptTotal = taxExemptTotal.add(tax, DEFAULT_CONTEXT);
					partTaxExemptTotal = partTaxExemptTotal.add(partPrice);
				}else {
					taxTotal = taxTotal.add(tax);
				}
			}
		}

		orderTotal = laborTotal.add(partTotal, DEFAULT_CONTEXT).add(taxTotal, DEFAULT_CONTEXT);
		return new OrderTotal(orderTotal, partTotal, partTaxExemptTotal, laborTotal, taxTotal, taxExemptTotal);
	}

	public static PaymentsTotal getPaymentTotals(List<PaymentRecord> payments) {
		BigDecimal total = getZero();
		BigDecimal cashTotal = getZero();
		BigDecimal checkTotal = getZero();
		BigDecimal creditCardTotal = getZero();
		BigDecimal clientCreditTotal = getZero();
		BigDecimal pendingTotal = getZero();
		BigDecimal giftCardTotal = getZero();
		BigDecimal financingTotal = getZero();
		BigDecimal tradeInTotal = getZero();
		BigDecimal refundTotal = getZero();

		for (PaymentRecord payment : payments) {
			switch (payment.getPaymentType()) {
			case CASH:
				cashTotal = cashTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				total = total.add(payment.getAmount(), DEFAULT_CONTEXT);
				break;
			case CHECK:
				checkTotal = checkTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				total = total.add(payment.getAmount(), DEFAULT_CONTEXT);
				break;
			case CLIENT_CREDIT:
				clientCreditTotal = clientCreditTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				break;
			case CREDIT_CARD:
				creditCardTotal = creditCardTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				total = total.add(payment.getAmount(), DEFAULT_CONTEXT);
				break;
			case FINANCING:
				financingTotal = financingTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				total = total.add(payment.getAmount(), DEFAULT_CONTEXT);
				break;
			case GIFT_CARD:
				giftCardTotal = giftCardTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				break;
			case PENDING:
				pendingTotal = pendingTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				break;
			case TRADE_IN:
				tradeInTotal = tradeInTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				total = total.add(payment.getAmount(), DEFAULT_CONTEXT);
				break;
			case REFUND:
				refundTotal = refundTotal.add(payment.getAmount(), DEFAULT_CONTEXT);
				total = total.add(payment.getAmount(), DEFAULT_CONTEXT);
			}
		}

		return new PaymentsTotal(total, cashTotal, checkTotal, creditCardTotal, clientCreditTotal, pendingTotal,
				giftCardTotal, financingTotal, tradeInTotal, refundTotal);
	}

	/**
	 * Takes all the parts of an order an calculates the total for the order.
	 * 
	 * @param frsList
	 *            The flat rate services for the order.
	 * @param parts
	 *            The parts for the order.
	 * @param services
	 *            The services for the order.
	 * 
	 * @return The total for the order.
	 */
	public static BigDecimal calculateOrderTotal(List<OrderToFlatRateService> flatRateServices, List<OrderToPart> parts,
			List<Service> services) {
		BigDecimal total = getZero();

		if (flatRateServices != null) {
			for (final OrderToFlatRateService otfrs : flatRateServices) {
				total = total.add(totalFlatRateService(otfrs), DEFAULT_CONTEXT);
			}
		}

		if (parts != null) {
			for (final OrderToPart otp : parts) {
				if (otp.isRefundedInd()) {
					continue;
				}
				total = total.add(totalPart(otp), DEFAULT_CONTEXT);
			}
		}

		if (services != null) {
			for (final Service service : services) {
				total = total.add(totalService(service), DEFAULT_CONTEXT);
			}
		}
		return total;
	}

	/**
	 * Takes all the payment options of an order and calculates the total for
	 * the order.
	 * 
	 * @param payments
	 *            The list of active payments for the order.
	 * @param tradeIns
	 *            The list of active trade ins for the order.
	 * @return The total amount of payments for the order.
	 */
	public static BigDecimal calculatePaymentTotal(Collection<PaymentRecord> payments) {
		BigDecimal total = getZero();
		if (payments != null) {
			for (PaymentRecord p : payments) {
				total = total.add(p.getAmount(), DEFAULT_CONTEXT);
			}
		}
		return total;
	}

	/**
	 * Calculates a sale price based on a list of sales.
	 * 
	 * @param basePrice
	 *            The base price to start from.
	 * @param flatOffSales
	 *            The list of flat off sales ie $5.00 off part.
	 * @param percentageOffSales
	 *            The list of percentage off sales ie 10% off part.
	 * @return The sale price, if no sales then will be the same as the base
	 *         price.
	 * @throws NullPointerException
	 *             If any argument is null.
	 */
	public static BigDecimal calculateSalePrice(BigDecimal basePrice, List<SaleRecord> flatOffSales,
			List<SaleRecord> percentageOffSales) {
		checkNotNull(basePrice);
		checkNotNull(flatOffSales);
		checkNotNull(percentageOffSales);
		BigDecimal price = basePrice;
		for (SaleRecord flatOffSale : flatOffSales) {
			price = price.subtract(flatOffSale.getAmountOff(), DEFAULT_CONTEXT);
		}

		for (SaleRecord percentageOffSale : percentageOffSales) {
			BigDecimal amountOff = price.multiply(
					percentageOffSale.getAmountOff().divide(new BigDecimal("100"), DEFAULT_CONTEXT), DEFAULT_CONTEXT);
			price = price.subtract(amountOff, DEFAULT_CONTEXT);
		}
		return price;
	}

	public static BigDecimal totalFlatRateServices(List<OrderFlatRateServiceRecord> otfrsList) {
		BigDecimal total = getZero();
		for (OrderFlatRateServiceRecord otfrs : otfrsList) {
			total = total.add(totalFlatRateService(otfrs), DEFAULT_CONTEXT);
		}
		return total;
	}

	/**
	 * Gets the total for an order flat rate service record.
	 * 
	 * @param otfrs
	 *            The record to total.
	 * @return The total.
	 */
	public static BigDecimal totalFlatRateService(OrderFlatRateServiceRecord otfrs) {
		if (otfrs.isRefunded()) {
			return getZero();
		}
		return otfrs.getServicePrice();
	}

	/**
	 * Gets the total for an order to flat rate service model.
	 * 
	 * @param otfrs
	 *            The model to total.
	 * @return The total.
	 */
	private static BigDecimal totalFlatRateService(OrderToFlatRateService otfrs) {
		if (otfrs.isRefundedInd()) {
			return getZero();
		}
		return otfrs.getServicePrice();
	}

	public static BigDecimal totalParts(List<OrderPartRecord> parts) {
		BigDecimal amount = getZero();
		for (OrderPartRecord part : parts) {
			amount = amount.add(totalPart(part), DEFAULT_CONTEXT);
		}
		return amount;
	}

	/**
	 * Gets the total for an order part record.
	 * 
	 * @param otp
	 *            The record to total.
	 * @return The total.
	 */
	public static BigDecimal totalPart(OrderPartRecord otp) {
		if (otp.isRefunded()) {
			return getZero();
		}
		return totalPart(otp.getPartPrice(), BigDecimal.valueOf(otp.getQuantity()), otp.getTaxAmount(),
				otp.isTaxExempt());
	}

	/**
	 * Gets the total for an order to part model.
	 * 
	 * @param otp
	 *            The model to total.
	 * @return The total for the model.
	 */
	private static BigDecimal totalPart(OrderToPart otp) {
		if (otp.isRefundedInd()) {
			return getZero();
		}
		return totalPart(otp.getPartPrice(), BigDecimal.valueOf(otp.getQuantity()), otp.getTaxedAmount(),
				otp.isTaxExemptInd());
	}

	public static BigDecimal totalPart(BigDecimal partPrice, BigDecimal quantity, BigDecimal taxAmount,
			boolean isTaxExempt) {
		BigDecimal preTax = calculatePrice(partPrice, quantity);
		if (isTaxExempt) {
			return preTax;
		}else{
			return preTax.add(calculatePrice(quantity, taxAmount));
		}
	}

	public static BigDecimal calculatePartPreTax(OrderPartRecord otp) {
		return calculatePrice(otp.getPartPrice(), otp.getQuantity());
	}

	public static BigDecimal totalServices(List<OrderServiceRecord> services) {
		BigDecimal total = getZero();
		for (OrderServiceRecord service : services) {
			total = total.add(totalService(service), DEFAULT_CONTEXT);
		}
		return total;
	}

	/**
	 * Gets the total for an order service record.
	 * 
	 * @param service
	 *            The record to total.
	 * @return The total.
	 */
	public static BigDecimal totalService(OrderServiceRecord service) {
		if (service.isRefunded()) {
			return getZero();
		}
		return service.getPricePerHour().multiply(service.getHoursQnt(), DEFAULT_CONTEXT);
	}

	/**
	 * Gets the total for an service model.
	 * 
	 * @param service
	 *            The model to total.
	 * @return The total.
	 */
	private static BigDecimal totalService(Service service) {
		if (service.isRefundedInd()) {
			return getZero();
		}
		return service.getPricePerHour().multiply(service.getHoursQnt(), DEFAULT_CONTEXT);
	}

	/**
	 * Calculates the amount of tax to be applied to the item.
	 * 
	 * @param priceBeforeTax
	 *            The price of the item.
	 * @return The amount of tax to be applied to the item.
	 */
	public static BigDecimal calculateTax(BigDecimal priceBeforeTax) {
		if (TAX_RATE == null) {
			throw new IllegalStateException("The tax rate must be set in order to calculate part prices properly.");
		}
		return priceBeforeTax.multiply(TAX_RATE, DEFAULT_CONTEXT);
	}

	/**
	 * Calculates the total price for a part given the part's price and
	 * quantity, does not apply tax to the price.
	 * 
	 * @param price
	 *            The price of the item.
	 * @param quantity
	 *            The amount of the item.
	 * @return The total price.
	 */
	public static BigDecimal calculatePrice(BigDecimal price, Long quantity) {
		if (quantity == null) {
			quantity = 0L;
		}
		return price.multiply(new BigDecimal(quantity), DEFAULT_CONTEXT);
	}

	public static BigDecimal calculatePrice(BigDecimal price, BigDecimal quantity) {
		if (quantity == null) {
			quantity = getZero();
		}
		return price.multiply(quantity, DEFAULT_CONTEXT);
	}

	/**
	 * Gets an accounts balance after applying all the debits and credits to the
	 * account.
	 * <ul>
	 * <li>A credit is a payment to the account.</li>
	 * <li>A debit is when the account is used to make a payment on an order
	 * </li>
	 * </ul>
	 * 
	 * @param credits
	 *            The list of credits to the account.
	 * @param debits
	 *            The list of debits to the account.
	 * @param openingBalance
	 *            The balance to apply all credits/debits to.
	 * @return The account balance.
	 */
	public static BigDecimal calculateAccountBalance(List<BigDecimal> credits,
			List<BigDecimal> debits) {
		BigDecimal balance = getZero();
		if (!CollectionUtility.isNullOrEmpty(credits)) {

			for (BigDecimal credit : credits) {
				balance = balance.add(credit, DEFAULT_CONTEXT);
			}
		}

		if (!CollectionUtility.isNullOrEmpty(debits)) {
			for (BigDecimal debit : debits) {
				balance = balance.subtract(debit, DEFAULT_CONTEXT);
			}
		}
		return balance;
	}

	public static BigDecimal calculateBalance(BigDecimal cost, List<PaymentRecord> payments) {
		return calculateBalance(cost, calculatePaymentTotal(payments));
	}

	public static BigDecimal calculateBalance(BigDecimal cost, BigDecimal payments) {
		BigDecimal diff = cost.subtract(payments, DEFAULT_CONTEXT);
		if (amountsMatch(diff, getZero())) {
			return getZero();
		}
		return diff;
	}
}
