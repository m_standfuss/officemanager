package officemanager.biz;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import officemanager.biz.maps.EntityMap;
import officemanager.data.access.CodeValueDao;
import officemanager.data.models.CodeValue;
import officemanager.utility.Tuple;

public class CodeValueCache {

	private static final Logger logger = LogManager.getLogger(CodeValueCache.class);

	private final CodeValueDao codeValueDao;
	private final EntityMap<CodeValue> entityMap;
	private final Map<Long, List<CodeValue>> codeSetMap;
	private final Map<Tuple<Long, String>, List<CodeValue>> codeSetAndCdfMap;

	public CodeValueCache() {
		codeValueDao = new CodeValueDao();
		entityMap = EntityMap.newEntityMap();
		codeSetMap = Maps.newHashMap();
		codeSetAndCdfMap = Maps.newHashMap();
	}

	public void clearCache() {
		entityMap.clear();
		codeSetMap.clear();
		codeSetAndCdfMap.clear();
	}

	public CodeValue getCodeValue(Long codeValue) {
		logger.debug("Starting getCodeValue for codeValue=" + codeValue);
		CodeValue toRtrn = entityMap.getEntity(codeValue);
		if (toRtrn == null) {
			logger.debug("Code value not found on cache querying data source.");
			toRtrn = _getCodeValue(codeValue);
			if (toRtrn == null) {
				logger.warn("Code value requested (" + codeValue + ") could not be found.");
			}
		} else {
			logger.debug("Code value was found on cache.");
		}
		return toRtrn;
	}

	public List<CodeValue> getCodeValues(List<Long> codeValues) {
		logger.debug("Starting getCodeValues for codeValues=" + Arrays.toString(codeValues.toArray()));
		List<Long> unfoundCodeValues = Lists.newArrayList();
		List<CodeValue> foundCodeValues = Lists.newArrayList();
		for (Long codeValue : codeValues) {
			CodeValue codeValueModel = entityMap.getEntity(codeValue);
			if (codeValueModel == null) {
				logger.debug("Code value " + codeValue + " not found on cache querying data source.");
				unfoundCodeValues.add(codeValue);
			} else {
				logger.debug("Code value " + codeValue + " found on cache.");
				foundCodeValues.add(codeValueModel);
			}
		}

		foundCodeValues.addAll(_getCodeValuesByCodeValues(unfoundCodeValues));
		return foundCodeValues;
	}

	public CodeValue getCodeValue(Long codeSet, String cdfMeaning) {
		logger.debug("Starting getCodeValue for codeSet=" + codeSet + " and cdfMeaning=" + cdfMeaning);
		List<CodeValue> codeValues = getCodeValuesBySetAndCdf(codeSet, Lists.newArrayList(cdfMeaning));
		if (codeValues.isEmpty()) {
			logger.warn("Code value requested (codeSet=" + codeSet + " and cdfMeaning=" + cdfMeaning
					+ ") could not be found.");
			return null;
		}
		if (codeValues.size() > 0) {
			logger.debug("More then one code value found with that cdf meaning returning first.");
		}
		return codeValues.get(0);
	}

	public List<CodeValue> getCodeValuesBySetAndCdf(Long codeSet, List<String> cdfMeanings) {
		logger.debug("Starting getCodeValue for codeSet=" + codeSet + " and cdfMeanings="
				+ Arrays.toString(cdfMeanings.toArray()));
		List<Tuple<Long, String>> unfoundCodeValues = Lists.newArrayList();
		List<CodeValue> foundCodeValues = Lists.newArrayList();
		for (String cdfMeaning : cdfMeanings) {
			List<CodeValue> codeValueModels = codeSetAndCdfMap.get(Tuple.newTuple(codeSet, cdfMeaning));
			if (codeValueModels == null) {
				logger.debug("Code value with codeSet=" + codeSet + " and cdfMeaning=" + cdfMeaning
						+ " not found querying data source.");
				unfoundCodeValues.add(Tuple.newTuple(codeSet, cdfMeaning));
			} else {
				logger.debug("Code value with codeSet=" + codeSet + " and cdfMeaning=" + cdfMeaning + " found.");
				foundCodeValues.addAll(codeValueModels);
			}
		}
		foundCodeValues.addAll(_getCodeValuesByCodeSetAndCdfs(unfoundCodeValues));
		return foundCodeValues;
	}

	public List<CodeValue> getCodeValuesByCodeSet(Long codeSet) {
		logger.debug("Starting getCodeValuesByCodeSet for codeSet=" + codeSet);
		return this.getCodeValuesByCodeSet(Lists.newArrayList(codeSet));
	}

	public List<CodeValue> getCodeValuesByCodeSet(List<Long> codeSets) {
		logger.debug("Starting getCodeValuesByCodeSet for codeSets=" + Arrays.toString(codeSets.toArray()));
		List<Long> unfoundCodeSets = Lists.newArrayList();
		List<CodeValue> foundCodeValues = Lists.newArrayList();
		for (Long codeSet : codeSets) {
			List<CodeValue> codeValueModels = codeSetMap.get(codeSet);
			if (codeValueModels == null) {
				logger.debug("Code set " + codeSet + " not found querying data source.");
				unfoundCodeSets.add(codeSet);
			} else {
				logger.debug("Code set " + codeSet + " found.");
				foundCodeValues.addAll(codeValueModels);
			}
		}
		foundCodeValues.addAll(_getCodeValuesByCodeSets(unfoundCodeSets));
		return foundCodeValues;
	}

	private List<CodeValue> _getCodeValuesByCodeSetAndCdfs(List<Tuple<Long, String>> codeSetsAndCdfs) {
		Map<Long, List<String>> codeSetToCdfMeaningMap = Maps.newHashMap();
		for (Tuple<Long, String> codeSetAndCdf : codeSetsAndCdfs) {
			if (!codeSetToCdfMeaningMap.containsKey(codeSetAndCdf.item1)) {
				codeSetToCdfMeaningMap.put(codeSetAndCdf.item1, Lists.newArrayList());
			}
			codeSetToCdfMeaningMap.get(codeSetAndCdf.item1).add(codeSetAndCdf.item2);
		}

		List<CodeValue> codeValues = Lists.newArrayList();
		for (Map.Entry<Long, List<String>> codeSetAndCdfs : codeSetToCdfMeaningMap.entrySet()) {
			Long codeSet = codeSetAndCdfs.getKey();
			List<CodeValue> currentResults = codeValueDao.findByCodeSetAndCdfMeaningList(codeSet,
					codeSetAndCdfs.getValue());
			Map<String, List<CodeValue>> cdfMap = Maps.newHashMap();
			for (CodeValue cv : currentResults) {
				if (!cdfMap.containsKey(cv.getCdfMeaning())) {
					cdfMap.put(cv.getCdfMeaning(), Lists.newArrayList());
				}
				cdfMap.get(cv.getCdfMeaning()).add(cv);
			}
			for (Map.Entry<String, List<CodeValue>> cdfSpecificCvs : cdfMap.entrySet()) {
				addCodeValuesToCache(codeSet, cdfSpecificCvs.getKey(), cdfSpecificCvs.getValue());
			}
		}
		return codeValues;
	}

	private CodeValue _getCodeValue(Long codeValue) {
		CodeValue codeValueModel = codeValueDao.findById(codeValue);
		addCodeValueToCache(codeValueModel);
		return codeValueModel;
	}

	private List<CodeValue> _getCodeValuesByCodeValues(List<Long> codeValues) {
		List<CodeValue> codeValueModels = codeValueDao.findByIdList(codeValues);
		for (CodeValue cv : codeValueModels) {
			addCodeValueToCache(cv);
		}
		return codeValueModels;
	}

	private List<CodeValue> _getCodeValuesByCodeSets(List<Long> codeSets) {
		List<CodeValue> codeValues = codeValueDao.findByCodeSetList(codeSets);
		Map<Long, List<CodeValue>> codeSetMap = Maps.newHashMap();
		for (Long codeSet : codeSets) {
			codeSetMap.put(codeSet, Lists.newArrayList());
		}

		for (CodeValue cv : codeValues) {
			codeSetMap.get(cv.getCodeSet()).add(cv);
		}
		for (Map.Entry<Long, List<CodeValue>> codeSetValues : codeSetMap.entrySet()) {
			addCodeSetToCache(codeSetValues.getKey(), codeSetValues.getValue());
		}
		return codeValues;
	}

	private void addCodeValueToCache(CodeValue codeValue) {
		entityMap.add(codeValue);
	}

	private void addCodeValuesToCache(Long codeSet, String cdfMeaning, List<CodeValue> codeValues) {
		codeSetAndCdfMap.put(Tuple.newTuple(codeSet, cdfMeaning), codeValues);
		for (CodeValue cv : codeValues) {
			entityMap.add(cv);
		}
	}

	private void addCodeSetToCache(Long codeSet, List<CodeValue> codeValues) {
		codeSetMap.put(codeSet, codeValues);
		Map<String, List<CodeValue>> cdfMap = Maps.newHashMap();
		for (CodeValue cv : codeValues) {
			entityMap.add(cv);
			if (!cdfMap.containsKey(cv.getCdfMeaning())) {
				cdfMap.put(cv.getCdfMeaning(), Lists.newArrayList());
			}
			cdfMap.get(cv.getCdfMeaning()).add(cv);
		}
		for (Map.Entry<String, List<CodeValue>> cdfSpecificCvs : cdfMap.entrySet()) {
			addCodeValuesToCache(codeSet, cdfSpecificCvs.getKey(), cdfSpecificCvs.getValue());
		}
	}
}
