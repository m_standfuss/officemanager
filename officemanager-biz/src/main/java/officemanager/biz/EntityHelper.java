package officemanager.biz;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import officemanager.data.models.OfficeManagerEntity;

/**
 * Helper class to do common logic across database entities.
 * 
 * @author Mike
 *
 */
public class EntityHelper {

	private static final Logger logger = LogManager.getLogger(EntityHelper.class);

	/**
	 * 
	 * /** Parses through a list of entities and pulls out all the primary keys
	 * and puts them into a list.
	 * 
	 * If a distinct list is wanted use the getPrimaryKeySet method.
	 * 
	 * @param clazz
	 *            The class type of the primary key expected.
	 * @param entityList
	 *            The list of entities to parse through.
	 * @return The list of primary keys.
	 */
	public static <T extends Serializable> List<T> getPrimaryKeyList(Class<T> clazz,
			List<? extends OfficeManagerEntity> entityList) {
		if (entityList == null || entityList.isEmpty()) {
			return Lists.newArrayList();
		}
		final List<T> list = Lists.newArrayList();
		for (final OfficeManagerEntity entity : entityList) {
			try {
				list.add(clazz.cast(entity.getPrimaryKey()));
			} catch (final ClassCastException e) {
				logger.error("Cannot correctly cast the primary key to the expected value.", e);
			}
		}
		return list;
	}

	/**
	 * Parses through a list of entities and pulls out all the primary keys and
	 * puts them into a set.
	 * 
	 * If the result is not needed to be distinct then use the getPrimaryKeyList
	 * method.
	 * 
	 * @param entityList
	 *            The list of entities to parse through.
	 * @return The set of primary keys.
	 */
	public static <T extends Serializable> Set<T> getPrimaryKeySet(Class<T> clazz,
			List<? extends OfficeManagerEntity> entityList) {
		if (entityList == null || entityList.isEmpty()) {
			return Sets.newHashSet();
		}

		final HashSet<T> set = Sets.newHashSet();
		for (final OfficeManagerEntity entity : entityList) {
			try {
				set.add(clazz.cast(entity.getPrimaryKey()));
			} catch (final ClassCastException e) {
				logger.error("Cannot correctly cast the primary key to the expected value.", e);
			}
		}
		return set;
	}
}
