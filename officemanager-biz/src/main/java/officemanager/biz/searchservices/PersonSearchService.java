package officemanager.biz.searchservices;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import officemanager.biz.EntityHelper;
import officemanager.biz.searchingcriteria.PersonSearchCriteria;
import officemanager.data.access.PersonDao;
import officemanager.data.access.PhoneDao;
import officemanager.data.models.Person;
import officemanager.data.models.Phone;
import officemanager.utility.CollectionUtility;
import officemanager.utility.StringUtility;

public class PersonSearchService {
	private static final Logger logger = LogManager.getLogger(PersonSearchService.class);

	private final PersonDao personDao;
	private final PhoneDao phoneDao;

	public PersonSearchService() {
		personDao = new PersonDao();
		phoneDao = new PhoneDao();
	}

	/**
	 * Finds the person ids that are reflective of the searching criteria.
	 *
	 * @param searchCriteria
	 *            The searching criteria.
	 * @return The list of person ids.
	 */
	public List<Long> searchForPersonIds(PersonSearchCriteria searchCriteria) {
		logger.debug("Starting searchForPersonIds with searchCriteria=" + searchCriteria);
		checkNotNull(searchCriteria);

		if (searchCriteria.isEmpty()) {
			logger.debug("No person search criteria specified.");
			return Lists.newArrayList();
		}

		List<List<Long>> listOfPersonIdResults = Lists.newArrayList();
		if (!StringUtility.isNullOrWhiteSpace(searchCriteria.nameFirst)
				|| !StringUtility.isNullOrWhiteSpace(searchCriteria.nameLast)) {
			logger.debug("A person's first and/or last name specified for searching.");
			List<Person> personList = personDao.findActiveByNameFirstAndLastLike(searchCriteria.nameFirst,
					searchCriteria.nameLast);
			listOfPersonIdResults.add(EntityHelper.getPrimaryKeyList(Long.class, personList));
		}

		if (!StringUtility.isNullOrWhiteSpace(searchCriteria.primaryPhone)) {
			logger.debug("A client's phone specified, searching.");
			List<Phone> phoneList = phoneDao.findByPhoneNumber(searchCriteria.primaryPhone);

			List<Long> personIdList = Lists.newArrayList();
			for (Phone phone : phoneList) {
				personIdList.add(phone.getPersonId());
			}
			listOfPersonIdResults.add(personIdList);
		}

		return Lists.newArrayList(CollectionUtility.intersectCollections(listOfPersonIdResults));
	}
}